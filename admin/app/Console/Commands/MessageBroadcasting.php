<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MessageBroadcasting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'message:broadcating';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This scheduler is created for broadcasting notifications on mobile devices at their inputted date and time.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$broadcast_date = new \DateTime;
        date_default_timezone_set('Asia/Kolkata');
        $broadcast_date = date('Y-m-d H:i:00');
        $notifications = DB::table('notifications')
                ->where("status", "Active")
                ->where("recurring", "On")
                ->where("broadcastDate", $broadcast_date)
                ->get();
        if (!empty($notifications)) {
            foreach ($notifications as $notification) {
                if($notification->type == "Customer"){
                    $type = '1';
                }else{
                    $type = '2';
                }
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "http://phytotherapy.in:8051/user/service/SendMultipleNotificationtousers");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "message=" . $notification->message . "&type=".$type."&couponId=" . $notification->coupon_id . "&serviceId=" . $notification->service_id . "&title=" . $notification->title . "&image=" . $notification->image);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $server_output = curl_exec($ch);
                curl_close($ch);
            }
        }
    }
}
