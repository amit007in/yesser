<?php

namespace App\Exports;

use App\Models\AdminContactus;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;

use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ContactExport implements FromCollection, WithHeadings
{
	use Exportable;
	protected $test;
	public function __construct (Request $request)
	{
		$this->test = $request->all();
	}

	public function collection()
	{
		$data = CommonController::set_starting_ending($this->test);
		$orders = AdminContactus::admin_contactus_all($data);
		$temp = array();

		foreach ($orders as $key => $value) {
			if($value->user_type_id == '1'){
				$created = "Customer";
			}elseif($value->user_type_id == '2'){
				$created = "Service";
			}else{
				$created = "Support";
			}
			$temp[] = array(
				'Id'		       =>	$key+1,
				'Created By'       =>   $created,
				'Message'          =>   $value->message,
				'Name'             =>   $value->user['name'],
				'Customer Phone'   =>   $value->user['phone_code'].' '.$value->user['phone_number']
			);
		}
		return collect($temp);
	}

	public function headings(): array
	{
		return [
			'Id',
			'Created By',
			'Message',
			'Name',
			'Customer Phone'
		];
	}
}