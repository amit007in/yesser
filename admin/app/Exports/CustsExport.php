<?php

namespace App\Exports;

use App\Models\UserDetail;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;

use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CustsExport implements FromCollection, WithHeadings
{

	use Exportable;

	protected $test;

	public function __construct (Request $request)
        {
            $this->test = $request->all();
        }

    public function collection()
        {	
        	// execute the middleware functions here //
        	$data = CommonController::set_starting_ending($this->test);
            $custs = UserDetail::admin_cust_listings($data);

        	$temp = array();

        	foreach ($custs as $key => $value) {
                
        		$temp[] = array(
        			'Id'		       =>	$key+1,
                    'Status'           =>   $value->blocked == '0' ? 'Active' : 'Blocked',
        			//'Email'		   =>	$value->email,
                    'Name'             =>   $value->name,
        			'Phone'		       =>	$value->phone_code.' '.$value->phone_number,
        			'Orders'           =>   $value->order_counts,
                    'Reviews'          =>	$value->review_counts,
                    'Registered At'    =>   $value->created_atz
        			);

        	}

        	return collect($temp);
        }

    public function headings(): array
        {
            return [
                'Id',
                'Status',
                //'Email',
                'Name',
                'Phone',
                'Orders',
                'Reviews',
                'Registered At'
            ];
        }

}