<?php

namespace App\Exports;

//use App\User;
use App\Models\UserDetail;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;

use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DriversExport implements FromCollection, WithHeadings
{

	use Exportable;

	protected $test;

	public function __construct (Request $request)
        {
            $this->test = $request->all();
        }

    public function collection()
        {
        	$data = CommonController::set_starting_ending($this->test);
        	$drivers = UserDetail::admin_drivers_listings($data,1);

        	$temp = array();

        	foreach ($drivers as $key => $value) {
                
                $ss = '';
                if(isset($value->category_brand->category_brand_details[0]->name)) {
                    $ss = $value->category_brand->category_brand_details[0]->name;
                }


            //////////      Products    ///////////////////////
            $products = [];
            if(isset($value->user_driver_detail_products))
                {
                    $len = COUNT($value->user_driver_detail_products);
                    for($i=0;$i<$len;$i++)
                        {
                            array_push($products, $value->user_driver_detail_products[$i]['name']);
                        }
                }
            $products = implode(', ', $products);
            //////////      Products    ///////////////////////


        		$temp[] = array(
        			'Id'		=>	$key+1,
        			//'EMAIL'		=>	$value->email,
                    'Name'      =>  $value->name,
        			'Phone'		=>	$value->phone_code.' '.$value->phone_number,
                    'Service'   =>  $value->category['name'],
                    'Brand'     =>  $ss,
        			'Reviews'	=>	$value->review_counts,
        			'Orders'	=>	$value->order_counts,
                    'Products'  =>  $products,
        			'Online'	=>	$value->online_status == 1 ? 'Online' : 'Offline',
        			'Status'	=>	$value->blocked == 0 ? 'Active' : 'Blocked',
        			'Approved'	=>	$value->approved == 1 ? 'Approved': 'Pending',
        			'Type'		=>	$value->organisation_id == 0 ? 'Independent' : 'Company',
                    'Registered At'  =>  $value->created_atz
        			);

        	}

        	return collect($temp);
        }

    public function headings(): array
        {
            return [
                'Id',
                //'EMAIL',
                'Name',
                'Phone',
                'Service',
                'Brand',
                'Reviews',
                'Orders',
                'Products',
                'Online',
                'Status',
                'Approved',
                'Type',
                'Registered At'
            ];
        }

}