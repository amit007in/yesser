<?php

namespace App\Exports;

use App\Models\Order;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;

use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrdersExport implements FromCollection, WithHeadings
{

	use Exportable;

	protected $test;

	public function __construct (Request $request)
        {
            $this->test = $request->all();
        }

    public function collection()
        {	
        	// execute the middleware functions here //
        	$data = CommonController::set_starting_ending($this->test);
            //$orders = Order::admin_orders_listings($data);
            $orders = Order::orders_listings_4export($data);
			
        	$temp = array();

        	foreach ($orders as $key => $value) {
                
                        if($value->order_status  == "Searching")
                            $order_status = 'Searching';
                        elseif($value->order_status  == "Confirmed")
                            $order_status = 'Confirmed';
                        elseif($value->order_status == "Scheduled" || $value->order_status == "DApproved")
                            $order_status = 'Scheduled';
                        elseif($value->order_status  == "Ongoing")
                            $order_status = 'Ongoing';
                        elseif($value->order_status == "SerReject" || $value->order_status == "SupReject")
                            $order_status = 'Rejected';
                        elseif($value->order_status == "SerTimeout" || $value->order_status == "SupTimeout" || $value->order_status == "DSchTimeout")
                            $order_status = 'Timeout';
                        elseif($value->order_status == "CustCancel")
                            $order_status = 'Customer Cancelled';
                        elseif($value->order_status == "DriverCancel" || $value->order_status == "DSchCancelled")
                            $order_status = 'Driver Cancelled';
                        elseif(($value->order_status == "SerComplete") || $value->order_status == "SupComplete")
                            $order_status = 'Completed';
                        elseif($value->order_status == "DPending")
                            $order_status = 'Confirmation Pending';
                        else                            
                            $order_status = $value->order_status;
						
					$totalPrice = array(); 
					for($i = 0;$i<count($value->order_products);$i++)
					{
						$totalPrice[] = $value->order_products[$i]['price_per_item'];
					}
					$totalProduct = array();
					for($i = 0;$i<count($value->order_products);$i++)
					{ 
						$totalProduct[] = $value->order_products[$i]['productName']; 
					} 
					$totalProduct = implode( ", ", $totalProduct );
        		$temp[] = array(
        			'Id'		       =>	$key+1,
                    'Status'           =>   $order_status,
                    'Type'             =>   $value->order_type == 'Service' ? 'Service' : 'Supprt',
                    'Customer'         =>   $value['customer_user']->name,
                    'Customer Phone'   =>   $value['customer_user']->phone_code.' '.$value['customer_user']->phone_number,
                    'Timing'           =>   $value->order_timingsz,
                    'Pickup Location'  =>   $value->pickup_address,
                    'DropOff Location' =>   $value->dropoff_address,
                    'Category'         =>   $value['category']->name,
                    'Brand'            =>   isset($value['category_brand']) ? $value['category_brand']->name : '',
                    //'Product'          =>   $value->payment['product_quantity'].' * '.$value->category_brand_product['name'],
                    'Product'          =>   $value->payment['product_quantity'].' * '.$totalProduct,
                    'Payment'          =>   number_format(array_sum($totalPrice),2),
                    //'Payment'          =>   $value->payment['final_charge'],
                    'Driver'    =>  isset($value['driver_user_detail']) ? $value['driver_user_detail']->name : '',
                    'Driver Phone'    =>  isset($value['driver_user_detail']) ? $value['driver_user_detail']->phone_code.' '.$value['driver_user_detail']->phone_number: '',
                    'Booked At'        =>   $value->created_atz
        			);
        	}
        	return collect($temp);
        }

    public function headings(): array
        {
            return [
                'Id',
                'Status',
                'Type',
                'Customer',
                'Customer Phone',
                'Timing',
                'Pickup Location',
                'DropOff Location',
                'Category',
                'Brand',
                'Product',
                'Payment',
                'Driver',
                'Driver Phone',
                'Booked At'
            ];
        }

}