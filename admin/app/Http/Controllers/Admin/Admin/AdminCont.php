<?php

namespace App\Http\Controllers\Admin\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Mail;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Illuminate\Cookie\CookieJar;
use Request as ORequest;

use App\Http\Controllers\CommonController;

use App\Models\Admin;
use App\Models\AdminUserDetailLogin;
use App\Models\Order;

class AdminCont extends Controller
{

public static function admin_test()
	{
		return View::make('Admin.Test.test');
	}

//////////////		Admin Login Get 	////////////////////////////////////////////////////////////
public static function admin_login_get()
	{
		return View::make('Admin.Other.login');
	}

//////////////		Admin Login Post 	////////////////////////////////////////////////////////////
public static function admin_login_post(Request $request)
	{
	try{

			$rules = array(
				'email'=>'required|email|exists:admins,email',
				'password'=>'required',
				'timezone'=>'required|timezone');

			$validator = Validator::make($request->all(),$rules);
				if($validator->fails())
					return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();

			$admin = Admin::where('email', $request['email'])->first();
			if(!\Hash::Check($request['password'], $admin->password))
				return Redirect::back()->witherrors('Password is Incorrect')->withInput()->with('fresh','0');

		 	$time = new \DateTime('now', new \DateTimeZone($request['timezone']));
			$request['timezonez'] = $time->format('P');

			$request['user_type_id'] = 1;
			$request['admin_id'] = $admin->admin_id;
			$request['ip_address'] = $request->ip();//$_SERVER['HTTP_X_FORWARDED_FOR']; // $request->ip();

			// AdminUserDetailLogin::where("admin_id",$request['admin_id'])->where('user_type_id',$request['user_type_id'])->where('ip_address',$request['ip_address'])->where("access_token","!=","")->update([
			// 		'access_token' => "",
			// 		'otp' => '',
			// 		'updated_at' => new \DateTime
			// ]);

			$admin_user_login_detail = AdminUserDetailLogin::insertNewRow($request->all());

			\Cookie::queue('adminAccessToken', $admin_user_login_detail->access_token , 60);
			\Cookie::queue('admintz', $request['timezonez'] , 60);

		 	Admin::super_admin_update($request,$admin);

			return Redirect::route('admin_dashboard')->with('successMsg','Logged In Successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

///////////////////////////				Logout 			///////////////////////////////////////////////////////////
public static function admin_logout()
	{
	try{
			$admin = \App('admin');

			AdminUserDetailLogin::where('access_token',$admin->access_token)->update([
				'access_token' => '',
				'otp' => '',
				'updated_at' => new \DateTime
			]);

			\Cookie::queue('adminAccessToken', 0 , 0);
			\Cookie::queue('admintz', 0 , 0);

			return Redirect::route('admin_login_get')->with('status', 'Logged Out Successfully');
		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

///////////////////////////////////			Admin Dash Board 			////////////////////////////////////////
public static function admin_dashboard(Request $request)
	{
		$admin = \App('admin');

		$starting_dt = Carbon::now($admin->timezonez)->subMonths(2)->format('d/m/Y');
		$ending_dt = Carbon::now($admin->timezonez)->addday(1)->format('d/m/Y');
		
		$orders = Order::admin_latest_orders($request->all());
		return View::make('Admin.dashboard', compact('orders'))->with('starting_dt',$starting_dt)->with('ending_dt',$ending_dt);
	}

///////////////////////////////////			Dashboard Data 		////////////////////////////////////////////////
public static function admin_dash_data(Request $request)
	{
	try{
			$admin = \App('admin');

			$request['daterange'] = urldecode($request['daterange']);
			$temp_array = explode(' - ', $request['daterange']);

			$request['starting_dt'] = Carbon::CreateFromFormat('d/m/Y',$temp_array[0],$request['timezonez'])->timezone('UTC')->format('Y-m-d');
			$request['ending_dt'] = Carbon::CreateFromFormat('d/m/Y',$temp_array[1],$request['timezonez'])->timezone('UTC')->format('Y-m-d');

			$request['starting_dt'] = $request['starting_dt'].' 00:00:00';
			$request['ending_dt'] = $request['ending_dt'].' 23:59:59';

			if(isset($request['latitude']) && isset($request['longitude']) && $request['latitude'] != "null" && $request['longitude'] != "null")
				{
					AdminUserDetailLogin::where('admin_user_detail_login_id', $admin->admin_user_detail_login_id)->limit(1)->update([
							'latitude' => $request['latitude'],
							'longitude' => $request['longitude'],
							'updated_at' => new \DateTime
					]);
				}

			$stats_data = AdminCont::stats_data($request);
			$graph_data = AdminCont::new_graph_data($request);

			return Response(array('success'=>'1', 'stats'=>$stats_data[0], 'graph_data'=>$graph_data),200);

		}
	catch(\Exception $e)
		{
			return Response(array('success'=>'0','msg'=>$e->getMessage()),200);
		}
	}


//////////////		Admin DashBoard Data		///////////////////////////////////////////////
	public static function stats_data($data)
		{

	return DB::SELECT("SELECT
		*,
		(SELECT COUNT(*) FROM orders as o WHERE o.order_timings BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' ) as order_counts,
		(SELECT COUNT(*) FROM payments as P WHERE P.payment_status='Completed' AND P.updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' ) as revenue_counts,
		(SELECT COUNT(*) FROM category_brand_products as CBP WHERE CBP.updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' ) as product_counts,

		(SELECT COUNT(*) FROM orders as O WHERE O.order_status IN ('SerCancel',  'SerReject', 'SerTimeout', 'DriverCancel', 'CustCancel', 'DSchCancelled', 'SysSchCancelled', 'DSchTimeout') AND O.order_timings BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' ) as cancelled_counts,
		(SELECT COUNT(*) FROM orders as O WHERE O.order_status IN ('SerCancel', 'CustCancel') AND O.order_timings BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' ) as customer_cancelled_counts,
		(SELECT COUNT(*) FROM orders as O WHERE O.order_status IN ('DriverCancel', 'DSchCancelled', 'DSchTimeout') AND O.order_timings BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' ) as provider_cancelled_counts,
		(SELECT COUNT(*) FROM orders as O WHERE O.order_status IN ('Scheduled', 'DPending', 'DApproved') AND O.order_timings BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' ) as scheduled_counts,

		(SELECT COUNT(*) FROM user_details as ud WHERE ud.user_type_id=1 AND ud.created_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' ) as customer_counts,
		(SELECT COUNT(*) FROM user_details as ud WHERE ud.user_type_id=2 AND ud.created_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' ) as dservice_counts,
		(SELECT COUNT(*) FROM user_details as ud WHERE ud.user_type_id=3 AND ud.created_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' ) as dsupport_counts,
		(SELECT COUNT(*) FROM organisations as o WHERE o.created_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."') as org_counts,
		(SELECT COUNT(*) FROM order_ratings WHERE created_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."') as review_counts
	FROM admins as a
	LIMIT 0, 1
	");

		}

/////////////////////		Graph Data 		/////////////////////////////////////
public static function new_graph_data($data)
	{

	    if(!isset($data['yr_only']))
	        $yr_only = Carbon::now($data['timezonez'])->format('Y');
	    else
	        $yr_only  = $data['yr_only'];

	$querys = DB::SELECT("SELECT
					months_tbl.id as id,
	CAST((SELECT COUNT(*) FROM user_details as ud WHERE ud.user_type_id=1 AND YEAR(ud.created_at) = :yr1 AND MONTH(ud.created_at) = months_tbl.id) AS UNSIGNED) as customers,
	CAST((SELECT COUNT(*) FROM user_details as ud WHERE ud.user_type_id=2 AND YEAR(ud.created_at) = :yr2 AND MONTH(ud.created_at) = months_tbl.id) AS UNSIGNED) as dservices,
	CAST((SELECT COUNT(*) FROM user_details as ud WHERE ud.user_type_id=3 AND YEAR(ud.created_at) = :yr3 AND MONTH(ud.created_at) = months_tbl.id) AS UNSIGNED) as dsupports,
	CAST((SELECT COUNT(*) FROM orders as o WHERE YEAR(o.order_timings) = :yr4 AND MONTH(o.order_timings) = months_tbl.id) AS UNSIGNED) as orders,
	CAST((SELECT COUNT(*) FROM organisations as o WHERE YEAR(o.created_at) = :yr5 AND MONTH(o.created_at) = months_tbl.id) AS UNSIGNED) as orgs,
	CAST((SELECT COUNT(*) FROM order_ratings WHERE YEAR(created_at) = :yr6 AND MONTH(created_at) = months_tbl.id) AS UNSIGNED) as reviews
	        			FROM months_tbl
	        ",['yr1'=>$yr_only, 'yr2'=>$yr_only, 'yr3'=>$yr_only, 'yr4'=>$yr_only, 'yr5'=>$yr_only, 'yr6'=>$yr_only]);

                $customers = $dservices = $dsupports = $orders = $orgs = $reviews = [];

	    		foreach($querys as $query)
		    		{
		    			array_push($customers, $query->customers);
		    			array_push($dservices, $query->dservices);
		    			array_push($dsupports, $query->dsupports);
		    			array_push($orders, $query->orders);
		    			array_push($orgs, $query->orgs);
						array_push($reviews, $query->reviews);
		    		}

    		return array('customers'=> $customers, 'dservices' => $dservices, 'dsupports' => $dsupports, 'orders' => $orders, 'orgs' => $orgs, 'reviews' => $reviews, 'dt'=> $yr_only);

		}

//////////////////		Admin Profile Update Get ////////////////////////////////////////////////////////////
public static function aprofile_update_get()
	{
	try{
			return View::make('Admin.Profile.profileUpdate');
		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

//////////////		Admin Profile Update Post ////////////////////////////////////////////////////////////
public static function aprofile_update_post(Request $request)
	{
	try{
			$data = Input::all();
			$rules = array(
				'name'=>'required',
				'phone_number' => 'required',
				'job'=>'required',
				'profile_pic'=>'image',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$admin = \App('admin');

			if(!empty($request['profile_pic']))
				{
					$request['profile_pic_name'] = CommonController::image_uploader(Input::file('profile_pic'));
					if(empty($request['profile_pic_name']))
						return Redirect::back()->withErrors('Sorry, profile pic not uploaded. Please try again');
				}
			else
				$request['profile_pic_name'] = $admin->admin['profile_pic'];

			Admin::where('admin_id',$admin->admin_id)->update(array(
				'name' => $request['name'],
				'phone_number' => $request['phone_number'],
				'job' => $request['job'],
				'profile_pic' => $request['profile_pic_name'],
				'updated_at' => new \DateTime
			));

			return Redirect::back()->with('status','Profile Updated Successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

/////////////		Admin Password Update Get  //////////////////////////////////////////////////////
public static function apassword_update_get()
	{
	try{
			return View::make('Admin.Profile.passwordUpdate');
		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

//////////////		Admin Password Update post  //////////////////////////////////////////////////////
public static function apassword_update_post(Request $request)
	{
	try{
			$rules = array('old_password'=>'required','password'=>'required|confirmed');

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$admin = \App('admin');

			if(!\Hash::check($request['old_password'],$admin->admin['password']))
				return Redirect::back()->withErrors('Please Enter the Correct Old Password');
			if(\Hash::check($request['password'],$admin->admin['password']))
				return Redirect::back()->withErrors('Please Enter different password than the current one');

			Admin::where('admin_id',$admin->admin_id)->update(array(
				'password'=>\Hash::make($request['password']),
				'updated_at'=>new \DateTime
			));

			return Redirect::Back()->with('status','Password Updated Successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

////////////////////////////		Admin Delete 		/////////////////////////////////////
public static function admin_delete(Request $request)
	{
	try{

			$rules = array(
				'organisation_id' => 'sometimes|exists:organisations,organisation_id',
				'customer_user_detail_id' => 'sometimes|exists:user_details,user_detail_id,user_type_id,1',
				'customer_user_id'  => 'sometimes|exists:users,user_id',
				'driver_user_detail_id' => 'sometimes|exists:user_details,user_detail_id,user_type_id,2',
				'driver_user_id' => 'sometimes|exists:users,user_id'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			if($request['organisation_id'])
				{

					$user_ids = Db::table('users')->where('organisation_id', $request['organisation_id'])->pluck('user_id');

					DB::table('payment_ledgers')->where('organisation_id', $request['organisation_id'])->orwhereIn('user_id',$user_ids)->delete();

					DB::table('organisation_coupon_users')->where('seller_organisation_id',$request['organisation_id'])->orwhere('customer_organisation_id',$request['organisation_id'])->orWhereIn('seller_user_id', $user_ids)->orWhereIn('customer_user_id', $user_ids)->delete();
					DB::table('organisation_coupons')->where('organisation_id', $request['organisation_id'])->delete();
					DB::table('organisation_category_brand_products')->where('organisation_id', $request['organisation_id'])->delete();
					DB::table('organisation_categories')->where('organisation_id',$request['organisation_id'])->delete();
					DB::table('organisation_areas')->where('organisation_id', $request['organisation_id'])->delete();

					DB::table('user_detail_notifications')->whereIn('user_id',$user_ids)->orwhereIn('sender_user_id',$user_ids)->delete();
					DB::table('user_driver_detail_services')->whereIn('user_id',$user_ids)->delete();
					DB::table('user_driver_detail_products')->whereIn('user_id',$user_ids)->delete();
					DB::table('user_cards')->whereIn('user_id',$user_ids)->delete();
					DB::table('user_bank_details')->whereIn('user_id',$user_ids)->delete();

					DB::table('payments')->whereIn('customer_user_id', $user_ids)->orWhereIn('seller_user_id', $user_ids)->delete();

					DB::table('order_requests')->where('driver_organisation_id', $request['organisation_id'])->orWhereIn('driver_user_id', $user_ids)->delete();
					DB::table('order_ratings')->where('customer_organisation_id', $request['organisation_id'])->orWhere('driver_organisation_id', $request['organisation_id'])->orWhereIn('customer_user_id',$user_ids)->orWhereIn('driver_user_detail_id', $user_ids)->delete();

					DB::table('order_images')->whereRaw('(order_images.order_id IN (SELECT order_id FROM orders WHERE orders.driver_organisation_id='.$request['organisation_id'].' OR orders.customer_organisation_id='.$request['organisation_id'].') )')->delete();

					DB::table('coupon_users')->whereIn('user_id',$user_ids)->delete();

					DB::Table('admin_user_detail_logins')->where('organisation_id', $request['organisation_id'])->orWhereIn('user_id',$user_ids)->delete();
					
					DB::table('orders')->where('customer_organisation_id', $request['organisation_id'])->orWhere('driver_organisation_id', $request['organisation_id'])->orWhereIn('customer_user_id',$user_ids)->orWhereIn('driver_user_id', $user_ids)->delete();

					DB::table('users')->whereIn('user_id',$user_ids)->delete();
					DB::table('organisations')->where('organisation_id', $request['organisation_id'])->delete();

					$msg = 'Organisation deleted successfully';

				}
			elseif(isset($request['customer_user_detail_id']) && isset($request['customer_user_id']) )
				{

					$user_detail = Db::table('user_details')->where('user_detail_id',$request['customer_user_detail_id'])->first();

				DB::table('users')->where('user_id',$user_detail->user_id)->update([
					'phone_number' => 11111111,
					'name' => 'Deleted',
					'updated_at' => new \DateTime
				]);

				DB::table('user_details')->where('user_id',$user_detail->user_id)->update([
					'online_status' => '0',
					'access_token' => '',
					'mulkiya_number' => '',
					'approved' => '1',
					'fcm_id' => '',
					'socket_id' => '',
					'otp' => '',
					'updated_at' => new \DateTime,
				]);

					// $order_ids = DB::table('orders')->where('customer_user_detail_id', $request['customer_user_detail_id'])->orWhere('driver_user_detail_id', $request['customer_user_detail_id'])->pluck('order_id');

					// DB::Table('admin_user_detail_logins')->where('user_detail_id',$request['customer_user_detail_id'])->delete();

					// DB::table('coupon_users')->where('user_detail_id',$request['customer_user_detail_id'])->delete();

					// DB::table('order_requests')->whereIn('order_id', $order_ids)->delete();
					// DB::table('order_ratings')->whereIn('order_id', $order_ids)->delete();
					// DB::table('order_images')->whereIn('order_id', $order_ids)->delete();

					// DB::table('organisation_coupon_users')->where('customer_user_detail_id',$request['customer_user_detail_id'])->delete();

					// DB::table('payments')->where('customer_user_detail_id', $request['customer_user_detail_id'])->orWhere('seller_user_detail_id', $request['customer_user_detail_id'])->delete();

					// DB::table('user_detail_notifications')->where('user_detail_id',$request['customer_user_detail_id'])->orwhere('sender_user_detail_id',$request['customer_user_detail_id'])->delete();

					// DB::table('user_cards')->where('user_id',$request['customer_user_id'])->delete();

					// DB::table('orders')->whereIn('order_id',$order_ids)->delete();

					// DB::table('user_details')->where('user_detail_id',$request['customer_user_detail_id'])->delete();

					$msg = 'Customer deleted successfully';
				}
			elseif(isset($request['driver_user_detail_id']) && isset($request['driver_user_id']) )
				{

					$user_detail = Db::table('user_details')->where('user_detail_id',$request['driver_user_detail_id'])->first();

				DB::table('users')->where('user_id',$user_detail->user_id)->update([
					'phone_number' => 11111111,
					'name' => 'Deleted',
					'updated_at' => new \DateTime
				]);

				DB::table('user_details')->where('user_id',$user_detail->user_id)->update([
					'online_status' => '0',
					'access_token' => '',
					'mulkiya_number' => '',
					'approved' => '0',
					'fcm_id' => '',
					'socket_id' => '',
					'blocked' => '1',
					'otp' => '',
					'updated_at' => new \DateTime,
				]);

					// DB::table('user_driver_detail_services')->where('user_detail_id',$request['driver_user_detail_id'])->delete();
					// DB::table('user_driver_detail_products')->where('user_detail_id',$request['driver_user_detail_id'])->delete();

					// DB::table('user_detail_notifications')->where('user_detail_id',$request['driver_user_detail_id'])->orwhere('sender_user_detail_id',$request['driver_user_detail_id'])->delete();

					// DB::table('user_bank_details')->where('user_id',$request['driver_user_id'])->delete();

					// DB::table('payment_ledgers')->where('user_detail_id', $request['driver_user_detail_id'])->delete();


					// DB::table('organisation_coupon_users')->where('seller_user_detail_id',$request['driver_user_detail_id'])->orwhere('customer_user_detail_id',$request['driver_user_detail_id'])->delete();

					// DB::table('payments')->where('customer_user_detail_id', $request['customer_user_detail_id'])->orWhere('seller_user_detail_id', $request['driver_user_detail_id'])->delete();

					// DB::table('organisation_coupons')->where('user_detail_id', $request['driver_user_detail_id'])->delete();

					// $order_ids = DB::table('orders')->where('driver_user_detail_id', $request['driver_user_detail_id'])->orwhere('customer_user_detail_id', $request['driver_user_detail_id'])->pluck('order_id');

					// DB::table('order_requests')->whereIn('order_id', $order_ids)->delete();
					// DB::table('order_ratings')->whereIn('order_id', $order_ids)->delete();
					// DB::table('order_images')->whereIn('order_id', $order_ids)->delete();

					// DB::table('coupon_users')->where('user_detail_id', $request['driver_user_detail_id'])->delete();

					// DB::Table('admin_user_detail_logins')->where('user_detail_id',$request['driver_user_detail_id'])->delete();

					// DB::table('orders')->whereIn('order_id',$order_ids)->delete();
					// DB::table('user_details')->where('user_detail_id',$request['driver_user_detail_id'])->delete();

					$msg = 'Service driver deleted successfully';
				}
			else
				{

					$msg = 'Deleted successfully';
				}

			return Redirect::Back()->with('status',$msg);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

//////////////////////////
/////////////		Admin Round Robin Update Get  //////////////////////////////////////////////////////
public static function aroundrobin_update_get()
{
	try{
			return View::make('Admin.Profile.roundRobinUpdate');
		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
}

//////////////		Admin Round Robin Update post  //////////////////////////////////////////////////////
public static function aroundrobin_update_post(Request $request)
{
	try{
		$rules = array('round_robin'=>'required');

		$validator = Validator::make($request->all(),$rules);
		if($validator->fails())
			return Redirect::back()->withErrors($validator->getMessageBag()->first());

		$admin = \App('admin');
		Admin::where('admin_id',$admin->admin_id)->update(array(
			'round_robin_time'=>$request['round_robin']
		));
		return Redirect::Back()->with('status','Round Robin Updated Successfully');
	}
	catch(\Exception $e)
	{
		return Redirect::back()->witherrors($e->getMessage())->withInput();
	}
}


}
