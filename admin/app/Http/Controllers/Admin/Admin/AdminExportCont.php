<?php

namespace App\Http\Controllers\Admin\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\CommonController;

use App\Models\Order;
use App\Models\Category;
use App\Models\UserDetail;

use App\Exports\CustsExport;
use App\Exports\DriversExport;
use App\Exports\OrdersExport;
use App\Exports\ContactExport;
class AdminExportCont extends Controller
{
////////////////////////		Export Drivers	    /////////////////////////////////////////////
public static function admin_drivers_preport(Request $request)
	{
	try{

			$request['user_type_ids'] = [2];

			$file_name = 'yesserman_export_'.time().'.xlsx';

			Excel::store(new DriversExport($request), $file_name);

			//$link = env('APP_URL').'/BuraqExpress/Uploads/'.$file_name; env('IMAGE_URL').'/'.
			$link = env('IMAGE_URL').'/'.$file_name;

			return Response(['success'=> 1, 'msg' => 'Exported successfully', 'link'=>$link ], 200);

		}
	catch(\Exception $e)
		{
			return Response(['success'=> 0, 'msg' => $e->getMessage()], 500);
			//return Redirect::back()->withErrors($e->getMessage());
		}
	}

////////////////////////		Export Customers 		/////////////////////////////////////////
public static function admin_custs_preport(Request $request)
	{
	try{

			$request['not_paginate'] = 1;
			$file_name = 'customers_export_'.time().'.xlsx';

			Excel::store(new CustsExport($request), $file_name);

			//$link = env('APP_URL').'/BuraqExpress/Uploads/'.$file_name;
			$link = env('IMAGE_URL').'/'.$file_name;

			return Response(['success'=> 1, 'msg' => 'Exported successfully', 'link'=>$link ], 200);

		}
	catch(\Exception $e)
		{
			return Response(['success'=> 0, 'msg' => $e->getMessage()], 500);
		}
	}

///////////////////////			Admin Orders Exports 		///////////////////////////
public static function admin_orders_preport(Request $request)
	{
	try{

			if(!isset($request['status_check']))
				$request['status_check'] = 'All';

			if(!isset($request['categories']))
				$request['categories'] = [0];

			if(!isset($request['ordering_filter']))
				$request['ordering_filter'] = 0;

			$file_name = 'orders_export_'.time().'.xlsx';

			Excel::store(new OrdersExport($request), $file_name);

			//$link = env('APP_URL').'/BuraqExpress/Uploads/'.$file_name;
			$link = env('IMAGE_URL').'/'.$file_name;
			return Response(['success'=> 1, 'msg' => 'Exported successfully', 'link'=>$link ], 200);

		}
	catch(\Exception $e)
		{
			return Response(['success'=> 0, 'msg' => $e->getMessage()], 500);
		}
	}

	///////////////////
	///////////////////////			Admin Contacts Exports 		///////////////////////////
	public static function admin_contacts_preport(Request $request)
	{
		try{
			$file_name = 'contacts_export_'.time().'.xlsx';

			Excel::store(new ContactExport($request), $file_name);

			$link = env('IMAGE_URL').'/'.$file_name;

			return Response(['success'=> 1, 'msg' => 'Exported successfully', 'link'=>$link ], 200);

		}
		catch(\Exception $e)
		{
			return Response(['success'=> 0, 'msg' => $e->getMessage()], 500);
		}
	}
}
