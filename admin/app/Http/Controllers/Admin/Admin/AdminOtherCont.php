<?php

namespace App\Http\Controllers\Admin\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use App\Models\AdminUserDetailLogin;
use App\Models\AdminContactus;
use App\Models\Coupon;
use App\Models\EmergencyContact;
use App\Models\EmergencyContactDetail;
use App\Models\Language;
use App\Models\OrderRating;
use App\Models\PaymentLedger;
use App\Models\Notification;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\CategoryBrand;
use App\Models\CategoryBrandProduct;
use App\Models\Category;
use App\Models\Promocode;
use App\Http\Controllers\CommonController;
use App\Models\PromocodeProduct;
use App\Models\PromoUser;
use App\Models\PromoUsersRide;
use App\Models\FloorMargin;
use App\Models\RewardPoint;
class AdminOtherCont extends Controller {

//////////////////////		All Languages    //////////////////////////
    public static function admin_language_all(Request $request) {
        try {
            $languages = Language::admin_get_all_languages($request->all());

            return View::make('Admin.Languages.allLanguages', compact('languages'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

////////////////////		User Block Unblock 	////////////////////////////////////////////////////

    public static function default_lang_change(Request $request) {
        try {

            $rules = array(
                'language_id' => 'required|exists:languages,language_id'
            );
            $msg = array('language_id.exists' => 'This language is currently not available');

            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

            $language = Language::where('language_id', $request['language_id'])->first();
            if ($language->blocked == '0')
                return Redirect::back()->withErrors('This language is already default language')->withInput();

            Language::where('language_id', '!=', $language->language_id)->where('is_default', '1')->update(array(
                'is_default' => '0',
                'updated_at' => new \DateTime
            ));

            Language::where('language_id', $language->language_id)->update(array(
                'is_default' => '1',
                'updated_at' => new \DateTime
            ));

            return Redirect::back()->with('status', 'Default language update successfully');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

//////////////////////////		Emergency Contacts All 		//////////////////////////////////////////
    public static function admin_econtacts_all(Request $request) {
        try {
            $econtacts = EmergencyContact::admin_get_lists($request->all());

            return View::make('Admin.EContacts.allEContacts', compact('econtacts'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

//////////////////////////		Block Unblock EContacts 	//////////////////////////////////////////
    public static function admin_econtact_block(Request $request) {
        try {

            $rules = array(
                'emergency_contact_id' => 'required|exists:emergency_contacts,emergency_contact_id',
                'block' => 'required|in:0,1'
            );
            $msg = array('emergency_contact_id.exists' => 'Sorry, this emergency contact is currently not available');

            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

            $econtact = EmergencyContact::where('emergency_contact_id', $request['emergency_contact_id'])->first();

            if ($request['block'] == 1) {

                if ($econtact->blocked == '1')
                    return Redirect::back()->withErrors('This emergency contact is already blocked')->withInput();

                EmergencyContact::where('emergency_contact_id', $econtact->emergency_contact_id)->update(array(
                    'blocked' => '1',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Emergency contact blocked successfully');
            }
            else {

                if ($econtact->blocked == '0')
                    return Redirect::back()->withErrors('This emergency contact is already unblocked')->withInput();

                EmergencyContact::where('emergency_contact_id', $econtact->emergency_contact_id)->update(array(
                    'blocked' => '0',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Emergency contact unblocked successfully');
            }
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

///////////////////////////		Emergency COntact Add Get 		///////////////////////////////////////
    public static function admin_econtact_aget(Request $request) {
        try {
            $ranking = EmergencyContact::count();

            return View::make('Admin.EContacts.addEContacts', compact('ranking'));
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

///////////////////////////		Emergency COntact Add Post 		///////////////////////////////////////
    public static function admin_econtact_apost(Request $request) {
        try {

            $data = Input::all();
            $rules = array(
                'phone_number' => 'required',
                'english_name' => 'required',
                /*'hindi_name' => 'required',
                'urdu_name' => 'required',
                'chinese_name' => 'required',*/
                'arabic_name' => 'required',
                'ranking' => 'required',
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());


            $econtact = EmergencyContact::add_new_record($request->all());
            $request['emergency_contact_id'] = $econtact->emergency_contact_id;

            EmergencyContactDetail::insert([
                [
                    'emergency_contact_id' => $request['emergency_contact_id'],
                    'language_id' => 1,
                    'name' => $request['english_name'],
                    'image' => ''
                ],
                [
                    'emergency_contact_id' => $request['emergency_contact_id'],
                    'language_id' => 2,
                    'name' => $request['english_name'],
                    'image' => ''
                ],
                [
                    'emergency_contact_id' => $request['emergency_contact_id'],
                    'language_id' => 3,
                    'name' => $request['english_name'],
                    'image' => ''
                ],
                [
                    'emergency_contact_id' => $request['emergency_contact_id'],
                    'language_id' => 4,
                    'name' => $request['english_name'],
                    'image' => ''
                ],
                [
                    'emergency_contact_id' => $request['emergency_contact_id'],
                    'language_id' => 5,
                    'name' => $request['arabic_name'],
                    'image' => ''
                ]
            ]);

            EmergencyContact::where('emergency_contact_id', '!=', $request['emergency_contact_id'])->where('sort_order', $request['ranking'])->limit(1)->update([
                'sort_order' => $request['total'] + 1,
                'updated_at' => new \DateTime
            ]);

            return Redirect::back()->with('status', 'Emergency contact added successfully');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

////////////////////////////		Update Emergy Contact 	///////////////////////////////
    public static function admin_econtact_uget(Request $request) {
        try {

            $data = Input::all();
            $rules = array(
                'emergency_contact_id' => 'required|exists:emergency_contacts,emergency_contact_id',
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first());

            $econtact = EmergencyContact::admin_single_details($request->all());
            $ranking = EmergencyContact::count();

            return View::make('Admin.EContacts.updateEContacts', compact('ranking', 'econtact'));
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

////////////////////////////		Update Emergy Contact Post 	///////////////////////////////
    public static function admin_econtact_upost(Request $request) {
        try {

            $rules = array(
                'emergency_contact_id' => 'required|exists:emergency_contacts,emergency_contact_id',
                'phone_number' => 'required',
                'english_name' => 'required',
                /*'hindi_name' => 'required',
                'urdu_name' => 'required',
                'chinese_name' => 'required',*/
                'arabic_name' => 'required',
                'ranking' => 'required',
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first());

            $econtact = EmergencyContact::admin_single_details($request->all());

            $request['updated_at'] = Carbon::now('UTC')->format('Y-m-d H:i:s');
            $data = $request->all();

///////////////////////////		Update Details 		/////////////////////////////////////////

            DB::transaction(function() use ($econtact, $data) {

                $temp1 = $econtact->emergency_contact_details[0]->emergency_contact_detail_id;
                $temp2 = $econtact->emergency_contact_details[1]->emergency_contact_detail_id;
                $temp3 = $econtact->emergency_contact_details[2]->emergency_contact_detail_id;
                $temp4 = $econtact->emergency_contact_details[3]->emergency_contact_detail_id;
                $temp5 = $econtact->emergency_contact_details[4]->emergency_contact_detail_id;

                DB::unprepared(DB::raw("UPDATE emergency_contacts SET sort_order=" . $data['ranking'] . ", phone_number='" . $data['phone_number'] . "', updated_at='" . $data['updated_at'] . "' WHERE emergency_contact_id=" . $data['emergency_contact_id'] . " "));

                DB::unprepared(DB::raw("UPDATE emergency_contact_details SET name='" . $data['english_name'] . "', updated_at='" . $data['updated_at'] . "' WHERE emergency_contact_detail_id=$temp1"));
                DB::unprepared(DB::raw("UPDATE emergency_contact_details SET name='" . $data['english_name'] . "', updated_at='" . $data['updated_at'] . "' WHERE emergency_contact_detail_id=$temp2"));
                DB::unprepared(DB::raw("UPDATE emergency_contact_details SET name='" . $data['english_name'] . "', updated_at='" . $data['updated_at'] . "' WHERE emergency_contact_detail_id=$temp3"));
                DB::unprepared(DB::raw("UPDATE emergency_contact_details SET name='" . $data['english_name'] . "', updated_at='" . $data['updated_at'] . "' WHERE emergency_contact_detail_id=$temp4"));
                DB::unprepared(DB::raw("UPDATE emergency_contact_details SET name='" . $data['arabic_name'] . "', updated_at='" . $data['updated_at'] . "' WHERE emergency_contact_detail_id=$temp5"));
            });

            DB::commit();

            if ($econtact->sort_order != $request['ranking']) {

                EmergencyContact::where('emergency_contact_id', '!=', $request['emergency_contact_id'])->where('sort_order', $request['ranking'])->limit(1)->update([
                    'sort_order' => $econtact->sort_order,
                    'updated_at' => $data['updated_at']
                ]);
            }

            return Redirect::back()->with('status', 'Emergency contact updated successfully');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

//////////////////////////		Coupons All 	/////////////////////////////////////////
    public static function admin_coupons_all(Request $request) {
        try {

            $data = CommonController::set_starting_ending($request->all());

            $coupons = Coupon::admin_coupons($data);

            return View::make('Admin.Coupons.allCoupons', compact('coupons'))->with('fstarting_dt', $data['fstarting_dt'])->with('fending_dt', $data['fending_dt']);
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

///////////////////////////		Coupon Add Get 		//////////////////////////////////////
    public static function admin_coupons_aget(Request $request) {
        $request['category_id'] = "1";
        $brands = CategoryBrand::admin_brands_list_add_drivers($request->all());
        $category_type = "Service";
        $language_id = "1";
        $category = Category::service_support_listings($category_type, $language_id);
        $users = array();
        return View::make('Admin.Coupons.addCoupons', compact('brands', 'category', 'users'));
    }

//////////////////		Add Coupon Post 		//////////////////////////////////////////
    public static function admin_coupons_apost(Request $request) {
        try {

            $data = Input::all();
            $rules = array(
                'code' => 'required|unique:coupons,code',
                'rides_value' => 'required|integer|min:1',
                'coupon_type' => 'required|in:Percentage,Value',
                'amount_value' => 'required|min:0',
                'price' => 'required|min:0',
                'expires_at' => 'required',
                'expiry_days' => 'required|min:1'
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

            if ($request['credit_start_dt'] != NULL) {
                $request['credit_start_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['credit_start_dt'], $request['timezone'])->timezone('UTC')->format('Y-m-d H:i:s');
            }

            $coupon = Coupon::add_new_record($request->all());

            return Redirect::back()->with('status', 'Coupon added successfully');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

///////////////////////////		Coupon Update Get 	////////////////////////////////////////////
    public static function admin_coupons_uget(Request $request) {
        try {

            $data = Input::all();
            $rules = array(
                'coupon_id' => 'required|exists:coupons,coupon_id'
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

            $coupon = Coupon::single_coupon_details($request->all());

            return View::make('Admin.Coupons.updateCoupons', compact('coupon'));
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

//////////////////		Add Coupon Post 		//////////////////////////////////////////
    public static function admin_coupons_upost(Request $request) {
        try {

            $data = Input::all();
            $rules = array(
                'coupon_id' => 'required|exists:coupons,coupon_id',
                'code' => 'required|unique:coupons,code,' . $request['coupon_id'] . ',coupon_id',
                'rides_value' => 'required|integer|min:1',
                'coupon_type' => 'required|in:Percentage,Value',
                'amount_value' => 'required|min:0',
                'price' => 'required|min:0',
                'expires_at' => 'required',
                'expiry_days' => 'required|min:1'
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

            if ($request['credit_start_dt'] != NULL) {
                $request['credit_start_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['credit_start_dt'], $request['timezone'])->timezone('UTC')->format('Y-m-d H:i:s');
            }

            $coupon = Coupon::update_record($request->all());

            return Redirect::back()->with('status', 'Coupon updated successfully');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

/////////////////////////////			Organisation Block Unblock 	///////////////////////////////
    public static function admin_coupon_block(Request $request) {
        try {

            $rules = array(
                'coupon_id' => 'required|exists:coupons,coupon_id',
                'block' => 'required|in:0,1'
            );
            $msg = array('coupon_id.exists' => 'Sorry, this coupon is currently not available');

            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

            $coupon = Coupon::where('coupon_id', $request['coupon_id'])->first();

            if ($request['block'] == 1) {

                if ($coupon->blocked == '1')
                    return Redirect::back()->withErrors('This coupon is already blocked')->withInput();

                Coupon::where('coupon_id', $coupon->coupon_id)->update(array(
                    'blocked' => '1',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Coupon blocked successfully');
            }
            else {

                if ($coupon->blocked == '0')
                    return Redirect::back()->withErrors('This coupon is already unblocked')->withInput();

                Coupon::where('coupon_id', $coupon->coupon_id)->update(array(
                    'blocked' => '0',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Coupon unblocked successfully');
            }
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////////		PromoCodes 		///////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * @role List All Promocode
     * @Method Get
     * @author Raghav Goyal
     */
    public static function admin_promocode_all(Request $request) {
        try {
            $data = CommonController::set_starting_ending($request->all());
            $coupons = Coupon::admin_coupons($data);
            //echo "<pre>";
            //print_r($coupons);
            //die;
            return View::make('Admin.Promocode.allCoupons', compact('coupons'))->with('fstarting_dt', $data['fstarting_dt'])->with('fending_dt', $data['fending_dt']);
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    public static function getpromousersrides(Request $request) {
        try {
            echo "<pre>";
            print_r($request->all());
            die;
            //$data = CommonController::set_starting_ending($request->all());
            $coupons = Coupon::users_coupons_get_maxrides($request);
            //echo "<pre>";
            //print_r($coupons);
            //die;
            return response()->json($coupons);
            //return View::make('Admin.Promocode.allCoupons', compact('coupons'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    /**
     * @role Add Promocode
     * @Method Get
     * @author Raghav Goyal
     */
    public static function admin_promocode_aget(Request $request) {
        $request['category_id'] = "1";
        $brands = CategoryBrand::admin_brands_list_add_drivers($request->all());
        $category_type = "Service";
        $language_id = "1";
        $category = Category::service_support_listings($category_type, $language_id);
        $data = array();
        $data['blocked_check'] = "Active";
        $users = UserDetail::cust_listings($data);
        return View::make('Admin.Promocode.addCoupons', compact('brands', 'category', 'users'));
        //return View::make('Admin.Promocode.addCoupons', compact('brands', 'category'));
    }

    public function autocomplete(Request $request) {
        $request['blocked_check'] = "Active";
        $custs = UserDetail::where('user_type_id', 1);
        ///////////////        Filters     ////////////////////////
        if (isset($request['blocked_check']) && $request['blocked_check'] == 'Active')
            $custs->where('user_details.blocked', '0');
        ///////////////        Filters     ////////////////////////
        $custs->join('users', 'users.user_id', 'user_details.user_id');
        $custs->select('*');
        $custs->where("users.name", "LIKE", "%{$request->query}%");
        $custs->orderBy('user_detail_id', 'DESC');
        return $custs->get();
    }

    /**
     * @role Add Promocode
     * @Method POST
     * @author Raghav Goyal
     */
    public static function admin_promocode_apost(Request $request) {
        try {
            $data = Input::all();
            /* $rules = array(
              'code' => 'required|unique:coupons,code',
              'rides_value' => 'required|integer|min:1',
              'coupon_type' => 'required|in:Percentage,Value',
              'amount_value' => 'required|min:0',
              'price' => 'required|min:0',
              'expires_at' => 'required',
              'expiry_days' => 'required|min:1'
              );

              $validator = Validator::make($request->all(),$rules);
              if($validator->fails())
              return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all()); */
            if(!isset($data['usersId']))
			{
				$data['usersId'] = array();
			}
			if(!isset($data['all_users']))
			{
				$data['all_users'] = '0';
			}
			if ($request['credit_start_dt'] != NULL) {
                $request['credit_start_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['credit_start_dt'], $request['timezone'])->timezone('UTC')->format('Y-m-d H:i:s');
            }
            if (Input::file('image')) {
                $request['profile_pic'] = CommonController::image_uploader(Input::file('image'));
            } else {
                //$request['profile_pic'] = "dummy.png";
                $request['profile_pic'] = "";
            }

            //date_format(CONVERT_TZ($request['sss'],'+00:00','" . $data['timezonez'] . "'),'%M %d %Y. %h:%i %p');
            $coupon = Coupon::add_new_record($request->all());
            if ($coupon) {
                PromocodeProduct::save_promocode_products($coupon->coupon_id, $data);
                PromoUser::save_promocode_users($coupon->coupon_id, $data);
                PromoUsersRide::save_promocode_users_rides($coupon->coupon_id, $data);
            }
            return Redirect::route('admin_promocode_all')->with('status', 'Promocode added successfully');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

///////////////////////////        Coupon Update Get     ////////////////////////////////////////////
    public static function admin_promocode_uget(Request $request) {
        try {
            $data = Input::all();
            $rules = array(
                'coupon_id' => 'required|exists:coupons,coupon_id'
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

            $coupon = Coupon::single_coupon_details($request->all());
            $request['category_id'] = "1";
            $brands = CategoryBrand::admin_brands_list_add_drivers($request->all());
            $category_type = "Service";
            $language_id = "1";
            $category = Category::service_support_listings($category_type, $language_id);
            $data = array();
            $data['blocked_check'] = "Active";
            $users = UserDetail::cust_listings($data);
            return View::make('Admin.Promocode.updateCoupons', compact('coupon', 'brands', 'category', 'users'));
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

//////////////////        Add Coupon Post         //////////////////////////////////////////
    public static function admin_promocode_upost(Request $request) {
        try {

            $data = Input::all();

            $rules = array(
                'coupon_id' => 'required|exists:coupons,coupon_id',
                'code' => 'required|unique:coupons,code,' . $request['coupon_id'] . ',coupon_id',
                'rides_value' => 'required|integer|min:1',
                'coupon_type' => 'required|in:Percentage,Value',
                'amount_value' => 'required|min:0',
                'price' => 'required|min:0',
                'expires_at' => 'required',
                'expiry_days' => 'required|min:1'
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());
			if(!isset($data['usersId']))
			{
				$data['usersId'] = array();
			}
			if(!isset($data['all_users']))
			{
				$data['all_users'] = '0';
			}
            $promo = Coupon::single_coupon_details($data);
            if (!empty(Input::file('image'))) {
                $request['profile_pic'] = CommonController::image_uploader(Input::file('image'));
            } else {
                $request['profile_pic'] = $promo->image;
            }
            if ($request['credit_start_dt'] != NULL) {
                $request['credit_start_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['credit_start_dt'], $request['timezone'])->timezone('UTC')->format('Y-m-d H:i:s');
            }
            $coupon = Coupon::update_record($request->all());

            DB::table('promocode_products')->where('coupon_id', $data['coupon_id'])->delete();
            PromocodeProduct::save_promocode_products($data['coupon_id'], $data);

            DB::table('promo_users')->where('coupon_id', $data['coupon_id'])->delete();
            PromoUser::save_promocode_users($data['coupon_id'], $data);

            //Get Promo User Method
            $uss = array();
            for ($i = 0; $i < count($data['usersId']); $i++) {
                $uss[] = $data['usersId'][$i];
            }
            $uss1 = array();
            $vv = DB::table('promo_users_rides')->where('coupon_id', $data['coupon_id'])->get()->toArray();
            for ($i = 0; $i < count($vv); $i++) {
                $uss1[] = $vv[$i]->user_id;
            }
            $Diff_user = array_diff($uss1, $uss);
            $New_User = array();
            foreach ($Diff_user as $newuser) {
                $New_User[] = $newuser;
            }
            for ($j = 0; $j < count($New_User); $j++) {
                DB::table('promo_users_rides')->where('user_id', $New_User[$j])->where('coupon_id', $data['coupon_id'])->delete();
            }
            for ($h = 0; $h < count($data['usersId']); $h++) {
                $vv1 = DB::table('promo_users_rides')->where('user_id', $data['usersId'][$h])->where('coupon_id', $data['coupon_id'])->first();
                if (empty($vv1)) {
                    DB::table('promo_users_rides')->insert(
                            ['coupon_id' => $data['coupon_id'], 'user_id' => $data['usersId'][$h], 'max_rides' => $data['rides_value'], 'created_at' => new \DateTime, 'updated_at' => new \DateTime]
                    );
                }
            }
            //End Method
            return Redirect::route('admin_promocode_all')->with('status', 'Promocode updated successfully');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

/////////////////////////////			Organisation Block Unblock 	///////////////////////////////
    public static function admin_promocode_block(Request $request) {
        try {

            $rules = array(
                'coupon_id' => 'required|exists:coupons,coupon_id',
                'block' => 'required|in:0,1'
            );
            $msg = array('coupon_id.exists' => 'Sorry, this coupon is currently not available');

            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

            $coupon = Coupon::where('coupon_id', $request['coupon_id'])->first();

            if ($request['block'] == 1) {

                if ($coupon->blocked == '1')
                    return Redirect::back()->withErrors('This coupon is already blocked')->withInput();

                Coupon::where('coupon_id', $coupon->coupon_id)->update(array(
                    'blocked' => '1',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Coupon blocked successfully');
            }
            else {

                if ($coupon->blocked == '0')
                    return Redirect::back()->withErrors('This coupon is already unblocked')->withInput();

                Coupon::where('coupon_id', $coupon->coupon_id)->update(array(
                    'blocked' => '0',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Coupon unblocked successfully');
            }
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

///////////////////////		Contact us listings 	/////////////////////////////////////////////
    public static function admin_contactus(Request $request) {
        try {

            $data = CommonController::set_starting_ending($request->all());

            $messages = AdminContactus::admin_contactus_all($data);

            return View::make('Admin.Contactus.ContactusAll', compact('messages'))->with('fstarting_dt', $data['fstarting_dt'])->with('fending_dt', $data['fending_dt']);
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

//////////////////		Admin Reply Contact US 	///////////////////////////////////////////////////
    public static function admin_contactus_preply(Request $request) {
        try {

            $data = Input::all();
            $rules = array(
                'parent_admin_contactus_id' => 'required|exists:admin_contactus,admin_contactus_id',
                'message' => 'required',
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

            $request['user_type_id'] = 5; //Admin
            $omsg = AdminContactus::where('admin_contactus_id', $request['parent_admin_contactus_id'])->first();
            $request['user_id'] = $omsg->user_id;
            $request['user_detail_id'] = $omsg->user_detail_id;
            $request['user_type_id'] = $omsg->user_type_id;

            $msg = AdminContactus::add_new_record($request->all());

            $ouser = User::where('user_id', $msg->user_id)->first();

///////////////		SMS 		////////////////////////////
            $request['phone_number'] = $ouser->phone_number;

            //$result = CommonController::send_sms($request->all());
///////////////		SMS 		////////////////////////////

            return Redirect::back()->with('status', 'Replied successfully');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

//////////////////////		Contact Us History 	/////////////////////////////////////////////////
    public static function admin_contactus_history(Request $request) {
        try {

            $rules = array(
                'admin_contactus_id' => 'required|exists:admin_contactus,admin_contactus_id'
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

            $msg = AdminContactus::single_contactus_details($request->all());

            return View::make('Admin.Contactus.ContactusHist', compact('msg'));
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

//////////////////////		Admin All Reviews 		/////////////////////////////////////////////
    public static function admin_all_reviews(Request $request) {
        try {

            $data = CommonController::set_starting_ending($request->all());

            $data['order_filter'] = isset($data['order_filter']) ? $data['order_filter'] : 'All';
            $data['createdby_filter'] = isset($data['createdby_filter']) ? $data['createdby_filter'] : 'All';
            $data['search'] = isset($data['search']) ? $data['search'] : '';

            $reviews = OrderRating::admin_reviews_new($data);
		
            return View::make('Admin.Reviews.allReviewsNew', compact('reviews', 'data'));
        } catch (\Exception $e) {
            dd($e->getMessage());
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

///////////////////////////////////////////////////////////////////////////////
////////////////////////		Ledgers 		///////////////////////////////
///////////////////////////////////////////////////////////////////////////////
    public static function admin_ledger_all(Request $request) {
        try {

            $data = CommonController::set_starting_ending($request->all());

            $ledgers = PaymentLedger::admin_all_ledgers($data);

            return View::make('Admin.Ledgers.allLedgers', compact('ledgers'))->with('fstarting_dt', $data['fstarting_dt'])->with('fending_dt', $data['fending_dt']);
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

///////////////////////////////////////////////////////////////////////////////
////////////////////////		Ledgers 		///////////////////////////////
///////////////////////////////////////////////////////////////////////////////
/////////////////////////		Admin Login History 		////////////////////////////////////////
    public static function admin_login_history(Request $request) {
        try {

            $admin = \App('admin');
            $request['admin_id'] = $admin->admin_id;

            $request['user_type_ids'] = [5];

            $data = CommonController::set_starting_ending($request->all());

            $histories = AdminUserDetailLogin::login_history_get($data);

            return View::make('Admin.History.allHistory', compact('histories'))->with('fstarting_dt', $data['fstarting_dt'])->with('fending_dt', $data['fending_dt']);
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    ////////////////////////////--Notification Module--///////////////////////

    /*
     * @role List Notifications
     * @Method GET
     * @author Raghav Goyal
     */
    public static function admin_notification_all(Request $request) {
        try {
            $notifications = Notification::admin_get_lists($request->all());

            return View::make('Admin.Notification.allNotifications', compact('notifications'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    /**
     * @role Add Notification
     * @Method GET
     * @author Raghav Goyal
     */
    public static function admin_notification_aget(Request $request) {
        try {
            $ranking = Notification::count();
            $promocode = Coupon::all();
            $language_id = $request['admin_language_id'];
            $category_type = "Service";
            $Services = Category::service_support_listings($category_type, $language_id);
            return View::make('Admin.Notification.addNotifications', compact('ranking', 'promocode', 'Services'));
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    /**
     * @role Add Notification POST
     * @Method POST
     * @author Raghav Goyal
     */
    public static function admin_notification_apost(Request $request) {
        try {
            $data = Input::all();
            $rules = array(
                'title' => 'required',
                'message' => 'required',
                'type' => 'required',
                'date' => 'required',
                'status' => 'required',
                'recurring' => 'required',
                'ranking' => 'required',
                'notificationImage' => 'mimes:mp4,wmv,jpg,png,jpeg',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());
            /*if (Input::file('notificationImage')) {
                $request['profile_pic_name'] = CommonController::image_uploader(Input::file('notificationImage'));
            } else {
                $request['profile_pic_name'] = "dummy.png";
            }*/
            if(Input::file('notificationImage'))
            {
                $imageupload = Input::file('notificationImage')->GetClientOriginalExtension();
                $request['profile_pic_name'] = CommonController::image_uploader(Input::file('notificationImage'));
                if(in_array($imageupload,['mp4','wmv']))
                {
                    $request['media_type'] = "Video";
                    $request['video_url']  = env('IMAGE_URL').$request['profile_pic_name'];
                    $request['profile_pic_name'] = "";
                    $request['video_thumbimage'] = env('IMAGE_URL')."/admin/public/BuraqExpress/Uploads/2bb3355e4d72a3393e70c612301c4226a0ccc708MKMm4nwXGGqluLm5AkzQVqVcR.png";
                }else{
                    $request['media_type'] = "Image";
                    $request['video_url']  = "";
                    $request['profile_pic_name'] =  env('IMAGE_URL').$request['profile_pic_name'];
                    $request['video_thumbimage']  = "";
                }
            }else{
                $request['profile_pic_name'] = "";
                $request['media_type'] = "";
                $request['video_url']  = "";
                $request['video_thumbimage']  = "";
            }
            $notification = Notification::add_new_record($request->all());
            $request['notificationId'] = $notification->notification_id;
            //Send Notification
            if ($data['type'] == "Customer") {
                $type = "1";
            } else {
                $type = "2";
            }
            $ss = isset($data['service_id']) ? $data['service_id'] : "0";
            $coupon_id = isset($data['coupon_id']) ? $data['coupon_id'] : "0";
            //if ($request['profile_pic_name'] != "") {
                if($request['profile_pic_name'] == "dummy.png"){
                    $request['profile_pic_name'] = "";
                }else{
                    $request['profile_pic_name'] = $request['profile_pic_name'];
                }
                
            //}
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://phytotherapy.in:8051/user/service/SendMultipleNotificationtousers");
            curl_setopt($ch, CURLOPT_POST, 1);
            //curl_setopt($ch, CURLOPT_POSTFIELDS, "message=".$data['message']."&type=".$type."");
            curl_setopt($ch, CURLOPT_POSTFIELDS, "message=" . $data['message'] . "&type=" . $type . "&couponId=" . $coupon_id . "&serviceId=" . $ss . "&title=" . $data['title'] . "&image=" . $request['profile_pic_name'] . "");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec($ch);
            curl_close($ch);
            return Redirect::route('admin_notification_all')->with('status', 'Notification added successfully');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    /**
     * @role Get Notification Details
     * @Method GET
     * @author Raghav Goyal
     */
    public static function admin_notification_vget(Request $request) {
        try {
            $data = Input::all();
            $rules = array(
                'notification_id' => 'required|exists:notifications,notification_id'
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

            $notification = Notification::single_notification_details($request->all());

            return View::make('Admin.Notification.viewNotification', compact('notification'));
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    /**
     * @role Notification change Status Active/In Active
     * @Method GET
     * @author Raghav Goyal
     * @date 2-May-2019
     */
    function admin_notification_active(Request $request) { 
        $notification = Notification::where('notification_id', $request['notification_id'])->first();
        if ($request['block'] == 1) {
            Notification::where('notification_id', $request['notification_id'])->update(array(
                'status' => 'In Active',
                'updated_at' => new \DateTime
            ));
            return Redirect::back()->with('status', 'Notification InActive successfully', ['blocked_check' => $request->type]);
        } else {
            Notification::where('notification_id', $request['notification_id'])->update(array(
                'status' => 'Active',
                'updated_at' => new \DateTime
            ));
            return Redirect::back()->with('status', 'Notification active successfully', ['blocked_check' => $request->type]);
        }
    }

    /**
     * @role delete Notification
     * @Method GET
     * @author Raghav Goyal
     * @date 02-May-2019
     */
    function admin_notification_delete(Request $request) {
        $notificationId = $request['notification_id'];
        DB::table('notifications')->where('notification_id', $notificationId)->delete();
        $msg = 'Notification deleted successfully';
        return Redirect::Back()->with('status', $msg);
    }

    /**
     * @role Get Edit Notification details
     * @Method GET
     * @author Raghav Goyal
     * @date 02-May-2019
     */
    function admin_notification_ugetold(Request $request) {
        try {
            $data = Input::all();
            $rules = array(
                'notification_id' => 'required|exists:notifications,notification_id'
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

            $notification = Notification::single_notification_details($request->all());
            $promocode = Coupon::all();
            return View::make('Admin.Notification.updateNotification', compact('notification', 'promocode'));
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    function admin_notification_uget(Request $request) {
        try {
            $data = Input::all();
            $rules = array(
                'notification_id' => 'required|exists:notifications,notification_id'
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

            $notification = Notification::single_notification_details($request->all());

            $language_id = $request['admin_language_id'];
            $category_type = "Service";
            $Services = Category::service_support_listings($category_type, $language_id);
            $promocode = Coupon::all();
            return View::make('Admin.Notification.updateNotification', compact('notification', 'Services', 'promocode'));
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    /**
     * @role Update Notification
     * @Method POST
     * @author Raghav Goyal
     * @date 02-May-2019
     */
    function admin_notification_uposttest(Request $request) {
        try {
            $data = Input::all();
            $notification = Notification::single_notification_details($data);
            if (!empty(Input::file('notificationImage'))) {
                $data['profile_pic_name'] = CommonController::image_uploader(Input::file('notificationImage'));
            } else {
                $data['profile_pic_name'] = $notification->image;
            }

            Notification::where('notification_id', $data['notification_id'])->update(array(
                'title' => $data['title'],
                'message' => $data['message'],
                'image' => $data['profile_pic_name'],
                'type' => $data['type'],
                'broadcastDate' => $data['date'],
                'status' => $data['status'],
                'recurring' => $data['recurring'],
                'coupon_id' => $data['coupon_id'],
                'updated_at' => new \DateTime
            ));
            return Redirect::back()->with('status', 'Notification updated successfully');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    function admin_notification_upost(Request $request) {
        try {
            $data = Input::all();

            $notification = Notification::single_notification_details($data);

            if (!empty(Input::file('notificationImage'))) {
                $data['profile_pic_name'] = CommonController::image_uploader(Input::file('notificationImage'));
            } else {
                $data['profile_pic_name'] = $notification->image;
            }

            Notification::where('notification_id', $data['notification_id'])->update(array(
                'title' => $data['title'],
                'message' => $data['message'],
                'image' => $data['profile_pic_name'],
                'type' => $data['type'],
                'broadcastDate' => $data['date'],
                'status' => $data['status'],
                'recurring' => $data['recurring'],
                'service_id' => isset($data['service_id']) ? $data['service_id'] : 0,
                'coupon_id' => $data['coupon_id'],
                'updated_at' => new \DateTime
            ));
            return Redirect::route('admin_notification_all')->with('status', 'Notification updated successfully');
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    /**
     * @role Admin Notification All
     * @Method ANY
     * @author Raghav Goyal
     * @date 02-May-2019
     */
    public static function admin_notification(Request $request) {
        try {
            $data = CommonController::set_starting_ending($request->all());
            $notifications = Notification::admin_notification_listings($data);

            return View::make('Admin.Notification.allNotifications', compact('notifications', 'data'))->with('fstarting_dt', $data['fstarting_dt'])->with('fending_dt', $data['fending_dt']);
        } catch (\Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    public static function admin_testnotfication_all(Request $request) {
        
        date_default_timezone_set('Asia/Kolkata');
        $dt = date('Y-m-d H:i:00');

        print_r($dt);
        die('testing');
        $notifications = DB::table('notifications')
                ->where("status", "Active")
                ->where("recurring", "On")
                ->where("broadcastDate", "2019-04-30 12:40:45")
                ->get();
        if (!empty($notifications)) {
            foreach ($notifications as $notification) {
                /*$endpoint = "http://phytotherapy.in:8051/user/service/SendMultipleNotificationtousers";
                $client = new \GuzzleHttp\Client();
                $response = $client->request('POST', $endpoint, ['notification' => [
                        'message' => $notification->message,
                        'type' => '1',
                    'couponId' => $notification->coupon_id,
                    'serviceId' => $notification->service_id,
                    'title' => $notification->title,
                    'image' => $notification->image
                ]]);
                $statusCode = $response->getStatusCode();
                $content = $response->getBody();
                dd($content);*/
                if($notification->type == "Customer")
                {
                    $type = '1';
                }else{
                    $type = '2';
                }
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "http://phytotherapy.in:8051/user/service/SendMultipleNotificationtousers");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "message=" . $notification->message . "&type=".$type."&couponId=" . $notification->coupon_id . "&serviceId=" . $notification->service_id . "&title=" . $notification->title . "&image=" . $notification->image);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $server_output = curl_exec($ch);
                curl_close($ch);
            }
        }
    }
	
	/**************************** Floor Margin Module *********************************/
	/**
	 * @role List All Floor Margins
	 * @Method Get
	 * @author Raghav Goyal
	 */
	public static function admin_floormargin_all(Request $request)
	{
		try{
			$data        = CommonController::set_starting_ending($request->all());
			$FloorMargin = FloorMargin::admin_get_lists($data);
			return View::make('Admin.FloorMargin.allfloormargin', compact('FloorMargin'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);
		}
		catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

	/**
	 * @role Add Floor Margin
	 * @Method Get
	 * @author Raghav Goyal
	 */
	public static function admin_floor_aget()
	{
		return View::make('Admin.FloorMargin.add');
	}

	/**
	 * @role Add Floor Margin
	 * @Method POST
	 * @author Raghav Goyal
	 */
	public static function admin_floor_apost(Request $request)
	{

		$floor = FloorMargin::where('floor_name', $request['floor_name'])->select("*")->first();
		if($floor)
		{
			return Redirect::Back()->with('status','Floor name already added');
		}
		FloorMargin::add_new_record($request->all());
		return Redirect::route('admin_floormargin_all')->with('status','Floor margin added successfully');
	}

	/**
	 * @role Delete Floor Margin
	 * @Method Get
	 * @author Raghav Goyal
	 */
	public static function admin_floor_delete(Request $request)
	{
		DB::table('floor_margins')->where('floormargin_id', $request['floormargin_id'])->delete();
		return Redirect::route('admin_floormargin_all')->with('status','Floor margin deleted successfully');
	}

	/**
	 * @role Active/De-Active Floor Margin
	 * @Method Get
	 * @author Raghav Goyal
	 */
	public static function admin_floor_active(Request $request)
	{
		if($request['block'] != 1)
		{
			$data = array();
			FloorMargin::where('floormargin_id',$request['floormargin_id'])->update(array(
				'status' => 0,
				'updated_at' =>new \DateTime
			));
			return Redirect::Back()->with('status','Floor margin de-active successfully');
		}
		else
		{
			$data = array();
			FloorMargin::where('floormargin_id',$request['floormargin_id'])->update(array(
				'status' => 1,
				'updated_at' => new \DateTime
			));
			return Redirect::Back()->with('status','Floor margin active successfully');
		}
	}

	/**
	* @role Get Floor Details 
	* @Method GET
	* @author Raghav Goyal
	*/
	public static function admin_floor_uget(Request $request)
	{
		try{
			$floordetails = FloorMargin::single_floor_details($request->all());
			return View::make('Admin.FloorMargin.update', compact('floordetails'));
		}
		catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

	/**
	* @role Update Floor Details 
	* @Method POST
	* @author Raghav Goyal
	*/
	public static function admin_floor_upost(Request $request)
	{
		try{
			$floordetails = FloorMargin::floor_details($request->all());
			if($floordetails)
			{
				return Redirect::Back()->with('status','Floor name already added');
			}
			$floor_status = isset($request['floor_status']) ? $request['floor_status'] : '0';
			FloorMargin::where('floormargin_id',$request['floor_id'])->update(array(
				'floor_name' => $request['floor_name'],
				'price' => $request['price'],
				'all_floor_price_status' => $floor_status,
				'updated_at' =>new \DateTime
			));
			return Redirect::route('admin_floormargin_all')->with('status','Floor margin updated successfully');
		}
		catch(\Exception $e)
		{ 
			return Redirect::back()->withErrors($e->getMessage());
		}
	}
	
	/********************** Reward Point Module *********************/
	/**
	*@role Get All Reward Point
	*@Method GET
	*@author Raghav
	*/
	public static function admin_rewardpoint_all(Request $request)
	{
		try{
			$data        = CommonController::set_starting_ending($request->all());
			$RewardPoint = RewardPoint::admin_get_lists($data);
			return View::make('Admin.RewardPoint.allrewardpint', compact('RewardPoint'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);
		}
		catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

	/**
	*@role Get All Reward Point
	*@Method GET
	*@author Raghav
	*/
	public static function admin_rewardpoint_aget(Request $request)
	{
		$request['category_id'] = '1';
		$brands                 = CategoryBrand::admin_brands_list_add_drivers($request->all());
		$category_type          = "Service"; 
		$language_id            = "1";
		$category               = Category::service_support_listings($category_type,$language_id);
		return View::make('Admin.RewardPoint.add', compact('brands','category'));
	}

	/**
	 * @role Add Reward Point
	 * @Method POST
	 * @author Raghav Goyal
	 */
	public static function admin_rewardpoint_apost(Request $request)
	{
		$rewardpoint = RewardPoint::where('send_invite', $request['send_invite'])->select("*")->first();
		if($rewardpoint)
		{
			return Redirect::Back()->with('status','Reward point already added');
		}
		RewardPoint::add_new_record($request->all());
		return Redirect::route('admin_rewardpoint_all')->with('status','Reward point added successfully');
	}
	
	/** 
	 * @role Active/De-Active Reward Point
	 * @Method Get
	 * @author Raghav Goyal
	 */
	public static function admin_rewardpoint_active(Request $request)
	{
		if($request['block'] == 0)
		{
			$data = array();
			RewardPoint::where('reward_id',$request['rewardId'])->update(array(
				'status' => 0,
				'updated_at' =>new \DateTime
			));
			return Redirect::Back()->with('status','Reward point de-active successfully');
		}
		else
		{
			$data = array();
			RewardPoint::where('reward_id',$request['rewardId'])->update(array(
				'status' => 1,
				'updated_at' => new \DateTime
			));
			return Redirect::Back()->with('status','Reward point active successfully');
		}
	}

	/**
	* @role Get Reward Point Details 
	* @Method GET
	* @author Raghav Goyal
	*/
	public static function admin_rewardpoint_uget(Request $request)
	{
		try
		{
			$rewarddetails = RewardPoint::single_reward_details($request->all());
			$category_type = "Service"; 
			$language_id   = "1";
			$category      = Category::service_support_listings($category_type,$language_id);
			return View::make('Admin.RewardPoint.update', compact('rewarddetails','category'));
		}
		catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

	/**
	* @role Update Reward Details 
	* @Method POST
	* @author Raghav Goyal
	*/
	public static function admin_rewardpoint_upost(Request $request)
	{
		try
		{
			$rewarddetails = RewardPoint::reward_details($request->all());
			if($rewarddetails)
			{
				return Redirect::Back()->with('status','Reward point already added');
			}
			RewardPoint::where('reward_id',$request['reward_id'])->update(array(
				'send_invite'        => $request['send_invite'],
				'add_point'          => $request['reward_point'],
				'reedemption_reward' => $request['reedemption_reward_point'],
				'reedemption_price'  => $request['reward_price'],
				'updated_at'         => new \DateTime
			));
			return Redirect::route('admin_rewardpoint_all')->with('status','Reward point updated successfully');
		}
		catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

}
