<?php

namespace App\Http\Controllers\Admin\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;

use App\Http\Controllers\CommonController;

use App\Models\Order;
use App\Models\OrderRating;
use App\Models\User;
use App\Models\UserDetail;

class AdminCustCont extends Controller
{

///////////////////////////////////		Customer All 		///////////////////////////////////////
public static function admin_custs(Request $request)
	{
	try{

			$data = CommonController::set_starting_ending($request->all());

			$custs = UserDetail::admin_cust_listings($data);
	
			return View::make('Admin.Customers.allCustomersNew', compact('custs', 'data'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

//////////////////////////		Custoemrs Ajax 		///////////////////////////////////////
public static function admin_custs_ajax(Request $request)
	{
	try{

			//$custs = UserDetail::admin_cust_ajax_list($request->all());

			// return Response::json([	
			// 	"results" => [
			// 	    {
			// 	      "id": 1,
			// 	      "text": "Option 1"
			// 	    },
			// 	    {
			// 	      "id": 2,
			// 	      "text": "Option 2"
			// 	    }
			//   	],
			//   "pagination": {
			//     	"more": true
			//   	}
			// ]);


		}
	catch(\Exception $e)
		{
			//return Redirect::back()->withErrors($e->getMessage());
		}
	}

///////////////////////		Custoemr Block Unblock 	///////////////////////////////////////////////
public static function admin_cust_block(Request $request)
	{
	try{

			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id',
				'block' => 'required|in:0,1'
			);
			$msg = array('user_detail_id.exists' => 'Sorry, this customer is currently not available');

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$customer = UserDetail::where('user_detail_id',$request['user_detail_id'])->first();

			if($request['block'] == 1)
				{

					if($customer->blocked == '1')
						return Redirect::back()->withErrors('This customer is already blocked')->withInput();

					UserDetail::where('user_detail_id',$customer->user_detail_id)->update(array(
						'blocked' => '1',
						'updated_at' =>new \DateTime
					));

					return Redirect::back()->with('status','Customer blocked successfully');

				}
			else
				{

					if($customer->blocked == '0')
						return Redirect::back()->withErrors('This customer is already unblocked')->withInput();

					UserDetail::where('user_detail_id',$customer->user_detail_id)->update(array(
						'blocked' => '0',
						'updated_at' => new \DateTime
					));

					return Redirect::back()->with('status','Customer unblocked successfully');

				}

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

//////////////////////////////		CUstomer Orders 	//////////////////////////////////////////
public static function admin_cust_orders(Request $request)
	{
	try{

			$rules = array(
					'user_detail_id'=>'required|exists:user_details,user_detail_id,user_type_id,1'
						);
			$msg = array('user_detail_id.exists'=>'Sorry, this customer is currently not available');

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$data = CommonController::set_starting_ending($request->all());

			$cust = UserDetail::cust_details($data);
			$orders = Order::cust_orders($data);

			return View::make('Admin.Customers.Orders.custOrdersAll', compact('cust', 'orders'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

///////////////////////////////		Order Details 	//////////////////////////////////////////////
public static function admin_cust_order_details(Request $request)
	{
	try{

			$rules = array(
					'order_id'=>'required|exists:orders,order_id'
						);
			$msg = array('order_id.exists'=>'Sorry, this order is currently not available');

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$order = Order::cust_order_details($request->all());

			if(isset($order->CRequest))
	 		$order->CRequest['full_track'] = json_decode($order->CRequest['full_track'], true);

			$request['user_detail_id'] = $order->customer_user_detail_id;
			$cust = UserDetail::cust_details($request->all());

			return View::make('Admin.Customers.Orders.custOrderDetails', compact('order', 'cust'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

//////////////////////		Custoemr Reviews 		//////////////////////////////
public static function admin_customer_reviews(Request $request)
	{
	try{
			$data = Input::all();
			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id,user_type_id,1',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());
			
			$data = CommonController::order_set_starting_ending($request->all());

		    $data['order_filter'] = isset($data['order_filter']) ? $data['order_filter'] : 'All';
		    $data['createdby_filter'] = isset($data['createdby_filter']) ? $data['createdby_filter'] : 'All';
		    $data['search'] = isset($data['search']) ? $data['search'] : '';

			$cust = UserDetail::cust_details($request->all());
			$reviews = OrderRating::admin_reviews_new($data);

			return View::make("Admin.Customers.CustReviews", compact('reviews', 'cust', 'data'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}



}
