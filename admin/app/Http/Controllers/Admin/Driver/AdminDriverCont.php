<?php

namespace App\Http\Controllers\Admin\Driver;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;

use App\Http\Controllers\CommonController;

use App\Models\Category;
use App\Models\Organisation;
use App\Models\OrganisationCategory;
use App\Models\CategoryBrand;
use App\Models\CategoryBrandProduct;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\UserDriverDetailProduct;

class AdminDriverCont extends Controller
{
/////////////////		Driver Block Unblock    ///////////////////////////////
public static function admin_driver_block(Request $request)
	{
	try{

			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id',
				'block' => 'required|in:0,1'
			);
			$msg = array('user_detail_id.exists' => 'Sorry, this driver is currently not available');

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$driver = UserDetail::where('user_detail_id',$request['user_detail_id'])->first();

			if($request['block'] == 1)
				{

					if($driver->blocked == '1')
						return Redirect::back()->withErrors('This driver is already blocked')->withInput();

					UserDetail::where('user_detail_id',$driver->user_detail_id)->update(array(
						'blocked' => '1',
						'updated_at' =>new \DateTime
					));

					return Redirect::back()->with('status','Driver blocked successfully');

				}
			else
				{

					if($driver->blocked == '0')
						return Redirect::back()->withErrors('This driver is already unblocked')->withInput();

					UserDetail::where('user_detail_id',$driver->user_detail_id)->update(array(
						'blocked' => '0',
						'updated_at' => new \DateTime
					));

					return Redirect::back()->with('status','Driver unblocked successfully');

				}

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

//////////////////////		Approve Unapprove Driver 		////////////////////////////////////
public static function admin_driver_approve(Request $request)
	{
	try{

			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id',
				'approve' => 'required|in:0,1'
			);
			$msg = array('user_detail_id.exists' => 'Sorry, this driver is currently not available');

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$driver = UserDetail::where('user_detail_id',$request['user_detail_id'])->first();

			if($request['approve'] == 1)
				{

					if($driver->approved == '1')
						return Redirect::back()->withErrors('This driver is already approved')->withInput();

					UserDetail::where('user_detail_id',$driver->user_detail_id)->update(array(
						'approved' => '1',
						'updated_at' =>new \DateTime
					));

					return Redirect::back()->with('status','Driver approved successfully');

				}
			else
				{

					if($driver->approved == '0')
						return Redirect::back()->withErrors('This driver is already unapproved')->withInput();

					UserDetail::where('user_detail_id',$driver->user_detail_id)->update(array(
						'approved' => '0',
						'updated_at' => new \DateTime
					));

					return Redirect::back()->with('status','Driver unapproved successfully');

				}

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

////////////////////////		Drivers Map 		///////////////////////////////
public static function admin_drivers_map(Request $request)
	{
	try{

			$services = Category::admin_get_service_categories($request->all());
			$supports = Category::admin_get_support_categories($request->all());

			return View::make('Admin.Drivers.allDriversMap', compact('services', 'supports'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

/////////////////////////		Map Data 	////////////////////////////////////////
public static function admin_drivers_map_data(Request $request)
	{
	try{

			$rules = array(
				'services' => 'required',
				'supports' => 'required'
			);

			$validator = Validator::make($request->all(),$rules);
				if($validator->fails())
					return Response(['success' => 0, 'statuscode' => 400, 'msg' => $validator->getMessageBag()->first() ],400);

			$request['category_ids'] = [];

			$services = json_decode($request['services'], true);
			$supports = json_decode($request['supports'], true);

			$request['category_ids'] = array_merge($services, $supports);

			if(empty($request['category_ids']))
				return Response(['success' => 1, 'statuscode' => 200, 'msg' =>"All Drivers", 'drivers' => [] ],200);

			$drivers = UserDetail::admin_drivers_map_get($request->all());

			return Response(['success' => 1, 'statuscode' => 200, 'msg' =>"All Drivers", 'drivers' => $drivers ],200);

		}
	catch(\Exception $e)
		{
			return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
		}
	}

/////////////////////////		Drivers Listings 	///////////////////////////////////////////////////
public static function admin_drivers_list(Request $request)
{
try{

		$request['user_type_ids'] = [2, 3];

		$data = CommonController::set_starting_ending($request->all());


	}
catch(\Exception $e)
	{
		return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
	}
}

///////////////////////////

}
