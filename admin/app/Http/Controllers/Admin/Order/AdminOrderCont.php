<?php

namespace App\Http\Controllers\Admin\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;

use App\Http\Controllers\CommonController;

use App\Models\Order;
use App\Models\Category;
use App\Models\OrderFailure;
//use App\Models\OrderRequest;
class AdminOrderCont extends Controller
{
////////////////////		Orders    ///////////////////////////
public static function admin_orders(Request $request)
	{
	try{
			$data = CommonController::set_starting_ending($request->all());

			if(!isset($data['status_check']))
				$data['status_check'] = 'All';

			if(!isset($data['categories']))
				$data['categories'] = [0];

			if(!isset($data['ordering_filter']))
				$data['ordering_filter'] = 1;

			$categories = Category::all_categories_single_lang($data['admin_language_id'], implode(',', $data['categories']));

			$orders = Order::admin_orders_listings($data);
			$data['daterange'] = $data['fstarting_dt'].' - '.$data['fending_dt'];
			
			return View::make('Admin.Orders.AdminOrdersAllNew', compact('orders', 'data', 'categories'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

/////////////////////		Export Orders 		////////////////////////////
public static function admin_export_orders(Request $request)
	{
	try{

			$rules = array(
					'format'=>'required|in:csv,pdf,excell'
						);

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$data = CommonController::set_starting_ending($request->all());

			if(!isset($data['status_check']))
				$data['status_check'] = 'All';

			if(!isset($data['order_type_check']))
				$data['order_type_check'] = 'All';

			$orders = Order::admin_orders_export_listings($data);

			dd($request->all(), $orders);


		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}


/////////////////////		Order Details 		////////////////////////////
public static function admin_order_details(Request $request)
	{
	try{

			$rules = array(
					'order_id'=>'required|exists:orders,order_id'
						);
			$msg = array('order_id.exists'=>'Sorry, this order is currently not available');

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$order = Order::order_details($request->all());
			$orderProduct = Order::order_details_products($request->all());
			
			if(isset($order->CRequest))
	 			$order->CRequest['full_track'] = json_decode($order->CRequest['full_track'], true);
			
			return View::make('Admin.Orders.AdminOrderDetails', compact('order','orderProduct'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}
	
	/**
	*@role Get All Failure Order
	*@Method POST
	*@author Raghav
	*/
	public static function admin_orders_failure(Request $request)
	{
		try
		{
			$orders     = OrderFailure::admin_order_failure_listings($request->all());
			$Order      = array();
			$OrderArray = array();
			for($i =0 ; $i<count($orders);$i++)
			{
				$strMonth              = json_decode($orders[$i]['products']);
				$OrderProducts         = implode(',',array_column($strMonth,'productName'));
				$stringMessage         = $orders[$i]['message'];
				$string                = substr($stringMessage, 4, -6);
				$Order['Id']           = $i;
				$Order['CategoryName'] = $string;
				$Order['OrderDate']    = $orders[$i]['order_date'];
				$Order['CustomerName'] = $orders[$i]['customer_name'];
				$Order['Status']       = "Sorry, no driver is currently available";
				$Order['OrderProduct'] = $OrderProducts;
				$Order['Created_at']   = $orders[$i]['created_atz'];
				$OrderArray[]          = $Order;
			}
			
			return View::make('Admin.OrderFailure.AdminOrderFailureAll', compact('OrderArray'));
		}
		catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}
//Add M2 Part 2
	public static function getDrivers(Request $request)
    {
		$order_Id     = $request['orderId'];
		$orderDetails = Order::select('*')->where('order_id', $order_Id)->get();
		$keys         = $orderDetails->toArray();
		if(!empty($keys))
		{
			$categoryId        = $keys[0]['category_id'];
			$category_brand_id = $keys[0]['category_brand_id'];
			$customer_user_id  = $keys[0]['customer_user_id'];
			$category_brand_id = $keys[0]['category_brand_id'];
			$dropoff_latitude  = $keys[0]['dropoff_latitude'];
			$dropoff_longitude = $keys[0]['dropoff_longitude'];
			//Get Order Products
			$ordersProducts = DB::table('order_products')
				->select('category_brand_product_id')
				->where('order_id',$order_Id)
				->get()
				->toArray();
			$data = array();
			for($i = 0;$i<count($ordersProducts);$i++)
			{
				$data[] = $ordersProducts[$i]->category_brand_product_id;
			}
			
			$string_version = implode(',', $data);
			//AddM3 Part 3.2
			$cards = DB::select("SELECT *, ud.user_id, u.name,u.phone_number, ud.notifications, ud.device_type, ud.language_id, ud.user_detail_id, ud.socket_id, ud.latitude, ud.longitude, ud.organisation_id, COALESCE(oc.buraq_percentage, udds.buraq_percentage) as buraq_percentage, COALESCE(o.bottle_charge, u.bottle_charge) as bottle_charge, COALESCE(o.blocked, '0') as org_blocked, (CASE WHEN ud.organisation_id = 0 THEN '1' ELSE (CASE WHEN oc.organisation_category_id IS NOT NULL THEN '1' ELSE '0' END) END) as org_cat FROM user_details as ud JOIN users as u ON (u.user_id=ud.user_id AND u.blocked='0') JOIN user_driver_detail_products uddp ON (uddp.user_detail_id=ud.user_detail_id AND uddp.user_id=ud.user_id AND uddp.category_brand_product_id IN ('".$string_version."') AND uddp.deleted='0') JOIN user_driver_detail_services as udds ON (udds.user_detail_id=ud.user_detail_id AND udds.deleted='0' AND udds.category_id='".$categoryId."') LEFT JOIN organisations as o ON (o.organisation_id=ud.organisation_id) LEFT JOIN organisation_categories as oc ON (oc.organisation_id=ud.organisation_id AND oc.category_id='".$categoryId."') WHERE (ud.user_type_id='2' AND ud.category_id='".$categoryId."' AND ud.category_brand_id='".$category_brand_id."' AND ud.online_status='1' AND ud.otp='' AND ud.access_token!='' AND ud.mulkiya_number!='' AND ud.blocked!='1' AND ud.user_id!='".$customer_user_id."') HAVING ( (org_blocked='0') AND (org_cat='1') AND (ROUND(((3959*acos(cos(radians('".$dropoff_latitude."')) * cos(radians(ud.latitude)) * cos(radians(ud.longitude)-radians('".$dropoff_longitude."'))+sin(radians('".$dropoff_latitude."'))*sin(radians(ud.latitude))))* 1.67),2) <= '100') )");
			
			for($j = 0;$j<count($cards);$j++)
			{
				$ordersR = DB::table('order_requests')
				->select('*')
				->where('driver_user_id',$cards[$j]->user_id)
				->where('driver_user_detail_id',$cards[$j]->user_detail_id)
				->where('order_request_status',"Accepted")
				->where('order_id',$order_Id)
				->get()
				->toArray();
				if(empty($ordersR))
				{
					$cards[$j]->accepted_status = '0'; //Not Accepted
				}
				else
				{
					$cards[$j]->accepted_status = '1'; // Accepted
				}
			}
		}
        return $cards;
    }
	
	public static function assignDriver(Request $request)
	{
		$orderId  = (int)$request['orderId'];
		$driverId = $request['driverId'];
		
		$re = AdminOrderCont::getordersdetails($orderId); //Add M3 Part 3.2

		$users = DB::table('order_requests')->select('*')->where('order_id', $orderId)->where('driver_user_detail_id', $driverId)->get()->toArray();
		$Driverusers = DB::table('user_details')->select('*')->where('user_detail_id', $driverId)->get()->toArray();
		if(empty($users))
		{
			/*DB::table('order_requests')->insert(
				['order_id' => $orderId, 'driver_user_id' => $Driverusers[0]->user_id, 'driver_user_detail_id' => $driverId, 'driver_organisation_id' => 0, 'order_request_status' => "Searching"]
			);*/
			DB::table('order_requests')->insert(
		    ['order_id' => $orderId, 'driver_user_id' => $Driverusers[0]->user_id,
		     'driver_user_detail_id' => $driverId, 'driver_organisation_id' => $Driverusers[0]->organisation_id,
			'order_request_status' => "Searching",'driver_request_latitude' => $Driverusers[0]->latitude,
			'driver_request_longitude' => $Driverusers[0]->longitude,'driver_current_latitude' => $Driverusers[0]->latitude,
			'driver_current_longitude' => $Driverusers[0]->longitude,'driver_confirmed_latitude' => 0.000000,
			'driver_confirmed_longitude' =>0.000000,'full_track' => "[]",
			'created_at'=>new \DateTime,'updated_at'=>new \DateTime ]
			);
		}
		$cards = DB::table('orders')
            ->where('order_id', $orderId)
            ->update(['order_status' => "Searching",'driver_user_id' => '0',
		     'driver_user_detail_id' => '0', 'created_at'=>new \DateTime,'updated_at'=>new \DateTime]);
		$cards = DB::table('order_requests')
		->where('order_id', $orderId)
		->update(['order_request_status' => "Searching", 'updated_at'=>new \DateTime]);
		$ch = curl_init();
		//curl_setopt($ch, CURLOPT_URL, "http://phytotherapy.in:8051/user/service/ReassignOrder");
		curl_setopt($ch, CURLOPT_URL, env('SOCKET_URL')."/user/service/ReassignOrder");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "orderId=" . $orderId . "&driverId=" . $driverId ."");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec($ch);
		curl_close($ch);
		
		return "true";
	}
	
	public static function shuffleDriver(Request $request)
	{
		$orderId  = (int)$request['orderId'];
		$driverId = $request['driverId'];
		$Acceptedusers = DB::table('order_requests')->select('driver_user_detail_id')->where('order_id', $orderId)->where('order_request_status',"Accepted")->get()->toArray();
		if(empty($Acceptedusers))
		{
			$AcceptedUser = '0';
		}else{
			$AcceptedUser = $Acceptedusers[0]->driver_user_detail_id;
		}
		
		
		$users = DB::table('order_requests')->select('*')->where('order_id', $orderId)->where('driver_user_detail_id', $driverId)->get()->toArray();
		$Driverusers = DB::table('user_details')->select('*')->where('user_detail_id', $driverId)->get()->toArray();
		if(empty($users))
		{
			DB::table('order_requests')->insert(
		    ['order_id' => $orderId, 'driver_user_id' => $Driverusers[0]->user_id,
		     'driver_user_detail_id' => $driverId, 'driver_organisation_id' => $Driverusers[0]->organisation_id,
			'order_request_status' => "Searching",'driver_request_latitude' => $Driverusers[0]->latitude,
			'driver_request_longitude' => $Driverusers[0]->longitude,'driver_current_latitude' => $Driverusers[0]->latitude,
			'driver_current_longitude' => $Driverusers[0]->longitude,'driver_confirmed_latitude' => 0.000000,
			'driver_confirmed_longitude' =>0.000000,'full_track' => "[]",
			'created_at'=>new \DateTime,'updated_at'=>new \DateTime ]
			);
		}
		$cards = DB::table('orders')
            ->where('order_id', $orderId)
            ->update(['order_status' => "Searching"]);
		$cards = DB::table('order_requests')
		->where('order_id', $orderId)
		->update(['order_request_status' => "Searching"]);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, env('SOCKET_URL')."/service/order/ReshuffleOrder");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "orderId=" . $orderId . "&driverId=" . $driverId ."&OldDriverId=". $AcceptedUser ."");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec($ch);
		curl_close($ch);
		
		return "true";
	}
	
	/*
	*@role Get Reward POint From Order & User Details
	*@date 13-09-2019
	*@Add M3 Part 3.2
	*/
	public static function getordersdetails($orderId)
	{
		$order                 = DB::table('orders')->select('customer_user_detail_id','reward_discount_point','schulderd_order_token')->where('order_id', $orderId)->get()->toArray();
		$customer_detail_id    = $order[0]->customer_user_detail_id;
		$order_reward_point    = $order[0]->reward_discount_point;
		
		$Stoken = $order[0]->schulderd_order_token;
		if($Stoken != "")
		{
			$cards = DB::table('orders')
				->where('schulderd_order_token',$Stoken)
				->where('order_id','!=',$orderId)
				->where('order_status','!=','SerComplete')
				->update(['order_status' => "Searching"]);
			
		}
		
		
		if($order_reward_point == '0')
		{
			return "true";
		}
		$CustomerDetail        = DB::table('user_details')->select('total_reward_point')->where('user_detail_id', $customer_detail_id)->get()->toArray();
		$customer_reward_point = $CustomerDetail[0]->total_reward_point;
		if((int)$customer_reward_point < (int)$order_reward_point)
		{
			$orderupdate = DB::table('orders')
				->where('order_id', $orderId)
				->update(['reward_discount_price' => 0.00,'reward_discount_point'=>0]);
		}
		return "true";
	}
	
	/*
	*@role Enable Disable Round Robin
	*@Add M3 Part 3.2
	*/
	public static function roundRobinEnableDisable(Request $request)
	{
		$value = $request['value'];
		DB::table('admins')
		->update(['round_robin' => $value]);
		return "true";
	}
}
