<?php

namespace App\Http\Controllers\Admin\Org;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;

use App\Http\Controllers\CommonController;

//App\Helpers

use App\Models\AdminUserDetailLogin;
use App\Models\CategoryBrandProduct;
use App\Models\Organisation;
use App\Models\OrganisationCategory;
use App\Models\OrganisationCategoryBrandProduct;
use App\Models\OrderRating;
use App\Models\Category;
use App\Models\PaymentLedger;

class AdminOrgCont extends Controller
{

///////////////////////////////////		Brand Products All 		///////////////////////////////////////
public static function admin_org_all(Request $request)
	{
	try{
			$data = CommonController::set_starting_ending($request->all());

			$request['category_type'] = 'Service';
			$services = Category::admin_categories_single_lang($request->all());

			$request['category_type'] = 'Support';			
			$supports = Category::admin_categories_single_lang($request->all());

			$organisations = Organisation::admin_all_Organisations($data);
			
			return View::make('Admin.Orgs.allOrgs', compact('organisations', 'services', 'supports'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

/////////////////////////////		Add New Organisation 		/////////////////////////////////////////
public static function admin_org_add(Request $request)
	{
	try{

			$request['category_type'] = 'Service';
			$services = Category::admin_categories_single_lang($request->all());

			$request['category_type'] = 'Support';			
			$supports = Category::admin_categories_single_lang($request->all());

			$ranking = Organisation::count();

			return View::make('Admin.Orgs.addOrg', compact('services', 'supports', 'ranking'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

/////////////////////////////		Add Organisation Post 	/////////////////////////////////////
public static function admin_org_add_post(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'name' => 'required',
				'image' => 'required|image',
				//'email' => 'required|email|unique:organisations,email',
				'phone_code' => 'required',
				'phone_number' => 'required|unique:organisations,phone_number',
				'username' => 'required|unique:organisations,username',
				'licence_number' => 'required|unique:organisations,licence_number',
				'password' => 'required',
				'servicesid' => 'sometimes|array',
				//'supportsid' => 'sometimes|array',
				// 'credit_amount_limit' => 'sometimes|integer|min:0',
				// 'credit_day_limit' => 'sometimes|integer|min:0',
				'credit_start_dt' => 'sometimes',
				'buraq_percentage' => 'required',
				'sort_order' => 'required'
			);
			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			if($request['credit_start_dt'] != NULL)
				{
					$request['credit_start_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['credit_start_dt'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');
				}

////////////////////////////		Images Upload 		////////////////////////////////
			$request['image_name'] = CommonController::image_uploader(Input::file('image'));
				if(empty($request['image_name']))
					return Redirect::back()->withErrors('Sorry, image not uploaded. Please try again')->withInput($request->all());
////////////////////////////		Images Upload 		////////////////////////////////

			$request['created_by'] = "Admin";
			$request['email'] = CommonController::generate_email();

			$organisation = Organisation::add_new_record($request->all());
			$request['organisation_id'] = $organisation->organisation_id;

			$con = [];
			if(isset($request['servicesid']) && !empty($request['servicesid']) )
				{
					foreach($request['servicesid'] as $serviceid)
						{
							$con[] = [
								'organisation_id' => $request['organisation_id'],
								'category_id' => $serviceid,
								'buraq_percentage' => $request['buraq_percentage'],
								'category_type' => 'Service',
								'created_at' => new \DateTime,
								'updated_at' => new \DateTime
							];
						}
				}

			if(isset($request['supportsid']) && !empty($request['supportsid']) )
				{
					foreach($request['supportsid'] as $supportid)
						{
							$con[] = [
								'organisation_id' => $request['organisation_id'],
								'category_id' => $supportid,
								'buraq_percentage' => $request['buraq_percentage'],
								'category_type' => 'Support',
								'created_at' => new \DateTime,
								'updated_at' => new \DateTime
							];
						}
				}

			if(!empty($con))
				OrganisationCategory::insert($con);

//////////////////		Org Products with Charges Dynamic 		//////////////////////////////////
		if(isset($request['servicesid']) && !empty($request['servicesid']) )
			{

				$con = [];
				$products = CategoryBrandProduct::whereIn('category_id',$request['servicesid'])->get();
				foreach($products as $product)
					{
						$con[] = [
							'organisation_id' => $request['organisation_id'],
							'category_id' => $product->category_id,
							'category_brand_id' => $product->category_brand_id,
							'category_brand_product_id' => $product->category_brand_product_id,
							'alpha_price' => $product->alpha_price,
							'price_per_quantity' => $product->price_per_quantity,
							'price_per_distance' => $product->price_per_distance,
							'price_per_weight' => $product->price_per_weight,
							'price_per_hr' => $product->price_per_hr,
							'price_per_sq_mt' => $product->price_per_sq_mt,
							'created_at' => new \DateTime,
							'updated_at' => new \DateTime
						];
					}

				if(!empty(($con)))
					OrganisationCategoryBrandProduct::insert($con);

			}
//////////////////		Org Products with Charges Dynamic 		//////////////////////////////////

			// $mail = \Mail::send('Emails.Common.Orgs.orgRegister', array('receiver'=>$organisation, 'data' => $request->all() ),
			// 	function($message) use ($organisation)
			// 		{

	  //   				$message->to($organisation->email, $organisation->name)->subject(env('APP_NAME').' - '.trans('messages.Organisation login credentials'));

	  //       });

			$request['sms'] = CommonController::send_sms([ 'phone_number' => $request['phone_number'], 'message' => 'You are registered as an organisation in '.env('APP_NAME') ]);

			///////////////		Ranking 	////////////////////////
			if($data['sort_order'] != $data['total'])
			{
				Organisation::where('sort_order','>=',$data['sort_order'])
				->update([
					'sort_order' => DB::RAW('sort_order + 1'),
					'updated_at' => new \DateTime
				]);
			}
			///////////////		Ranking 	////////////////////////

			$request['sms'] = CommonController::send_sms([ 'phone_number' => "96894704604", 'message' => 'You are registered as an organisation in '.env('APP_NAME'). '. Please check email '.$request['email'].' for credentials'  ]);
			
			return Redirect::back()->with('status','Organisation added successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////////////	Organisation Update Get 		///////////////////////////////////////
public static function admin_org_update(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$organisation = Organisation::admin_single_org_details($request->all());

			$request['category_type'] = 'Service';
			$services = Category::admin_org_cat_single_lang($request->all());

			$request['category_type'] = 'Support';			
			$supports = Category::admin_org_cat_single_lang($request->all());

			$ranking = Organisation::count();

			return View::make('Admin.Orgs.updateOrg', compact('organisation', 'services', 'supports', 'ranking'));

		}
	catch(Illuminate\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////////////	Organisation Update Post 		///////////////////////////////////////
public static function admin_org_update_post(Request $request)
	{
	try{
			$data = Input::all();
			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
				'name' => 'required',
				'image' => 'sometimes|image',
				//'email' => 'required|email|unique:organisations,email,'.$request['organisation_id'].',organisation_id',
				'phone_code' => 'required',
				'phone_number' => 'required|unique:organisations,phone_number,'.$request['organisation_id'].',organisation_id',
				'buraq_percentage' => 'required',
				'sort_order' => 'required'
				// 'username' => 'required|unique:organisations,username,username,'.$request['organisation_id'].',organisation_id',
				// 'licence_number' => 'required|unique:organisations,licence_number,'.$request['organisation_id'].',organisation_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			if($request['credit_start_dt'] != NULL)
				{
					$request['credit_start_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['credit_start_dt'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');
				}

			$organisation = Organisation::where('organisation_id', $request['organisation_id'])->first();

////////////////////////////		Images Upload 		////////////////////////////////
			if(!empty(Input::file('image')))
				{
				$request['image_name'] = CommonController::image_uploader(Input::file('image'));
					if(empty($request['image_name']))
						return Redirect::back()->withErrors('Sorry, image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['image_name'] = $organisation->image;
				}
////////////////////////////		Images Upload 		////////////////////////////////

			Organisation::update_record($request->all());

			$request['categories'] = [];

			if(isset($request['servicesid']) && !empty($request['servicesid']) )
				$request['categories'] = $request['servicesid'];

			if(isset($request['supportsid']) && !empty($request['supportsid']) )
				$request['categories'] = array_merge($request['categories'], $request['supportsid']);

			OrganisationCategory::whereNotIn('category_id',$request['categories'])->where('organisation_id',$request['organisation_id'])->update([
				'deleted' => '1',
				'updated_at' => new \DateTime
			]);
			$con = [];

			foreach($request['categories'] as $category)
				{
	
					$check = OrganisationCategory::where('organisation_id', $request['organisation_id'])->where('category_id', $category)->first();
					if($check)
						{
							OrganisationCategory::where('organisation_category_id',$check->organisation_category_id)->update([
								'deleted' => '0',
								'updated_at' => new \DateTime
							]);
						}
					else
						{
							$con[] = [
								'organisation_id' => $request['organisation_id'],
								'category_id' => $category,
								'buraq_percentage' => $request['buraq_percentage'],
								'category_type' =>  $category < 6 ? 'Service' : 'Support',
								"deleted" => "0",
								"created_at" => new \DateTime,
								"updated_at" => new \DateTime
							];
						}

				}

			if(!empty($con))
				OrganisationCategory::insert($con);

//////////////////		Org Products with Charges Dynamic 		//////////////////////////////////
		if(isset($request['servicesid']) && !empty($request['servicesid']) )
			{

				$con = [];
				$products = CategoryBrandProduct::whereIn('category_id',$request['servicesid'])->whereRaw("( (SELECT COUNT(*) FROM organisation_category_brand_products as ocbp WHERE ocbp.organisation_id=".$request['organisation_id']." AND ocbp.category_brand_product_id=category_brand_products.category_brand_product_id) = 0 )")->get();

				foreach($products as $product)
					{
						$con[] = [
							'organisation_id' => $request['organisation_id'],
							'category_id' => $product->category_id,
							'category_brand_id' => $product->category_brand_id,
							'category_brand_product_id' => $product->category_brand_product_id,
							'alpha_price' => $product->alpha_price,
							'price_per_quantity' => $product->price_per_quantity,
							'price_per_distance' => $product->price_per_distance,
							'price_per_weight' => $product->price_per_weight,
							'price_per_hr' => $product->price_per_hr,
							'price_per_sq_mt' => $product->price_per_sq_mt,
							'created_at' => new \DateTime,
							'updated_at' => new \DateTime
						];
					}

				if(!empty(($con)))
					OrganisationCategoryBrandProduct::insert($con);

			}
//////////////////		Org Products with Charges Dynamic 		//////////////////////////////////

			///////////////		Ranking 	////////////////////////
			if($data['sort_order'] != $organisation->sort_order)
				{
					Organisation::where('organisation_id',$data['organisation_id'])->update([
						'sort_order' => $data['sort_order'],
						'updated_at' => new \DateTime
					]);

					Organisation::where('sort_order','>=',$data['sort_order'])->where('organisation_id','!=',$data['organisation_id'])
					->update([
						'sort_order' => DB::RAW('sort_order + 1'),
						'updated_at' => new \DateTime
					]);
				}
			///////////////		Ranking 	////////////////////////


			return Redirect::back()->with('status','Organisation updated successfully');

	}
	catch(\Exception $e)
		{
			dd($e->getMessage(), $request->all());
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}
/////////////////////////////			Organisation Block Unblock 	///////////////////////////////
public static function admin_org_block(Request $request)
	{
	try{

			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
				'block' => 'required|in:0,1'
			);
			$msg = array('organisation_id.exists' => 'Sorry, this organisation is currently not available');

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$organisation = Organisation::where('organisation_id',$request['organisation_id'])->first();

			if($request['block'] == 1)
				{

					if($organisation->blocked == '1')
						return Redirect::back()->withErrors('This organisation is already blocked')->withInput();

					Organisation::where('organisation_id',$organisation->organisation_id)->update(array(
						'blocked' => '1',
						'updated_at' =>new \DateTime
					));

					return Redirect::back()->with('status','Organisation blocked successfully');

				}
			else
				{

					if($organisation->blocked == '0')
						return Redirect::back()->withErrors('This organisation is already unblocked')->withInput();

					Organisation::where('organisation_id',$organisation->organisation_id)->update(array(
						'blocked' => '0',
						'updated_at' => new \DateTime
					));

					return Redirect::back()->with('status','Organisation unblocked successfully');

				}

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

////////////////////////		Org Reviews 		////////////////////////////////////////////
public static function admin_org_reviews(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$organisation = Organisation::admin_single_org_details($request->all());

			$data = CommonController::order_set_starting_ending($request->all());

		    $data['order_filter'] = isset($data['order_filter']) ? $data['order_filter'] : 'All';
		    $data['createdby_filter'] = isset($data['createdby_filter']) ? $data['createdby_filter'] : 'All';
		    $data['search'] = isset($data['search']) ? $data['search'] : '';

			$reviews = OrderRating::admin_reviews_new($data);

			return View::make("Admin.Orgs.Reviews.AdminOrgReviews", compact('reviews', 'organisation', 'data'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

//////////////////////		Org Ledgers 	////////////////////////////////////////////////////
public static function admin_org_ledger_all (Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$data = CommonController::order_set_starting_ending($request->all());

			$organisation = Organisation::admin_single_org_details($request->all());

			$ledgers = PaymentLedger::admin_all_ledgers($data);

			return View::make("Admin.Orgs.Ledgers.orgLedgerAll", compact('ledgers', 'organisation'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

//////////////////////		Admin Ledger Add Post 	////////////////////////////////////
public static function admin_org_ledger_padd (Request $request)
	{
	try{

			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
				'amount' => 'required|min:0',
				'ledger_dt' => 'required',
				'type' => 'required'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$request['ledger_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['ledger_dt'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');
			$request['user_type_id'] = 4;

			PaymentLedger::insert_new_record($request->all());

			return Redirect::back()->with('status','Ledger added successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

/////////////////////////////		Ledger Update 	/////////////////////////////////////
public static function admin_org_ledger_pupdate (Request $request)
	{
	try{

			$rules = array(
				'payment_ledger_id' => 'required|exists:payment_ledgers,payment_ledger_id',
				'amount' => 'required|min:0',
				'uledger_dt' => 'required',
				'type' => 'required'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$request['ledger_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['uledger_dt'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');
			$request['user_type_id'] = 4;

			PaymentLedger::where('payment_ledger_id',$request['payment_ledger_id'])->limit(1)->update([
					'amount' => $request['amount'],
					'type' => $request['type'],
					'ledger_dt' => $request['ledger_dt'],
					'description' => isset($request['description']) ? $request['description'] : "",
					'updated_at' => new \DateTime
			]);

			return Redirect::back()->with('status','Ledger updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

/////////////////////////////		Lender Delete 	//////////////////////////////
public static function admin_org_ledger_delete (Request $request)
	{
	try{

			$rules = array(
				'payment_ledger_id' => 'required|exists:payment_ledgers,payment_ledger_id'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			PaymentLedger::where('payment_ledger_id',$request['payment_ledger_id'])->limit(1)->update([
					'deleted' => "1",
					'updated_at' => new \DateTime
			]);

			return Redirect::back()->with('status','Ledger deleted successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

/////////////////////////		Admin Login History 		////////////////////////////////////////
public static function admin_org_login_history(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$organisation = Organisation::admin_single_org_details($request->all());

			$request['user_type_ids'] = [4];

			$data = CommonController::set_starting_ending($request->all());

			$histories = AdminUserDetailLogin::login_history_get($data);

			return View::make('Admin.Orgs.LoginHistory.allLoginHistory', compact('histories','organisation'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}


///////////////////////////

}
