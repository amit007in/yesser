<?php

namespace App\Http\Controllers\Admin\Org;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;

use App\Http\Controllers\CommonController;

use App\Models\Category;
use App\Models\Organisation;
use App\Models\OrganisationArea;
use App\Models\OrganisationCategory;
use App\Models\OrganisationCategoryBrandProduct;
use App\Models\OrganisationCoupon;
use App\Models\CategoryBrand;
use App\Models\CategoryBrandProduct;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\UserDriverDetailProduct;
use App\Models\UserDriverDetailService;

class AdminOrgServiceCont extends Controller
{

///////////////////////		Products Listing Dynamically 		/////////////////
public static function admin_brand_products(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Response(['success' => 0, 'statuscode' => 400, 'msg' => $validator->getMessageBag()->first() ],400);
				

			$products = CategoryBrandProduct::brand_all_products_json($request->all());

			$con = '';
    		foreach ($products as $product) 
	    		{
					$con = $con.'<option value="'.$product->category_brand_product_id.'">'.$product->name.'</option>';
	    		}

			return Response(['success' => 1, 'statuscode' => 200, 'con' => $con, 'products' => $products ],200);

		}
	catch(\Exception $e)
		{
			return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
		}
	}

///////////////////////	Product Listings Driver Update 	///////////////////////////
public static function admin_dupdate_brands(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id',
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Response(['success' => 0, 'statuscode' => 400, 'msg' => $validator->getMessageBag()->first() ],400);
				

			$products = CategoryBrandProduct::brand_driver_products_json($request->all());

			$con = '';
    		foreach ($products as $product) 
	    		{

					if($product->selected == '1')
						$con = $con.'<option value="'.$product->category_brand_product_id.'" selected="true">'.$product->name.'</option>';
					else
						$con = $con.'<option value="'.$product->category_brand_product_id.'">'.$product->name.'</option>';

	    		}

			return Response(['success' => 1, 'statuscode' => 200, 'con' => $con, 'products' => $products ],200);

		}
	catch(\Exception $e)
		{
			return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
		}
	}

///////////////////////		Unique Driver Check 	/////////////////////////////
public static function admin_driver_unique_check(Request $request)
	{
	try{

			if(isset($request['email']))
				{

					$ocheck = User::where('email', $request['email'])->first();
					if($ocheck)
						return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this email is already taken' ],200);

				}
			elseif(isset($request['phone_number']))
				{
					
					$ocheck = User::where('phone_number', $request['phone_number'])->first();
					if($ocheck)
						return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this phone number is already taken' ],200);

				}
			elseif(isset($request['mulkiya_number']))
				{
					
					$ocheck = UserDetail::where('mulkiya_number', $request['mulkiya_number'])->first();
					if($ocheck)
						return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this licence number is already taken' ],200);

				}				

				return Response(['success' => 1, 'statuscode' => 200, 'msg' => 'Validated successfully' ],200);

		}
	catch(\Exception $e)
		{
			return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
		}
	}

///////////////////////		Update Unique Driver Check 	/////////////////////////////
public static function admin_driver_uunique_check(Request $request)
	{
	try{

		if(isset($request['email']))
			{

				$ocheck = User::where('email', $request['email'])->where('user_id', '!=' ,$request['user_id'])->first();
				if($ocheck)
					return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this email is already taken' ],200);

			}
		elseif(isset($request['phone_number']))
			{
					
				$ocheck = User::where('phone_number', $request['phone_number'])->where('user_id', '!=' ,$request['user_id'])->first();
				if($ocheck)
					return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this phone number is already taken' ],200);

			}
		elseif(isset($request['mulkiya_number']))
			{
					
				$ocheck = UserDetail::where('mulkiya_number', $request['mulkiya_number'])->where('user_id', '!=' ,$request['user_id'])->first();
				if($ocheck)
					return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this licence number is already taken' ],200);

			}				

				return Response(['success' => 1, 'statuscode' => 200, 'msg' => 'Validated successfully' ],200);

		}
	catch(\Exception $e)
		{
			return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
		}
	}



///////////////////////			Org Services    /////////////////////////////////
public static function admin_org_service_all(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$request['category_type'] = 'Service';
			$request['user_type_id'] = "2";

			$organisation = Organisation::admin_single_org_details($request->all());
			$services = OrganisationCategory::org_all_categories($request->all());

			return View::make('Admin.Orgs.Services.orgServicesAll', compact('organisation', 'services'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

///////////////////////			Driver Add Get 		//////////////////////////////
public static function admin_org_service_dadd(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
				'category_id' => 'required|exists:categories,category_id,category_type,Service'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$organisation = Organisation::admin_single_org_details($request->all());
			$brands = CategoryBrand::admin_brands_list_add_drivers($request->all());

			return View::make('Admin.Orgs.Services.Drivers.addServiceDrivers', compact('organisation', 'brands'))->with('category_id', $request['category_id']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

///////////////////////			Driver Add Post 		//////////////////////////////
public static function admin_org_service_dadd_post(Request $request)
	{
	try{

			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
				'name' => 'required',
				// 'profile_pic' => 'required|image',
				//'email' => 'required|email|unique:users,email',
				'phone_number' => 'required|unique:users,phone_number',
				'mulkiya_number' => 'required|unique:user_details,mulkiya_number',
				'mulkiya_validity' => 'required',
				// 'mulkiya_front' => 'sometimes|image',
				// 'mulkiya_back' => 'sometimes|image',
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
				'productids' => 'required|array',
				//'password' => 'required',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$category = Category::where('category_id', $request['category_id'])->first();
			$request['buraq_percentage'] = $category->buraq_percentage;
			$request['maximum_rides'] = $category->maximum_rides;

////////////////////////////		Images Upload 		////////////////////////////////
			// $request['profile_pic_name'] = CommonController::image_uploader(Input::file('profile_pic'));
			// 	if(empty($request['profile_pic_name']))
			// 		return Redirect::back()->withErrors('Sorry, profile pic not uploaded. Please try again')->withInput($request->all());
////////////////////////////		Images Upload 		////////////////////////////////
$request['profile_pic_name'] = "";
			if(!empty(Input::file('mulkiya_front')))
				{	
					$request['mulkiya_front_name'] = CommonController::image_uploader(Input::file('mulkiya_front'));
						if(empty($request['mulkiya_front_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_front_name'] = '';
				}

			if(!empty(Input::file('mulkiya_back')))
				{	
					$request['mulkiya_back_name'] = CommonController::image_uploader(Input::file('mulkiya_back'));
						if(empty($request['mulkiya_back_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_back_name'] = '';
				}

			$request['created_by'] = 'Admin';
			$request['category_type'] = 'Service';
			$request['email'] = CommonController::generate_email();

			$user = User::insert_new_record($request->all());

			$request['user_id'] = $user->user_id;
			$request['user_type_id'] = 2;//Service
			$request['mulkiya_validity'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['mulkiya_validity'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');

			$userDetail = UserDetail::insert_new_record($request->all());
			$request['user_detail_id'] = $userDetail->user_detail_id;

			UserDriverDetailService::insert_new_record($request->all());

			$con = [];
			foreach($request['productids'] as $productid)
				{

					$product = CategoryBrandProduct::where('category_brand_product_id', $productid)->first();

					$con[] = [
						'user_id' => $request['user_id'],
						'user_detail_id' => $request['user_detail_id'],
						'category_brand_product_id' => $productid,
						'alpha_price' => $product->alpha_price,
						'price_per_quantity' => $product->price_per_quantity,
						'price_per_distance' => $product->price_per_distance,
						'price_per_weight' => $product->price_per_weight,
						'price_per_hr' => $product->price_per_hr,
						'price_per_sq_mt' => $product->price_per_sq_mt,
						'deleted' => '0',
						'created_at' => new \DateTime,
						'updated_at' => new \DateTime
					];
				}

			if(!empty($con))
				UserDriverDetailProduct::insert($con);

			$request['sms'] = CommonController::send_sms([ 'phone_number' => $request['phone_number'], 'message' => env('APP_NAME').' - '.trans('messages.Congrats you are added as driver')  ]);

			return Redirect::back()->with('status','Yesser Man added successfully');

		}
	catch(Illuminate				\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////////////////		Organisation Service Drivers 		////////////////////////////////////
public static function admin_org_service_dall(Request $request)
	{
	try{

			// $rules = array(
			// 	'organisation_id' => 'required|exists:organisations,organisation_id',
			// );

			// $validator = Validator::make($request->all(),$rules);
			// if($validator->fails())
			// 	return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			// $data = CommonController::set_starting_ending($request->all());

			// $data['category_type'] = 'Service';
			// $data['user_type_ids'] = [2];

			// $organisation = Organisation::admin_single_org_details($request->all());
			// $drivers = UserDetail::admin_get_cat_drivers($data);

			// return View::make('Admin.Orgs.Services.Drivers.allDrivers', compact('organisation', 'drivers'))->with('category_id', $request['category_id'])->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$data = CommonController::set_starting_ending($request->all());

			$data['category_type'] = 'Service';
			$data['user_type_ids'] = [2];

			$organisation = Organisation::admin_single_org_details($request->all());
			$drivers = UserDetail::admin_drivers_listings($data);
			$category = Category::admin_single_cat_details($request->all());

			return View::make('Admin.Orgs.Services.Drivers.allOrgDriversNew', compact('organisation', 'drivers', 'data', 'category'))->with('category_id', $request['category_id'])->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////////////////	Driver Update Get 		/////////////////////////////////////
public static function admin_org_service_dupdate(Request $request)
	{
	try{

			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$driver = UserDetail::admin_driver_detail_update_get($request->all());

			$request['category_id'] = $driver->category_id;
			$request['organisation_id'] = $driver->organisation_id;

			// $organisation = Organisation::admin_single_org_details($request->all());
			$brands = CategoryBrand::admin_brands_list_add_drivers($request->all());

			return View::make('Admin.Orgs.Services.Drivers.updateServiceDrivers', compact('driver', 'brands'))->with('category_id', $request['category_id']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////////////////	Driver Update Post 		/////////////////////////////////////
public static function admin_org_service_dupdate_post(Request $request)
	{
	try{

			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id',
				'name' => 'required',
				'profile_pic' => 'sometimes|image',
				//'email' => 'required|email|unique:users,email,'.$request['user_id'].',user_id',
				'phone_number' => 'required|unique:users,phone_number,'.$request['user_id'].',user_id',
				'mulkiya_number' => 'required|unique:user_details,mulkiya_number,'.$request['user_detail_id'].',user_detail_id',
				'mulkiya_validity' => 'required',
				'mulkiya_front' => 'sometimes|image',
				'mulkiya_back' => 'sometimes|image',
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
				'productids' => 'required|array',
				// 'address' => 'required',
				// 'address_latitude' => 'required',
				// 'address_longitude' => 'required'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$driver = UserDetail::admin_driver_detail_update_get($request->all());

			$request['user_id'] = $driver->user_id;
			$request['organisation_id'] = $driver->organisation_id;

////////////////////////////		Images Upload 		////////////////////////////////
			if(!empty(Input::file('profile_pic')))
				{
				$request['profile_pic_name'] = CommonController::image_uploader(Input::file('profile_pic'));
					if(empty($request['profile_pic_name']))
						return Redirect::back()->withErrors('Sorry, profile pic not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['profile_pic_name'] = $driver->profile_pic;
				}
////////////////////////////		Images Upload 		////////////////////////////////

			if(!empty(Input::file('mulkiya_front')))
				{	
					$request['mulkiya_front_name'] = CommonController::image_uploader(Input::file('mulkiya_front'));
						if(empty($request['mulkiya_front_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_front_name'] = $driver->mulkiya_front;
				}

			if(!empty(Input::file('mulkiya_back')))
				{	
					$request['mulkiya_back_name'] = CommonController::image_uploader(Input::file('mulkiya_back'));
						if(empty($request['mulkiya_back_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_back_name'] = $driver->mulkiya_back;
				}

			$request['user_type_id'] = 2;//Support
			$request['mulkiya_validity'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['mulkiya_validity'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');


			User::where('user_id',$request['user_id'])->limit(1)->update([
					'name' => $request['name'],
					'phone_code' => ($request['phone_code']) ? $request['phone_code'] :  $driver->phone_code,
					'phone_number' => $request['phone_number'],
					'buraq_percentage' => isset($request['buraq_percentage']) ? $request['buraq_percentage'] : $driver->buraq_percentage,
					'bottle_charge' => isset($request['bottle_charge']) ? $request['bottle_charge'] : $driver->bottle_charge,
					'address' => isset($request['address']) ? $request['address'] : "",
					'address_latitude' => isset($request['address_latitude']) ? $request['address_latitude'] : 21.47,
					'address_longitude' => isset($request['address_longitude']) ? $request['address_longitude'] : 55.97,
					'updated_at' => new \DateTime
			]);

			UserDetail::where('user_detail_id', $request['user_detail_id'])->limit(1)->update([
				'category_brand_id' => $request['category_brand_id'],
				'profile_pic' => $request['profile_pic_name'],
				'mulkiya_number' => $request['mulkiya_number'],
				'mulkiya_front' => $request['mulkiya_front_name'],
				'mulkiya_back' => $request['mulkiya_back_name'],
				'mulkiya_validity' => $request['mulkiya_validity'],
				'updated_at' => new \DateTime
			]);

			UserDriverDetailProduct::where('user_detail_id',$request['user_detail_id'])->whereNotIn('category_brand_product_id', $request['productids'] )->update([
					'deleted' => '1',
					'updated_at' => new \DateTime
			]);

			$con = [];
			$undel = [];

			foreach($request['productids'] as $productid)
				{

				$check = UserDriverDetailProduct::where('user_detail_id', $request['user_detail_id'])->where('category_brand_product_id', $productid)->first();
				if(!$check)
					{

						$product = CategoryBrandProduct::where('category_brand_product_id',$productid)->first();

						$con[] = [
									'user_id' => $request['user_id'],
									'user_detail_id' => $request['user_detail_id'],
									'category_brand_product_id' => $productid,
									'deleted' => '0',
									'alpha_price' => $product['alpha_price'],
									'price_per_quantity' => $product['price_per_quantity'],
									'price_per_distance' => $product['price_per_distance'],
									'price_per_weight' => $product['price_per_weight'],
									'price_per_hr' => $product['price_per_hr'],
									'price_per_sq_mt' => $product['price_per_sq_mt'],
									'created_at' => new \DateTime,
									'updated_at' => new \DateTime
						];

					}
				else
					{
						$undel[] = $check->user_driver_detail_product_id;
					}

				}

			if(!empty($con))
				UserDriverDetailProduct::insert($con);

			if(!empty(($undel)))
				UserDriverDetailProduct::whereIn('user_driver_detail_product_id', $undel)->update([
					'deleted' => '0',
					'updated_at' => new \DateTime
				]);

			return Redirect::back()->with('status','Driver updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////		Org Service Areas 		/////////////////////////////////////
public static function admin_org_ser_areas(Request $request)
	{
	try{

			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			if($request['category_id'] != 2)
				return Redirect::back()->withErrors("Sorry, area selection is currently only available to drinking water service");

			$data = CommonController::set_starting_ending($request->all());

			$organisation = Organisation::admin_single_org_details($request->all());
			$areas = OrganisationArea::category_wise_org_areas($data);

			//org_service_drivers($organisation_id, $category_id, $user_type_id);

			return View::make('Admin.Orgs.Services.Areas.adminOrgSerAreas', compact('areas', 'organisation'))->with('category_id', $request['category_id'])->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

////////////////////////		Add NEw Area 		///////////////////////////////////////
public static function admin_org_area_add(Request $request)
	{
	try{

			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			if($request['category_id'] != 2)
				return Redirect::back()->withErrors("Sorry, area selection is currently only available to drinking water service");

			$organisation = Organisation::admin_single_org_details($request->all());

			$drivers = UserDetail::org_service_drivers($request['organisation_id'], $request['category_id'], 2);

			return View::make('Admin.Orgs.Services.Areas.AdminOrgSerAreaAdd', compact('organisation', 'drivers'))->with('category_id', $request['category_id']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////		Area Add Post 		/////////////////////////////////////////////////
public static function admin_org_area_padd(Request $request)
	{
	try{

			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
				'label' => 'required',
				'address' => 'required',
				'address_latitude' => 'required',
				'address_longitude' => 'required',
				'distance' => 'required',
				'drivers' => 'required'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$request['created_by'] = "Admin";

			if($request['category_id'] != 2)
				return Redirect::back()->withErrors("Sorry, area selection is currently only available to drinking water service");

			if(!isset($request['days']))
				$request['days'] = [];

			$organisation_area = OrganisationArea::insert_new_record($request->all());

///////////////////		Area Drivers 	////////////////////////////
			$con = [];
			foreach ($request['drivers'] as $key => $driver) {
				
				$con[] = [
					'organisation_area_id' => $organisation_area->organisation_area_id,
					'user_detail_id' => $driver,
					'organisation_id' => $request['organisation_id'],
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				];

			}
			if(!empty($con))
				DB::table('organisation_area_drivers')->insert($con);
///////////////////		Area Drivers 	////////////////////////////

			return Redirect::back()->with('status','Area added successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

////////////////////////		Area Update 		/////////////////////////////////////////
public static function admin_org_area_update(Request $request)
	{
	try{

			$rules = array(
				'organisation_area_id' => 'required|exists:organisation_areas,organisation_area_id'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$area = OrganisationArea::where('organisation_area_id',$request['organisation_area_id'])->first();

			$request['organisation_id'] = $area->organisation_id;
			$organisation = Organisation::admin_single_org_details($request->all());

			$drivers = UserDetail::org_service_area_drivers($request['organisation_id'], 2, 2, $request['organisation_area_id']);

			return View::make('Admin.Orgs.Services.Areas.AdminOrgSerAreaUpdate', compact('area', 'organisation', 'drivers'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

////////////////////////		Area Update 		/////////////////////////////////////////
public static function admin_org_area_pupdate(Request $request)
	{
	try{

			$rules = array(
				'organisation_area_id' => 'required|exists:organisation_areas,organisation_area_id',
				'label' => 'required',
				'address' => 'required',
				'address_latitude' => 'required',
				'address_longitude' => 'required',
				'distance' => 'required',
				'drivers' => 'required'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			if(!isset($request['days']))
				$request['days'] = [];

			OrganisationArea::update_record($request->all());

///////////////////		Area Drivers 	////////////////////////////
			DB::table('organisation_area_drivers')->where('organisation_area_id',$request['organisation_area_id'])->delete();
			$con = [];
			foreach ($request['drivers'] as $key => $driver) {
				
				$con[] = [
					'organisation_area_id' => $request['organisation_area_id'],
					'user_detail_id' => $driver,
					'organisation_id' => $request['organisation_id'],
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				];

			}
			if(!empty($con))
				DB::table('organisation_area_drivers')->insert($con);
///////////////////		Area Drivers 	////////////////////////////

			return Redirect::back()->with('status','Area updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

///////////////////////		Org Area Block Unblock 		////////////////////////////////////////
public static function admin_org_ser_area_block(Request $request)
	{
	try{

			$rules = array(
				'organisation_area_id' => 'required|exists:organisation_areas,organisation_area_id',
			);

			$validator = Validator::make($request->all(),$rules);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$area = OrganisationArea::where('organisation_area_id',$request['organisation_area_id'])->first();

			if($request['block'] == 1)
				{

					if($area->blocked == '1')
						return Redirect::back()->withErrors('This area is already blocked')->withInput();

					OrganisationArea::where('organisation_area_id',$area->organisation_area_id)->update(array(
						'blocked' => '1',
						'updated_at' =>new \DateTime
					));

					return Redirect::back()->with('status','Area blocked successfully');

				}
			else
				{

					if($area->blocked == '0')
						return Redirect::back()->withErrors('This area is already unblocked')->withInput();

					OrganisationArea::where('organisation_area_id',$area->organisation_area_id)->update(array(
						'blocked' => '0',
						'updated_at' => new \DateTime
					));

					return Redirect::back()->with('status','Area unblocked successfully');

				}

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

//////////////////////////	Coupons 		/////////////////////////////
public static function admin_org_ser_etokens(Request $request)
	{
	try{

			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$data = CommonController::set_starting_ending($request->all());

			$category = Category::admin_single_cat_details($request->all());
			$coupons = OrganisationCoupon::org_coupons_all($data);
			$organisation = Organisation::admin_single_org_details($request->all());

			return View::make('Admin.Orgs.Services.Etokens.AdminOrgETokens', compact('category', 'coupons', 'organisation'))->with('category_id', $request['category_id'])->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

////////////////////////		Etkoen Add 		///////////////////////////////////////////////
public static function admin_org_etoken_add(Request $request)
	{
	try{

			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$category = Category::admin_single_cat_details($request->all());
			$brands = CategoryBrand::admin_brands_list_add_drivers($request->all());
			$organisation = Organisation::admin_single_org_details($request->all());

		return View::make('Admin.Orgs.Services.Etokens.AdminOrgETokenAdd', compact('category', 'brands', 'organisation'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////////		Org Coupon Add Post 	//////////////////////////
public static function admin_org_etoken_padd(Request $request)
	{
	try{

			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
				'price' => 'required|min:0',
				'quantity' => 'required:min:1',
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
				'category_brand_product_id' => 'required|exists:category_brand_products,category_brand_product_id'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$request['created_by'] = 'Admin';

			$etoken = OrganisationCoupon::insert_new_record($request->all());

			return Redirect::back()->with('status','EToken added successfully');
		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////			Org Coupon Update 		//////////////////////////
public static function admin_org_etoken_update(Request $request)
	{
	try{

			$rules = array(
				'organisation_coupon_id' => 'required|exists:organisation_coupons,organisation_coupon_id'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$coupon = OrganisationCoupon::where('organisation_coupon_id', $request['organisation_coupon_id'])->first();

			$request['category_id'] = $coupon->category_id;
			$request['organisation_id'] = $coupon->organisation_id;

			$category = Category::admin_single_cat_details($request->all());
			$brands = CategoryBrand::admin_brands_list_add_drivers($request->all());
			$organisation = Organisation::admin_single_org_details($request->all());

			return View::make('Admin.Orgs.Services.Etokens.AdminOrgETokenUpdate', compact('coupon', 'category', 'brands', 'organisation'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////			Org Coupon Update Post 		//////////////////////////
public static function admin_org_etoken_pupdate(Request $request)
	{
	try{

			$rules = array(
				'organisation_coupon_id' => 'required|exists:organisation_coupons,organisation_coupon_id',
				'price' => 'required|min:0',
				'quantity' => 'required:min:1',
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
				'category_brand_product_id' => 'required|exists:category_brand_products,category_brand_product_id'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			OrganisationCoupon::update_record($request->all());

			return Redirect::back()->with('status','EToken updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}


/////////////////////////		Coupon Block Unblock 	//////////////////////////////////
public static function admin_org_coupon_block(Request $request)
	{
	try{

			$rules = array(
				'organisation_coupon_id' => 'required|exists:organisation_coupons,organisation_coupon_id'
			);

			$validator = Validator::make($request->all(),$rules);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$coupon = OrganisationCoupon::where('organisation_coupon_id',$request['organisation_coupon_id'])->first();

			if($request['block'] == 1)
				{

					if($coupon->blocked == '1')
						return Redirect::back()->withErrors('This EToken is already blocked')->withInput();

					OrganisationCoupon::where('organisation_coupon_id',$coupon->organisation_coupon_id)->update(array(
						'blocked' => '1',
						'updated_at' =>new \DateTime
					));

					return Redirect::back()->with('status','EToken blocked successfully');

				}
			else
				{

					if($coupon->blocked == '0')
						return Redirect::back()->withErrors('This EToken is already unblocked')->withInput();

					OrganisationCoupon::where('organisation_coupon_id',$coupon->organisation_coupon_id)->update(array(
						'blocked' => '0',
						'updated_at' => new \DateTime
					));

					return Redirect::back()->with('status','EToken unblocked successfully');

				}

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

///////////////////////////////////////////////////////////////
//////////////////////////		Products 		///////////////////////////////////////
///////////////////////////////////////////////////////////////
public static function admin_org_service_products(Request $request)
	{
	try{

			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$category = Category::admin_single_cat_details($request->all());
			$organisation = Organisation::admin_single_org_details($request->all());
			$org_products = OrganisationCategoryBrandProduct::org_services_products_listings($request->all());

			return View::make('Admin.Orgs.Services.Products.adminOrgSerProducts', compact('category', 'organisation', 'org_products'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////		Product Price Update 		/////////////////////////////////
public static function admin_org_ser_product_update(Request $request)
	{
	try{

			$rules = array(
				'organisation_category_brand_product_id' => 'required|exists:organisation_category_brand_products',
				'alpha_price' => 'required|min:0',
				'price_per_quantity' => 'required|min:0',
				'price_per_distance' => 'required|min:0',
				'price_per_weight' => 'required|min:0',
				'price_per_hr' => 'required|min:0',
				'price_per_sq_mt' => 'required|min:0'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			OrganisationCategoryBrandProduct::where('organisation_category_brand_product_id',$request['organisation_category_brand_product_id'])->limit(1)->update([
					'alpha_price' => $request['alpha_price'],
					'price_per_quantity' => $request['price_per_quantity'],
					'price_per_distance' => $request['price_per_distance'],
					'price_per_weight' => $request['price_per_weight'],
					'price_per_hr' => $request['price_per_hr'],
					'price_per_sq_mt' => $request['price_per_sq_mt'],
					'updated_at' => new \DateTime
			]);

			return Redirect::back()->with('status','Product price updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

///////////////////////////////////////////////////////////////
//////////////////////////		Products 		///////////////////////////////////////
///////////////////////////////////////////////////////////////

/////////////////////		Update Org Cat Percentage 	///////////////////////////////////
public static function admin_org_cat_percent(Request $request)
	{
	try{

			$rules = array(
				'organisation_category_id' => 'required|exists:organisation_categories,organisation_category_id',
				'organisation_id' => 'required|exists:organisations,organisation_id',
				//'buraq_percentage' => 'required|min:0|max:100'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			// OrganisationCategory::where('organisation_category_id', $request['organisation_category_id'])->limit(1)->update([
			// 		'buraq_percentage' => $request['buraq_percentage'],
			// 		'updated_at' => new \DateTime
			// ]);

			if(isset($request['bottle_charge']))
				{
					Organisation::where('organisation_id', $request['organisation_id'])->update([
						'bottle_charge' => $request['bottle_charge'],
						'updated_at' => new \DateTime
					]);
				}

			return Redirect::back()->with('status','Buraq percentage updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////


}
