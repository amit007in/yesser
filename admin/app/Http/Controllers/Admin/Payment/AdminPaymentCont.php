<?php

namespace App\Http\Controllers\Admin\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;

use App\Http\Controllers\CommonController;

use App\Models\Payment;
use App\Models\Order;
use App\Models\OrganisationCouponUser;
use App\Models\CouponUser;

class AdminPaymentCont extends Controller
{
////////////////////////	Service All Payments    ////////////////////////////////////
public static function admin_ser_pay_all(Request $request)
	{
	try{

			$data = CommonController::set_starting_ending($request->all());

			$data['seller_user_type_id'] = 2;

			$payments = Payment::admin_orders_payment($data);
		
			$psums = Payment::admin_ser_pay_sums($data);

			return View::make("Admin.Payments.serPayments", compact('payments', 'psums'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

////////////////////////	Support All Payments    ////////////////////////////////////
public static function admin_sup_pay_all(Request $request)
	{
	try{

			$data = CommonController::set_starting_ending($request->all());

			$data['seller_user_type_id'] = 3;

			$payments = Payment::admin_orders_payment($data);
			$psums = Payment::admin_ser_pay_sums($data);

			return View::make("Admin.Payments.supPayments", compact('payments', 'psums'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

//////////////////////////		Coupon Payments All 		////////////////////////////
public static function admin_coupon_pay_all(Request $request)
	{
	try{

			$data = CommonController::set_starting_ending($request->all());

			$data['seller_user_type_id'] = 5;

			$payments = CouponUser::admin_coupon_payments($data);
			$psums = CouponUser::admin_coupon_pay_sums($data);

			return View::make("Admin.Payments.couponPayments", compact('payments', 'psums'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

////////////////////////		Etokens Payments All 	////////////////////////////////
public static function admin_etoken_pay_all(Request $request)
	{
	try{

			$data = CommonController::set_starting_ending($request->all());
			// $payments = OrganisationCouponUser::admin_etoken_payments($data);
			//$psums = CouponUser::admin_coupon_pay_sums($data);

			$payments = Payment::admin_etoken_payments($data);
			$psums = Payment::admin_etoken_pay_sums($data);

			return View::make("Admin.Payments.eTokenPayments", compact('payments', 'psums'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

///////////////////////

}
