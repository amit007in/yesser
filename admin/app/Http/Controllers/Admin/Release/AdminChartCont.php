<?php

namespace App\Http\Controllers\Admin\Release;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Mail;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Illuminate\Cookie\CookieJar;
use Request as ORequest;
use App\Http\Controllers\CommonController;
use App\Models\Admin;
use App\Models\AdminUserDetailLogin;
use App\Models\Category;
use App\Models\Order;
use App\Models\UserType;
use App\Models\ReleaseOffer;
class AdminChartCont extends Controller
{
	///////////// Get All Chart Analysis ///////////////////////
	public static function admin_chartanalysis(Request $request)
	{
		try
		{
			$data = CommonController::set_starting_ending($request->all());
			if(!isset($data['categories']))
				$data['categories'] = [0];
			$categories = Category::all_categories_single_lang($data['admin_language_id'], implode(',', $data['categories']));
			return View::make('Admin.Chart.chart', compact('categories'));
		}
		catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}
	
	///////////// Get Total Order Chart Analysis ///////////////////////
	public static function admin_chartfilter(Request $request)
	{
		try
		{
			$ArrayMain   = array();
			$NewCatArray = array();
			$array       = array();
			$Categoryname       = array(); //Get Quartly Order
			$TotalOrders = [];
			$category = DB::table('categories')
				->join('category_details', 'categories.category_id', '=', 'category_details.category_id')
				->select('category_details.name','category_details.category_id')
				->where('categories.blocked','0')
				->where('category_details.language_id',$request['admin_language_id'])
				->get()->toArray();
			for($i =0;$i<count($category);$i++)
			{
				$categoryId    = $category[$i]->category_id;
				$orders        = DB::table('orders')->where('category_id',$categoryId)->count();
				$array['name'] = $category[$i]->name;
				array_push($Categoryname,$array['name']); //Get Quartly Order
				$array['y']    = $orders;
				if($i == 0)
				{
					$array['selected'] = "true";
				}else{
					$array['selected'] = "false";
				}
				$TotalOrders      = AdminChartCont::getTotalOrdersMonths($request->all(),$categoryId);
				
				$CatArray['name'] = $category[$i]->name;
				$CatArray['y'] = $TotalOrders;
				$NewCatArray[]    = $CatArray;
				$ArrayMain[]      = $array;
			}
			$TotalOrderss     = AdminChartCont::getTotalOrdersQuartly(); //Get Quartly Order

			$NewArray['CategoryOrder']        = $ArrayMain;
			$NewArray['TotalOrders']          = $NewCatArray;
			$NewArray['TotalQuartlyOrders']   = $TotalOrderss; //Get Quartly Order
			$NewArray['Categoryname']         = $Categoryname; //Get Quartly Order
			
			return $NewArray;
		}
		catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}
	
	/////////////////////		Graph Data 		/////////////////////////////////////
	public static function getTotalOrdersMonths($data,$id)
	{
		$yr_only = Carbon::now($data['timezonez'])->format('Y');
		$querys = DB::SELECT("SELECT
						months_tbl.id as id,
		CAST((SELECT COUNT(*) FROM user_details as ud WHERE ud.user_type_id=1 AND YEAR(ud.created_at) = :yr1 AND MONTH(ud.created_at) = months_tbl.id) AS UNSIGNED) as customers,
		CAST((SELECT COUNT(*) FROM user_details as ud WHERE ud.user_type_id=2 AND YEAR(ud.created_at) = :yr2 AND MONTH(ud.created_at) = months_tbl.id) AS UNSIGNED) as dservices,
		CAST((SELECT COUNT(*) FROM user_details as ud WHERE ud.user_type_id=3 AND YEAR(ud.created_at) = :yr3 AND MONTH(ud.created_at) = months_tbl.id) AS UNSIGNED) as dsupports,
		CAST((SELECT COUNT(*) FROM orders as o WHERE YEAR(o.order_timings) = :yr4 AND MONTH(o.order_timings) = months_tbl.id  AND o.category_id=".$id.") AS UNSIGNED) as orders,
		CAST((SELECT COUNT(*) FROM organisations as o WHERE YEAR(o.created_at) = :yr5 AND MONTH(o.created_at) = months_tbl.id) AS UNSIGNED) as orgs,
		CAST((SELECT COUNT(*) FROM order_ratings WHERE YEAR(created_at) = :yr6 AND MONTH(created_at) = months_tbl.id) AS UNSIGNED) as reviews
							FROM months_tbl
		",['yr1'=>$yr_only, 'yr2'=>$yr_only, 'yr3'=>$yr_only, 'yr4'=>$yr_only, 'yr5'=>$yr_only, 'yr6'=>$yr_only]);

		$customers = $dservices = $dsupports = $orders = $orgs = $reviews = [];

		foreach($querys as $query)
		{
			array_push($orders, $query->orders);
		}
		$totalorders = array_sum($orders);
		return $totalorders;
	}
	
	
	/////////////////////		Graph Data 		/////////////////////////////////////
	public static function getTotalOrdersQuartly()
	{
		$currentYear = Carbon::now()->format('Y');
		//First Quater
		$Q1start=Carbon::createMidnightDate($currentYear,1,1)->format('Y-m-d');
		$Q1end=Carbon::createMidnightDate($currentYear,3,31)->format('Y-m-d');
		//Second Quater
		$Q2start=Carbon::createMidnightDate($currentYear,4,1)->format('Y-m-d');
		$Q2end=Carbon::createMidnightDate($currentYear,6,30)->format('Y-m-d');
		//Third Quater
		$Q3start=Carbon::createMidnightDate($currentYear,7,1)->format('Y-m-d');
		$Q3end=Carbon::createMidnightDate($currentYear,9,30)->format('Y-m-d');
		//Fourth Quater
		$Q4start=Carbon::createMidnightDate($currentYear,10,1)->format('Y-m-d');
		$Q4end=Carbon::createMidnightDate($currentYear,12,31)->format('Y-m-d');

			
			$ArrayMain   = array();
			$NewCatArray = array();
			$NewCatArray1 = array();
			$array       = array();
			//$TotalOrders = [];
			$CatArray    = [];
			$CatArray1    = [];
			$category = DB::table('categories')
				->join('category_details', 'categories.category_id', '=', 'category_details.category_id')
				->select('category_details.name','category_details.category_id')
				->where('categories.blocked','0')
				->where('category_details.language_id','1')
				->get()->toArray();
			for($j =0;$j<4;$j++)
			{
				if($j == 0)
				{
					$startDate = $Q1start;
					$endDate   = $Q1end;
				}elseif($j == 1)
				{
					$startDate = $Q2start;
					$endDate   = $Q2end;
				}elseif($j == 2)
				{
					$startDate = $Q3start;
					$endDate   = $Q3end;
				}else{
					$startDate = $Q4start;
					$endDate   = $Q4end;
				}
				$Totalorders = [];
				for($i =0;$i<count($category);$i++)
				{
					$CatArray = [];
					$categoryId    = $category[$i]->category_id;
					$array['name'] = $category[$i]->name;
					$ArrayMain[]      = $array;
					$FirstQorders = DB::table('orders')
						->select(DB::raw('COUNT(orders.order_id) as totalorders'))
						->whereBetween(DB::raw('DATE(created_at)'), array($startDate, $endDate))
						->where('category_id',$categoryId)
						->get();
					$CatArray[]               = $FirstQorders[0]->totalorders;
					
					$CatArray1['name']        = $category[$i]->name;
					//$CatArray1['TotalOrders'] = $CatArray;
					$NewCatArray[] = $CatArray1;
					array_push($Totalorders, $CatArray[0]);
				}
				$NewCatArray1[] = $Totalorders;
			}
			
			//$NewArray['TotalOrders']   = $NewCatArray1;
			return $NewCatArray1;
	}
	
	
	
	
	///////////// Get Total Order Chart Analysis ///////////////////////
	public static function admin_orderTypeChart(Request $request)
	{
		try
		{
			$Array      = array();
			$CategoryId = $request['category'];
			$category   = DB::table('category_details')
				->select('category_details.name')
				->where('category_details.language_id',$request['admin_language_id'])
				->get()->toArray();
			$TotalScheduled = DB::table('orders')
                 ->select(DB::raw('count(*) as y'))
				 ->where('future','1')
				 ->where('category_id',$CategoryId)
                 ->count();
		    $TotalOnDemand = DB::table('orders')
                 ->select(DB::raw('count(*) as y'))
				 ->where('future','0')
				 ->where('category_id',$CategoryId)
                 ->count();
			 $Array['sorder'] = $TotalScheduled;
			 $Array['dorder'] = $TotalOnDemand;
			 $graph_data = AdminChartCont::new_graph_data($request->all());
			 $product_data = AdminChartCont::getProducts($request->all());
			 $brands_data = AdminChartCont::getBrandsData($request->all());
			 $brands_data_product = AdminChartCont::getBrandsDataWithProduct($request->all());
			 
			 $Array['products'] = $product_data;
			 $Array['orders']   = $graph_data;
			 $Array['Brands']   = $brands_data;
			 $Array['CategoryRevenue']   = $brands_data_product;
			 return $Array;
		}
		catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}
	/////// Get Monthly Buraq24 Data Analysis///////
	public static function getBrandsDataWithProduct($data)
	{
		//$from   = Carbon::now()->startOfMonth()->subMonth()->toDateString();
		//$to     = Carbon::now()->endOfMonth()->subMonth()->toDateString();
		$from = Carbon::now()->subMonth()->toDateString();
		$to   = Carbon::now()->toDateString();
		$orders = DB::table('orders')
		->select(array(DB::raw('DATE(created_at) as OrderCreate'), DB::raw('COUNT(orders.order_id) as totalorders')))
		->whereBetween(DB::raw('DATE(created_at)'), array($from, $to))
		->where('category_id',$data['category'])
		->groupBy(DB::raw('DATE(created_at)'))
		->get()
		->toArray();
		$ArrayMain         = array();
		$NewOrderProductArray = array();
		$array             = array();
		$Totalorders       = [];
		$payments          = [];
		$Dates             = [];
		$ProductName             = [];
		
		for($i =0;$i<count($orders);$i++)
		{
			$time       = $orders[$i]->OrderCreate;
			$categoryId = $data['category'];
			$payment    = DB::table('order_products')
			->where(DB::raw('DATE(created_at)'), $time)
			->where('category_id',$categoryId)
			->sum('price_per_item');
			$totalorder            = $orders[$i]->totalorders;
			array_push($payments, (float)number_format($payment,2));
			array_push($Totalorders, $totalorder);
			array_push($Dates, $time);
		}
		//Order Array
		$OrderChartArray         = array();
		$OrderChartArray['name'] = 'Orders'; 
		$OrderChartArray['type'] = 'column'; 
		$OrderChartArray['data'] = $Totalorders;
		$NewOrderProductArray[]  = $OrderChartArray;
		//Payment Array
		$PaymentChartArray         = array();
		$PaymentChartArray['name'] = 'Revenue'; 
		$PaymentChartArray['type'] = 'spline'; 
		$PaymentChartArray['data'] = $payments;
		$NewOrderProductArray[]    = $PaymentChartArray;
		//Start Product Wise
		$categoryProducts = DB::table('category_brand_product_details')
			->join('category_brand_products', 'category_brand_products.category_brand_product_id', '=', 'category_brand_product_details.category_brand_product_id')
			->select('category_brand_product_details.name','category_brand_product_details.category_brand_product_id')
			->where('category_brand_products.blocked','0')
			->where('category_brand_products.category_id',$data['category'])
			->where('category_brand_products.category_brand_id',$data['brandids'])
			->where('category_brand_product_details.language_id',$data['admin_language_id'])
			->get()->toArray();
		for($j =0;$j<count($categoryProducts);$j++)
		{ 
			$ProductOrders             = [];
			$categoryProductId  = $categoryProducts[$j]->category_brand_product_id;
			for($i =0;$i<count($orders);$i++)
			{
				$time       = $orders[$i]->OrderCreate;
				$ordersProductCount = DB::table('order_products')
					->where('category_id',$data['category'])
					->where('category_brand_id',$data['brandids'])
					->where('category_brand_product_id',$categoryProductId)
					->where(DB::raw('DATE(created_at)'), $time)
					->count();
				array_push($ProductOrders, $ordersProductCount);
			}
			$array['name']      = $categoryProducts[$j]->name;
			$array['type']      = 'column';
			$array['data']      = $ProductOrders;
			$NewOrderProductArray[] = $array;
		}
		//End Product Wise
		
		$ArrayMain = array('Orders'=> $NewOrderProductArray,'Dates'=>$Dates);
		return $ArrayMain;
		
	}
	
	////// Get Brands Wise Data//////////
	public static function getBrandsData($data)
	{
		$ArrayMain = array();
		$array     = array();
		$categoryBrands = DB::table('category_brand_details')
		->select('category_brand_details.*')
		->where('category_brand_details.category_id',$data['category'])
		->where('category_brand_details.language_id',$data['admin_language_id'])
		->get()
		->toArray();
		
		for($i =0;$i<count($categoryBrands);$i++)
		{ 
			$categoryId       = $categoryBrands[$i]->category_id;
			$categoryBrandsId = $categoryBrands[$i]->category_brand_id;
			$categoryBrandsDetails = DB::table('category_brands')->select('*')
			->where('category_brand_id',$categoryBrandsId)
			->where('blocked','0')
			->get()
			->toArray();
			if(!empty($categoryBrandsDetails))
			{
				$yr_only = Carbon::now($data['timezonez'])->format('Y');
				$querys = DB::SELECT("SELECT
								months_tbl.id as id,
				CAST((SELECT COUNT(*) FROM user_details as ud WHERE ud.user_type_id=1 AND YEAR(ud.created_at) = :yr1 AND MONTH(ud.created_at) = months_tbl.id) AS UNSIGNED) as customers,
				CAST((SELECT COUNT(*) FROM user_details as ud WHERE ud.user_type_id=2 AND YEAR(ud.created_at) = :yr2 AND MONTH(ud.created_at) = months_tbl.id) AS UNSIGNED) as dservices,
				CAST((SELECT SUM(price_per_item) FROM order_products as ud WHERE YEAR(ud.created_at) = :yr3 AND MONTH(ud.created_at) = months_tbl.id AND ud.category_id=".$categoryId." AND ud.category_brand_id=".$categoryBrandsId.") AS UNSIGNED) as dsupports,
				CAST((SELECT COUNT(*) FROM orders as o WHERE YEAR(o.order_timings) = :yr4 AND MONTH(o.order_timings) = months_tbl.id  AND o.category_id=".$categoryId." AND o.category_brand_id=".$categoryBrandsId.") AS UNSIGNED) as orders,
				CAST((SELECT COUNT(*) FROM organisations as o WHERE YEAR(o.created_at) = :yr5 AND MONTH(o.created_at) = months_tbl.id) AS UNSIGNED) as orgs,
				CAST((SELECT COUNT(*) FROM order_ratings WHERE YEAR(created_at) = :yr6 AND MONTH(created_at) = months_tbl.id) AS UNSIGNED) as reviews
									FROM months_tbl
				",['yr1'=>$yr_only, 'yr2'=>$yr_only, 'yr3'=>$yr_only, 'yr4'=>$yr_only, 'yr5'=>$yr_only, 'yr6'=>$yr_only]);

					$orders = [];
					$payments = [];
					
					foreach($querys as $query)
					{
						array_push($orders, $query->orders);
						array_push($payments,$query->dsupports);
					}
					$array['name']    = $categoryBrands[$i]->name;
					$array['data']    = $payments;
					$ArrayMain[]      = $array;
			}
		}
		return $ArrayMain;
	}
	
	////// Get All Products Data ////////////
	public static function getProducts($data)
	{
		$ArrayMain = array();
		$array     = array();
		$categoryProducts = DB::table('category_brand_product_details')
				->join('category_brand_products', 'category_brand_products.category_brand_product_id', '=', 'category_brand_product_details.category_brand_product_id')
				->select('category_brand_product_details.name','category_brand_product_details.category_brand_product_id')
				->where('category_brand_products.blocked','0')
				->where('category_brand_products.category_id',$data['category'])
				->where('category_brand_products.category_brand_id',$data['brandids'])
				->where('category_brand_product_details.language_id',$data['admin_language_id'])
				->get()->toArray();
		for($i =0;$i<count($categoryProducts);$i++)
		{ 
			$categoryProductId       = $categoryProducts[$i]->category_brand_product_id;
			$orders = DB::table('order_products')
					->where('category_id',$data['category'])
					->where('category_brand_id',$data['brandids'])
					->where('category_brand_product_id',$categoryProductId)
					->count();
			$array['name']    = $categoryProducts[$i]->name;
			$array['y']       = $orders;
			if($i == 0)
			{
				$array['selected']       = "true";
				$array['sliced']       = "true";
			}else{
				$array['selected']       = "false";
				$array['sliced']       = "false";
			}
			$ArrayMain[]      = $array;
		}
		return $ArrayMain;
	}
	/////////////////////		Graph Data 		/////////////////////////////////////
	public static function new_graph_data($data)
	{
		$yr_only = Carbon::now($data['timezonez'])->format('Y');
		$querys = DB::SELECT("SELECT
						months_tbl.id as id,
		CAST((SELECT COUNT(*) FROM user_details as ud WHERE ud.user_type_id=1 AND YEAR(ud.created_at) = :yr1 AND MONTH(ud.created_at) = months_tbl.id) AS UNSIGNED) as customers,
		CAST((SELECT COUNT(*) FROM user_details as ud WHERE ud.user_type_id=2 AND YEAR(ud.created_at) = :yr2 AND MONTH(ud.created_at) = months_tbl.id) AS UNSIGNED) as dservices,
		CAST((SELECT COUNT(*) FROM user_details as ud WHERE ud.user_type_id=3 AND YEAR(ud.created_at) = :yr3 AND MONTH(ud.created_at) = months_tbl.id) AS UNSIGNED) as dsupports,
		CAST((SELECT COUNT(*) FROM orders as o WHERE YEAR(o.order_timings) = :yr4 AND MONTH(o.order_timings) = months_tbl.id  AND o.category_id=".$data['category'].") AS UNSIGNED) as orders,
		CAST((SELECT COUNT(*) FROM organisations as o WHERE YEAR(o.created_at) = :yr5 AND MONTH(o.created_at) = months_tbl.id) AS UNSIGNED) as orgs,
		CAST((SELECT COUNT(*) FROM order_ratings WHERE YEAR(created_at) = :yr6 AND MONTH(created_at) = months_tbl.id) AS UNSIGNED) as reviews
							FROM months_tbl
		",['yr1'=>$yr_only, 'yr2'=>$yr_only, 'yr3'=>$yr_only, 'yr4'=>$yr_only, 'yr5'=>$yr_only, 'yr6'=>$yr_only]);

			$customers = $dservices = $dsupports = $orders = $orgs = $reviews = [];

			foreach($querys as $query)
			{
				array_push($customers, $query->customers);
				array_push($dservices, $query->dservices);
				array_push($dsupports, $query->dsupports);
				array_push($orders, $query->orders);
				array_push($orgs, $query->orgs);
				array_push($reviews, $query->reviews);
			}
		return $orders;
	}
	
	public static function generateDateRange($start_date,$end_date)
	{
		$dates = [];

		for($date = $start_date->copy(); $date->lte($end_date); $date->addDay()) {
			$dates[] = $date->format('Y-m-d');
		}

		return $dates;
	}
	////////////Get All Reg Users each day/////////////
	public static function newRegistrationRate(Request $request)
	{
		if(!isset($request['users']))
	        $yr_only = '1';
	    else
	        $yr_only  = $request['users'];
		$from = Carbon::now()->subMonth()->toDateString();
		$to   = Carbon::now()->toDateString();
		if($yr_only == '3'){
			$users = DB::table('users')
			->join('user_details', 'user_details.user_id', '=', 'users.user_id')
			->select(array(DB::raw('DATE(users.created_at) as BuraqUser'), DB::raw('COUNT(users.user_id) as totalUsers')))
			->whereBetween(DB::raw('DATE(users.created_at)'), array($from, $to))
			->where('user_details.user_type_id','2')
			->where('users.organisation_id','!=','0')
			->groupBy(DB::raw('DATE(users.created_at)'))
			->get()
			->toArray();
		}
		else
		{
			$users = DB::table('users')
			->join('user_details', 'user_details.user_id', '=', 'users.user_id')
			->select(array(DB::raw('DATE(users.created_at) as BuraqUser'), DB::raw('COUNT(users.user_id) as totalUsers')))
			->whereBetween(DB::raw('DATE(users.created_at)'), array($from, $to))
			->where('user_details.user_type_id',$yr_only)
			->groupBy(DB::raw('DATE(users.created_at)'))
			->get()
			->toArray();
		}
		$dates = AdminChartCont::getAllDates($from,$to);
		$Dates = [];
		for($i =0;$i<count($users);$i++)
		{
			$time       = $users[$i]->BuraqUser;
			array_push($Dates, $time);
		}
	
		for($j =0;$j<count($dates);$j++)
		{
			if (!in_array($dates[$j], $Dates))
			{
				$myArray = (object) array('BuraqUser' =>$dates[$j],'totalUsers'=>'0');
				array_push($users,$myArray);
			}
		}
		$array  = [];
		$array1 = [];
		for($j =0;$j<count($users);$j++)
		{
			$array['date'] = $users[$j]->BuraqUser;
			$array['Total'] = $users[$j]->totalUsers;
			$array1[] = $array;
		}
		usort(
			$array1,
			function($a, $b)
			{
				$da = strtotime($a['date']);
				$db = strtotime($b['date']);
				return $da > $db; // use "<" to reverse sort
			}
		);
		//Calculate Percentage
		$calulation = [];
		$TotalUsers = [];
		for($k =0;$k<count($array1);$k++)
		{
			if($k == 0)
			{
				$Pday = (int)$array1[$k]['Total']; 
			}else{
				$Pday = (int)$array1[$k - 1]['Total']; 
			}
			$cR = (int)$array1[$k]['Total'];
			if($Pday == 0 || $cR == 0){
				$cal = 0;
			}
			else
			{
				$cal = ($cR - $Pday)/$Pday;
			}
			array_push($calulation,round($cal, 2));
			array_push($TotalUsers,$cR);
		}
		//Get Login Users
		$LoginUsers = AdminChartCont::getLoginUsers($request->all());
		$ArrayMain = array('Users'=> $calulation,'Dates'=>$dates,'TotalUsers'=>$TotalUsers,'LoginUsers'=>$LoginUsers);
		return $ArrayMain;
		
	}
	
	///////// Get All Login Users/////////
	public static function getLoginUsers($data)
	{
		if(!isset($data['users']))
	        $yr_only = '1';
	    else
	        $yr_only  = $data['users'];
		
		$from = Carbon::now()->subMonth()->toDateString();
		$to   = Carbon::now()->toDateString();
		if($yr_only == '1')
		{
			$users = DB::table('user_logins')
			->select(array(DB::raw('DATE(login_at) as BuraqUser'), DB::raw('COUNT(user_id) as totalUsers')))
			->whereBetween(DB::raw('DATE(login_at)'), array($from, $to))
			->where('user_type_id',$yr_only)
			//->where('access_token','!=','')
			->groupBy(DB::raw('DATE(login_at)'))
			->get()
			->toArray();
		}
		else if($yr_only == '2')
		{
			/*$users = DB::table('user_details')
			->select(array(DB::raw('DATE(updated_at) as BuraqUser'), DB::raw('COUNT(user_detail_id) as totalUsers')))
			->whereBetween(DB::raw('DATE(updated_at)'), array($from, $to))
			->where('user_type_id',$yr_only)
			->where('online_status','1')
			->groupBy(DB::raw('DATE(updated_at)'))
			->get()
			->toArray();*/
			$users = DB::table('user_logins')
			->select(array(DB::raw('DATE(login_at) as BuraqUser'), DB::raw('COUNT(user_id) as totalUsers')))
			->whereBetween(DB::raw('DATE(login_at)'), array($from, $to))
			->where('user_type_id',$yr_only)
			//->where('access_token','!=','')
			->groupBy(DB::raw('DATE(login_at)'))
			->get()
			->toArray();
		}else
		{
			$users = DB::table('user_details')
			->join('users', 'user_details.user_id', '=', 'users.user_id')
			->select(array(DB::raw('DATE(user_details.updated_at) as BuraqUser'), DB::raw('COUNT(user_details.updated_at) as totalUsers')))
			->whereBetween(DB::raw('DATE(user_details.updated_at)'), array($from, $to))
			->where('user_details.user_type_id','2')
			->where('user_details.online_status','1')
			->where('users.organisation_id','!=','0')
			->groupBy(DB::raw('DATE(user_details.updated_at)'))
			->get()
			->toArray();
		}
		$dates = AdminChartCont::getAllDates($from,$to);
		$Dates = [];
		for($i =0;$i<count($users);$i++)
		{
			$time       = $users[$i]->BuraqUser;
			array_push($Dates, $time);
		}
		for($j =0;$j<count($dates);$j++)
		{
			if (!in_array($dates[$j], $Dates))
			{
				$myArray = (object) array('BuraqUser' =>$dates[$j],'totalUsers'=>'0');
				array_push($users,$myArray);
			}
		}
		//Sort User On Basis Of dates
		$array     = [];
		$arrayUser = [];
		for($j =0;$j<count($users);$j++)
		{
			$array['date'] = $users[$j]->BuraqUser;
			$array['Total'] = $users[$j]->totalUsers;
			$arrayUser[] = $array;
		}
		usort(
			$arrayUser,
			function($a, $b)
			{
				$da = strtotime($a['date']);
				$db = strtotime($b['date']);
				return $da > $db;
			}
		);
		//Calculate Percentage
		$calulation  = [];
		$TotalUsers  = [];
		$ActiveUsers = [];
		for($k =0;$k<count($arrayUser);$k++)
		{
			if($k == 0)
			{
				$Pday = (int)$arrayUser[$k]['Total']; 
			}else{
				$Pday = (int)$arrayUser[$k - 1]['Total']; 
			}
			$cR = (int)$arrayUser[$k]['Total'];
			if($Pday == 0 || $cR == 0){
				$cal = 0;
			}
			else
			{
				$cal = ($cR - $Pday)/$Pday;
			}
			array_push($calulation,round($cal, 2));
			array_push($TotalUsers,$cR);
		}
		return array('Percentage'=>$calulation,'TotalUsers'=>$TotalUsers);
	}
	
	////Common Function For All Dates Between////
	public static function getAllDates($from,$to)
	{
		$Startfrom = Carbon::parse($from);
		$Endto     = Carbon::parse($to);
		$dates = [];
		for($d = $Startfrom; $d->lte($Endto); $d->addDay()) {
			$dates[] = $d->format('Y-m-d');
		}
		return $dates;
	}
	
	/////////////////////		Graph Data 		/////////////////////////////////////
	public static function getUserTotalOrdersMonths(Request $request)
	{
		//$data = CommonController::set_starting_ending($request->all());
		//$daterange = $request['daterange'];
		$CategoryId        = $request['category'];
		//echo $CategoryId; die;
		if(!isset($request['daterange']))
	        $yr_only = Carbon::now($data['timezonez'])->format('Y');
	    else
	        $yr_only  = $request['daterange'];
		//$yr_only = Carbon::now($request['timezonez'])->format('Y');
		$querys = DB::SELECT("SELECT
						months_tbl.id as id,
		CAST((SELECT COUNT(DISTINCT(user_id)) FROM user_logins as ud WHERE ud.user_type_id=1 AND YEAR(ud.created_at) = :yr1 AND MONTH(ud.created_at) = months_tbl.id) AS UNSIGNED) as activecustomers,
		CAST((SELECT COUNT(*) FROM user_details as ud WHERE ud.user_type_id=1 AND YEAR(ud.created_at) = :yr2 AND MONTH(ud.created_at) = months_tbl.id) AS UNSIGNED) as totalcustomers,
		CAST((SELECT COUNT(*) FROM user_details as ud WHERE ud.user_type_id=3 AND YEAR(ud.created_at) = :yr3 AND MONTH(ud.created_at) = months_tbl.id) AS UNSIGNED) as dsupports,
		CAST((SELECT COUNT(*) FROM orders as o WHERE YEAR(o.order_timings) = :yr4 AND MONTH(o.order_timings) = months_tbl.id  AND o.category_id=".$CategoryId.") AS UNSIGNED) as orders,
		CAST((SELECT COUNT(*) FROM organisations as o WHERE YEAR(o.created_at) = :yr5 AND MONTH(o.created_at) = months_tbl.id) AS UNSIGNED) as orgs,
		CAST((SELECT COUNT(*) FROM order_ratings WHERE YEAR(created_at) = :yr6 AND MONTH(created_at) = months_tbl.id) AS UNSIGNED) as reviews
							FROM months_tbl
		",['yr1'=>$yr_only, 'yr2'=>$yr_only, 'yr3'=>$yr_only, 'yr4'=>$yr_only, 'yr5'=>$yr_only, 'yr6'=>$yr_only]);

		$totalcustomers = $customers = $dservices = $dsupports = $orders = $orgs = $reviews = [];

		foreach($querys as $query)
		{
			array_push($orders, $query->orders);
			array_push($customers, $query->activecustomers);
			array_push($totalcustomers, $query->totalcustomers);
		}
		return array('Activecustomers'=> $customers, 'orders' => $orders, 'TotalCustomer' => $totalcustomers);
	}
	
	//// Get All User Details ///
	public static function getUserOrderCustomerList(Request $request)
	{
		$month   = DB::table('months_tbl')
				->select('id')
				->where('short_name',$request['month'])
				->get()->toArray();
		if($month[0]->id > 10)
		{
			$monthid = $month[0]->id;
		}else{
			$monthid = '0'.$month[0]->id;
		}
		//Get All Custmoer With ORder Count
		$users = DB::table('orders')
			->join('users', 'orders.customer_user_id', '=', 'users.user_id')
			->select(array(DB::raw('users.name as customers'), DB::raw('COUNT(orders.order_id) as orders'),DB::raw('users.user_id as UserId')))
			->where(DB::raw('MONTH(orders.order_timings)'), $monthid)
			->where(DB::raw('YEAR(orders.order_timings)'), $request['year'])
			->where('category_id',$request['categories'])
			->groupBy(DB::raw('orders.customer_user_id'))
			->get()
			->toArray();
		$TotalCusomter = array();
		foreach($users as $user)
		{
			$Array['id'] = $user->UserId;
			$Array['Username'] = $user->customers;
			$Array['TotalOrder'] = $user->orders;
			$TotalCusomter[] = $Array;
		}
		
		array_multisort(array_map(function($TotalCusomter) {
		  return $TotalCusomter['TotalOrder'];
		}, $TotalCusomter), SORT_DESC, $TotalCusomter);
		return $TotalCusomter;
	}
	
}
