<?php

namespace App\Http\Controllers\Admin\Release;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Mail;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Illuminate\Cookie\CookieJar;
use Request as ORequest;
use App\Http\Controllers\CommonController;
use App\Models\Admin;
use App\Models\AdminUserDetailLogin;
use App\Models\Order;
use App\Models\UserType;
use App\Models\ReleaseOffer;
class AdminReleaseCont extends Controller
{
	///////////// Get All Release Offer ///////////////////////
	public static function admin_release_offer(Request $request)
	{
		try
		{
			$data   = CommonController::set_starting_ending($request->all());
			$offers = ReleaseOffer::admin_release_offer_listing($data);
			return View::make('Admin.SentOffer.sent', compact('offers'));
		}
		catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

	//////////////////////		Approve Reject Offer 		////////////////////////////////////
	public static function admin_approve_offer(Request $request)
	{
		try{
			$rules = array(
				'id' => 'required',
				'approve' => 'required|in:Approve,Reject'
			);
			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$driver = ReleaseOffer::where('release_offer_id',$request['id'])->first();
			if($request['approve'] == "Approve")
			{
				if($driver->status == "Approved")
					return Redirect::back()->withErrors('This offer is already approved')->withInput();
				ReleaseOffer::where('release_offer_id',$request['id'])->update(array(
					'status' => "Approved",
					'updated_at' =>new \DateTime
				));
				
				$ch = curl_init();
				//curl_setopt($ch, CURLOPT_URL, "http://phytotherapy.in:8051/service/other/SendOffertoCustomer");
				curl_setopt($ch, CURLOPT_URL, env('SOCKET_URL')."/service/other/SendOffertoCustomer");
				curl_setopt($ch, CURLOPT_POST, 1);
				//curl_setopt($ch, CURLOPT_POSTFIELDS, "message=".$data['message']."&type=".$type."");
				curl_setopt($ch, CURLOPT_POSTFIELDS, "latitude=" . $driver->start_latitude . "&longitude=" . $driver->start_longitude . "");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$server_output = curl_exec($ch);
				curl_close($ch);
				
				
				return Redirect::back()->with('status','Offer approved successfully');
			}
			else
			{
				if($driver->status == "Rejected")
					return Redirect::back()->withErrors('This offer is already rejected')->withInput();
				ReleaseOffer::where('release_offer_id',$request['id'])->update(array(
					'status' => "Rejected",
					'updated_at' => new \DateTime
				));
				return Redirect::back()->with('status','Offer rejected successfully');
			}
		}
		catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

	//////////////////////		Update MArgin 		////////////////////////////////////
	public static function admin_update_margin(Request $request)
	{
		try{
			ReleaseOffer::where('release_offer_id',$request['id'])->update(array(
				'margin' => $request['margin'],
				'updated_at' =>new \DateTime
			));
			return Redirect::back()->with('status','Margin updated successfully');
		}
		catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

}
