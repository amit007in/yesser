<?php

namespace App\Http\Controllers\Admin\Service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;

use App\Http\Controllers\CommonController;

use App\Models\AdminUserDetailLogin;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderRating;
use App\Models\Organisation;
use App\Models\OrganisationCategory;
use App\Models\CategoryBrand;
use App\Models\CategoryBrandProduct;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\UserDriverDetailProduct;
use App\Models\UserDriverDetailService;
use App\Models\PaymentLedger;

class AdminSerDriverCont extends Controller
{

/////////////////////////		New All Driver Listings 	///////////////////////////////////////////////
public static function admin_ser_dall_new(Request $request)
	{ 
	try{

			$request['user_type_ids'] = [2];

			$data = CommonController::set_starting_ending($request->all());

			$services = Category::admin_get_service_categories($request->all());
			$drivers = UserDetail::admin_drivers_listings($data);
			
			return View::make('Admin.Services.Drivers.allSerDriversNew',compact('drivers', 'services', 'data'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

/////////////////////////		New Single Service Drivers 	///////////////////////////////////////////
public static function admin_single_ser_dall_new(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$data = CommonController::set_starting_ending($request->all());

			$data['category_type'] = 'Service';
			$data['user_type_ids'] = [2];

			$category = Category::admin_single_cat_details($request->all());

			$data['category_id_check'] = $data['category_id'];
			$drivers = UserDetail::admin_drivers_listings($data);

			return View::make('Admin.Services.Drivers.singleSerDriversNew', compact('category', 'drivers', 'data'))->with('category_id', $request['category_id'])->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}


/////////////////////////		Drivers Listings 	///////////////////////////////////////////////////
public static function admin_ser_dall(Request $request)
	{ 
	try{

			$request['user_type_ids'] = [2];

			$data = CommonController::set_starting_ending($request->all());

			$services = Category::admin_get_service_categories($request->all());
			$drivers = UserDetail::admin_drivers_listing($data);

			return View::make('Admin.Services.Drivers.allSerDrivers',compact('drivers', 'services'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

////////////////////////	Products 		/////////////////////////////////////////////////
public static function admin_ser_dproducts(Request $request)
	{
	try{

			$rules = array(
				'driver_user_detail_id' => 'required|exists:user_details,user_detail_id,user_type_id,2',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$request['user_detail_id'] = $request['driver_user_detail_id'];
			$driver = UserDetail::admin_driver_detail_update_get($request->all());

			$request['category_id'] = $driver->category_id;
			$category = Category::admin_single_cat_details($request->all());

			$products = UserDriverDetailProduct::driver_products_listings($request->all());

			return View::make('Admin.Services.Drivers.serDriverProducts', compact('category', 'driver', 'products'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

////////////////////	Service Driver Product Update 	/////////////////////////////////////////
public static function admin_ser_dproduct_update(Request $request)
	{
	try{

			$rules = array(
				'user_driver_detail_product_id' => 'required|exists:user_driver_detail_products',
				'alpha_price' => 'required|min:0',
				'price_per_quantity' => 'required|min:0',
				'price_per_distance' => 'required|min:0',
				'price_per_weight' => 'required|min:0',
				'price_per_hr' => 'required|min:0',
				'price_per_sq_mt' => 'required|min:0'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			UserDriverDetailProduct::where('user_driver_detail_product_id',$request['user_driver_detail_product_id'])->limit(1)->update([
					'alpha_price' => $request['alpha_price'],
					'price_per_quantity' => $request['price_per_quantity'],
					'price_per_distance' => $request['price_per_distance'],
					'price_per_weight' => $request['price_per_weight'],
					'price_per_hr' => $request['price_per_hr'],
					'price_per_sq_mt' => $request['price_per_sq_mt'],
					'updated_at' => new \DateTime
			]);

			return Redirect::back()->with('status','Product price updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////////////////	Driver Update Get 		/////////////////////////////////////
public static function admin_ser_dupdate(Request $request){ 
	try{
			$rules = array('user_detail_id' => 'required|exists:user_details,user_detail_id');

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$driver = UserDetail::admin_driver_detail_update_get($request->all());

			$request['category_id'] = $driver->category_id;

			$brands = CategoryBrand::admin_brands_list_add_drivers($request->all());
			$category = Category::admin_single_cat_details($request->all());
                       
			return View::make('Admin.Services.Drivers.updateSerDriver', compact('driver', 'brands', 'category'))->with('category_id', $request['category_id']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////////////////	Driver Update Post 		/////////////////////////////////////
public static function admin_ser_pdupdate(Request $request)
	{
	try{
			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id',
				'name' => 'required',
				//'profile_pic' => 'sometimes|image',
				'phone_number' => 'required|unique:users,phone_number,'.$request['user_id'].',user_id',
				'mulkiya_number' => 'required|unique:user_details,mulkiya_number,'.$request['user_detail_id'].',user_detail_id',
				'mulkiya_validity' => 'required',
				//'mulkiya_front' => 'sometimes|image',
				//'mulkiya_back' => 'sometimes|image',
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
				'productids' => 'required|array',
				//'buraq_percentage' => 'required|min:0|max:100'
			);
			
			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$driver = UserDetail::admin_driver_detail_update_get($request->all());

			$request['user_id'] = $driver->user_id;
			$request['organisation_id'] = $driver->organisation_id;
			$request['profile_pic_name'] = $driver->profile_pic;
			$request['mulkiya_front_name'] = $driver->mulkiya_front;
			$request['mulkiya_back_name'] = $driver->mulkiya_back;
////////////////////////////		Images Upload 		////////////////////////////////
			/*if(!empty(Input::file('profile_pic')))
				{
				$request['profile_pic_name'] = CommonController::image_uploader(Input::file('profile_pic'));
					if(empty($request['profile_pic_name']))
						return Redirect::back()->withErrors('Sorry, profile pic not uploaded. Please try again')->withInput($request->all());
				
				//$DriverImaeUpload = CommonController::driver_image_uploader(Input::file('profile_pic'));
				}
			else
				{
					$request['profile_pic_name'] = $driver->profile_pic;
				}

			if(!empty(Input::file('mulkiya_front')))
				{	
					$request['mulkiya_front_name'] = CommonController::image_uploader(Input::file('mulkiya_front'));
						if(empty($request['mulkiya_front_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_front_name'] = $driver->mulkiya_front;
				}

			if(!empty(Input::file('mulkiya_back')))
				{	
					$request['mulkiya_back_name'] = CommonController::image_uploader(Input::file('mulkiya_back'));
						if(empty($request['mulkiya_back_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_back_name'] = $driver->mulkiya_back;
				}*/

			$request['user_type_id'] = 2;//Support
			$request['mulkiya_validity'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['mulkiya_validity'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');


			User::where('user_id',$request['user_id'])->limit(1)->update([
				'name' => $request['name'],
				'phone_code' => ($request['phone_code']) ? $request['phone_code'] :  $driver->phone_code,
				'phone_number' => $request['phone_number'],
				'buraq_percentage' => isset($request['buraq_percentage']) ? $request['buraq_percentage'] : $driver->buraq_percentage,
				'bottle_charge' => isset($request['bottle_charge']) ? $request['bottle_charge'] : $driver->bottle_charge,
				'address' => isset($request['address']) ? $request['address'] : "",
				'address_latitude' => isset($request['address_latitude']) ? $request['address_latitude'] : 21.47,
				'address_longitude' => isset($request['address_longitude']) ? $request['address_longitude'] : 55.97,
				'updated_at' => new \DateTime
			]);

			UserDetail::where('user_detail_id', $request['user_detail_id'])->limit(1)->update([
				'category_brand_id' => $request['category_brand_id'],
				'profile_pic' => $request['profile_pic_name'],
				'mulkiya_number' => $request['mulkiya_number'],
				'mulkiya_front' => $request['mulkiya_front_name'],
				'mulkiya_back' => $request['mulkiya_back_name'],
				'mulkiya_validity' => $request['mulkiya_validity'],
				'updated_at' => new \DateTime
			]);

			UserDriverDetailProduct::where('user_detail_id',$request['user_detail_id'])->whereNotIn('category_brand_product_id', $request['productids'] )->update([
					'deleted' => '1',
					'updated_at' => new \DateTime
			]);

			$con = [];
			$undel = [];

			foreach($request['productids'] as $productid)
				{

				$check = UserDriverDetailProduct::where('user_detail_id', $request['user_detail_id'])->where('category_brand_product_id', $productid)->first();
				if(!$check)
					{

						$product = CategoryBrandProduct::where('category_brand_product_id',$productid)->first();

						$con[] = [
									'user_id' => $request['user_id'],
									'user_detail_id' => $request['user_detail_id'],
									'category_brand_product_id' => $productid,
									'deleted' => '0',
									'alpha_price' => $product['alpha_price'],
									'price_per_quantity' => $product['price_per_quantity'],
									'price_per_distance' => $product['price_per_distance'],
									'price_per_weight' => $product['price_per_weight'],
									'price_per_hr' => $product['price_per_hr'],
									'price_per_sq_mt' => $product['price_per_sq_mt'],
									'created_at' => new \DateTime,
									'updated_at' => new \DateTime
						];

					}
				else
					{
						$undel[] = $check->user_driver_detail_product_id;
					}

				}

			if(!empty($con))
				UserDriverDetailProduct::insert($con);

			if(!empty(($undel)))
				UserDriverDetailProduct::whereIn('user_driver_detail_product_id', $undel)->update([
					'deleted' => '0',
					'updated_at' => new \DateTime
				]);


			return Redirect::back()->with('status','Yesser Man updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////////		Driver Orders 	///////////////////////////////////////////////
public static function admin_ser_dorders(Request $request)
	{
	try{

			$rules = array(
				'driver_user_detail_id' => 'required|exists:user_details,user_detail_id,user_type_id,2',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$data = CommonController::set_starting_ending($request->all());

			$data['user_detail_id'] = $request['driver_user_detail_id'];
			$driver = UserDetail::admin_driver_detail_update_get($data);
			$data['order_type_ids'] = ["Service"];

			$orders = Order::admin_orders_list($data);

			return View::make('Admin.Services.Drivers.serDriverOrders', compact('driver', 'orders'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

////////////////////////////		Reviews 	////////////////////////////////////////////////
public static function admin_ser_dreviews(Request $request)
	{
	try{

		$rules = array(
			'driver_user_detail_id' => 'required|exists:user_details,user_detail_id,user_type_id,2',
		);

		$validator = Validator::make($request->all(),$rules);
		if($validator->fails())
			return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

		$data = CommonController::set_starting_ending($request->all());

		$data['user_detail_id'] = $request['driver_user_detail_id'];
		$driver = UserDetail::admin_driver_detail_update_get($data);

		$data['order_filter'] = isset($data['order_filter']) ? $data['order_filter'] : 'All';
		$data['createdby_filter'] = isset($data['createdby_filter']) ? $data['createdby_filter'] : 'All';
		$data['search'] = isset($data['search']) ? $data['search'] : '';

		$reviews = OrderRating::admin_reviews_new($data);

		return View::make('Admin.Services.Drivers.serDriverReviews', compact('driver', 'reviews', 'data'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////////		Add Driver Get 		/////////////////////////////////////////
public static function admin_single_ser_dadd(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$brands = CategoryBrand::admin_brands_list_add_drivers($request->all());

			$category = Category::admin_single_cat_details($request->all());

			return View::make('Admin.Services.Drivers.addSerDriver', compact('brands', 'category'));

	}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////////		Add Driver Get 		/////////////////////////////////////////
public static function admin_single_ser_pdadd(Request $request)
	{
	try{
			$rules = array(
				'category_id' => 'required|exists:categories,category_id',
				'name' => 'required',
				//'profile_pic' => 'required|image',
				'phone_number' => 'required|unique:users,phone_number',
				'mulkiya_number' => 'required|unique:user_details,mulkiya_number',
				'mulkiya_validity' => 'required',
				//'mulkiya_front' => 'sometimes|image',
				//'mulkiya_back' => 'sometimes|image',
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
				'productids' => 'required|array',
				'buraq_percentage' => 'required|min:0|max:100'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$category = Category::where('category_id', $request['category_id'])->first();

			// if(!isset($request['buraq_percentage']))
			// 	$request['buraq_percentage'] = $category->buraq_percentage;

			$request["phone_code"] = isset($request['phone_code']) ? $request['phone_code'] : '+968';
			$request['maximum_rides'] = $category->maximum_rides;

////////////////////////////		Images Upload 		////////////////////////////////
			/*$request['profile_pic_name'] = CommonController::image_uploader(Input::file('profile_pic'));
				if(empty($request['profile_pic_name']))
					return Redirect::back()->withErrors('Sorry, profile pic not uploaded. Please try again')->withInput($request->all());
			if(!empty(Input::file('mulkiya_front')))
				{	
					$request['mulkiya_front_name'] = CommonController::image_uploader(Input::file('mulkiya_front'));
						if(empty($request['mulkiya_front_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_front_name'] = '';
				}

			if(!empty(Input::file('mulkiya_back')))
				{	
					$request['mulkiya_back_name'] = CommonController::image_uploader(Input::file('mulkiya_back'));
						if(empty($request['mulkiya_back_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_back_name'] = '';
				}*/
				$request['profile_pic_name'] = "";
				$request['mulkiya_front_name'] = "";
				$request['mulkiya_back_name']  = "";
			//$request['created_by'] = 'Admin';
			$request['created_by'] = 'App';
			$request['category_type'] = 'Service';
			$request['email'] = CommonController::generate_email();

			$user = User::insert_new_record($request->all());

			$request['user_id'] = $user->user_id;
			$request['user_type_id'] = 2;//Service
			$request['mulkiya_validity'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['mulkiya_validity'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');

			$userDetail = UserDetail::insert_new_record($request->all());
			$request['user_detail_id'] = $userDetail->user_detail_id;

			UserDriverDetailService::insert_new_record($request->all());

			$con = [];
			foreach($request['productids'] as $productid)
				{

					$product = CategoryBrandProduct::where('category_brand_product_id', $productid)->first();

					$con[] = [
						'user_id' => $request['user_id'],
						'user_detail_id' => $request['user_detail_id'],
						'category_brand_product_id' => $productid,
						'alpha_price' => $product->alpha_price,
						'price_per_quantity' => $product->price_per_quantity,
						'price_per_distance' => $product->price_per_distance,
						'price_per_weight' => $product->price_per_weight,
						'price_per_hr' => $product->price_per_hr,
						'price_per_sq_mt' => $product->price_per_sq_mt,
						'deleted' => '0',
						'created_at' => new \DateTime,
						'updated_at' => new \DateTime
					];
				}

			if(!empty($con))
				UserDriverDetailProduct::insert($con);

			$request['sms'] = CommonController::send_sms([ 'phone_number' => $request['phone_number'], 'message' => env('APP_NAME').' - '.trans('messages.Congrats you are added as driver')  ]);

			return Redirect::back()->with('status','Yesser Man added successfully');

	}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////		Service Drivers All 		//////////////////////////////////////////////
public static function admin_single_ser_dall(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$data = CommonController::set_starting_ending($request->all());

			$data['category_type'] = 'Service';
			$data['user_type_ids'] = [2];

			$category = Category::admin_single_cat_details($request->all());

			$drivers = UserDetail::admin_get_cat_drivers($data);

			return View::make('Admin.Services.Drivers.singleSerDrivers', compact('category', 'drivers'))->with('category_id', $request['category_id'])->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////		Update Org Cat Percentage 	///////////////////////////////////
public static function admin_driver_cat_percent(Request $request)
	{
	try{

			$rules = array(
				'user_id' => 'required|exists:users,user_id',
				'buraq_percentage' => 'required|min:0|max:100'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			if(!isset($request['bottle_charge']) || $request['bottle_charge'] == null)
				$request['bottle_charge'] = 0;

			User::where('user_id', $request['user_id'])->update([
				'buraq_percentage' => $request['buraq_percentage'],
				'bottle_charge' => $request['bottle_charge'],
				'updated_at' => new \DateTime
			]);

			return Redirect::back()->with('status','YesSer percentage updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////		Org Ledgers 	////////////////////////////////////////////////////
public static function admin_ser_ledger_all (Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$data = CommonController::order_set_starting_ending($request->all());

			$driver = UserDetail::admin_driver_detail_update_get($request->all());

			$ledgers = PaymentLedger::admin_all_ledgers($data);

			return View::make("Admin.Services.Drivers.serviceDLedgersAll", compact('ledgers', 'driver'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

//////////////////////		Admin Ledger Add Post 	////////////////////////////////////
public static function admin_ser_ledger_padd (Request $request)
	{
	try{

			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id',
				'amount' => 'required|min:0',
				'ledger_dt' => 'required',
				'type' => 'required'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$userDetail = UserDetail::where('user_detail_id', $request['user_detail_id'])->first();
			$request['user_id'] = $userDetail->user_id;

			$request['ledger_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['ledger_dt'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');
			$request['user_type_id'] = 2;

			PaymentLedger::insert_new_record($request->all());

			return Redirect::back()->with('status','Ledger added successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

/////////////////////////////		Ledger Update 	/////////////////////////////////////
public static function admin_ser_ledger_pupdate (Request $request)
	{
	try{

			$rules = array(
				'payment_ledger_id' => 'required|exists:payment_ledgers,payment_ledger_id',
				'amount' => 'required|min:0',
				'uledger_dt' => 'required',
				'type' => 'required'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$request['ledger_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['uledger_dt'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');
			$request['user_type_id'] = 4;

			PaymentLedger::where('payment_ledger_id',$request['payment_ledger_id'])->limit(1)->update([
					'amount' => $request['amount'],
					'type' => $request['type'],
					'ledger_dt' => $request['ledger_dt'],
					'description' => isset($request['description']) ? $request['description'] : "",
					'updated_at' => new \DateTime
			]);

			return Redirect::back()->with('status','Ledger updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

//////////////////////		Driver Track 		/////////////////////////////////////////////////
public static function admin_driver_track(Request $request)
	{
	try{

			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id,user_type_id,2',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$driver = UserDetail::admin_driver_detail_update_get($request->all());

			return View::make("Admin.Services.Drivers.trackDriver",compact("driver"));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

////////////////////////////		Service Driver Logings 	/////////////////////////////////////
public static function admin_ser_dlogin_history(Request $request)
	{
	try{

			$rules = array(
				'driver_user_detail_id' => 'required|exists:user_details,user_detail_id,user_type_id,2',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$request['user_type_ids'] = [2];
			$request['user_detail_id'] = $request['driver_user_detail_id'];

			$driver = UserDetail::admin_driver_detail_update_get($request->all());

			$data = CommonController::set_starting_ending($request->all());

			$histories = AdminUserDetailLogin::login_history_get($data);

			return View::make('Admin.Services.Drivers.allLoginHistory', compact('histories','driver'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}


////////////////////////////

}

