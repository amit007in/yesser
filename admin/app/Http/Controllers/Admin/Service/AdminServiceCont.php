<?php

namespace App\Http\Controllers\Admin\Service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Mail;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Illuminate\Cookie\CookieJar;
use Request as ORequest;

use App\Http\Controllers\CommonController;

use App\Models\UserDetail;
use App\Models\UserDriverDetailProduct;
use App\Models\Order;
use App\Models\Organisation;
use App\Models\OrganisationCategoryBrandProduct;
use App\Models\Category;
use App\Models\CategoryBrand;
use App\Models\CategoryBrandDetail;
use App\Models\CategoryBrandProduct;
use App\Models\CategoryBrandProductDetail;
use App\Models\CategoryBrandProductGeofencingPrice;

class AdminServiceCont extends Controller
{
///////////////////////////////		Service Categories    /////////////////////////////////////////
public static function admin_service_cat_all(Request $request)
	{
	try{
			$categories = Category::admin_get_service_categories($request->all());
			return View::make('Admin.Services.Cats.allServiceCats', compact('categories'));
		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

////////////////////////////		Service Distance Update 		///////////////////////////////
public static function admin_ser_dist_pupdate(Request $request)
	{
	try{
			$rules = array(
				'category_id' => 'required|exists:categories,category_id',
				'maximum_distance' => 'required|min:0',
				'buraq_percentage' => 'required|min:0|max:100'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			Category::where('category_id',$request['category_id'])->update([
				'maximum_distance' => $request['maximum_distance'],
				'buraq_percentage' => $request['buraq_percentage'],
				'interval_time'    => isset($request['Interval']) ? $request['Interval'] : '', //Add M2 Part 2
				'transit_offer_percentage' => isset($request['transit_offer_percentage']) ? $request['transit_offer_percentage'] : '0', //Add M2 Part 2
				'transit_buraq_margin' => isset($request['transit_buraq_margin']) ? $request['transit_buraq_margin'] : '0', //Add M2 Part 2
				'driver_quotation_timeout' => isset($request['DriverInterval']) ? $request['DriverInterval'] : '', //Add M2 Part 2
				'updated_at' => new \DateTime
			]);

			return Redirect::back()->with('status','Details updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}
	//Add M2 Part 2
	public static function admin_enable_ser_orders(Request $request)
	{
		try{
			$rules = array(
				'category_id' => 'required|exists:categories,category_id'
			);
			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			Category::where('category_id',$request['category_id'])->update([
				'reassign_status' => '0',
				'updated_at' => new \DateTime
			]);
			return Redirect::back()->with('status','Details updated successfully');

		}
		catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}
	//Add M2 Part 2
	public static function admin_disable_ser_orders(Request $request)
	{
		try{
			$rules = array(
				'category_id' => 'required|exists:categories,category_id'
			);
			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			Category::where('category_id',$request['category_id'])->update([
				'reassign_status' => '1',
				'updated_at' => new \DateTime
			]);
			return Redirect::back()->with('status','Details updated successfully');
		}
		catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

///////////////////////////////		Service Cateory Brands 		////////////////////////////////
public static function admin_service_cat_brands(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$data = CommonController::set_starting_ending($request->all());

			$category = Category::admin_single_cat_details($request->all());
			$brands = CategoryBrand::admin_single_cat_brands($data);

			return View::make('Admin.Services.Cats.Brands.CatAllBrands', compact('category', 'brands'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);
		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

///////////////////////////////		Service Cateory Brand Add Get 		////////////////////////////////
public static function admin_service_cat_add(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'category_id' => 'required|exists:categories,category_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$admin = \App('admin');

			$category = Category::admin_single_cat_details($request->all());
			$ranking = CategoryBrand::where('category_id', $request['category_id'])->count();

			return View::make('Admin.Services.Cats.Brands.CatAddBrands', compact('category', 'ranking'));
		}
	catch(Illuminate\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

///////////////////////////////		Service Cateory Brand Add Post 		////////////////////////////////
public static function admin_service_cat_add_post(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'category_id' => 'required|exists:categories,category_id',
				'english_name' => 'required',
				'english_image' => 'required|image',
				/*'hindi_name' => 'required',
				'hindi_image' => 'required|image',
				'urdu_name' => 'required',
				'urdu_image' => 'required|image',
				'chinese_name' => 'required',
				'chinese_image' => 'required|image',*/
				'arabic_name' => 'required',
				'arabic_image' => 'required|image',
				'ranking' => 'required',
				'buraq_percentage' => 'required|min:0.01|max:100'
			);
			$msg = [
				'english_name.required' => 'Please provide the english name',
				'english_image.required' => 'Please provide the english image',
				'english_image.image' => 'Only Image is allowed',

				/*'hindi_name.required' => 'Please provide the hindi name',
				'hindi_image.required' => 'Please provide the hindi image',
				'hindi_image.image' => 'Only Image is allowed',

				'urdu_name.required' => 'Please provide the urdu name',
				'urdu_image.required' => 'Please provide the urdu image',
				'urdu_image.image' => 'Only Image is allowed',

				'chinese_name.required' => 'Please provide the chinese name',
				'chinese_image.required' => 'Please provide the chinese image',
				'chinese_image.image' => 'Only Image is allowed',*/

				'arabic_name.required' => 'Please provide the arabic name',
				'arabic_image.required' => 'Please provide the arabic image',
				'arabic_image.image' => 'Only Image is allowed',
			];

			$validator = Validator::make($request->all(),$rules,$msg);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

////////////////////////////		Images Upload 		////////////////////////////////
			$request['english_image_name'] = CommonController::image_uploader(Input::file('english_image'));
				if(empty($request['english_image_name']))
					return Redirect::back()->withErrors('Sorry, english image not uploaded. Please try again')->withInput($request->all());
			$request['hindi_image_name'] = CommonController::image_uploader(Input::file('english_image'));
				if(empty($request['english_image_name']))
					return Redirect::back()->withErrors('Sorry, hindi image not uploaded. Please try again')->withInput($request->all());
			$request['urdu_image_name'] = CommonController::image_uploader(Input::file('english_image'));
				if(empty($request['english_image_name']))
					return Redirect::back()->withErrors('Sorry, urdu image not uploaded. Please try again')->withInput($request->all());
			$request['chinese_image_name'] = CommonController::image_uploader(Input::file('english_image'));
				if(empty($request['english_image_name']))
					return Redirect::back()->withErrors('Sorry, chinese image not uploaded. Please try again')->withInput($request->all());
			$request['arabic_image_name'] = CommonController::image_uploader(Input::file('arabic_image'));
				if(empty($request['english_image_name']))
					return Redirect::back()->withErrors('Sorry, arabic image not uploaded. Please try again')->withInput($request->all());
////////////////////////////		Images Upload 		////////////////////////////////

			$cbrand = CategoryBrand::add_new_category_brand($request->all());
			$request['category_brand_id'] = $cbrand->category_brand_id;

			CategoryBrandDetail::insert([
				[
					'category_id' => $request['category_id'],
					'language_id' => 1,
					'category_brand_id' => $request['category_brand_id'],
					'name' => $request['english_name'],
					'image' => $request['english_image_name'],
					'description' => isset($request['english_description']) ? $request['english_description'] : '',
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				],
				[
					'category_id' => $request['category_id'],
					'language_id' => 2,
					'category_brand_id' => $request['category_brand_id'],
					'name' => $request['english_name'],
					'image' => $request['english_image_name'],
					'description' => isset($request['hindi_description']) ? $request['hindi_description'] : '',
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				],
				[
					'category_id' => $request['category_id'],
					'language_id' => 3,
					'category_brand_id' => $request['category_brand_id'],
					'name' => $request['english_name'],
					'image' => $request['english_image_name'],
					'description' => isset($request['urdu_description']) ? $request['urdu_description'] : '',
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				],
				[
					'category_id' => $request['category_id'],
					'language_id' => 4,
					'category_brand_id' => $request['category_brand_id'],
					'name' => $request['english_name'],
					'image' => $request['english_image_name'],
					'description' => isset($request['chinese_description']) ? $request['chinese_description'] : '',
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				],
				[
					'category_id' => $request['category_id'],
					'language_id' => 5,
					'category_brand_id' => $request['category_brand_id'],
					'name' => $request['arabic_name'],
					'image' => $request['arabic_image_name'],
					'description' => isset($request['arabic_description']) ? $request['arabic_description'] : '',
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				]
			]);

			CategoryBrand::where('category_brand_id', '!=', $request['category_brand_id'])->where('category_id',$request['category_id'])->where('sort_order',$request['ranking'])->limit(1)->update([
					'sort_order' => $request['total']+1,
					'updated_at' => new \DateTime
			]);

			return Redirect::back()->with('status','Category added successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

///////////////////////////////			Brand Update Get		/////////////////////////////////////////
public static function admin_service_brand_update(Request $request)
	{
	try{

			$rules = array(
				'category_brand_id' => 'required|exists:category_brands,category_brand_id'
			);
			$msg = array('category_brand_id.exists' => 'Sorry, this brand is currently not available');

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$admin = \App('admin');

			$brand = CategoryBrand::admin_single_brand_details($request->all());

			$request['category_id'] = $brand->category_id;
			$category = Category::admin_single_cat_details($request->all());

			$ranking = CategoryBrand::where('category_id', $brand->category_id)->count();

			return View::make('Admin.Services.Cats.Brands.CatUpdateBrands', compact('brand', 'ranking', 'category'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

///////////////////////////////			Brand Update Post		/////////////////////////////////////////
public static function admin_service_brand_update_post(Request $request)
	{
	try{
			
			$rules = array(
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
				'english_name' => 'required',
				'english_image' => 'image',
				/*'hindi_name' => 'required',
				'hindi_image' => 'image',
				'urdu_name' => 'required',
				'urdu_image' => 'image',
				'chinese_name' => 'required',
				'chinese_image' => 'image',*/
				'arabic_name' => 'required',
				'arabic_image' => 'image',
				'ranking' => 'required',
				'buraq_percentage' => 'required|min:0.01|max:100'
			);
			$msg = [

				'english_name.required' => 'Please provide the english name',
				'english_image.image' => 'Only Image is allowed',

				/*'hindi_name.required' => 'Please provide the hindi name',
				'hindi_image.image' => 'Only Image is allowed',

				'urdu_name.required' => 'Please provide the urdu name',
				'urdu_image.image' => 'Only Image is allowed',

				'chinese_name.required' => 'Please provide the chinese name',
				'chinese_image.image' => 'Only Image is allowed',*/

				'arabic_name.required' => 'Please provide the arabic name',
				'arabic_image.image' => 'Only Image is allowed',
			];

			$msg = array();

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$brand = CategoryBrand::admin_single_brand_details($request->all());
			$start_time = isset($request['start_time']) ? $request['start_time'] : "";
			$end_time = isset($request['end_time']) ? $request['end_time'] : "";
			$maximum_radius = isset($request['maximum_radius']) ? $request['maximum_radius'] : 0;
			$is_default     = isset($request['myCheck']) ? $request['myCheck'] : 0;
			CategoryBrand::where('category_brand_id',$request['category_brand_id'])->limit(1)->update([
				'buraq_percentage' => $request['buraq_percentage'],
				'sort_order' => $request['ranking'],
				'start_time' => $start_time,
				'end_time' => $end_time,
				'maximum_radius' => $maximum_radius,
				'is_default' => $is_default,
				'updated_at' => new \DateTime
			]);

////////////////////////////		Images Upload 		////////////////////////////////
			if(!empty($request['english_image']))
				{
					$request['english_image_name'] = CommonController::image_uploader(Input::file('english_image'));
					if(empty($request['english_image_name']))
						return Redirect::back()->withErrors('Sorry, english image not uploaded. Please try again')->withInput($request->all());
				}
			else
				$request['english_image_name'] = $brand->category_brand_details[0]->image;

			/*if(!empty($request['english_image']))
				{
					$request['hindi_image_name'] = CommonController::image_uploader(Input::file('english_image'));
					if(empty($request['hindi_image_name']))
						return Redirect::back()->withErrors('Sorry, hindi image not uploaded. Please try again')->withInput($request->all());
				}
			else
				if(!empty($request['english_image']))
				{
					$request['urdu_image_name'] = CommonController::image_uploader(Input::file('english_image'));
					if(empty($request['urdu_image_name']))
						return Redirect::back()->withErrors('Sorry, urdu image not uploaded. Please try again')->withInput($request->all());
				}
			else
				if(!empty($request['english_image']))
				{
					$request['chinese_image_name'] = CommonController::image_uploader(Input::file('english_image'));
					if(empty($request['chinese_image_name']))
						return Redirect::back()->withErrors('Sorry, chinese image not uploaded. Please try again')->withInput($request->all());
				}
			else*/
				$request['hindi_image_name'] = $request['english_image_name'];

			
				$request['urdu_image_name'] = $request['english_image_name'];

			
				$request['chinese_image_name'] = $request['english_image_name'];

			if(!empty($request['arabic_image']))
				{
					$request['arabic_image_name'] = CommonController::image_uploader(Input::file('arabic_image'));
					if(empty($request['arabic_image_name']))
						return Redirect::back()->withErrors('Sorry, arabic image not uploaded. Please try again')->withInput($request->all());
				}
			else
				$request['arabic_image_name'] = $brand->category_brand_details[4]->image;
////////////////////////////		Images Upload 		////////////////////////////////

		$request['updated_at'] = Carbon::now('UTC')->format('Y-m-d H:i:s');
		$data = $request->all();

///////////////////////////		Update Details 		/////////////////////////////////////////
	//try{
    	
	    	DB::transaction(function() use ($brand, $data) {

			$temp1 = $brand->category_brand_details[0]->category_brand_detail_id;
			$temp2 = $brand->category_brand_details[1]->category_brand_detail_id;
			$temp3 = $brand->category_brand_details[2]->category_brand_detail_id;
			$temp4 = $brand->category_brand_details[3]->category_brand_detail_id;
			$temp5 = $brand->category_brand_details[4]->category_brand_detail_id;

			DB::unprepared(DB::raw("UPDATE category_brand_details SET name='".$data['english_name']."', image='".$data['english_image_name']."', description ='". (isset($data['english_description']) ? $data['english_description'] : "" ) ."', updated_at='".$data['updated_at']."' WHERE category_brand_detail_id=$temp1"));
			DB::unprepared(DB::raw("UPDATE category_brand_details SET name='".$data['english_name']."', image='".$data['hindi_image_name']."', description ='". (isset($data['english_description']) ? $data['english_description'] : "" ) ."', updated_at='".$data['updated_at']."' WHERE category_brand_detail_id=$temp2"));
			DB::unprepared(DB::raw("UPDATE category_brand_details SET name='".$data['english_name']."', image='".$data['urdu_image_name']."', description ='". (isset($data['english_description']) ? $data['english_description'] : "" ) ."', updated_at='".$data['updated_at']."' WHERE category_brand_detail_id=$temp3"));
			DB::unprepared(DB::raw("UPDATE category_brand_details SET name='".$data['english_name']."', image='".$data['chinese_image_name']."', description ='". (isset($data['english_description']) ? $data['english_description'] : "" ) ."', updated_at='".$data['updated_at']."' WHERE category_brand_detail_id=$temp4"));
			DB::unprepared(DB::raw("UPDATE category_brand_details SET name='".$data['arabic_name']."', image='".$data['arabic_image_name']."', description ='". (isset($data['arabic_description']) ? $data['arabic_description'] : "" ) ."', updated_at='".$data['updated_at']."' WHERE category_brand_detail_id=$temp5"));

	    	});

	    	DB::commit();

	// 	}
	// catch (\Exception $e)
	// 	{
	//     	DB::rollback();
	// 	}
///////////////////////////		Update Details 		/////////////////////////////////////////

			if($brand->sort_order != $request['ranking'])
				{

					CategoryBrand::where('category_id',$request['category_id'])->where('sort_order',$request['ranking'])->where('category_brand_type', $brand->category_brand_type)->limit(1)->update([
							'sort_order' => $brand->sort_order,
							'updated_at' => $data['updated_at']
					]);

				}

			return Redirect::back()->with('status','Category updated successfully');

		}
	catch(\Exception $e)
		{
			DB::rollback();
			return Redirect::back()->withErrors($e->getMessage());
		}
	}


///////////////////////////////			Brand Block Unbloc 		/////////////////////////////////////////
public static function admin_brand_block(Request $request)
	{
	try{

			$rules = array(
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
				'block' => 'required|in:0,1'
			);
			$msg = array('category_brand_id.exists' => 'Sorry, this Category is currently not available');

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$cbrand = CategoryBrand::where('category_brand_id',$request['category_brand_id'])->first();

			if($request['block'] == 1)
				{

					if($cbrand->blocked == '1')
						return Redirect::back()->withErrors('This Category is already blocked')->withInput();

					CategoryBrand::where('category_brand_id',$cbrand->category_brand_id)->update(array(
						'blocked' => '1',
						'updated_at' =>new \DateTime
					));

					return Redirect::back()->with('status','Category blocked successfully');

				}
			else
				{

					if($cbrand->blocked == '0')
						return Redirect::back()->withErrors('This Category is already unblocked')->withInput();

					CategoryBrand::where('category_brand_id',$cbrand->category_brand_id)->update(array(
						'blocked' => '0',
						'updated_at' => new \DateTime
					));

					return Redirect::back()->with('status','Category unblocked successfully');

				}

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}
///////////////////////////////////		Brand Products All 		///////////////////////////////////////
public static function admin_brand_products_all(Request $request)
	{
	try{

			$rules = array(
				'category_brand_id' => 'required|exists:category_brands,category_brand_id'
			);
			$msg = [
				'category_brand_id.exists' => 'Sorry, this Category is currently not available'
			];

			$msg = array();

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$data = CommonController::set_starting_ending($request->all());

			$brand = CategoryBrand::admin_single_brand_details($request->all());
			$products = CategoryBrandProduct::single_brand_all_products($data);

			return View::make('Admin.Services.Cats.Brands.Products.allBrandProducts', compact('brand', 'products'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

/////////////////////////////		Product Block unblock 		/////////////////////////////////////////
public static function admin_products_block(Request $request)
	{
	try{

			$rules = array(
				'category_brand_product_id' => 'required|exists:category_brand_products,category_brand_product_id',
				'block' => 'required|in:0,1'
			);
			$msg = array('category_brand_product_id.exists' => 'Sorry, this product is currently not available');

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$product = CategoryBrandProduct::where('category_brand_product_id',$request['category_brand_product_id'])->first();

			if($request['block'] == 1)
				{

					if($product->blocked == '1')
						return Redirect::back()->withErrors('This product is already blocked')->withInput();

					CategoryBrandProduct::where('category_brand_product_id',$product->category_brand_product_id)->update(array(
						'blocked' => '1',
						'updated_at' =>new \DateTime
					));

					return Redirect::back()->with('status','Product blocked successfully');

				}
			else
				{

					if($product->blocked == '0')
						return Redirect::back()->withErrors('This product is already unblocked')->withInput();

					CategoryBrandProduct::where('category_brand_product_id',$product->category_brand_product_id)->update(array(
						'blocked' => '0',
						'updated_at' => new \DateTime
					));

					return Redirect::back()->with('status','Product unblocked successfully');

				}

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}
	
	/**
	*@role Product Geo Fencing Enable/Disable
	*@author Raghav
	*@Method GET
	*@ M3
	*/
	
	public static function admin_products_geo_fencing_block(Request $request)
	{
		try{
			$rules = array(
				'category_id' => 'required|exists:categories,category_id',
				'block' => 'required|in:0,1'
			);
			$msg = array('category_id.exists' => 'Sorry, this Category is currently not available');

			$validator = Validator::make($request->all(),$rules,$msg);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$product = Category::where('category_id',$request['category_id'])->first();

			if($request['block'] == 1)
			{
				if($product->geofencing_status == '1')
					return Redirect::back()->withErrors('This Category geofencing is already disabled')->withInput();

				Category::where('category_id',$product->category_id)->update(array(
					'geofencing_status' => '1',
					'updated_at' =>new \DateTime
				));
					return Redirect::back()->with('status','Category geofencing disabled successfully');
			}
			else
			{
				if($product->geofencing_status == '0')
					return Redirect::back()->withErrors('This Category geofencing is already enabled')->withInput();
					Category::where('category_id',$product->category_id)->update(array(
						'geofencing_status' => '0',
						'updated_at' => new \DateTime
					));
				return Redirect::back()->with('status','Category geofencing enabled successfully');
			}
		}
		catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

////////////////////////////////		Add Product 			////////////////////////////////////////
public static function admin_product_add(Request $request)
	{
	try{

			$rules = array(
				'category_brand_id' => 'required|exists:category_brands,category_brand_id'
			);
			$msg = array('category_brand_id.exists' => 'Sorry, this Category is currently not available');

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$brand = CategoryBrand::admin_single_brand_details($request->all());
			$ranking = CategoryBrandProduct::where('category_brand_id',$request['category_brand_id'])->count();

			return View::make('Admin.Services.Cats.Brands.Products.addBrandProducts', compact('brand', 'ranking'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

////////////////////////////////		Add Product Post			////////////////////////////////////
public static function admin_product_add_post(Request $request)
	{
	try{
			$rules = array(
				'category_id' => 'required|exists:categories,category_id',
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
				'english_name' => 'required',
				/*'hindi_name' => 'required',
				'urdu_name' => 'required',
				'chinese_name' => 'required',*/
				'arabic_name' => 'required',
				'ranking' => 'required',
				'actual_value' => 'required|min:0',
				'price_per_quantity' => 'required|min:0',
				'price_per_distance' => 'required|min:0',
				'price_per_weight' => 'required|min:0',
				'price_per_hr' => 'required|min:0',
				'price_per_sq_mt' => 'required|min:0',
				'image' => 'sometimes|image',
				'min_quantity' => 'required|integer|min:1|lt:max_quantity',
				'max_quantity' => 'required|integer|gt:min_quantity'
			);
			$msg = [
				'english_name.required' => 'Please provide the english name',
				/*'hindi_name.required' => 'Please provide the hindi name',
				'urdu_name.required' => 'Please provide the urdu name',
				'chinese_name.required' => 'Please provide the chinese name',*/
				'arabic_name.required' => 'Please provide the arabic name',
				'min_quantity.lt' => "Minimun quantity should be less than max quantity",
				'max_quantity.gt' => "Maximum quantity should be greater than min quantity"
			];

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

////////////////////////////		Images Upload 		////////////////////////////////
			if(!empty($request['image']))
				{
					$request['image_name'] = CommonController::image_uploader(Input::file('image'));
					if(empty($request['image_name']))
						return Redirect::back()->withErrors('Sorry, image not uploaded. Please try again')->withInput($request->all());
				}
			else
				$request['image_name'] = '';
////////////////////////////		Images Upload 		////////////////////////////////

			$cbp = CategoryBrandProduct::insert_new_record($request->all());
			$request['category_brand_product_id'] = $cbp->category_brand_product_id;
			//M3 Part
			if(isset($request['geofencing_area']))
			{
				$datass = $request->all();
				$product = "";
				$product->category_id       = $request['category_id'];
				$product->category_brand_id = $request['category_brand_id'];
				CategoryBrandProductGeofencingPrice::save_products_price($datass,$product);
			}
			CategoryBrandProductDetail::insert([
				[
					'category_id' => $request['category_id'],
					'category_brand_id' => $request['category_brand_id'],
					'category_brand_product_id' => $request['category_brand_product_id'],
					'language_id' => 1,					
					'name' => $request['english_name'],
					'description' => isset($request['english_description']) ? $request['english_description'] : '',
					'gift_offer' => "",
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				],
				[
					'category_id' => $request['category_id'],
					'category_brand_id' => $request['category_brand_id'],
					'category_brand_product_id' => $request['category_brand_product_id'],
					'language_id' => 2,
					'name' => $request['english_name'],
					'description' => isset($request['hindi_description']) ? $request['hindi_description'] : '',
					'gift_offer' => "",
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				],
				[
					'category_id' => $request['category_id'],
					'category_brand_id' => $request['category_brand_id'],
					'category_brand_product_id' => $request['category_brand_product_id'],
					'language_id' => 3,
					'name' => $request['english_name'],
					'description' => isset($request['urdu_description']) ? $request['urdu_description'] : '',
					'gift_offer' => "",
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				],
				[
					'category_id' => $request['category_id'],
					'category_brand_id' => $request['category_brand_id'],
					'category_brand_product_id' => $request['category_brand_product_id'],
					'language_id' => 4,
					'name' => $request['english_name'],
					'description' => isset($request['chinese_description']) ? $request['chinese_description'] : '',
					'gift_offer' => "",
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				],
				[
					'category_id' => $request['category_id'],
					'category_brand_id' => $request['category_brand_id'],
					'category_brand_product_id' => $request['category_brand_product_id'],
					'language_id' => 5,
					'name' => $request['arabic_name'],
					'description' => isset($request['arabic_description']) ? $request['arabic_description'] : '',
					'gift_offer' => "",
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				]
			]);

			CategoryBrandProduct::where('category_brand_product_id', '!=', $request['category_brand_product_id'])->where('category_id',$request['category_id'])->where('category_brand_id', $request['category_brand_id'])->where('sort_order',$request['ranking'])->limit(1)->update([
					'sort_order' => $request['total']+1,
					'updated_at' => new \DateTime
			]);

//////////////////////		Org Insert 	///////////////////////////////
//	$orgs = Organisation::whereRaw('( ((SELECT COUNT(*) FROM organisation_categories as oc WHERE oc.category_id='.$request['category_id'].' AND oc.organisation_id=organisations.organisation_id) = 1) )')->get();
	$orgs = Organisation::whereRaw('( SELECT COUNT(*) FROM organisation_categories as oc WHERE oc.category_id='.$request['category_id'].' AND oc.organisation_id=organisations.organisation_id != 0 )')->get();
	$con = [];
//dd($orgs, $request->all());
	foreach($orgs as $org)
		{

			$con[] = [
				'organisation_id' => $org->organisation_id,
				'category_id' => $request['category_id'],
				'category_brand_id' => $request['category_brand_id'],
				'category_brand_product_id' => $request['category_brand_product_id'],
				'alpha_price' => $request['alpha_price'],
				'price_per_quantity' => $request['price_per_quantity'],
				'price_per_distance' => $request['price_per_distance'],
				'price_per_weight' => $request['price_per_weight'],
				'price_per_hr' => $request['price_per_hr'],
				'price_per_sq_mt' => $request['price_per_sq_mt'],
				'created_at' => new \DateTime,
				'updated_at' => new \DateTime
			];

		}
	if(!empty($con))
		OrganisationCategoryBrandProduct::insert($con);
//////////////////////		Org Insert 	///////////////////////////////

//////////////////////		Drivers Insert 	///////////////////////////
	$con = [];

	$drivers = UserDetail::where('organisation_id','=',0)->where('category_id', $request['category_id'])->where('category_brand_id', $request['category_brand_id'])->get();

	foreach($drivers as $driver)
		{
			
			$con[] = [
				'user_id' => $driver->user_id,
				'user_detail_id' => $driver->user_detail_id,
				'category_brand_product_id' => $request['category_brand_product_id'],
				'alpha_price' => $request['alpha_price'],
				'price_per_quantity' => $request['price_per_quantity'],
				'price_per_distance' => $request['price_per_distance'],
				'price_per_weight' => $request['price_per_weight'],
				'price_per_hr' => $request['price_per_hr'],
				'price_per_sq_mt' => $request['price_per_sq_mt'],
				'created_at' => new \DateTime,
				'updated_at' => new \DateTime
			];

		}

	if(!empty($con))
		UserDriverDetailProduct::insert($con);
//////////////////////		Drivers Product Update 	///////////////////

			return Redirect::back()->with('status','Product added successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}
//M3
///////////////////////////////		Brand Product Update 		///////////////////////////////////////////
public static function admin_product_update(Request $request)
	{
	try{

			$rules = array(
				'category_brand_product_id' => 'required|exists:category_brand_products,category_brand_product_id',
			);

			$validator = Validator::make($request->all(),$rules);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$product = CategoryBrandProduct::admin_single_details($request->all());

			$request['category_brand_id'] = $product->category_brand_id;
			$GeoPrices = CategoryBrandProductGeofencingPrice::get_productprice_details($request->all());
			$ranking = CategoryBrandProduct::where('category_brand_id',$request['category_brand_id'])->count();
			$brand = CategoryBrand::admin_single_brand_details($request->all());
			
			return View::make('Admin.Services.Cats.Brands.Products.updateBrandProducts', compact('product', 'brand', 'ranking','GeoPrices'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

///////////////////////////////		Brand Product Update Post 		////////////////////////////////////////
public static function admin_product_update_post(Request $request)
	{
	try{
			$rules = array(
				'category_brand_product_id' => 'required|exists:category_brand_products,category_brand_product_id',
				'english_name' => 'required',
				/*'hindi_name' => 'required',
				'urdu_name' => 'required',
				'chinese_name' => 'required',*/
				'arabic_name' => 'required',
				'ranking' => 'required',
				'actual_value' => 'required|min:0',
				'price_per_quantity' => 'required|min:0',
				'price_per_distance' => 'required|min:0',
				'price_per_weight' => 'required|min:0',
				'price_per_hr' => 'required|min:0',
				'price_per_sq_mt' => 'required|min:0',
				'min_quantity' => 'required|integer|min:1|lt:max_quantity',
				'max_quantity' => 'required|integer|gt:min_quantity'
			);
			$msg = [
				'english_name.required' => 'Please provide the english name',
				/*'hindi_name.required' => 'Please provide the hindi name',
				'urdu_name.required' => 'Please provide the urdu name',
				'chinese_name.required' => 'Please provide the chinese name',*/
				'arabic_name.required' => 'Please provide the arabic name',
				'min_quantity.lt' => "Minimun quantity should be less than max quantity",
				'max_quantity.gt' => "Maximum quantity should be greater than min quantity"
			];

			$validator = Validator::make($request->all(), $rules, $msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$product = CategoryBrandProduct::admin_single_details($request->all());

		$request['updated_at'] = Carbon::now('UTC')->format('Y-m-d H:i:s');
		$data = $request->all();

////////////////////////////		Images Upload 		////////////////////////////////
			if(!empty($data['image']))
				{
					$data['image_name'] = CommonController::image_uploader(Input::file('image'));
					if(empty($data['image_name']))
						return Redirect::back()->withErrors('Sorry, image not uploaded. Please try again')->withInput($request->all());
				}
			else
				$data['image_name'] = $product->image;
////////////////////////////		Images Upload 		////////////////////////////////

///////////////////////////		Update Details 		/////////////////////////////////////////
//	try{
			
	    	DB::transaction(function() use ($product, $data) {

			$temp1 = $product->category_brand_product_details[0]->category_brand_product_detail_id;
			$temp2 = $product->category_brand_product_details[1]->category_brand_product_detail_id;
			$temp3 = $product->category_brand_product_details[2]->category_brand_product_detail_id;
			$temp4 = $product->category_brand_product_details[3]->category_brand_product_detail_id;
			$temp5 = $product->category_brand_product_details[4]->category_brand_product_detail_id;

			$data['multiple']      = isset($data['multiple']) ? $data['multiple'] : '1';
			$data['gift_quantity'] = isset($data['gift_quantity']) ? $data['gift_quantity'] : "0";
			$data['gift_offer']    = isset($data['gift_offer']) ? $data['gift_offer'] : "";
			$data['product_margin']= isset($data['product_margin']) ? $data['product_margin'] : '0.0'; //Add M3 Part 3.2

			$data['should_use_buraq_margin'] = isset($data['should_use_buraq_margin']) ? $data['should_use_buraq_margin'] : 0;//Add M3 Part 3.2
			
			DB::unprepared(DB::raw("UPDATE category_brand_products SET  actual_value=".$data['actual_value'].",product_margin=".$data['product_margin'].",should_use_buraq_margin=".$data['should_use_buraq_margin'].", alpha_price=".$data['alpha_price'].", price_per_quantity=".$data['price_per_quantity'].", price_per_distance=".$data['price_per_distance'].", price_per_weight=".$data['price_per_weight'].", price_per_hr=".$data['price_per_hr'].", price_per_sq_mt=".$data['price_per_sq_mt'].", sort_order=".$data['ranking'].", multiple='".$data['multiple']."', image ='".$data['image_name']."', min_quantity=".$data['min_quantity'].", max_quantity=".$data['max_quantity'].", gift_quantity=".$data['gift_quantity'].", gift_offer='".$data['gift_offer']."', updated_at='".$data['updated_at']."' WHERE  category_brand_product_id=".$data['category_brand_product_id']."  "));
			DB::unprepared(DB::raw("UPDATE category_brand_product_details SET name='".$data['english_name']."', description ='". (isset($data['english_description']) ? $data['english_description'] : "" ) ."',gift_offer ='". (isset($data['gift_offer']) ? $data['gift_offer'] : "" ) ."', updated_at='".$data['updated_at']."' WHERE category_brand_product_detail_id=$temp1"));
			DB::unprepared(DB::raw("UPDATE category_brand_product_details SET name='".$data['english_name']."', description ='". (isset($data['hindi_description']) ? $data['hindi_description'] : "" ) ."', gift_offer ='". (isset($data['gift_offer_hindi']) ? $data['gift_offer_hindi'] : "" ) ."', updated_at='".$data['updated_at']."' WHERE category_brand_product_detail_id=$temp2"));
			DB::unprepared(DB::raw("UPDATE category_brand_product_details SET name='".$data['english_name']."', description ='". (isset($data['urdu_description']) ? $data['urdu_description'] : "" ) ."', gift_offer ='". (isset($data['gift_offer_urdu']) ? $data['gift_offer_urdu'] : "" ) ."', updated_at='".$data['updated_at']."' WHERE category_brand_product_detail_id=$temp3"));
			DB::unprepared(DB::raw("UPDATE category_brand_product_details SET name='".$data['english_name']."', description ='". (isset($data['chinese_description']) ? $data['chinese_description'] : "" ) ."', gift_offer ='". (isset($data['gift_offer_chinese']) ? $data['gift_offer_chinese'] : "" ) ."', updated_at='".$data['updated_at']."' WHERE category_brand_product_detail_id=$temp4"));
			DB::unprepared(DB::raw("UPDATE category_brand_product_details SET name='".$data['arabic_name']."', description ='". (isset($data['arabic_description']) ? $data['arabic_description'] : "" ) ."', gift_offer ='". (isset($data['gift_offer_arabic']) ? $data['gift_offer_arabic'] : "" ) ."', updated_at='".$data['updated_at']."' WHERE category_brand_product_detail_id=$temp5"));

	    	});
	
	    	DB::commit();

	// 	}
	// catch (\Exception $e)
	// 	{
	//     	DB::rollback();
	// 	}
///////////////////////////		Update Details 		/////////////////////////////////////////
			DB::table('category_brand_product_geofencing_prices')->where('category_brand_product_id', $data['category_brand_product_id'])->delete();
			if(isset($data['geofencing_area']))
			{
				
				CategoryBrandProductGeofencingPrice::save_products_price($data,$product);
			}
			if($product->sort_order != $request['ranking'])
				{

					CategoryBrandProduct::where('category_brand_product_id', '!=', $request['category_brand_product_id'])->where('category_id',$product->category_id)->where('category_brand_id', $product->category_brand_id)->where('sort_order',$request['ranking'])->limit(1)->update([
							'sort_order' => $product->sort_order,
							'updated_at' => new \DateTime
					]);

				}

			return Redirect::back()->with('status','Product updated successfully');

		}
	catch(\Exception $e)
		{
			DB::rollback();
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

////////////////////		Service Orders 	/////////////////////////////////////////////
public static function admin_ser_orders(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$data = CommonController::set_starting_ending($request->all());
			$data['order_type_ids'] = ["Service"];

			$category = Category::admin_single_cat_details($request->all());
			
			$orders = Order::admin_orders_list($data);
			return View::make('Admin.Services.Orders.serOrders', compact('category', 'orders'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

////////////////////	Service All Products 		///////////////////////////////////////
public static function admin_ser_prods_all(Request $request)
	{
	try{
			$categories = Category::admin_get_service_categories($request->all());
			$products = CategoryBrandProduct::all_services_all_products($request->all());
			
			return View::make('Admin.Services.Products.allProducts', compact('categories', 'products'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

////////////////////		Product Update 		///////////////////////////////////////////
public static function admin_ser_prod_update(Request $request)
	{
	try{

			$rules = array(
				'category_brand_product_id' => 'required|exists:category_brand_products',
				'alpha_price' => 'required|min:0',
				'price_per_quantity' => 'required|min:0',
				'price_per_distance' => 'required|min:0',
				'price_per_weight' => 'required|min:0',
				'price_per_hr' => 'required|min:0',
				'price_per_sq_mt' => 'required|min:0',
				'min_quantity' => 'required|integer|min:1|lt:max_quantity',
				'max_quantity' => 'required|integer|gt:min_quantity'
			);

			$msg = [
				'min_quantity.lt' => "Minimun quantity should be less than max quantity",
				'max_quantity.gt' => "Maximum quantity should be greater than min quantity"
			];

			$validator = Validator::make($request->all(),$rules, $msg);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			CategoryBrandProduct::where('category_brand_product_id',$request['category_brand_product_id'])->limit(1)->update([
					'alpha_price' => $request['alpha_price'],
					'price_per_quantity' => $request['price_per_quantity'],
					'price_per_distance' => $request['price_per_distance'],
					'price_per_weight' => $request['price_per_weight'],
					'price_per_hr' => $request['price_per_hr'],
					'price_per_sq_mt' => $request['price_per_sq_mt'],
					'updated_at' => new \DateTime
			]);

			return Redirect::back()->with('status','Product price updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

//////////////////////			New Product Add 		/////////////////////////////////////////
public static function admin_product_new_add(Request $request)
	{
	try{

			$categories = Category::admin_get_service_categories($request->all());

			return View::make("Admin.Services.Products.addProduct",compact('categories'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}
//M3
///////////////////////////		New Product Update Get 		/////////////////////////////////////////
public static function admin_product_new_update(Request $request)
	{
	try{

			$rules = array(
				'category_brand_product_id' => 'required|exists:category_brand_products,category_brand_product_id',
			);

			$validator = Validator::make($request->all(),$rules);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$product = CategoryBrandProduct::admin_single_details($request->all());

			$request['category_brand_id'] = $product->category_brand_id;
			$GeoPrices = CategoryBrandProductGeofencingPrice::get_productprice_details($request->all());
			$ranking = CategoryBrandProduct::where('category_brand_id',$request['category_brand_id'])->count();
			$brand = CategoryBrand::admin_single_brand_details($request->all());

			return View::make('Admin.Services.Products.updateBrandProducts', compact('product', 'brand', 'ranking','GeoPrices'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}


/////////////////////////


}
