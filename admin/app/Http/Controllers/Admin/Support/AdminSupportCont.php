<?php

namespace App\Http\Controllers\Admin\Support;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;

use App\Http\Controllers\CommonController;

use App\Models\Category;
use App\Models\CategoryDetail;
use App\Models\CategoryBrand;
use App\Models\CategoryBrandDetail;
use App\Models\CategoryBrandProduct;
use App\Models\CategoryBrandProductDetail;

class AdminSupportCont extends Controller
{

////////////////////////		Admin Support Categoirs     ////////////////////////////////
public static function admin_support_cat_all(Request $request)
	{
	try{
			$categories = Category::admin_get_support_categories($request->all());

			return View::make('Admin.Supports.Cats.allSupportCats', compact('categories'));
		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

///////////////////////////////			Support Block Unblock 		/////////////////////////////////////////
public static function admin_support_category_block(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id,category_type,Support',
				'block' => 'required|in:0,1'
			);
			$msg = array('category_id.exists' => 'Sorry, this suport category is currently not available');

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$category = Category::where('category_id',$request['category_id'])->first();

			if($request['block'] == 1)
				{

					if($category->blocked == '1')
						return Redirect::back()->withErrors('This Support Category is already blocked')->withInput();

					Category::where('category_id',$category->category_id)->update(array(
						'blocked' => '1',
						'updated_at' =>new \DateTime
					));

					return Redirect::back()->with('status','Support Category blocked successfully');

				}
			else
				{

					if($category->blocked == '0')
						return Redirect::back()->withErrors('This Support Category is already unblocked')->withInput();

					Category::where('category_id',$category->category_id)->update(array(
						'blocked' => '0',
						'updated_at' => new \DateTime
					));

					return Redirect::back()->with('status','Support Category unblocked successfully');

				}

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

////////////////////////		Support CAtegory Add 		///////////////////////////////
public static function admin_support_cat_add(Request $request)
	{
	try{
			$ranking = Category::where('category_type','Support')->count();

			return View::make('Admin.Supports.Cats.AddSupportCats', compact('ranking'));
		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

////////////////////////		Add Support Category Post 		///////////////////////////
public static function admin_support_cat_add_post(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'english_name' => 'required',
				'image' => 'required|image',
				'hindi_name' => 'required',
				'urdu_name' => 'required',
				'chinese_name' => 'required',
				'arabic_name' => 'required',
				'ranking' => 'required',
				'buraq_percentage' => 'required|min:0.01|max:100'
			);
			$msg = [
				'english_name.required' => 'Please provide the english name',
				'image.required' => 'Please provide the image',
				'image.image' => 'Only Image is allowed',
				'hindi_name.required' => 'Please provide the hindi name',
				'urdu_name.required' => 'Please provide the urdu name',
				'chinese_name.required' => 'Please provide the chinese name',
				'arabic_name.required' => 'Please provide the arabic name',
			];

			$validator = Validator::make($request->all(),$rules,$msg);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

////////////////////////////		Images Upload 		////////////////////////////////
			$request['image_name'] = CommonController::image_uploader(Input::file('image'));
				if(empty($request['image_name']))
					return Redirect::back()->withErrors('Sorry, image not uploaded. Please try again')->withInput($request->all());
////////////////////////////		Images Upload 		////////////////////////////////
			$request['category_type'] = 'Support';

			$category = Category::add_new_record($request->all());
			$request['category_id'] = $category->category_id;

			CategoryDetail::insert([
				[
					'category_id' => $request['category_id'],
					'language_id' => 1,
					'name' => $request['english_name'],
					'description' => '',
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				],
				[
					'category_id' => $request['category_id'],
					'language_id' => 2,
					'name' => $request['hindi_name'],
					'description' => '',
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				],
				[
					'category_id' => $request['category_id'],
					'language_id' => 3,
					'name' => $request['urdu_name'],
					'description' => '',
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				],
				[
					'category_id' => $request['category_id'],
					'language_id' => 4,
					'name' => $request['chinese_name'],
					'description' => '',
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				],
				[
					'category_id' => $request['category_id'],
					'language_id' => 5,
					'name' => $request['arabic_name'],
					'description' => '',
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				]
			]);

			Category::where('category_id', '!=', $request['category_id'])->where('category_type',$request['category_type'])->where('sort_order',$request['ranking'])->limit(1)->update([
					'sort_order' => $request['total']+1,
					'updated_at' => new \DateTime
			]);

			return Redirect::back()->with('status','Support category added successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

////////////////////////		Support Category Update 		/////////////////////////////
public static function admin_support_cat_update(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'category_id' => 'required|exists:categories,category_id,category_type,Support',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$category = Category::admin_single_cat_details($request->all());
			$ranking = Category::where('category_type','Support')->count();

			return View::make('Admin.Supports.Cats.UpdateSupportCats', compact('ranking', 'category'));
		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

////////////////////////		Support Category Update Post 		///////////////////////////
public static function admin_support_cat_update_post(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id,category_type,Support',
				'english_name' => 'required',
				'image' => 'image',
				'hindi_name' => 'required',
				'urdu_name' => 'required',
				'chinese_name' => 'required',
				'arabic_name' => 'required',
				'ranking' => 'required',
				'buraq_percentage' => 'required|min:0.01|max:100'
			);
			$msg = [
				'english_name.required' => 'Please provide the english name',
				'image.image' => 'Only Image is allowed',
				'hindi_name.required' => 'Please provide the hindi name',
				'urdu_name.required' => 'Please provide the urdu name',
				'chinese_name.required' => 'Please provide the chinese name',
				'arabic_name.required' => 'Please provide the arabic name',
			];

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$category = Category::admin_single_cat_details($request->all());

			if(!empty($request['image']))
				{
					$request['image_name'] = CommonController::image_uploader(Input::file('image'));
					if(empty($request['image_name']))
						return Redirect::back()->withErrors('Sorry, image not uploaded. Please try again')->withInput($request->all());
				}
			else
				$request['image_name'] = $category->image;

			$request['updated_at'] = Carbon::now('UTC')->format('Y-m-d H:i:s');
			$data = $request->all();

///////////////////////////		Update Details 		/////////////////////////////////////////
   	
	    	DB::transaction(function() use ($category, $data) {

			$temp1 = $category->category_details[0]->category_detail_id;
			$temp2 = $category->category_details[1]->category_detail_id;
			$temp3 = $category->category_details[2]->category_detail_id;
			$temp4 = $category->category_details[3]->category_detail_id;
			$temp5 = $category->category_details[4]->category_detail_id;

			DB::unprepared(DB::raw("UPDATE categories SET sort_order=".$data['ranking'].", buraq_percentage='".$data['buraq_percentage']."', image='".$data['image_name']."', updated_at='".$data['updated_at']."' WHERE category_id=".$data['category_id']." "));
			DB::unprepared(DB::raw("UPDATE category_details SET name='".$data['english_name']."', description ='', updated_at='".$data['updated_at']."' WHERE category_detail_id=$temp1"));
			DB::unprepared(DB::raw("UPDATE category_details SET name='".$data['hindi_name']."', description ='', updated_at='".$data['updated_at']."' WHERE category_detail_id=$temp2"));
			DB::unprepared(DB::raw("UPDATE category_details SET name='".$data['urdu_name']."', description ='', updated_at='".$data['updated_at']."' WHERE category_detail_id=$temp3"));
			DB::unprepared(DB::raw("UPDATE category_details SET name='".$data['chinese_name']."', description ='', updated_at='".$data['updated_at']."' WHERE category_detail_id=$temp4"));
			DB::unprepared(DB::raw("UPDATE category_details SET name='".$data['arabic_name']."', description ='', updated_at='".$data['updated_at']."' WHERE category_detail_id=$temp5"));

	    	});

	    	DB::commit();


			if($category->sort_order != $request['ranking'])
				{

					Category::where('category_id','!=',$request['category_id'])->where('sort_order',$request['ranking'])->where('category_type', $category->category_type)->limit(1)->update([
							'sort_order' => $category->sort_order,
							'updated_at' => $data['updated_at']
					]);

				}

			return Redirect::back()->with('status','Support category updated successfully');

		}
	catch(\Exception $e)
		{
			DB::rollback();
			return Redirect::back()->withErrors($e->getMessage());
		}
	}
////////////////////////

}
