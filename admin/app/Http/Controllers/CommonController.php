<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Validator, Hash, DateTime, Mail, DB, AWS, PushNotification;
use Carbon\Carbon;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use App\Models\Coupon;
use App\Models\Category;
use App\Models\CategoryBrand;
use App\Models\CategoryBrandProduct;
use App\Models\Organisation;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\UserOrderNotification;

class CommonController extends Controller
{

////////////////////////////////////		App Categories List 	/////////////////////////////
public static function app_cat_lists(Request $request)
	{
	try{

//			return Response(['success'=> 0, 'msg' => '$e->getMessage()' ], 500);

			$request['app_language_id'] =  isset($request['app_language_id']) ? $request['app_language_id'] : 1;

			$services = Category::app_get_services_list($request->all());
			$supports = Category::app_get_supports_list($request->all());

			return Response(['success'=> 1, 'msg' => 'Categories listing', 'services' => $services, 'supports' => $supports], 200);

		}
	catch(\Exception $e)
		{
			return Response(['success'=> 0, 'msg' => $e->getMessage() ], 500);
		}
	}

////////////////////////////////////		Test Api 		////////////////////////////////////////////////////
public static function test_api(Request $request)
	{
	try{

			$request['app_language_id'] = 1;
			// $request['timezonez'] = "+05:30";
			// $request['timezone'] = "Asia/Kolkata";

			// $category = Category::admin_single_cat_details($request->all());
			// $brands = CategoryBrand::admin_single_cat_brands($request->all());

			$services = Category::app_get_services_list($request->all());
			$supports = Category::app_get_supports_list($request->all());

			return Response(['success'=> 1, 'services' => $services, 'supports' => $supports ], 200);

		}
	catch(\Exception $e)
		{
			return Response(['success'=> 0, 'msg' => $e->getMessage() ], 500);
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

//////////////////////////////////			Image Uploader 		////////////////////////////////////////////////
public static function image_uploader($image)
	{
	try{

	  		$extention = $image->GetClientOriginalExtension();
	    	$filename = substr(sha1(time().time()), 0, 40) . str_random(25).".{$extention}";

			// $s3 = \AWS::createClient('s3');

			// $upload_success = $s3->putObject(array(
			// 		'ACL' => 'public-read',
			// 	    'Bucket'     => env('AWS_BUCKET'),
			// 		'Key'    => 'Uploads/'.$filename,
			// 		'Body'   => fopen($image->getPathname(), 'r'),
			// 		'ContentType' => 'image/'.$extention,
			// ));

			$upload_success = $image->move(public_path() . '/BuraqExpress/Uploads', $filename);

			return $upload_success ? $filename : '';

		}
	catch(\Exception $e)
		{
			return '';
		}
	}
	
	//////////////////////////////////			Image Uploader 		////////////////////////////////////////////////
public static function driver_image_uploader($image)
	{
	try{

	  		$extention = $image->GetClientOriginalExtension();
	    	$filename = substr(sha1(time().time()), 0, 40) . str_random(25).".{$extention}";

			// $s3 = \AWS::createClient('s3');

			// $upload_success = $s3->putObject(array(
			// 		'ACL' => 'public-read',
			// 	    'Bucket'     => env('AWS_BUCKET'),
			// 		'Key'    => 'Uploads/'.$filename,
			// 		'Body'   => fopen($image->getPathname(), 'r'),
			// 		'ContentType' => 'image/'.$extention,
			// )); 
			
			//$target_file = '/'.$_SERVER['DOCUMENT_ROOT'].'/api/public/Uploads'.$filename;
			//move_uploaded_file($image->getPathName(), $target_file);
			
			//$upload_success = $image->move($_SERVER['DOCUMENT_ROOT']."/api/public/Uploads/", $filename);

			return $upload_success ? $filename : '';

		}
	catch(\Exception $e)
		{
			return '';
		}
	}

//////////////////////////////		Set Starting Ending Date 		/////////////////////////////////////////
public static function set_starting_ending($data)
    {

        if(!isset($data['daterange']))
        	{
				$data['starting_dt'] = "2017-06-01";
            	//$data['starting_dt'] = Carbon::now()->subMonths(2)->format('Y-m-d');
                $data['ending_dt'] = Carbon::now()->addday(1)->format('Y-m-d');
            }
        else
        	{
            	$data['daterange'] = urldecode($data['daterange']);
                $temp_array = explode(' - ', $data['daterange']);
                $data['starting_dt'] = Carbon::CreateFromFormat('d/m/Y',$temp_array[0],$data['timezone'])->timezone('UTC')->format('Y-m-d');
                $data['ending_dt'] = Carbon::CreateFromFormat('d/m/Y',$temp_array[1],$data['timezone'])->timezone('UTC')->format('Y-m-d');
            }

        $data['starting_dt'] = $data['starting_dt'].' 00:00:00';
        $data['ending_dt'] = $data['ending_dt'].' 23:59:59';

        $data['starting_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s',$data['starting_dt'],$data['timezonez'])->timezone('UTC')->format('Y-m-d H:i:s');
        $data['ending_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s',$data['ending_dt'],$data['timezonez'])->timezone('UTC')->format('Y-m-d H:i:s');

        $data['fstarting_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s',$data['starting_dt'],'UTC')->timezone($data['timezonez'])->format('d/m/Y');
        $data['fending_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s',$data['ending_dt'],'UTC')->timezone($data['timezonez'])->format('d/m/Y');

        return $data;

    }
//////////////////////////////		Set Starting Ending Date 		/////////////////////////////////////////
public static function order_set_starting_ending($data)
    {

        if(!isset($data['daterange']))
        	{
            	$data['starting_dt'] = Carbon::now()->subMonths(2)->format('Y-m-d');
                $data['ending_dt'] = Carbon::now()->addMonths(2)->format('Y-m-d');
            }
        else
        	{
            	$data['daterange'] = urldecode($data['daterange']);
                $temp_array = explode(' - ', $data['daterange']);
                $data['starting_dt'] = Carbon::CreateFromFormat('d/m/Y',$temp_array[0],$data['timezone'])->timezone('UTC')->format('Y-m-d');
                $data['ending_dt'] = Carbon::CreateFromFormat('d/m/Y',$temp_array[1],$data['timezone'])->timezone('UTC')->format('Y-m-d');
            }

        $data['starting_dt'] = $data['starting_dt'].' 00:00:00';
        $data['ending_dt'] = $data['ending_dt'].' 23:59:59';

        $data['starting_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s',$data['starting_dt'],$data['timezonez'])->timezone('UTC')->format('Y-m-d H:i:s');
        $data['ending_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s',$data['ending_dt'],$data['timezonez'])->timezone('UTC')->format('Y-m-d H:i:s');

        $data['fstarting_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s',$data['starting_dt'],'UTC')->timezone($data['timezonez'])->format('d/m/Y');
        $data['fending_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s',$data['ending_dt'],'UTC')->timezone($data['timezonez'])->format('d/m/Y');

        return $data;

    }

/////////////////////		Send SMS 		////////////////////////////////////////////
public static function send_sms($data)
	{

        $client = new Client();


        $phone_code = "968";

        $query = [
                    'UserID' =>env('SMS_USER_ID'),
                    'Password' => env('SMS_PASSWORD'),
                    'Message' => $data['message'],
                    'Language' => '0',
                    'MobileNo' => [$phone_code.$data['phone_number']],
                    'ScheddateTime' => Carbon::now('UTC')->addSeconds(5)->timezone("Asia/Muscat")->format("m/d/Y H:i:s")
        ];

        $link = 'https://ismartsms.net/RestApi/api/SMS/PostSMS';

		$response = $client->request('POST', $link, [

			'headers' => [
			    'Content-Type' => 'application/x-www-form-urlencoded'
			],

		    'form_params' => $query
		]);

		// $code = $response->getStatusCode(); // 200
		// $reason = $response->getReasonPhrase(); // OK

		// dd($response, $query, $code, $reason);


        return $response->getBody();

    }

/////////////		OTP Generate 	////////////////////////////////////////////////////
public static function generate_otp()
	{
		//return 4444;
		return rand(1000,9999);
	}

//////////////		Generate Email 		/////////////////////////////////////////////////
public static function generate_email()
	{
		return uniqid().env('MAIL_ENDING');
	}

////////////////////////////
//////////////		Drivers Check 		//////////////////////////////////
////////////////////////////

////////////////		Add Driver Unique Check 		////////////////////////////
public static function driver_unique_acheck(Request $request)
	{
	try{

		if(isset($request['phone_number']))
			{
					
				$ocheck = User::where('phone_number', $request['phone_number'])->first();
				if($ocheck)
					return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this phone number is already taken' ],200);

			}
		elseif(isset($request['licence_number']))
			{
					
				$ocheck = UserDetail::where('licence_number', $request['licence_number'])->first();
				if($ocheck)
					return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this licence number is already taken' ],200);

			}				

			return Response(['success' => 1, 'statuscode' => 200, 'msg' => 'Validated successfully' ],200);

		}
	catch(\Exception $e)
		{
			return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
		}
	}
///////////////////		Update Driver Unique Checks 		/////////////////////////
public static function driver_unique_ucheck(Request $request)
	{
	try{

		if(isset($request['phone_number']))
			{
					
				$ocheck = User::where('phone_number', $request['phone_number'])->where('user_id', '!=' ,$request['user_id'])->first();
				if($ocheck)
					return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this phone number is already taken' ],200);

			}
		elseif(isset($request['licence_number']))
			{
					
				$ocheck = UserDetail::where('licence_number', $request['licence_number'])->where('user_id', '!=' ,$request['user_id'])->first();
				if($ocheck)
					return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this licence number is already taken' ],200);

			}				

				return Response(['success' => 1, 'statuscode' => 200, 'msg' => 'Validated successfully' ],200);

		}
	catch(\Exception $e)
		{
			return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
		}
	}

////////////////////////////
//////////////		Drivers Check 		//////////////////////////////////
////////////////////////////


///////////////////////		Products Listing Dynamically 		/////////////////
public static function brand_products(Request $request)
	{
	try{

			$rules = array(
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Response(['success' => 0, 'statuscode' => 400, 'msg' => $validator->getMessageBag()->first() ],400);				

			$products = CategoryBrandProduct::brand_all_products_json($request->all());

			$con = '';
    		foreach ($products as $product) 
	    		{

	    			if(isset($request['category_brand_product_id']) && ($product->category_brand_product_id == $request['category_brand_product_id']) )
	    			{
	    				$con = $con.'<option value="'.$product->category_brand_product_id.'" selected>'.$product->name.'</option>';
	    			}
	    			else
	    			{
	    				$con = $con.'<option value="'.$product->category_brand_product_id.'">'.$product->name.'</option>';	
	    			}

					
	    		}

			return Response(['success' => 1, 'statuscode' => 200, 'con' => $con, 'products' => $products ],200);

		}
	catch(\Exception $e)
		{
			return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
		}
	}
        
        public function autocomplete(Request $request){
            
                $request['blocked_check']  = "Active";
        $custs = UserDetail::where('user_type_id',1);
        ///////////////        Filters     ////////////////////////
        if(isset($request['blocked_check']) && $request['blocked_check'] == 'Active' )
            $custs->where('user_details.blocked','0');
        ///////////////        Filters     ////////////////////////
        $custs->join('users','users.user_id','user_details.user_id');
        $custs->select('*');
        $custs->where("users.name","LIKE","%".$request['q']."%");
        $custs->orderBy('user_detail_id', 'DESC');
        return $custs->get();
    }

///////////////////////	Driver Update Product Listings 	///////////////////////////
public static function dupdate_product_list(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id',
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Response(['success' => 0, 'statuscode' => 400, 'msg' => $validator->getMessageBag()->first() ],400);
				

			$products = CategoryBrandProduct::brand_driver_products_json($request->all());

			$con = '';
                        
    		foreach ($products as $product) 
	    		{

				if($product->selected == '1')
					$con = $con.'<option value="'.$product->category_brand_product_id.'" selected="true">'.$product->name.'</option>';
				else
					$con = $con.'<option value="'.$product->category_brand_product_id.'">'.$product->name.'</option>';

	    		}

			return Response(['success' => 1, 'statuscode' => 200, 'con' => $con, 'products' => $products ],200);

		}
	catch(\Exception $e)
		{
			return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
		}
	}
        
        ///////////////////////    Coupans Selected Users Listings     ///////////////////////////
    public static function dupdate_users_list(Request $request)
    {
        try{
            $data = Input::all();
            $dataa = array();    
            $dataa['blocked_check']  = "Active";
            $users                  = UserDetail::cust_listings($dataa);
            $Selectedusers          = UserDetail::getUserList($request->all());
            $con = '';
           foreach ($users as $use)
            {
                if(in_array($use->user_id,$Selectedusers)){
                    $con = $con.'<option value="'.$use->user_id.'" selected="true">'.$use->name.'</option>';
                }else{
                    $con = $con.'<option value="'.$use->user_id.'">'.$use->name.'</option>';
                }
            }
            return Response(['success' => 1, 'statuscode' => 200, 'con' => $con ],200);
        }
        catch(\Exception $e)
        {
            return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
        }
    }
    
    ///////////////////////        Brands With Selected Listing Dynamically         /////////////////
    public static function dupdate_coupon_brands_listings(Request $request)
    {
        try{
            
            $categorybrand = DB::table('coupons')->select('brand_id')->where('coupon_id',$request['coupon_id'])->get();
            if(count($categorybrand) > 0){
                $userslit = $categorybrand[0]->brand_id;
            }else{
                $userslit = '0';
            }
            
            $brands = CategoryBrand::admin_brands_list_add_drivers($request->all());
            $con = '';
            $category_brand_id = 0;
            $con = '<option value="">Select Option</option>';
           foreach ($brands as $key => $brand)
            {
                if($key == 0)
                    $category_brand_id = $brand->category_brand_id;

                    if($brand->category_brand_id == $userslit)
                    {
                        $con = $con.'<option value="'.$brand->category_brand_id.'" selected="true">'.$brand->name.'</option>';
                    }else{
                        $con = $con.'<option value="'.$brand->category_brand_id.'">'.$brand->name.'</option>';
                    }
            }
            return Response(['success' => 1, 'statuscode' => 200, 'con' => $con],200);
        }
        catch(\Exception $e)
        {
            return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
        }
    }
    
     ///////////////////////        Products  With Selected Listing Dynamically         /////////////////
    public static function dupdate_coupon_brand_products(Request $request)
    {
        try{
            $rules = array(
                'category_brand_id' => 'required|exists:category_brands,category_brand_id',
            );

            $validator = Validator::make($request->all(),$rules);
            if($validator->fails())
                return Response(['success' => 0, 'statuscode' => 400, 'msg' => $validator->getMessageBag()->first() ],400);                

            $products      = CategoryBrandProduct::brand_all_products_json($request->all());
            
            $categorybrand = DB::table('promocode_products')->select('product_id')->where('coupon_id',$request['coupon_id'])->get();
            $rowData = array();
            if(count($categorybrand) > 0){
                for($i = 0;$i<count($categorybrand);$i++)
                {
                    $rowData[] = $categorybrand[$i]->product_id;
                }
                $userslit = $rowData;
            }else{
                $userslit = array();
            }
            
            //$con = '';
            $con = '<option value="fdgfdgfdg">Select Option</option>';
            foreach ($products as $product)
            {
                //if($product->category_brand_product_id == $userslit)
                if(in_array($product->category_brand_product_id,$userslit))
                {
                    $con = $con.'<option value="'.$product->category_brand_product_id.'" selected="true">'.$product->name.'</option>';
                }else{
                    $con = $con.'<option value="'.$product->category_brand_product_id.'">'.$product->name.'</option>';
                }
            }
            
            /*$categorybrand = DB::table('coupons')->select('product_id')->where('coupon_id',$request['coupon_id'])->get();
            if(count($categorybrand) > 0){
                $userslit = $categorybrand[0]->product_id;
            }else{
                $userslit = '0';
            }
            
            $con = '';
            foreach ($products as $product)
            {
                if($product->category_brand_product_id == $userslit)
                {
                    $con = $con.'<option value="'.$product->category_brand_product_id.'" selected="true">'.$product->name.'</option>';
                }else{
                    $con = $con.'<option value="'.$product->category_brand_product_id.'">'.$product->name.'</option>';
                }
            }*/
            //return Response(['success' => 1, 'statuscode' => 200, 'con' => $con, 'products' => $products ],200);
            return Response(['success' => 1, 'statuscode' => 200, 'con' => $con],200);


        }
        catch(\Exception $e)
        {
            return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
        }
    }

//////////////////		Organisation Unique CHeck 	/////////////////////////
////////////////////		Unique Ness CHeck 		///////////////////////////////////////////////////
public static function org_unique_check(Request $request)
{
try{

		// if(isset($request['email']))
		// 	{

		// 		$ocheck = Organisation::where('email', $request['email'])->first();
		// 		if($ocheck)
		// 			return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this email is already taken' ],200);

		// 	}
		// else
		if(isset($request['username']))
			{
				
				$ocheck = Organisation::where('username', $request['username'])->first();
				if($ocheck)
					return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this username is already taken' ],200);

			}
		elseif(isset($request['licence_number']))
			{
				
				$ocheck = Organisation::where('licence_number', $request['licence_number'])->first();
				if($ocheck)
					return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this licence number is already taken' ],200);

			}
		elseif(isset($request['phone_number']))
			{
				
				$ocheck = Organisation::where('phone_number', $request['phone_number'])->first();
				if($ocheck)
					return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this phone number is already taken' ],200);

			}

			return Response(['success' => 1, 'statuscode' => 200, 'msg' => 'Validated successfully' ],200);

	}
catch(\Exception $e)
	{
		return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
	}
}

//////////////////		Organisation Update Unique Check 	/////////////////
public static function org_uunique_check(Request $request)
{
try{

		// if(isset($request['email']))
		// 	{

		// 		$ocheck = Organisation::where('organisation_id','!=', $request['organisation_id'])->where('email', $request['email'])->first();
		// 		if($ocheck)
		// 			return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this email is already taken' ],200);

		// 	}
		// else
		if(isset($request['phone_number']))
			{
				
				$ocheck = Organisation::where('organisation_id','!=', $request['organisation_id'])->where('phone_number', $request['phone_number'])->first();
				if($ocheck)
					return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this phone number is already taken' ],200);

			}

			return Response(['success' => 1, 'statuscode' => 200, 'msg' => 'Validated successfully' ],200);

	}
catch(\Exception $e)
	{
		return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
	}
}

/////////////////	Unique Code Check Add 	//////////////////////////////////////////////
public static function coupon_unique_check(Request $request)
	{
	try{

		if(isset($request['code']))
			{
					
				$ocheck = Coupon::where('code', $request['code'])->first();
				if($ocheck)
					return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this coupon code is already taken' ],200);

			}

			return Response(['success' => 1, 'statuscode' => 200, 'msg' => 'Validated successfully' ],200);

		}
	catch(\Exception $e)
		{
			return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
		}
	}

///////////////////		Update Coupon Unique Checks 		/////////////////////////
public static function coupon_uunique_check(Request $request)
	{
	try{

		if(isset($request['code']))
			{
					
				$ocheck = Coupon::where('code', $request['code'])->where('coupon_id', '!=' ,$request['coupon_id'])->first();
				if($ocheck)
					return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this coupon code is already taken' ],200);

			}

			return Response(['success' => 1, 'statuscode' => 200, 'msg' => 'Validated successfully' ],200);

		}
	catch(\Exception $e)
		{
			return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
		}
	}

//////////////////		Brand Listings 	//////////////////////////////////////////////
public static function brands_listings(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'category_id' => 'required|exists:categories,category_id',
			);

			$request['timezonez'] = "+04:00";

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Response(['success' => 0, 'statuscode' => 400, 'msg' => $validator->getMessageBag()->first() ],400);
				
			$brands = CategoryBrand::admin_brands_list_add_drivers($request->all());

			$con = '';
			$category_brand_id = 0;
                        $con = '<option value="">Select Option</option>';
    		foreach ($brands as $key => $brand)
	    		{
	    			if($key == 0)
	    				$category_brand_id = $brand->category_brand_id;

					$con = $con.'<option value="'.$brand->category_brand_id.'">'.$brand->name.'</option>';
	    		}

	    	$ranking = CategoryBrandProduct::where('category_id', $request['category_id'])->where('category_brand_id',$category_brand_id)->count();
	    	$ranking_options = "";
	    	for($i=1;$i<=$ranking+1;$i++)
	    	{
	    		if($i == $ranking+1)
	    			$ranking_options = $ranking_options.'<option value="'.$i.'" selected>'.$i.'</option>';
	    		else
	    			$ranking_options = $ranking_options.'<option value="'.$i.'">'.$i.'</option>';
	    	}

			return Response(['success' => 1, 'statuscode' => 200, 'con' => $con, 'brands' => $brands, 'ranking_options' => $ranking_options, 'total' => $ranking ],200);

		}
	catch(\Exception $e)
		{
			return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
		}
	}

	
/**
*@role Get Notification Liting
*@author Raghav
*/
public static function notification_listing(Request $request)
	{
	try{
			$Notification = UserOrderNotification::orderBy('user_order_notification_id', 'DESC')->where('is_read','0')->get();
			$con = '';
			$con = '<div id="notificationTitle">Notifications <span class="close hidenotification">x</span></div>';
			$NotificationStatus = 'Notification';
			$i = 1;
			$totalNotification = count($Notification);
			if($totalNotification == 0)
			{
				$con = $con . "<div class='notification-item'><span style='background:blue' class='notification-span'></span>" .
						"<div class='notification-comment'><a style='text-decoration:none;color:black'>No Notification received</a></div>" .
						"</div>";
				$NotificationStatus = "No Notification";
			}
    		foreach ($Notification as $message)
			{
				$orderId =  $message['order_id'];
				//Get User Name
				$users = DB::table('orders')
				->join('user_details', 'orders.customer_user_detail_id', '=', 'user_details.user_detail_id')
				->join('users', 'user_details.user_id', '=', 'users.user_id')
				->where('orders.order_id',$orderId)
				->select('users.name')
				->get();
				//Get Service Name
				$CategoryName = DB::table('orders')
						->join('category_details', 'orders.category_id', '=', 'category_details.category_id')
						->where('orders.order_id',$orderId)
						->where('category_details.language_id','1')
						->select('category_details.name')
						->get();
				$CategoryName = $CategoryName[0]->name." service";
				
				
				if($message['type'] == 'SerRequest')
				{
					$messages = str_replace("service",$CategoryName,$message['message']);
					//$messages = $message['message'];
				}
				else
				{
					$name      = $users[0]->name;
					$messages  = str_replace("your",$name,$message['message']);
					$messages  = $messages." for ".$CategoryName;
				}
				
				if($message['type'] == 'SerComplete')
				{
					$color = "#1ab394";
					
				}else if($message['type'] == 'DSchCancelled')
				{
					$color = "#ed5565";
				}else
				{
					$color = "#f8ac59";
				}

				$url = route('admin_order_details').'?order_id='.$orderId.'';
				$con = $con . "<div class='notification-item'><span style='background:".$color."' class='notification-span'></span>".
						"<div class='notification-comment'><a style='text-decoration:none;color:black' target='_blank' href=".$url.">".$i.". ".$messages."</a></div>" .
						"</div>";
						$i++;
						
				if($request['is_read'] == '1')
				{
					DB::table('user_order_notifications')
						->where('user_order_notification_id', $message['user_order_notification_id'])
						->update(['is_read' => 1]);
				}
			}
			
			
			return Response(['success' => 1, 'statuscode' => 200, 'con' => $con,'totalNotification'=> $totalNotification,'NotificationStatus'=>$NotificationStatus],200);

	}
	catch(\Exception $e)
		{
			return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
		}
	}
	
	
///////////////////////			Brands Ranking List 		//////////////////////////////////
public static function brands_ranking_listings(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Response(['success' => 0, 'statuscode' => 400, 'msg' => $validator->getMessageBag()->first() ],400);

	    	$ranking = CategoryBrandProduct::where('category_brand_id',$request['category_brand_id'])->count();
	    	$ranking_options = "";
	    	for($i=1;$i<=$ranking+1;$i++)
		    	{
		    		if($i == $ranking+1)
		    			$ranking_options = $ranking_options.'<option value="'.$i.'" selected>'.$i.'</option>';
		    		else
		    			$ranking_options = $ranking_options.'<option value="'.$i.'">'.$i.'</option>';
		    	}

			return Response(['success' => 1, 'statuscode' => 200, 'ranking_options' => $ranking_options, 'total' => $ranking ],200);

		}
	catch(\Exception $e)
		{
			return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
		}
	}


        public function unserializeUsers(Request $params) { 
            try{ 
                return Response(['success' => 1, 'statuscode' => 200, 'data' => unserialize($params->users) ],200);
            } catch (Exception $e) {
                return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
            }
            
        }
        
        public static function getpromousersrides(Request $request)
	{
		try{
                    $data = $request->all();
                    //dd($data);
			//$data = CommonController::set_starting_ending($request->all());
                    $coupons = Coupon::users_coupons_get_maxrides($data['coupon_id']);
                    $ranking_options = "";
                    
                    for($i=0;$i<count($coupons);$i++)
                    {

                        $ranking_options = $ranking_options.'<tr><td>'.$coupons[$i]->name.'</td><td>'.$coupons[$i]->max_rides.'</td></tr>';
                    }
                    return response()->json($ranking_options);
                    //return View::make('Admin.Promocode.allCoupons', compact('coupons'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);
		}
		catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}
		
		/**
		*@role Get Payment Details
		*@date 13-09-2019
		*
		*/
		public function payment_order_detail(Request $request)
		{
			try{
				$data    = $request->all();
				$orderId = $data['orderId'];
				$order   = DB::table('orders')
					->select('category_details.name as categoryName','users.name','users.buraq_percentage')
					->join('category_details','category_details.category_id','=','orders.category_id')
					->join('users','users.user_id','=','orders.customer_user_id')
					->where('category_details.language_id', '1')
					->where('orders.order_id',$orderId)
					->get();
				$TotalProducts = DB::table('order_products')->where('order_id',$orderId)->sum('product_quantity');
				@$order[0]->totalProducts = (int)$TotalProducts;
				return Response(['success' => 1,'order' => $order[0] ],200);
			}
			catch(\Exception $e)
			{
				return Redirect::back()->withErrors($e->getMessage());
			}
		}
		
		/**
		*@role Get Payment Details
		*@date 13-09-2019
		*
		*/
		public function payment_confirm_order_detail(Request $request)
		{
			try{$data    = $request->all();
				$orderId = $data['orderId'];
				$TransId = $data['transId'];
				$orderupdate = DB::table('payments')
					->where('order_id', $orderId)
					->update(['payment_status' => "Completed",'payment_type' => "Card",'user_card_id' => 0,'transaction_id'=>$TransId,'updated_at'=>new \DateTime]);
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, env('SOCKET_URL')."/user/service/paymentsuccess");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, "orderId=" . $orderId . "");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$server_output = curl_exec($ch);
				curl_close($ch);
				return Response(['success' => 1,'order' => "true" ],200);
			}
			catch(\Exception $e)
			{
				return Redirect::back()->withErrors($e->getMessage());
			}
		}

///////////////////////////

}
