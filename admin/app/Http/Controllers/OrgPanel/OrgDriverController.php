<?php

namespace App\Http\Controllers\OrgPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Mail;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Request as ORequest;

use App\Http\Controllers\CommonController;

use App\Models\Category;
use App\Models\CategoryBrandProduct;
use App\Models\Order;
use App\Models\UserDetail;
use App\Models\UserDriverDetailProduct;
use App\Models\OrderRating;

class OrgDriverController extends Controller
{

/////////////////////////		New All Driver Listings 	///////////////////////////////////////////////
public static function org_services_dall_new(Request $request)
	{
	try{

			$request['user_type_ids'] = [2];

			$data = CommonController::set_starting_ending($request->all());

			$data['category_type'] = 'Service';
			$services = Category::admin_get_service_categories($data);

			$drivers = UserDetail::admin_drivers_listings($data);

			return View::make('OrgPanel.Drivers.Services.orgAllSerDriversNew',compact('drivers', 'services', 'data'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

////////////////////		Track Drivers    /////////////////////////
public static function org_drivers_track(Request $request)
	{

		$request['category_type'] = 'Service';
		$services = Category::admin_org_cat_single_lang($request->all());

		$request['category_type'] = 'Support';
		$supports = Category::admin_org_cat_single_lang($request->all());

		return View::make('OrgPanel.Drivers.orgTrackDrivers', compact("services", "supports"));

	}



////////////////////		All Services All Drivers 		////////////////////////
public static function org_services_dall (Request $request)
	{
	try{

			$data = CommonController::order_set_starting_ending($request->all());

			$data['category_type'] = 'Service';
			$services = Category::admin_org_cat_single_lang($data);

			$data['user_type_ids'] = [2];
			$drivers = UserDetail::admin_drivers_listing($data);

			return View::make('OrgPanel.Drivers.Services.orgAllSerDrivers', compact('services', 'drivers'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

/////////////////////////		Driver Orders 	///////////////////////////////////////////////
public static function org_ser_dorders(Request $request)
	{
	try{

			$rules = array(
				'driver_user_detail_id' => 'required|exists:user_details,user_detail_id,user_type_id,2',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$data = CommonController::order_set_starting_ending($request->all());

			$data['user_detail_id'] = $request['driver_user_detail_id'];
			$driver = UserDetail::admin_driver_detail_update_get($data);
			
			//$data['order_type_ids'] = ["Service"];
			//$orders = Order::admin_orders_list($data);

			$request['category_id'] = $driver->category_id;
			$category = Category::admin_single_cat_details($request->all());

			if(!isset($data['status_check']))
				$data['status_check'] = 'All';

			if(!isset($data['order_type_check']))
				$data['order_type_check'] = 'All';

			if(!isset($data['status_check']))
				$data['status_check'] = 'All';

			if(!isset($data['categories']))
				$data['categories'] = [0];

			if(!isset($data['ordering_filter']))
				$data['ordering_filter'] = 1;


			$orders = Order::org_orders_listings($data);

			$data['daterange'] = $data['fstarting_dt'].' - '.$data['fending_dt'];

			return View::make('OrgPanel.Drivers.Services.orgAllSerDOrdersNew', compact('driver', 'orders', 'category', 'data'));

			//return View::make('OrgPanel.Drivers.Services.orgAllSerDOrders', compact('driver', 'orders', 'category'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

////////////////////////////		Reviews 	////////////////////////////////////////////////
public static function org_ser_dreviews(Request $request)
	{
	try{

			$rules = array(
				'driver_user_detail_id' => 'required|exists:user_details,user_detail_id,user_type_id,2',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$data = CommonController::set_starting_ending($request->all());

			$data['user_detail_id'] = $request['driver_user_detail_id'];
			$driver = UserDetail::admin_driver_detail_update_get($data);

			$data['created_bys'] = ["Customer", "Driver"];
			$reviews = OrderRating::admin_all_reviews($data);

			return View::make('OrgPanel.Drivers.Services.orgAllSerDReviews', compact('driver', 'reviews'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////////



////////////////////////////
///////////////////////////////		Support 		/////////////////////////////////////
////////////////////////////

//////////////////		All Support Driverrs 		//////////////////////////////// 
public static function org_supports_dall (Request $request)
	{
	try{

			$data = CommonController::order_set_starting_ending($request->all());

			$data['category_type'] = 'Support';
			$supports = Category::admin_org_cat_single_lang($data);

			$data['user_type_ids'] = [3];
			$drivers = UserDetail::admin_drivers_listing($data);

			return View::make('OrgPanel.Drivers.Supports.orgAllSupportsDrivers', compact('supports', 'drivers'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

////////////////////		

}
