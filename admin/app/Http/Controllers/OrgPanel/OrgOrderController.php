<?php

namespace App\Http\Controllers\OrgPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Mail;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Request as ORequest;

use App\Http\Controllers\CommonController;

use App\Models\CategoryBrandProduct;
use App\Models\Order;
use App\Models\Category;

class OrgOrderController extends Controller
{
///////////////		Org All Orders    /////////////////////////////////
public static function org_orders(Request $request)
	{
	try{

			$data = CommonController::set_starting_ending($request->all());

			if(!isset($data['status_check']))
				$data['status_check'] = 'All';

			if(!isset($data['categories']))
				$data['categories'] = [0];

			if(!isset($data['ordering_filter']))
				$data['ordering_filter'] = 1;

			$categories = Category::all_categories_single_lang($data['admin_language_id'], implode(',', $data['categories']));

			$orders = Order::org_orders_listings($data);

			$data['daterange'] = $data['fstarting_dt'].' - '.$data['fending_dt'];

			return View::make('OrgPanel.Orders.orgOrdersNew', compact('orders', 'data', 'categories'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

///////////////			Order Details 		/////////////////////////////
public static function org_order_details(Request $request)
	{
	try{

			$rules = array(
					'order_id'=>'required|exists:orders,order_id'
						);
			$msg = array('order_id.exists'=>'Sorry, this order is currently not available');

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$order = Order::order_details($request->all());
			$orderProduct = Order::order_details_products($request->all());
			if(isset($order->CRequest))
	 		$order->CRequest['full_track'] = json_decode($order->CRequest['full_track'], true);
			return View::make('OrgPanel.Orders.orgOrderDetails', compact('order','orderProduct'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}
	
	public static function assignDriver(Request $request)
	{
		$orderId  = $request['orderId'];
		$driverId = $request['driverId'];
		$cards = DB::table('orders')
            ->where('order_id', $orderId)
            ->update(['order_status' => "Searching"]);
		$cards = DB::table('order_requests')
		->where('order_id', $orderId)
		->update(['order_request_status' => "Searching"]);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://phytotherapy.in:8051/user/service/ReassignOrder");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "orderId=" . $orderId . "&driverId=" . $driverId ."");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec($ch);
		curl_close($ch);
		
		return "true";
	}


///////////////////


}
