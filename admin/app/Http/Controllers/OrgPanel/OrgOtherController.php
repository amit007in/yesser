<?php

namespace App\Http\Controllers\OrgPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Mail;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Request as ORequest;

use App\Http\Controllers\CommonController;

use App\Models\Category;
use App\Models\CategoryBrand;
use App\Models\CategoryBrandProduct;
use App\Models\Order;
use App\Models\OrderRating;
use App\Models\OrganisationCoupon;
use App\Models\PaymentLedger;

class OrgOtherController extends Controller
{
///////////////////////		Reviews    ///////////////////////////////
public static function org_review(Request $request)
	{
	try{

		$data = CommonController::order_set_starting_ending($request->all());

		$data['order_filter'] = isset($data['order_filter']) ? $data['order_filter'] : 'All';
		$data['createdby_filter'] = isset($data['createdby_filter']) ? $data['createdby_filter'] : 'All';
		$data['search'] = isset($data['search']) ? $data['search'] : '';

		//$reviews = OrderRating::org_all_reviews($data);
		$reviews = OrderRating::admin_reviews_new($data);

		return View::make("OrgPanel.Reviews.orgReviews", compact('reviews', 'data'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

//////////////////////////	Coupons 		/////////////////////////////
public static function org_ser_coupons(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$data = CommonController::set_starting_ending($request->all());

			$category = Category::admin_single_cat_details($request->all());
			$coupons = OrganisationCoupon::org_coupons_all($data);

			return View::make('OrgPanel.Services.Etokens.orgETokens', compact('category', 'coupons'))->with('category_id', $request['category_id'])->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////			Org Coupon Add 		//////////////////////////
public static function org_coupon_add(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$category = Category::admin_single_cat_details($request->all());
			$brands = CategoryBrand::admin_brands_list_add_drivers($request->all());

			return View::make('OrgPanel.Services.Etokens.orgETokenAdd', compact('category', 'brands'))->with('category_id', $request['category_id']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////////		Org Coupon Add Post 	//////////////////////////
public static function org_coupon_padd(Request $request)
	{
	try{

			$rules = array(
				'price' => 'required|min:0',
				'quantity' => 'required:min:1',
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
				'category_brand_product_id' => 'required|exists:category_brand_products,category_brand_product_id'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$request['created_by'] = 'Org';

			$user = OrganisationCoupon::insert_new_record($request->all());

			return Redirect::back()->with('status','EToken added successfully');
		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////			Org Coupon Update 		//////////////////////////
public static function org_coupon_update(Request $request)
	{
	try{

			$rules = array(
				'organisation_coupon_id' => 'required|exists:organisation_coupons,organisation_coupon_id,organisation_id,'.$request['organisation_id']
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$coupon = OrganisationCoupon::where('organisation_coupon_id', $request['organisation_coupon_id'])->first();

			$request['category_id'] = $coupon->category_id;

			$category = Category::admin_single_cat_details($request->all());
			$brands = CategoryBrand::admin_brands_list_add_drivers($request->all());

			return View::make('OrgPanel.Services.Etokens.orgETokenUpdate', compact('coupon', 'category', 'brands'))->with('category_id', $request['category_id']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////			Org Coupon Update Post 		//////////////////////////
public static function org_coupon_pupdate(Request $request)
	{
	try{

			$rules = array(
				'organisation_coupon_id' => 'required|exists:organisation_coupons,organisation_coupon_id,organisation_id,'.$request['organisation_id'],
				'price' => 'required|min:0',
				'quantity' => 'required:min:1',
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
				'category_brand_product_id' => 'required|exists:category_brand_products,category_brand_product_id'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			OrganisationCoupon::update_record($request->all());

			return Redirect::back()->with('status','EToken updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////////		Coupon Block Unblock 	//////////////////////////////////
public static function org_coupon_block(Request $request)
	{
	try{

			$rules = array(
				'organisation_coupon_id' => 'required|exists:organisation_coupons,organisation_coupon_id,organisation_id,'.$request['organisation_id'],
			);

			$validator = Validator::make($request->all(),$rules);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$coupon = OrganisationCoupon::where('organisation_coupon_id',$request['organisation_coupon_id'])->first();

			if($request['block'] == 1)
				{

					if($coupon->blocked == '1')
						return Redirect::back()->withErrors('This EToken is already blocked')->withInput();

					OrganisationCoupon::where('organisation_coupon_id',$coupon->organisation_coupon_id)->update(array(
						'blocked' => '1',
						'updated_at' =>new \DateTime
					));

					return Redirect::back()->with('status','EToken blocked successfully');

				}
			else
				{

					if($coupon->blocked == '0')
						return Redirect::back()->withErrors('This EToken is already unblocked')->withInput();

					OrganisationCoupon::where('organisation_coupon_id',$coupon->organisation_coupon_id)->update(array(
						'blocked' => '0',
						'updated_at' => new \DateTime
					));

					return Redirect::back()->with('status','EToken unblocked successfully');

				}

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

/////////////////////////		Organisation Ledgers 		/////////////////////////////////////
public static function org_ledgers(Request $request)
	{
	try{

			$data = CommonController::order_set_starting_ending($request->all());

			$ledgers = PaymentLedger::admin_all_ledgers($data);

			return View::make('OrgPanel.Ledgers.orgLedgers', compact('ledgers'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

///////////////////////////

}
