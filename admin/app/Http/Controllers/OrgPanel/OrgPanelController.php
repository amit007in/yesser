<?php

namespace App\Http\Controllers\OrgPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Mail;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Request as ORequest;

use App\Http\Controllers\CommonController;

use App\Models\Admin;
use App\Models\AdminUserDetailLogin;
use App\Models\Category;
use App\Models\Organisation;
use App\Models\OrganisationCategory;
use App\Models\CategoryBrandProduct;
use App\Models\OrganisationCategoryBrandProduct;

class OrgPanelController extends Controller
{

//////////////		Org Login Get 	////////////////////////////////////////////////////////////
public static function org_panel_login()
	{
		return View::make('OrgPanel.Other.orgLogin');
		//return View::make('Admin.Other.login');
	}

//////////////		Org Login Post 	////////////////////////////////////////////////////////////
public static function org_panel_plogin(Request $request)
	{
	try{
			$rules = array(
				'username'=>'required|exists:organisations,username',
				'password'=>'required',
				'timezone'=>'required|timezone');

			$validator = Validator::make($request->all(),$rules);
				if($validator->fails())
					return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();

			$org = Organisation::panel_login_get($request->all());
			if(!\Hash::Check($request['password'], $org->password))
				return Redirect::back()->witherrors('Password is Incorrect')->withInput();

		 	$time = new \DateTime('now', new \DateTimeZone($request['timezone']));
			$request['timezonez'] = $time->format('P');

			$request['organisation_id'] = $org->organisation_id;
			$request['user_type_id'] = 4;
			$request['ip_address'] = $request['ip_address'];// $_SERVER['HTTP_X_FORWARDED_FOR'];

			// AdminUserDetailLogin::where("organisation_id",$request['organisation_id'])->where('user_type_id',$request['user_type_id'])->where('ip_address',$request['ip_address'])->where("access_token","!=","")->update([
			// 		'access_token' => "",
			// 		'otp' => '',
			// 		'updated_at' => new \DateTime
			// ]);

			$admin_user_login_detail = AdminUserDetailLogin::insertNewRow($request->all());

			\Cookie::queue('OrgAccessToken', $admin_user_login_detail->access_token , 60*60);
			\Cookie::queue('OrgLanguage', $admin_user_login_detail->language_id , 60*60);

			return Redirect::route('org_dashboard')->with('successMsg','Logged In Successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

////////////////////		Org Register 	//////////////////////////////////////////////
public static function org_register(Request $request)
	{
	try{
			$services = Category::service_support_listings("Service", 1);
			$supports = Category::service_support_listings("Support", 1);

			return View::make("OrgPanel.Other.orgRegister", compact('services', 'supports'));
		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

////////////////////		Org Register Post 	/////////////////////////////////////////
public static function org_register_post(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'name' => 'required',
				'image' => 'required|image',
				'phone_code' => 'required',
				'phone_number' => 'required|unique:organisations,phone_number',
				'username' => 'required|unique:organisations,username',
				'licence_number' => 'required|unique:organisations,licence_number',
				'password' => 'required',
				'servicesid' => 'sometimes|array',
				//'supportsid' => 'sometimes|array',
				'address_latitude' => 'required',
				'address_longitude' => 'required'
			);
			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

////////////////////////////		Images Upload 		////////////////////////////////
			$request['image_name'] = CommonController::image_uploader(Input::file('image'));
				if(empty($request['image_name']))
					return Redirect::back()->withErrors('Sorry, image not uploaded. Please try again')->withInput($request->all());
////////////////////////////		Images Upload 		////////////////////////////////
			$admin = Admin::first();
			$request['buraq_percentage'] = $admin->buraq_percentage;
			$request['credit_start_dt'] = null;

			$request['created_by'] = "Org";
			$request['email'] = CommonController::generate_email();

			$request['sort_order'] = Organisation::count()+1;

			$organisation = Organisation::add_new_record($request->all());
			$request['organisation_id'] = $organisation->organisation_id;

			$con = [];
			if(isset($request['servicesid']) && !empty($request['servicesid']) )
				{
					foreach($request['servicesid'] as $serviceid)
						{
							$con[] = [
								'organisation_id' => $request['organisation_id'],
								'category_id' => $serviceid,
								'buraq_percentage' => $request['buraq_percentage'],
								'category_type' => 'Service',
								'created_at' => new \DateTime,
								'updated_at' => new \DateTime
							];
						}
				}

			if(isset($request['supportsid']) && !empty($request['supportsid']) )
				{
					foreach($request['supportsid'] as $supportid)
						{
							$con[] = [
								'organisation_id' => $request['organisation_id'],
								'category_id' => $supportid,
								'buraq_percentage' => $request['buraq_percentage'],
								'category_type' => 'Support',
								'created_at' => new \DateTime,
								'updated_at' => new \DateTime
							];
						}
				}

			if(!empty($con))
				OrganisationCategory::insert($con);

//////////////////		Org Products with Charges Dynamic 		//////////////////////////////////
		if(isset($request['servicesid']) && !empty($request['servicesid']) )
			{

				$con = [];
				$products = CategoryBrandProduct::whereIn('category_id',$request['servicesid'])->get();
				foreach($products as $product)
					{
						$con[] = [
							'organisation_id' => $request['organisation_id'],
							'category_id' => $product->category_id,
							'category_brand_id' => $product->category_brand_id,
							'category_brand_product_id' => $product->category_brand_product_id,
							'alpha_price' => $product->alpha_price,
							'price_per_quantity' => $product->price_per_quantity,
							'price_per_distance' => $product->price_per_distance,
							'price_per_weight' => $product->price_per_weight,
							'price_per_hr' => $product->price_per_hr,
							'price_per_sq_mt' => $product->price_per_sq_mt,
							'created_at' => new \DateTime,
							'updated_at' => new \DateTime
						];
					}

				if(!empty(($con)))
					OrganisationCategoryBrandProduct::insert($con);

			}
//////////////////		Org Products with Charges Dynamic 		//////////////////////////////////

			$request['sms'] = CommonController::send_sms([ 'phone_number' => $request['phone_number'], 'message' => 'You are registered as an organisation in '.env('APP_NAME') ]);

			return Redirect::route('org_panel_login2')->with('status','Registered successfully. Please login now');

			//return Redirect::back()->with('status','Registered successfully. Please login now');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

/////////////////////		Org DashBoard 		//////////////////////////////////////////
public static function org_dashboard(Request $request)
	{
	try{
			//$OrgDetails = \App("OrgDetails");

		//dd(get_browser( $request->header('User-Agent') , true));

			$starting_dt = Carbon::now($request['timezonez'])->subMonths(2)->format('d/m/Y');
			$ending_dt = Carbon::now($request['timezonez'])->addday(1)->format('d/m/Y');

			return View::make('OrgPanel.orgDash')->with('starting_dt',$starting_dt)->with('ending_dt',$ending_dt);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

///////////////////////////////////			Dashboard Data 		////////////////////////////////////////////////

public static function org_dashboard_data(Request $request)
	{
	try{
			$org = \App('OrgDetails');

			$request['daterange'] = urldecode($request['daterange']);
			$temp_array = explode(' - ', $request['daterange']);

			$request['starting_dt'] = Carbon::CreateFromFormat('m/d/Y',$temp_array[0],$request['timezonez'])->timezone('UTC')->format('Y-m-d');
			$request['ending_dt'] = Carbon::CreateFromFormat('m/d/Y',$temp_array[1],$request['timezonez'])->timezone('UTC')->format('Y-m-d');

			$request['starting_dt'] = $request['starting_dt'].' 00:00:00';
			$request['ending_dt'] = $request['ending_dt'].' 23:59:59';

			if(isset($request['latitude']) && isset($request['longitude']) && $request['latitude'] != "null" && $request['longitude'] != "null")
				{
					AdminUserDetailLogin::where('admin_user_detail_login_id', $org->admin_user_detail_login_id)->limit(1)->update([
							'latitude' => $request['latitude'],
							'longitude' => $request['longitude'],
							'updated_at' => new \DateTime
					]);
				}

			$stats_data = OrgPanelController::stats_data($request->all());
			$graph_data = OrgPanelController::new_graph_data($request->all());

			return Response(array('success'=>'1', 'stats'=>$stats_data[0], 'graph_data'=>$graph_data),200);

		}
	catch(\Exception $e)
		{
			return Response(array('success'=>'0','msg'=>$e->getMessage()),200);
		}
	}

//////////////		Admin DashBoard Data		///////////////////////////////////////////////

	public static function stats_data($data)
		{

return DB::SELECT("SELECT
		*,
		(SELECT COUNT(*) FROM organisation_categories as oc WHERE oc.organisation_id = '".$data['organisation_id']."' AND category_type='Service') as service_counts,
		(SELECT COUNT(*) FROM organisation_categories as oc WHERE oc.organisation_id = '".$data['organisation_id']."' AND category_type='Support') as support_counts,
		(SELECT COUNT(*) FROM orders as o WHERE o.driver_organisation_id = '".$data['organisation_id']."' AND order_timings BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' ) as order_counts,
		(SELECT COUNT(*) FROM user_details as ud WHERE ud.organisation_id= '".$data['organisation_id']."' AND ud.user_type_id=2 AND ud.created_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' ) as dservice_counts,
		(SELECT COUNT(*) FROM user_details as ud WHERE ud.organisation_id= '".$data['organisation_id']."' AND ud.user_type_id=3 AND ud.created_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' ) as dsupport_counts
	FROM organisations as o
	WHERE o.organisation_id = '".$data['organisation_id']."'
	LIMIT 0, 1
	");

		}

/////////////////////		Graph Data 		/////////////////////////////////////
public static function new_graph_data($data)
	{

	    if(!isset($data['yr_only']))
	        $yr_only = Carbon::now($data['timezonez'])->format('Y');
	    else
	        $yr_only  = $data['yr_only'];

	$querys = DB::SELECT("SELECT
					months_tbl.id as id,
	CAST((SELECT COUNT(*) FROM orders as o WHERE o.driver_organisation_id = '".$data['organisation_id']."' AND YEAR(o.order_timings) = :yr1 AND MONTH(o.order_timings) = months_tbl.id) AS UNSIGNED) as orders,
	CAST((SELECT COUNT(*) FROM user_details as ud WHERE ud.organisation_id= '".$data['organisation_id']."' AND ud.user_type_id=2 AND YEAR(ud.created_at) = :yr2 AND MONTH(ud.created_at) = months_tbl.id) AS UNSIGNED) as dservices,
	CAST((SELECT COUNT(*) FROM user_details as ud WHERE ud.organisation_id= '".$data['organisation_id']."' AND ud.user_type_id=3 AND YEAR(ud.created_at) = :yr3 AND MONTH(ud.created_at) = months_tbl.id) AS UNSIGNED) as dsupports
	        			FROM months_tbl
	        ",['yr1'=>$yr_only, 'yr2'=>$yr_only, 'yr3'=>$yr_only]);

                $dservices = $dsupports = $orders = array();

	    		foreach($querys as $query)
		    		{
		    			array_push($orders, $query->orders);
		    			array_push($dservices, $query->dservices);
		    			array_push($dsupports, $query->dsupports);
		    			
		    		}

    		return array('orders'=> $orders, 'dservices' => $dservices, 'dsupports' => $dsupports, 'dt'=> $yr_only);

		}


//////////////////////		Profile Update Get 		//////////////////////////////////////
public static function org_profile(Request $request)
	{
	try{

			$organisation = Organisation::admin_single_org_details($request->all());

			$request['category_type'] = 'Service';
			$services = Category::admin_org_cat_single_lang($request->all());

			$request['category_type'] = 'Support';
			$supports = Category::admin_org_cat_single_lang($request->all());

			return View::make('OrgPanel.Profile.updateOrg', compact('organisation', 'services', 'supports'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

////////////////////		Update UniqueNess Check 		///////////////////////////////////////////////////
public static function org_uunique_check(Request $request)
{
try{
		if(isset($request['phone_number']))
			{
				
				$ocheck = Organisation::where('organisation_id','!=', $request['organisation_id'])->where('phone_number', $request['phone_number'])->first();
				if($ocheck)
					return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this phone number is already taken' ],200);

			}

			return Response(['success' => 1, 'statuscode' => 200, 'msg' => 'Validated successfully' ],200);

	}
catch(\Exception $e)
	{
		return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
	}
}

//////////////////////		Profile Update Post 		//////////////////////////////////////
public static function org_pprofile(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'name' => 'required',
				'image' => 'sometimes|image',
				'phone_code' => 'required',
				'phone_number' => 'required|unique:organisations,phone_number,'.$request['organisation_id'].',organisation_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$organisation = Organisation::where('organisation_id', $request['organisation_id'])->first();

////////////////////////////		Images Upload 		////////////////////////////////
			if(!empty(Input::file('image')))
				{
				$request['image_name'] = CommonController::image_uploader(Input::file('image'));
					if(empty($request['image_name']))
						return Redirect::back()->withErrors('Sorry, image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['image_name'] = $organisation->image;
				}
////////////////////////////		Images Upload 		////////////////////////////////

			$admin = Admin::first();
			$request['buraq_percentage'] = $admin->buraq_percentage;

			Organisation::org_update_record($request->all());

			$request['categories'] = [];

			if(isset($request['servicesid']) && !empty($request['servicesid']) )
				$request['categories'] = $request['servicesid'];	//array_merge($a1,$a2);

			if(isset($request['supportsid']) && !empty($request['supportsid']) )
				$request['categories'] = array_merge($request['categories'], $request['supportsid']);

			OrganisationCategory::whereNotIn('category_id',$request['categories'])->where('organisation_id',$request['organisation_id'])->update([
				'deleted' => '1',
				'updated_at' => new \DateTime
			]);
			$con = [];

			foreach($request['categories'] as $category)
				{
	
					$check = OrganisationCategory::where('organisation_id', $request['organisation_id'])->where('category_id', $category)->first();
					if($check)
						{
							OrganisationCategory::where('organisation_category_id',$check->organisation_category_id)->update([
								'deleted' => '0',
								'updated_at' => new \DateTime
							]);
						}
					else
						{
							$con[] = [
								'organisation_id' => $request['organisation_id'],
								'category_id' => $category,
								'buraq_percentage' => $request['buraq_percentage'],
								'category_type' =>  $category < 6 ? 'Service' : 'Support',
								"deleted" => "0",
								"created_at" => new \DateTime,
								"updated_at" => new \DateTime
							];
						}

				}

			if(!empty($con))
				OrganisationCategory::insert($con);

//////////////////		Org Products with Charges Dynamic 		//////////////////////////////////
		if(isset($request['servicesid']) && !empty($request['servicesid']) )
			{

				$con = [];
				$products = CategoryBrandProduct::whereIn('category_id',$request['servicesid'])->whereRaw("( (SELECT COUNT(*) FROM organisation_category_brand_products as ocbp WHERE ocbp.organisation_id=".$request['organisation_id']." AND ocbp.category_brand_product_id=category_brand_products.category_brand_product_id) = 0 )")->get();

				foreach($products as $product)
					{
						$con[] = [
							'organisation_id' => $request['organisation_id'],
							'category_id' => $product->category_id,
							'category_brand_id' => $product->category_brand_id,
							'category_brand_product_id' => $product->category_brand_product_id,
							'alpha_price' => $product->alpha_price,
							'price_per_quantity' => $product->price_per_quantity,
							'price_per_distance' => $product->price_per_distance,
							'price_per_weight' => $product->price_per_weight,
							'price_per_hr' => $product->price_per_hr,
							'price_per_sq_mt' => $product->price_per_sq_mt,
							'created_at' => new \DateTime,
							'updated_at' => new \DateTime
						];
					}

				if(!empty(($con)))
					OrganisationCategoryBrandProduct::insert($con);

			}
//////////////////		Org Products with Charges Dynamic 		//////////////////////////////////

			return Redirect::back()->with('status','Profile updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}
////////////////////////		Password Update Get 	/////////////////////////////////////
public static function org_password()
	{
		return View::make('OrgPanel.Profile.updatePasswordOrg');
	}

////////////////////////		Password Update Post 	/////////////////////////////////////
public static function org_ppassword(Request $request)
	{
	try{
			$rules = array(
				'old_password'=>'required',
				'password'=>'required|confirmed|min:6'
			);
			$msg = array(
				'old_password.required' => 'Please provide your old password',
				'password.required' => 'Please provide new password',
				'password.min' => 'Your new password must be at least 6 characters long'
			);
			
			$validator = Validator::make($request->all(),$rules, $msg);
			if($validator->fails())
				return Redirect::back()->witherrors($validator->getMessageBag()->first());

			$org = \App('OrgDetails');

			if(!\Hash::Check($request['old_password'], $org->organisation['password']))
				return Redirect::back()->witherrors('Old Password is Incorrect');
			else if(\Hash::Check($request['password'], $org->organisation['password']))
				return Redirect::back()->witherrors('New password cannot be same as old password');

			Organisation::where('organisation_id', $org->organisation_id)->update([
					'password' => \Hash::make($request['password']),
					'updated_at' => new \DateTime
			]);

			return Redirect::back()->with('status','Password updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}
////////////////////////		Logout 		////////////////////////////////////////////////
public static function org_logout(Request $request)
	{
	try{

			\Cookie::queue('OrgAccessToken', 0, 0);
			\Cookie::queue('OrgLanguage', 0, 0);

			AdminUserDetailLogin::where('access_token',$request['access_token'])->update([
				"access_token" => "",
				"otp" => "",
				"updated_at" => new \DateTime
			]);

			return Redirect::route('org_panel_login1')->with('status', 'Logged Out Successfully');
		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

/////////////////////////		Delete Drivers Etc 		/////////////////////////////////////
public static function org_delete(Request $request)
	{
	try{

			$org = \App('OrgDetails');

			$rules = array(
				'driver_user_detail_id' => 'sometimes|exists:user_details,user_detail_id,user_type_id,2,organisation_id,'.$org->organisation_id,
				'driver_user_id' => 'sometimes|exists:users,user_id'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			if(isset($request['driver_user_detail_id']) && isset($request['driver_user_id']) )
				{

					DB::table('user_driver_detail_services')->where('user_detail_id',$request['driver_user_detail_id'])->delete();
					DB::table('user_driver_detail_products')->where('user_detail_id',$request['driver_user_detail_id'])->delete();

					DB::table('user_detail_notifications')->where('user_detail_id',$request['driver_user_detail_id'])->orwhere('sender_user_detail_id',$request['driver_user_detail_id'])->delete();

					DB::table('user_bank_details')->where('user_id',$request['driver_user_id'])->delete();

					DB::table('payment_ledgers')->where('user_detail_id', $request['driver_user_detail_id'])->delete();


					DB::table('organisation_coupon_users')->where('seller_user_detail_id',$request['driver_user_detail_id'])->orwhere('customer_user_detail_id',$request['driver_user_detail_id'])->delete();

					DB::table('payments')->where('customer_user_detail_id', $request['customer_user_detail_id'])->orWhere('seller_user_detail_id', $request['driver_user_detail_id'])->delete();

					DB::table('organisation_coupons')->where('user_detail_id', $request['driver_user_detail_id'])->delete();

					$order_ids = DB::table('orders')->where('driver_user_detail_id', $request['driver_user_detail_id'])->orwhere('customer_user_detail_id', $request['driver_user_detail_id'])->pluck('order_id');

					DB::table('order_requests')->whereIn('order_id', $order_ids)->delete();
					DB::table('order_ratings')->whereIn('order_id', $order_ids)->delete();
					DB::table('order_images')->whereIn('order_id', $order_ids)->delete();

					DB::table('coupon_users')->where('user_detail_id', $request['driver_user_detail_id'])->delete();

					DB::Table('admin_user_detail_logins')->where('user_detail_id',$request['driver_user_detail_id'])->delete();

					DB::table('orders')->whereIn('order_id',$order_ids)->delete();
					DB::table('user_details')->where('user_detail_id',$request['driver_user_detail_id'])->delete();

					$msg = 'Service driver deleted successfully';
				}
			else
				{

					$msg = 'Deleted successfully';
				}

			return Redirect::Back()->with('status',$msg);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}


/////////////////////////

}
