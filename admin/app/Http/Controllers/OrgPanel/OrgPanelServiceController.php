<?php

namespace App\Http\Controllers\OrgPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;

use App\Http\Controllers\CommonController;

use App\Models\Category;
use App\Models\Organisation;
use App\Models\OrganisationArea;
use App\Models\OrganisationCategory;
use App\Models\OrganisationCategoryBrandProduct;
use App\Models\CategoryBrand;
use App\Models\CategoryBrandProduct;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\UserDriverDetailProduct;
use App\Models\UserDriverDetailService;

class OrgPanelServiceController extends Controller
{

///////////////////////			Org Services    /////////////////////////////////
public static function org_services_all(Request $request)
	{
	try{

			$request['category_type'] = 'Service';
			$request['user_type_id'] = 2;

			$services = OrganisationCategory::org_all_categories($request->all());

			return View::make('OrgPanel.Services.orgServices', compact('services'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////		Service Drivers 		/////////////////////////////
public static function org_ser_drivers(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$data = CommonController::set_starting_ending($request->all());

			$data['category_type'] = 'Service';
			$data['user_type_ids'] = [2];

			$category = Category::admin_single_cat_details($request->all());
			$drivers = UserDetail::admin_get_cat_drivers($data);

			return View::make('OrgPanel.Services.Drivers.orgSerDrivers', compact('drivers', 'category'))->with('category_id', $request['category_id'])->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////			Service Add Drivers 		//////////////////////////
public static function org_ser_adriver(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$category = Category::admin_single_cat_details($request->all());
			$brands = CategoryBrand::admin_brands_list_add_drivers($request->all());

			return View::make('OrgPanel.Services.Drivers.orgSerDriverAdd', compact('category', 'brands'))->with('category_id', $request['category_id']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////			Driver Block Unblock 		//////////////////////////
public static function org_ser_bdriver(Request $request)
	{
	try{

			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id,organisation_id,'.$request['organisation_id'],
				'block' => 'required|in:0,1'
			);
			$msg = array('user_detail_id.exists' => 'Sorry, this driver is currently not available');

			$validator = Validator::make($request->all(),$rules,$msg);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$driver = UserDetail::where('user_detail_id',$request['user_detail_id'])->first();

			if($request['block'] == 1)
				{

					if($driver->blocked == '1')
						return Redirect::back()->withErrors('This driver is already blocked')->withInput();

					UserDetail::where('user_detail_id',$driver->user_detail_id)->update(array(
						'blocked' => '1',
						'updated_at' =>new \DateTime
					));

					return Redirect::back()->with('status','Driver blocked successfully');

				}
			else
				{

					if($driver->blocked == '0')
						return Redirect::back()->withErrors('This driver is already unblocked')->withInput();

					UserDetail::where('user_detail_id',$driver->user_detail_id)->update(array(
						'blocked' => '0',
						'updated_at' => new \DateTime
					));

					return Redirect::back()->with('status','Driver unblocked successfully');

				}

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

///////////////////////			Driver Add Post 		//////////////////////////////
public static function org_ser_padriver(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
				'name' => 'required',
				//'profile_pic' => 'required|image',
				'phone_number' => 'required|unique:users,phone_number',
				'mulkiya_number' => 'required|unique:user_details,mulkiya_number',
				'mulkiya_validity' => 'required',
				//'mulkiya_front' => 'required|image',
				//'mulkiya_back' => 'required|image',
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
				'productids' => 'required|array'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			//$org = \App('OrgDetails');

////////////////////////////		Images Upload 		////////////////////////////////
			// $request['profile_pic_name'] = CommonController::image_uploader(Input::file('profile_pic'));
			// 	if(empty($request['profile_pic_name']))
			// 		return Redirect::back()->withErrors('Sorry, profile pic not uploaded. Please try again')->withInput($request->all());
////////////////////////////		Images Upload 		////////////////////////////////
 $request['profile_pic_name'] = "";
			if(!empty(Input::file('mulkiya_front')))
				{	
					$request['mulkiya_front_name'] = CommonController::image_uploader(Input::file('mulkiya_front'));
						if(empty($request['mulkiya_front_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_front_name'] = '';
				}

			if(!empty(Input::file('mulkiya_back')))
				{	
					$request['mulkiya_back_name'] = CommonController::image_uploader(Input::file('mulkiya_back'));
						if(empty($request['mulkiya_back_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_back_name'] = '';
				}

			$category = Category::where('category_id',$request['category_id'])->first();

			$request['created_by'] = 'Org';
			$request['category_type'] = 'Service';
			$request['email'] = CommonController::generate_email();
			$request['maximum_rides'] = $category->maximum_rides;

			$user = User::insert_new_record($request->all());

			$request['user_id'] = $user->user_id;
			$request['user_type_id'] = 2;//Service

			$request['mulkiya_validity'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['mulkiya_validity'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');


			$userDetail = UserDetail::insert_new_record($request->all());
			$request['user_detail_id'] = $userDetail->user_detail_id;

			$request['buraq_percentage'] = 10;

			UserDriverDetailService::insert_new_record($request->all());

			$con = [];
			foreach($request['productids'] as $productid)
				{

					$product = CategoryBrandProduct::where('category_brand_product_id', $productid)->first();

					$con[] = [
						'user_id' => $request['user_id'],
						'user_detail_id' => $request['user_detail_id'],
						'category_brand_product_id' => $productid,
						'alpha_price' => $product->alpha_price,
						'price_per_quantity' => $product->price_per_quantity,
						'price_per_distance' => $product->price_per_distance,
						'price_per_weight' => $product->price_per_weight,
						'price_per_hr' => $product->price_per_hr,
						'price_per_sq_mt' => $product->price_per_sq_mt,
						'deleted' => '0',
						'created_at' => new \DateTime,
						'updated_at' => new \DateTime
					];
				}

			if(!empty($con))
				UserDriverDetailProduct::insert($con);

			$request['sms'] = CommonController::send_sms([ 'phone_number' => $request['phone_number'], 'message' => env('APP_NAME').' - '.trans('messages.Congrats you are added as driver')  ]);

			return Redirect::back()->with('status','Yesser Man added successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

////////////////////		Driver Udpate Get 		///////////////////////////////////////////
public static function org_ser_udriver(Request $request)
	{
	try{

			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id,organisation_id,'.$request['organisation_id'],
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$driver = UserDetail::admin_driver_detail_update_get($request->all());

			$request['category_id'] = $driver->category_id;

			$category = Category::admin_single_cat_details($request->all());
			$brands = CategoryBrand::admin_brands_list_add_drivers($request->all());

			return View::make('OrgPanel.Services.Drivers.orgSerDriverUpdate', compact('driver', 'category', 'brands'))->with('category_id', $request['category_id']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

////////////////////		Driver Udpate Post 		/////////////////////////////////////////
public static function org_ser_pudriver(Request $request)
	{
	try{

			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id',
				'name' => 'required',
				//'profile_pic' => 'sometimes|image',
				'phone_number' => 'required|unique:users,phone_number,'.$request['user_id'].',user_id',
				'mulkiya_number' => 'required|unique:user_details,mulkiya_number,'.$request['user_detail_id'].',user_detail_id',
				'mulkiya_validity' => 'required',
				//'mulkiya_front' => 'sometimes|image',
				//'mulkiya_back' => 'sometimes|image',
				'category_brand_id' => 'required|exists:category_brands,category_brand_id',
				'productids' => 'required|array',

			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$driver = UserDetail::admin_driver_detail_update_get($request->all());

			$request['user_id'] = $driver->user_id;

////////////////////////////		Images Upload 		////////////////////////////////
			if(!empty(Input::file('profile_pic')))
				{
				$request['profile_pic_name'] = CommonController::image_uploader(Input::file('profile_pic'));
					if(empty($request['profile_pic_name']))
						return Redirect::back()->withErrors('Sorry, profile pic not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['profile_pic_name'] = $driver->profile_pic;
				}
////////////////////////////		Images Upload 		////////////////////////////////

			if(!empty(Input::file('mulkiya_front')))
				{	
					$request['mulkiya_front_name'] = CommonController::image_uploader(Input::file('mulkiya_front'));
						if(empty($request['mulkiya_front_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_front_name'] = $driver->mulkiya_front;
				}

			if(!empty(Input::file('mulkiya_back')))
				{	
					$request['mulkiya_back_name'] = CommonController::image_uploader(Input::file('mulkiya_back'));
						if(empty($request['mulkiya_back_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_back_name'] = $driver->mulkiya_back;
				}

			$request['user_type_id'] = 2;//Support
			$request['mulkiya_validity'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['mulkiya_validity'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');


			User::where('user_id',$request['user_id'])->limit(1)->update([
					'name' => $request['name'],
					'phone_number' => $request['phone_number'],
					'address' => isset($request['address']) ? $request['address'] : "",
					'address_latitude' => isset($request['address_latitude']) ? $request['address_latitude'] : 21.47,
					'address_longitude' => isset($request['address_longitude']) ? $request['address_longitude'] : 55.97,
					'updated_at' => new \DateTime
			]);

			UserDetail::where('user_detail_id', $request['user_detail_id'])->limit(1)->update([
				'category_brand_id' => $request['category_brand_id'],
				'profile_pic' => $request['profile_pic_name'],
				'mulkiya_number' => $request['mulkiya_number'],
				'mulkiya_front' => $request['mulkiya_front_name'],
				'mulkiya_back' => $request['mulkiya_back_name'],
				'mulkiya_validity' => $request['mulkiya_validity'],
				'updated_at' => new \DateTime
			]);

			UserDriverDetailProduct::where('user_detail_id',$request['user_detail_id'])->whereNotIn('category_brand_product_id', $request['productids'] )->update([
					'deleted' => '1',
					'updated_at' => new \DateTime
			]);

			$con = [];
			$undel = [];

			foreach($request['productids'] as $productid)
				{

				$check = UserDriverDetailProduct::where('user_detail_id', $request['user_detail_id'])->where('category_brand_product_id', $productid)->first();
				if(!$check)
					{

						$product = CategoryBrandProduct::where('category_brand_product_id',$productid)->first();

						$con[] = [
									'user_id' => $request['user_id'],
									'user_detail_id' => $request['user_detail_id'],
									'category_brand_product_id' => $productid,
									'deleted' => '0',
									'alpha_price' => $product['alpha_price'],
									'price_per_quantity' => $product['price_per_quantity'],
									'price_per_distance' => $product['price_per_distance'],
									'price_per_weight' => $product['price_per_weight'],
									'price_per_hr' => $product['price_per_hr'],
									'price_per_sq_mt' => $product['price_per_sq_mt'],
									'created_at' => new \DateTime,
									'updated_at' => new \DateTime
						];

					}
				else
					{
						$undel[] = $check->user_driver_detail_product_id;
					}

				}

			if(!empty($con))
				UserDriverDetailProduct::insert($con);

			if(!empty(($undel)))
				UserDriverDetailProduct::whereIn('user_driver_detail_product_id', $undel)->update([
					'deleted' => '0',
					'updated_at' => new \DateTime
				]);



			return Redirect::back()->with('status','Yesser Man updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////		Org Service Areas 		/////////////////////////////////////
public static function org_ser_areas(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			if($request['category_id'] != 2)
				return Redirect::back()->withErrors("Sorry, area selection is currently only available to drinking water service");

			$data = CommonController::set_starting_ending($request->all());

			$areas = OrganisationArea::category_wise_org_areas($data);

			return View::make('OrgPanel.Services.Areas.orgSerAreas', compact('areas'))->with('category_id', $request['category_id'])->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////		Area Add Get 		/////////////////////////////////////////////////
public static function org_area_add(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			if($request['category_id'] != 2)
				return Redirect::back()->withErrors("Sorry, area selection is currently only available to drinking water service");

			$drivers = UserDetail::org_service_drivers($request['organisation_id'], $request['category_id'], 2);

			return View::make('OrgPanel.Services.Areas.orgSerAreaAdd', compact('drivers'))->with('category_id', $request['category_id']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////		Area Add Post 		/////////////////////////////////////////////////
public static function org_area_padd(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
				'label' => 'required',
				'address' => 'required',
				'address_latitude' => 'required',
				'address_longitude' => 'required',
				'distance' => 'required',
				'drivers' => 'required'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$request['created_by'] = "Org";

			if($request['category_id'] != 2)
				return Redirect::back()->withErrors("Sorry, area selection is currently only available to drinking water service");

			if(!isset($request['days']))
				$request['days'] = [];

			$organisation_area = OrganisationArea::insert_new_record($request->all());

///////////////////		Area Drivers 	////////////////////////////
			$con = [];
			foreach ($request['drivers'] as $key => $driver) {
				
				$con[] = [
					'organisation_area_id' => $organisation_area->organisation_area_id,
					'user_detail_id' => $driver,
					'organisation_id' => $request['organisation_id'],
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				];

			}
			if(!empty($con))
				DB::table('organisation_area_drivers')->insert($con);
///////////////////		Area Drivers 	////////////////////////////

			return Redirect::back()->with('status','Area added successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

////////////////////////		Area Update 		/////////////////////////////////////////
public static function org_area_update(Request $request)
	{
	try{

			$rules = array(
				'organisation_area_id' => 'required|exists:organisation_areas,organisation_area_id,organisation_id,'.$request['organisation_id']
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$area = OrganisationArea::where('organisation_area_id',$request['organisation_area_id'])->first();

			$drivers = UserDetail::org_service_area_drivers($request['organisation_id'], 2, 2, $request['organisation_area_id']);

			return View::make('OrgPanel.Services.Areas.orgSerAreaUpdate', compact('area', 'drivers'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

////////////////////////		Area Update 		/////////////////////////////////////////
public static function org_area_pupdate(Request $request)
	{
	try{

			$rules = array(
				'organisation_area_id' => 'required|exists:organisation_areas,organisation_area_id,organisation_id,'.$request['organisation_id'],
				'label' => 'required',
				'address' => 'required',
				'address_latitude' => 'required',
				'address_longitude' => 'required',
				'distance' => 'required',
				'drivers' => 'required'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			if(!isset($request['days']))
				$request['days'] = [];

			OrganisationArea::update_record($request->all());

///////////////////		Area Drivers 	////////////////////////////
			DB::table('organisation_area_drivers')->where('organisation_area_id',$request['organisation_area_id'])->delete();
			$con = [];
			foreach ($request['drivers'] as $key => $driver) {
				
				$con[] = [
					'organisation_area_id' => $request['organisation_area_id'],
					'user_detail_id' => $driver,
					'organisation_id' => $request['organisation_id'],
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime
				];

			}
			if(!empty($con))
				DB::table('organisation_area_drivers')->insert($con);
///////////////////		Area Drivers 	////////////////////////////

			return Redirect::back()->with('status','Area updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

///////////////////////		Org Area Block Unblock 		////////////////////////////////////////
public static function org_ser_area_block(Request $request)
	{
	try{

			$rules = array(
				'organisation_area_id' => 'required|exists:organisation_areas,organisation_area_id,organisation_id,'.$request['organisation_id'],
			);

			$validator = Validator::make($request->all(),$rules);
				if($validator->fails())
					return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

			$area = OrganisationArea::where('organisation_area_id',$request['organisation_area_id'])->first();

			if($request['block'] == 1)
				{

					if($area->blocked == '1')
						return Redirect::back()->withErrors('This area is already blocked')->withInput();

					OrganisationArea::where('organisation_area_id',$area->organisation_area_id)->update(array(
						'blocked' => '1',
						'updated_at' =>new \DateTime
					));

					return Redirect::back()->with('status','Area blocked successfully');

				}
			else
				{

					if($area->blocked == '0')
						return Redirect::back()->withErrors('This area is already unblocked')->withInput();

					OrganisationArea::where('organisation_area_id',$area->organisation_area_id)->update(array(
						'blocked' => '0',
						'updated_at' => new \DateTime
					));

					return Redirect::back()->with('status','Area unblocked successfully');

				}

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

//////////////////		Organisation Service Products 		///////////////////////////////////
public static function org_service_products(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id,category_type,Service',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$category = Category::admin_single_cat_details($request->all());
			//$org_products = OrganisationCategoryBrandProduct::org_services_products_listings($request->all());
			$org_products = CategoryBrandProduct::single_service_all_products($request->all());

			//dd($org_products[0]);

			return View::make('OrgPanel.Services.Products.orgSerProducts', compact('category', 'org_products'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////		Product Price Update 		/////////////////////////////////
public static function org_ser_product_update(Request $request)
	{
	try{

			$rules = array(
				'organisation_category_brand_product_id' => 'required|exists:organisation_category_brand_products',
				'alpha_price' => 'required|min:0',
				'price_per_quantity' => 'required|min:0',
				'price_per_distance' => 'required|min:0',
				'price_per_weight' => 'required|min:0',
				'price_per_hr' => 'required|min:0',
				'price_per_sq_mt' => 'required|min:0'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			OrganisationCategoryBrandProduct::where('organisation_category_brand_product_id',$request['organisation_category_brand_product_id'])->limit(1)->update([
					'alpha_price' => $request['alpha_price'],
					'price_per_quantity' => $request['price_per_quantity'],
					'price_per_distance' => $request['price_per_distance'],
					'price_per_weight' => $request['price_per_weight'],
					'price_per_hr' => $request['price_per_hr'],
					'price_per_sq_mt' => $request['price_per_sq_mt'],
					'updated_at' => new \DateTime
			]);

			return Redirect::back()->with('status','Product price updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

///////////////////


}
