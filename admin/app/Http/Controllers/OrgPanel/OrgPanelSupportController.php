<?php

namespace App\Http\Controllers\OrgPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;

use App\Http\Controllers\CommonController;

use App\Models\Category;
use App\Models\Organisation;
use App\Models\OrganisationArea;
use App\Models\OrganisationCategory;
use App\Models\CategoryBrand;
use App\Models\CategoryBrandProduct;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\UserDriverDetailProduct;

class OrgPanelSupportController extends Controller
{
///////////////		Org Support All    /////////////////////
public static function org_supports_all(Request $request)
	{
	try{

			$request['category_type'] = 'Support';
			$request['user_type_id'] = 3;

			$supports = OrganisationCategory::org_all_categories($request->all());

			return View::make('OrgPanel.Supports.orgSupports', compact('supports'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////		Support Drivers 		/////////////////////////////
public static function org_sup_drivers(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id,category_type,Support',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$data = CommonController::set_starting_ending($request->all());

			$data['category_type'] = 'Support';
			$data['user_type_ids'] = [3];

			$category = Category::admin_single_cat_details($request->all());
			$drivers = UserDetail::admin_get_cat_drivers($data);

			return View::make('OrgPanel.Supports.Drivers.orgSupDrivers', compact('drivers', 'category'))->with('category_id', $request['category_id'])->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////			Service Add Drivers 		//////////////////////////
public static function org_sup_adriver(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id,category_type,Support',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$category = Category::admin_single_cat_details($request->all());

			return View::make('OrgPanel.Supports.Drivers.orgSupDriverAdd', compact('category'))->with('category_id', $request['category_id']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

///////////////////////			Driver Add Post 		//////////////////////////////
public static function org_sup_padriver(Request $request)
	{
	try{

			$rules = array(
				'category_id' => 'required|exists:categories,category_id,category_type,Support',
				'name' => 'required',
				'profile_pic' => 'required|image',
				'phone_number' => 'required|unique:users,phone_number',
				'mulkiya_number' => 'required|unique:user_details,mulkiya_number',
				'mulkiya_validity' => 'required',
				'mulkiya_front' => 'required|image',
				'mulkiya_back' => 'required|image',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			//$org = \App('OrgDetails');

////////////////////////////		Images Upload 		////////////////////////////////
			$request['profile_pic_name'] = CommonController::image_uploader(Input::file('profile_pic'));
				if(empty($request['profile_pic_name']))
					return Redirect::back()->withErrors('Sorry, profile pic not uploaded. Please try again')->withInput($request->all());
////////////////////////////		Images Upload 		////////////////////////////////

			if(!empty(Input::file('mulkiya_front')))
				{	
					$request['mulkiya_front_name'] = CommonController::image_uploader(Input::file('mulkiya_front'));
						if(empty($request['mulkiya_front_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_front_name'] = '';
				}

			if(!empty(Input::file('mulkiya_back')))
				{	
					$request['mulkiya_back_name'] = CommonController::image_uploader(Input::file('mulkiya_back'));
						if(empty($request['mulkiya_back_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_back_name'] = '';
				}

			$category = Category::where('category_id',$request['category_id'])->first();

			$request['created_by'] = 'Org';
			$request['category_type'] = 'Support';
			$request['email'] = CommonController::generate_email();
			$request['maximum_rides'] = $category->maximum_rides;
			$request['category_brand_id'] = 0;

			$user = User::insert_new_record($request->all());

			$request['user_id'] = $user->user_id;
			$request['user_type_id'] = 3;//Support

			$request['mulkiya_validity'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['mulkiya_validity'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');

			$userDetail = UserDetail::insert_new_record($request->all());
			$request['user_detail_id'] = $userDetail->user_detail_id;

			$request['sms'] = CommonController::send_sms([ 'phone_number' => $request['phone_number'], 'message' => env('APP_NAME').' - '.trans('messages.Congrats you are added as driver')  ]);

			return Redirect::back()->with('status','Driver added successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

////////////////////		Driver Udpate Get 		///////////////////////////////////////////
public static function org_sup_udriver(Request $request)
	{
	try{

			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id,organisation_id,'.$request['organisation_id'],
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$driver = UserDetail::admin_driver_detail_update_get($request->all());

			$request['category_id'] = $driver->category_id;

			$category = Category::admin_single_cat_details($request->all());

			return View::make('OrgPanel.Supports.Drivers.orgSupDriverUpdate', compact('driver', 'category'))->with('category_id', $request['category_id']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

////////////////////		Driver Udpate Post 		/////////////////////////////////////////
public static function org_sup_pudriver(Request $request)
	{
	try{

			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id',
				'name' => 'required',
				'profile_pic' => 'sometimes|image',
				'phone_number' => 'required|unique:users,phone_number,'.$request['user_id'].',user_id',
				'mulkiya_number' => 'required|unique:user_details,mulkiya_number,'.$request['user_detail_id'].',user_detail_id',
				'mulkiya_validity' => 'required',
				'mulkiya_front' => 'sometimes|image',
				'mulkiya_back' => 'sometimes|image',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$driver = UserDetail::admin_driver_detail_update_get($request->all());

			$request['user_id'] = $driver->user_id;

////////////////////////////		Images Upload 		////////////////////////////////
			if(!empty(Input::file('profile_pic')))
				{
				$request['profile_pic_name'] = CommonController::image_uploader(Input::file('profile_pic'));
					if(empty($request['profile_pic_name']))
						return Redirect::back()->withErrors('Sorry, profile pic not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['profile_pic_name'] = $driver->profile_pic;
				}
////////////////////////////		Images Upload 		////////////////////////////////

			if(!empty(Input::file('mulkiya_front')))
				{	
					$request['mulkiya_front_name'] = CommonController::image_uploader(Input::file('mulkiya_front'));
						if(empty($request['mulkiya_front_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_front_name'] = $driver->mulkiya_front;
				}

			if(!empty(Input::file('mulkiya_back')))
				{	
					$request['mulkiya_back_name'] = CommonController::image_uploader(Input::file('mulkiya_back'));
						if(empty($request['mulkiya_back_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_back_name'] = $driver->mulkiya_back;
				}

			$request['user_type_id'] = 3;//Support
			$request['mulkiya_validity'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['mulkiya_validity'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');


			User::where('user_id',$request['user_id'])->limit(1)->update([
					'name' => $request['name'],
					'phone_number' => $request['phone_number'],
					'updated_at' => new \DateTime
			]);

			UserDetail::where('user_detail_id', $request['user_detail_id'])->limit(1)->update([
				'profile_pic' => $request['profile_pic_name'],
				'mulkiya_number' => $request['mulkiya_number'],
				'mulkiya_front' => $request['mulkiya_front_name'],
				'mulkiya_back' => $request['mulkiya_back_name'],
				'mulkiya_validity' => $request['mulkiya_validity'],
				'updated_at' => new \DateTime
			]);

			return Redirect::back()->with('status','Driver updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////


}