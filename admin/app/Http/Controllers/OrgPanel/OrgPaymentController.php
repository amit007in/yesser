<?php

namespace App\Http\Controllers\OrgPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;

use App\Http\Controllers\CommonController;

use App\Models\Payment;
use App\Models\Order;
use App\Models\OrganisationCouponUser;
use App\Models\CouponUser;


class OrgPaymentController extends Controller
{
////////////////////////	Service All Payments    ////////////////////////////////////
public static function org_ser_pay_all(Request $request)
	{
	try{

			$data = CommonController::set_starting_ending($request->all());

			$data['seller_user_type_id'] = 2;

			$payments = Payment::org_orders_payment($data);
			$psums = Payment::org_ser_pay_sums($data);

			return View::make("OrgPanel.Payments.orgSerPayments", compact('payments', 'psums'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

////////////////////////	Support All Payments    ////////////////////////////////////
public static function org_sup_pay_all(Request $request)
	{
	try{

			$data = CommonController::set_starting_ending($request->all());

			$data['seller_user_type_id'] = 3;

			$payments = Payment::org_orders_payment($data);
			$psums = Payment::org_ser_pay_sums($data);

			return View::make("OrgPanel.Payments.orgSupPayments", compact('payments', 'psums'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

////////////////////////		Etokens Payments All 	////////////////////////////////
public static function org_etoken_pay_all(Request $request)
	{
	try{

			$data = CommonController::set_starting_ending($request->all());

			$data['seller_user_type_id'] = 4;

			$data['seller_organisation_id'] = $request['organisation_id'];

			$payments = Payment::admin_etoken_payments($data);
			$psums = Payment::admin_etoken_pay_sums($data);

			return View::make("OrgPanel.Payments.orgETokenPayments", compact('payments', 'psums'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

///////////////////////
}
