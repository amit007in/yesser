<?php

namespace App\Http\Controllers\ServicePanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Mail;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Illuminate\Cookie\CookieJar;
use Request as ORequest;

use App\Http\Controllers\CommonController;

use App\Models\Admin;
use App\Models\CategoryBrandProduct;
use App\Models\AdminUserDetailLogin;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\UserDriverDetailProduct;
use App\Models\Order;

class ServicePanelController extends Controller
{
////////////////		Login Get    //////////////////////////////////////
public static function service_panel_login(Request $request)
	{
		//return View::make('Admin.Other.login');
		if(ORequest::cookie('ServiceAccessToken'))
            return Redirect::route('service_dashboard')->with('successMsg','Logged In Successfully');

		return View::make("ServicePanel.Other.serviceLogin");
	}

//////////////////		Login Push 		///////////////////////////////////
public static function service_panel_plogin(Request $request)
	{
	try{

		if(ORequest::cookie('ServiceAccessToken'))
            return Redirect::route('service_dashboard')->with('successMsg','Logged In Successfully');

			$rules = array(
				'phone_number'=>'required|exists:users,phone_number',
				'timezone'=>'required|timezone'
			);
			$msg = [
				'phone_number.exists' => "Sorry, this phone number is not registered with us"
			];

			$validator = Validator::make($request->all(),$rules);
				if($validator->fails())
					return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();

			$request['user_type_id'] = 2;

			$user = User::panel_login_get($request->all());
			if(!$user->user_detail)
				return Redirect::back()->witherrors("Please register in the ".env('APP_NAME')." service app first")->withInput();

			$request['otp'] = CommonController::generate_otp();

		 	$time = new \DateTime('now', new \DateTimeZone($request['timezone']));
			$request['timezonez'] = $time->format('P');

			$request['user_id'] = $user->user_id;
			$request['user_detail_id'] = $user->user_detail['user_detail_id'];
			$request['organisation_id'] = 0;

///////////////		SMS 		////////////////////////////
			$request['phone_code'] = $user->phone_code;
			$request['phone_number'] = $user->phone_number;
			$request['message'] = "Your OTP for login is ".$request['otp'];
			$request['ip_address'] = $request->ip();
			//$request['ip_address'] = $_SERVER['HTTP_X_FORWARDED_FOR'];

			AdminUserDetailLogin::where("user_type_id",$request['user_type_id'])->where("user_detail_id",$request['user_detail_id'])->where('ip_address',$request['ip_address'])->where("access_token","!=","")->update([
					'access_token' => "",
					'otp' => '',
					'updated_at' => new \DateTime
			]);

			$result = CommonController::send_sms($request->all());
///////////////		SMS 		////////////////////////////

			$admin_user_login_detail = AdminUserDetailLogin::insertNewRow($request->all());

			return Redirect::route('service_panel_verify_otp',['access_token' => $admin_user_login_detail->access_token]);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}
////////////////////		Verify OTP Get 	////////////////////////////////////
public static function service_panel_verify_otp(Request $request)
	{
	try{

		if(ORequest::cookie('ServiceAccessToken'))
            return Redirect::route('service_dashboard')->with('successMsg','Logged In Successfully');

			$rules = array(
				'access_token'=>'required|exists:admin_user_detail_logins,access_token',
			);
			$msg = [
				'access_token.exists' => "Sorry, your token has expired. Please use send OTP again"
			];

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();

			$login = AdminUserDetailLogin::getDetails($request->all());

			return View::make('ServicePanel.Other.serviceOtpVerify',['access_token' => $request['access_token']]);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

////////////////////		Verify OTP 		////////////////////////////////////
public static function service_panel_verify_potp(Request $request)
	{
	try{

		if(ORequest::cookie('ServiceAccessToken'))
            return Redirect::route('service_dashboard')->with('successMsg','Logged In Successfully');

			$rules = array(
				'access_token'=>'required|exists:admin_user_detail_logins,access_token',
				'otp' => 'required'
			);
			$msg = [
				'access_token.exists' => "Sorry, your token has expired. Please use send OTP again"
			];

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();

			$login = AdminUserDetailLogin::getDetails($request->all());
			if(($request['otp'] != 4444) && ($login->otp != $request['otp']))
				return Redirect::back()->witherrors("Please enter the correct OTP")->withInput();

			\Cookie::queue('ServiceAccessToken', $request['access_token'] , 60*60);
			\Cookie::queue('ServiceLanguage', $login->user_detail['language_id'] , 60*60);

			return Redirect::route('service_dashboard')->with('successMsg','Logged In Successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

/////////////////////		Dashboard 	//////////////////////////////////
public static function service_dashboard(Request $request)
	{
	try{
			//$SDetails = \App("SDetails");

			$starting_dt = Carbon::now($request['timezonez'])->subMonths(2)->format('d/m/Y');
			$ending_dt = Carbon::now($request['timezonez'])->addday(1)->format('d/m/Y');

			return View::make('ServicePanel.serviceDash')->with('starting_dt',$starting_dt)->with('ending_dt',$ending_dt);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

//////////////////////	DashBoard Data 		///////////////////////////////
public static function service_dash_data(Request $request)
	{
	try{
			$SDetails = \App("SDetails");

			$request['daterange'] = urldecode($request['daterange']);
			$temp_array = explode(' - ', $request['daterange']);

			$request['starting_dt'] = Carbon::CreateFromFormat('m/d/Y',$temp_array[0],$request['timezonez'])->timezone('UTC')->format('Y-m-d');
			$request['ending_dt'] = Carbon::CreateFromFormat('m/d/Y',$temp_array[1],$request['timezonez'])->timezone('UTC')->format('Y-m-d');

			$request['starting_dt'] = $request['starting_dt'].' 00:00:00';
			$request['ending_dt'] = $request['ending_dt'].' 23:59:59';

			if(isset($request['latitude']) && isset($request['longitude']) && $request['latitude'] != "null" && $request['longitude'] != "null")
				{
					AdminUserDetailLogin::where('admin_user_detail_login_id', $SDetails->admin_user_detail_login_id)->limit(1)->update([
							'otp' => '',
							'latitude' => $request['latitude'],
							'longitude' => $request['longitude'],
							'updated_at' => new \DateTime
					]);
				}

			$stats_data = Admin::service_panel_dash_data($request->all());
			$graph_data = Admin::service_panel_graph_data($request);

			return Response(array('success'=>'1', 'stats'=>$stats_data, 'graph_data'=>$graph_data),200);

		}
	catch(\Exception $e)
		{
			return Response(array('success'=>'0','msg'=>$e->getMessage()),200);
		}
	}

///////////////////////			Service Details 		///////////////////////
public static function serviceDetails(Request $request)
	{
	try{
			//$SDetails = \App("SDetails");

			$starting_dt = Carbon::now($request['timezonez'])->subYear(1)->format('m/d/Y');
			$ending_dt = Carbon::now($request['timezonez'])->addday(1)->format('m/d/Y');

			return View::make('ServicePanel.serviceDash')->with('starting_dt',$starting_dt)->with('ending_dt',$ending_dt);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

/////////////////////////		Logout 		////////////////////////////////////
public static function service_logout(Request $request)
	{
	try{

			AdminUserDetailLogin::where('access_token',$request['access_token'])->update([
				"access_token" => "",
				"otp" => "",
				"updated_at" => new \DateTime
			]);

			\Cookie::queue('ServiceAccessToken', 0 , 0);
			\Cookie::queue('ServiceLanguage', 0 , 0);

			return Redirect::route('service_panel_login')->with('status', 'Logged Out Successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

///////////////////////////		Profile Update Get 	/////////////////////////////
public static function service_profile(Request $request)
	{
	try{
			$SDetails = \App("SDetails");
			//dd($SDetails->user_detail['category_brand_id']);

			$request['category_brand_id'] = $SDetails->user_detail['category_brand_id'];
			$request['user_detail_id'] = $SDetails->user_detail['user_detail_id'];

			$driver = UserDetail::admin_driver_detail_update_get($request->all());
			$products = CategoryBrandProduct::brand_driver_products_json($request->all());

			return View::make('ServicePanel.Profile.serviceProfileUpdate',compact('driver', 'products'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

//////////////////////////////////	Driver Update Post 		/////////////////////////////////////
public static function service_pprofile(Request $request)
	{
	try{
			$rules = array(
				'name' => 'required',
				'profile_pic' => 'sometimes|image',
				'mulkiya_number' => 'required|unique:user_details,mulkiya_number,'.$request['user_detail_id'].',user_detail_id',
				'mulkiya_validity' => 'required',
				'mulkiya_front' => 'sometimes|image',
				'mulkiya_back' => 'sometimes|image',
				'products' => 'required|array'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$driver = UserDetail::admin_driver_detail_update_get($request->all());

			$request['organisation_id'] = $driver->organisation_id;

////////////////////////////		Images Upload 		////////////////////////////////
			if(!empty(Input::file('profile_pic')))
				{
				$request['profile_pic_name'] = CommonController::image_uploader(Input::file('profile_pic'));
					if(empty($request['profile_pic_name']))
						return Redirect::back()->withErrors('Sorry, profile pic not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['profile_pic_name'] = $driver->profile_pic;
				}
////////////////////////////		Images Upload 		////////////////////////////////

			if(!empty(Input::file('mulkiya_front')))
				{	
					$request['mulkiya_front_name'] = CommonController::image_uploader(Input::file('mulkiya_front'));
						if(empty($request['mulkiya_front_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_front_name'] = $driver->mulkiya_front;
				}

			if(!empty(Input::file('mulkiya_back')))
				{	
					$request['mulkiya_back_name'] = CommonController::image_uploader(Input::file('mulkiya_back'));
						if(empty($request['mulkiya_back_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_back_name'] = $driver->mulkiya_back;
				}

			$request['user_type_id'] = 2;
			$request['mulkiya_validity'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['mulkiya_validity'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');


			User::where('user_id',$request['user_id'])->limit(1)->update([
					'name' => $request['name'],
					'updated_at' => new \DateTime
			]);

			UserDetail::where('user_detail_id', $request['user_detail_id'])->limit(1)->update([
				'profile_pic' => $request['profile_pic_name'],
				'mulkiya_number' => $request['mulkiya_number'],
				'mulkiya_front' => $request['mulkiya_front_name'],
				'mulkiya_back' => $request['mulkiya_back_name'],
				'mulkiya_validity' => $request['mulkiya_validity'],
				'updated_at' => new \DateTime
			]);

			UserDriverDetailProduct::where('user_detail_id',$request['user_detail_id'])->whereNotIn('category_brand_product_id', $request['products'] )->update([
					'deleted' => '1',
					'updated_at' => new \DateTime
			]);

			$con = [];
			foreach($request['products'] as $productid)
				{

					$check = UserDriverDetailProduct::where('user_detail_id', $request['user_detail_id'])->where('category_brand_product_id', $productid)->first();
					if(!$check)
						{
							$con[] = [
								'user_id' => $request['user_id'],
								'user_detail_id' => $request['user_detail_id'],
								'category_brand_product_id' => $productid,
								'deleted' => '0'
							];
						}
					else
						{

							UserDriverDetailProduct::where('user_driver_detail_product_id', $check->user_driver_detail_product_id)->limit(1)->update([
									'deleted' => '0',
									'updated_at' => new \DateTime
							]);

						}

				}

			if(!empty($con))
				UserDriverDetailProduct::insert($con);

			return Redirect::back()->with('status','Profile updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////			Profile Update Unique Check 		///////////////////////////////
public static function service_profile_uunique(Request $request)
	{
	try{

		if(isset($request['mulkiya_number']))
			{
					
				$ocheck = UserDetail::where('mulkiya_number', $request['mulkiya_number'])->where('user_id', '!=' ,$request['user_id'])->first();
				if($ocheck)
					return Response(['success' => 0, 'statuscode' => 400, 'msg' => 'Sorry this mulkiya number is already taken' ],200);

			}				

				return Response(['success' => 1, 'statuscode' => 200, 'msg' => 'Validated successfully' ],200);

		}
	catch(\Exception $e)
		{
			return Response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage() ],500);
		}
	}


///////////////////////

}
