<?php

namespace App\Http\Controllers\ServicePanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Mail;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Illuminate\Cookie\CookieJar;
use Request as ORequest;

use App\Http\Controllers\CommonController;

use App\Models\CategoryBrandProduct;
use App\Models\AdminUserDetailLogin;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\UserDriverDetailProduct;
use App\Models\Order;

class ServicePanelOrderController extends Controller
{
/////////////////////////		Service All Orders    /////////////////////////////////////
public static function service_oall(Request $request)
	{
	try{

			$data = CommonController::order_set_starting_ending($request->all());

			$orders = Order::driver_all_orders($data);
			$products = CategoryBrandProduct::brand_driver_products_json($data);

			return View::make('ServicePanel.Orders.All', compact('orders', 'products'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

/////////////////////////		Service Completed Orders    /////////////////////////////////////
public static function ser_ocompleted(Request $request)
	{
	try{

			$data = CommonController::order_set_starting_ending($request->all());

			$data['order_status_array'] = ['SerComplete'];

			$orders = Order::driver_all_orders($data);
			$products = CategoryBrandProduct::brand_driver_products_json($data);

			return View::make('ServicePanel.Orders.Completed', compact('orders', 'products'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

/////////////////////////		Services Scheduled 		//////////////////////////////////////////
public static function ser_oscheduled(Request $request)
	{
	try{

			$data = CommonController::order_set_starting_ending($request->all());

			$data['order_status_array'] = ['Scheduled', 'DPending', 'DApproved'];

			$orders = Order::driver_all_orders($data);
			$products = CategoryBrandProduct::brand_driver_products_json($data);

			return View::make('ServicePanel.Orders.Scheduled', compact('orders', 'products'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

/////////////////////////		Services Cancelled 		//////////////////////////////////////////
public static function ser_ocancelled(Request $request)
	{
	try{

			$data = CommonController::order_set_starting_ending($request->all());

			$data['order_status_array'] = ['DriverCancel', 'CustCancel', 'SysSchCancelled', 'DSchCancelled', 'DSchTimeout'];

			$orders = Order::driver_all_orders($data);
			$products = CategoryBrandProduct::brand_driver_products_json($data);

			return View::make('ServicePanel.Orders.Cancelled', compact('orders', 'products'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}


/////////////////////////////		Order Details 		////////////////////////////////////
public static function service_order_details(Request $request)
	{
	try{

			$rules = array(
				'order_id' => 'required|exists:orders,order_id,driver_user_detail_id,'.$request['user_detail_id']
			);
			$msg = [
				'order_id.exists' => "Sorry, this order is currently not available"
			];

			$validator = Validator::make($request->all(),$rules);
				if($validator->fails())
					return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();

			$order = Order::service_driver_single_ride_details($request->all());

			//dd($order['category'], $order['category_brand'], $order['category_brand_product']);

			return View::make('ServicePanel.Orders.serviceOrderDetails', compact('order'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

/////////////////////////////		All ORders 		////////////////////////////////////////
public static function service_order_all(Request $request)
	{
	try{

			$data = CommonController::order_set_starting_ending($request->all());

			$orders = Order::driver_all_orders($data);

			//dd($order['category'], $order['category_brand'], $order['category_brand_product']);

			return View::make('ServicePanel.Orders.serviceOrderDetails', compact('order'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}


////////////////////////////

}
