<?php

namespace App\Http\Controllers\ServicePanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Mail;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Illuminate\Cookie\CookieJar;
use Request as ORequest;

use App\Http\Controllers\CommonController;

use App\Models\Category;
use App\Models\CategoryBrand;
use App\Models\CategoryBrandProduct;
use App\Models\UserDriverDetailProduct;
use App\Models\OrderRating;
use App\Models\PaymentLedger;

class ServicePanelOtherController extends Controller
{
/////////////////////	Service Panel Products    ///////////////////////////////////////////
public static function service_products_all(Request $request)
	{
	try{

			$SDetails = \App("SDetails");

			$request['category_brand_id'] = $SDetails->user_detail['category_brand_id'];
			$brand = CategoryBrand::admin_single_brand_details($request->all());

			$request['category_id'] = $brand->category_id;
			$category = Category::admin_single_cat_details($request->all());

			//$products = UserDriverDetailProduct::service_driver_products($request->all());
			$products = CategoryBrandProduct::single_service_all_products($request->all());

			return View::make('ServicePanel.Adons.allProducts', compact('category', 'brand', 'products'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

////////////////////		Product Update 		///////////////////////////////////////////
public static function service_panel_product_update(Request $request)
	{
	try{

			$rules = array(
				'user_driver_detail_product_id' => 'required|exists:user_driver_detail_products,user_driver_detail_product_id,user_detail_id,'.$request['user_detail_id'],
				'alpha_price' => 'required|min:0',
				'price_per_quantity' => 'required|min:0',
				'price_per_distance' => 'required|min:0',
				'price_per_weight' => 'required|min:0',
				'price_per_hr' => 'required|min:0',
				'price_per_sq_mt' => 'required|min:0'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			UserDriverDetailProduct::where('user_driver_detail_product_id',$request['user_driver_detail_product_id'])->limit(1)->update([
					'alpha_price' => $request['alpha_price'],
					'price_per_quantity' => $request['price_per_quantity'],
					'price_per_distance' => $request['price_per_distance'],
					'price_per_weight' => $request['price_per_weight'],
					'price_per_hr' => $request['price_per_hr'],
					'price_per_sq_mt' => $request['price_per_sq_mt'],
					'updated_at' => new \DateTime
			]);

			return Redirect::back()->with('status','Product price updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}

/////////////////////		Service Driver Reviews 		/////////////////////////////////////////
public static function service_reviews_all(Request $request)
	{
	try{

			$data = CommonController::set_starting_ending($request->all());

			$data['created_bys'] = ["Customer", "Driver"];
			$reviews = OrderRating::admin_all_reviews($data);

			return View::make('ServicePanel.Adons.allReviews', compact('reviews'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}


///////////////////////			Service Panel Ledgers 		/////////////////////////////////
public static function ser_ledgers (Request $request)
	{
	try{

			$data = CommonController::order_set_starting_ending($request->all());

			$ledgers = PaymentLedger::admin_all_ledgers($data);

			return View::make("ServicePanel.Adons.allLedgers", compact('ledgers'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}



/////////////////////

}
