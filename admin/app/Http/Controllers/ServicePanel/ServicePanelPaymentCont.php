<?php

namespace App\Http\Controllers\ServicePanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;

use App\Http\Controllers\CommonController;

use App\Models\Payment;
use App\Models\Order;

class ServicePanelPaymentCont extends Controller
{
////////////////		Service Panel Service Payment    /////////////////////////////////
public static function driver_ser_pay_all(Request $request)
	{
	try{

			$data = CommonController::set_starting_ending($request->all());

			$data['seller_user_type_id'] = $request['user_type_id'];

			$payments = Payment::driver_orders_payment($data);
			$psums = Payment::driver_pay_sums($data);

			return View::make("ServicePanel.Payments.SerPayments", compact('payments', 'psums'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage());
		}
	}


////////////////////

}
