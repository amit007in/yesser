<?php

namespace App\Http\Controllers\SupportPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Mail;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Illuminate\Cookie\CookieJar;
use Request as ORequest;

use App\Http\Controllers\CommonController;

use App\Models\CategoryBrandProduct;
use App\Models\AdminUserDetailLogin;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\UserDriverDetailProduct;
use App\Models\Order;


class SupportPanelOrderController extends Controller
{
/////////////////////////		support All Orders    /////////////////////////////////////
public static function support_order_all(Request $request)
	{
	try{

			$data = CommonController::order_set_starting_ending($request->all());

			$orders = Order::driver_all_orders($data);
			$products = CategoryBrandProduct::brand_driver_products_json($data);

			return View::make('SupportPanel.Orders.supportAllOrders', compact('orders', 'products'))->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			dd($e->getMessage());
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

/////////////////////////////		Order Details 		////////////////////////////////////
public static function support_order_details(Request $request)
	{
	try{

			$rules = array(
				'order_id' => 'required|exists:orders,order_id,driver_user_detail_id,'.$request['user_detail_id']
			);
			$msg = [
				'order_id.exists' => "Sorry, this order is currently not available"
			];

			$validator = Validator::make($request->all(),$rules);
				if($validator->fails())
					return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();

			$order = Order::support_driver_single_ride_details($request->all());

			//dd($order['category'], $order['category_brand'], $order['category_brand_product']);

			return View::make('SupportPanel.Orders.supportOrderDetails', compact('order'));

		}
	catch(\Exception $e)
		{
			dd($e->getMessage());
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}


/////////////////////////////
}
