<?php

namespace App\Http\Middleware;

use Request;
use Closure;
use Session;
use Illuminate\Support\Facades\Redirect;
use View;
use DB;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Route;

use App\Models\Admin;
use App\Models\AdminUserDetailLogin;


class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
        {
            $currentPath = Route::currentRouteName();

            if(! Request::cookie('adminAccessToken') )
                return Redirect::route('admin_login_get')->withErrors('Please Login First');

            $Details = AdminUserDetailLogin::where('access_token', Request::cookie('adminAccessToken'))
            ->with([
                'admin' => function($q1) {
                    $q1->select("*",
                        DB::RAW("(CONCAT('".env('RESIZE_URL')."',profile_pic)) AS pic_url")
                    );
                }
            ])
            ->first();
            if(!$Details)
                return Redirect::route('admin_login_get')->withErrors('Please Login First');

            $time = new \DateTime('now', new \DateTimeZone($Details->timezone));
            $request['timezonez'] = $time->format('P');
            $request['timezone'] = $Details->timezone;
            $request['admin_language_id'] = $Details['admin']->language_id;

            \Cookie::queue('adminAccessToken', $Details->access_token , 60);
            \Cookie::queue('admintz', $request['timezonez'] , 60);

            \App::instance('admin', $Details);
            View::share('admin', $Details);

//////////////////      Language Set    //////////////////////////////////
            if($request['admin_language_id'] == 2)
                {
                    \App::setLocale('2_Hindi');
                }
            elseif($request['admin_language_id'] == 3)
                {
                    \App::setLocale('3_Urdu');
                }
            elseif($request['admin_language_id'] == 4)
                {
                    \App::setLocale('4_Chinese');
                }
            elseif($request['admin_language_id'] == 5)
                {
                    \App::setLocale('5_Arabic');
                }
//////////////////      Language Set    //////////////////////////////////

//dd(\App::getLocale());

            return $next($request);

        }

}
