<?php

namespace App\Http\Middleware;

use Closure;

use Redirect;
use Request;

use App\Models\AdminUserDetailLogin;

class PanelOrgLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $currentPath = \Route::currentRouteName();

        if(! Request::cookie('OrgAccessToken') )
            return Redirect::route('org_panel_login1')->withErrors('Please Login First');

        $request['access_token'] = Request::cookie('OrgAccessToken');
        $request['user_type_id'] = 2;
        $request['admin_language_id'] = Request::cookie("OrgLanguage") ? Request::cookie("OrgLanguage") : 1;

            $admin = AdminUserDetailLogin::middlewareGetDetails($request->all());
            if(!$admin || !$admin->organisation)
                {
                    \Cookie::queue('OrgAccessToken', 0 , 0);
                    \Cookie::queue('OrgLanguage', 0 , 0);

                    return Redirect::route('org_panel_login1')->withErrors('Please Login First');
                }

            $time = new \DateTime('now', new \DateTimeZone($admin->timezone));
            $request['timezonez'] = $time->format('P');
            $request['timezone'] = $admin->timezone;

            $request['admin_language_id'] = $admin->organisation['language_id'];
            $request['organisation_id'] = $admin->organisation['organisation_id'];

            \Cookie::queue('OrgAccessToken', $admin->access_token , 60*60);
            \Cookie::queue('OrgLanguage', $admin->user_detail['language_id'] , 60*60);

            \App::instance('OrgDetails', $admin);
            \View::share('OrgDetails', $admin);

//////////////////      Language Set    //////////////////////////////////
            if($request['admin_language_id'] == 2)
                {
                    \App::setLocale('2_Hindi');
                }
            elseif($request['admin_language_id'] == 3)
                {
                    \App::setLocale('3_Urdu');
                }
            elseif($request['admin_language_id'] == 4)
                {
                    \App::setLocale('4_Chinese');
                }
            elseif($request['admin_language_id'] == 5)
                {
                    \App::setLocale('5_Arabic');
                }
//////////////////      Language Set    //////////////////////////////////


        return $next($request);
    }
}
