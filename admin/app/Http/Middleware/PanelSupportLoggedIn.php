<?php

namespace App\Http\Middleware;

use Closure;

use Redirect;
use Request as ORequest;

use App\Models\AdminUserDetailLogin;

class PanelSupportLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

            $currentPath = \Route::currentRouteName();

            if(!ORequest::cookie('SupportAccessToken'))
                return Redirect::route('support_panel_login')->withErrors('Please Login First');

            $request['access_token'] = ORequest::cookie('SupportAccessToken');
            $request['user_type_id'] = 3;
            $request['admin_language_id'] = ORequest::cookie("SupportLanguage") ? ORequest::cookie("SupportLanguage") : 1;

            $admin = AdminUserDetailLogin::middlewareGetDetails($request->all());
            if(!$admin)
                {
                    \Cookie::queue('SupportAccessToken', $request['access_token'], 0);
                    \Cookie::queue('ServiceLanguage', $login->user_detail['language_id'], 0);
                    return Redirect::route('support_panel_login')->withErrors('Please Login First');
                }

            $time = new \DateTime('now', new \DateTimeZone($admin->timezone));
            $request['timezonez'] = $time->format('P');
            $request['timezone'] = $admin->timezone;

            $request['admin_language_id'] = $admin->user_detail['language_id'];
            $request['user_detail_id'] = $admin->user_detail['user_detail_id'];
            $request['category_brand_id'] = $admin->user_detail['category_brand_id'];

            \Cookie::queue('SupportAccessToken', $admin->access_token, 60*60);
            \Cookie::queue('ServiceLanguage', $admin->user_detail['language_id'], 60*60);

            \App::instance('SDetails', $admin);
            \View::share('SDetails', $admin);

//////////////////      Language Set    //////////////////////////////////
            if($request['admin_language_id'] == 2)
                {
                    \App::setLocale('2_Hindi');
                }
            elseif($request['admin_language_id'] == 3)
                {
                    \App::setLocale('3_Urdu');
                }
            elseif($request['admin_language_id'] == 4)
                {
                    \App::setLocale('4_Chinese');
                }
            elseif($request['admin_language_id'] == 5)
                {
                    \App::setLocale('5_Arabic');
                }
//////////////////      Language Set    //////////////////////////////////

        return $next($request);
    }
}
