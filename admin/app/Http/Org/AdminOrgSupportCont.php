<?php

namespace App\Http\Controllers\Admin\Org;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;

use App\Http\Controllers\CommonController;

use App\Models\Category;
use App\Models\Organisation;
use App\Models\OrganisationCategory;
use App\Models\CategoryBrand;
use App\Models\CategoryBrandProduct;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\UserDriverDetailProduct;


class AdminOrgSupportCont extends Controller
{
/////////////////////////		Organisation Support   //////////////////////////
public static function admin_org_support_all(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$request['category_type'] = 'Support';
			$request['user_type_id'] = 3;

			$organisation = Organisation::admin_single_org_details($request->all());
			$supports = OrganisationCategory::org_all_categories($request->all());

			return View::make('Admin.Orgs.Supports.orgSupportsAll', compact('organisation', 'supports'));

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

/////////////////////////		Support Driver Add Get 	//////////////////////////////
public static function admin_org_support_dadd(Request $request)
	{
	try{

			$data = Input::all();
			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
				'category_id' => 'required|exists:categories,category_id,category_type,Support'
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$organisation = Organisation::admin_single_org_details($request->all());

			return View::make('Admin.Orgs.Supports.Drivers.addSupportDrivers', compact('organisation'))->with('category_id', $request['category_id']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

///////////////////////			Driver Add Post 		//////////////////////////////
public static function admin_org_support_dadd_post(Request $request)
	{
	try{
			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
				'category_id' => 'required|exists:categories,category_id,category_type,Support',
				'name' => 'required',
				'profile_pic' => 'required|image',
				//'email' => 'required|email|unique:users,email',
				'phone_number' => 'required|unique:users,phone_number',
				'mulkiya_number' => 'required|unique:user_details,mulkiya_number',
				'mulkiya_validity' => 'required',
				'mulkiya_front' => 'sometimes|image',
				'mulkiya_back' => 'sometimes|image',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

////////////////////////////		Images Upload 		////////////////////////////////
			$request['profile_pic_name'] = CommonController::image_uploader(Input::file('profile_pic'));
				if(empty($request['profile_pic_name']))
					return Redirect::back()->withErrors('Sorry, profile pic not uploaded. Please try again')->withInput($request->all());
////////////////////////////		Images Upload 		////////////////////////////////

			if(!empty(Input::file('mulkiya_front')))
				{	
					$request['mulkiya_front_name'] = CommonController::image_uploader(Input::file('mulkiya_front'));
						if(empty($request['mulkiya_front_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_front_name'] = '';
				}

			if(!empty(Input::file('mulkiya_back')))
				{	
					$request['mulkiya_back_name'] = CommonController::image_uploader(Input::file('mulkiya_back'));
						if(empty($request['mulkiya_back_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_back_name'] = '';
				}

			$category = Category::where('category_id', $request['category_id'])->first();
			$request['maximum_rides'] = $category->maximum_rides;

			$request['created_by'] = 'Admin';
			$request['category_type'] = 'Support';
			$request['email'] = CommonController::generate_email();

			$user = User::insert_new_record($request->all());

			$request['user_id'] = $user->user_id;
			$request['user_type_id'] = 3;//Support
			$request['mulkiya_validity'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['mulkiya_validity'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');


			$userDetail = UserDetail::insert_new_record($request->all());
			$request['user_detail_id'] = $userDetail->user_detail_id;

			// $mail = \Mail::send('Emails.Common.Orgs.Drivers.driverRegister', array('receiver'=>$user, 'data' => $request->all() ),
			// 	function($message) use ($user)
			// 		{

	  //   				$message->to($user->email, $user->name)->subject(env('APP_NAME').' - '.trans('messages.Congrats you are added as driver') );

	  //       		});

			$request['sms'] = CommonController::send_sms([ 'phone_number' => ['968'.$request['phone_number']], 'message' => env('APP_NAME').' - '.trans('messages.Congrats you are added as driver')  ]);

			return Redirect::back()->with('status','Driver added successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////////////////		Organisation Support Drivers 		////////////////////////////////////
public static function admin_org_support_dall(Request $request)
	{
	try{

			$rules = array(
				'organisation_id' => 'required|exists:organisations,organisation_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$data = CommonController::set_starting_ending($request->all());

			$data['category_type'] = 'Support';
			$data['user_type_ids'] = [3];

			$organisation = Organisation::admin_single_org_details($request->all());
			$drivers = UserDetail::admin_get_cat_drivers($data);

			return View::make('Admin.Orgs.Supports.Drivers.allSupportDrivers', compact('organisation', 'drivers'))->with('category_id', $request['category_id'])->with('fstarting_dt',$data['fstarting_dt'])->with('fending_dt',$data['fending_dt']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////////////////	Driver Update Get 		/////////////////////////////////////
public static function admin_org_support_dupdate(Request $request)
	{
	try{

			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$driver = UserDetail::admin_driver_detail_update_get($request->all());

			$request['category_id'] = $driver->category_id;
			$request['organisation_id'] = $driver->organisation_id;

			return View::make('Admin.Orgs.Supports.Drivers.updateSupportDrivers', compact('driver'))->with('category_id', $request['category_id']);

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}

//////////////////////////////////	Driver Update Post 		/////////////////////////////////////
public static function admin_org_support_dupdate_post(Request $request)
	{
	try{

			$rules = array(
				'user_detail_id' => 'required|exists:user_details,user_detail_id',
				'name' => 'required',
				'profile_pic' => 'sometimes|image',
				//'email' => 'required|email|unique:users,email,'.$request['user_id'].',user_id',
				'phone_number' => 'required|unique:users,phone_number,'.$request['user_id'].',user_id',
				'mulkiya_number' => 'required|unique:user_details,mulkiya_number,'.$request['user_detail_id'].',user_detail_id',
				'mulkiya_validity' => 'required',
				'mulkiya_front' => 'sometimes|image',
				'mulkiya_back' => 'sometimes|image',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

			$driver = UserDetail::admin_driver_detail_update_get($request->all());

			$request['user_id'] = $driver->user_id;
			$request['organisation_id'] = $driver->organisation_id;

////////////////////////////		Images Upload 		////////////////////////////////
			if(!empty(Input::file('profile_pic')))
				{
				$request['profile_pic_name'] = CommonController::image_uploader(Input::file('profile_pic'));
					if(empty($request['profile_pic_name']))
						return Redirect::back()->withErrors('Sorry, profile pic not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['profile_pic_name'] = $driver->profile_pic;
				}
////////////////////////////		Images Upload 		////////////////////////////////

			if(!empty(Input::file('mulkiya_front')))
				{	
					$request['mulkiya_front_name'] = CommonController::image_uploader(Input::file('mulkiya_front'));
						if(empty($request['mulkiya_front_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_front_name'] = $driver->mulkiya_front;
				}

			if(!empty(Input::file('mulkiya_back')))
				{	
					$request['mulkiya_back_name'] = CommonController::image_uploader(Input::file('mulkiya_back'));
						if(empty($request['mulkiya_back_name']))
					return Redirect::back()->withErrors('Sorry, licence front image not uploaded. Please try again')->withInput($request->all());
				}
			else
				{
					$request['mulkiya_back_name'] = $driver->mulkiya_back;
				}

			$request['user_type_id'] = 3;//Support
			$request['mulkiya_validity'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $request['mulkiya_validity'], $request['timezone'] )->timezone('UTC')->format('Y-m-d H:i:s');


			User::where('user_id',$request['user_id'])->limit(1)->update([
					'name' => $request['name'],
					//'email' => $request['email'],
					'phone_number' => $request['phone_number'],
					'updated_at' => new \DateTime
			]);

			UserDetail::where('user_detail_id', $request['user_detail_id'])->limit(1)->update([
				'profile_pic' => $request['profile_pic_name'],
				'mulkiya_number' => $request['mulkiya_number'],
				'mulkiya_front' => $request['mulkiya_front_name'],
				'mulkiya_back' => $request['mulkiya_back_name'],
				'mulkiya_validity' => $request['mulkiya_validity'],
				'updated_at' => new \DateTime
			]);

			return Redirect::back()->with('status','Driver updated successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->withErrors($e->getMessage())->withInput($request->all());
		}
	}


////////////////////////

}
