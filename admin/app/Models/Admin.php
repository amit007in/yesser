<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 29 Jul 2018 07:48:47 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;
/**
 * Class Admin
 * 
 * @property int $admin_id
 * @property int $language_id
 * @property string $name
 * @property string $job
 * @property string $email
 * @property int $phone_number
 * @property string $password
 * @property string $forgot_token
 * @property \Carbon\Carbon $forgot_token_validity
 * @property string $profile_pic
 * @property int $service_maximum_dist
 * @property int $support_maximum_dist
 * @property float $buraq_percentage
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Admin extends Eloquent
{
	protected $primaryKey = 'admin_id';

	protected $casts = [
		'language_id' => 'int',
		'buraq_percentage' => 'float'
	];

	protected $dates = [
		'forgot_token_validity'
	];

	protected $hidden = [
		'password',
		'forgot_token'
	];

	protected $fillable = [
		'language_id',
		'name',
		'job',
		'email',
		'phone_number',
		'password',
		'forgot_token',
		'forgot_token_validity',
		'profile_pic',
		'service_maximum_dist',
		'support_maximum_dist',
		'buraq_percentage',
		'round_robin',
		'round_robin_time'
	];

	public function admin_user_detail_logins()
		{
			return $this->hasMany(\App\Models\AdminUserDetailLogin::class, 'admin_id', 'admin_id');
		}

	public function language()
		{
			return $this->belongsTo(\App\Models\Language::class, 'language_id', 'language_id');
		}

//////////////////////////		Admin Update 		/////////////////////////////
public static function super_admin_update($data,$admin)
	{
		return Admin::where('admin_id',$admin->admin_id)->update(array(
			'name'=>isset($data['name']) ? $data['name'] : $admin->name,
			'job'=>isset($data['job']) ? $data['job'] : $admin->job,
			'profile_pic'=>isset($data['profile_pic']) ? $data['profile_pic'] : $admin->profile_pic,
			'email'=>isset($data['email']) ? $data['email'] : $admin->email,
			'phone_number' => isset($data['phone_number']) ? $data['phone_number'] : $admin->phone_number,
			'password'=>isset($data['password']) ? \Hash::make($data['password']) : $admin->password,
			'service_maximum_dist' => isset($data['service_maximum_dist']) ? $data['service_maximum_dist'] : $admin->service_maximum_dist,
			'support_maximum_dist' => isset($data['support_maximum_dist']) ? $data['support_maximum_dist'] : $admin->support_maximum_dist,
			//'buraq_percentage' => $data['buraq_percentage'],
			'updated_at'=>new \DateTime
		));
	}

/////////////////////		Service Panel 	Dashboard Data 	//////////////////////////////////
public static function service_panel_dash_data($data)
	{

		return Admin::select(
			DB::RAW("(SELECT COUNT(*) FROM orders WHERE driver_user_detail_id=".$data['user_detail_id']." AND order_timings BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."') as all_counts"),
			DB::RAW("(SELECT COUNT(*) FROM orders WHERE order_status IN ('SerComplete') AND driver_user_detail_id=".$data['user_detail_id']." AND order_timings BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."') as completed_counts"),
			DB::RAW("(SELECT COUNT(*) FROM orders WHERE order_status IN ('Scheduled', 'DPending', 'DApproved') AND driver_user_detail_id=".$data['user_detail_id']." AND order_timings BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."') as schedule_counts"),
			DB::RAW("(SELECT COUNT(*) FROM orders WHERE order_status IN ('CustCancel', 'DSchCancelled', 'SysSchCancelled', 'DSchTimeout', 'DriverCancel') AND driver_user_detail_id=".$data['user_detail_id']." AND order_timings BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."') as cancelled_counts"),
			DB::RAW("(SELECT COUNT(*) FROM order_ratings AS ORT1 WHERE (driver_user_detail_id=".$data['user_detail_id']." OR customer_user_detail_id=".$data['user_detail_id']." AND updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."')) as review_counts")
		)
		->first();

	}

/////////////////////		Graph Data 		/////////////////////////////////////
public static function service_panel_graph_data($data)
	{

	    if(!isset($data['yr_only']))
	        $yr_only = Carbon::now($data['timezonez'])->format('Y');
	    else
	        $yr_only  = $data['yr_only'];

	$querys = DB::SELECT("SELECT 
		months_tbl.id as id,
		(SELECT COUNT(*) FROM orders as O1 WHERE O1.driver_user_detail_id=:ud1 AND YEAR(O1.created_at) = :yr1 AND (MONTH(O1.created_at) = months_tbl.id) ) as all_count,
		(SELECT COUNT(*) FROM orders as O2 WHERE O2.order_status IN ('SerComplete') AND O2.driver_user_detail_id=:ud2 AND YEAR(O2.created_at) = :yr2 AND (MONTH(O2.created_at) = months_tbl.id) ) as completed,
		(SELECT COUNT(*) FROM orders as O3 WHERE O3.order_status IN ('Scheduled', 'DPending', 'DApproved') AND O3.driver_user_detail_id=:ud3 AND YEAR(O3.created_at) = :yr3 AND (MONTH(O3.created_at) = months_tbl.id) ) as scheduled,
		(SELECT COUNT(*) FROM orders as O4 WHERE O4.order_status IN ('CustCancel', 'DSchCancelled', 'SysSchCancelled', 'DSchTimeout', 'DriverCancel') AND O4.driver_user_detail_id=:ud4 AND YEAR(O4.created_at) = :yr4 AND (MONTH(O4.created_at) = months_tbl.id) ) as cancelled,
		(SELECT COUNT(*) FROM order_ratings AS ORT1 WHERE (driver_user_detail_id=:ud5 OR customer_user_detail_id=:ud6) AND YEAR(created_at) = :yr5 AND MONTH(created_at) = months_tbl.id ) as reviews

	    FROM months_tbl
	    ",['yr1'=>$yr_only, 'yr2'=>$yr_only, 'yr3'=>$yr_only, 'yr4'=>$yr_only, 'yr5'=>$yr_only, 'ud1' => $data['user_detail_id'], 'ud2' => $data['user_detail_id'], 'ud3' => $data['user_detail_id'], 'ud4' => $data['user_detail_id'], 'ud5' => $data['user_detail_id'], 'ud6' => $data['user_detail_id'] ]);

        $all_count = $completed = $scheduled = $cancelled = $reviews = [];

	    foreach($querys as $query)
			{
		    	array_push($all_count, $query->all_count);
		    	array_push($completed, $query->completed);
		    	array_push($scheduled, $query->scheduled);
		    	array_push($cancelled, $query->cancelled);
		    	array_push($reviews, $query->reviews);		    			
		    }

    		return array('all_count'=> $all_count, 'completed' => $completed, 'scheduled' => $scheduled, 'cancelled' => $cancelled, 'reviews' => $reviews, 'dt'=> $yr_only);

		}

////////////////////////
}
