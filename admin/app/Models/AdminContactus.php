<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 Aug 2018 17:37:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;

/**
 * Class AdminContactus
 * 
 * @property int $admin_contactus_id
 * @property int $user_id
 * @property int $user_detail_id
 * @property int $user_type_id
 * @property int $parent_admin_contactus_id
 * @property string $message
 * @property string $is_read
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 * @property \App\Models\UserDetail $user_detail
 * @property \App\Models\UserType $user_type
 *
 * @package App\Models
 */
class AdminContactus extends Eloquent
{
	protected $table = 'admin_contactus';
	protected $primaryKey = 'admin_contactus_id';

	protected $casts = [
		'user_id' => 'int',
		'user_detail_id' => 'int',
		'user_type_id' => 'int',
		'parent_admin_contactus_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'user_detail_id',
		'user_type_id',
		'parent_admin_contactus_id',
		'message',
		'is_read'
	];

	public function user()
		{
			return $this->belongsTo(\App\Models\User::class, 'user_id', 'user_id');
		}

	public function user_detail()
		{
			return $this->belongsTo(\App\Models\UserDetail::class, 'user_detail_id', 'user_detail_id');
		}

	public function user_type()
		{
			return $this->belongsTo(\App\Models\UserType::class, 'user_type_id', 'user_type_id');
		}

	public function replies()
		{
			return $this->HasMany(\App\Models\AdminContactus::class, 'parent_admin_contactus_id', 'admin_contactus_id');
		}

///////////////////		Admin Contactus Lisitngs 	///////////////////////////////
public static function admin_contactus_all($data)
	{

			$msgs = AdminContactus::whereBetween('created_at', [$data['starting_dt'], $data['ending_dt']]);

			$msgs->select("*",
				DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
				DB::RAW("( date_format(CONVERT_TZ(updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz"),
				DB::RAW("(SELECT COUNT(*) FROM admin_contactus as ac WHERE ac.parent_admin_contactus_id=admin_contactus.admin_contactus_id) as history_counts")
			);

			$msgs->with([

				'user',

				'user_detail' => function($q1) {

					$q1->select("*",
						DB::RAW("(CASE WHEN profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('IMAGE_URL')."',profile_pic) END) as profile_pic_url")
					);

				}

			]);

			$msgs->where('parent_admin_contactus_id',0);

			return $msgs->get();

	}

///////////////////		Admin Contactus Lisitngs 	///////////////////////////////
public static function single_contactus_details($data)
	{

			$msg = AdminContactus::where('admin_contactus_id', $data['admin_contactus_id']);

			$msg->select("*");

			$msg->with([

				'user',

				'user_detail' => function($q1) {

					$q1->select("*",
						DB::RAW("(CASE WHEN profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('SOCKET_URL')."/Uploads/',profile_pic) END) as profile_pic_url")
					);

				},

				'replies' => function($q2) use ($data) {
					$q2->select("*",
						DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")
					);
				}

			]);

			return $msg->first();

	}

//////////////////////		Contact Us Details 		//////////////////////////////////
public static function add_new_record($data)
	{
		$msg = new AdminContactus();

		$msg->user_id = isset($data['user_id']) ? $data['user_id'] : 0;
		$msg->user_detail_id = isset($data['user_detail_id']) ? $data['user_detail_id'] : 0;
		$msg->user_type_id = isset($data['user_type_id']) ? $data['user_type_id'] : 0;
		$msg->parent_admin_contactus_id = $data['parent_admin_contactus_id'];
		$msg->message = $data['message'];
		$msg->is_read = "0";
		$msg->created_at = new \DateTime;
		$msg->updated_at = new \DateTime;

		$msg->save();

		return $msg;

	}

////////////////////////		

}
