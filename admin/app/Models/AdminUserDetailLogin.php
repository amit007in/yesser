<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 17 Aug 2018 05:38:38 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;

/**
 * Class AdminUserDetailLogin
 * 
 * @property int $admin_user_detail_login_id
 * @property int $user_detail_id
 * @property int $user_id
 * @property int $organisation_id
 * @property int $user_type_id
 * @property string $access_token
 * @property string $timezone
 * @property string $timezonez
 * @property string $otp
 * @property string $ip_address
 * @property float $latitude
 * @property float $longitude
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\UserDetail $user_detail
 * @property \App\Models\User $user
 * @property \App\Models\Organisation $organisation
 * @property \App\Models\UserType $user_type
 *
 * @package App\Models
 */
class AdminUserDetailLogin extends Eloquent
{
	protected $primaryKey = 'admin_user_detail_login_id';

	protected $casts = [
		'admin_id' => 'admin_id',
		'user_detail_id' => 'int',
		'user_id' => 'int',
		'organisation_id' => 'int',
		'user_type_id' => 'int',
		'latitude' => 'float',
		'longitude' => 'float'
	];

	protected $hidden = [
		'access_token'
	];

	protected $fillable = [
		'admin_id',
		'user_detail_id',
		'user_id',
		'organisation_id',
		'user_type_id',
		'latitude',
		'longitude',
		'access_token',
		'timezone',
		'timezonez',
		'otp',
		'ip_address'
	];

	public function admin()
		{
			return $this->belongsTo(\App\Models\Admin::class, 'admin_id', 'admin_id');
		}

	public function user_detail()
		{
			return $this->belongsTo(\App\Models\UserDetail::class, 'user_detail_id', 'user_detail_id');
		}

	public function user()
		{
			return $this->belongsTo(\App\Models\User::class, 'user_id', 'user_id');
		}

	public function organisation()
		{
			return $this->belongsTo(\App\Models\Organisation::class, 'organisation_id', 'organisation_id');
		}

	public function user_type()
		{
			return $this->belongsTo(\App\Models\UserType::class, 'user_type_id', 'user_type_id');
		}

////////////////////		Insert New Row 	///////////////////////////////
public static function insertNewRow($data)
	{

		$adminLogin = new AdminUserDetailLogin();

		$adminLogin->user_detail_id = isset($data['user_detail_id']) ? $data['user_detail_id'] : 0;
		$adminLogin->admin_id = isset($data['admin_id']) ? $data['admin_id'] : 0;
		$adminLogin->user_id = isset($data['user_id']) ? $data['user_id'] : 0;
		$adminLogin->organisation_id = isset($data['organisation_id']) ? $data['organisation_id'] : 0;
		$adminLogin->user_type_id = $data['user_type_id'];
		$adminLogin->access_token = str_random(100);
		$adminLogin->timezone = $data['timezone'];
		$adminLogin->timezonez = $data['timezonez'];
		$adminLogin->otp = isset($data['otp']) ? $data['otp'] : "";
		$adminLogin->ip_address = isset($data['ip_address']) ? $data['ip_address'] : "";
		$adminLogin->created_at = new \DateTime;
		$adminLogin->updated_at = new \DateTime;

		$adminLogin->save();

		return $adminLogin;

	}

//////////////////////////		Update Row 		///////////////////////////////
public static function updateRow($data)
	{
	
		AdminUserDetailLogin::where('access_token',$data['access_token'])->update([
			'access_token' => str_random(100),
			'otp' => $data['otp'],
			'updated_at' => new \DateTime
		]);

	}

////////////////////////		Logged In details 		//////////////////////
public static function getDetails($data)
	{

		$detail = AdminUserDetailLogin::where('access_token', $data['access_token']);

		$detail->with([

			'user_detail'

		]);

		return $detail->first();

	}
///////////////////////////		Middleware Data Get 		//////////////////
public static function middlewareGetDetails($data)
	{

		$detail = AdminUserDetailLogin::where('access_token', $data['access_token']);

		$detail->with([
			'user',
			'user_detail' => function($q2) use ($data) {

				$q2->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('SERVICE_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);
//$SDetails->user_detail['profile_pic_url']
				// $q2->with([
				// 	'category' => function($qq1) use ($data) {

				// 		$qq1->join('category_details', 'category_details.category_id','categories.category_id')->where('category_details.language_id','=',$data['admin_language_id']);

				// 		$qq1->select('categories.category_id', 'category_details.name as cat_name', 'category_details.description as ca_description');

				// 	}
				// ]);

			},
			'organisation' => function($q3) use ($data) {

				$q3->select("*",
					DB::RAW("(CASE WHEN image='' THEN '".env('ORG_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',image) END) as image_url")
				);

			}
			//'user_type'
		]);

		return $detail->first();

	}

/////////////////////////		Login History Get 		//////////////////////////////////////////
public static function login_history_get($data)
	{

		$histories = AdminUserDetailLogin::whereBetween('created_at', [$data['starting_dt'], $data['ending_dt']])
		->select("*",
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
			DB::RAW("( date_format(CONVERT_TZ(updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
		);

		if(isset($data['user_type_ids']))
			$histories->whereIn('user_type_id',$data['user_type_ids']);

		if(isset($data['admin_id']))
			$histories->where('admin_id',$data['admin_id']);

		if(isset($data['user_detail_id']))
			$histories->where('user_detail_id',$data['user_detail_id']);

		if(isset($data['organisation_id']))
			$histories->where('organisation_id',$data['organisation_id']);

		return $histories->get();

	}

///////////////////////////

}
