<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 30 Jul 2018 09:47:55 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

use DB;

/**
 * Class Category
 * 
 * @property int $category_id
 * @property string $category_type
 * @property int $sort_order
 * @property float $buraq_percentage
 * @property float $maximum_distance
 * @property string $image
 * @property string $blocked
 * @property string $default_brands
 * @property int $maximum_rides
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_details
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_product_details
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_products
 * @property \Illuminate\Database\Eloquent\Collection $category_brands
 * @property \Illuminate\Database\Eloquent\Collection $category_details
 *
 * @package App\Models
 */
class Category extends Eloquent
{
	protected $primaryKey = 'category_id';

	protected $casts = [
		'sort_order' => 'int',
		'buraq_percentage' => 'float',
		'maximum_rides' => 'int',
		'reassign_status' => 'int',
		'geofencing_status' => 'int'
	];

	protected $fillable = [
		'category_type',
		'buraq_percentage',
		'sort_order',
		'image',
		'default_brands',
		'blocked',
		'maximum_distance',
		'maximum_rides',
		'interval_time', //Add M2 Part 2
		'reassign_status',//Add M2 Part 2
		'transit_offer_percentage',//Add M2 Part 2
		'transit_buraq_margin',//Add M2 Part 2
		'driver_quotation_timeout',//Add M2 Part 2
		'geofencing_status'//Add M2 Part 2
	];

	public function category_brand_details()
		{
			return $this->hasMany(\App\Models\CategoryBrandDetail::class, 'category_id', 'category_id');
		}

	public function category_brand_product_details()
		{
			return $this->hasMany(\App\Models\CategoryBrandProductDetail::class, 'category_id', 'category_id');
		}

	public function category_brand_products()
		{
			return $this->hasMany(\App\Models\CategoryBrandProduct::class, 'category_id', 'category_id');
		}

	public function category_brands()
		{
			return $this->hasMany(\App\Models\CategoryBrand::class, 'category_id', 'category_id');
		}

	public function category_details()
		{
			return $this->hasMany(\App\Models\CategoryDetail::class, 'category_id', 'category_id');
		}

	public function category_detail_single()
		{
			return $this->hasOne(\App\Models\CategoryDetail::class, 'category_id', 'category_id');
		}

	public function organisation_areas()
		{
			return $this->hasMany(\App\Models\OrganisationArea::class, 'category_id', 'category_id');
		}

	public function organisation_categories()
		{
			return $this->hasMany(\App\TempModels\OrganisationCategory::class, 'category_id', 'category_id');
		}

	public function user_driver_detail_services()
		{
			return $this->hasMany(\App\Models\UserDriverDetailService::class, 'category_id', 'category_id');
		}

	public function org_products()
		{
			return $this->hasMany(\App\Models\OrganisationCategoryBrandProduct::class, 'category_id', 'category_id');
		}

	// protected $appends = ['english_detail'];
 
 // 	function getEnglishDetailAttribute() {

	//  	//$con = new \stdClass();

	//  	return $this->category_details( /*function($q) {

	//  		// $q->where('language_id',1);
	//  		// $q->select('name', 'description', 'image');

	//  	}*/ );

//	}
 
//////////////////////////////		Add New Record 		////////////////////////////////////////////////////////
public static function add_new_record($data)
	{
		
		$cat = new Category();

		$cat->category_type = $data['category_type'];
		$cat->sort_order = $data['ranking'];
		$cat->buraq_percentage = $data['buraq_percentage'];
		$cat->maximum_distance = isset($data['maximum_distance']) ? $data['maximum_distance'] : 5;
		$cat->image = isset($data['image_name']) ? $data['image_name'] : "";
		$cat->maximum_rides = isset($data['maximum_rides']) ? $data['maximum_rides'] : 10;
		$cat->save();

		return $cat;

	}

//////////////////////////////		Admin Get Service Categories 		////////////////////////////////////////
public static function admin_get_service_categories($data)
	{

		$categories = Category::where('category_type','Service');

		$categories->select('*',
			DB::RAW("( date_format(CONVERT_TZ(updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz"),
			DB::RAW("( SELECT COUNT(*) FROM category_brands as cb WHERE cb.category_id=categories.category_id) as brand_counts"),
			DB::RAW("(SELECT COUNT(*) FROM orders WHERE orders.category_id=categories.category_id) as order_counts"),
			DB::RAW("(SELECT COUNT(*) FROM user_details as ud WHERE ud.category_id=categories.category_id) as driver_counts")
			
			// DB::RAW("( CASE WHEN categories.category_id IN (1,2) THEN (SELECT COUNT(*) FROM category_subs as cs WHERE cs.category_id=categories.category_id AND cs.category_sub_type='Quantity') ELSE 0 END) as subcat_quantity_count"),
			// DB::RAW("(SELECT COUNT(*) FROM category_subs as cs WHERE cs.category_id=categories.category_id AND cs.category_sub_type='Quantity') as subcat_quantity_count"),
		);

		$categories->with([
			'category_details' => function($q1) {
				$q1->orderBy('language_id','ASC');
			},
			// 'category_subs' => function($q2) {
			// 	$q2->orderBy('sort_order','ASC');
			// }
		]);

		$categories->orderBy('sort_order','ASC');

		return $categories->get();

	}

////////////////////////////////		Single Category Details 		//////////////////////////////////
public static function admin_single_cat_details($data)
	{

		$category = Category::where('category_id', $data['category_id']);

		$category->select('*',
			DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
		);

		$category->with([

			'category_details' => function($q1) {
				$q1->orderBy('language_id','ASC');
			}

		]);

		return $category->first();

	}

////////////////////////////////		Admin Get SUpport Categoires 	///////////////////////////////////
public static function admin_get_support_categories($data)
	{

		$categories = Category::where('category_type', 'Support');

		$categories->select('*',
			DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url"),
			DB::RAW("( date_format(CONVERT_TZ(updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
		);

		$categories->with([
			'category_details' => function($q1) {
				$q1->orderBy('language_id','ASC');
			}
		]);		

		return $categories->get();

	}

////////////////		Admin Categories Listings With SIngle Language Name 		////////
public static function admin_categories_single_lang($data)
	{
		$categories = Category::where('categories.category_type',$data['category_type']);
		$categories->join('category_details', 'category_details.category_id','categories.category_id')->where('category_details.language_id','=',$data['admin_language_id']);

		$categories->select('categories.category_id', 'category_details.name as cat_name', 'category_details.description as ca_description');

		$categories->orderBy('sort_order','ASC');

		return $categories->get();

	}

////////////////		All Categories Listings 	Psrt 3.2	/////////////////////////////////////
public static function all_categories_single_lang($admin_language_id, $catids)
	{
		$categories = Category::join('category_details', 'category_details.category_id','categories.category_id')->where('categories.blocked','0')->where('category_details.language_id','=',$admin_language_id);

		$categories->select('categories.category_id', 'category_details.name as name',
			DB::RAW("(CASE WHEN ( (0 IN (".$catids.")) OR (categories.category_id IN (".$catids.")) ) THEN true ELSE false END) as selected")
		);

		$categories->orderBy('category_id','ASC');

		return $categories->get();

	}

////////////////		Admin Org Cats With Single Language Name 		////////
public static function admin_org_cat_single_lang($data)
	{
		$categories = Category::where('categories.category_type',$data['category_type']);

		$categories->join('category_details', 'category_details.category_id','categories.category_id')->where('category_details.language_id','=',$data['admin_language_id']);
		
		$categories->leftJoin('organisation_categories', function($q1) use ($data){

			$q1->on('organisation_categories.category_id', '=' ,'categories.category_id');
			$q1->where('organisation_categories.organisation_id', $data['organisation_id']);
			$q1->where('organisation_categories.deleted','0');

		});

		$categories->select('categories.category_id', 'category_details.name as cat_name', 'category_details.description as ca_description', 'organisation_categories.organisation_category_id',
			DB::RAW("(CASE WHEN organisation_categories.organisation_category_id IS NOT NULL THEN '1' ELSE '0' END) as is_selected")
		);

		$categories->orderBy('sort_order','ASC');

		return $categories->get();

	}


//////////////////////////////		App Get Services Listings 	////////////////////////////////////
public static function app_get_services_list($data)
	{

		$services = Category::where('category_type','Service');

		$services->join('category_details', function($j1) use ($data) {

			$j1->on('category_details.category_id','categories.category_id')->where('language_id',$data['app_language_id']);

		});

		$services->select('categories.category_id',  'default_brands', 'name', 'description');

		$services->with([

	/////////////	Brands 	///////////////////////
			'category_brands' => function($q1) use ($data) {

				$q1->join('category_brand_details', function($j2) use ($data) {

					$j2->on('category_brand_details.category_brand_id', 'category_brands.category_brand_id')->where('language_id',$data['app_language_id']);

				});

				$q1->select('category_brands.category_brand_id', 'category_brands.category_id', 'name', 'image', 'description',
					DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
				);

				$q1->where('category_brands.blocked','0');

				/////////////////		Products 		/////////////////////////
				$q1->with([

					'category_brand_products' => function($q2) use ($data) {

						$q2->join('category_brand_product_details', function($j3) use ($data) {

							$j3->on('category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('language_id', $data['app_language_id']);

						});

						$q2->select('category_brand_products.category_brand_product_id', 'category_brand_products.category_id', 'category_brand_products.category_brand_id', 'category_brand_products.multiple', 'name', 'description');

					}

				]);
				/////////////////		Products 		/////////////////////////

				$q1->orderBy('category_brands.sort_order','ASC');

			}
	/////////////	Brands 	///////////////////////

		]);

		$services->orderBy('sort_order','ASC');

		return $services->get();

	}

//////////////////////////////			Get Support List 			////////////////////////////
public static function app_get_supports_list($data)
	{

		$supports = Category::where('category_type','Support');

		$supports->select('category_id', 'sort_order', 'image', 'category_type',
			DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
		);

		$supports->where('blocked','0');

		$supports->orderBy('sort_order','ASC');

		return $supports->get();

	}

////////////////////////////////	Services Listings 	/////////////////////////////////////////
public static function service_support_listings($category_type, $language_id)
	{
		return Category::where('category_type', $category_type)
		->where('blocked','0')
		->join('category_details', 'category_details.category_id','categories.category_id')
		->where('category_details.language_id','=', $language_id)
		->select("*")
		->get();

	}

////////////////////////////////

}