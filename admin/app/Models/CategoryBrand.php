<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 30 Jul 2018 09:47:55 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

use DB;

/**
 * Class CategoryBrand
 * 
 * @property int $category_brand_id
 * @property int $category_id
 * @property int $sort_order
 * @property float $buraq_percentage
 * @property string $category_brand_type
 * @property string $blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Category $category
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_details
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_product_details
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_products
 *
 * @package App\Models
 */
class CategoryBrand extends Eloquent
{
	protected $primaryKey = 'category_brand_id';

	protected $casts = [
		'category_id' => 'int',
		'sort_order' => 'int',
		'buraq_percentage' => 'float',
		'maximum_radius'=> 'int',//Add New Field M2
		'is_default'=> 'int'//Add New Field M2
	];

	protected $fillable = [
		'category_id',
		'sort_order',
		'buraq_percentage',
		'category_brand_type',
		'blocked',
		'start_time',
		'end_time',
		'maximum_radius',//Add New Field M2
		'is_default'//Add New Field M2
	];

	public function category()
		{
			return $this->belongsTo(\App\Models\Category::class);
		}

	// public function category_brand_detail_single()
	// 	{
	// 		return $this->hasOne(\App\Models\CategoryBrandDetail::class, 'category_brand_id', 'category_brand_id');
	// 	}
	public function category_brand_details()
		{
			return $this->hasMany(\App\Models\CategoryBrandDetail::class, 'category_brand_id', 'category_brand_id');
		}

	public function category_brand_product_details()
		{
			return $this->hasMany(\App\Models\CategoryBrandProductDetail::class, 'category_brand_id', 'category_brand_id');
		}

	public function category_brand_products()
		{
			return $this->hasMany(\App\Models\CategoryBrandProduct::class, 'category_brand_id', 'category_brand_id');
		}

	public function org_products()
		{
			return $this->hasMany(\App\Models\OrganisationCategoryBrandProduct::class, 'category_brand_id', 'category_brand_id');
		}

////////////////////		Attributes 		//////////////////////////////
	// protected $appends = ["eng_details", "hindi_details", "urdu_details", "chinese_details", "arabic_details"];

	// function getEngDetailsAttribute()
	// 	{
	// 		return $this->category_brand_detail_single()->where('language_id',1)->first();
	// 	}
	// function getHindiDetailsAttribute()
	// 	{
	// 		return $this->category_brand_detail_single()->where('language_id',2)->first();
	// 	}
	// function getUrduDetailsAttribute()
	// 	{
	// 		return $this->category_brand_detail_single()->where('language_id',3)->first();
	// 	}
	// function getChineseDetailsAttribute()
	// 	{
	// 		return $this->category_brand_detail_single()->where('language_id',4)->first();
	// 	}
	// function getArabicDetailsAttribute()
	// 	{
	// 		return $this->category_brand_detail_single()->where('language_id',5)->first();
	// 	}
////////////////////		Attributes 		//////////////////////////////

//////////////////////////////////		Single Category Brands 		///////////////////////////////////////////
public static function admin_single_cat_brands($data)
	{

		$brands = CategoryBrand::where('category_id',$data['category_id']);

		$brands->select('*',
			DB::RAW("( date_format(CONVERT_TZ(updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz"),
			DB::RAW("( SELECT COUNT(*) FROM category_brand_products as cbp where cbp.category_brand_id=category_brands.category_brand_id ) as product_counts")
		);

		$brands->with([
			'category_brand_details' => function($q1) use ($data) {

				$q1->select('*',
					//DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")					
					DB::RAW("( CONCAT('".env('IMAGE_URL')."',image) ) as image_url")					
				);
				$q1->orderBy('language_id','ASC');

			}
		]);

		$brands->whereBetween('updated_at', [$data['starting_dt'], $data['ending_dt']]);

		return $brands->get();

	}

/////////////////////////////////	Single Brand Details 		////////////////////////////////////////////////
public static function admin_single_brand_details($data)
	{

		$brands = CategoryBrand::where('category_brand_id',$data['category_brand_id']);

		$brands->select('*');

		$brands->with([
			'category_brand_details' => function($q1) use ($data) {

				$q1->select('*',
					//DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
					DB::RAW("( CONCAT('".env('IMAGE_URL')."',image) ) as image_url")
				);
				$q1->orderBy('language_id','ASC');

			}
		]);

		return $brands->first();

	}


//////////////////////////////////		Insert New Brand 		////////////////////////////////////////////////
public static function add_new_category_brand($data)
	{

		$cbrand = new CategoryBrand();
		$cbrand->category_id = $data['category_id'];
		$cbrand->sort_order = $data['ranking'];
		$cbrand->buraq_percentage = $data['buraq_percentage'];
		$cbrand->category_brand_type = isset($data['category_brand_type']) ? $data['category_brand_type'] : "Brand";
		$cbrand->start_time = isset($data['start_time']) ? $data['start_time'] : ""; 
		$cbrand->end_time   = isset($data['end_time']) ? $data['end_time'] : ""; 
		$cbrand->maximum_radius = isset($data['maximum_radius']) ? $data['maximum_radius'] : 0; //Add M2 Part 2
		$cbrand->is_default     = isset($data['myCheck']) ? $data['myCheck'] : 0;//Add M2 Part 2
		$cbrand->save();

		return $cbrand;

	}

//////////////////////////////////		 Brands list Add Drivers 	/////////////////////////////////
public static function admin_brands_list_add_drivers($data)
	{

		$brands = CategoryBrand::where('category_brands.category_id', $data['category_id']);

		$brands->join('category_brand_details', 'category_brand_details.category_brand_id', 'category_brands.category_brand_id')->where('category_brand_details.language_id', $data['admin_language_id']);
		//->where('category_brands.blocked','=','0');

		$brands->orderBy('category_brands.sort_order','ASC');
                $brands->where('category_brands.blocked','0');
		return $brands->get();

	}

//////////////////////////

}
