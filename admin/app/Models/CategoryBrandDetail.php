<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 30 Jul 2018 09:47:55 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CategoryBrandDetail
 * 
 * @property int $category_brand_detail_id
 * @property int $category_id
 * @property int $language_id
 * @property int $category_brand_id
 * @property string $name
 * @property string $image
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Category $category
 * @property \App\Models\Language $language
 * @property \App\Models\CategoryBrand $category_brand
 *
 * @package App\Models
 */
class CategoryBrandDetail extends Eloquent
{
	protected $primaryKey = 'category_brand_detail_id';

	protected $casts = [
		'category_id' => 'int',
		'language_id' => 'int',
		'category_brand_id' => 'int'
	];

	protected $fillable = [
		'category_id',
		'language_id',
		'category_brand_id',
		'name',
		'image',
		'description'
	];

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class);
	}

	public function language()
	{
		return $this->belongsTo(\App\Models\Language::class);
	}

	public function category_brand()
	{
		return $this->belongsTo(\App\Models\CategoryBrand::class);
	}
}
