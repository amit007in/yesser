<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 30 Jul 2018 09:47:55 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

use DB;

/**
 * Class CategoryBrandProduct
 * 
 * @property int $category_brand_product_id
 * @property int $category_id
 * @property int $category_brand_id
 * @property string $category_sub_type
 * @property string $si_unit
 * @property float $actual_value
 * @property float $alpha_price
 * @property float $price_per_quantity
 * @property float $price_per_distance
 * @property float $price_per_weight
 * @property float $price_per_hr
 * @property float $price_per_sq_mt
 * @property int $sort_order
 * @property string $multiple
 * @property string $blocked
 * @property string $image
 * @property int $min_quantity
 * @property int $max_quantity
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Category $category
 * @property \App\Models\CategoryBrand $category_brand
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_product_details
 *
 * @package App\Models
 */
class CategoryBrandProduct extends Eloquent
{
	protected $primaryKey = 'category_brand_product_id';

	protected $casts = [
		'category_id' => 'int',
		'category_brand_id' => 'int',
		'actual_value' => 'float',
		'alpha_price' => 'float',
		'price_per_quantity' => 'float',
		'price_per_distance' => 'float',
		'price_per_weight' => 'float',
		'price_per_hr' => 'float',
		'price_per_sq_mt' => 'float',
		'sort_order' => 'int',
		'min_quantity' => 'int',
		'max_quantity' => 'int',
		'gift_quantity'=> 'int',
		'geofencing_status' => 'int',
		'should_use_buraq_margin' => 'int'
	];

	protected $fillable = [
		'category_id',
		'category_brand_id',
		'category_sub_type',
		'si_unit',
		'actual_value',
		'alpha_price',
		'price_per_quantity',
		'price_per_distance',
		'price_per_weight',
		'price_per_hr',
		'price_per_sq_mt',
		'sort_order',
		'multiple',
		'image',
		'min_quantity',
		'max_quantity',
		'blocked',
		'gift_quantity',
		'gift_offer',
		'product_margin',
		'geofencing_status',
		'should_use_buraq_margin'
	];

	public function category()
		{
			return $this->belongsTo(\App\Models\Category::class, 'category_id', 'category_id');
		}

	public function category_brand()
		{
			return $this->belongsTo(\App\Models\CategoryBrand::class, 'category_brand_id', 'category_brand_id');
		}

	public function category_brand_product_details()
		{
			return $this->hasMany(\App\Models\CategoryBrandProductDetail::class, 'category_brand_product_id', 'category_brand_product_id');
		}

	public function user_driver_detail_products()
		{
			return $this->hasMany(\App\Models\UserDriverDetailProduct::class, 'category_brand_product_id', 'category_brand_product_id');
		}

	public function org_products()
		{
			return $this->hasMany(\App\Models\OrganisationCategoryBrandProduct::class, 'category_brand_product_id', 'category_brand_product_id');
		}

///////////////////////			Single Brand All Products 		//////////////////////////////////////
public static function single_brand_all_products($data)
	{

		$products = CategoryBrandProduct::where('category_brand_id', $data['category_brand_id']);

		$products->select('*',
			DB::RAW("( date_format(CONVERT_TZ(updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz"),
			DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
		);

		$products->with(['category_brand_product_details']);

		$products->whereBetween('updated_at', [$data['starting_dt'], $data['ending_dt']]);

		return $products->get();

	}

///////////////////////			Admin Single Brand Details 		//////////////////////////////////////
public static function admin_single_details($data)
	{

		$products = CategoryBrandProduct::where('category_brand_product_id', $data['category_brand_product_id']);

		$products->select('*',
			//DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
			DB::RAW("( CONCAT('".env('IMAGE_URL')."',image) ) as image_url")
		);

		$products->with(['category_brand_product_details']);

		return $products->first();

	}


//////////////////////		Insert New Record 		////////////////////////////////////////////////////
public static function insert_new_record($data)
	{

		$cbp = new CategoryBrandProduct();

		$cbp->category_id = $data['category_id'];
		$cbp->category_brand_id = $data['category_brand_id'];
		$cbp->category_sub_type = isset($data['category_sub_type']) ? $data['category_sub_type'] : "Quantity";
		$cbp->si_unit = isset($data['si_unit']) ? $data['si_unit'] : '';
		$cbp->actual_value = $data['actual_value'];
		$cbp->alpha_price = $data['alpha_price'];

		$cbp->price_per_quantity = $data['price_per_quantity'];
		$cbp->price_per_distance = $data['price_per_distance'];
		$cbp->price_per_weight = $data['price_per_weight'];
		$cbp->price_per_hr = $data['price_per_hr'];
		$cbp->price_per_sq_mt = $data['price_per_sq_mt'];

		$cbp->sort_order = $data['ranking'];
		$cbp->multiple = isset($data['multiple']) ? $data['multiple'] : "0";
		$cbp->image = isset($data['image_name']) ? $data['image_name'] : "";

		$cbp->min_quantity   = isset($data['min_quantity']) ? $data['min_quantity'] : 0;
		$cbp->max_quantity   = isset($data['max_quantity']) ? $data['max_quantity'] : 1000;
		$cbp->gift_quantity  = isset($data['gift_quantity']) ? $data['gift_quantity'] : 0;
		$cbp->gift_offer     = isset($data['gift_offer']) ? $data['gift_offer'] : "";
		$cbp->product_margin = isset($data['product_margin']) ? $data['product_margin'] : '0.0'; //Add M3 Part 3.2
		$cbp->should_use_buraq_margin = isset($data['should_use_buraq_margin']) ? $data['should_use_buraq_margin'] : 0; //Add M3 Part 3.2
		$cbp->geofencing_status = '0'; //Add M3
		$cbp->save();

		return $cbp;

	}

//////////////////////		Brand All Products JSON 		////////////////////////
public static function brand_all_products_json($data)
	{

		$products = CategoryBrandProduct::where('category_brand_products.category_brand_id', $data['category_brand_id']);

		$products->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id');

		$products->where('category_brand_product_details.language_id', $data['admin_language_id']);

		$products->select('*',
			DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
		);

		$products->orderBy('category_brand_products.sort_order', 'ASC');

		return $products->get();

	}

//////////////////////		Driver Update Product JSON 	/////////////////////////////
public static function brand_driver_products_json($data)
	{

		$products = CategoryBrandProduct::where('category_brand_products.category_brand_id', $data['category_brand_id']);

		$products->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id');

		$products->leftJoin('user_driver_detail_products', function($q1) use ($data) {
			
			$q1->on('user_driver_detail_products.category_brand_product_id','category_brand_products.category_brand_product_id')
			->where('user_driver_detail_products.user_detail_id', $data['user_detail_id'] )
			->where('user_driver_detail_products.deleted','0');

		});

		$products->where('category_brand_product_details.language_id', $data['admin_language_id']);

		$products->orderBy('category_brand_products.sort_order', 'ASC');

		$products->select('*',
			DB::RAW("( CASE WHEN user_driver_detail_products.user_driver_detail_product_id IS NULL THEN '0' ELSE '1' END) as selected"),
			'category_brand_products.category_brand_product_id as category_brand_product_id',
			DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
		);

		return $products->get();

	}

//////////////////////		Admin ALl Products 		/////////////////////////////////////

public static function all_services_all_products($data)
	{

		$products = CategoryBrandProduct::select("*",
			DB::RAW("( date_format(CONVERT_TZ(updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz"),
			DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
		);

		$products->with([

			'category_brand_product_details' => function($q1) use ($data) {
				$q1->orderBy('language_id','ASC');
			},

			'category_brand' => function($q1) {
				$q1->with(['category_brand_details']);
			},

			'category' => function($q2) {
				$q2->with(['category_details']);
			}

		]);

		return $products->get();

	}

//////////////////////		Single Service All Products 		////////////////////////////
public static function single_service_all_products($data)
	{

		$products = CategoryBrandProduct::select("*",
				DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
			);

		$products->where('category_id',$data['category_id'])->where('blocked','0');

		$products->with([

			'category_brand_product_details' => function($q1) use ($data) {
				$q1->orderBy('language_id','ASC');
			},

			'category_brand' => function($q1) {
				$q1->with([
					'category_brand_details' => function($q2) {

						$q2->select('*', 
							DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
						);

					}
				]);
			},

		]);

		return $products->get();

	}



}
