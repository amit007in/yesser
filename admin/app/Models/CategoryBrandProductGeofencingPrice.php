<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 17 Aug 2018 05:38:38 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CategoryBrandProductGeofencingPrice
 * 
 * @property int $id
 * @property int $category_id
 * @property int $category_brand_id
 * @property int $category_brand_product_id
 * @property string $latitude
 * @property string $longitude
 * @property string $area_name
 * @property int $radius
 * @property string $price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @package App\Models
 */
class CategoryBrandProductGeofencingPrice extends Eloquent
{
	protected $primaryKey = 'id';

	protected $casts = [
		'category_id' => 'int',
		'category_brand_id' => 'int',
		'category_brand_product_id' => 'int',
		'radius' => 'int',
	];

	protected $fillable = [
		'category_id',
		'category_brand_id',
		'category_brand_product_id',
		'latitude',
		'longitude',
		'area_name',
		'radius',
		'price'
	];
	
	/**
	*@role Save Product Geofencing Prices
	*@author Raghav
	*/
	public static function save_products_price($data,$product) 
	{ 
        for ($i=0; $i < count($data['geofencing_area']); $i++) 
		{
             $store                            = new CategoryBrandProductGeofencingPrice;
             $store->category_id               = $product->category_id;
             $store->category_brand_id         = $product->category_brand_id;
             $store->category_brand_product_id = $data['category_brand_product_id'];
             $store->latitude                  = $data['geofencing_lat'][$i];
             $store->longitude                 = $data['geofencing_lng'][$i]; 
             $store->area_name                 = $data['geofencing_area'][$i];  
             $store->radius                    = $data['geofencing_radius'][$i];  
             $store->price                     = $data['geofencing_price'][$i];  
             $store->save();
        }   
    }
	
	public static function get_productprice_details($data) 
	{ 
		$categories = CategoryBrandProductGeofencingPrice::where('category_brand_product_id',$data['category_brand_product_id'])->get();
		return $categories;
	}
	
	
}