<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 30 Jul 2018 09:47:55 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CategoryDetail
 * 
 * @property int $category_detail_id
 * @property int $category_id
 * @property int $language_id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Category $category
 * @property \App\Models\Language $language
 *
 * @package App\Models
 */
class CategoryDetail extends Eloquent
{
	protected $primaryKey = 'category_detail_id';

	protected $casts = [
		'category_id' => 'int',
		'language_id' => 'int'
	];

	protected $fillable = [
		'category_id',
		'language_id',
		'name',
		'description'
	];

	public function category()
		{
			return $this->belongsTo(\App\Models\Category::class, 'category_id', 'category_id');
		}

	public function language()
		{
			return $this->belongsTo(\App\Models\Language::class, 'language_id', 'language_id');
		}

}
