<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Aug 2018 09:00:55 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;

/**
 * Class Coupon
 * 
 * @property int $coupon_id
 * @property string $code
 * @property float $amount_value
 * @property int $rides_value
 * @property \Carbon\Carbon $expires_at
 * @property string $blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $coupon_type
 * @property string $price
 *
 * @package App\Models
 */
class Coupon extends Eloquent {

    protected $primaryKey = 'coupon_id';
    protected $casts = [
        'amount_value' => 'float',
        'rides_value' => 'int',
        'price' => 'float',
        'category_id' => 'int',
        'brand_id' => 'int',
        'product_id' => 'int',
		'all_users_status'=>'int'
    ];
    protected $dates = [
        'expires_at',
        'start_at'
    ];
    protected $fillable = [
        'code',
        'amount_value',
        'rides_value',
        'expires_at',
        'blocked',
        'coupon_type',
        'title',
        'description',
        'users',
        'start_at',
        'image'
    ];

    public function coupon_user() {
        return $this->hasMany(\App\Models\CouponUser::class, 'coupon_id', 'coupon_id');
    }

//////////////////////////////			Admin Coupons All 	///////////////////////////////////
    public static function admin_coupons($data) {

        $coupons = Coupon::whereBetween('coupons.created_at', [$data['starting_dt'], $data['ending_dt']]);
        $coupons->select("*", DB::RAW("(SELECT COUNT(*) FROM coupon_users as cu WHERE cu.coupon_id=coupons.coupon_id) as purchase_counts"), DB::RAW("( date_format(CONVERT_TZ(coupons.expires_at,'+00:00','" . $data['timezonez'] . "'),'%M %d %Y. %h:%i %p') ) as expires_atz"), DB::RAW("( date_format(CONVERT_TZ(coupons.created_at,'+00:00','" . $data['timezonez'] . "'),'%M %d %Y. %h:%i %p') ) as created_atz")
        );
        $result = $coupons->get();

        for ($i = 0; $i < count($result); $i++) {
            $categoryname = array();
            $category_id = $result[$i]['category_id'];
            $brand_id = $result[$i]['brand_id'];
            $product_id = $result[$i]['product_id'];
            $coupon_id = $result[$i]['coupon_id'];
            
            $promousers = DB::table('promo_users_rides')
                    ->select('promo_users_rides.id','users.name','promo_users_rides.max_rides')
                    ->join('users', 'promo_users_rides.user_id', '=', 'users.user_id')
                    ->where('coupon_id', $coupon_id)->get()->toArray();
            
            if (count($promousers) > 0) {
                $result[$i]['promousers'] = count($promousers);
                $result[$i]['promo_users_rides'] = $promousers;
            } else {
                $result[$i]['promousers'] = "0";
                $result[$i]['promo_users_rides'] = [];
            }


            $categoryname = DB::table('category_details')->select('name')->where('language_id', $data['admin_language_id'])->where('category_id', $category_id)->get();
            if (count($categoryname) > 0) {
                $result[$i]['categoryname'] = $categoryname[0]->name;
            } else {
                $result[$i]['categoryname'] = "N/A";
            }
            $categorybrand = DB::table('category_brand_details')->select('name')->where('language_id', $data['admin_language_id'])->where('category_brand_id', $brand_id)->get();
            if (count($categorybrand) > 0) {
                $result[$i]['categorybrand'] = $categorybrand[0]->name;
            } else {
                $result[$i]['categorybrand'] = "N/A";
            }
          
            $categoryproduct = DB::table('promocode_products')
                    ->join('category_brand_product_details', 'promocode_products.product_id', '=', 'category_brand_product_details.category_brand_product_id')
                    ->where('promocode_products.coupon_id', $coupon_id)
                    ->where('category_brand_product_details.language_id', $data['admin_language_id'])
                    ->select('category_brand_product_details.name')
                    ->get();
           
            if (count($categoryproduct) > 0) {
                
                $aa = array();
                foreach($categoryproduct as $Products)
                {
                    $aa[] = $Products->name;
                }
                $result[$i]['categoryproduct'] = implode(",",$aa);
                //$result[$i]['categoryproduct'] = $categoryproduct[0]->name;
            } else {
                $result[$i]['categoryproduct'] = "N/A";
            }
        }
        //die;
        return $result;
    }
    
    public static function users_coupons_get_maxrides($id) {
        

            $promousers = DB::table('promo_users_rides')
                    ->select('promo_users_rides.id','users.name','promo_users_rides.max_rides')
                    ->join('users', 'promo_users_rides.user_id', '=', 'users.user_id')
                    ->where('coupon_id', $id)->get()->toArray();
            
          
        //die;
        return $promousers;
    }

////////////////////////////		Add New Record 	/////////////////////////////////////////////
    public static function add_new_record($data) {
        $coupon = new Coupon();
        $coupon->code = $data['code'];
        $coupon->amount_value = $data['amount_value'];
        $coupon->rides_value = $data['rides_value'];
        $coupon->expires_at = $data['expires_at'];
        $coupon->blocked = '0';
        $coupon->coupon_type = $data['coupon_type'];
        $coupon->price = $data['price'];
        $coupon->expiry_days = $data['expiry_days'];

        //Add New Parameter
        $coupon->title = $data['title'];
        $coupon->description = $data['description'];
        $coupon->category_id = $data['category_id'];
        $coupon->brand_id = $data['brandids'];
        //$coupon->product_id       = $data['productids'];
        $coupon->users = "";
        $coupon->start_at = $data['start_at'];
        $coupon->image = env('IMAGE_URL') . $data['profile_pic'];
		$coupon->all_users_status = isset($data['all_users']) ? $data['all_users'] : '0';
        $coupon->created_at = new \DateTime;
        $coupon->updated_at = new \DateTime;
        $coupon->save();
        return $coupon;
    }

  

//////////////////////////////		Single Coupon Details 	//////////////////////////////////////
    public static function single_coupon_details($data) {
        return Coupon::where('coupon_id', $data['coupon_id'])
                        ->select("*", DB::RAW("( date_format(CONVERT_TZ(expires_at,'+00:00','" . $data['timezonez'] . "'),'%Y-%m-%d %h:%i:%s') ) as expires_at"))
                        ->first();
    }

/////////////////////////////        Update Coupon     ///////////////////////////////////////////////
    public static function update_record($data) {
        return Coupon::where('coupon_id', $data['coupon_id'])->limit(1)->update([
                    'code' => $data['code'],
                    'amount_value' => $data['amount_value'],
                    'rides_value' => $data['rides_value'],
                    'price' => $data['price'],
                    'expiry_days' => $data['expiry_days'],
                    'expires_at' => $data['expires_at'],
                    'coupon_type' => $data['coupon_type'],
                    'title' => $data['title'],
                    'description' => $data['description'],
                    'category_id' => $data['category_id'],
                    'brand_id' => $data['brandids'],
                    'product_id' => "0",
                    'users' => "",
                    'start_at' => $data['start_at'],
                    'image' => $data['profile_pic'],
					'all_users_status' => isset($data['all_users']) ? $data['all_users'] : '0',
                    'updated_at' => new \DateTime
        ]);
    }

////////////////////////////
}
