<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Aug 2018 09:00:55 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;

/**
 * Class CouponUser
 * 
 * @property int $coupon_user_id
 * @property int $coupon_id
 * @property int $user_id
 * @property int $user_detail_id
 * @property int $user_type_id
 * @property int $payment_id
 * @property int $rides_value
 * @property int $coupon_value
 * @property int $rides_left
 * @property int $price
 * @property string $coupon_type
 * @property string $status
 * @property string $deleted
 * @property string $blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $expires_at
 *
 * @package App\Models
 */
class CouponUser extends Eloquent
{
	protected $primaryKey = 'coupon_user_id';

	protected $casts = [
		'coupon_id' => 'int',
		'user_id' => 'int',
		'user_detail_id' => 'int',
		'user_type_id' => 'int',
		'payment_id' => 'int',
		'rides_value' => 'int',
	];

	protected $dates = [
		'expires_at'
	];

	protected $fillable = [
		'coupon_id',
		'user_id',
		'user_detail_id',
		'user_type_id',
		'payment_id',
		'rides_value',
		'coupon_value',
		'rides_left',
		'price',
		'coupon_type',
		'status',
		'deleted',
		'blocked',
		'expires_at'
	];

	public function coupon()
		{
			return $this->belongsTo(\App\Models\Coupon::class, 'coupon_id', 'coupon_id');
		}

	public function payer()
		{
			return $this->belongsTo(\App\Models\User::class, 'user_id', 'user_id');
		}

	public function payer_detail()
		{
			return $this->belongsTo(\App\Models\UserDetail::class, 'user_detail_id', 'user_detail_id');
		}

	public function user_type()
		{
			return $this->belongsTo(\App\Models\UserType::class, 'user_type_id', 'user_type_id');
		}

	public function payment()
		{
			return $this->belongsTo(\App\Models\Payment::class, 'payment_id', 'payment_id');
		}

	public function order()
		{
			return $this->hasMany(\App\Models\Order::class, 'coupon_user_id', 'coupon_user_id');
		}

//////////////////		Coupon Payments 	//////////////////////////////////////////
public static function admin_coupon_payments($data)
	{

		$payments = CouponUser::select("*",
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz"),
			DB::RAW("( date_format(CONVERT_TZ(expires_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as expires_atz")
		);

		$payments->with([

			'payment',

			'coupon',

			'payer',
			
			'payer_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			}

		]);

		return $payments->get();

	}

///////////////////		Coupon Pay Sum 		///////////////////////////////////////////////
public static function admin_coupon_pay_sums($data)
	{

		$sum = Admin::select("*",
			DB::RAW("(SELECT COALESCE(SUM(final_charge),0) FROM payments WHERE order_id = 0 AND seller_user_type_id=".$data['seller_user_type_id']." AND updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."') as total_sum")
			);

			return $sum->first();

	}

///////////////////////

}
