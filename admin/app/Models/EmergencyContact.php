<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 05 Aug 2018 07:11:28 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

use DB;
/**
 * Class EmergencyContact
 * 
 * @property int $emergency_contact_id
 * @property int $sort_order
 * @property int $phone_number
 * @property string $blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $emergency_contact_details
 *
 * @package App\Models
 */
class EmergencyContact extends Eloquent
{
	protected $primaryKey = 'emergency_contact_id';

	protected $casts = [
		'sort_order' => 'int',
		'phone_number' => 'int'
	];

	protected $fillable = [
		'sort_order',
		'phone_number',
		'blocked'
	];

	public function emergency_contact_details()
	{
		return $this->hasMany(\App\Models\EmergencyContactDetail::class, 'emergency_contact_id');
	}

/////////////////////////////		Admin Get Contacts 		//////////////////////////////////
public static function admin_get_lists($data)
	{

		$econtacts = EmergencyContact::select('*',
			DB::RAW("( date_format(CONVERT_TZ(updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
		);

		$econtacts->with([

			'emergency_contact_details' => function($q1) {
				$q1->orderBy('language_id','ASC');
			}

		]);

		return $econtacts->get();

	}

////////////////////////		Single Contact Details 		//////////////////////////////////
public static function admin_single_details($data)
	{

		$econtact = EmergencyContact::select('*');

		$econtact->where('emergency_contact_id', $data['emergency_contact_id']);

		$econtact->with([

			'emergency_contact_details' => function($q1) {
				$q1->orderBy('language_id','ASC');
			}

		]);

		return $econtact->first();

	}

////////////////////////////////		Add New Record 		//////////////////////////////////
public static function add_new_record($data)
	{

		$econtact = new EmergencyContact();

		$econtact->sort_order = $data['ranking'];
		$econtact->phone_number = $data['phone_number'];

		$econtact->save();

		return $econtact;

	}

//////////////////////////

}
