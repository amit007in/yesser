<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Aug 2018 09:00:55 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;

/**
 * Class FloorMargin
 * 
 * @property int $floormargin_id
 * @property int $floor_name
 * @property int $price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $status
 *
 * @package App\Models
 */
class FloorMargin extends Eloquent
{
	protected $primaryKey = 'floormargin_id';
	protected $fillable = [
		'floor_name',
		'price',
		'status', 
		'all_floor_price_status' 
	];

	/**
	* @role Get All Floor Margin
	* @Method POST
	* @author Raghav
	* @date 05-Aug-2019
	*/
    public static function admin_get_lists($data)
    {
        $FloorMargin = FloorMargin::select('*',
            DB::RAW("( date_format(CONVERT_TZ(updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
        );
        return $FloorMargin->get();
    }

	/**
	* @role Add Floor Margin
	* @Method POST
	* @author Raghav Goyal
	*/
	public static function add_new_record($data)
	{
		$FloorMargin             = new FloorMargin();
		$FloorMargin->floor_name = $data['floor_name'];
		$FloorMargin->status     = 0;
		$FloorMargin->price      = $data['price'];
		$FloorMargin->all_floor_price_status      = '1';
		$FloorMargin->save();
		return $FloorMargin;
	}

	/**
	 * @role Get Floor Details
	 * @Method Get
	 * @author Raghav Goyal
	 */
	public static function single_floor_details($data)
	{
		return FloorMargin::where('floormargin_id', $data['floormargin_id'])
		->select("*")
		->first();
	}

	/**
	 * @role Get Floor Details
	 * @Method Get
	 * @author Raghav Goyal
	 */
	public static function floor_details($data)
	{
		return FloorMargin::where('floor_name', $data['floor_name'])->where('floormargin_id','!=',$data['floor_id'])
		->select("*")
		->first();
	}

	/////////////////////////////		Update Coupon 	///////////////////////////////////////////////
	public static function update_record($data)
	{
		return Coupon::where('coupon_id', $data['coupon_id'])->limit(1)->update([
			'code' => $data['code'],
			'amount_value' => $data['amount_value'],
			'rides_value' => $data['rides_value'],
			'price' => $data['price'],
			'expiry_days' => $data['expiry_days'],
			'expires_at' => $data['expires_at'],
			'coupon_type' => $data['coupon_type'],
			'updated_at' => new \DateTime
		]);
	}

	/**
	 * @role Admin Notification list using Filter
	 */
	public static function admin_notification_listings($data)
	{
		if(isset($data['type_check'])){
			$custs = Notification::whereBetween('notifications.broadcastDate', [$data['starting_dt'], $data['ending_dt']]);
		}else{
			$custs = new Notification;
		}
		//$custs->whereBetween('notifications.broadcastDate', [$data['starting_dt'], $data['ending_dt']]);
		///////////////		Filters 	////////////////////////
		if(isset($data['blocked_check']) && $data['blocked_check'] == 'Active' )
			$custs->where('notifications.status','Active');
		elseif(isset($data['blocked_check']) && $data['blocked_check'] == 'In Active' )
			$custs->where('notifications.status','In Active');

		if(isset($data['type_check']) && $data['type_check'] == 'Customer' )
			$custs->where('notifications.type','Customer');
		elseif(isset($data['type_check']) && $data['type_check'] == 'Driver' )
			$custs->where('notifications.type','Driver');

		///////////////		Filters 	////////////////////////
		$custs->select('*');
		$custs->orderBy('notification_id', 'DESC');
		if(isset($data['not_paginate']) && $data['not_paginate'] == 1)
			return $custs->get();
		else
			return $custs->paginate();
	}

}
