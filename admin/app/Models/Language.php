<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 29 Jul 2018 07:48:47 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

use DB;
/**
 * Class Language
 * 
 * @property int $language_id
 * @property string $language_name
 * @property string $language_code
 * @property string $display_for
 * @property int $sort_order
 * @property string $blocked
 * @property string $is_default
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $user_details
 *
 * @package App\Models
 */
class Language extends Eloquent
{
	protected $primaryKey = 'language_id';

	protected $casts = [
		'sort_order' => 'int'
	];

	protected $fillable = [
		'language_name',
		'language_code',
		'display_for',
		'sort_order',
		'blocked',
		'is_default'
	];

	public function admins()
		{
			return $this->hasMany(\App\TempModels\Admin::class, 'language_id', 'language_id');
		}

	public function category_brand_details()
		{
			return $this->hasMany(\App\Models\CategoryBrandDetail::class, 'language_id', 'language_id');
		}

	public function category_brand_product_details()
		{
			return $this->hasMany(\App\Models\CategoryBrandProductDetail::class, 'language_id', 'language_id');
		}

	public function category_details()
		{
			return $this->hasMany(\App\Models\CategoryDetail::class, 'language_id', 'language_id');
		}

	public function user_details()
		{
			return $this->hasMany(\App\Models\UserDetail::class, 'language_id', 'language_id');
		}

	public function organisations()
		{
			return $this->hasMany(\App\TempModels\Organisation::class, 'language_id', 'language_id');
		}

	public function user_details()
		{
			return $this->hasMany(\App\Models\UserDetail::class, 'language_id', 'language_id');
		}

	public function emergeny_contact_details()
		{
			return $this->hasMany(\App\Models\EmergenyContactDetail::class, 'language_id', 'language_id');
		}

////////////////////////////			All languages 			////////////////////////////////
public static function admin_get_all_languages($data)
	{
		return Language::
		select('*',
			DB::RAW("( date_format(CONVERT_TZ(updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
		)
		->get();
	}

/////////////////////////////

}
