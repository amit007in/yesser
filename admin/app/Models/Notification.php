<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Aug 2018 09:00:55 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;

/**
 * Class Coupon
 * 
 * @property int $notification_id
 * @property string $title
 * @property string $message
 * @property string $type
 * @property string $broadcastDate
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $status
 * @property string $recurring
 *
 * @package App\Models
 */
class Notification extends Eloquent
{
	protected $primaryKey = 'notification_id';
	protected $fillable = [
		'title',
		'message',
		'image',
                'videothumb',
                'video',
		'type',
		'broadcastDate',
		'status',
		'recurring',
                'media_type',
                'service_id'
	];

	////////////////////////////		Admin Get Contacts 		//////////////////////////////////
    public static function admin_get_lists($data)
    {

        $notification = Notification::select('*',
            DB::RAW("( date_format(CONVERT_TZ(updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
        );

        return $notification->get();

    }


//////////////////////////////			Admin Coupons All 	///////////////////////////////////
public static function admin_coupons($data)
	{
	
		$coupons = Coupon::whereBetween('created_at', [$data['starting_dt'], $data['ending_dt']]);

		$coupons->select("*",
			DB::RAW("(SELECT COUNT(*) FROM coupon_users as cu WHERE cu.coupon_id=coupons.coupon_id) as purchase_counts"),
			DB::RAW("( date_format(CONVERT_TZ(expires_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as expires_atz"),
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")
		);

		return $coupons->get();

	}

	/**
	* @role Add Notification
	* @Method POST
	* @author Raghav Goyal
	*/
	public static function add_new_record($data)
	{
		$Notification                = new Notification();
		$Notification->title         = $data['title'];
		$Notification->message       = $data['message'];
		$Notification->image         = $data['profile_pic_name'];
		$Notification->type          = $data['type'];
		$Notification->broadcastDate = $data['date'];
		$Notification->status        = $data['status'];
		$Notification->recurring     = $data['recurring'];
                
                $Notification->media_type    = $data['media_type'];
                $Notification->video         = $data['video_url'];
                $Notification->videothumb    = $data['video_thumbimage'];
                
                $Notification->coupon_id     = isset($data['coupon_id']) ? (int) $data['coupon_id'] : 0;
                $Notification->service_id    = isset($data['service_id']) ? $data['service_id'] : 0;
		$Notification->created_at    = new \DateTime;
		$Notification->updated_at    = new \DateTime;
		$Notification->save();
		return $Notification;
	}

	/**
	 * @role Get Notification Details
	 * @Method Get
	 * @author Raghav Goyal
	 */
	public static function single_notification_details($data)
	{
		return Notification::where('notification_id', $data['notification_id'])
		->select("*")
		->first();
	}

/////////////////////////////		Update Coupon 	///////////////////////////////////////////////
public static function update_record($data)
{
	return Coupon::where('coupon_id', $data['coupon_id'])->limit(1)->update([
		'code' => $data['code'],
		'amount_value' => $data['amount_value'],
		'rides_value' => $data['rides_value'],
		'price' => $data['price'],
		'expiry_days' => $data['expiry_days'],
		'expires_at' => $data['expires_at'],
		'coupon_type' => $data['coupon_type'],
		'updated_at' => new \DateTime
	]);
}

	/**
	 * @role Admin Notification list using Filter
	 */
	public static function admin_notification_listings($data)
	{
		$custs = Notification::whereBetween('notifications.broadcastDate', [$data['starting_dt'], $data['ending_dt']]);
		//$custs->whereBetween('notifications.broadcastDate', [$data['starting_dt'], $data['ending_dt']]);
		///////////////		Filters 	////////////////////////
		if(isset($data['blocked_check']) && $data['blocked_check'] == 'Active' )
			$custs->where('notifications.status','Active');
		elseif(isset($data['blocked_check']) && $data['blocked_check'] == 'In Active' )
			$custs->where('notifications.status','In Active');

		if(isset($data['type_check']) && $data['type_check'] == 'Customer' )
			$custs->where('notifications.type','Customer');
		elseif(isset($data['type_check']) && $data['type_check'] == 'Driver' )
			$custs->where('notifications.type','Driver');

		///////////////		Filters 	////////////////////////
		$custs->select('*');
		$custs->orderBy('notification_id', 'DESC');
		if(isset($data['not_paginate']) && $data['not_paginate'] == 1)
			return $custs->get();
		else
			return $custs->paginate();
	}

}
