<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 Aug 2018 17:37:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;
/**
 * Class Order
 * 
 * @property int $order_id
 * @property string $order_token
 * @property int $customer_user_id
 * @property int $customer_user_detail_id
 * @property int $driver_user_id
 * @property int $driver_user_detail_id
 * @property int $category_id
 * @property int $category_brand_id
 * @property int $category_brand_product_id
 * @property int $card_id
 * @property int $customer_organisation_id
 * @property int $driver_organisation_id
 * @property int $customer_user_type_id
 * @property int $driver_user_type_id
 * @property int $continuous_order_id
 * @property string $order_status
 * @property string $payment_status
 * @property string $refund_status
 * @property string $pickup_address
 * @property float $pickup_latitude
 * @property float $pickup_longitude
 * @property string $dropoff_address
 * @property float $dropoff_latitude
 * @property float $dropoff_longitude
 * @property string $category_type
 * @property string $order_type
 * @property string $created_by
 * @property \Carbon\Carbon $order_timings
 * @property \Carbon\Carbon $continouos_startdt
 * @property \Carbon\Carbon $continuous_enddt
 * @property \Carbon\Carbon $continuous_time
 * @property string $continuous
 * @property string $transaction_id
 * @property string $payment_type
 * @property string $cancel_reason
 * @property string $refund_id
 * @property string $cancelled_by
 * @property string $order_weight
 * @property string $order_distance
 * @property string $initial_amount
 * @property string $admin_charge
 * @property string $final_charge
 * @property string $bank_charge
 * @property string $bottle_returned
 * @property string $bottle_price
 * @property int $product_quantity
 * @property string $product_actual_value
 * @property string $product_alpha_price
 * @property string $product_price_per_quantity
 * @property string $product_price_per_distance
 * @property string $promotion_code
 * @property int $organisation_coupon_user_id
 * @property int $coupon_user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 * @property \App\Models\Organisation $organisation
 * @property \App\Models\UserType $user_type
 * @property \App\Models\OrganisationCouponUser $organisation_coupon_user
 * @property \App\Models\UserDetail $user_detail
 * @property \App\Models\Category $category
 * @property \App\Models\CategoryBrand $category_brand
 * @property \App\Models\CategoryBrandProduct $category_brand_product
 * @property \App\Models\UserCard $user_card
 * @property \Illuminate\Database\Eloquent\Collection $order_ratings
 * @property \Illuminate\Database\Eloquent\Collection $order_requests
 *
 * @package App\Models
 */
class Order extends Eloquent
{
	protected $primaryKey = 'order_id';

	protected $casts = [
		'customer_user_id' => 'int',
		'customer_user_detail_id' => 'int',
		'driver_user_id' => 'int',
		'driver_user_detail_id' => 'int',
		'category_id' => 'int',
		'category_brand_id' => 'int',
		'category_brand_product_id' => 'int',
		'card_id' => 'int',
		'customer_organisation_id' => 'int',
		'driver_organisation_id' => 'int',
		'customer_user_type_id' => 'int',
		'driver_user_type_id' => 'int',
		'continuous_order_id' => 'int',
		'pickup_latitude' => 'float',
		'pickup_longitude' => 'float',
		'dropoff_latitude' => 'float',
		'dropoff_longitude' => 'float',
		'product_quantity' => 'int',
		'organisation_coupon_user_id' => 'int',
		'coupon_user_id' => 'int',
		'reward_discount_point' => 'int'
	];

	protected $dates = [
		'order_timings',
		'continouos_startdt',
		'continuous_enddt',
		'continuous_time'
	];

	protected $hidden = [
		'order_token'
	];

	protected $fillable = [
		'order_token',
		'customer_user_id',
		'customer_user_detail_id',
		'driver_user_id',
		'driver_user_detail_id',
		'category_id',
		'category_brand_id',
		'category_brand_product_id',
		'card_id',
		'customer_organisation_id',
		'driver_organisation_id',
		'customer_user_type_id',
		'driver_user_type_id',
		'continuous_order_id',
		'order_status',
		'payment_status',
		'refund_status',
		'pickup_address',
		'pickup_latitude',
		'pickup_longitude',
		'dropoff_address',
		'dropoff_latitude',
		'dropoff_longitude',
		'category_type',
		'order_type',
		'created_by',
		'order_timings',
		'continouos_startdt',
		'continuous_enddt',
		'continuous_time',
		'continuous',
		'transaction_id',
		'payment_type',
		'cancel_reason',
		'refund_id',
		'cancelled_by',
		'order_weight',
		'order_distance',
		'initial_amount',
		'admin_charge',
		'final_charge',
		'bank_charge',
		'bottle_returned',
		'bottle_price',
		'product_quantity',
		'product_actual_value',
		'product_alpha_price',
		'product_price_per_quantity',
		'product_price_per_distance',
		'promotion_code',
		'organisation_coupon_user_id',
		'coupon_user_id',
		'reward_discount_price',
		'floor_charge',
		'reward_discount_point'
	];

		// 'card_id',
		// 'continuous_order_id',

//////////////	Customer 	///////////////////
	public function customer_user()
		{
			return $this->belongsTo(\App\Models\User::class, 'customer_user_id');
		}

	public function customer_user_detail()
		{
			return $this->belongsTo(\App\Models\UserDetail::class, 'customer_user_detail_id');
		}

	public function customer_organisation()
		{
			return $this->belongsTo(\App\Models\Organisation::class, 'customer_organisation_id');
		}

	public function customer_user_type()
		{
			return $this->belongsTo(\App\Models\UserType::class, 'customer_user_type_id');
		}
//////////////	Customer 	///////////////////

//////////////	Driver 	///////////////////////
	public function driver_user()
		{
			return $this->belongsTo(\App\Models\User::class, 'driver_user_id');
		}

	public function driver_user_detail()
		{
			return $this->belongsTo(\App\Models\UserDetail::class, 'driver_user_detail_id');
		}

	public function driver_organisation()
		{
			return $this->belongsTo(\App\Models\Organisation::class, 'driver_organisation_id');
		}

	public function driver_user_type()
		{
			return $this->belongsTo(\App\Models\UserType::class, 'driver_user_type_id');
		}
//////////////	Driver 	///////////////////////

	public function category()
		{
			return $this->belongsTo(\App\Models\Category::class, 'category_id', 'category_id');
		}

	public function category_brand()
		{
			return $this->belongsTo(\App\Models\CategoryBrand::class, 'category_brand_id', 'category_brand_id');
		}

	public function category_brand_product()
		{
			return $this->belongsTo(\App\Models\CategoryBrandProduct::class, 'category_brand_product_id', 'category_brand_product_id');
		}

	public function user_card()
		{
			return $this->belongsTo(\App\Models\UserCard::class, 'card_id');
		}

	public function order()
		{
			return $this->belongsTo(\App\Models\Order::class, 'continuous_order_id');
		}

	public function organisation_coupon_user()
		{
			return $this->belongsTo(\App\Models\OrganisationCouponUser::class, 'organisation_coupon_user_id', 'organisation_coupon_user_id');
		}

	public function coupon_user()
		{
			return $this->belongsTo(\App\Models\CouponUser::class, 'coupon_user_id', 'coupon_user_id');
		}
		
		public function order_products()
		{
			return $this->hasMany(\App\Models\OrderProduct::class, 'order_id', 'order_id');
		}

///////////		Ratings 	////////////////////////
	public function customer_rating()
		{
			return $this->hasOne(\App\Models\OrderRating::class, 'order_id', 'order_id')->where('created_by', 'Customer');
		}
	public function driver_rating()
		{
			return $this->hasOne(\App\Models\OrderRating::class, 'order_id', 'order_id')->where('created_by', 'Driver');
		}
 ///////////		Ratings 	////////////////////////

	public function order_request()
		{
			return $this->hasOne(\App\Models\OrderRequest::class, 'order_id', 'order_id');
		}
	public function order_requests()
		{
			return $this->hasMany(\App\Models\OrderRequest::class, 'order_id', 'order_id');
		}

	public function CRequest()
		{
			return $this->hasOne(\App\Models\OrderRequest::class, 'order_id', 'order_id')->whereIn('order_request_status', ['Accepted', 'Confirmed'] );
		}

	public function payment()
		{
			return $this->hasOne(\App\Models\Payment::class, 'order_id', 'order_id');
		}

///////////////////			Driver All Orders 	//////////////////////////
public static function driver_all_orders($data)
	{

		$orders = Order::where('driver_user_detail_id', $data['user_detail_id']);
		$orders->whereBetween('order_timings', [$data['starting_dt'], $data['ending_dt']]);

		$orders->select("*",
			DB::RAW("( date_format(CONVERT_TZ(order_timings,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as order_timingsz"),
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
			DB::RAW("( date_format(CONVERT_TZ(orders.updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
		);

		if(isset($data['order_status_array']))
			$orders->whereIn('order_status',$data['order_status_array']);

		$orders->with([

			'payment',

			'customer_user_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			'customer_user',

			'category_brand_product' => function($q2) use ($data) {

				$q2->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);

			}

		]);

		return $orders->get();

	}
/////////////////////		Service Panel Single Order Details 	/////////////////////////////////////
public static function service_driver_single_ride_details($data)
	{

		$order = Order::where('order_id',$data['order_id']);

		$order->select("*",
			DB::RAW("( date_format(CONVERT_TZ(order_timings,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as order_timingsz"),
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
			DB::RAW("( date_format(CONVERT_TZ(orders.updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
		);

		$order->with([

			'customer_user_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			'customer_user',

			'order_request' => function($q3) use ($data) {

				$q3->where('driver_user_detail_id', $data['user_detail_id']);

			},

/////////////////		Category 	/////////////////////////////////
			'category_brand_product' => function($q4) use ($data) {

				$q4->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);
			},
			'category_brand' => function($q5) use ($data) {

				$q5->join('category_brand_details', 'category_brand_details.category_brand_id', 'category_brands.category_brand_id')->where('category_brand_details.language_id', $data['admin_language_id']);
			},
			'category' => function($q6) use ($data) {

				$q6->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);
			},
/////////////////		Category 	/////////////////////////////////


/////////////////		Ratings 	/////////////////////////////////
			'customer_rating' => function($q7) use ($data) {

				$q7->select('*',
					DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")
				);

			},
			'driver_rating' => function($q8) use ($data) {

				$q8->select('*',
					DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")
				);

			}
/////////////////		Ratings 	/////////////////////////////////

		]);

		return $order->first();

	}

////////////////////////		All Orders 		//////////////////////////////////
public static function org_all_orders($data)
	{

		$orders = Order::whereBetween('order_timings', [$data['starting_dt'], $data['ending_dt']]);

		$orders->where('driver_organisation_id', $data['organisation_id']);

		$orders->select("*",
			DB::RAW("( date_format(CONVERT_TZ(order_timings,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as order_timingsz"),
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
			DB::RAW("( date_format(CONVERT_TZ(orders.updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
		);

		$orders->with([

			'customer_user_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			'customer_user',

			'category_brand_product' => function($q2) use ($data) {

				$q2->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);

			},

			'category_brand' => function($q3) use ($data) {

				$q3->join('category_brand_details', 'category_brand_details.category_brand_id', 'category_brands.category_brand_id')->where('category_brand_details.language_id', $data['admin_language_id']);

			},

			'driver_user_detail' => function($q4) use ($data) {

				$q4->select('*',
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			'driver_user' => function($q5) use ($data) {

				$q5->select("*");

			}

		]);

		return $orders->get();

	}

//////////////////////		Customer All Orders 	//////////////////////////////////////
public static function cust_orders($data)
	{

		$orders = Order::whereBetween('order_timings', [$data['starting_dt'], $data['ending_dt']]);

		$orders->where('customer_user_detail_id', $data['user_detail_id']);

		$orders->select("*",
			DB::RAW("( date_format(CONVERT_TZ(order_timings,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as order_timingsz"),
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
			DB::RAW("( date_format(CONVERT_TZ(orders.updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
		);

		$orders->with([

			'category_brand_product' => function($q2) use ($data) {

				$q2->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);

			},

			'category_brand' => function($q3) use ($data) {

				$q3->join('category_brand_details', 'category_brand_details.category_brand_id', 'category_brands.category_brand_id')->where('category_brand_details.language_id', $data['admin_language_id']);

			},

			'driver_user_detail' => function($q4) use ($data) {

				$q4->select('*',
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			'driver_user' => function($q5) use ($data) {

				$q5->select("*");

			}

		]);

		return $orders->get();

	}

///////////////////////		Customer Order Details 	///////////////////////////////
public static function cust_order_details($data)
	{

		$order = Order::where('order_id',$data['order_id']);

		$order->select("*",
			DB::RAW("( date_format(CONVERT_TZ(order_timings,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as order_timingsz"),
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
			DB::RAW("( date_format(CONVERT_TZ(orders.updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
		);

		$order->with([

///////////////////		Order Requests 		///////////////////////////////
		// 'order_requests' => function($q1) use ($data) {

		// 	$q1->with([

		// 		'driver_user_detail' => function($q11) use ($data) {

		// 			$q11->select("*",
		// 				DB::RAW("(CASE WHEN profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
		// 			);

		// 		},

		// 		'driver_user'

		// 	]);

		// },
///////////////////		Order Requests 		///////////////////////////////

/////////////////		Category 	/////////////////////////////////
		'category_brand_product' => function($q4) use ($data) {

			$q4->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);
		},
		'category_brand' => function($q5) use ($data) {

			$q5->join('category_brand_details', 'category_brand_details.category_brand_id', 'category_brands.category_brand_id')->where('category_brand_details.language_id', $data['admin_language_id']);
		},
		'category' => function($q6) use ($data) {

			$q6->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);
		},
/////////////////		Category 	/////////////////////////////////


/////////////////		Ratings 	/////////////////////////////////
		'customer_rating' => function($q7) use ($data) {

			$q7->select('*',
				DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")
			);

		},
		'driver_rating' => function($q8) use ($data) {

			$q8->select('*',
				DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")
			);

		},
/////////////////		Ratings 	/////////////////////////////////

		'organisation_coupon_user' => function($q9) use ($data) {

			$q9->select("*",
	DB::RAW("(SELECT SUM(P1.final_charge) FROM payments P1 WHERE P1.organisation_coupon_user_id=organisation_coupon_users.organisation_coupon_user_id AND P1.payment_status='Completed' AND P1.refund_status='NoNeed') as successPaymentsAmount"),
	DB::RAW("(SELECT SUM(P2.final_charge) FROM payments P2 WHERE P2.organisation_coupon_user_id=organisation_coupon_users.organisation_coupon_user_id AND P2.payment_status='Pending' AND P2.refund_status='NoNeed') as pendingPaymentsAmount"),
	DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")
			);

		},

		'CRequest',

/////////////////		Driver 	//////////////////////////////////
		'driver_user_detail' => function($q4) use ($data) {

				$q4->select('*',
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

		},

		'driver_user' => function($q5) use ($data) {

				$q5->select("*");

		}
/////////////////		Driver 	//////////////////////////////////

		]);

		return $order->first();

	}

///////////////////////////		Orders All 		/////////////////////////////////////
public static function admin_orders_all($data)
	{

		$orders = Order::whereBetween('order_timings', [$data['starting_dt'], $data['ending_dt']]);

		$orders->select("*",
			DB::RAW("( date_format(CONVERT_TZ(order_timings,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as order_timingsz"),
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")
		);

		$orders->with([

			'payment',

/////////////////		Category 	/////////////////////////////////
			'category_brand_product' => function($q4) use ($data) {

				$q4->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);
			},
/////////////////		Category 	/////////////////////////////////

/////////////////		Driver 	//////////////////////////////////
			'driver_user_detail' => function($q4) use ($data) {

					$q4->select('*',
						DB::RAW("(CASE WHEN profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
					);

			},

			'driver_user' => function($q5) use ($data) {

					$q5->select("*");

			},
/////////////////		Driver 	//////////////////////////////////

/////////////////		Customer /////////////////////////////////
			'customer_user_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			'customer_user',
/////////////////		Customer 	//////////////////////////////

		]);

		return $orders->get();

	}

////////////////////		Admin Order Details 		//////////////////////////////
public static function order_details($data)
	{

		$order = Order::where('order_id',$data['order_id']);

		$order->select("*",
			DB::RAW("( date_format(CONVERT_TZ(order_timings,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as order_timingsz"),
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
			DB::RAW("( date_format(CONVERT_TZ(orders.updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
		);

		$order->with([

			'payment',

/////////////////		Category 	//////////////////////////////
		'category_brand_product' => function($q4) use ($data) {

			$q4->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);
		},
		'category_brand' => function($q5) use ($data) {

			$q5->join('category_brand_details', 'category_brand_details.category_brand_id', 'category_brands.category_brand_id')->where('category_brand_details.language_id', $data['admin_language_id']);
		},
		'category' => function($q6) use ($data) {

			$q6->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);
		},
/////////////////		Category 	//////////////////////////////


/////////////////		Ratings 	//////////////////////////////
		'customer_rating' => function($q7) use ($data) {

			$q7->select('*',
				DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")
			);

		},
		'driver_rating' => function($q8) use ($data) {

			$q8->select('*',
				DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")
			);

		},
/////////////////		Ratings 	//////////////////////////////

		'organisation_coupon_user' => function($q9) use ($data) {

			$q9->select("*",
	DB::RAW("(SELECT COALESCE(SUM(P1.final_charge),0) FROM payments P1 WHERE P1.organisation_coupon_user_id=organisation_coupon_users.organisation_coupon_user_id AND P1.payment_status='Completed' AND P1.refund_status='NoNeed') as successPaymentsAmount"),
	DB::RAW("(SELECT COALESCE(SUM(P2.final_charge),0) FROM payments P2 WHERE P2.organisation_coupon_user_id=organisation_coupon_users.organisation_coupon_user_id AND P2.payment_status='Pending' AND P2.refund_status='NoNeed') as pendingPaymentsAmount"),
	DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")
			);

		},

		'coupon_user' => function($q10) use ($data) {

			$q10->join('coupons', 'coupons.coupon_id', 'coupon_users.coupon_id');
			$q10->select("*", 'coupon_users.coupon_type', 'coupon_users.coupon_value',
				DB::RAW("( date_format(CONVERT_TZ(coupon_users.created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
				DB::RAW("( date_format(CONVERT_TZ(coupon_users.expires_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as expires_atz")
			);

		},

		'CRequest',

/////////////////		Driver 	//////////////////////////////////
		'driver_user_detail' => function($q4) use ($data) {

					$q4->select('*',
						DB::RAW("(CASE WHEN profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
					);

			},

		'driver_user' => function($q5) use ($data) {

					$q5->select("*");

			},
/////////////////		Driver 	//////////////////////////////////

/////////////////		Customer /////////////////////////////////
		'customer_user_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

		'customer_user',
/////////////////		Customer 	//////////////////////////////

		]);

		return $order->first();

	}

/////////////////////		Admin Orders Listings	////////////////////////////////
public static function admin_orders_list($data)
	{
		$orders = Order::select("*",
			DB::RAW("( date_format(CONVERT_TZ(order_timings,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as order_timingsz"),
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
			DB::RAW("( date_format(CONVERT_TZ(orders.updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
	);

		$orders->with([
			'payment',

/////////////////		Category 	//////////////////////////////
		'category_brand_product' => function($q4) use ($data) {

			$q4->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);
		},
		'category_brand' => function($q5) use ($data) {

			$q5->join('category_brand_details', 'category_brand_details.category_brand_id', 'category_brands.category_brand_id')->where('category_brand_details.language_id', $data['admin_language_id']);
		},
		'category' => function($q6) use ($data) {

			$q6->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);
		},
/////////////////		Category 	//////////////////////////////

		'CRequest',

/////////////////		Customer /////////////////////////////////
		'customer_user_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

		'customer_user',
/////////////////		Customer 	//////////////////////////////

/////////////////		Driver 	//////////////////////////////////
		'driver_user_detail' => function($q4) use ($data) {

					$q4->select('*',
						DB::RAW("(CASE WHEN profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
					);

			},

		'driver_user' => function($q5) use ($data) {

					$q5->select("*");

			},
		'order_products' => function($q8) use ($data) {
			$q8->select('*');
		}
/////////////////		Driver 	//////////////////////////////////

		]);

		$orders->whereIn('order_type', $data['order_type_ids']);
		$orders->whereBetween('order_timings', [$data['starting_dt'], $data['ending_dt']]);

		if(isset($data['category_id']))
			$orders->where('category_id', $data['category_id']);

		if(isset($data['driver_user_detail_id']))
			{
				$orders->where('driver_user_detail_id',$data['driver_user_detail_id']);

			}

		return $orders->get();

	}

/////////////////////		Latest 10 Orders 		/////////////////////////////////////////
public static function admin_latest_orders($data)
{

	$orders = Order::select("*",
			DB::RAW("( date_format(CONVERT_TZ(order_timings,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as order_timingsz"),
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
			DB::RAW("( date_format(CONVERT_TZ(orders.updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
		);

		$orders->with([
			'payment',

/////////////////		Category 	//////////////////////////////
		'category' => function($q6) use ($data) {

			$q6->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);
		},
/////////////////		Category 	//////////////////////////////

/////////////////		Customer /////////////////////////////////
		'customer_user_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

		'customer_user',
		'order_products' => function($q8) use ($data) {
			$q8->select('*');
		}
/////////////////		Customer 	//////////////////////////////
		]);

	$orders->orderBy("created_at","DESC");

	//$orders->skip(0)->take(10);

	//return $orders->get();
	return $orders->paginate();

}

//////////////////////////		Admin Order Listings 		///////////////////////////////////////////
public static function admin_orders_listings($data)
{

	$orders = Order::select("*");

///////////////		Filters 	////////////////////////
	$orders->whereBetween('orders.order_timings', [$data['starting_dt'], $data['ending_dt']]);

	if($data['status_check'] == 'Searching' )
		$orders->whereIn('order_status',['Searching']);
	elseif($data['status_check'] == 'Confirmed')
		$orders->whereIn('order_status',['Confirmed']);
	elseif($data['status_check'] == 'Scheduled')
		$orders->whereIn('order_status',['Scheduled', 'DApproved']);
	elseif($data['status_check'] == 'Ongoing')
		$orders->whereIn('order_status',['Ongoing']);
	elseif($data['status_check'] == 'Rejected')
		$orders->whereIn('order_status',['SerReject', 'SupReject']);
	elseif($data['status_check'] == 'Timeout')
		$orders->whereIn('order_status',['SerTimeout', 'SupTimeout']);
	elseif($data['status_check'] == 'Customer Cancelled')
		$orders->whereIn('order_status',['CustCancel']);
	elseif($data['status_check'] == 'Driver Cancelled')
		$orders->whereIn('order_status',['DriverCancel', 'DSchCancelled']);
	elseif($data['status_check'] == 'Completed')
		$orders->whereIn('order_status',['SerComplete', 'SupComplete']);
	elseif($data['status_check'] == 'Confirmation Pending')
		$orders->whereIn('order_status',['DPending']);
	elseif($data['status_check'] == 'Driver not found')
		$orders->whereIn('order_status',['Driver not found']);
	// if($data['order_type_check'] == "Service")
	// 	$orders->where('order_type','Service');
	// elseif($data['order_type_check'] == "Support")
	// 	$orders->where('order_type','Support');
	if(!in_array(0, $data['categories']))
		$orders->whereIn('category_id', $data['categories']);

	if(isset($data['search']) && $data['search'] != '')
		$orders->whereRaw("( (pickup_address LIKE '%".$data['search']."%') || (dropoff_address LIKE '%".$data['search']."%') || (name LIKE '%".$data['search']."%') || (phone_number LIKE '%".$data['search']."%') )");
///////////////		Filters 	////////////////////////

	$orders->join('users','users.user_id','orders.customer_user_id');

	$orders->select("*",
		DB::RAW("( date_format(CONVERT_TZ(order_timings,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as order_timingsz"),
		DB::RAW("( date_format(CONVERT_TZ(orders.created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
		DB::RAW("( date_format(CONVERT_TZ(orders.updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
	);

	$orders->with([

		'payment',

/////////////////		Category 	//////////////////////////////
		'category_brand_product' => function($q4) use ($data) {

			$q4->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);
		},
		'category_brand' => function($q5) use ($data) {

			$q5->join('category_brand_details', 'category_brand_details.category_brand_id', 'category_brands.category_brand_id')->where('category_brand_details.language_id', $data['admin_language_id']);
		},
		'category' => function($q6) use ($data) {

			$q6->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);
		},
/////////////////		Category 	//////////////////////////////

/////////////////		Customer /////////////////////////////////
		'customer_user_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},
/////////////////		Customer 	//////////////////////////////

/////////////////		Driver 	//////////////////////////////////
		'driver_user_detail' => function($q4) use ($data) {

			$q4->join('users', 'users.user_id','user_details.user_id');

			$q4->select('*',
				DB::RAW("(CASE WHEN profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
			);

		},
		'order_products' => function($q8) use ($data) {
			$q8->select('*');
		}
/////////////////		Driver 	//////////////////////////////////

		]);

	if($data['ordering_filter'] == '0')
		$orders->orderBy('orders.created_at', 'ASC');
	elseif($data['ordering_filter'] == '1')
		$orders->orderBy('orders.created_at', 'DESC');
	elseif($data['ordering_filter'] == '2')
		$orders->orderBy('orders.order_timings','ASC');
	else
		$orders->orderBy('orders.order_timings','DESC');

	return $orders->paginate();

}


///////////////////////		Order Listings For Export 	///////////////////////////////////////////////
public static function orders_listings_4export($data)
{

	$orders = Order::select("*");

///////////////		Filters 	////////////////////////
	$orders->whereBetween('orders.order_timings', [$data['starting_dt'], $data['ending_dt']]);

	if($data['status_check'] == 'Searching' )
		$orders->whereIn('order_status',['Searching']);
	elseif($data['status_check'] == 'Confirmed')
		$orders->whereIn('order_status',['Confirmed']);
	elseif($data['status_check'] == 'Scheduled')
		$orders->whereIn('order_status',['Scheduled', 'DApproved']);
	elseif($data['status_check'] == 'Ongoing')
		$orders->whereIn('order_status',['Ongoing']);
	elseif($data['status_check'] == 'Rejected')
		$orders->whereIn('order_status',['SerReject', 'SupReject']);
	elseif($data['status_check'] == 'Timeout')
		$orders->whereIn('order_status',['SerTimeout', 'SupTimeout']);
	elseif($data['status_check'] == 'Customer Cancelled')
		$orders->whereIn('order_status',['CustCancel']);
	elseif($data['status_check'] == 'Driver Cancelled')
		$orders->whereIn('order_status',['DriverCancel', 'DSchCancelled']);
	elseif($data['status_check'] == 'Completed')
		$orders->whereIn('order_status',['SerComplete', 'SupComplete']);
	elseif($data['status_check'] == 'Confirmation Pending')
		$orders->whereIn('order_status',['DPending']);

	// if($data['order_type_check'] == "Service")
	// 	$orders->where('order_type','Service');
	// elseif($data['order_type_check'] == "Support")
	// 	$orders->where('order_type','Support');

	if(!in_array(0, $data['categories']))
		$orders->whereIn('category_id', $data['categories']);

	if(isset($data['search']) && $data['search'] != '')
		$orders->whereRaw("( (pickup_address LIKE '%".$data['search']."%') || (dropoff_address LIKE '%".$data['search']."%') || (name LIKE '%".$data['search']."%') || (phone_number LIKE '%".$data['search']."%') )");
///////////////		Filters 	////////////////////////

	$orders->join('users','users.user_id','orders.customer_user_id');

	$orders->select("*",
		DB::RAW("( date_format(CONVERT_TZ(order_timings,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as order_timingsz"),
		DB::RAW("( date_format(CONVERT_TZ(orders.created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
		DB::RAW("( date_format(CONVERT_TZ(orders.updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
	);

	$orders->with([

		'payment',

/////////////////		Category 	//////////////////////////////
		'category_brand_product' => function($q4) use ($data) {

			$q4->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);
		},
		'category_brand' => function($q5) use ($data) {

			$q5->join('category_brand_details', 'category_brand_details.category_brand_id', 'category_brands.category_brand_id')->where('category_brand_details.language_id', $data['admin_language_id']);
		},
		'category' => function($q6) use ($data) {

			$q6->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);
		},
/////////////////		Category 	//////////////////////////////

/////////////////		Customer /////////////////////////////////
		'customer_user_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},
/////////////////		Customer 	//////////////////////////////


/////////////////		Driver 	//////////////////////////////////
		'driver_user_detail' => function($q4) use ($data) {

			$q4->join('users', 'users.user_id','user_details.user_id');

			$q4->select('*',
				DB::RAW("(CASE WHEN profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
			);

		},
		'order_products' => function($q8) use ($data) {
			$q8->select('*');
		}
/////////////////		Driver 	//////////////////////////////////

		]);

	if($data['ordering_filter'] == '0')
		$orders->orderBy('orders.created_at', 'ASC');
	elseif($data['ordering_filter'] == '1')
		$orders->orderBy('orders.created_at', 'DESC');
	elseif($data['ordering_filter'] == '2')
		$orders->orderBy('orders.order_timings','ASC');
	else
		$orders->orderBy('orders.order_timings','DESC');

	return $orders->get();

}


///////////////////////////		Organisation Orders Listings 		//////////////////////////////////
public static function org_orders_listings($data)
{
	$orders = Order::select("*");

	if(isset($data['driver_user_detail_id']))
		$orders->where('driver_user_detail_id', $data['driver_user_detail_id']);
//categories
///////////////		Filters 	////////////////////////
	$orders->where('driver_organisation_id', $data['organisation_id']);
	$orders->whereBetween('orders.order_timings', [$data['starting_dt'], $data['ending_dt']]);

	if($data['status_check'] == 'Searching' )
		$orders->whereIn('order_status',['Searching']);
	elseif($data['status_check'] == 'Confirmed')
		$orders->whereIn('order_status',['Confirmed']);
	elseif($data['status_check'] == 'Scheduled')
		$orders->whereIn('order_status',['Scheduled', 'DApproved']);
	elseif($data['status_check'] == 'Ongoing')
		$orders->whereIn('order_status',['Ongoing']);
	elseif($data['status_check'] == 'Rejected')
		$orders->whereIn('order_status',['SerReject', 'SupReject']);
	elseif($data['status_check'] == 'Timeout')
		$orders->whereIn('order_status',['SerTimeout', 'SupTimeout']);
	elseif($data['status_check'] == 'Customer Cancelled')
		$orders->whereIn('order_status',['CustCancel']);
	elseif($data['status_check'] == 'Driver Cancelled')
		$orders->whereIn('order_status',['DriverCancel', 'DSchCancelled']);
	elseif($data['status_check'] == 'Completed')
		$orders->whereIn('order_status',['SerComplete', 'SupComplete']);
	elseif($data['status_check'] == 'Confirmation Pending')
		$orders->whereIn('order_status',['DPending']);
	elseif($data['status_check'] == 'Driver not found')
		$orders->whereIn('order_status',['Driver not found']);
		
	if(isset($data['categories']) && !in_array(0, $data['categories']))
		$orders->whereIn('category_id', $data['categories']);

	if(isset($data['search']) && $data['search'] != '')
		$orders->whereRaw("( (pickup_address LIKE '%".$data['search']."%') || (dropoff_address LIKE '%".$data['search']."%') || (name LIKE '%".$data['search']."%') || (phone_number LIKE '%".$data['search']."%') )");
///////////////		Filters 	////////////////////////

	$orders->join('users','users.user_id','orders.customer_user_id');

	$orders->select("*",
		DB::RAW("( date_format(CONVERT_TZ(order_timings,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as order_timingsz"),
		DB::RAW("( date_format(CONVERT_TZ(orders.created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
		DB::RAW("( date_format(CONVERT_TZ(orders.updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
	);

	$orders->with([

		'payment',

/////////////////		Category 	//////////////////////////////
		'category_brand_product' => function($q4) use ($data) {

			$q4->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);
		},
		'category_brand' => function($q5) use ($data) {

			$q5->join('category_brand_details', 'category_brand_details.category_brand_id', 'category_brands.category_brand_id')->where('category_brand_details.language_id', $data['admin_language_id']);
		},
		'category' => function($q6) use ($data) {

			$q6->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);
		},
/////////////////		Category 	//////////////////////////////


/////////////////		Customer /////////////////////////////////
		'customer_user_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},
/////////////////		Customer 	//////////////////////////////

/////////////////		Driver 	//////////////////////////////////
		'driver_user_detail' => function($q4) use ($data) {

			$q4->join('users', 'users.user_id','user_details.user_id');

			$q4->select('*',
				DB::RAW("(CASE WHEN profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
			);

		},
		'order_products' => function($q8) use ($data) {
			$q8->select('*');
		}
/////////////////		Driver 	//////////////////////////////////

		]);

	if($data['ordering_filter'] == '0')
		$orders->orderBy('orders.created_at', 'ASC');
	elseif($data['ordering_filter'] == '1')
		$orders->orderBy('orders.created_at', 'DESC');
	elseif($data['ordering_filter'] == '2')
		$orders->orderBy('orders.order_timings','ASC');
	else
		$orders->orderBy('orders.order_timings','DESC');

	return $orders->paginate();

}
	
	/**
	*@role Get All Failure Order
	*@Method POST
	*@author Raghav
	*/
	public static function order_details_products($data)
	{
		$orders = DB::table('order_products')
			->join('category_brand_details', 'category_brand_details.category_brand_id', '=', 'order_products.category_brand_id')
			->join('category_details', 'category_details.category_id', '=', 'order_products.category_id')
			->where('order_products.order_id',$data['order_id'])
			->where('category_brand_details.language_id',$data['admin_language_id'])
			->where('category_details.language_id',$data['admin_language_id'])
			->get(['order_products.*', 'category_brand_details.name as BrandName', 'category_details.name as category_name'])
			->toArray();
			
		
		$ArrayMain = array();
		$Order     = array();
		$final_charge = 0.0;
		for($i = 0; $i<count($orders);$i++ )
		{ 
			$order['Id']             = $orders[$i]->category_brand_product_id;
			$order['CategoryName']   = $orders[$i]->category_name;
			$order['BrandName']      = $orders[$i]->BrandName;
			$order['productName']    = $orders[$i]->productName;
			$order['Quantity']       = $orders[$i]->product_quantity;
			$order['price_per_item'] = $orders[$i]->price_per_item;
			$OrderPayment          = $final_charge + $orders[$i]->price_per_item;
			$final_charge          = $OrderPayment;
			$ArrayMain[]           = $order;
		}
		
		$Oldorders = DB::table('orders')
		->where('order_id',$data['order_id'])
		->join('category_brand_details', 'category_brand_details.category_brand_id', '=', 'orders.category_brand_id')
		->join('category_details', 'category_details.category_id', '=', 'orders.category_id')
		->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', '=', 'orders.category_brand_product_id')
		->join('category_brand_products', 'category_brand_products.category_brand_product_id', '=', 'category_brand_product_details.category_brand_product_id')
		->where('category_brand_details.language_id',$data['admin_language_id'])
		->where('category_details.language_id',$data['admin_language_id'])
		->where('category_brand_product_details.language_id',$data['admin_language_id'])
		->get(['category_brand_details.name as BrandName','category_brand_products.min_quantity as product_quantity','category_brand_products.price_per_quantity as price_per_quantity', 'category_details.name as category_name', 'category_brand_product_details.*'] )
		->toArray();
		
		//Get Payment
		$ordersPayment = DB::table('payments')
			->where('payments.order_id',$data['order_id'])
			->get()
			->toArray();
			
		if(!empty($Oldorders))
		{
			if(!empty($ordersPayment))
			{
				$oldOrderPayment = $ordersPayment[0]->final_charge;
			}else{
				$oldOrderPayment = 0.0;
			}
			$TotalFinalPayment = $oldOrderPayment + $final_charge;
		}
		else
		{
			$TotalFinalPayment = $final_charge;
		}
		
		for($j =0;$j<count($Oldorders);$j++)
		{
			$order['Id']           = $Oldorders[$j]->category_brand_product_id;
			$order['CategoryName'] = $Oldorders[$j]->category_name;
			$order['BrandName']    = $Oldorders[$j]->BrandName;
			$order['productName']  = $Oldorders[$j]->name;
			$order['Quantity']     = $Oldorders[$j]->product_quantity;
			$order['price_per_item'] = $Oldorders[$j]->price_per_quantity;
			$ArrayMain[] = $order;
		}
		$ArrayMains = array();
		$ArrayMains['Products']     = $ArrayMain;
		$ArrayMains['OrderPayment'] = number_format($TotalFinalPayment, 2);
		return $ArrayMains;
	}

}
