<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Aug 2018 09:00:55 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;

/**
 * Class OrderFailure
 * 
 *
 * @package App\Models
 */
class OrderFailure extends Eloquent
{
    protected $primaryKey = 'failure_id';
	protected $fillable = [
		'message',
		'products',
		'customer_id',
		'customer_name',
		'order_date',
		'sms_object'
    ];
    

    public static function admin_order_failure_listings($data)
    {
        $orders = OrderFailure::select("*",
		DB::RAW("( date_format(CONVERT_TZ(order_failures.order_date,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")
	);
        return $orders->get();
    }


}