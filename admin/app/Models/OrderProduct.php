<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Aug 2018 09:00:55 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;

/**
 * Class OrderFailure
 * 
 *
 * @package App\Models
 */
class OrderProduct extends Eloquent
{
    protected $primaryKey = 'order_product_id';
	protected $fillable = [
		'order_id',
		'category_id',
		'category_brand_id',
		'category_brand_product_id',
		'product_weight',
		'price_per_item',
		'image_url',
		'productName',
		'product_quantity',
		'gift_offer'
    ];
    

}