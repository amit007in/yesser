<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 Aug 2018 17:37:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;

/**
 * Class OrderRating
 * 
 * @property int $order_rating_id
 * @property int $order_id
 * @property int $customer_user_detail_id
 * @property int $customer_organisation_id
 * @property int $driver_organisation_id
 * @property int $driver_user_detail_id
 * @property int $ratings
 * @property string $comments
 * @property string $created_by
 * @property string $blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Order $order
 * @property \App\Models\UserDetail $user_detail
 * @property \App\Models\Organisation $organisation
 *
 * @package App\Models
 */
class OrderRating extends Eloquent
{
	protected $primaryKey = 'order_rating_id';

	protected $casts = [
		'order_id' => 'int',
		'customer_user_id' => 'int',
		'customer_user_detail_id' => 'int',
		'customer_organisation_id' => 'int',
		'driver_organisation_id' => 'int',
		'driver_user_id' => 'int',
		'driver_user_detail_id' => 'int',
		'ratings' => 'int'
	];

	protected $fillable = [
		'order_id',
		'customer_user_detail_id',
		'driver_user_detail_id',
		'customer_organisation_id',
		'driver_organisation_id',
		'ratings',
		'comments',
		'created_by',
		'blocked'
	];

	public function order()
		{
			return $this->belongsTo(\App\Models\Order::class, 'order_id', 'order_id');
		}

//////////////	Customer 	///////////////////
	public function customer_user()
		{
			return $this->belongsTo(\App\Models\User::class, 'customer_user_id');
		}

	public function customer_user_detail()
		{
			return $this->belongsTo(\App\Models\UserDetail::class, 'customer_user_detail_id');
		}

	public function customer_organisation()
		{
			return $this->belongsTo(\App\Models\Organisation::class, 'customer_organisation_id');
		}
//////////////	Customer 	///////////////////


//////////////	Driver 	///////////////////////
	public function driver_user()
		{
			return $this->belongsTo(\App\Models\User::class, 'driver_user_id');
		}

	public function driver_user_detail()
		{
			return $this->belongsTo(\App\Models\UserDetail::class, 'driver_user_detail_id');
		}

	public function driver_organisation()
		{
			return $this->belongsTo(\App\Models\Organisation::class, 'driver_organisation_id');
		}
//////////////	Driver 	///////////////////////

//////////////////////		All Reviews 	///////////////////////
public static function org_all_reviews($data)
	{
	
		$reviews = OrderRating::whereBetween('created_at', [$data['starting_dt'], $data['ending_dt']]);

		$reviews->where('driver_organisation_id', $data['organisation_id']);

		$reviews->whereIn('created_by', $data['created_bys']);

		$reviews->select('*',
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")
		);

		$reviews->with([

			'customer_user_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			'customer_user',


			'driver_user_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			'driver_user',

			'order'

		]);

		return $reviews->get();

	}

//////////////////		Admin Reviews 	////////////////////////////////////////////////
public static function admin_all_reviews($data)
	{
	
		$reviews = OrderRating::whereBetween('created_at', [$data['starting_dt'], $data['ending_dt']]);

		$reviews->whereIn('created_by', $data['created_bys']);

		$reviews->select('*',
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")
		);

		if(isset($data['user_detail_id']))
			{
				$reviews->where('customer_user_detail_id', $data['user_detail_id'])->orWhere('driver_user_detail_id', $data['user_detail_id']);
			}
		elseif(isset($data['driver_user_detail_id']))
			{
				$reviews->where('customer_user_detail_id', $data['driver_user_detail_id'])->orWhere('driver_user_detail_id', $data['driver_user_detail_id']);
			}


		$reviews->with([

			'customer_user_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			'customer_user',

			'driver_user_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			'driver_user',

			'order'

		]);

		return $reviews->get();

	}

//////////////////		Admin Reviews 	////////////////////////////////////////////////
public static function admin_reviews_new($data)
	{
	
	$reviews = OrderRating::whereBetween('order_ratings.created_at', [$data['starting_dt'], $data['ending_dt']]);

	$reviews->join('users as cust', 'cust.user_id', 'order_ratings.customer_user_id');
	$reviews->join('user_details as cust_detail', 'cust_detail.user_detail_id', 'order_ratings.customer_user_detail_id');

	$reviews->join('users as driver', 'driver.user_id', 'order_ratings.driver_user_id');
	$reviews->join('user_details as driver_detail', 'driver_detail.user_detail_id', 'order_ratings.driver_user_detail_id');

	$reviews->join('orders', 'orders.order_id', 'order_ratings.order_id');

	$reviews->select('order_ratings.*','driver.created_by', 'orders.order_id as order_id',
		'cust.phone_number as cust_phone_number','cust.name as cust_name',
		'driver.phone_number as driver_phone_number','driver.name as driver_name',
		'orders.order_type','cust_detail.profile_pic as cust_profile_pic','driver_detail.profile_pic as driver_profile_pic',
		DB::RAW("( date_format(CONVERT_TZ(order_ratings.created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
		DB::RAW("(CASE WHEN cust_detail.profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('SOCKET_URL')."/Uploads/',cust_detail.profile_pic) END) as cust_profile_pic_url"),
		DB::RAW("(CASE WHEN driver_detail.profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',driver_detail.profile_pic) END) as driver_profile_pic_url")
	);

	$whereRaw = '1=1';

	/////////	Order Type Filtering 	////////////////////
	if($data['order_filter'] != 'All')
		$whereRaw = $whereRaw.' AND order_type = "'.$data['order_filter'].'" ';
	/////////	Order Type Filtering 	////////////////////

	if($data['createdby_filter'] != 'All')
		$whereRaw = $whereRaw.' AND order_ratings.created_by = "'.$data['createdby_filter'].'" ';

	if(isset($data['user_detail_id']))
		{////////		Specific User Reviews Only ////////////////////

		$whereRaw = $whereRaw.' AND ((order_ratings.customer_user_detail_id = "'.$data['user_detail_id'].'") OR (order_ratings.driver_user_detail_id = "'.$data['user_detail_id'].'")) ';

		if($data['search'] != '')
			{///////////////	Order Search Filtering 		/////////////////

			$whereRaw = $whereRaw.' AND ( (driver.name LIKE "%'.$data['search'].'%") OR (driver.phone_number LIKE "%'.$data['search'].'%") OR (order_ratings.ratings = "'.$data['search'].'") OR (order_ratings.comments LIKE "%'.$data['search'].'%") ) ';

			}///////////////	Order Search Filtering 		/////////////////

		}////////		Specific User Reviews Only ////////////////////
	elseif(isset($data['driver_user_detail_id']))
		{

		$whereRaw = $whereRaw.' AND ((order_ratings.customer_user_detail_id = "'.$data['driver_user_detail_id'].'") OR (order_ratings.driver_user_detail_id = "'.$data['driver_user_detail_id'].'")) ';

		if($data['search'] != '')
			{///////////////	Order Search Filtering 		/////////////////

			$whereRaw = $whereRaw.' AND ( (cust.name LIKE"%'.$data['search'].'%") OR (cust.phone_number LIKE "%'.$data['search'].'%") OR (order_ratings.ratings = "'.$data['search'].'") OR (order_ratings.comments LIKE "%'.$data['search'].'%") ) ';

			}///////////////	Order Search Filtering 		/////////////////

		}///////////////	Order Search Filtering 		/////////////////
	elseif($data['search'] != '')
		{///////////////	Order Search Filtering 		/////////////////

		$whereRaw = $whereRaw.' AND ( ((cust.name LIKE "%'.$data['search'].'%") OR (cust.phone_number LIKE "%'.$data['search'].'%")) OR ((driver.name LIKE "%'.$data['search'].'%") OR (driver.phone_number LIKE "%'.$data['search'].'%")) OR (order_ratings.ratings = "'.$data['search'].'") OR (order_ratings.comments LIKE "%'.$data['search'].'%") )';

		}///////////////	Order Search Filtering 		/////////////////


	if(isset($data['driver_organisation_id']) && $data['driver_organisation_id'] != 0)
		$whereRaw = $whereRaw.' AND driver.organisation_id='.$data['driver_organisation_id'].' ';
	if(isset($data['customer_organisation_id']) && $data['customer_organisation_id'] != 0)
		$whereRaw = $whereRaw.' AND cust.organisation_id='.$data['customer_organisation_id'].' ';
	if(isset($data['organisation_id']) && $data['organisation_id'] != 0)
		$whereRaw = $whereRaw.' AND driver.organisation_id='.$data['organisation_id'].' ';

	$reviews->whereRaw($whereRaw);

	$reviews->orderBy('order_ratings.created_at','DESC');

	return $reviews->paginate();

	}

//////////////////

}
