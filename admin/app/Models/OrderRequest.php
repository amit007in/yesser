<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 Aug 2018 17:37:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderRequest
 * 
 * @property int $order_request_id
 * @property int $order_id
 * @property int $driver_user_id
 * @property int $driver_user_detail_id
 * @property string $order_request_status
 * @property float $driver_request_latitude
 * @property float $driver_request_longitude
 * @property float $driver_current_latitude
 * @property float $driver_current_longitude
 * @property \Carbon\Carbon $accepted_at
 * @property string $rotation
 * @property float $driver_accepted_latitude
 * @property float $driver_accepted_longitude
 * @property string $full_trank
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Order $order
 * @property \App\Models\User $user
 * @property \App\Models\UserDetail $user_detail
 *
 * @package App\Models
 */
class OrderRequest extends Eloquent
{
	protected $primaryKey = 'order_request_id';

	protected $casts = [
		'order_id' => 'int',
		'driver_user_id' => 'int',
		'driver_user_detail_id' => 'int',
		'driver_request_latitude' => 'float',
		'driver_request_longitude' => 'float',
		'driver_current_latitude' => 'float',
		'driver_current_longitude' => 'float',
		'driver_accepted_latitude' => 'float',
		'driver_accepted_longitude' => 'float'
	];

	protected $dates = [
		'accepted_at'
	];

	protected $fillable = [
		'order_id',
		'driver_user_id',
		'driver_user_detail_id',
		'order_request_status',
		'driver_request_latitude',
		'driver_request_longitude',
		'driver_current_latitude',
		'driver_current_longitude',
		'accepted_at',
		'rotation',
		'driver_accepted_latitude',
		'driver_accepted_longitude'
	];

	public function order()
		{
			return $this->belongsTo(\App\Models\Order::class, 'order_id', 'order_id');
		}

	public function driver_user()
		{
			return $this->belongsTo(\App\Models\User::class, 'driver_user_id');
		}

	public function driver_user_detail()
		{
			return $this->belongsTo(\App\Models\UserDetail::class, 'driver_user_detail_id');
		}

}
