<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 02 Aug 2018 10:57:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

use DB;

/**
 * Class Organisation
 * 
 * @property int $organisation_id
 * @property int $language_id
 * @property string $name
 * @property string $licence_number
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $image
 * @property string $phone_code
 * @property string $phone_number
 * @property string $stripe_connect_id
 * @property string $forgot_token
 * @property \Carbon\Carbon $forgot_token_validity
 * @property string $timezone
 * @property string $timezonez
 * @property string $blocked
 * @property string $created_by
 * @property float $buraq_percentage
 * @property string $address
 * @property float $address_latitude
 * @property float $address_longitude
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $Organisation_categories
 *
 * @package App\Models
 */
class Organisation extends Eloquent
{
	protected $primaryKey = 'organisation_id';

	protected $casts = [
		'language_id' => 'int',
		'credit_amount_limit' => 'float',
		'credit_day_limit' => 'int',
		'buraq_percentage' => 'float'
	];

	protected $dates = [
		'forgot_token_validity'
	];

	protected $hidden = [
		'password',
		'forgot_token'
	];

	protected $fillable = [
		'language_id',
		'name',
		'licence_number',
		'username',
		'email',
		'password',
		'image',
		'phone_code',
		'phone_number',
		'stripe_connect_id',
		'forgot_token',
		'forgot_token_validity',
		'timezone',
		'timezonez',
		'credit_amount_limit',
		'credit_day_limit',
		'credit_start_dt',
		'blocked',
		'sort_order',
		'created_by',
		'buraq_percentage'
	];

	public function language()
		{
			return $this->belongsTo(\App\Models\Language::class, 'language_id', 'language_id');
		}


	public function admin_user_detail_logins()
		{
			return $this->hasMany(\App\Models\AdminUserDetailLogin::class, 'user_organisation_id');
		}

	// public function ser_orders()
	// 	{
	// 		return $this->hasMany(\App\Models\Order::class, 'driver_organisation_id');
	// 	}

	// public function cust_orders()
	// 	{
	// 		return $this->hasMany(\App\Models\Order::class, 'customer_organisation_id');
	// 	}

	public function organisation_areas()
		{
			return $this->hasMany(\App\Models\OrganisationArea::class, 'organisation_id', 'organisation_id');
		}

	public function organisation_services()
		{
			return $this->hasMany(\App\Models\OrganisationCategory::class, 'organisation_id', 'organisation_id');
			//->where('category_type', 'Service');
		}
	public function organisation_supports()
		{
			return $this->hasMany(\App\Models\OrganisationCategory::class, 'organisation_id', 'organisation_id');
			//->where('category_type','Support');
		}

	public function org_products()
		{
			return $this->hasMany(\App\Models\OrganisationCategoryBrandProduct::class, 'organisation_id', 'organisation_id');
		}

	public function user()
		{
			return $this->hasMany(\App\Models\User::class, 'user_id', 'user_id');
		}

/////////////////////////////		Admin All organisations 	////////////////////////////////////////
public static function admin_all_organisations($data)
	{

		$organisations = Organisation::select('*',
			DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url"),
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
			DB::RAW("(SELECT COUNT(*) FROM order_ratings WHERE order_ratings.driver_organisation_id=organisations.organisation_id) as review_counts"),
			DB::RAW("(SELECT COUNT(*) FROM payment_ledgers as pl WHERE pl.organisation_id=organisations.organisation_id AND deleted='0') as ledger_counts"),
			DB::RAW("(SELECT COUNT(*) FROM admin_user_detail_logins as audl WHERE audl.organisation_id=organisations.organisation_id AND user_type_id='4') as login_counts")
		);

		$organisations->with([

			'organisation_services' => function($q1) use ($data) {

				$q1->join('categories','categories.category_id','organisation_categories.category_id')
				->where('organisation_categories.deleted','0')
				->where('categories.category_type','Service');
				
				$q1->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);

				$q1->select('organisation_category_id','organisation_categories.organisation_id',  'organisation_categories.category_id', 'categories.category_type', 'category_details.name as cat_name', 'category_details.description as ca_description' );

			},
			'organisation_supports' => function($q1) use ($data) {

				$q1->join('categories','categories.category_id','organisation_categories.category_id')
				->where('organisation_categories.deleted','0')
				->where('categories.category_type','Support');
				
				$q1->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);

				$q1->select('organisation_category_id','organisation_categories.organisation_id',  'organisation_categories.category_id', 'categories.category_type', 'category_details.name as cat_name', 'category_details.description as ca_description');

			}

		]);

		$organisations->whereBetween('updated_at', [$data['starting_dt'], $data['ending_dt']]);

		return $organisations->get();

	}

/////////////////////////////		Add New Record 		//////////////////////////////////////////////////
public static function add_new_record($data)
	{
		
		$org = new Organisation();

		$org->language_id = isset($data['language_id']) ? $data['language_id'] : 1;
		$org->name = $data['name'];
		$org->licence_number = $data['licence_number'];
		$org->username = $data['username'];
		$org->email = $data['email'];
		$org->password = \Hash::make($data['password']);
		$org->image = $data['image_name'];
		$org->phone_code = isset($data['phone_code']) ? $data['phone_code'] : "+968";
		$org->phone_number = $data['phone_number'];
		$org->stripe_connect_id = '';
		$org->forgot_token = '';
		$org->forgot_token_validity = null;
		$org->created_by = $data['created_by'];

		$org->credit_amount_limit = isset($data['credit_amount_limit']) ? $data['credit_amount_limit'] : -1;
		$org->credit_day_limit = isset($data['credit_day_limit']) ? $data['credit_day_limit'] : -1;
		$org->credit_start_dt = $data['credit_start_dt'];
		$org->buraq_percentage = $data['buraq_percentage'];

		$org->address = $data['address'];
		$org->address_latitude = $data['address_latitude'];
		$org->address_longitude = $data['address_longitude'];

		$org->sort_order = $data['sort_order'];

		$org->save();

		return $org;

	}

/////////////////////////////		Update Record 		/////////////////////////////////////////////
public static function update_record($data)
	{

		Organisation::where('organisation_id',$data['organisation_id'])->limit(1)->update([
			'name' => $data['name'],
			//'email' => $data['email'],
			'image' => $data['image_name'],
			'phone_code' => isset($data['phone_code']) ? $data['phone_code'] : "+968",
			'phone_number' => $data['phone_number'],
			'credit_amount_limit' => isset($data['credit_amount_limit']) ? $data['credit_amount_limit'] : -1,
			'credit_day_limit' => isset($data['credit_day_limit']) ? $data['credit_day_limit'] : -1,
			'credit_start_dt' => $data['credit_start_dt'],
			'buraq_percentage' => $data['buraq_percentage'],
			'address' => isset($data['address']) ? $data['address'] : "",
			'address_latitude' => isset($data['address_latitude']) ? $data['address_latitude'] : 0.0,
			'address_longitude' => isset($data['address_longitude']) ? $data['address_longitude'] : 0.0,
			//'sort_order' => $data['sort_order'],
			'updated_at' => new \DateTime
		]);

	}

/////////////////////////////		Admin Single Org Details 	/////////////////////////////////////
public static function admin_single_org_details($data)
	{
		$org = Organisation::where('organisation_id', $data['organisation_id']);

		$org->select('*',
			DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url"),
			DB::RAW("( CASE WHEN credit_start_dt IS NULL THEN '' ELSE date_format(CONVERT_TZ(credit_start_dt,'+00:00','".$data['timezonez']."'),'%Y-%m-%d %H:%i:%s') END) as credit_start_dt")
		);

		return $org->first();
	}

/////////////////////////////	Panel Login Details 		/////////////////////////////
public static function panel_login_get($data)
	{

		$org = Organisation::where('username', $data['username']);

		return $org->first();

	}
/////////////////////////////		Org Update Record 		////////////////////////////
public static function org_update_record($data)
	{

		Organisation::where('organisation_id',$data['organisation_id'])->limit(1)->update([
			'name' => $data['name'],
			'image' => $data['image_name'],
			'phone_code' => isset($data['phone_code']) ? $data['phone_code'] : "+968",
			'phone_number' => $data['phone_number'],
			'address' => isset($data['address']) ? $data['address'] : "",
			'address_latitude' => isset($data['address_latitude']) ? $data['address_latitude'] : 0.0,
			'address_longitude' => isset($data['address_longitude']) ? $data['address_longitude'] : 0.0,
			'updated_at' => new \DateTime
		]);

	}

////////////////////

}
