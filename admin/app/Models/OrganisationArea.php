<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 Aug 2018 17:37:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;

/**
 * Class OrganisationArea
 * 
 * @property int $organisation_area_id
 * @property int $organisation_id
 * @property int $category_id
 * @property string $address
 * @property float $address_latitude
 * @property float $address_longitude
 * @property int $distance
 * @property string $sunday_service
 * @property string $monday_service
 * @property string $tuesday_service
 * @property string $wednesday_service
 * @property string $thursday_service
 * @property string $friday_service
 * @property string $saturday_service
 * @property string $created_by
 * @property string $deleted
 * @property string $blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Organisation $organisation
 *
 * @package App\Models
 */
class OrganisationArea extends Eloquent
{
	protected $primaryKey = 'organisation_area_id';

	protected $casts = [
		'organisation_id' => 'int',
		'category_id' => 'int',
		'address_latitude' => 'float',
		'address_longitude' => 'float',
		'distance' => 'int'
	];

	protected $fillable = [
		'organisation_id',
		'category_id',
		'label',
		'address',
		'address_latitude',
		'address_longitude',
		'distance',
		'sunday_service',
		'monday_service',
		'tuesday_service',
		'wednesday_service',
		'thursday_service',
		'friday_service',
		'saturday_service',
		'created_by',
		'deleted',
		'blocked'
	];

	public function organisation()
		{
			return $this->belongsTo(\App\Models\Organisation::class, 'organisation_id', 'organisation_id');
		}

	public function category()
		{
			return $this->belongsTo(\App\Models\Category::class, 'category_id', 'category_id');
		}

//////////////////////	Category Wise Org Areas 		///////////////////////////////////
public static function category_wise_org_areas($data)
	{

		$areas = OrganisationArea::where('organisation_id',$data['organisation_id'])->where('category_id',$data['category_id'])->where('deleted','0');

		$areas->whereBetween('created_at', [$data['starting_dt'], $data['ending_dt']]);

		$areas->select('*',
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")
		);

		return $areas->get();

	}

//////////////////////		Insert New Record 		///////////////////////////////////////
public static function insert_new_record($data)
	{

		$oa = new OrganisationArea();

		$oa->organisation_id = $data['organisation_id'];
		$oa->category_id = $data['category_id'];
		$oa->label = $data['label'];
		$oa->address = $data['address'];
		$oa->address_latitude = $data['address_latitude'];
		$oa->address_longitude = $data['address_longitude'];
		$oa->distance = $data['distance'];
		$oa->sunday_service = in_array('sunday', $data['days']) ? "1" : "0";
		$oa->monday_service = in_array('monday', $data['days']) ? "1" : "0";
		$oa->tuesday_service = in_array('tuesday', $data['days']) ? "1" : "0";
		$oa->wednesday_service = in_array('wednesday', $data['days']) ? "1" : "0";
		$oa->thursday_service = in_array('thrusday', $data['days']) ? "1" : "0";
		$oa->friday_service = in_array('friday', $data['days']) ? "1" : "0";
		$oa->saturday_service = in_array('saturday', $data['days']) ? "1" : "0";
		$oa->created_by = $data['created_by'];
		$oa->deleted = "0";
		$oa->blocked = "0";
		$oa->created_at = new \DateTime;
		$oa->updated_at = new \DateTime;

		$oa->save();

		return $oa;

	}

///////////////////////			Update Recorg 		////////////////////////////////////////
public static function update_record($data)
	{

		OrganisationArea::where('organisation_area_id',$data['organisation_area_id'])->limit(1)->update([
			'address' => $data['address'],
			'label' => $data['label'],
			'address_latitude' => $data['address_latitude'],
			'address_longitude' => $data['address_longitude'],
			'distance' => $data['distance'],
			'sunday_service' => in_array('sunday', $data['days']) ? "1" : "0",
			'monday_service' => in_array('monday', $data['days']) ? "1" : "0",
			'tuesday_service' => in_array('tuesday', $data['days']) ? "1" : "0",
			'wednesday_service' => in_array('wednesday', $data['days']) ? "1" : "0",
			'thursday_service' => in_array('thrusday', $data['days']) ? "1" : "0",
			'friday_service' => in_array('friday', $data['days']) ? "1" : "0",
			'saturday_service' => in_array('saturday', $data['days']) ? "1" : "0",
			'updated_at' => new \DateTime
		]);

	}

//////////////////////

}
