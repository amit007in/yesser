<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 02 Aug 2018 10:57:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

use DB;

/**
 * Class OrganisationCategory
 * 
 * @property int $organisation_category_id
 * @property int $organisation_id
 * @property int $category_id
 * @property string $category_type
 * @property float $buraq_percentage
 * @property string $deleted
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Organization $organisation
 * @property \App\Models\Category $category
 *
 * @package App\Models
 */
class OrganisationCategory extends Eloquent
{
	protected $primaryKey = 'organisation_category_id';

	protected $casts = [
		'organisation_id' => 'int',
		'category_id' => 'int',
		'buraq_percentage' => 'float'
	];

	protected $fillable = [
		'organisation_id',
		'category_id',
		'category_type',
		'buraq_percentage',
		'deleted'
	];

	public function organisation()
		{
			return $this->belongsTo(\App\Models\Organisation::class, 'organisation_id', 'organisation_id');
		}

	public function category()
		{
			return $this->belongsTo(\App\Models\Category::class, 'category_id', 'category_id');
		}

////////////////		Admin Org All Categories 	////////////////////////////////////
public static function org_all_categories($data)
	{

		$cats = OrganisationCategory::where('organisation_id', $data['organisation_id'])->where('deleted','0')->
		where('organisation_categories.category_type',$data['category_type']);

		$cats->join('categories', 'categories.category_id', '=', 'organisation_categories.category_id');
		//$cats->join('category_details', 'category_details.category_id','categories.category_id')->where('category_details.language_id','=',$data['admin_language_id']);

		$cats->with(['category.category_details']);

		$cats->select('*', 'organisation_categories.buraq_percentage as buraq_percentage',

			DB::RAW("(SELECT COUNT(*) FROM organisation_category_brand_products as ocbp WHERE ocbp.organisation_id=".$data['organisation_id']." AND ocbp.category_id=organisation_categories.category_id) as product_counts"),
			//DB::RAW("( SELECT COUNT(*) FROM category_brand_products as cbp where cbp.category_brand_id=category_brands.category_brand_id ) as product_counts")

			DB::RAW("(SELECT COUNT(*) FROM user_details as ud WHERE ud.organisation_id=".$data['organisation_id']." AND ud.category_id=organisation_categories.category_id AND ud.user_type_id=".$data['user_type_id'].") as driver_counts"),
			DB::RAW("(SELECT COUNT(*) FROM organisation_areas as oa WHERE oa.organisation_id=".$data['organisation_id']." AND oa.deleted='0' AND oa.category_id=organisation_categories.category_id) as area_counts"),
			DB::RAW("(SELECT COUNT(*) FROM organisation_coupons as oc WHERE oc.organisation_id=".$data['organisation_id']." AND oc.deleted='0' AND oc.category_id=organisation_categories.category_id ) as coupon_counts"),
			DB::RAW("( CONCAT('".env('RESIZE_URL')."',categories.image) ) as image_url"),
			DB::RAW("(SELECT COUNT(*) FROM orders WHERE orders.category_id=categories.category_id AND driver_organisation_id=".$data['organisation_id']." ) as order_counts")
		);

		return $cats->get();

	}

/////////////////

}
