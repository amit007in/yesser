<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 01 Sep 2018 07:01:17 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;
/**
 * Class OrganisationCategoryBrandProduct
 * 
 * @property int $organisation_category_brand_product_id
 * @property int $organisation_id
 * @property int $category_id
 * @property int $category_brand_id
 * @property int $category_brand_product_id
 * @property float $alpha_price
 * @property float $price_per_quantity
 * @property float $price_per_distance
 * @property float $price_per_weight
 * @property float $price_per_hr
 * @property float $price_per_sq_mt
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class OrganisationCategoryBrandProduct extends Eloquent
{
	protected $primaryKey = 'organisation_category_brand_product_id';

	protected $casts = [
		'organisation_id' => 'int',
		'category_id' => 'int',
		'category_brand_id' => 'int',
		'category_brand_product_id' => 'int',
		'alpha_price' => 'float',
		'price_per_quantity' => 'float',
		'price_per_distance' => 'float',
		'price_per_weight' => 'float',
		'price_per_hr' => 'float',
		'price_per_sq_mt' => 'float',
	];

	protected $fillable = [
		'organisation_id',
		'category_id',
		'category_brand_id',
		'category_brand_product_id',
		'alpha_price',
		'price_per_quantity',
		'price_per_distance',
		'price_per_weight',
		'price_per_hr',
		'price_per_sq_mt'
	];

	public function organisation()
		{
			return $this->belongsTo(\App\Models\Organisation::class, 'organisation_id', 'organisation_id');
		}

	public function category()
		{
			return $this->belongsTo(\App\Models\Category::class, 'category_id', 'category_id');
		}

	public function category_brand()
		{
			return $this->belongsTo(\App\Models\CategoryBrand::class, 'category_brand_id', 'category_brand_id');
		}

	public function category_brand_product()
		{
			return $this->belongsTo(\App\Models\CategoryBrandProduct::class, 'category_brand_product_id', 'category_brand_product_id');
		}

//////////////////////		Org Brand Products 		//////////////////////////////
public static function org_services_products_listings($data)
	{

		$products = OrganisationCategoryBrandProduct::where('organisation_id', $data['organisation_id'])->where('category_id', $data['category_id']);

		$products->with([

			'category_brand' => function($q0) {

				$q0->with(['category_brand_details' => function($q01) {

					$q01->select('*',
						DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
					);
					$q01->orderBy('language_id','ASC');

					}
				]);

			},

			'category_brand_product' => function($q1) {

				$q1->with(['category_brand_product_details']);

			}

		]);

		return $products->get();

	}

//////////////////////////

}
