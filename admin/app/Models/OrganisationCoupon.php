<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 Aug 2018 17:37:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;
/**
 * Class OrganisationCoupon
 * 
 * @property int $organisation_coupon_id
 * @property int $organisation_id
 * @property int $category_id
 * @property int $category_brand_id
 * @property int $category_brand_product_id
 * @property float $price
 * @property int $quantity
 * @property string $description
 * @property string $deleted
 * @property string $blocked
 * @property string $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Organisation $organisation
 * @property \App\Models\Category $category
 * @property \App\Models\CategoryBrand $category_brand
 * @property \App\Models\CategoryBrandProduct $category_brand_product
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class OrganisationCoupon extends Eloquent
{
	protected $primaryKey = 'organisation_coupon_id';

	protected $casts = [
		'organisation_id' => 'int',
		'category_id' => 'int',
		'category_brand_id' => 'int',
		'category_brand_product_id' => 'int',
		'price' => 'float',
		'quantity' => 'int'
	];

	protected $fillable = [
		'organisation_id',
		'category_id',
		'category_brand_id',
		'category_brand_product_id',
		'price',
		'quantity',
		'description',
		'deleted',
		'blocked',
		'created_by'
	];

	public function organisation()
	{
		return $this->belongsTo(\App\Models\Organisation::class, 'organisation_id', 'organisation_id');
	}

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class, 'category_id', 'category_id');
	}

	public function category_brand()
	{
		return $this->belongsTo(\App\Models\CategoryBrand::class, 'category_brand_id', 'category_brand_id');
	}

	public function category_brand_product()
	{
		return $this->belongsTo(\App\Models\CategoryBrandProduct::class, 'category_brand_product_id', 'category_brand_product_id');
	}

	public function users()
	{
		return $this->belongsToMany(\App\Models\User::class, 'organisation_coupon_users')
					->withPivot('organisation_coupon_user_id', 'organisation_id', 'user_detail_id', 'price', 'quantity', 'quantity_left')
					->withTimestamps();
	}

///////////////////////////		Coupons All 		/////////////////////////////////////
public static function org_coupons_all($data)
	{

		$coupons = OrganisationCoupon::whereBetween('created_at', [$data['starting_dt'], $data['ending_dt']]);

		$coupons->select("*",
			DB::RAW("(SELECT COUNT(*) FROM organisation_coupon_users as ocu WHERE ocu.organisation_coupon_id=organisation_coupons.organisation_coupon_id ) as purchase_counts"),
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")	
		);

		$coupons->where('organisation_id', $data['organisation_id']);

		$coupons->with([


			'category_brand_product' => function($q2) use ($data) {

				$q2->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);

			}


		]);

		return $coupons->get();

	}
/////////////////////			Insert New Record 		////////////////////////////
public static function insert_new_record($data)
	{

		$oc = new OrganisationCoupon();

		$oc->organisation_id = $data['organisation_id'];
		$oc->category_id = $data['category_id'];
		$oc->category_brand_id = $data['category_brand_id'];
		$oc->category_brand_product_id = $data['category_brand_product_id'];
		$oc->price = $data['price'];
		$oc->quantity = $data['quantity'];
		$oc->description = isset($data['description']) ? $data['description'] : "";
		$oc->deleted = "0";
		$oc->blocked = "0";
		$oc->created_by = isset($data['created_by']) ? $data['created_by'] : "Org";
		$oc->created_at = new \DateTime;
		$oc->updated_at = new \DateTime;

		$oc->save();

		return $oc;

	}

/////////////////////////////		Update Record 		/////////////////////////////////////////////
public static function update_record($data)
	{
		OrganisationCoupon::where('organisation_coupon_id',$data['organisation_coupon_id'])->update([
			'category_brand_id' => $data['category_brand_id'],
			'category_brand_product_id' => $data['category_brand_product_id'],
			'price' => $data['price'],
			'quantity' => $data['quantity'],
			'description' => isset($data['description']) ? $data['description'] : "",
			'updated_at' => new \DateTime
		]);

	}

////////////////////

}
