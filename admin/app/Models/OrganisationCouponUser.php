<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 Aug 2018 17:37:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;
/**
 * Class OrganisationCouponUser
 * 
 * @property int $organisation_coupon_user_id
 * @property int $seller_user_id
 * @property int $seller_user_detail_id
 * @property int $seller_user_type_id
 * @property int $seller_organisation_id
 * @property int $organisation_coupon_id
 * @property int $category_id
 * @property int $category_brand_id
 * @property int $category_brand_product_id
 * @property int $customer_user_id
 * @property int $customer_user_detail_id
 * @property int $customer_user_type_id
 * @property int $customer_organisation_id
 * @property int $payment_id
 * @property float $price
 * @property int $quantity
 * @property int $quantity_left
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Organisation $organisation
 * @property \App\Models\User $user
 * @property \App\Models\UserDetail $user_detail
 * @property \App\Models\OrganisationCoupon $organisation_coupon
 * @property \Illuminate\Database\Eloquent\Collection $orders
 *
 * @package App\Models
 */
class OrganisationCouponUser extends Eloquent
{
	protected $primaryKey = 'organisation_coupon_user_id';

	protected $casts = [
		'seller_user_id' => 'int',
		'seller_user_detail_id' => 'int',
		'seller_user_type_id' => 'int',
		'seller_organisation_id' => 'int',
		'organisation_coupon_id' => 'int',
		'category_id' => 'int',
		'category_brand_id' => 'int',
		'category_brand_product_id' => 'int',
		'customer_user_id' => 'int',
		'customer_user_detail_id' => 'int',
		'customer_user_type_id' => 'int',
		'customer_organisation_id' => 'int',
		'payment_id' => 'int',
		'price' => 'float',
		'quantity' => 'int',
		'quantity_left' => 'int',
	];

	protected $dates = [
	];

	protected $fillable = [
		'seller_user_id',
		'seller_user_detail_id',
		'seller_user_type_id',
		'seller_organisation_id',
		'organisation_coupon_id',
		'category_id',
		'category_brand_id',
		'category_brand_product_id',
		'customer_user_id',
		'customer_user_detail_id',
		'customer_user_type_id',
		'customer_organisation_id',
		'payment_id',
		'price',
		'quantity',
		'quantity_left',
	];

////////////////////		Seller 		///////////////////////////////////////////////////////
	public function seller()
		{
			return $this->belongsTo(\App\Models\User::class, 'seller_user_id', 'user_id');
		}
	public function seller_detail()
		{
			return $this->belongsTo(\App\Models\UserDetail::class, 'seller_user_detail_id', 'user_detail_id');
		}
	public function seller_type()
		{
			return $this->belongsTo(\App\Models\UserType::class, 'seller_user_type_id', 'user_type_id');
		}
	public function seller_org()
		{
			return $this->belongsTo(\App\Models\Organisation::class, 'seller_organisation_id', 'organisation_id');
		}
////////////////////		Seller 		///////////////////////////////////////////////////////

	public function organisation_coupon()
		{
			return $this->belongsTo(\App\Models\OrganisationCoupon::class, 'organisation_coupon_id', 'organisation_coupon_id');
		}

////////////////////		Category 		///////////////////////////////////////////////////
	public function category()
		{
			return $this->belongsTo(\App\Models\Category::class, 'category_id', 'category_id');
		}

	public function category_brand()
		{
			return $this->belongsTo(\App\Models\CategoryBrand::class, 'category_brand_id', 'category_brand_id');
		}

	public function category_brand_product()
		{
			return $this->belongsTo(\App\Models\CategoryBrandProduct::class, 'category_brand_product_id', 'category_brand_product_id');
		}
////////////////////		Category 		///////////////////////////////////////////////////


////////////////////		Payer 		///////////////////////////////////////////////////////
	public function payer()
		{
			return $this->belongsTo(\App\Models\User::class, 'customer_user_id', 'user_id');
		}
	public function payer_detail()
		{
			return $this->belongsTo(\App\Models\UserDetail::class, 'customer_user_detail_id', 'user_detail_id');
		}
	public function payer_type()
		{
			return $this->belongsTo(\App\Models\UserType::class, 'customer_user_type_id', 'user_type_id');
		}
	public function payer_org()
		{
			return $this->belongsTo(\App\Models\Organisation::class, 'customer_organisation_id', 'organisation_id');
		}
////////////////////		Payer 		///////////////////////////////////////////////////////

	public function payment()
		{
			return $this->belongsTo(\App\Models\Payment::class, 'payment_id', 'payment_id');
		}

	public function orders()
		{
			return $this->hasMany(\App\Models\Order::class, 'organisation_coupon_user_id', 'organisation_coupon_user_id');
		}

////////////////////	Organisation Payments 		/////////////////////////////////////
public static function org_etoken_payments($data)
{

		$payments = OrganisationCouponUser::select("*",
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
		);

		$pamyents->where("seller_organisation_id", $data['organisation_id']);

		$payments->with([

			'payer',
			
			'payer_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			'payment',

					/////////////////		Category 	/////////////////////////////////
			'category_brand_product' => function($q41) use ($data) {

				$q41->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);
			},
			'category_brand' => function($q42) use ($data) {

				$q42->join('category_brand_details', 'category_brand_details.category_brand_id', 'category_brands.category_brand_id')->where('category_brand_details.language_id', $data['admin_language_id']);
			},
			'category' => function($q43) use ($data) {

				$q43->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);
			},
					/////////////////		Category 	/////////////////////////////////

		]);

		return $payments->get();

}


/////////////////////

}
