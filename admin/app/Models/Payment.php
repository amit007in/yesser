<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 06 Sep 2018 14:45:07 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Db;

use App\Models\Admin;
/**
 * Class Payment
 * 
 * @property int $payment_id
 * @property int $customer_user_id
 * @property int $customer_user_detail_id
 * @property int $customer_user_type_id
 * @property int $user_card_id
 * @property int $customer_organisation_id
 * @property int $order_id
 * @property int $seller_user_id
 * @property int $seller_user_detail_id
 * @property int $seller_user_type_id
 * @property int $seller_organisation_id
 * @property int $organisation_coupon_user_id
 * @property string $payment_type
 * @property string $payment_status
 * @property string $refund_status
 * @property string $transaction_id
 * @property string $refund_id
 * @property float $buraq_percentage
 * @property string $product_actual_value
 * @property int $product_quantity
 * @property string $product_weight
 * @property string $product_sq_mt
 * @property string $order_distance
 * @property string $order_time
 * @property string $product_alpha_charge
 * @property string $product_per_quantity_charge
 * @property string $product_per_weight_charge
 * @property string $product_per_distance_charge
 * @property string $product_per_hr_charge
 * @property string $product_per_sq_mt_charge
 * @property string $initial_charge
 * @property string $admin_charge
 * @property string $bank_charge
 * @property string $final_charge
 * @property string $bottle_charge
 * @property int $bottle_returned_value
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\User $user
 * @property \App\Models\UserType $user_type
 * @property \App\Models\UserDetail $user_detail
 * @property \Illuminate\Database\Eloquent\Collection $coupon_users
 *
 * @package App\Models
 */

class Payment extends Eloquent
{
	protected $primaryKey = 'payment_id';

	protected $casts = [
		'customer_user_id' => 'int',
		'customer_user_detail_id' => 'int',
		'customer_user_type_id' => 'int',
		'user_card_id' => 'int',
		'customer_organisation_id' => 'int',
		'order_id' => 'int',
		'seller_user_id' => 'int',
		'seller_user_detail_id' => 'int',
		'seller_user_type_id' => 'int',
		'seller_organisation_id' => 'int',
		'organisation_coupon_user_id' => 'int',
		'buraq_percentage' => 'float',
		'product_quantity' => 'int',
		'bottle_returned_value' => 'int'
	];

	protected $fillable = [
		'customer_user_id',
		'customer_user_detail_id',
		'customer_user_type_id',
		'user_card_id',
		'customer_organisation_id',
		'order_id',
		'seller_user_id',
		'seller_user_detail_id',
		'seller_user_type_id',
		'seller_organisation_id',
		'organisation_coupon_user_id',
		'payment_type',
		'payment_status',
		'refund_status',
		'transaction_id',
		'refund_id',
		'buraq_percentage',
		'product_actual_value',
		'product_quantity',
		'product_weight',
		'product_sq_mt',
		'order_distance',
		'order_time',
		'product_alpha_charge',
		'product_per_quantity_charge',
		'product_per_weight_charge',
		'product_per_distance_charge',
		'product_per_hr_charge',
		'product_per_sq_mt_charge',
		'initial_charge',
		'admin_charge',
		'bank_charge',
		'final_charge',
		'bottle_charge',
		'bottle_returned_value'
	];

//////////////////////////////////	Payer 	////////////////////////////
	public function payer()
		{
			return $this->belongsTo(\App\Models\User::class, 'customer_user_id', 'user_id');
		}

	public function payer_detail()
		{
			return $this->belongsTo(\App\Models\UserDetail::class, 'customer_user_detail_id', 'user_detail_id');
		}

	public function payer_type()
		{
			return $this->belongsTo(\App\Models\UserType::class, 'customer_user_type_id', 'user_type_id');
		}

	public function payer_card()
		{
			return $this->belongsTo(\App\Models\UserCard::class, 'user_card_id', 'user_card_id');
		}

	public function payer_org()
		{
			return $this->belongsTo(\App\Models\Organisation::class, 'customer_organisation_id', 'organisation_id');
		}
//////////////////////////////////	Payer 	////////////////////////////

	public function order()
		{
			return $this->belongsTo(\App\Models\Order::class, 'order_id', 'order_id');
		}

//////////////////////////////////	Seller 	////////////////////////////
	public function seller()
		{
			return $this->belongsTo(\App\Models\User::class, 'seller_user_id', 'user_id');
		}

	public function seller_detail()
		{
			return $this->belongsTo(\App\Models\UserDetail::class, 'seller_user_detail_id', 'user_detail_id');
		}

	public function seller_type()
		{
			return $this->belongsTo(\App\Models\UserType::class, 'seller_user_type_id', 'user_type_id');
		}

	public function seller_org()
		{
			return $this->belongsTo(\App\Models\Organisation::class, 'seller_organisation_id', 'organisation_id');
		}
		public function order_products()
		{
			return $this->hasMany(\App\Models\OrderProduct::class, 'order_id', 'order_id');
		}


	//////////////////////////////////	Seller 	////////////////////////////

	public function purchased_etokens()
		{
			return $this->belongsTo(\App\Models\OrganisationCouponUser::class, 'organisation_coupon_user_id', 'organisation_coupon_user_id');
		}

	public function purchased_coupons()
		{
			return $this->hasMany(\App\Models\CouponUser::class, 'payment_id', 'payment_id');
		}

//////////////////////////	Admin Order Payments 	////////////////////////////
public static function admin_orders_payment($data)
	{

		$payments = Payment::select("*",
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
		);

		$payments->where('order_id','!=',0);
		//$payments->where('seller_user_detail_id', '!=', 0);
		$payments->where('seller_user_type_id',$data['seller_user_type_id']);

		$payments->whereBetween('updated_at', [$data['starting_dt'], $data['ending_dt']]);

		$payments->with([
			'payer',
			'payer_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			'seller',
			'seller_detail' => function($q2) {

				$q2->select('*',
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			'seller_org' => function($q3) {

				$q3->select('*',
					DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
				);

			},

			/////////////		Order 	///////////////////////
			'order' => function($q4) use ($data) {

				$q4->with([

					/////////////////		Category 	/////////////////////////////////
			'category_brand_product' => function($q41) use ($data) {

				$q41->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);
			},
			'category_brand' => function($q42) use ($data) {

				$q42->join('category_brand_details', 'category_brand_details.category_brand_id', 'category_brands.category_brand_id')->where('category_brand_details.language_id', $data['admin_language_id']);
			},
			'category' => function($q43) use ($data) {

				$q43->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);
			},
			'order_products' => function($q8) use ($data) {
				$q8->select('*');
			}
					/////////////////		Category 	/////////////////////////////////

				]);

			}
			/////////////		Order 	///////////////////////

		]);

		return $payments->get();

	}

////////////////		Sums 		/////////////////////////////////////////////
public static function admin_ser_pay_sums($data)
	{

		$sum = Admin::select("*",
			DB::RAW("(SELECT COALESCE(SUM(final_charge),0) FROM payments WHERE order_id != 0 AND seller_user_type_id=".$data['seller_user_type_id']." AND updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."') as total_sum"),
			DB::RAW("(SELECT COALESCE(SUM(final_charge),0) FROM payments WHERE order_id != 0 AND seller_user_type_id=".$data['seller_user_type_id']." AND updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' AND payment_status='Completed' AND payment_type='Card') as completed_card_sum"),
			DB::RAW("(SELECT COALESCE(SUM(final_charge),0) FROM payments WHERE order_id != 0 AND seller_user_type_id=".$data['seller_user_type_id']." AND updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' AND payment_status='Completed' AND payment_type='Cash') as completed_cash_sum"),
			DB::RAW("(SELECT COALESCE(SUM(final_charge),0) FROM payments WHERE order_id != 0 AND seller_user_type_id=".$data['seller_user_type_id']." AND updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' AND payment_status='Completed' AND payment_type='eToken') as completed_etoken_sum")
		);

		return $sum->first();

	}

////////////////////			Org Orders Payment 		///////////////////////////
public static function org_orders_payment($data)
	{

		$payments = Payment::select("*",
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
		);

		$payments->where('order_id','!=',0);
		$payments->where('seller_organisation_id', $data['organisation_id']);
		//$payments->where('seller_user_detail_id', '!=', 0);
		$payments->where('seller_user_type_id',$data['seller_user_type_id']);

		$payments->whereBetween('updated_at', [$data['starting_dt'], $data['ending_dt']]);

		$payments->with([
			'payer',
			'payer_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			'seller',
			'seller_detail' => function($q2) {

				$q2->select('*',
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('DRIVER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			/////////////		Order 	///////////////////////
			'order' => function($q4) use ($data) {

				$q4->with([

					/////////////////		Category 	/////////////////////////////////
			'category_brand_product' => function($q41) use ($data) {

				$q41->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);
			},
			'category_brand' => function($q42) use ($data) {

				$q42->join('category_brand_details', 'category_brand_details.category_brand_id', 'category_brands.category_brand_id')->where('category_brand_details.language_id', $data['admin_language_id']);
			},
			'category' => function($q43) use ($data) {

				$q43->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);
			},
					/////////////////		Category 	/////////////////////////////////

				]);

			}
			/////////////		Order 	///////////////////////

		]);

		return $payments->get();

	}

////////////////		Org Payment Sums 		/////////////////////////////////////////////
public static function org_ser_pay_sums($data)
	{

		$sum = Admin::select("*",
			DB::RAW("(SELECT COALESCE(SUM(final_charge),0) FROM payments WHERE order_id != 0 AND seller_user_type_id=".$data['seller_user_type_id']." AND updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' AND seller_organisation_id=".$data['organisation_id']." ) as total_sum"),
			DB::RAW("(SELECT COALESCE(SUM(final_charge),0) FROM payments WHERE order_id != 0 AND seller_user_type_id=".$data['seller_user_type_id']." AND updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' AND payment_status='Completed' AND payment_type='Card' AND seller_organisation_id=".$data['organisation_id'].") as completed_card_sum"),
			DB::RAW("(SELECT COALESCE(SUM(final_charge),0) FROM payments WHERE order_id != 0 AND seller_user_type_id=".$data['seller_user_type_id']." AND updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' AND payment_status='Completed' AND payment_type='Cash' AND seller_organisation_id=".$data['organisation_id'].") as completed_cash_sum"),
			DB::RAW("(SELECT COALESCE(SUM(final_charge),0) FROM payments WHERE order_id != 0 AND seller_user_type_id=".$data['seller_user_type_id']." AND updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' AND payment_status='Completed' AND payment_type='eToken' AND seller_organisation_id=".$data['organisation_id'].") as completed_etoken_sum")
		);

		return $sum->first();

	}

////////////////////			Single Driver Payments 		///////////////////////////
public static function driver_orders_payment($data)
	{

		$payments = Payment::select("*",
			DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
		);

		$payments->where('order_id','!=',0);
		$payments->where('seller_user_detail_id', $data['user_detail_id']);
		//$payments->where('seller_user_type_id',$data['seller_user_type_id']);

		$payments->whereBetween('updated_at', [$data['starting_dt'], $data['ending_dt']]);

		$payments->with([
			'payer',
			'payer_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			/////////////		Order 	///////////////////////
			'order' => function($q4) use ($data) {

				$q4->with([

					/////////////////		Category 	/////////////////////////////////
			'category_brand_product' => function($q41) use ($data) {

				$q41->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);
			},
			'category_brand' => function($q42) use ($data) {

				$q42->join('category_brand_details', 'category_brand_details.category_brand_id', 'category_brands.category_brand_id')->where('category_brand_details.language_id', $data['admin_language_id']);
			},
			'category' => function($q43) use ($data) {

				$q43->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);
			},
					/////////////////		Category 	/////////////////////////////////

				]);

			}
			/////////////		Order 	///////////////////////

		]);

		return $payments->get();

	}

////////////////		Org Payment Sums 		/////////////////////////////////////////////
public static function driver_pay_sums($data)
	{

		$sum = Admin::select("*",
			DB::RAW("(SELECT COALESCE(SUM(final_charge),0) FROM payments WHERE order_id != 0 AND updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' AND seller_user_detail_id=".$data['user_detail_id']." ) as total_sum"),
			DB::RAW("(SELECT COALESCE(SUM(final_charge),0) FROM payments WHERE order_id != 0 AND updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' AND payment_status='Completed' AND payment_type='Card' AND seller_user_detail_id=".$data['user_detail_id'].") as completed_card_sum"),
			DB::RAW("(SELECT COALESCE(SUM(final_charge),0) FROM payments WHERE order_id != 0 AND updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' AND payment_status='Completed' AND payment_type='Cash' AND seller_user_detail_id=".$data['user_detail_id'].") as completed_cash_sum"),
			DB::RAW("(SELECT COALESCE(SUM(final_charge),0) FROM payments WHERE order_id != 0 AND updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."' AND payment_status='Completed' AND payment_type='eToken' AND seller_user_detail_id=".$data['user_detail_id'].") as completed_etoken_sum")
		);

		return $sum->first();

	}

////////////////////		Admin Etokens Payments 		//////////////////////////////
public static function admin_etoken_payments($data)
	{
	
	$payments = Payment::select('*',
	DB::RAW("( date_format(CONVERT_TZ(updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
	);

////////////////		Filters 	///////////////////////////////////
	if(isset($data['seller_organisation_id']))
		$payments->where('seller_organisation_id', $data['seller_organisation_id']);
	
	if(isset($data['customer_user_detail_id']))
		$payments->where('customer_user_detail_id', $data['customer_user_detail_id']);

	if(isset($data['seller_user_detail_id']))
		$payments->where('seller_user_detail_id', $data['seller_user_detail_id']);	
	////////////////		Filters 	///////////////////////////////////

	$payments->where('order_id','=',0)->where('organisation_coupon_user_id','!=',0);

	$payments->whereBetween('updated_at', [$data['starting_dt'], $data['ending_dt']]);

	$payments->with([
		'payer' => function($q0) {
			$q0->select('user_id', 'name','phone_code', 'phone_number');
		},
		'payer_detail' => function($q1) {
			$q1->select("user_detail_id", "user_id", "profile_pic",
				DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);
		},
		'seller_org' => function($q2) {
			$q2->select('organisation_id', 'name', 'username', 'phone_code', 'phone_number', 'image',
				DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
			);
		},

		'purchased_etokens' => function($q3) use ($data) {
			$q3->select('organisation_coupon_user_id', 'category_id', 'category_brand_id', 'category_brand_product_id', 'bottle_quantity', 'quantity', 'quantity_left', 'address'
			);
			$q3->with([
		/////////////////		Category 	/////////////////////////////////
				'category_brand_product' => function($q31) use ($data) {
					$q31->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'category_brand_products.category_brand_product_id')->where('category_brand_product_details.language_id', $data['admin_language_id']);
				},
				'category_brand' => function($q42) use ($data) {
					$q42->join('category_brand_details', 'category_brand_details.category_brand_id', 'category_brands.category_brand_id')->where('category_brand_details.language_id', $data['admin_language_id']);
				},
				'category' => function($q43) use ($data) {
					$q43->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);
				},
		/////////////////		Category 	/////////////////////////////////
			]);
		}

	]);

	return $payments->get();

	}

//////////////////////		Etoken Payments Sum 		//////////////////////////
public static function admin_etoken_pay_sums($data)
	{

		$pending_sum = "(P1.order_id = 0) AND (P1.organisation_coupon_user_id != 0) AND (P1.payment_status='Pending') AND (P1.updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."')";
		$completed_sum = "(P2.order_id = 0) AND (P2.organisation_coupon_user_id != 0) AND (P2.payment_status='Completed') AND (P2.updated_at BETWEEN '".$data['starting_dt']."' AND '".$data['ending_dt']."')";

		if(isset($data['seller_organisation_id']))
			{
				$pending_sum = $pending_sum.' AND (P1.seller_organisation_id='.$data['seller_organisation_id'].')';
				$completed_sum = $completed_sum.' AND (P2.seller_organisation_id='.$data['seller_organisation_id'].')';
			}

		if(isset($data['customer_user_detail_id']))
			{
				$pending_sum = $pending_sum.' AND (P1.customer_user_detail_id='.$data['customer_user_detail_id'].')';
				$completed_sum = $completed_sum.' AND (P2.customer_user_detail_id='.$data['customer_user_detail_id'].')';
			}

		if(isset($data['seller_user_detail_id']))
			{
				$pending_sum = $pending_sum.' AND (P1.seller_user_detail_id='.$data['seller_user_detail_id'].')';
				$completed_sum = $completed_sum.' AND (P2.seller_user_detail_id='.$data['seller_user_detail_id'].')';
			}

		$sum = Admin::select(
			DB::RAW("(SELECT COALESCE(SUM(final_charge),0) FROM payments P1 WHERE ".$pending_sum.") as pending_sum"),
			DB::RAW("(SELECT COALESCE(SUM(final_charge),0) FROM payments P2 WHERE ".$completed_sum.") as completed_sum")			
		);

		return $sum->first();

	}


}
