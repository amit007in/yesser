<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 05 Sep 2018 08:00:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;
/**
 * Class PaymentLedger
 * 
 * @property int $payment_ledger_id
 * @property int $organisation_id
 * @property int $user_id
 * @property int $user_detail_id
 * @property int $user_type_id
 * @property string $amount
 * @property string $type
 * @property string $description
 * @property string $deleted
 * @property \Carbon\Carbon $ledger_dt
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class PaymentLedger extends Eloquent
{
	protected $primaryKey = 'payment_ledger_id';

	protected $casts = [
		'organisation_id' => 'int',
		'user_id' => 'int',
		'user_detail_id' => 'int',
		'user_type_id' => 'int'
	];

	protected $fillable = [
		'organisation_id',
		'user_id',
		'user_detail_id',
		'user_type_id',
		'amount',
		'type',
		'description',
		'ledger_dt',
		'deleted'
	];

	public function organisation()
		{
			return $this->belongsTo(\App\Models\Organisation::class, 'organisation_id', 'organisation_id');
		}

	public function user()
		{
			return $this->belongsTo(\App\Models\User::class, 'user_id', 'user_id');
		}

	public function user_detail()
		{
			return $this->belongsTo(\App\Models\UserDetail::class, 'user_detail_id', 'user_detail_id');
		}

	public function user_type()
		{
			return $this->belongsTo(\App\Models\UserType::class, 'user_type_id', 'user_type_id');
		}

//////////////////////		Admin All Ledgers 	////////////////////////////////////////
public static function admin_all_ledgers($data)
	{

		$ledgers = PaymentLedger::select("*",
	DB::RAW("( date_format(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
	DB::RAW("( date_format(CONVERT_TZ(ledger_dt,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as ledger_dtz"),
	DB::RAW("( date_format(CONVERT_TZ(ledger_dt,'+00:00','".$data['timezonez']."'),'%Y-%m-%d %h:%i:%s') ) as ledger_dtzt")
		);

		$ledgers->where('deleted','0');

		if(isset($data['organisation_id']))
			$ledgers->where('organisation_id', $data['organisation_id']);
		else if(isset($data['user_detail_id']))
			$ledgers->where('user_detail_id', $data['user_detail_id']);

		$ledgers->with([

			'organisation' => function($q1) {

				$q1->select('*',
					DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
				);

			},

			'user_detail' => function($q1) {

				$q1->select("*",
					DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
				);

			},

			'user',

			'user_type'

		]);

		$ledgers->whereBetween('updated_at', [$data['starting_dt'], $data['ending_dt']]);

		return $ledgers->get();

	}

//////////////////////		Insert New Record 		//////////////////////////////////////////
public static function insert_new_record($data)
	{

		$ledger = new PaymentLedger();

		$ledger->organisation_id = isset($data['organisation_id']) ? $data['organisation_id'] : 0;
		$ledger->user_id = isset($data['user_id']) ? $data['user_id'] : 0;
		$ledger->user_detail_id = isset($data['user_detail_id']) ? $data['user_detail_id'] : 0;
		$ledger->user_type_id = $data['user_type_id'];
		$ledger->amount = $data['amount'];
		$ledger->type = $data['type'];
		$ledger->description = isset($data['description']) ? $data['description'] : "";
		$ledger->deleted = "0";
		$ledger->ledger_dt = $data['ledger_dt'];
		$ledger->created_at = new \DateTime;
		$ledger->updated_at = new \DateTime;

		$ledger->save();

		return $ledger;


 // * @property int $payment_ledger_id
 // * @property int $organisation_id
 // * @property int $user_id
 // * @property int $user_detail_id
 // * @property int $user_type_id
 // * @property string $amount
 // * @property string $type
 // * @property string $description
 // * @property string $deleted
 // * @property \Carbon\Carbon $ledger_dt
 // * @property \Carbon\Carbon $created_at
 // * @property \Carbon\Carbon $updated_at

	}

//////////////////////////

}
