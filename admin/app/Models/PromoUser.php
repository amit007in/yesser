<?php

/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

namespace App\Models;
use Reliese\Database\Eloquent\Model as Eloquent;
use DB;
/**
* Description of PromoUser
*
* @author sachin
*/
class PromoUser extends Eloquent{
   protected $primaryKey = 'id';

   protected $fillable = [
        'id',
        'user_id',
        'coupon_id',
        'category_id',
        'brand_id'
    ];


     public static function save_promocode_users($coupon_id, $data) {
       for ($i=0; $i < count($data['usersId']); $i++) {
            $store = new PromoUser;
            $store->user_id = $data['usersId'][$i];
            $store->category_id = $data['category_id'];
            $store->brand_id = $data['brandids'];
            $store->coupon_id = $coupon_id;
            $store->save();
       }
   }

}