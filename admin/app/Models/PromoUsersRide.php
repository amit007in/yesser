<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;

/**
 * Description of PromoUser
 *
 * @author sachin
 */
class PromoUsersRide extends Eloquent {

    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'user_id',
        'coupon_id',
        'max_rides'
    ];

    public static function save_promocode_users_rides($coupon_id, $data) {
        for ($i = 0; $i < count($data['usersId']); $i++) {
            $promo_rides = new PromoUsersRide;
            $promo_rides->coupon_id = $coupon_id;
            $promo_rides->user_id = $data['usersId'][$i];
            $promo_rides->max_rides = $data['rides_value'];
            $promo_rides->save();
        }
    }

}