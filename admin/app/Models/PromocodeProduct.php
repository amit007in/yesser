<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;
use Reliese\Database\Eloquent\Model as Eloquent;
use DB;
/**
 * Description of PromocodeProducts
 *
 * @author sachin
 */
class PromocodeProduct extends Eloquent{
    protected $primaryKey = 'id';
    
    protected $fillable = [
		'id',
		'product_id',
		'coupon_id'
	];
    
    
      public static function save_promocode_products($coupon_id, $data) { 
        for ($i=0; $i < count($data['productids']); $i++) {
             $store = new PromocodeProduct;
             $store->product_id = $data['productids'][$i];
             $store->category_id = $data['category_id'];
             $store->brand_id = $data['brandids'];
             $store->coupon_id = $coupon_id;  
             $store->save();
        }   
    }
    
}
