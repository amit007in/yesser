<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 Aug 2018 09:00:55 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use DB;

/**
 * Class ReleaseOffer
 * 
 *
 * @package App\Models
 */
class ReleaseOffer extends Eloquent
{
    protected $primaryKey = 'release_offer_id';
	protected $fillable = [
		'driver_id',
		'start_address',
		'end_address',
		'margin',
		'offer_price',
		'status',
		'product_id',
		'expire_date',
		'offer_expire'
    ];
    
    /**
    *@role Get All Offers From Driver
    *@author Raghav Goyal
    *@date 28-June-2019
    */
    public static function admin_release_offer_listing($data)
    {

        $offers = ReleaseOffer::select("release_offers.*,users.name,user_details.profile_pic");
        $offers->select("*",
			DB::RAW("( date_format(CONVERT_TZ(release_offers.created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz")
			//DB::RAW("( CONCAT('".env('SOCKET_URL')."/public/Uploads/',user_details.profile_pic) ) as image_url")
		);
        $offers->join('user_details', 'user_details.user_id', 'release_offers.driver_id');
        $offers->join('users', 'users.user_id', 'release_offers.driver_id');
		$offers->where('user_details.user_type_id', '2');
		$offers->orderBy('release_offers.release_offer_id','DESC');
        return $offers->paginate();
    }


}