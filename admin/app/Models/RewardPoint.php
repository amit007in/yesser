<?php

/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

namespace App\Models;
use Reliese\Database\Eloquent\Model as Eloquent;
use DB;
/**
* Description of RewardPoint
*
* @author Raghav
*/

class RewardPoint extends Eloquent
{
  protected $primaryKey = 'reward_id';

  protected $fillable = [
    'send_invite',
    'add_point',
    'status'
  ];

  /**
  * @role Get All RewardPoint
  * @Method POST
  * @author Raghav
  * @date 05-Aug-2019
  */
  public static function admin_get_lists($data)
  {
    $RewardPoint = RewardPoint::select('*',
      DB::RAW("( date_format(CONVERT_TZ(updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_atz")
    );
    return $RewardPoint->get();
  }

  /**
  * @role Add Reward Point
  * @Method POST
  * @author Raghav Goyal
  */
  public static function add_new_record($data)
  {
    $RewardPoint                            = new RewardPoint();
    $RewardPoint->send_invite               = $data['send_invite'];
    $RewardPoint->add_point                 = $data['reward_point'];
    $RewardPoint->status                    = 0;
    $RewardPoint->created_at                = new \DateTime;
    $RewardPoint->updated_at                = new \DateTime;
    $RewardPoint->save();
    return $RewardPoint;
  }

  /**
   * @role Get Reward Details
   * @Method Get
   * @author Raghav Goyal
   */
  public static function single_reward_details($data)
  {
    return RewardPoint::where('reward_id', $data['reward_id'])
    ->select("*")
    ->first();
  }

  /**
   * @role Get Reward Details
   * @Method Get
   * @author Raghav Goyal
   */
  public static function reward_details($data)
  {
    return RewardPoint::where('reward_id','!=', $data['reward_id'])->where('send_invite',$data['send_invite'])
    ->select("*")
    ->first();
  }

}