<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 04 Aug 2018 05:25:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class User
 * 
 * @property int $user_id
 * @property int $organisation_id
 * @property string $stripe_customer_id
 * @property string $stripe_connect_id
 * @property string $name
 * @property string $email
 * @property string $phone_code
 * @property int $phone_number
 * @property String $address
 * @property Float $address_latitude
 * @property Float $address_longitude
 * @property string $stripe_connect_token
 * @property string $blocked
 * @property string $bottle_charge
 * @property string $buraq_percentage
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $user_bank_details
 * @property \Illuminate\Database\Eloquent\Collection $user_cards
 * @property \Illuminate\Database\Eloquent\Collection $user_details
 * @property \Illuminate\Database\Eloquent\Collection $user_driver_detail_products
 *
 * @package App\Models
 */
class User extends Eloquent
{
	protected $primaryKey = 'user_id';

	protected $casts = [
		'organisation_id' => 'int',
		'phone_number' => 'int',
		'address_latitude' => 'float',
		'address_longitude' => 'float'
	];

	protected $hidden = [
		'stripe_connect_token'
	];

	protected $fillable = [
		'organisation_id',
		'stripe_customer_id',
		'stripe_connect_id',
		'name',
		'email',
		'phone_code',
		'phone_number',
		'address',
		'address_latitude',
		'address_longitude',
		'stripe_connect_token',
		'created_by',
		'bottle_charge',
		'blocked'
	];

	public function admin_contactuses()
		{
			return $this->hasMany(\App\Models\AdminContactus::class, 'user_id', 'user_id');
		}

	public function admin_user_detail_logins()
		{
			return $this->hasMany(\App\Models\AdminUserDetailLogin::class, 'user_id', 'user_id');
		}

	public function order_requests()
		{
			return $this->hasMany(\App\Models\OrderRequest::class, 'driver_user_id');
		}

	public function organisations()
		{
			return $this->belongsToMany(\App\Models\Organisation::class, 'organisation_coupon_users')
						->withPivot('organisation_coupon_user_id', 'user_detail_id', 'organisation_coupon_id', 'category_id', 'category_brand_id', 'category_brand_product_id', 'price', 'quantity', 'quantity_left', 'expiry_days', 'expiries_at')
						->withTimestamps();
		}

	public function organisation_coupons()
		{
			return $this->belongsToMany(\App\Models\OrganisationCoupon::class, 'organisation_coupon_users')
						->withPivot('organisation_coupon_user_id', 'organisation_id', 'user_detail_id', 'category_id', 'category_brand_id', 'category_brand_product_id', 'price', 'quantity', 'quantity_left', 'expiry_days', 'expiries_at')
						->withTimestamps();
		}

	public function user_bank_details()
		{
			return $this->hasMany(\App\Models\UserBankDetail::class, 'user_id', 'user_id');
		}

	public function user_cards()
		{
			return $this->hasMany(\App\Models\UserCard::class, 'user_id', 'user_id');
		}

	public function user_detail()
		{
			return $this->hasOne(\App\Models\UserDetail::class, 'user_id', 'user_id');
		}
	public function user_details()
		{
			return $this->hasMany(\App\Models\UserDetail::class, 'user_id', 'user_id');
		}

	public function user_driver_detail_products()
		{
			return $this->hasMany(\App\Models\UserDriverDetailProduct::class, 'user_id', 'user_id');
		}

	public function user_driver_detail_services()
	{
		return $this->hasMany(\App\Models\UserDriverDetailService::class, 'user_id', 'user_id');
	}

////////////////////////////		Insert New Record 		//////////////////////////////////////
public static function insert_new_record($data)
	{

		$user = new User();

		$user->organisation_id = isset($data['organisation_id']) ? $data['organisation_id'] : 0;
		$user->stripe_customer_id = isset($data['stripe_customer_id']) ? $data['stripe_customer_id'] : '';
		$user->stripe_connect_id = isset($data['stripe_connect_id']) ? $data['stripe_connect_id'] : '';
		$user->name = $data['name'];
		$user->email = $data['email'];
		$user->phone_code = isset($data['phone_code']) ? $data['phone_code'] : '+968';
		$user->phone_number = $data['phone_number'];

		$user->address = isset($data['address']) ? $data['address'] : "";
		$user->address_latitude = isset($data['address_latitude']) ? $data['address_latitude'] : 21.47;
		$user->address_longitude = isset($data['address_longitude']) ? $data['address_longitude'] : 55.97;
		$user->bottle_charge = isset($data['bottle_charge']) ? $data['bottle_charge'] : 0;
		$user->buraq_percentage = isset($data['buraq_percentage']) ? $data['buraq_percentage'] : 10;

		$user->stripe_connect_token = isset($data['strip_connect_token']) ? $data['strip_connect_token'] : '';
		$user->created_by = isset($data['created_by']) ? $data['created_by'] : 'App';

		$user->save();

		return $user;

	}

////////////////////////////		Panel Login Get 	//////////////////////////////////////////
public static function panel_login_get($data)
	{
	
		$user = User::where('phone_number',$data['phone_number']);

		// if(isset($data['user_type_id']))
		// 	$user->where('user_type_id',$data['user_type_id']);

		$user->with([

			'user_detail' => function($q1) use ($data) {

				$q1->where('user_type_id',$data['user_type_id']);

			}

		]);

		return $user->first();

	}

///////////////////////////

}
