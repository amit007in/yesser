<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 04 Aug 2018 05:25:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserCard
 * 
 * @property int $user_card_id
 * @property int $user_id
 * @property string $card_id
 * @property string $fingerprint
 * @property string $last4
 * @property string $brand
 * @property string $is_default
 * @property string $deleted
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UserCard extends Eloquent
{
	protected $primaryKey = 'user_card_id';

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'card_id',
		'fingerprint',
		'last4',
		'brand',
		'is_default',
		'deleted'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'user_id', 'user_id');
	}
}
