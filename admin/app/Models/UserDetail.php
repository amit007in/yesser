<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 04 Aug 2018 05:25:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

use DB;

/**
 * Class UserDetail
 * 
 * @property int $user_detail_id
 * @property int $user_id
 * @property int $user_type_id
 * @property int $language_id
 * @property int $category_id
 * @property int $category_brand_id
 * @property int $organisation_id
 * @property string $otp
 * @property string $password
 * @property string $profile_pic
 * @property string $access_token
 * @property float $latitude
 * @property float $longitude
 * @property string $timezone
 * @property string $timezonez
 * @property string $socket_id
 * @property string $fcm_id
 * @property string $created_by
 * @property string $blocked
 * @property string $forgot_token
 * @property \Carbon\Carbon $forgot_token_validity
 * @property string $mulkiya_number
 * @property string $mulkiya_front
 * @property string $mulkiya_back
 * @property \Carbon\Carbon $mulkiya_validity
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 * @property \App\Models\UserType $user_type
 * @property \App\Models\Language $language
 * @property \App\Models\Category $category
 * @property \App\Models\CategoryBrand $category_brand
 * @property \Illuminate\Database\Eloquent\Collection $user_driver_detail_products
 *
 * @package App\Models
 */
class UserDetail extends Eloquent
{
	protected $primaryKey = 'user_detail_id';

	protected $casts = [
		'user_id' => 'int',
		'user_type_id' => 'int',
		'language_id' => 'int',
		'category_id' => 'int',
		'category_brand_id' => 'int',
		'organisation_id' => 'int',
		'latitude' => 'float',
		'longitude' => 'float'
	];

	protected $dates = [
		'forgot_token_validity'
	];

	protected $hidden = [
		'password',
		'forgot_token'
	];

	protected $fillable = [
		'user_id',
		'user_type_id',
		'language_id',
		'category_id',
		'category_brand_id',
		'organisation_id',
		'otp',
		'password',
		'profile_pic',
		'access_token',
		'latitude',
		'longitude',
		'timezone',
		'timezonez',
		'socket_id',
		'fcm_id',
		'created_by',
		'blocked',
		'forgot_token',
		'forgot_token_validity',
		'mulkiya_number',
		'mulkiya_front',
		'mulkiya_back',
		'mulkiya_validity'
	];

	public function admin_contactuses()
		{
			return $this->hasMany(\App\Models\AdminContactus::class, 'user_detail_id', 'user_detail_id');
		}

	public function admin_user_detail_logins()
		{
			return $this->hasMany(\App\Models\AdminUserDetailLogin::class, 'user_detail_id', 'user_detail_id');
		}

	public function order_requests()
		{
			return $this->hasMany(\App\Models\OrderRequest::class, 'driver_user_detail_id');
		}

	public function organisation_coupon_users()
		{
			return $this->hasMany(\App\Models\OrganisationCouponUser::class);
		}

	public function user_driver_detail_products()
		{
			return $this->hasMany(\App\Models\UserDriverDetailProduct::class, 'user_detail_id', 'user_detail_id');
		}

	public function user_driver_detail_services()
		{
			return $this->hasMany(\App\Models\UserDriverDetailService::class, 'user_detail_id', 'user_detail_id');
		}

	public function category()
		{
			return $this->belongsTo(\App\Models\Category::class, 'category_id', 'category_id');
		}

	public function category_brand()
		{
			return $this->belongsTo(\App\Models\CategoryBrand::class, 'category_brand_id', 'category_brand_id');
		}

	public function organisation()
		{
			return $this->belongsTo(\App\Models\Organisation::class, 'organisation_id', 'organisation_id');
		}

	public function user()
		{
			return $this->belongsTo(\App\Models\User::class, 'user_id', 'user_id');
		}

	public function user_type()
		{
			return $this->belongsTo(\App\Models\UserType::class, 'user_type_id', 'user_type_id');
		}

	public function language()
		{
			return $this->belongsTo(\App\Models\Language::class, 'language_id', 'language_id');
		}


/////////////////////////		Insert New Record 	/////////////////////////////
public static function insert_new_record($data)
	{

		$udetail = new UserDetail();

		$udetail->user_id = $data['user_id'];
		$udetail->user_type_id = $data['user_type_id'];
		$udetail->language_id = isset($data['language_id']) ? $data['language_id'] : 1;
		$udetail->category_id = isset($data['category_id']) ? $data['category_id'] : 0;
		$udetail->category_brand_id = isset($data['category_brand_id']) ? $data['category_brand_id'] : 0;
		$udetail->organisation_id = isset($data['organisation_id']) ? $data['organisation_id'] : 0;

		$udetail->otp = isset($data['otp']) ? $data['otp'] : '';
		$udetail->password = isset($data['password']) ? \Hash::make($data['password']) : '';
		$udetail->profile_pic = $data['profile_pic_name'];
		$udetail->access_token = isset($data['access_token']) ? $data['access_token'] : '';
		$udetail->latitude = isset($data['latitude']) ? $data['latitude'] : 0;
		$udetail->longitude = isset($data['longitude']) ? $data['longitude'] : 0;
		$udetail->timezone = isset($data['timezone']) ? $data['timezone'] : 'Asia/Mascat';
		$udetail->timezonez = isset($data['timezonez']) ? $data['timezonez'] : '+04:00';
		$udetail->socket_id = isset($data['socket_id']) ? $data['socket_id'] : '';
		$udetail->fcm_id = isset($data['fcm_id']) ? $data['fcm_id'] : '';
		$udetail->forgot_token = '';
		$udetail->forgot_token_validity = NULL;
		$udetail->created_by = isset($data['created_by']) ? $data['created_by'] : 'App';
		$udetail->mulkiya_number = $data['mulkiya_number'];
		$udetail->mulkiya_front = $data['mulkiya_front_name'];
		$udetail->mulkiya_back = $data['mulkiya_back_name'];
		$udetail->mulkiya_validity = $data['mulkiya_validity'];

		$udetail->maximum_rides = $data['maximum_rides'];
		$udetail->blocked = "0";

		$udetail->save();

		return $udetail;

	}

//////////////////////		Admin Driver Detail Update 		////////////////////////////
public static function admin_driver_detail_update_get($data)
	{

		$driver = UserDetail::where('user_detail_id', $data['user_detail_id']);

		$driver->join('users','users.user_id','user_details.user_id');

		$driver->select('*',
			DB::RAW("(CASE WHEN profile_pic='' THEN '".env('SERVICE_DEFAULT_IMAGE')."' ELSE CONCAT('".env('SOCKET_URL')."/Uploads/',profile_pic) END) as profile_pic_url"),
			DB::RAW("( CASE WHEN mulkiya_front IS NULL THEN '' ELSE CONCAT('".env('RESIZE_URL')."', mulkiya_front) END) as mulkiya_front_url"),
			DB::RAW("( CASE WHEN mulkiya_back IS NULL THEN '' ELSE CONCAT('".env('RESIZE_URL')."', mulkiya_back) END) as mulkiya_back_url"),
			DB::RAW("( date_format(CONVERT_TZ(mulkiya_validity,'+00:00','".$data['timezonez']."'),'%Y-%m-%d %H:%i:%s') ) as mulkiya_validity")
		);

		$driver->with([

			'organisation' => function($q1) {

				$q1->select('*',
					DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
				);

			},

			'category_brand' => function($q2) {
				$q2->with(['category_brand_details']);
			}

		]);

		return $driver->first();

	}

//////////////////////		Admin Customer Listings 	/////////////////////////
public static function admin_customers($data)
{

	$custs = UserDetail::where('user_type_id',1);

	$custs->whereBetween('created_at', [$data['starting_dt'], $data['ending_dt']]);


	$custs->select('*',
			DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url"),
			DB::RAW("( date_format(CONVERT_TZ(user_details.created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
			DB::RAW("(SELECT COUNT(*) FROM orders as o WHERE o.customer_user_detail_id=user_details.user_detail_id) as order_counts"),
			DB::RAW("(SELECT COUNT(*) FROM order_ratings WHERE customer_user_detail_id = user_details.user_detail_id OR driver_user_detail_id=user_detail_id) as review_counts")
		);

	$custs->with([

		'user'

	]);

	return $custs->get();

}

/////////////////////////		Single Custoemr Details 	///////////////////////
public static function cust_details($data)
{

	$cust = UserDetail::where('user_type_id',1)->where('user_detail_id',$data['user_detail_id']);

	$cust->select('*',
			DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url")
			// DB::RAW("(SELECT COUNT(*) FROM orders as o WHERE o.customer_user_detail_id=user_details.user_detail_id) as order_counts"),
			// DB::RAW("(SELECT COUNT(*) FROM order_ratings WHERE customer_user_detail_id = user_details.user_detail_id OR driver_user_detail_id=user_detail_id) as review_counts")
		);

	$cust->with([
		'user'
	]);

	return $cust->first();

}

//////////////////////////	Admin Get Drivers Map 	///////////////////////////////
public static function admin_drivers_map_get($data)
	{

		$drivers = UserDetail::whereIn('category_id', $data['category_ids'])->where('latitude','!=',0)->where('longitude', '!=', 0);

		$drivers->with([
			'user'
		]);

		return $drivers->get();

	}

///////////////////////////		Admin Drivers Listings 	//////////////////////////////
public static function admin_drivers_listing($data)
{

	$drivers = UserDetail::select("*",
		DB::RAW("(CASE WHEN profile_pic='' THEN '".env('SERVICE_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url"),
		DB::RAW("( date_format(CONVERT_TZ(user_details.created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
		'user_details.blocked as blocked',
		'user_details.created_by as created_by',
		DB::RAW("(SELECT COUNT(*) FROM orders as o WHERE o.driver_user_detail_id=user_details.user_detail_id) as order_counts"),
		DB::RAW("(SELECT COUNT(*) FROM order_ratings WHERE order_ratings.driver_user_detail_id=user_details.user_detail_id) as review_counts"),
		DB::RAW("(SELECT COUNT(*) FROM payment_ledgers as pl WHERE pl.user_detail_id=user_details.user_detail_id AND deleted='0') as ledger_counts"),
		DB::RAW("(SELECT COUNT(*) FROM user_driver_detail_products as uddp WHERE uddp.user_detail_id=user_details.user_detail_id AND deleted='0') as product_counts"),
		DB::RAW("(SELECT COUNT(*) FROM admin_user_detail_logins as audl WHERE audl.user_detail_id=user_details.user_detail_id AND user_type_id='2') as login_counts")
	);
	$drivers->join('users','users.user_id','user_details.user_id');

	$drivers->with([

		'category' => function($q6) use ($data) {

				$q6->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);
			},

	]);

	if(isset($data['organisation_id']))
		$drivers->where('user_details.organisation_id',$data['organisation_id']);

//	$drivers->where('user_details.category_id', '!=', 0);
	$drivers->whereIn('user_type_id', $data['user_type_ids']);
	$drivers->whereBetween('user_details.created_at', [$data['starting_dt'], $data['ending_dt']]);

	return $drivers->get();

}

////////////////		Admin Get Category/Org Drivers 		///////////////////
public static function admin_get_cat_drivers($data)
	{

		$drivers = UserDetail::whereIn('user_type_id', $data['user_type_ids']);

		$drivers->where('category_id',$data['category_id']);

		if(isset($data['organisation_id']))
			$drivers->where('user_details.organisation_id', $data['organisation_id']);

		$drivers->whereBetween('user_details.created_at', [$data['starting_dt'], $data['ending_dt']]);

		$drivers->join('users','users.user_id','user_details.user_id');

		$drivers->select('*', 'user_details.blocked as blocked', 'user_details.created_by as created_by',

		DB::RAW("(CASE WHEN profile_pic='' THEN '".env('SERVICE_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url"),
		DB::RAW("( date_format(CONVERT_TZ(user_details.created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
		'user_details.blocked as blocked',
		'user_details.created_by as created_by',
		DB::RAW("(SELECT COUNT(*) FROM orders as o WHERE o.driver_user_detail_id = user_details.user_detail_id) as order_counts"),
		DB::RAW("(SELECT COUNT(*) FROM order_ratings WHERE order_ratings.driver_user_detail_id = user_details.user_detail_id) as review_counts"),
		DB::RAW("(SELECT COUNT(*) FROM user_driver_detail_products as uddp WHERE uddp.user_detail_id=user_details.user_detail_id AND deleted='0') as product_counts"),
		DB::RAW("(SELECT COUNT(*) FROM admin_user_detail_logins as audl WHERE audl.user_detail_id=user_details.user_detail_id AND user_type_id='2') as login_counts")
		);

		return $drivers->get();

	}

////////////////////		Drivers Listings With Pagination 		//////////////////////////////////
public static function admin_drivers_listings($data,$paginate=null)
	{

		$drivers = UserDetail::whereIn('user_type_id', $data['user_type_ids']);

		if(isset($data['category_id']))
			$drivers->where('category_id',$data['category_id']);

		if(isset($data['organisation_id']))
			$drivers->where('user_details.organisation_id', $data['organisation_id']);

///////////////		Filters 	////////////////////////

			if(isset($data['blocked_check']) && $data['blocked_check'] == 'Active' )
				$drivers->where('user_details.blocked','0');
			elseif(isset($data['blocked_check']) && $data['blocked_check'] == 'Blocked' )
				$drivers->where('user_details.blocked','1');

			if(isset($data['approved_check']) && $data['approved_check'] == 'Approved' )
				$drivers->where('user_details.approved','1');
			elseif(isset($data['approved_check']) && $data['approved_check'] == 'Pending' )
				$drivers->where('user_details.approved','0');

			if(isset($data['created_by_check']) && $data['created_by_check'] == 'Admin' )
				$drivers->where('user_details.created_by','Admin');
			elseif(isset($data['created_by_check']) && $data['created_by_check'] == 'Company' )
				$drivers->where('user_details.created_by','Org');
			elseif(isset($data['created_by_check']) && $data['created_by_check'] == 'App' )
				$drivers->where('user_details.created_by','App');

			if(isset($data['type_check']) && $data['type_check'] == 'Single')
				$drivers->where('user_details.organisation_id',0);
			elseif(isset($data['type_check']) && $data['type_check'] == 'Company')
				$drivers->where('user_details.organisation_id','!=',0);

			if(isset($data['online_check']) && $data['online_check'] == 'Online' )
				$drivers->where('user_details.online_status','1');
			elseif(isset($data['online_check']) && $data['online_check'] == 'Offline' )
				$drivers->where('user_details.online_status','0');

			if(isset($data['category_id_check']) && $data['category_id_check'] != 0)
				$drivers->where('category_id',$data['category_id_check']);

			$drivers->whereBetween('user_details.created_at', [$data['starting_dt'], $data['ending_dt']]);

			if(isset($data['search']) && $data['search'] != '')
				$drivers->whereRaw("( (name LIKE '%".$data['search']."%') OR (mulkiya_number LIKE '%".$data['search']."%') OR (phone_number LIKE '%".$data['search']."%') )");
///////////////		Filters 	////////////////////////
		

		$drivers->join('users','users.user_id','user_details.user_id');

		$drivers->select('*', 'user_details.blocked as blocked', 'user_details.created_by as created_by',

		DB::RAW("(CASE WHEN profile_pic='' THEN '".env('SERVICE_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url"),
		DB::RAW("( date_format(CONVERT_TZ(user_details.created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
		'user_details.blocked as blocked',
		'user_details.created_by as created_by',
		DB::RAW("(SELECT COUNT(*) FROM orders as o WHERE o.driver_user_detail_id = user_details.user_detail_id) as order_counts"),
		DB::RAW("(SELECT COUNT(*) FROM order_ratings WHERE order_ratings.driver_user_detail_id = user_details.user_detail_id) as review_counts"),
		DB::RAW("(SELECT COUNT(*) FROM payment_ledgers as pl WHERE pl.user_detail_id=user_details.user_detail_id AND deleted='0') as ledger_counts"),
		DB::RAW("(SELECT COUNT(*) FROM user_driver_detail_products as uddp WHERE uddp.user_detail_id=user_details.user_detail_id AND deleted='0') as product_counts"),
		DB::RAW("(SELECT COUNT(*) FROM admin_user_detail_logins as audl WHERE audl.user_detail_id=user_details.user_detail_id AND user_type_id='2') as login_counts")
		);

		$drivers->with([

			'category' => function($q6) use ($data) {

					$q6->join('category_details', 'category_details.category_id', 'categories.category_id')->where('category_details.language_id', $data['admin_language_id']);
				},

			'organisation' => function($q1) {

				$q1->select('*'
					//DB::RAW("( CONCAT('".env('RESIZE_URL')."',image) ) as image_url")
				);

			},

			'user_driver_detail_products' => function($q2) use ($data) {

				$q2->join('category_brand_product_details', 'category_brand_product_details.category_brand_product_id', 'user_driver_detail_products.category_brand_product_id')
				->where('category_brand_product_details.language_id', '=', $data['admin_language_id']);
				$q2->select('user_id', 'user_detail_id', 'name');

			}


		]);

		$drivers->orderBy('user_detail_id', 'DESC');

		if($paginate){
			$drivers->with([
					'category_brand' => function($q7) {
						$q7->with(['category_brand_details']);
					}
				]);
			return $drivers->get();
		}else{
			return $drivers->paginate();
		}

	}


////////////////////		Drivers Listings With Pagination 		//////////////////////////////////
public static function admin_cust_listings($data)
	{


		$custs = UserDetail::where('user_type_id',1);

		$custs->whereBetween('user_details.created_at', [$data['starting_dt'], $data['ending_dt']]);

///////////////		Filters 	////////////////////////
		if(isset($data['blocked_check']) && $data['blocked_check'] == 'Active' )
			$custs->where('user_details.blocked','0');
		elseif(isset($data['blocked_check']) && $data['blocked_check'] == 'Blocked' )
			$custs->where('user_details.blocked','1');

		if(isset($data['search']) && $data['search'] != '')
			$custs->whereRaw("( (name LIKE '%".$data['search']."%') OR (phone_number LIKE '%".$data['search']."%') )");
///////////////		Filters 	////////////////////////

		$custs->join('users','users.user_id','user_details.user_id');

		$custs->select('*', 'user_details.blocked as blocked',
			DB::RAW("(CASE WHEN profile_pic='' THEN '".env('CUSTOMER_DEFAULT_IMAGE')."' ELSE CONCAT('".env('RESIZE_URL')."',profile_pic) END) as profile_pic_url"),
			DB::RAW("( date_format(CONVERT_TZ(user_details.created_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as created_atz"),
			DB::RAW("(SELECT COUNT(*) FROM orders as o WHERE o.customer_user_detail_id=user_details.user_detail_id) as order_counts"),
			DB::RAW("(SELECT COUNT(*) FROM order_ratings WHERE customer_user_detail_id = user_details.user_detail_id OR driver_user_detail_id=user_detail_id) as review_counts")
		);

		$custs->orderBy('user_detail_id', 'DESC');

		if(isset($data['not_paginate']) && $data['not_paginate'] == 1)
			return $custs->get();
		else
			return $custs->paginate();

	}
        
        public static function cust_listings($data)
    {
        $custs = UserDetail::where('user_type_id',1);
        ///////////////        Filters     ////////////////////////
        if(isset($data['blocked_check']) && $data['blocked_check'] == 'Active' )
            $custs->where('user_details.blocked','0');
        ///////////////        Filters     ////////////////////////
        $custs->join('users','users.user_id','user_details.user_id');
        $custs->select('*');
        $custs->orderBy('user_detail_id', 'DESC');
        return $custs->get();
    }


    public static function getUserList($data)
    {
        $categorybrand = DB::table('promo_users')->select('user_id')->where('coupon_id',$data['user_detail_id'])->get();
        $rowData = array();
        if(count($categorybrand) > 0){
            for($i = 0;$i<count($categorybrand);$i++)
            {
                $rowData[] = $categorybrand[$i]->user_id;
            }
            $userslit = $rowData;
        }else{
            $userslit = array();
        }
        return $userslit;
    }

///////////////////////////		AJax Custoemrs Listings 		//////////////////////////////
public static function admin_cust_ajax_list($data)
	{

		//$custs = UserDetail::where('user_type_id')

	}

/////////////////////////////		Org Drivers Specific Service 		///////////////////////
public static function org_service_drivers($organisation_id, $category_id, $user_type_id)
	{

		$drivers = UserDetail::where('category_id', $category_id)->where('user_details.organisation_id', $organisation_id)->where('user_details.blocked','0')->where('user_type_id',$user_type_id);

		$drivers->join('users', 'users.user_id','user_details.user_id');

		$drivers->select('user_details.user_id', 'user_detail_id', 'name', 'phone_code', 'phone_number');

		return $drivers->get();

	}

/////////////////		Org Service Area Drivers List with Check 		////////////////////////
public static function org_service_area_drivers($organisation_id, $category_id, $user_type_id, $organisation_area_id)
	{

		$drivers = UserDetail::where('category_id', $category_id)->where('user_details.organisation_id', $organisation_id)->where('user_details.blocked','0')->where('user_type_id',$user_type_id);

		$drivers->join('users', 'users.user_id','user_details.user_id');
		
		//$drivers->leftJoin('organisation_area_drivers', 'organisation_area_drivers.user_detail_id', 'user_details.user_detail_id')->where('organisation_area_id', $organisation_area_id);
		$drivers->leftJoin('organisation_area_drivers', function($q) use ($organisation_area_id) {

			$q->on('organisation_area_drivers.user_detail_id','=', 'user_details.user_detail_id')->where('organisation_area_id', $organisation_area_id);

		});

		$drivers->select('user_details.user_id', 'user_details.user_detail_id', 'name', 'phone_code', 'phone_number', 
			DB::RAW("(CASE WHEN organisation_area_drivers.organisation_area_driver_id IS NULL THEN 0 ELSE 1 END) as selected")
		);

		return $drivers->get();

	}

/////////////////


}
