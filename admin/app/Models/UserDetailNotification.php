<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 17 Aug 2018 05:38:38 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserDetailNotification
 * 
 * @property int $user_detail_notification_id
 * @property int $user_id
 * @property int $user_detail_id
 * @property int $user_type_id
 * @property int $user_organisation_id
 * @property int $sender_user_id
 * @property int $sender_user_detail_id
 * @property int $sender_type_id
 * @property int $sender_organisation_id
 * @property int $order_id
 * @property int $order_request_id
 * @property int $order_rating_id
 * @property string $message
 * @property string $type
 * @property string $is_read
 * @property string $deleted
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class UserDetailNotification extends Eloquent
{
	protected $primaryKey = 'user_detail_notification_id';

	protected $casts = [
		'user_id' => 'int',
		'user_detail_id' => 'int',
		'user_type_id' => 'int',
		'user_organisation_id' => 'int',
		'sender_user_id' => 'int',
		'sender_user_detail_id' => 'int',
		'sender_type_id' => 'int',
		'sender_organisation_id' => 'int',
		'order_id' => 'int',
		'order_request_id' => 'int',
		'order_rating_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'user_detail_id',
		'user_type_id',
		'user_organisation_id',
		'sender_user_id',
		'sender_user_detail_id',
		'sender_type_id',
		'sender_organisation_id',
		'order_id',
		'order_request_id',
		'order_rating_id',
		'message',
		'type',
		'is_read',
		'deleted'
	];
}
