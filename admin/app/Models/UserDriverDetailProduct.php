<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 04 Aug 2018 05:25:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserDriverDetailProduct
 * 
 * @property int $user_driver_detail_product_id
 * @property int $user_id
 * @property int $user_detail_id
 * @property int $category_brand_product_id
 * @property float $alpha_price
 * @property float $price_per_quantity
 * @property float $price_per_distance
 * @property float $price_per_weight
 * @property float $price_per_hr
 * @property float $price_per_sq_mt
 * @property string $deleted
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 * @property \App\Models\UserDetail $user_detail
 * @property \App\Models\UserDetailDriver $user_detail_driver
 * @property \App\Models\CategoryBrandProduct $category_brand_product
 *
 * @package App\Models
 */
class UserDriverDetailProduct extends Eloquent
{
	protected $primaryKey = 'user_driver_detail_product_id';

	protected $casts = [
		'user_id' => 'int',
		'user_detail_id' => 'int',
		'category_brand_product_id' => 'int',
		'alpha_price' => 'float',
		'price_per_quantity' => 'float',
		'price_per_distance' => 'float',
		'price_per_weight' => 'float',
		'price_per_hr' => 'float',
		'price_per_sq_mt' => 'float',
	];

	protected $fillable = [
		'user_id',
		'user_detail_id',
		'category_brand_product_id',
		'deleted',
		'alpha_price',
		'price_per_quantity',
		'price_per_distance',
		'price_per_weight',
		'price_per_hr',
		'price_per_sq_mt'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'user_id', 'user_id');
	}

	public function user_detail()
	{
		return $this->belongsTo(\App\Models\UserDetail::class, 'user_detail_id', 'user_detail_id');
	}

	public function category_brand_product()
	{
		return $this->belongsTo(\App\Models\CategoryBrandProduct::class, 'category_brand_product_id', 'category_brand_product_id');
	}

/////////////////////////		Driver Products Listings 	//////////////////////////////
public static function driver_products_listings($data)
	{

		$products = UserDriverDetailProduct::where('user_detail_id', $data['user_detail_id']);

		$products->with([

			'category_brand_product' => function($q1) {

				$q1->with(['category_brand_product_details']);

			}

		]);

		return $products->get();

	}

/////////////////////		Service Drivers Products 		////////////////////////////////
public static function service_driver_products($data)
	{

		$products = UserDriverDetailProduct::where('user_detail_id', $data['user_detail_id'])->where('deleted', '0');

		$products->with([

			'category_brand_product' => function($q1) {

				$q1->with(['category_brand_product_details']);

			}

		]);

		return $products->get();

	}

/////////////////////

}
