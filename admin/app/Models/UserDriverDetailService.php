<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 18 Aug 2018 06:32:55 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserDriverDetailService
 * 
 * @property int $user_driver_detail_service_id
 * @property int $user_id
 * @property int $user_detail_id
 * @property int $category_id
 * @property string $deleted
 * @property float $buraq_percentage
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 * @property \App\Models\UserDetail $user_detail
 * @property \App\Models\Category $category
 *
 * @package App\Models
 */
class UserDriverDetailService extends Eloquent
{
	protected $primaryKey = 'user_driver_detail_service_id';

	protected $casts = [
		'user_id' => 'int',
		'user_detail_id' => 'int',
		'category_id' => 'int',
		'buraq_percentage' => 'float'
	];

	protected $fillable = [
		'user_id',
		'user_detail_id',
		'category_id',
		'deleted',
		'buraq_percentage'
	];

	public function user()
		{
			return $this->belongsTo(\App\Models\User::class, 'user_id', 'user_id');
		}

	public function user_detail()
		{
			return $this->belongsTo(\App\Models\UserDetail::class, 'user_detail_id', 'user_detail_id');
		}

	public function category()
		{
			return $this->belongsTo(\App\Models\Category::class, 'category_id', 'category_id');
		}

//////////////		Insert New Record 	/////////////////////////////////////
public static function insert_new_record($data)
	{
	
		$uds = new UserDriverDetailService();

		$uds->user_id = $data['user_id'];
		$uds->user_detail_id = $data['user_detail_id'];
		$uds->category_id = $data['category_id'];
		$uds->buraq_percentage = $data['buraq_percentage'];
		$uds->deleted = "0";
		$uds->created_at = new \DateTime;
		$uds->updated_at = new \DateTime;

		$uds->save();

		return $uds;

	}

///////////////

}
