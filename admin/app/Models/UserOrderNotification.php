<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 17 Aug 2018 05:38:38 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserOrderNotification
 * 
 * @property int $user_order_notification_id
 * @property int $order_id
 * @property int $sender_user_detail_id
 * @property int $receiver_user_detail_id
 * @property string $message
 * @property string $type
 * @property int $is_read
 * @property \Carbon\Carbon $created_at
 *
 * @package App\Models
 */
class UserOrderNotification extends Eloquent
{
	protected $primaryKey = 'user_order_notification_id';

	protected $casts = [
		'order_id' => 'int',
		'sender_user_detail_id' => 'int',
		'receiver_user_detail_id' => 'int',
		'is_read' => 'int',
	];

	protected $fillable = [
		'order_id',
		'sender_user_detail_id',
		'receiver_user_detail_id',
		'message',
		'type',
		'is_read'
	];
}
