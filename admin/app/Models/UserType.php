<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 29 Jul 2018 07:48:47 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserType
 * 
 * @property int $user_type_id
 * @property string $type
 * @property string $blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $user_details
 *
 * @package App\Models
 */
class UserType extends Eloquent
{
	protected $primaryKey = 'user_type_id';

	protected $fillable = [
		'type',
		'blocked'
	];

	public function user_details()
		{
			return $this->hasMany(\App\Models\UserDetail::class, 'user_type_id', 'user_type_id');
		}

	public function admin_user_detail_logins()
		{
			return $this->hasMany(\App\Models\AdminUserDetailLogin::class, 'user_type_id', 'user_type_id');
		}

}
