<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Admin
 * 
 * @property int $admin_id
 * @property int $language_id
 * @property string $name
 * @property string $job
 * @property string $email
 * @property int $phone_number
 * @property string $password
 * @property string $forgot_token
 * @property \Carbon\Carbon $forgot_token_validity
 * @property string $profile_pic
 * @property int $service_maximum_dist
 * @property int $support_maximum_dist
 * @property float $buraq_percentage
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Language $language
 *
 * @package App\Models
 */
class Admin extends Eloquent
{
	protected $primaryKey = 'admin_id';

	protected $casts = [
		'language_id' => 'int',
		'phone_number' => 'int',
		'service_maximum_dist' => 'int',
		'support_maximum_dist' => 'int',
		'buraq_percentage' => 'float'
	];

	protected $dates = [
		'forgot_token_validity'
	];

	protected $hidden = [
		'password',
		'forgot_token'
	];

	protected $fillable = [
		'language_id',
		'name',
		'job',
		'email',
		'phone_number',
		'password',
		'forgot_token',
		'forgot_token_validity',
		'profile_pic',
		'service_maximum_dist',
		'support_maximum_dist',
		'buraq_percentage'
	];

	public function language()
	{
		return $this->belongsTo(\App\Models\Language::class);
	}
}
