<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AdminContactus
 * 
 * @property int $admin_contactus_id
 * @property int $user_id
 * @property int $user_detail_id
 * @property int $user_type_id
 * @property int $parent_admin_contactus_id
 * @property string $message
 * @property string $is_read
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\UserType $user_type
 * @property \App\Models\User $user
 * @property \App\Models\UserDetail $user_detail
 *
 * @package App\Models
 */
class AdminContactus extends Eloquent
{
	protected $table = 'admin_contactus';
	protected $primaryKey = 'admin_contactus_id';

	protected $casts = [
		'user_id' => 'int',
		'user_detail_id' => 'int',
		'user_type_id' => 'int',
		'parent_admin_contactus_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'user_detail_id',
		'user_type_id',
		'parent_admin_contactus_id',
		'message',
		'is_read'
	];

	public function user_type()
	{
		return $this->belongsTo(\App\Models\UserType::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function user_detail()
	{
		return $this->belongsTo(\App\Models\UserDetail::class);
	}
}
