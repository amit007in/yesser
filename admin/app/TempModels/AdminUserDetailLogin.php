<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AdminUserDetailLogin
 * 
 * @property int $admin_user_detail_login_id
 * @property int $admin_id
 * @property int $user_detail_id
 * @property int $user_id
 * @property int $organisation_id
 * @property int $user_type_id
 * @property string $access_token
 * @property string $timezone
 * @property string $timezonez
 * @property string $ip_address
 * @property float $latitude
 * @property float $longitude
 * @property string $otp
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\UserType $user_type
 *
 * @package App\Models
 */
class AdminUserDetailLogin extends Eloquent
{
	protected $primaryKey = 'admin_user_detail_login_id';

	protected $casts = [
		'admin_id' => 'int',
		'user_detail_id' => 'int',
		'user_id' => 'int',
		'organisation_id' => 'int',
		'user_type_id' => 'int',
		'latitude' => 'float',
		'longitude' => 'float'
	];

	protected $hidden = [
		'access_token'
	];

	protected $fillable = [
		'admin_id',
		'user_detail_id',
		'user_id',
		'organisation_id',
		'user_type_id',
		'access_token',
		'timezone',
		'timezonez',
		'ip_address',
		'latitude',
		'longitude',
		'otp'
	];

	public function user_type()
	{
		return $this->belongsTo(\App\Models\UserType::class);
	}
}
