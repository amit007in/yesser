<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Category
 * 
 * @property int $category_id
 * @property string $category_type
 * @property float $buraq_percentage
 * @property int $maximum_rides
 * @property int $sort_order
 * @property string $image
 * @property string $maximum_distance
 * @property string $blocked
 * @property string $default_brands
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_details
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_product_details
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_products
 * @property \Illuminate\Database\Eloquent\Collection $category_brands
 * @property \Illuminate\Database\Eloquent\Collection $category_details
 * @property \Illuminate\Database\Eloquent\Collection $orders
 * @property \Illuminate\Database\Eloquent\Collection $organisation_areas
 * @property \Illuminate\Database\Eloquent\Collection $organisation_categories
 * @property \Illuminate\Database\Eloquent\Collection $organisations
 * @property \Illuminate\Database\Eloquent\Collection $organisation_coupon_users
 * @property \Illuminate\Database\Eloquent\Collection $organisation_coupons
 * @property \Illuminate\Database\Eloquent\Collection $user_driver_detail_services
 *
 * @package App\Models
 */
class Category extends Eloquent
{
	protected $primaryKey = 'category_id';

	protected $casts = [
		'buraq_percentage' => 'float',
		'maximum_rides' => 'int',
		'sort_order' => 'int'
	];

	protected $fillable = [
		'category_type',
		'buraq_percentage',
		'maximum_rides',
		'sort_order',
		'image',
		'maximum_distance',
		'blocked',
		'default_brands'
	];

	public function category_brand_details()
	{
		return $this->hasMany(\App\Models\CategoryBrandDetail::class);
	}

	public function category_brand_product_details()
	{
		return $this->hasMany(\App\Models\CategoryBrandProductDetail::class);
	}

	public function category_brand_products()
	{
		return $this->hasMany(\App\Models\CategoryBrandProduct::class);
	}

	public function category_brands()
	{
		return $this->hasMany(\App\Models\CategoryBrand::class);
	}

	public function category_details()
	{
		return $this->hasMany(\App\Models\CategoryDetail::class);
	}

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class);
	}

	public function organisation_areas()
	{
		return $this->hasMany(\App\Models\OrganisationArea::class);
	}

	public function organisation_categories()
	{
		return $this->hasMany(\App\Models\OrganisationCategory::class);
	}

	public function organisations()
	{
		return $this->belongsToMany(\App\Models\Organisation::class, 'organisation_category_brand_products')
					->withPivot('organisation_category_brand_product_id', 'category_brand_id', 'category_brand_product_id', 'alpha_price', 'price_per_quantity', 'price_per_distance', 'price_per_weight', 'price_per_hr', 'price_per_sq_mt')
					->withTimestamps();
	}

	public function organisation_coupon_users()
	{
		return $this->hasMany(\App\Models\OrganisationCouponUser::class);
	}

	public function organisation_coupons()
	{
		return $this->hasMany(\App\Models\OrganisationCoupon::class);
	}

	public function user_driver_detail_services()
	{
		return $this->hasMany(\App\Models\UserDriverDetailService::class);
	}
}
