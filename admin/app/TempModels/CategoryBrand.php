<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CategoryBrand
 * 
 * @property int $category_brand_id
 * @property int $category_id
 * @property int $sort_order
 * @property float $buraq_percentage
 * @property string $category_brand_type
 * @property string $blocked
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\Category $category
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_details
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_product_details
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_products
 * @property \Illuminate\Database\Eloquent\Collection $organisations
 * @property \Illuminate\Database\Eloquent\Collection $organisation_coupon_users
 * @property \Illuminate\Database\Eloquent\Collection $organisation_coupons
 *
 * @package App\Models
 */
class CategoryBrand extends Eloquent
{
	protected $primaryKey = 'category_brand_id';

	protected $casts = [
		'category_id' => 'int',
		'sort_order' => 'int',
		'buraq_percentage' => 'float'
	];

	protected $fillable = [
		'category_id',
		'sort_order',
		'buraq_percentage',
		'category_brand_type',
		'blocked'
	];

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class);
	}

	public function category_brand_details()
	{
		return $this->hasMany(\App\Models\CategoryBrandDetail::class);
	}

	public function category_brand_product_details()
	{
		return $this->hasMany(\App\Models\CategoryBrandProductDetail::class);
	}

	public function category_brand_products()
	{
		return $this->hasMany(\App\Models\CategoryBrandProduct::class);
	}

	public function organisations()
	{
		return $this->belongsToMany(\App\Models\Organisation::class, 'organisation_category_brand_products')
					->withPivot('organisation_category_brand_product_id', 'category_id', 'category_brand_product_id', 'alpha_price', 'price_per_quantity', 'price_per_distance', 'price_per_weight', 'price_per_hr', 'price_per_sq_mt')
					->withTimestamps();
	}

	public function organisation_coupon_users()
	{
		return $this->hasMany(\App\Models\OrganisationCouponUser::class);
	}

	public function organisation_coupons()
	{
		return $this->hasMany(\App\Models\OrganisationCoupon::class);
	}
}
