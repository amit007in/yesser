<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CategoryBrandProduct
 * 
 * @property int $category_brand_product_id
 * @property int $category_id
 * @property int $category_brand_id
 * @property string $category_sub_type
 * @property string $si_unit
 * @property float $actual_value
 * @property float $alpha_price
 * @property float $price_per_quantity
 * @property float $price_per_distance
 * @property float $price_per_weight
 * @property float $price_per_hr
 * @property float $price_per_sq_mt
 * @property int $sort_order
 * @property string $multiple
 * @property string $image
 * @property string $blocked
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\Category $category
 * @property \App\Models\CategoryBrand $category_brand
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_product_details
 * @property \Illuminate\Database\Eloquent\Collection $organisations
 * @property \Illuminate\Database\Eloquent\Collection $organisation_coupon_users
 * @property \Illuminate\Database\Eloquent\Collection $organisation_coupons
 * @property \Illuminate\Database\Eloquent\Collection $user_driver_detail_products
 *
 * @package App\Models
 */
class CategoryBrandProduct extends Eloquent
{
	protected $primaryKey = 'category_brand_product_id';

	protected $casts = [
		'category_id' => 'int',
		'category_brand_id' => 'int',
		'actual_value' => 'float',
		'alpha_price' => 'float',
		'price_per_quantity' => 'float',
		'price_per_distance' => 'float',
		'price_per_weight' => 'float',
		'price_per_hr' => 'float',
		'price_per_sq_mt' => 'float',
		'sort_order' => 'int'
	];

	protected $fillable = [
		'category_id',
		'category_brand_id',
		'category_sub_type',
		'si_unit',
		'actual_value',
		'alpha_price',
		'price_per_quantity',
		'price_per_distance',
		'price_per_weight',
		'price_per_hr',
		'price_per_sq_mt',
		'sort_order',
		'multiple',
		'image',
		'blocked'
	];

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class);
	}

	public function category_brand()
	{
		return $this->belongsTo(\App\Models\CategoryBrand::class);
	}

	public function category_brand_product_details()
	{
		return $this->hasMany(\App\Models\CategoryBrandProductDetail::class);
	}

	public function organisations()
	{
		return $this->belongsToMany(\App\Models\Organisation::class, 'organisation_category_brand_products')
					->withPivot('organisation_category_brand_product_id', 'category_id', 'category_brand_id', 'alpha_price', 'price_per_quantity', 'price_per_distance', 'price_per_weight', 'price_per_hr', 'price_per_sq_mt')
					->withTimestamps();
	}

	public function organisation_coupon_users()
	{
		return $this->hasMany(\App\Models\OrganisationCouponUser::class);
	}

	public function organisation_coupons()
	{
		return $this->hasMany(\App\Models\OrganisationCoupon::class);
	}

	public function user_driver_detail_products()
	{
		return $this->hasMany(\App\Models\UserDriverDetailProduct::class);
	}
}
