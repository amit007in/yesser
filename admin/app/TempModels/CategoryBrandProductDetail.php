<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CategoryBrandProductDetail
 * 
 * @property int $category_brand_product_detail_id
 * @property int $category_id
 * @property int $category_brand_id
 * @property int $category_brand_product_id
 * @property int $language_id
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\Category $category
 * @property \App\Models\CategoryBrand $category_brand
 * @property \App\Models\CategoryBrandProduct $category_brand_product
 * @property \App\Models\Language $language
 *
 * @package App\Models
 */
class CategoryBrandProductDetail extends Eloquent
{
	protected $primaryKey = 'category_brand_product_detail_id';

	protected $casts = [
		'category_id' => 'int',
		'category_brand_id' => 'int',
		'category_brand_product_id' => 'int',
		'language_id' => 'int'
	];

	protected $fillable = [
		'category_id',
		'category_brand_id',
		'category_brand_product_id',
		'language_id',
		'name',
		'description'
	];

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class);
	}

	public function category_brand()
	{
		return $this->belongsTo(\App\Models\CategoryBrand::class);
	}

	public function category_brand_product()
	{
		return $this->belongsTo(\App\Models\CategoryBrandProduct::class);
	}

	public function language()
	{
		return $this->belongsTo(\App\Models\Language::class);
	}
}
