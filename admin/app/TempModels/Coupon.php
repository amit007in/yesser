<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Coupon
 * 
 * @property int $coupon_id
 * @property string $code
 * @property string $amount_value
 * @property int $rides_value
 * @property string $coupon_type
 * @property float $price
 * @property int $expiry_days
 * @property string $blocked
 * @property string $created_at
 * @property string $updated_at
 * @property string $expires_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Coupon extends Eloquent
{
	protected $primaryKey = 'coupon_id';

	protected $casts = [
		'rides_value' => 'int',
		'price' => 'float',
		'expiry_days' => 'int'
	];

	protected $fillable = [
		'code',
		'amount_value',
		'rides_value',
		'coupon_type',
		'price',
		'expiry_days',
		'blocked',
		'expires_at'
	];

	public function users()
	{
		return $this->belongsToMany(\App\Models\User::class, 'coupon_users')
					->withPivot('coupon_user_id', 'user_detail_id', 'user_type_id', 'payment_id', 'rides_value', 'coupon_value', 'rides_left', 'price', 'coupon_type', 'deleted', 'blocked', 'expires_at')
					->withTimestamps();
	}
}
