<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CouponUser
 * 
 * @property int $coupon_user_id
 * @property int $coupon_id
 * @property int $user_id
 * @property int $user_detail_id
 * @property int $user_type_id
 * @property int $payment_id
 * @property int $rides_value
 * @property string $coupon_value
 * @property int $rides_left
 * @property string $price
 * @property string $coupon_type
 * @property string $deleted
 * @property string $blocked
 * @property string $created_at
 * @property string $updated_at
 * @property string $expires_at
 * 
 * @property \App\Models\Payment $payment
 * @property \App\Models\Coupon $coupon
 * @property \App\Models\User $user
 * @property \App\Models\UserDetail $user_detail
 * @property \App\Models\UserType $user_type
 *
 * @package App\Models
 */
class CouponUser extends Eloquent
{
	protected $primaryKey = 'coupon_user_id';

	protected $casts = [
		'coupon_id' => 'int',
		'user_id' => 'int',
		'user_detail_id' => 'int',
		'user_type_id' => 'int',
		'payment_id' => 'int',
		'rides_value' => 'int',
		'rides_left' => 'int'
	];

	protected $fillable = [
		'coupon_id',
		'user_id',
		'user_detail_id',
		'user_type_id',
		'payment_id',
		'rides_value',
		'coupon_value',
		'rides_left',
		'price',
		'coupon_type',
		'deleted',
		'blocked',
		'expires_at'
	];

	public function payment()
	{
		return $this->belongsTo(\App\Models\Payment::class);
	}

	public function coupon()
	{
		return $this->belongsTo(\App\Models\Coupon::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function user_detail()
	{
		return $this->belongsTo(\App\Models\UserDetail::class);
	}

	public function user_type()
	{
		return $this->belongsTo(\App\Models\UserType::class);
	}
}
