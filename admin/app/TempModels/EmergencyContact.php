<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmergencyContact
 * 
 * @property int $emergency_contact_id
 * @property int $sort_order
 * @property int $phone_number
 * @property string $blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $emergency_contact_details
 *
 * @package App\Models
 */
class EmergencyContact extends Eloquent
{
	protected $primaryKey = 'emergency_contact_id';

	protected $casts = [
		'sort_order' => 'int',
		'phone_number' => 'int'
	];

	protected $fillable = [
		'sort_order',
		'phone_number',
		'blocked'
	];

	public function emergency_contact_details()
	{
		return $this->hasMany(\App\Models\EmergencyContactDetail::class);
	}
}
