<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmergencyContactDetail
 * 
 * @property int $emergency_contact_detail_id
 * @property int $emergency_contact_id
 * @property int $language_id
 * @property string $name
 * @property string $image
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\EmergencyContact $emergency_contact
 * @property \App\Models\Language $language
 *
 * @package App\Models
 */
class EmergencyContactDetail extends Eloquent
{
	protected $primaryKey = 'emergency_contact_detail_id';

	protected $casts = [
		'emergency_contact_id' => 'int',
		'language_id' => 'int'
	];

	protected $fillable = [
		'emergency_contact_id',
		'language_id',
		'name',
		'image'
	];

	public function emergency_contact()
	{
		return $this->belongsTo(\App\Models\EmergencyContact::class);
	}

	public function language()
	{
		return $this->belongsTo(\App\Models\Language::class);
	}
}
