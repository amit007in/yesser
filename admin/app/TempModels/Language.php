<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Language
 * 
 * @property int $language_id
 * @property string $language_name
 * @property string $language_code
 * @property string $display_for
 * @property int $sort_order
 * @property string $blocked
 * @property string $is_default
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $admins
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_details
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_product_details
 * @property \Illuminate\Database\Eloquent\Collection $category_details
 * @property \Illuminate\Database\Eloquent\Collection $emergency_contact_details
 * @property \Illuminate\Database\Eloquent\Collection $organisations
 * @property \Illuminate\Database\Eloquent\Collection $user_details
 *
 * @package App\Models
 */
class Language extends Eloquent
{
	protected $primaryKey = 'language_id';

	protected $casts = [
		'sort_order' => 'int'
	];

	protected $fillable = [
		'language_name',
		'language_code',
		'display_for',
		'sort_order',
		'blocked',
		'is_default'
	];

	public function admins()
	{
		return $this->hasMany(\App\Models\Admin::class);
	}

	public function category_brand_details()
	{
		return $this->hasMany(\App\Models\CategoryBrandDetail::class);
	}

	public function category_brand_product_details()
	{
		return $this->hasMany(\App\Models\CategoryBrandProductDetail::class);
	}

	public function category_details()
	{
		return $this->hasMany(\App\Models\CategoryDetail::class);
	}

	public function emergency_contact_details()
	{
		return $this->hasMany(\App\Models\EmergencyContactDetail::class);
	}

	public function organisations()
	{
		return $this->hasMany(\App\Models\Organisation::class);
	}

	public function user_details()
	{
		return $this->hasMany(\App\Models\UserDetail::class);
	}
}
