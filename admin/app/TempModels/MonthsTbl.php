<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MonthsTbl
 * 
 * @property int $id
 * @property string $short_name
 * @property string $long_name
 *
 * @package App\Models
 */
class MonthsTbl extends Eloquent
{
	protected $table = 'months_tbl';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'short_name',
		'long_name'
	];
}
