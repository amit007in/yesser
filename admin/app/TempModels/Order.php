<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Order
 * 
 * @property int $order_id
 * @property string $order_token
 * @property int $customer_user_id
 * @property int $customer_user_detail_id
 * @property int $customer_organisation_id
 * @property int $customer_user_type_id
 * @property int $driver_user_id
 * @property int $driver_user_detail_id
 * @property int $driver_organisation_id
 * @property int $driver_user_type_id
 * @property int $category_id
 * @property int $category_brand_id
 * @property int $category_brand_product_id
 * @property int $continuous_order_id
 * @property string $order_status
 * @property string $pickup_address
 * @property float $pickup_latitude
 * @property float $pickup_longitude
 * @property string $dropoff_address
 * @property float $dropoff_latitude
 * @property float $dropoff_longitude
 * @property string $order_type
 * @property string $created_by
 * @property string $order_timings
 * @property string $future
 * @property string $continouos_startdt
 * @property string $continuous_enddt
 * @property \Carbon\Carbon $continuous_time
 * @property string $continuous
 * @property string $cancel_reason
 * @property string $cancelled_by
 * @property int $organisation_coupon_user_id
 * @property int $coupon_user_id
 * @property string $track_path
 * @property string $track_image
 * @property string $details
 * @property string $material_details
 * @property string $my_turn
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\Category $category
 * @property \App\Models\User $user
 * @property \App\Models\UserDetail $user_detail
 * @property \App\Models\UserType $user_type
 * @property \Illuminate\Database\Eloquent\Collection $order_images
 * @property \Illuminate\Database\Eloquent\Collection $order_ratings
 * @property \Illuminate\Database\Eloquent\Collection $order_requests
 *
 * @package App\Models
 */
class Order extends Eloquent
{
	protected $primaryKey = 'order_id';

	protected $casts = [
		'customer_user_id' => 'int',
		'customer_user_detail_id' => 'int',
		'customer_organisation_id' => 'int',
		'customer_user_type_id' => 'int',
		'driver_user_id' => 'int',
		'driver_user_detail_id' => 'int',
		'driver_organisation_id' => 'int',
		'driver_user_type_id' => 'int',
		'category_id' => 'int',
		'category_brand_id' => 'int',
		'category_brand_product_id' => 'int',
		'continuous_order_id' => 'int',
		'pickup_latitude' => 'float',
		'pickup_longitude' => 'float',
		'dropoff_latitude' => 'float',
		'dropoff_longitude' => 'float',
		'organisation_coupon_user_id' => 'int',
		'coupon_user_id' => 'int'
	];

	protected $dates = [
		'continuous_time'
	];

	protected $hidden = [
		'order_token'
	];

	protected $fillable = [
		'order_token',
		'customer_user_id',
		'customer_user_detail_id',
		'customer_organisation_id',
		'customer_user_type_id',
		'driver_user_id',
		'driver_user_detail_id',
		'driver_organisation_id',
		'driver_user_type_id',
		'category_id',
		'category_brand_id',
		'category_brand_product_id',
		'continuous_order_id',
		'order_status',
		'pickup_address',
		'pickup_latitude',
		'pickup_longitude',
		'dropoff_address',
		'dropoff_latitude',
		'dropoff_longitude',
		'order_type',
		'created_by',
		'order_timings',
		'future',
		'continouos_startdt',
		'continuous_enddt',
		'continuous_time',
		'continuous',
		'cancel_reason',
		'cancelled_by',
		'organisation_coupon_user_id',
		'coupon_user_id',
		'track_path',
		'track_image',
		'details',
		'material_details',
		'my_turn'
	];

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'customer_user_id');
	}

	public function user_detail()
	{
		return $this->belongsTo(\App\Models\UserDetail::class, 'customer_user_detail_id');
	}

	public function user_type()
	{
		return $this->belongsTo(\App\Models\UserType::class, 'customer_user_type_id');
	}

	public function order_images()
	{
		return $this->hasMany(\App\Models\OrderImage::class);
	}

	public function order_ratings()
	{
		return $this->hasMany(\App\Models\OrderRating::class);
	}

	public function order_requests()
	{
		return $this->hasMany(\App\Models\OrderRequest::class);
	}
}
