<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderImage
 * 
 * @property int $order_image_id
 * @property int $order_id
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\Order $order
 *
 * @package App\Models
 */
class OrderImage extends Eloquent
{
	protected $primaryKey = 'order_image_id';

	protected $casts = [
		'order_id' => 'int'
	];

	protected $fillable = [
		'order_id',
		'image'
	];

	public function order()
	{
		return $this->belongsTo(\App\Models\Order::class);
	}
}
