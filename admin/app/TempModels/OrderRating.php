<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderRating
 * 
 * @property int $order_rating_id
 * @property int $order_id
 * @property int $customer_user_id
 * @property int $customer_user_detail_id
 * @property int $customer_organisation_id
 * @property int $driver_user_id
 * @property int $driver_organisation_id
 * @property int $driver_user_detail_id
 * @property int $ratings
 * @property string $comments
 * @property string $created_by
 * @property string $deleted
 * @property string $blocked
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\Order $order
 * @property \App\Models\User $user
 * @property \App\Models\UserDetail $user_detail
 *
 * @package App\Models
 */
class OrderRating extends Eloquent
{
	protected $primaryKey = 'order_rating_id';

	protected $casts = [
		'order_id' => 'int',
		'customer_user_id' => 'int',
		'customer_user_detail_id' => 'int',
		'customer_organisation_id' => 'int',
		'driver_user_id' => 'int',
		'driver_organisation_id' => 'int',
		'driver_user_detail_id' => 'int',
		'ratings' => 'int'
	];

	protected $fillable = [
		'order_id',
		'customer_user_id',
		'customer_user_detail_id',
		'customer_organisation_id',
		'driver_user_id',
		'driver_organisation_id',
		'driver_user_detail_id',
		'ratings',
		'comments',
		'created_by',
		'deleted',
		'blocked'
	];

	public function order()
	{
		return $this->belongsTo(\App\Models\Order::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'customer_user_id');
	}

	public function user_detail()
	{
		return $this->belongsTo(\App\Models\UserDetail::class, 'customer_user_detail_id');
	}
}
