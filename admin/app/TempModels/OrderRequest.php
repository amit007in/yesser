<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderRequest
 * 
 * @property int $order_request_id
 * @property int $order_id
 * @property int $driver_user_id
 * @property int $driver_user_detail_id
 * @property int $driver_organisation_id
 * @property string $order_request_status
 * @property float $driver_request_latitude
 * @property float $driver_request_longitude
 * @property float $driver_current_latitude
 * @property float $driver_current_longitude
 * @property string $accepted_at
 * @property string $rotation
 * @property float $driver_confirmed_latitude
 * @property float $driver_confirmed_longitude
 * @property string $confirmed_at
 * @property string $started_at
 * @property float $driver_started_latitude
 * @property float $driver_started_longitude
 * @property string $full_track
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\Order $order
 * @property \App\Models\User $user
 * @property \App\Models\UserDetail $user_detail
 *
 * @package App\Models
 */
class OrderRequest extends Eloquent
{
	protected $primaryKey = 'order_request_id';

	protected $casts = [
		'order_id' => 'int',
		'driver_user_id' => 'int',
		'driver_user_detail_id' => 'int',
		'driver_organisation_id' => 'int',
		'driver_request_latitude' => 'float',
		'driver_request_longitude' => 'float',
		'driver_current_latitude' => 'float',
		'driver_current_longitude' => 'float',
		'driver_confirmed_latitude' => 'float',
		'driver_confirmed_longitude' => 'float',
		'driver_started_latitude' => 'float',
		'driver_started_longitude' => 'float'
	];

	protected $fillable = [
		'order_id',
		'driver_user_id',
		'driver_user_detail_id',
		'driver_organisation_id',
		'order_request_status',
		'driver_request_latitude',
		'driver_request_longitude',
		'driver_current_latitude',
		'driver_current_longitude',
		'accepted_at',
		'rotation',
		'driver_confirmed_latitude',
		'driver_confirmed_longitude',
		'confirmed_at',
		'started_at',
		'driver_started_latitude',
		'driver_started_longitude',
		'full_track'
	];

	public function order()
	{
		return $this->belongsTo(\App\Models\Order::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'driver_user_id');
	}

	public function user_detail()
	{
		return $this->belongsTo(\App\Models\UserDetail::class, 'driver_user_detail_id');
	}
}
