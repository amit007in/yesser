<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Organisation
 * 
 * @property int $organisation_id
 * @property int $language_id
 * @property string $name
 * @property string $licence_number
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $image
 * @property string $phone_code
 * @property string $phone_number
 * @property string $stripe_connect_id
 * @property string $forgot_token
 * @property \Carbon\Carbon $forgot_token_validity
 * @property string $timezone
 * @property string $timezonez
 * @property float $credit_amount_limit
 * @property int $credit_day_limit
 * @property string $credit_start_dt
 * @property string $blocked
 * @property int $sort_order
 * @property float $buraq_percentage
 * @property string $address
 * @property float $address_latitude
 * @property float $address_longitude
 * @property string $bottle_charge
 * @property string $created_by
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\Language $language
 * @property \Illuminate\Database\Eloquent\Collection $organisation_areas
 * @property \Illuminate\Database\Eloquent\Collection $categories
 * @property \Illuminate\Database\Eloquent\Collection $category_brands
 * @property \Illuminate\Database\Eloquent\Collection $category_brand_products
 *
 * @package App\Models
 */
class Organisation extends Eloquent
{
	protected $primaryKey = 'organisation_id';

	protected $casts = [
		'language_id' => 'int',
		'credit_amount_limit' => 'float',
		'credit_day_limit' => 'int',
		'sort_order' => 'int',
		'buraq_percentage' => 'float',
		'address_latitude' => 'float',
		'address_longitude' => 'float'
	];

	protected $dates = [
		'forgot_token_validity'
	];

	protected $hidden = [
		'password',
		'forgot_token'
	];

	protected $fillable = [
		'language_id',
		'name',
		'licence_number',
		'username',
		'email',
		'password',
		'image',
		'phone_code',
		'phone_number',
		'stripe_connect_id',
		'forgot_token',
		'forgot_token_validity',
		'timezone',
		'timezonez',
		'credit_amount_limit',
		'credit_day_limit',
		'credit_start_dt',
		'blocked',
		'sort_order',
		'buraq_percentage',
		'address',
		'address_latitude',
		'address_longitude',
		'bottle_charge',
		'created_by'
	];

	public function language()
	{
		return $this->belongsTo(\App\Models\Language::class);
	}

	public function organisation_areas()
	{
		return $this->hasMany(\App\Models\OrganisationArea::class);
	}

	public function categories()
	{
		return $this->belongsToMany(\App\Models\Category::class, 'organisation_category_brand_products')
					->withPivot('organisation_category_brand_product_id', 'category_brand_id', 'category_brand_product_id', 'alpha_price', 'price_per_quantity', 'price_per_distance', 'price_per_weight', 'price_per_hr', 'price_per_sq_mt')
					->withTimestamps();
	}

	public function category_brands()
	{
		return $this->belongsToMany(\App\Models\CategoryBrand::class, 'organisation_category_brand_products')
					->withPivot('organisation_category_brand_product_id', 'category_id', 'category_brand_product_id', 'alpha_price', 'price_per_quantity', 'price_per_distance', 'price_per_weight', 'price_per_hr', 'price_per_sq_mt')
					->withTimestamps();
	}

	public function category_brand_products()
	{
		return $this->belongsToMany(\App\Models\CategoryBrandProduct::class, 'organisation_category_brand_products')
					->withPivot('organisation_category_brand_product_id', 'category_id', 'category_brand_id', 'alpha_price', 'price_per_quantity', 'price_per_distance', 'price_per_weight', 'price_per_hr', 'price_per_sq_mt')
					->withTimestamps();
	}
}
