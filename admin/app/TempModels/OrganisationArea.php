<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrganisationArea
 * 
 * @property int $organisation_area_id
 * @property int $organisation_id
 * @property int $category_id
 * @property string $label
 * @property string $address
 * @property float $address_latitude
 * @property float $address_longitude
 * @property int $distance
 * @property bool $sunday_service
 * @property bool $monday_service
 * @property bool $tuesday_service
 * @property bool $wednesday_service
 * @property bool $thursday_service
 * @property bool $friday_service
 * @property bool $saturday_service
 * @property string $created_by
 * @property string $deleted
 * @property string $blocked
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\Category $category
 * @property \App\Models\Organisation $organisation
 *
 * @package App\Models
 */
class OrganisationArea extends Eloquent
{
	protected $primaryKey = 'organisation_area_id';

	protected $casts = [
		'organisation_id' => 'int',
		'category_id' => 'int',
		'address_latitude' => 'float',
		'address_longitude' => 'float',
		'distance' => 'int',
		'sunday_service' => 'bool',
		'monday_service' => 'bool',
		'tuesday_service' => 'bool',
		'wednesday_service' => 'bool',
		'thursday_service' => 'bool',
		'friday_service' => 'bool',
		'saturday_service' => 'bool'
	];

	protected $fillable = [
		'organisation_id',
		'category_id',
		'label',
		'address',
		'address_latitude',
		'address_longitude',
		'distance',
		'sunday_service',
		'monday_service',
		'tuesday_service',
		'wednesday_service',
		'thursday_service',
		'friday_service',
		'saturday_service',
		'created_by',
		'deleted',
		'blocked'
	];

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class);
	}

	public function organisation()
	{
		return $this->belongsTo(\App\Models\Organisation::class);
	}
}
