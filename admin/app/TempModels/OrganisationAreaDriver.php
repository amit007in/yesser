<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrganisationAreaDriver
 * 
 * @property int $organisation_area_driver_id
 * @property int $organisation_area_id
 * @property int $user_detail_id
 * @property int $organisation_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @package App\Models
 */
class OrganisationAreaDriver extends Eloquent
{
	protected $primaryKey = 'organisation_area_driver_id';

	protected $casts = [
		'organisation_area_id' => 'int',
		'user_detail_id' => 'int',
		'organisation_id' => 'int'
	];

	protected $fillable = [
		'organisation_area_id',
		'user_detail_id',
		'organisation_id'
	];
}
