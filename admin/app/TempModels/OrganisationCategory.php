<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrganisationCategory
 * 
 * @property int $organisation_category_id
 * @property int $organisation_id
 * @property int $category_id
 * @property float $buraq_percentage
 * @property string $category_type
 * @property string $deleted
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\Category $category
 *
 * @package App\Models
 */
class OrganisationCategory extends Eloquent
{
	protected $primaryKey = 'organisation_category_id';

	protected $casts = [
		'organisation_id' => 'int',
		'category_id' => 'int',
		'buraq_percentage' => 'float'
	];

	protected $fillable = [
		'organisation_id',
		'category_id',
		'buraq_percentage',
		'category_type',
		'deleted'
	];

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class);
	}
}
