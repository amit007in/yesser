<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrganisationCategoryBrandProduct
 * 
 * @property int $organisation_category_brand_product_id
 * @property int $organisation_id
 * @property int $category_id
 * @property int $category_brand_id
 * @property int $category_brand_product_id
 * @property float $alpha_price
 * @property float $price_per_quantity
 * @property float $price_per_distance
 * @property float $price_per_weight
 * @property float $price_per_hr
 * @property float $price_per_sq_mt
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\Organisation $organisation
 * @property \App\Models\Category $category
 * @property \App\Models\CategoryBrand $category_brand
 * @property \App\Models\CategoryBrandProduct $category_brand_product
 *
 * @package App\Models
 */
class OrganisationCategoryBrandProduct extends Eloquent
{
	protected $primaryKey = 'organisation_category_brand_product_id';

	protected $casts = [
		'organisation_id' => 'int',
		'category_id' => 'int',
		'category_brand_id' => 'int',
		'category_brand_product_id' => 'int',
		'alpha_price' => 'float',
		'price_per_quantity' => 'float',
		'price_per_distance' => 'float',
		'price_per_weight' => 'float',
		'price_per_hr' => 'float',
		'price_per_sq_mt' => 'float'
	];

	protected $fillable = [
		'organisation_id',
		'category_id',
		'category_brand_id',
		'category_brand_product_id',
		'alpha_price',
		'price_per_quantity',
		'price_per_distance',
		'price_per_weight',
		'price_per_hr',
		'price_per_sq_mt'
	];

	public function organisation()
	{
		return $this->belongsTo(\App\Models\Organisation::class);
	}

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class);
	}

	public function category_brand()
	{
		return $this->belongsTo(\App\Models\CategoryBrand::class);
	}

	public function category_brand_product()
	{
		return $this->belongsTo(\App\Models\CategoryBrandProduct::class);
	}
}
