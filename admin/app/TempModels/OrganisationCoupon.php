<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrganisationCoupon
 * 
 * @property int $organisation_coupon_id
 * @property int $organisation_id
 * @property int $category_id
 * @property int $category_brand_id
 * @property int $category_brand_product_id
 * @property int $user_id
 * @property int $user_detail_id
 * @property int $user_type_id
 * @property float $price
 * @property int $quantity
 * @property string $description
 * @property string $deleted
 * @property string $blocked
 * @property string $created_by
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\Category $category
 * @property \App\Models\CategoryBrand $category_brand
 * @property \App\Models\CategoryBrandProduct $category_brand_product
 * @property \App\Models\UserType $user_type
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class OrganisationCoupon extends Eloquent
{
	protected $primaryKey = 'organisation_coupon_id';

	protected $casts = [
		'organisation_id' => 'int',
		'category_id' => 'int',
		'category_brand_id' => 'int',
		'category_brand_product_id' => 'int',
		'user_id' => 'int',
		'user_detail_id' => 'int',
		'user_type_id' => 'int',
		'price' => 'float',
		'quantity' => 'int'
	];

	protected $fillable = [
		'organisation_id',
		'category_id',
		'category_brand_id',
		'category_brand_product_id',
		'user_id',
		'user_detail_id',
		'user_type_id',
		'price',
		'quantity',
		'description',
		'deleted',
		'blocked',
		'created_by'
	];

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class);
	}

	public function category_brand()
	{
		return $this->belongsTo(\App\Models\CategoryBrand::class);
	}

	public function category_brand_product()
	{
		return $this->belongsTo(\App\Models\CategoryBrandProduct::class);
	}

	public function user_type()
	{
		return $this->belongsTo(\App\Models\UserType::class);
	}

	public function users()
	{
		return $this->belongsToMany(\App\Models\User::class, 'organisation_coupon_users', 'organisation_coupon_id', 'customer_user_id')
					->withPivot('organisation_coupon_user_id', 'seller_user_id', 'seller_user_detail_id', 'seller_user_type_id', 'seller_organisation_id', 'category_id', 'category_brand_id', 'category_brand_product_id', 'customer_user_detail_id', 'customer_user_type_id', 'customer_organisation_id', 'bottle_quantity', 'quantity', 'quantity_left', 'address', 'address_latitude', 'address_longitude')
					->withTimestamps();
	}
}
