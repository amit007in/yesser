<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrganisationCouponUser
 * 
 * @property int $organisation_coupon_user_id
 * @property int $seller_user_id
 * @property int $seller_user_detail_id
 * @property int $seller_user_type_id
 * @property int $seller_organisation_id
 * @property int $organisation_coupon_id
 * @property int $category_id
 * @property int $category_brand_id
 * @property int $category_brand_product_id
 * @property int $customer_user_id
 * @property int $customer_user_detail_id
 * @property int $customer_user_type_id
 * @property int $customer_organisation_id
 * @property int $bottle_quantity
 * @property int $quantity
 * @property int $quantity_left
 * @property string $address
 * @property float $address_latitude
 * @property float $address_longitude
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\Category $category
 * @property \App\Models\CategoryBrand $category_brand
 * @property \App\Models\CategoryBrandProduct $category_brand_product
 * @property \App\Models\User $user
 * @property \App\Models\UserDetail $user_detail
 * @property \App\Models\UserType $user_type
 * @property \App\Models\OrganisationCoupon $organisation_coupon
 *
 * @package App\Models
 */
class OrganisationCouponUser extends Eloquent
{
	protected $primaryKey = 'organisation_coupon_user_id';

	protected $casts = [
		'seller_user_id' => 'int',
		'seller_user_detail_id' => 'int',
		'seller_user_type_id' => 'int',
		'seller_organisation_id' => 'int',
		'organisation_coupon_id' => 'int',
		'category_id' => 'int',
		'category_brand_id' => 'int',
		'category_brand_product_id' => 'int',
		'customer_user_id' => 'int',
		'customer_user_detail_id' => 'int',
		'customer_user_type_id' => 'int',
		'customer_organisation_id' => 'int',
		'bottle_quantity' => 'int',
		'quantity' => 'int',
		'quantity_left' => 'int',
		'address_latitude' => 'float',
		'address_longitude' => 'float'
	];

	protected $fillable = [
		'seller_user_id',
		'seller_user_detail_id',
		'seller_user_type_id',
		'seller_organisation_id',
		'organisation_coupon_id',
		'category_id',
		'category_brand_id',
		'category_brand_product_id',
		'customer_user_id',
		'customer_user_detail_id',
		'customer_user_type_id',
		'customer_organisation_id',
		'bottle_quantity',
		'quantity',
		'quantity_left',
		'address',
		'address_latitude',
		'address_longitude'
	];

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class);
	}

	public function category_brand()
	{
		return $this->belongsTo(\App\Models\CategoryBrand::class);
	}

	public function category_brand_product()
	{
		return $this->belongsTo(\App\Models\CategoryBrandProduct::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'customer_user_id');
	}

	public function user_detail()
	{
		return $this->belongsTo(\App\Models\UserDetail::class, 'customer_user_detail_id');
	}

	public function user_type()
	{
		return $this->belongsTo(\App\Models\UserType::class, 'seller_user_type_id');
	}

	public function organisation_coupon()
	{
		return $this->belongsTo(\App\Models\OrganisationCoupon::class);
	}
}
