<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Payment
 * 
 * @property int $payment_id
 * @property int $customer_user_id
 * @property int $customer_user_detail_id
 * @property int $customer_user_type_id
 * @property int $user_card_id
 * @property int $customer_organisation_id
 * @property int $order_id
 * @property int $seller_user_id
 * @property int $seller_user_detail_id
 * @property int $seller_user_type_id
 * @property int $seller_organisation_id
 * @property int $organisation_coupon_user_id
 * @property string $payment_type
 * @property string $payment_status
 * @property string $refund_status
 * @property string $transaction_id
 * @property string $refund_id
 * @property float $buraq_percentage
 * @property string $product_actual_value
 * @property int $product_quantity
 * @property string $product_weight
 * @property string $product_sq_mt
 * @property string $order_distance
 * @property string $order_time
 * @property string $product_alpha_charge
 * @property string $product_per_quantity_charge
 * @property string $product_per_weight_charge
 * @property string $product_per_distance_charge
 * @property string $product_per_hr_charge
 * @property string $product_per_sq_mt_charge
 * @property string $initial_charge
 * @property string $admin_charge
 * @property string $bank_charge
 * @property string $final_charge
 * @property string $bottle_charge
 * @property int $bottle_returned_value
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\User $user
 * @property \App\Models\UserType $user_type
 * @property \App\Models\UserDetail $user_detail
 * @property \Illuminate\Database\Eloquent\Collection $coupon_users
 *
 * @package App\Models
 */
class Payment extends Eloquent
{
	protected $primaryKey = 'payment_id';

	protected $casts = [
		'customer_user_id' => 'int',
		'customer_user_detail_id' => 'int',
		'customer_user_type_id' => 'int',
		'user_card_id' => 'int',
		'customer_organisation_id' => 'int',
		'order_id' => 'int',
		'seller_user_id' => 'int',
		'seller_user_detail_id' => 'int',
		'seller_user_type_id' => 'int',
		'seller_organisation_id' => 'int',
		'organisation_coupon_user_id' => 'int',
		'buraq_percentage' => 'float',
		'product_quantity' => 'int',
		'bottle_returned_value' => 'int'
	];

	protected $fillable = [
		'customer_user_id',
		'customer_user_detail_id',
		'customer_user_type_id',
		'user_card_id',
		'customer_organisation_id',
		'order_id',
		'seller_user_id',
		'seller_user_detail_id',
		'seller_user_type_id',
		'seller_organisation_id',
		'organisation_coupon_user_id',
		'payment_type',
		'payment_status',
		'refund_status',
		'transaction_id',
		'refund_id',
		'buraq_percentage',
		'product_actual_value',
		'product_quantity',
		'product_weight',
		'product_sq_mt',
		'order_distance',
		'order_time',
		'product_alpha_charge',
		'product_per_quantity_charge',
		'product_per_weight_charge',
		'product_per_distance_charge',
		'product_per_hr_charge',
		'product_per_sq_mt_charge',
		'initial_charge',
		'admin_charge',
		'bank_charge',
		'final_charge',
		'bottle_charge',
		'bottle_returned_value'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'customer_user_id');
	}

	public function user_type()
	{
		return $this->belongsTo(\App\Models\UserType::class, 'customer_user_type_id');
	}

	public function user_detail()
	{
		return $this->belongsTo(\App\Models\UserDetail::class, 'customer_user_detail_id');
	}

	public function coupon_users()
	{
		return $this->hasMany(\App\Models\CouponUser::class);
	}
}
