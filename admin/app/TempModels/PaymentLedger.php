<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PaymentLedger
 * 
 * @property int $payment_ledger_id
 * @property int $organisation_id
 * @property int $user_id
 * @property int $user_detail_id
 * @property int $user_type_id
 * @property string $amount
 * @property string $type
 * @property \Carbon\Carbon $ledger_dt
 * @property string $description
 * @property string $deleted
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\UserType $user_type
 *
 * @package App\Models
 */
class PaymentLedger extends Eloquent
{
	protected $primaryKey = 'payment_ledger_id';

	protected $casts = [
		'organisation_id' => 'int',
		'user_id' => 'int',
		'user_detail_id' => 'int',
		'user_type_id' => 'int'
	];

	protected $dates = [
		'ledger_dt'
	];

	protected $fillable = [
		'organisation_id',
		'user_id',
		'user_detail_id',
		'user_type_id',
		'amount',
		'type',
		'ledger_dt',
		'description',
		'deleted'
	];

	public function user_type()
	{
		return $this->belongsTo(\App\Models\UserType::class);
	}
}
