<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class User
 * 
 * @property int $user_id
 * @property int $organisation_id
 * @property string $stripe_customer_id
 * @property string $stripe_connect_id
 * @property string $name
 * @property string $email
 * @property string $phone_code
 * @property int $phone_number
 * @property string $address
 * @property float $address_latitude
 * @property float $address_longitude
 * @property string $stripe_connect_token
 * @property string $blocked
 * @property string $bottle_charge
 * @property float $buraq_percentage
 * @property string $created_by
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $admin_contactuses
 * @property \Illuminate\Database\Eloquent\Collection $coupons
 * @property \Illuminate\Database\Eloquent\Collection $order_ratings
 * @property \Illuminate\Database\Eloquent\Collection $order_requests
 * @property \Illuminate\Database\Eloquent\Collection $orders
 * @property \Illuminate\Database\Eloquent\Collection $organisation_coupons
 * @property \Illuminate\Database\Eloquent\Collection $payments
 * @property \Illuminate\Database\Eloquent\Collection $user_bank_details
 * @property \Illuminate\Database\Eloquent\Collection $user_cards
 * @property \Illuminate\Database\Eloquent\Collection $user_detail_notifications
 * @property \Illuminate\Database\Eloquent\Collection $user_details
 * @property \Illuminate\Database\Eloquent\Collection $user_driver_detail_products
 * @property \Illuminate\Database\Eloquent\Collection $user_driver_detail_services
 *
 * @package App\Models
 */
class User extends Eloquent
{
	protected $primaryKey = 'user_id';

	protected $casts = [
		'organisation_id' => 'int',
		'phone_number' => 'int',
		'address_latitude' => 'float',
		'address_longitude' => 'float',
		'buraq_percentage' => 'float'
	];

	protected $hidden = [
		'stripe_connect_token'
	];

	protected $fillable = [
		'organisation_id',
		'stripe_customer_id',
		'stripe_connect_id',
		'name',
		'email',
		'phone_code',
		'phone_number',
		'address',
		'address_latitude',
		'address_longitude',
		'stripe_connect_token',
		'blocked',
		'bottle_charge',
		'buraq_percentage',
		'created_by'
	];

	public function admin_contactuses()
	{
		return $this->hasMany(\App\Models\AdminContactus::class);
	}

	public function coupons()
	{
		return $this->belongsToMany(\App\Models\Coupon::class, 'coupon_users')
					->withPivot('coupon_user_id', 'user_detail_id', 'user_type_id', 'payment_id', 'rides_value', 'coupon_value', 'rides_left', 'price', 'coupon_type', 'deleted', 'blocked', 'expires_at')
					->withTimestamps();
	}

	public function order_ratings()
	{
		return $this->hasMany(\App\Models\OrderRating::class, 'customer_user_id');
	}

	public function order_requests()
	{
		return $this->hasMany(\App\Models\OrderRequest::class, 'driver_user_id');
	}

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class, 'customer_user_id');
	}

	public function organisation_coupons()
	{
		return $this->belongsToMany(\App\Models\OrganisationCoupon::class, 'organisation_coupon_users', 'customer_user_id')
					->withPivot('organisation_coupon_user_id', 'seller_user_id', 'seller_user_detail_id', 'seller_user_type_id', 'seller_organisation_id', 'category_id', 'category_brand_id', 'category_brand_product_id', 'customer_user_detail_id', 'customer_user_type_id', 'customer_organisation_id', 'bottle_quantity', 'quantity', 'quantity_left', 'address', 'address_latitude', 'address_longitude')
					->withTimestamps();
	}

	public function payments()
	{
		return $this->hasMany(\App\Models\Payment::class, 'customer_user_id');
	}

	public function user_bank_details()
	{
		return $this->hasMany(\App\Models\UserBankDetail::class);
	}

	public function user_cards()
	{
		return $this->hasMany(\App\Models\UserCard::class);
	}

	public function user_detail_notifications()
	{
		return $this->hasMany(\App\Models\UserDetailNotification::class);
	}

	public function user_details()
	{
		return $this->hasMany(\App\Models\UserDetail::class);
	}

	public function user_driver_detail_products()
	{
		return $this->hasMany(\App\Models\UserDriverDetailProduct::class);
	}

	public function user_driver_detail_services()
	{
		return $this->hasMany(\App\Models\UserDriverDetailService::class);
	}
}
