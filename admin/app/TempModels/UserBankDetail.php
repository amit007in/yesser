<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserBankDetail
 * 
 * @property int $user_bank_detail_id
 * @property int $user_id
 * @property string $bank_name
 * @property string $bank_account_number
 * @property string $active
 * @property string $deleted
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UserBankDetail extends Eloquent
{
	protected $primaryKey = 'user_bank_detail_id';

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'bank_name',
		'bank_account_number',
		'active',
		'deleted'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
