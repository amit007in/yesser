<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserDetail
 * 
 * @property int $user_detail_id
 * @property int $user_id
 * @property int $user_type_id
 * @property int $language_id
 * @property int $category_id
 * @property int $category_brand_id
 * @property int $organisation_id
 * @property string $online_status
 * @property string $otp
 * @property string $password
 * @property string $profile_pic
 * @property string $access_token
 * @property float $latitude
 * @property float $longitude
 * @property string $timezone
 * @property string $timezonez
 * @property string $socket_id
 * @property string $device_type
 * @property string $fcm_id
 * @property string $mulkiya_number
 * @property string $mulkiya_front
 * @property string $mulkiya_back
 * @property \Carbon\Carbon $mulkiya_validity
 * @property string $created_by
 * @property string $blocked
 * @property string $approved
 * @property string $forgot_token
 * @property string $forgot_token_validity
 * @property int $maximum_rides
 * @property string $notifications
 * @property float $bearing
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\User $user
 * @property \App\Models\UserType $user_type
 * @property \App\Models\Language $language
 * @property \Illuminate\Database\Eloquent\Collection $admin_contactuses
 * @property \Illuminate\Database\Eloquent\Collection $coupon_users
 * @property \Illuminate\Database\Eloquent\Collection $order_ratings
 * @property \Illuminate\Database\Eloquent\Collection $order_requests
 * @property \Illuminate\Database\Eloquent\Collection $orders
 * @property \Illuminate\Database\Eloquent\Collection $organisation_coupon_users
 * @property \Illuminate\Database\Eloquent\Collection $payments
 * @property \Illuminate\Database\Eloquent\Collection $user_detail_notifications
 * @property \Illuminate\Database\Eloquent\Collection $user_driver_detail_products
 * @property \Illuminate\Database\Eloquent\Collection $user_driver_detail_services
 *
 * @package App\Models
 */
class UserDetail extends Eloquent
{
	protected $primaryKey = 'user_detail_id';

	protected $casts = [
		'user_id' => 'int',
		'user_type_id' => 'int',
		'language_id' => 'int',
		'category_id' => 'int',
		'category_brand_id' => 'int',
		'organisation_id' => 'int',
		'latitude' => 'float',
		'longitude' => 'float',
		'maximum_rides' => 'int',
		'bearing' => 'float'
	];

	protected $dates = [
		'mulkiya_validity'
	];

	protected $hidden = [
		'password',
		'access_token',
		'forgot_token'
	];

	protected $fillable = [
		'user_id',
		'user_type_id',
		'language_id',
		'category_id',
		'category_brand_id',
		'organisation_id',
		'online_status',
		'otp',
		'password',
		'profile_pic',
		'access_token',
		'latitude',
		'longitude',
		'timezone',
		'timezonez',
		'socket_id',
		'device_type',
		'fcm_id',
		'mulkiya_number',
		'mulkiya_front',
		'mulkiya_back',
		'mulkiya_validity',
		'created_by',
		'blocked',
		'approved',
		'forgot_token',
		'forgot_token_validity',
		'maximum_rides',
		'notifications',
		'bearing'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function user_type()
	{
		return $this->belongsTo(\App\Models\UserType::class);
	}

	public function language()
	{
		return $this->belongsTo(\App\Models\Language::class);
	}

	public function admin_contactuses()
	{
		return $this->hasMany(\App\Models\AdminContactus::class);
	}

	public function coupon_users()
	{
		return $this->hasMany(\App\Models\CouponUser::class);
	}

	public function order_ratings()
	{
		return $this->hasMany(\App\Models\OrderRating::class, 'customer_user_detail_id');
	}

	public function order_requests()
	{
		return $this->hasMany(\App\Models\OrderRequest::class, 'driver_user_detail_id');
	}

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class, 'customer_user_detail_id');
	}

	public function organisation_coupon_users()
	{
		return $this->hasMany(\App\Models\OrganisationCouponUser::class, 'customer_user_detail_id');
	}

	public function payments()
	{
		return $this->hasMany(\App\Models\Payment::class, 'customer_user_detail_id');
	}

	public function user_detail_notifications()
	{
		return $this->hasMany(\App\Models\UserDetailNotification::class);
	}

	public function user_driver_detail_products()
	{
		return $this->hasMany(\App\Models\UserDriverDetailProduct::class);
	}

	public function user_driver_detail_services()
	{
		return $this->hasMany(\App\Models\UserDriverDetailService::class);
	}
}
