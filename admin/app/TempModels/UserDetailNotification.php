<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserDetailNotification
 * 
 * @property int $user_detail_notification_id
 * @property int $user_id
 * @property int $user_detail_id
 * @property int $user_type_id
 * @property int $user_organisation_id
 * @property int $sender_user_id
 * @property int $sender_user_detail_id
 * @property int $sender_type_id
 * @property int $sender_organisation_id
 * @property int $order_id
 * @property int $order_request_id
 * @property int $order_rating_id
 * @property string $message
 * @property string $type
 * @property string $is_read
 * @property string $deleted
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\User $user
 * @property \App\Models\UserType $user_type
 * @property \App\Models\UserDetail $user_detail
 *
 * @package App\Models
 */
class UserDetailNotification extends Eloquent
{
	protected $primaryKey = 'user_detail_notification_id';

	protected $casts = [
		'user_id' => 'int',
		'user_detail_id' => 'int',
		'user_type_id' => 'int',
		'user_organisation_id' => 'int',
		'sender_user_id' => 'int',
		'sender_user_detail_id' => 'int',
		'sender_type_id' => 'int',
		'sender_organisation_id' => 'int',
		'order_id' => 'int',
		'order_request_id' => 'int',
		'order_rating_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'user_detail_id',
		'user_type_id',
		'user_organisation_id',
		'sender_user_id',
		'sender_user_detail_id',
		'sender_type_id',
		'sender_organisation_id',
		'order_id',
		'order_request_id',
		'order_rating_id',
		'message',
		'type',
		'is_read',
		'deleted'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function user_type()
	{
		return $this->belongsTo(\App\Models\UserType::class);
	}

	public function user_detail()
	{
		return $this->belongsTo(\App\Models\UserDetail::class);
	}
}
