<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserDriverDetailProduct
 * 
 * @property int $user_driver_detail_product_id
 * @property int $user_id
 * @property int $user_detail_id
 * @property int $category_brand_product_id
 * @property float $alpha_price
 * @property float $price_per_quantity
 * @property float $price_per_distance
 * @property float $price_per_weight
 * @property float $price_per_hr
 * @property float $price_per_sq_mt
 * @property string $deleted
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\CategoryBrandProduct $category_brand_product
 * @property \App\Models\User $user
 * @property \App\Models\UserDetail $user_detail
 *
 * @package App\Models
 */
class UserDriverDetailProduct extends Eloquent
{
	protected $primaryKey = 'user_driver_detail_product_id';

	protected $casts = [
		'user_id' => 'int',
		'user_detail_id' => 'int',
		'category_brand_product_id' => 'int',
		'alpha_price' => 'float',
		'price_per_quantity' => 'float',
		'price_per_distance' => 'float',
		'price_per_weight' => 'float',
		'price_per_hr' => 'float',
		'price_per_sq_mt' => 'float'
	];

	protected $fillable = [
		'user_id',
		'user_detail_id',
		'category_brand_product_id',
		'alpha_price',
		'price_per_quantity',
		'price_per_distance',
		'price_per_weight',
		'price_per_hr',
		'price_per_sq_mt',
		'deleted'
	];

	public function category_brand_product()
	{
		return $this->belongsTo(\App\Models\CategoryBrandProduct::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function user_detail()
	{
		return $this->belongsTo(\App\Models\UserDetail::class);
	}
}
