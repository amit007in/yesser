<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserDriverDetailService
 * 
 * @property int $user_driver_detail_service_id
 * @property int $user_id
 * @property int $user_detail_id
 * @property int $category_id
 * @property float $buraq_percentage
 * @property string $deleted
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property \App\Models\Category $category
 * @property \App\Models\User $user
 * @property \App\Models\UserDetail $user_detail
 *
 * @package App\Models
 */
class UserDriverDetailService extends Eloquent
{
	protected $primaryKey = 'user_driver_detail_service_id';

	protected $casts = [
		'user_id' => 'int',
		'user_detail_id' => 'int',
		'category_id' => 'int',
		'buraq_percentage' => 'float'
	];

	protected $fillable = [
		'user_id',
		'user_detail_id',
		'category_id',
		'buraq_percentage',
		'deleted'
	];

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function user_detail()
	{
		return $this->belongsTo(\App\Models\UserDetail::class);
	}
}
