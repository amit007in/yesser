<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 28 Nov 2018 10:54:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserType
 * 
 * @property int $user_type_id
 * @property string $type
 * @property string $blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $admin_contactuses
 * @property \Illuminate\Database\Eloquent\Collection $admin_user_detail_logins
 * @property \Illuminate\Database\Eloquent\Collection $coupon_users
 * @property \Illuminate\Database\Eloquent\Collection $orders
 * @property \Illuminate\Database\Eloquent\Collection $organisation_coupon_users
 * @property \Illuminate\Database\Eloquent\Collection $organisation_coupons
 * @property \Illuminate\Database\Eloquent\Collection $payment_ledgers
 * @property \Illuminate\Database\Eloquent\Collection $payments
 * @property \Illuminate\Database\Eloquent\Collection $user_detail_notifications
 * @property \Illuminate\Database\Eloquent\Collection $user_details
 *
 * @package App\Models
 */
class UserType extends Eloquent
{
	protected $primaryKey = 'user_type_id';

	protected $fillable = [
		'type',
		'blocked'
	];

	public function admin_contactuses()
	{
		return $this->hasMany(\App\Models\AdminContactus::class);
	}

	public function admin_user_detail_logins()
	{
		return $this->hasMany(\App\Models\AdminUserDetailLogin::class);
	}

	public function coupon_users()
	{
		return $this->hasMany(\App\Models\CouponUser::class);
	}

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class, 'customer_user_type_id');
	}

	public function organisation_coupon_users()
	{
		return $this->hasMany(\App\Models\OrganisationCouponUser::class, 'seller_user_type_id');
	}

	public function organisation_coupons()
	{
		return $this->hasMany(\App\Models\OrganisationCoupon::class);
	}

	public function payment_ledgers()
	{
		return $this->hasMany(\App\Models\PaymentLedger::class);
	}

	public function payments()
	{
		return $this->hasMany(\App\Models\Payment::class, 'customer_user_type_id');
	}

	public function user_detail_notifications()
	{
		return $this->hasMany(\App\Models\UserDetailNotification::class);
	}

	public function user_details()
	{
		return $this->hasMany(\App\Models\UserDetail::class);
	}
}
