<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admins', function(Blueprint $table)
		{
			$table->bigInteger('admin_id', true)->unsigned();
			$table->string('name');
			$table->string('job');
			$table->string('email')->index('email');
			$table->text('password', 65535);
			$table->string('forgot_token', 100);
			$table->dateTime('forgot_token_validity')->nullable();
			$table->string('profile_pic', 100);
			$table->string('timezone', 100)->default('Asia/Kolkata');
			$table->string('timezonez', 10)->default('+05:30');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admins');
	}

}
