<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryBrandDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('category_brand_details', function(Blueprint $table)
		{
			$table->bigInteger('category_brand_detail_id', true)->unsigned();
			$table->bigInteger('category_id')->unsigned()->index('category_id');
			$table->bigInteger('language_id')->unsigned()->index('language_id');
			$table->bigInteger('category_brand_id')->unsigned()->index('category_brand_id');
			$table->string('name');
			$table->string('image');
			$table->text('description', 65535)->nullable();
			$table->timestamps();
			$table->unique(['language_id','category_brand_id'], 'language_id_2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('category_brand_details');
	}

}
