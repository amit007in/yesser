<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryBrandProductDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('category_brand_product_details', function(Blueprint $table)
		{
			$table->bigInteger('category_brand_product_detail_id', true)->unsigned();
			$table->bigInteger('category_id')->unsigned()->index('category_id');
			$table->bigInteger('category_brand_id')->unsigned()->index('category_brand_id');
			$table->bigInteger('category_brand_product_id')->unsigned()->index('category_brand_product_id');
			$table->bigInteger('language_id')->unsigned()->index('language_id');
			$table->string('name');
			$table->text('description', 65535)->nullable();
			$table->timestamps();
			$table->unique(['category_brand_product_id','language_id','category_brand_id','category_id'], 'category_brand_product_id_2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('category_brand_product_details');
	}

}
