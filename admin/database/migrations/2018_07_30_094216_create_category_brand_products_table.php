<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryBrandProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('category_brand_products', function(Blueprint $table)
		{
			$table->bigInteger('category_brand_product_id', true)->unsigned();
			$table->bigInteger('category_id')->unsigned()->index('category_id');
			$table->bigInteger('category_brand_id')->unsigned()->index('category_brand_id');
			$table->string('category_sub_type', 10)->default('Quantity')->comment('Quantity or Brand or Company');
			$table->string('si_unit', 10);
			$table->float('actual_value', 10);
			$table->float('alpha_price', 10)->default(0.00)->comment('Alpha Value can be zero');
			$table->float('price_per_quantity', 10);
			$table->bigInteger('sort_order');
			$table->enum('blocked', array('0','1'))->default('0');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('category_brand_products');
	}

}
