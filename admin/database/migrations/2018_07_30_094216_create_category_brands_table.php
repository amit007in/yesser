<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryBrandsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('category_brands', function(Blueprint $table)
		{
			$table->bigInteger('category_brand_id', true)->unsigned();
			$table->bigInteger('category_id')->unsigned()->index('category_id');
			$table->bigInteger('sort_order');
			$table->string('category_brand_type', 10)->nullable()->comment('Either its Brand or Model or Make Etc depending on category type');
			$table->enum('blocked', array('0','1'))->default('0');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('category_brands');
	}

}
