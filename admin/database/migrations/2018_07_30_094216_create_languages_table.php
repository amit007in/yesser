<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('languages', function(Blueprint $table)
		{
			$table->bigInteger('language_id', true)->unsigned();
			$table->string('language_name', 100);
			$table->string('language_code', 2);
			$table->string('display_for', 3);
			$table->bigInteger('sort_order');
			$table->enum('blocked', array('0','1'))->default('0');
			$table->enum('is_default', array('0','1'))->default('0');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('languages');
	}

}
