<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMasterCylindersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('master_cylinders', function(Blueprint $table)
		{
			$table->bigInteger('master_cylinder_id', true)->unsigned();
			$table->string('cylinder_name');
			$table->integer('actual_value');
			$table->float('cylinder_price');
			$table->bigInteger('sort_order');
			$table->enum('blocked', array('0','1'))->default('0');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('master_cylinders');
	}

}
