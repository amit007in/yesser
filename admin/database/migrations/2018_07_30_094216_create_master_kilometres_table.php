<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMasterKilometresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('master_kilometres', function(Blueprint $table)
		{
			$table->bigInteger('master_kilometre_id', true)->unsigned();
			$table->string('kilometre_name');
			$table->integer('range_form');
			$table->integer('range_to');
			$table->bigInteger('sort_order');
			$table->enum('blocked', array('0','1'))->default('0');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('master_kilometres');
	}

}
