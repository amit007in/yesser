<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMasterLitresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('master_litres', function(Blueprint $table)
		{
			$table->bigInteger('master_litre_id', true)->unsigned();
			$table->string('litre_name');
			$table->bigInteger('actual_value');
			$table->float('litter_price');
			$table->bigInteger('sort_order');
			$table->enum('water_type', array('DRINKING','NON-DRINKING'))->nullable()->default('DRINKING');
			$table->enum('blocked', array('0','1'))->default('0');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('master_litres');
	}

}
