<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserCardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_cards', function(Blueprint $table)
		{
			$table->bigInteger('user_card_id', true)->unsigned();
			$table->bigInteger('user_id')->unsigned()->index('user_id');
			$table->string('card_id', 100);
			$table->string('fingerprint', 100);
			$table->string('last4', 4);
			$table->string('brand', 20);
			$table->enum('is_default', array('0','1'))->default('0');
			$table->enum('deleted', array('0','1'))->default('0');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_cards');
	}

}
