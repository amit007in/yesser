<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_details', function(Blueprint $table)
		{
			$table->bigInteger('user_detail_id', true)->unsigned();
			$table->bigInteger('user_id')->unsigned()->index('user_id');
			$table->bigInteger('user_type_id')->unsigned()->index('user_type_id');
			$table->bigInteger('language_id')->unsigned()->default(1)->index('language_id');
			$table->string('otp', 4);
			$table->dateTime('otp_validity')->nullable();
			$table->text('password', 65535);
			$table->string('profile_pic', 100)->nullable();
			$table->float('latitude', 7, 4);
			$table->float('longitude', 7, 4);
			$table->string('timezone', 100)->default('Asia/Kolkata');
			$table->string('timezonez', 10)->default('+05:30');
			$table->string('socket_id')->nullable();
			$table->string('fcm_id', 500);
			$table->enum('blocked', array('0','1'))->default('0');
			$table->string('forgot_token', 100)->nullable();
			$table->dateTime('forgot_token_validity')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_details');
	}

}
