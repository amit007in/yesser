<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->bigInteger('user_id', true)->unsigned();
			$table->bigInteger('organisation_id')->unsigned();
			$table->string('stripe_customer_id', 100);
			$table->string('stripe_connect_id', 100);
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email');
			$table->string('phone_code', 5);
			$table->bigInteger('phone_number')->index('phone_number');
			$table->string('facebook_id', 100)->nullable();
			$table->string('gmail_id', 100)->nullable();
			$table->string('strip_connect_token', 100);
			$table->enum('blocked', array('0','1'))->default('0');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
