<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCategoryBrandProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('category_brand_products', function(Blueprint $table)
		{
			$table->foreign('category_id', 'category_brand_products_ibfk_1')->references('category_id')->on('categories')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('category_brand_id', 'category_brand_products_ibfk_2')->references('category_brand_id')->on('category_brands')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('category_brand_products', function(Blueprint $table)
		{
			$table->dropForeign('category_brand_products_ibfk_1');
			$table->dropForeign('category_brand_products_ibfk_2');
		});
	}

}
