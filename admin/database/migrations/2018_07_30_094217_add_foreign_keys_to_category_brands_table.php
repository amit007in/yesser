<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCategoryBrandsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('category_brands', function(Blueprint $table)
		{
			$table->foreign('category_id', 'category_brands_ibfk_1')->references('category_id')->on('categories')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('category_brands', function(Blueprint $table)
		{
			$table->dropForeign('category_brands_ibfk_1');
		});
	}

}
