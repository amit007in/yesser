<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCategoryDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('category_details', function(Blueprint $table)
		{
			$table->foreign('category_id', 'category_details_ibfk_1')->references('category_id')->on('categories')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('language_id', 'category_details_ibfk_2')->references('language_id')->on('languages')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('category_details', function(Blueprint $table)
		{
			$table->dropForeign('category_details_ibfk_1');
			$table->dropForeign('category_details_ibfk_2');
		});
	}

}
