<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_details', function(Blueprint $table)
		{
			$table->foreign('user_id', 'user_details_ibfk_1')->references('user_id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_type_id', 'user_details_ibfk_2')->references('user_type_id')->on('user_types')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('language_id', 'user_details_ibfk_3')->references('language_id')->on('languages')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_details', function(Blueprint $table)
		{
			$table->dropForeign('user_details_ibfk_1');
			$table->dropForeign('user_details_ibfk_2');
			$table->dropForeign('user_details_ibfk_3');
		});
	}

}
