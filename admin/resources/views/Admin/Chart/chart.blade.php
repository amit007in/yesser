@extends('Admin.layout')

@section('title')
    Chart Analysis
@stop

@section('content')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
<link href="{{ URL::asset('AdminAssets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('AdminAssets/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('AdminAssets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
	<style>
	.bc-accordian .ibox-content{
	display: none;
	}
	</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Chart Analysis
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_chartanalysis') }}">
                                <b>
                                    Chart Analysis
                                </b>
                            </a>
                        </li>
                    </ol>
                </div>
            </div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Total Orders (%age Wise)</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<div id="container1" style="min-width: 100%; height: 100%; margin: 0 auto"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<meta name="csrf-token" content="{{ csrf_token() }}">
					<div class="row">
						<div style="margin-left: 5px;" class="col-lg-4 col-md-4">
							<div class="form-group">
								<label class="pull-left">Select Order Type</label>
								<select class="form-control categories" id="order_type" name="order_type" required="true">
									<option value="1" title="Yearly" selected>Yearly</option>
									<option value="2" title="Quarterly">Quarterly</option>
								</select>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<label class="pull-left">Filter</label><br>
							<button type="button" style="margin-left: -31px" onclick="getchangeorderType()"; class="btn btn-primary btn-big">Filter</button>
						</div>
					</div>
                </div>
            </div>
			<div class="row" id="yearlyorders">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Total Yearly Orders</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<div id="container5" style="min-width: 100%; height: 100%; margin: 0 auto"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row" id="quaterlyorders" style="display:none;">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Total Quarterly Orders</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<div id="container12" style="min-width: 100%; height: 100%; margin: 0 auto"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<meta name="csrf-token" content="{{ csrf_token() }}">
					<div class="row">
						<div style="margin-left: 5px;" class="col-lg-4 col-md-4">
							<div class="form-group">
								<label class="pull-left">Select User Type</label>
								<select class="form-control categories" id="user_type" name="user_type" required="true">
									<option value="" title="Select">Select</option>
									<option value="1" title="Customer">Customer</option>
									<option value="2" title="Driver">Driver</option>
									<option value="3" title="Organizational Driver">Organizational Driver</option>
								</select>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<label class="pull-left">Filter</label><br>
							<button type="button" style="margin-left: -31px" onclick="getUserTypes()"; class="btn btn-primary btn-big">Filter</button>
						</div>
					</div>
                </div>
            </div>
			<div class="row bc-accordian">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Registration Rate</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<div id="container8" style="min-width: 100%; height: 100%; margin: 0 auto"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row bc-accordian">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Login Rate</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<div id="container9" style="min-width: 100%; height: 100%; margin: 0 auto"></div>
						</div>
					</div>
				</div>
			</div>
            <div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<!--<form method="post" action="{{ route('admin_chartfilter') }}">-->
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<meta name="csrf-token" content="{{ csrf_token() }}">
						<div class="row">
							<div style="margin-left: 5px;" class="col-lg-4 col-md-4">
								<div class="form-group">
									<label class="pull-left">Categories</label>
									<select onchange="load_brands(this.value)" class="form-control categories" id="categories" name="categories" required="true">
										<option value="" title="Select">Select Category</option>
										@foreach($categories as $category)
											<option value="{{$category->category_id}}" title="{{$category->name}}">
												{{ $category->name }}
											</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class='col-lg-4 col-md-4 col-sm-4'>
                                <label>Brands</label>
                                <select class="brandids form-control border-info" id="brandids" required="true" name="brandids" >
                                </select>
                            </div> 
							<div class="col-lg-3 col-md-3 col-sm-3">
								<label class="pull-left">Filter</label><br>
								<button type="button" style="margin-left: -31px" onclick="getOrderTypes()"; class="btn btn-primary btn-big">Filter</button>
							</div>
						</div>
					<!--</form>-->
                </div>
            </div>
			<div class="row bc-accordian">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Order Types</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<div id="container2" style="min-width: 100%; height: 100%; margin: 0 auto"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row bc-accordian">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Product Types On Demand</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<div id="container4" style="min-width: 100%; height: 100%; margin: 0 auto"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row bc-accordian">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Monthly Total Orders</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<div id="container3" style="min-width: 100%; height: 100%; margin: 0 auto"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row bc-accordian">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Price and On Demand Availablity</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<div id="container6" style="min-width: 100%; height: 100%; margin: 0 auto"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row bc-accordian">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Average Monthly YesSer Data</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<div id="container7" style="min-width: 100%; height: 100%; margin: 0 auto"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<!--<form method="post" action="{{ route('admin_chartfilter') }}">-->
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<meta name="csrf-token" content="{{ csrf_token() }}">
						<div class="row">
							<div style="margin-left: 5px;" class="col-lg-4 col-md-4">
								<div class="form-group">
									<label class="pull-left">Categories</label>
									<select class="form-control ucategories" id="ucategories" name="ucategories" required="true">
										<option value="" title="Select">Select Category</option>
										@foreach($categories as $category)
											<option value="{{$category->category_id}}" title="{{$category->name}}">
												{{ $category->name }}
											</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-lg-4 col-md-4">
								<label>
									Filter Booking Yearly
								</label>
								<center>
								<div class="input-group date">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" value="<?php echo date("Y"); ?>" id="starting_date" name="starting_date">
								</div></center>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3">
								<label class="pull-left">Filter</label><br>
								<button type="button" style="margin-left: -31px;margin-top: 5px;" onclick="getuserMonthlyOrderData()"; class="btn btn-primary btn-big">Filter</button>
							</div>
						</div>
					<!--</form>-->
                </div>
            </div>
			<div class="row bc-accordian">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Monthly User Order Data</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<div id="container10" style="min-width: 100%; height: 100%; margin: 0 auto"></div>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
	<!-- Modal: modalCart -->
	<div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	  aria-hidden="true">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header" style="background-color: black;">
			<h4 class="modal-title" style="color: white;" id="myModalLabel">Customer List</h4>
			<button style="margin-top: -20px;opacity: 5;color: white;" type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">×</span>
			</button>
		  </div>
		  <div class="modal-body" style="height: 500px;overflow: auto;">
			<table class="table table-hover">
			  <thead>
				<tr>
				  <th>#</th>
				  <th>Customer name</th>
				  <th>Total Orders</th>
				</tr>
			  </thead>
			  <tbody id="appendList">
			  </tbody>
			</table>
		  </div>
		</div>
	  </div>
	</div>
	<!-- Modal: modalCart -->
	<div class="ajax_loader" id='loadingmessage' style='display:none;position:absolute;width:100%;z-index: 9999;height:100%;left:0;top:0;background:no-repeat center rgba(0,0,0,0.25)'>
	<img style="position: absolute;left: 50%;top: 20%;height: 70px;" src="{{ URL::asset('Images/Spinner.gif') }}"/>	
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
	<script src="{{ URL::asset('AdminAssets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
	<script src="{{ URL::asset('AdminAssets/js/plugins/select2/select2.full.min.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        $("#chartanalysis").addClass("active");
		//Total Orders For All Services
		new_get_dashboard_data();
		function new_get_dashboard_data()
		{
			$('#loadingmessage').css('display','block');
			var categories = $('#categories').val();
			route = '{!! route('admin_chartfilter') !!}?category='+categories;
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					graph_data(data);
					$('#loadingmessage').css('display','none');
				}
			});
		}
		
		function graph_data(data)
        {
            $('#container1').highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'pie'
                },
                title: {
                    text: 'Total Orders'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer'
					}
				},
                series: [{
					name: 'Categories',
					colorByPoint: true,
					data: data.CategoryOrder
				}]
            });
			
			$('#container5').highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Total Yearly Orders'
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'COUNT'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [
				{
					name: "Orders",
					colorByPoint: true,
					data: data.TotalOrders
				}]
            });
			
			//Get Quarlty Report
			$('#container12').highcharts({
			credits: {
                    enabled: false
              },
              chart: {
				type: 'column'
			  },
			  title: {
				text: 'Total Quarterly Orders'
			  },
			  xAxis: {
                    categories: data.Categoryname,
                },
			  yAxis: {
				title: {
					text: 'COUNT'
				}
			  },
			  tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
			  plotOptions: {
				column: {
				  pointPadding: 0.2,
				  borderWidth: 0
				}
			  },
			  series: [{
				name: 'Q1',
				data: data.TotalQuartlyOrders[0]

			  }, {
				name: 'Q2',
				data: data.TotalQuartlyOrders[1]

			  }, {
				name: 'Q3',
				data: data.TotalQuartlyOrders[2]

			  }, {
				name: 'Q4',
				data: data.TotalQuartlyOrders[3]

			  }]
            });
			
        }
		
		//Total Orders For All Services
		function getuserMonthlyOrderData()
		{
			var categories = $('#ucategories').val();
			if(categories == "")
			{
				alert("Kindly Select category name");
				return false;
			}
			var daterange = $('#starting_date').val();
			route = '{!! route('admin_userMonthlyOrderData') !!}?category='+categories+'&daterange='+daterange;
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					viewUserOrderGraph(data,daterange,categories);
					$('#starting_date').val(daterange);
				}
			});
		}
		function viewUserOrderGraph(data,daterange,categories)
		{
			$('#container10').highcharts({
                chart: {
					type: 'column'
				},
				title: {
					text: 'Stacked column chart'
				},
				xAxis: {
					categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
				},
				yAxis: {
					min: 0,
					title: {
						text: 'Count'
					}
				},
				tooltip: {
					pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
					shared: true
				},
				plotOptions: {
					column: {
						stacking: 'percent'
					}
				},
				series: [{
					name: 'Total Customer',
					data: data.TotalCustomer
				}, {
					name: 'Active Customer',
					data: data.Activecustomers
				}, {
					name: 'Customer Orders',
					data: data.orders,
					cursor: 'pointer',
					point: {
						events: {
							click: function () {
								ShowCustomers(this.category,daterange,categories);
							}
						}
					}
				}]
            });
			
		}
		function ShowCustomers(month,year,categories)
		{
			route = '{!! route('admin_UserCustomerLIst') !!}?month='+month+'&year='+year+'&categories='+categories;
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					console.log(data);
					$('#appendList').empty();
					var HTML = "";
					for(var i =0;i<data.length;i++)
					{
						var cusername     = data[i].Username;
						var UserId        = data[i].id;
						var Orders        = data[i].TotalOrder;
						HTML += "<tr>";
						HTML += "<td>"+UserId+"</td>";
						HTML += "<td>"+cusername+"</td>";
						HTML += "<td>"+Orders+"</td>";
						HTML += "</tr>";
					}
					if(HTML == "")
					{
						HTML += "<tr>";
						HTML += "<td colspan='3'>No Customer Found</td>";
						HTML += "</tr>";
					}
					$('#appendList').html(HTML);
					$('#modalCart').modal('show');
				}
			});
		}
		//Total Orders For All Services
		function getOrderTypes()
		{
			//alert($(".wrapper").scrollTop() + " px");
			$('#loadingmessage img').css('top','35%');
			$('#loadingmessage').css('display','block');
			var categories = $('#categories').val();
			if(categories == "")
			{
				alert("Kindly Select category name");
				$('#loadingmessage').css('display','none');
				return false;
			}
			var brandids   = $('#brandids').val();
			if(brandids == "")
			{
				alert("Kindly Select Product name");
				$('#loadingmessage').css('display','none');
				return false;
			}
			route = '{!! route('admin_orderTypeChart') !!}?category='+categories+'&brandids='+brandids;
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					$('#loadingmessage').css('display','none');
					viewOrderTypeGraph(data);
					//getuserMonthlyOrderData();
				}
			});
		}
		function viewOrderTypeGraph(data)
        {
            $('#container2').highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'pie'
                },
                title: {
                    text: 'Order Types'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer'
					}
				},
                series: [{
					name: 'Categories',
					colorByPoint: true,
					data: [{
						name: 'Scheduled',
						y: data.sorder,
						sliced: true,
						selected: true
					},
					{
						name: 'On Demand Orders',
						y: data.dorder
					}]
				}]
            });
			
			$('#container4').highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'pie'
                },
                title: {
                    text: 'Product Types On Demand'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer'
					}
				},
                series: [{
				name: 'Categories',
				colorByPoint: true,
				data: data.products
			}]
            });
			
			$('#container3').highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Monthly Total Orders'
                },
                xAxis: {
                    categories: [
                        'January',
                        'February',
                        'March',
                        'April',
                        'May',
                        'June',
                        'July',
                        'August',
                        'September',
                        'October',
                        'November',
                        'December'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    allowDecimals: false,
                    title: {
                        text: 'COUNT'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [
                {
                    name: 'Orders',
                    data: data.orders,
                    color: "#3c8dbc"
                }
                ]
            });
			
			$('#container6').highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Monthly Average Stats'
                },
                xAxis: {
                    categories: [
                        'January',
                        'February',
                        'March',
                        'April',
                        'May',
                        'June',
                        'July',
                        'August',
                        'September',
                        'October',
                        'November',
                        'December'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    allowDecimals: false,
                    title: {
                        text: 'COUNT(OMR)'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
					column: {
						pointPadding: 0.2,
						borderWidth: 0
					}
				},
                series: data.Brands
            });
			
			$('#container7').highcharts({
                chart: {
					zoomType: 'xy'
				},
				title: {
					text: 'Average Monthly YesSer Data',
					align: 'left'
				},
				xAxis: [{
					categories: data.CategoryRevenue.Dates,
                    crosshair: true
				}],
				yAxis: [{
                    min: 0,
                    allowDecimals: false,
				    tickInterval: 50,
				    lineWidth: 1,
                    title: {
                        text: 'COUNT(OMR)'
                    }
                }],
				tooltip: {
					shared: true
				},
				legend: {
					layout: 'vertical',
					align: 'left',
					x: 80,
					verticalAlign: 'top',
					y: 55,
					floating: true,
					backgroundColor:
						Highcharts.defaultOptions.legend.backgroundColor || // theme
						'rgba(255,255,255,0.25)'
				},
				series: data.CategoryRevenue.Orders,
				responsive: {
					rules: [{
						condition: {
							maxWidth: 500
						},
						chartOptions: {
							legend: {
								floating: false,
								layout: 'horizontal',
								align: 'center',
								verticalAlign: 'bottom',
								x: 0,
								y: 0
							}
						}
					}]
				}
			  
            });
			

        }
		
		//Total Users
		function getUserTypes()
		{
			var categories = $('#user_type').val();
			if(categories == "")
			{
				alert("Kindly Select user type");
				return false;
			}
			route = '{!! route('newRegistrationRate') !!}?users='+categories;
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					viewUserTypeGraph(data);
				}
			});
		}
		function viewUserTypeGraph(data)
		{
			$('#container8').highcharts({
                chart: {
					type: 'column'
				},
				title: {
					text: 'Registration Rate chart'
				},
				xAxis: {
					categories: data.Dates
				},
                
				credits: {
					enabled: false
				},
				series: [{
					name: 'Registration Rate (%)',
					data: data.Users
				},{
					name: 'Registration Users (Count)',
					data: data.TotalUsers
				}]
            });
			$('#container9').highcharts({
                chart: {
					type: 'column'
				},
				title: {
					text: 'Login Rate chart'
				},
				xAxis: {
					categories: data.Dates
				},
				credits: {
					enabled: false
				},
				series: [{
					name: 'Login Rate (%)',
					data: data.LoginUsers.Percentage
				},{
					name: 'Login Users (Count)',
					data: data.LoginUsers.TotalUsers
				}]
            });
		}
		
		
		
		 $("#brandids").select2({
          dropdownCssClass: "bigdrop",
          placeholder: "Select Brands"
        });
		$("#categories").select2({
          dropdownCssClass: "bigdrop",
          placeholder: "Select Category"
        });
		$("#user_type").select2({
          dropdownCssClass: "bigdrop",
          placeholder: "Select User Type"
        });
		function load_brands(category_id)
        {
            $.ajax({
                type: "POST",
                cache: false,
                dataType: "json",
                url: '{{route("brands_listings")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    category_id: category_id,
                    admin_language_id: '{{$admin->admin['language_id']}}'
                },
                success:function(result){
                    if(result.success == 1)
                        {
                            $("#brandids").select2("val", "");
                            $('#brandids').html(result.con);
                        }
                    else
                        {
                            toastr.error('Error.', result.msg );
                        }
                },
                error: function(result){
                    toastr.error('Error.', result.msg );
                }
            }); 

        }
		$(document).ready(function()
            {
				$("#starting_date").datepicker( {
                    format: "yyyy",
                    viewMode: "years", 
                    minViewMode: "years",
					autoclose: true
				});
                $('input[name="daterange"]').daterangepicker({
                    opens: 'center',
                    autoApply: true,
                    format: 'MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('MM/YYYY'),
                });

            });
		$("#order_type").select2({
          dropdownCssClass: "bigdrop",
          placeholder: "Select Order Type"
        });
		function getchangeorderType()
		{
			$result = $('#order_type').val();
			if($result == '1')
			{
				$('#quaterlyorders').css('display','none');
				$('#yearlyorders').css('display','block');
			}
			if($result == '2')
			{
				$('#yearlyorders').css('display','none');
				$('#quaterlyorders').css('display','block');
			}
			
		}
	</script>
@stop
