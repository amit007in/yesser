@extends('Admin.layout')

@section('title')
    Contact Us
@stop

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
			<meta name="csrf-token" content="{{ csrf_token() }}">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Contact Us
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_contactus')}}"><b>Contact Us</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                <div class="row">

                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="form-group">
                            <label class="pull-left">Status</label>
                                <select class="form-control" id="filter1">
                                    <option value="">All</option>
                                    <option value="Customer">Customer</option>
                                    <option value="Service">Service</option>
                                    <!--<option value="Support">Support</option>-->
                                </select>                 
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4">
                                    <label>
                                        Filter Created At
                                    </label>

                                    <center>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
                                    </center>                        
                    </div>
<a class="btn btn-success btn-big" style="margin-top: 23px;" id="export_excell" onclick="export_excell()">Export Excel</a>
                </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Created By</th>
                                    <th>Message</th>
                                    <th>Name</th>
                                    <th>ProfilePic</th>
                                    <th>Reply</th>
                                    <th>History</th>
                                    <th>Created At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($messages as $message)
                <tr class="gradeA footable-odd">

                    <td>{{ $message->admin_contactus_id }}</td>

                    <td>
                        <center>
                            @if($message->user_type_id == '1')
                                <span class="label label-primary">Customer</span>
                            @elseif($message->user_type_id == '2')
                                <span class="label label-warning">Service</span>
                            @else
                                <span class="label label-success">Support</span>
                            @endif
                        </center>
                    </td>

                    <td>
                        {{ $message->message }}
                    </td>

                    <td>
                        {{ $message['user']->name }} ({{ $message['user']->phone_number }})
                    </td>

                    <td>
					@if($message['user_detail']->profile_pic == "")
						<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $message['user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
						</a>
					 @else
						<a href="{{ env('SOCKET_URL').'/Uploads/'.$message['user_detail']->profile_pic }}" title="{{ $message['user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ env('SOCKET_URL').'/Uploads/'.$message['user_detail']->profile_pic }}"  class="img-circle">
						</a>
					 @endif	
 
                    </td>

                    <td>
    <a class="btn btn-success" href="#" data-toggle="modal" data-target="#modal-form2" onclick="update_contact_id('{{$message->admin_contactus_id}}')">
        <i class="fa fa-pencil" aria-hidden="true"> Reply</i>
    </a>
                    </td>

                    <td>
                        <a href="{{route('admin_contactus_history',['admin_contactus_id' => $message->admin_contactus_id])}}" class="btn btn-default">
                            {{ $message->history_counts }}
                        </a>
                    </td>

                    <td>
                        <i class="fa fa-clock-o"> {{ $message->created_atz }}</i>
                    </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>

        </div>
    </div>
</div>

    <!--                Reply Modal                                -->
    <div class="modal inmodal modal-form2 modal-bg" id="modal-form2" role="dialog" aria-hidden="false">
        <div class="modal-dialog">
            <form method="post" action="{{route('admin_contactus_preply')}}" enctype="multipart/form-data" >
                <div class="modal-content animated swing">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <i class="fa fa-pencil modal-icon"></i>
                        <h4 class="modal-title">Reply</h4>
                    </div>
                    <div class="modal-body">
                        <label class="pull-left">Reply</label>
                        <textarea name="message" required class="form-control" autofocus="on">{{Form::old('message')}}</textarea>
                        <br>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="modal-footer">

                        <input hidden="on" value="" name="parent_admin_contactus_id" id="parent_admin_contactus_id"></input>

                        <button type="submit" class="btn btn-primary" id="add_rform_btn">Reply</button>
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--                Reply Modal                                -->

    <script>

    $("#contactus").addClass("active");

    // $('#admin_reply').click(function (e)
    //     {

    //     });

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

        $('#daterange').on('apply.daterangepicker', function(ev, picker)
            {

                dt = document.getElementById('daterange').value;

                window.location.href = '{{route("admin_contactus")}}?daterange='+dt;

            });

        });

            var table = $('#example').dataTable( 
                {
                   "order": [[ 0, "desc" ]],
                   // "scrollX": true,
                   // "scrollY": 500,
                   "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [ 5 ] },
                                { "bSearchable": true, "aTargets": [ 2, 4 ] }
                    ]
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });

            $("#filter2").on('change', function()
                {
                    table.fnFilter($(this).val(), 2);
                });

        function update_contact_id(admin_contactus_id)
            {
                document.getElementById('parent_admin_contactus_id').value = admin_contactus_id;
            }
		function export_excell()
            {
                var daterange = $('#daterange').val();
				console.log(daterange);

                toastr.info('Info.', 'Excell is getting prepared. Please wait...');
                $('#export_excell').attr("disabled", "disabled");

                $.ajax({
                    dataType: "json",
                    url: '{{route("admin_contacts_preport")}}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    data: {daterange},
                    success:  function(result){

                        $('#export_excell').attr("disabled", false);

                        window.location.href = result.link;
                        console.log(result);
                
                    }
                });

            }
    </script>


@stop
