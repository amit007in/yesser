@extends('Admin.layout')

@section('title')
    Contact Us History
@stop

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Contact Us History
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_contactus')}}">Contact Us</a>
                        </li>
                        <li>
                            <a href="{{route('admin_contactus_history',['admin_contactus_id' => $msg->admin_contactus_id])}}"><b>Contact Us History</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                <div class="row">

    <div class="row">
        <div class="col-md-6 col-lg-offset-3">
            <div class="ibox-content text-center">
                <h2>{{ $msg['user']->name }}</h2>
                    <div class="m-b-sm">
                            <a href="{{ $msg['user_detail']->profile_pic_url }}" title="{{ $msg['user']->name }}" class="lightBoxGallery" data-gallery="">
                                <img src="{{ $msg['user_detail']->profile_pic_url }}" class="img-circle " alt="profile" >
                            </a>
                            <br>
                            <center>
                                {{$msg->message}}
                            </center>
                            <br>
    <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#modal-form2" data-admin_contactus_id="{{$msg->admin_contactus_id}}" id="admin_reply">
        <i class="fa fa-pencil" aria-hidden="true"> Reply</i>
    </a>

                    </div>
            </div>
        </div>
    </div>

<!--         <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="form-group">
                <label class="pull-left">Order Type</label>
                    <select class="form-control" id="filter1">
                        <option value="">All</option>
                        <option value="Customer">Customer</option>
                        <option value="Service">Service</option>
                        <option value="Support">Support</option>
                        <option value="Admin">Admin</option>
                    </select>
            </div>
        </div>
 -->
<!--         <div class="col-lg-6 col-md-6 col-sm-6">
        </div>
 -->

                </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <!-- <th>Created By</th> -->
                                    <th>Message</th>
                                    <th>Created At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($msg->replies as $message)
                <tr class="gradeA footable-odd">

                    <td>{{ $message->admin_contactus_id }}</td>

<!--                     <td>
                        <center>
                            @if($message->user_type_id == '1')
                                <span class="label label-primary">Customer</span>
                            @elseif($message->user_type_id == '2')
                                <span class="label label-warning">Service</span>
                            @elseif($message->user_type_id == '3')
                                <span class="label label-success">Support</span>
                            @else
                                <span class="label label-info">Admin</span>
                            @endif
                        </center>
                    </td>

 -->                    <td>
                        {{ $message->message }}
                    </td>

                    <td>
                        <i class="fa fa-clock-o"> {{ $message->created_atz }}</i>
                    </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>

        </div>
    </div>
</div>

    <!--                Reply Modal                                -->
    <div class="modal inmodal modal-form2 modal-bg" id="modal-form2" role="dialog" aria-hidden="false">
        <div class="modal-dialog">
            <form method="post" action="{{route('admin_contactus_preply')}}" enctype="multipart/form-data" >
                <div class="modal-content animated swing">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <i class="fa fa-pencil modal-icon"></i>
                        <h4 class="modal-title">Reply</h4>
                    </div>
                    <div class="modal-body">
                        <label class="pull-left">Reply</label>
                        <textarea name="message" required class="form-control" autofocus="on">{{Form::old('message')}}</textarea>
                        <br>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="modal-footer">

                        <input hidden="on" value="" name="parent_admin_contactus_id" id="parent_admin_contactus_id"></input>

                        <button type="submit" class="btn btn-primary" id="add_rform_btn">Reply</button>
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--                Reply Modal                                -->

    <script>

    $("#contactus").addClass("active");

    $('#admin_reply').click(function (e)
        {
            document.getElementById('parent_admin_contactus_id').value = $(this).data('admin_contactus_id');
        });


            var table = $('#example').dataTable( 
                {
                   "order": [[ 0, "desc" ]],
                   "scrollX": true,
                   "scrollY": 500,
                });

            // $("#filter1").on('change', function()
            //     {
            //         table.fnFilter($(this).val(), 1);
            //     });

    </script>


@stop
