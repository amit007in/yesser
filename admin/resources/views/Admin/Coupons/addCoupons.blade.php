@extends('Admin.layout')

@section('title') 
    Add Coupon
@stop

    @section('content')

<link href="{{ URL::asset('AdminAssets/css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add Coupon</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_coupons_all')}}">All Coupons</a>
                        </li>
                        <li>
                            <a href="{{route('admin_coupons_aget') }}">
                                <b>Add Coupon</b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

        <br>
            <form method="post" action="{{route('admin_coupons_apost') }}" enctype="multipart/form-data" id="addForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Coupon Code</label>
                <input type="text" placeholder="Coupon Code" class="form-control" required name="code" value="{!! Form::old('code') !!}" autofocus="on" id="code" maxlength="30">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Maximum Rides</label>
                <input type="number" placeholder="Maximum Rides" class="form-control" required name="rides_value" value="{!! Form::old('rides_value') !!}" autofocus="on" id="rides_value" min="1">
            </div> 

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">

            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Coupon Price</label>
                <input type="number" placeholder="Coupon Price" class="form-control" required name="price" value="{!! Form::old('price') !!}" id="price" step="any" min="0">
            </div> 
           
            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
            <label>Expires At</label>
    <div class="input-group date">
    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="expires_at" id="expires_at" value="{{Request::old('expires_at')}}" placeholder="Expires At">
    </div>
            </div> 

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Coupon Type</label>
                <select name="coupon_type" class="form-control" onchange="change_value_input(this.value)">
                    <option value="Percentage">Percentage</option>
                    <option value="Value">Value</option>
                </select>
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Coupon Value</label>
                <input type="number" placeholder="Coupon Value" class="form-control" required name="amount_value" value="{!! Form::old('amount_value') !!}" id="amount_value" step="any" min="0">
            </div> 

        </div>
    </div>
<br>

    <div class="row">
        <div class="form-group">

            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Coupon Expiry Days</label>
                <input type="number" placeholder="Coupon Expiry Days" class="form-control" required name="expiry_days" value="{!! Form::old('expiry_days') !!}" id="expiry_days" min="1">
            </div> 
           
        </div>
    </div>
<br>



            <br><br><br>

                <div class="row">
                    <div class="form-group">
                        <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            {!! Form::submit('Add Coupon', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">

    $("#settings_all").addClass("active");
    $("#coupons_all").addClass("active");

    checkSuccess = function(response) {

            switch (jQuery.parseJSON(response).statuscode) {
                case 200:
                    return true;
                case 400:
                    return false;
            }
            return true;
        };

        $("#addForm").validate({
            rules: {
                code: {
                    remote: {
                            url: "{{ROUTE("coupon_unique_check")}}",
                            type: "POST",
                            cache: false,
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                code: function() { return $("#code").val(); }
                            },
                            dataFilter: function(response) {
                                return checkSuccess(response);
                            }
                        }

                } 
            },
            messages: {
                code: {
                    required: "Coupon Code is required",
                    remote: "Sorry, this coupon code is already taken"
                },
                rides_value: {
                    required: "Maximum rides parameter is required"
                },
                amount_value: {
                    required: "Coupon value is required"
                },
                price: {
                    required: "Price is required"
                },
                expiry_days: {
                    required: "Expiry days is required"
                }

            }
        });

    $('#expires_at').datetimepicker({
        startDate: new Date(),
        format: 'yyyy-mm-dd hh:ii:ss'
    });

    function change_value_input(value)
        {
            if(value == "Percentage")
            {
                document.getElementById("amount_value").max = 100;
            }
            else
            {
                $("#amount_value").removeAttr("max");
            }
        }

        if("{{Form::old('coupon_type')}}")
            change_value_input("{{Form::old('coupon_type')}}");
        else
            change_value_input("Percentage");

</script>


@stop



