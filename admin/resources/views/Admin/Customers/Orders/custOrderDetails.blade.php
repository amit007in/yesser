@extends('Admin.layout')

@section('title') 
    Order Details
@stop

@section('content')

<style>
  html, body, #map-canvas {
    height: 400px;
    margin: 0px;
    padding: 0px
  }
</style>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="row">

                <div class="col-lg-10">
                    <h2>
                        Order Details
                    </h2>
                    
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('org_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_custs')}}"> Customers</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_cust_orders',['user_detail_id' => $cust->user_detail_id]) }}">
                                Customer Orders
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin_cust_order_details',['order_id'=>$order->id])}}">
                                <b>
                                    Order Details
                                </b>                            
                            </a>
                        </li>
                    </ol>
                </div>
                
                <div class="col-lg-2">

                </div>
            
            </div>
        </div>

        <div class="row">
<!--            Map Canvas                            -->
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div id="map1" style="width:100%; height:450px;"></div>
            </div>
<!--            Map Canvas                            -->
        </div>

        <div class="row">

<!--            Order Details                         -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Order Info</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Status</th>
                            <th>Timing</th>
                            <th>Pickup Location</th>
                            <th>Dropoff Location</th>
                            <th>Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                <tr>

                    <td>{{ $order->order_id }}</td>

                    <td>
                        @include('Common.Partials.orderStatus')
                    </td>


                    <td><i class="fa fa-clock-o"></i> {{ $order->order_timingsz }} </td>

                    <td>
                        {{ $order->pickup_address }}
                    </td>

                    <td>
                        {{ $order->dropoff_address }}
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order->created_atz }} </td>

                </tr>

                    </tbody>
                </table>

        </div>

            </div>

        </div>

    </div>
<!--            Order Details                         -->

<!--            Product Details                      -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Product Info</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Product</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                <tr>

                    <td>{{ $order['category_brand_product']->category_brand_product_id }}</td>

                    <td>{{ $order['category']->name }}</td>

                    <td>{{ $order['category_brand']->name }}</td>

                    <td>{{ $order['category_brand_product']->name }}</td>

                    <td>
                        {{ $order->product_quantity }}
                    </td>

                </tr>

                    </tbody>
                </table>

        </div>

            </div>

        </div>

    </div>
<!--            Product Details                      -->

<!--            Payment Details                         -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Payment Info</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Transaction Id</th>
                            <th>Status</th>
                            <th>Method</th>
                            <th>Alpha Charges</th>
                            <th>Admin Charges</th>
                            <th>Bank Charges</th>
                            <th>Total Charges</th>
                        </tr>
                    </thead>
                    <tbody>
                <tr>

                    <td>
                        {{ $order->transaction_id }}
                    </td>

                    <td>
                        <span class="label label-primary">
                            {{ $order->payment_status }}
                        </span>
                    </td>


                    <td>
                        @if($order->payment_type  == "Cash")
                            <span class="label label-success">
                                Cash
                            </span>
                        @elseif($order->payment_type  == "Card")
                            <span class="label label-primary">
                                Card
                            </span>
                        @else
                            <span class="label label-info">
                                EToken
                            </span>
                        @endif
                    </td>

                    <td>
                        ORM {{ $order->product_alpha_price }}
                    </td>

                    <td>
                        OMR {{ $order->admin_charge }}
                    </td>

                    <td>
                        OMR {{ $order->bank_charge }}
                    </td>

                    <td>
                        ORM {{ $order->final_charge }}
                    </td>

                </tr>

                    </tbody>
                </table>

        </div>

            </div>

        </div>

    </div>
<!--            Payment Details                         -->

<!--            Coupon Details                         -->
@if($order->organisation_coupon_user)
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Coupon Info</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Purchased At</th>
                            <th>Coupon Price</th>
                            <th>Amount Left</th>
                            <th>Expires At</th>
                        </tr>
                    </thead>
                    <tbody>
                <tr>

                    <td>
                        {{ $order['organisation_coupon_user']->organisation_coupon_id }}
                    </td>

                    <td>
                        <i class="fa fa-clock-o"> {{ $order['organisation_coupon_user']->created_atz }}</i>
                    </td>

                    <td>
                       OMR {{ $order['organisation_coupon_user']->price }}
                    </td>

                    <td>
                        {{ $order['organisation_coupon_user']->quantity_left }}
                    </td>

                    <td>
                        <i class="fa fa-clock-o"> {{ $order['organisation_coupon_user']->expiries_atz }}</i>
                    </td>

                </tr>

                    </tbody>
                </table>

        </div>

            </div>

        </div>

    </div>
@endif
<!--            Coupon Details                         -->

<!--            Customer Details                         -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Customer Info</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>ProfilePic</th>
                            <th>Ordered At</th>
                        </tr>
                    </thead>
                    <tbody>
                <tr>

                    <td>
                        {{ $cust->user_detail_id }}
                    </td>

                     <td>
                        {{ $cust['user']->name }} ({{ $cust['user']->phone_number }})
                    </td>

                    <td>
    <a href="{{ $cust->profile_pic_url }}" title="{{ $cust['user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
        <img src="{{ $cust->profile_pic_url}}"  class="img-circle">
    </a>                        
                    </td>

                    <td>
                        <i class="fa fa-clock-o"> {{ $order->created_atz }}</i>
                    </td>

                </tr>

                    </tbody>
                </table>

        </div>

            </div>

        </div>

    </div>
<!--            User Details                         -->


<!--            Ratings                              -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Ratings Given By</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">

            <div class="col-sm-6 col-md-6 col-lg-6">
                <center><h4>Customer</h4></center>
                <table class="table table-hover">
                    <thead>
                        <tr>
<!--                             <th>Id</th> -->
                            <th width="5%">Ratings</th>
                            <th width="80%">Comments</th>
                            <th width="15%">Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($order['customer_rating'])

                <tr>

<!--                     <td>
                        {{ $order['customer_rating']->order_rating_id }}
                    </td>
 -->
                     <td>
                        <i class="fa fa-star-half-empty"></i> {{ $order['customer_rating']->ratings }}
                    </td>

                    <td>
                        {{ $order['customer_rating']->comments }}
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order['customer_rating']->created_atz }} </td>

                </tr>


                        @endif
                    </tbody>
                </table>                
            </div>


            <div class="col-sm-6 col-md-6 col-lg-6">
                <center><h4>Driver</h4></center>
                <table class="table table-hover">
                    <thead>
                        <tr>
<!--                             <th>Id</th> -->
                            <th width="5%">Ratings</th>
                            <th width="80%">Comments</th>
                            <th width="15%">Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($order['driver_rating'])

                <tr>

<!--                     <td>
                        {{ $order['driver_rating']->order_rating_id }}
                    </td>
 -->
                     <td>
                        <i class="fa fa-star-half-empty"></i> {{ $order['driver_rating']->ratings }}
                    </td>

                    <td>
                        {{ $order['driver_rating']->comments }}
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order['driver_rating']->created_atz }} </td>

                </tr>


                        @endif
                    </tbody>
                </table>                
            </div>

        </div>

            </div>

        </div>

    </div>
<!--            Ratings                              -->

        </div>

<br><br><br>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key={{env('GOOGLE_MAP_KEY')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.dev.js"></script>

<script type="text/javascript">
         
    $("#customers").addClass("active");

    var socket = io.connect("{{env('SOCKET_URL')}}", { query: { admin_access_token: "{{$admin->access_token}}" } } );

    var pickup_address = "{{$order->pickup_address}}";
    var pickup_latitude = parseFloat("{{$order->pickup_latitude}}");
    var pickup_longitude = parseFloat("{{$order->pickup_longitude}}");

    var dropoff_address = "{{$order->dropoff_address}}";
    var dropoff_latitude = parseFloat("{{$order->dropoff_latitude}}");
    var dropoff_longitude = parseFloat("{{$order->dropoff_longitude}}");

    var order_status = "{{$order->order_status}}";

    var driver_latitude, driver_longitude, myMap, infowindow, bounds, pickupMarker, dropoffMarker, driverMarker, mapOptions, order_status, driver_icon, driver_new_longitude, driver_new_longitude, driver_rotation;
    
    ////console.log(pickup_latitude, pickup_longitude, dropoff_latitude, dropoff_longitude);

    // var roptions = {
    //             componentRestrictions: {country: ['om','in']}
    //         };

    function driver_icon_image(category_id)
        {
            if(category_id == 1)////////////       Gas     ////////////////
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_gas.png";
            else if(category_id == 2)////////////       Drinking Water  ///
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_drink_water.png";
            else if(category_id == 3)////////////       Water Tanker    ///
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_water_tank.png";
            else if(category_id == 4)////////////       Freight        /////////
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_freights.png";
            else if(category_id == 5)////////////       Tow        /////////////
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_tow.png";
            else if(category_id == 6)////////////       Heavy        ///////////
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_heavy.png";
            else
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/sup_icon.png";
        }

        driver_icon_image("{{$order->category_id}}");
        ////console.log('{{$order->category_id}}',driver_icon);

    function load_map()
        {

            mapOptions = {
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoom: 7,
                    center:new google.maps.LatLng(pickup_latitude,pickup_longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false
                };

            myMap = new google.maps.Map(document.getElementById('map1'), mapOptions);

            infowindow = new google.maps.InfoWindow();
            bounds = new google.maps.LatLngBounds();

/////////////// Pick up Marker  ///////////////////////
            pickupMarker = new google.maps.Marker({
                animation: google.maps.Animation.DROP,
                position: {"lat": pickup_latitude,"lng": pickup_longitude},
                title: 'Pickup - '+pickup_address,
                map: myMap,
                icon: "{{env('ADMIN_ASSET')}}Tracking/marker_pickup.png",
            });

            bounds.extend(pickupMarker.position);
            //addInfoWindow(pickupMarker, pickup_address);////    Add Info Window
/////////////// Pick up Marker  ///////////////////////

/////////////// DropOff Marker  ///////////////////////
            if(dropoff_latitude != 0.000000 && dropoff_longitude != 0.000000)
                {

                    dropoffMarker = new google.maps.Marker({
                        animation: google.maps.Animation.DROP,
                        position: {"lat": dropoff_latitude,"lng": dropoff_longitude},
                        title: 'Dropoff - '+dropoff_address,
                        map: myMap,
                        icon: "{{env('ADMIN_ASSET')}}Tracking/marker_drop.png"
                    });

                    bounds.extend(dropoffMarker.position);
                    //addInfoWindow(dropoffMarker, dropoff_address);////    Add Info Window

                }
/////////////// DropOff Marker  ///////////////////////

            if(order_status == "Ongoing" || order_status == "Confirmed")
                {
                    pollingFunction();
                    ////console.log('asdddddddddddddddddddddddddddddddddddddddddddddd', order_status);
                }
            else
                {
                    ////console.log('zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz', order_status);
                }
    
        // google.maps.event.addListener(myMap, 'click', function(event) {
        //     addPoint(event);
        // });

        }

var rotation = 10;

////console.log(google.maps.SymbolPath.FORWARD_CLOSED_ARROW, 'google.maps.SymbolPath.FORWARD_CLOSED_ARROW');

function addPoint(event) {

    marker = new google.maps.Marker({
        position: myMap.getCenter(),
        map: myMap,
        title: "MY MARKER",
        icon: {
            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
            scale: 4,
            strokeColor: '#00F',
            rotation: 0,
        }
    });
    // myMap.panTo(event.latLng);
    // ////console.log('marker.getIcon();', marker.getIcon());
}

    var increment = 1;
    function pollingFunction()
        {
            
            trackDriver();

            // if(order_status == "Ongoing" || order_status == "Confirmed")
            //     {
            //         setInterval( trackDriver, 70000);
            //     }
            // console.log('order_status', order_status);
        }

/*    function addInfoWindow(marker, message) 
        {

            if(infoWindow) {
               infoWindow.close();
            }

            var infoWindow = new google.maps.InfoWindow({
                content: message
            });

            google.maps.event.addListener(marker, 'click', function () {
                infoWindow.open(myMap, marker);
            });
        }*/

    function trackDriver()
        {
    ////console.log("TrackDriver");

            if(order_status == "Ongoing" || order_status == "Confirmed")
                return null;

        socket.emit("PanelCommonEvent", { type:"TrackDriver", order_id: parseInt("{{$order->order_id}}") }, (response) => {
            ////console.log(response);
            if(response.success == 0)
                {
                    toastr.error('Not able to track',response.msg);
                }
            else if(response.order && response.order.CRequest)
                {
                    order_status = response.order.order_status;

                    driver_latitude = parseFloat(response.order.CRequest.driver_current_latitude)+(parseFloat(0.09)*increment);
                    driver_longitude = parseFloat(response.order.CRequest.driver_current_longitude)+(parseFloat(0.09)*increment);
                    driver_rotation = parseFloat(response.order.CRequest.rotation)+parseFloat(10*increment);
                    increment++;
                    add_driver_marker()
                    draw_polyLine(response.order.CRequest.full_track);

                    console.log('zzzzzzzzzzzzzzzzzzzzzzzzzzzzzz', driver_latitude, driver_longitude, driver_rotation, increment);
                                
                }

        });

        }

    function add_driver_marker(request)
        {

            if (driverMarker && driverMarker.setMap) {
                driverMarker.setMap(null);
            }

            driverMarker = new google.maps.Marker({
                position: new google.maps.LatLng(driver_latitude, driver_longitude),  //myMap.getCenter(),
                map: myMap,
                title: "{{$order['driver_user']->name}}",
                icon: {
                    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,   //url: driver_icon,
                    scale: 4,
                    strokeColor: '#00F',
                    rotation: driver_rotation,
                }
            });

            // var icon = driverMarker.getIcon();
            // icon.rotation = driver_rotation;
            // driverMarker.setIcon(icon);

            myMap.panTo(driverMarker.position);
            bounds.extend(driverMarker.position);             

    //         myMap.panTo(event.latLng);

    //         addInfoWindow(driverMarker, dropoff_address);////    Add Info Window

        }

function transition(result){
    i = 0;
    deltaLat = (result[0] - position[0])/numDeltas;
    deltaLng = (result[1] - position[1])/numDeltas;
    moveMarker();
}

function moveMarker(){
    position[0] += deltaLat;
    position[1] += deltaLng;
    var latlng = new google.maps.LatLng(position[0], position[1]);
    marker.setTitle("Latitude:"+position[0]+" | Longitude:"+position[1]);
    marker.setPosition(latlng);
    // if(i!=numDeltas){
    //     i++;
    //     setTimeout(moveMarker, delay);
    // }
}


    function draw_polyLine(full_track)
        {
//console.log(full_track);

            var ridePlanCoordinates = [];
            var full_tracks = JSON.parse(full_track);

            for(i=0; i< full_tracks.length; i++ )
                {
                    ridePlanCoordinates.push( new google.maps.LatLng(full_tracks[i].latitude, full_tracks[i].longitude ) );
                    //var point = new google,maps.LatLng()
                }

            //////console.log(full_tracks[i-1].latitude, full_tracks[i-1].longitude);
            //////console.log('Pollyline', ridePlanCoordinates);

            if(ridePlanCoordinates.length != 0)
                {

                ridepath = new google.maps.Polyline({
                    path: ridePlanCoordinates,
                    strokeColor: '#FF0000',
                    strokeOpacity: 1.5,
                    strokeWeight: 1.5
                });

                ridepath.setMap(myMap);


                }

        }

    // $(document).ready(function() 
    //     {
    //         ////console.log('awddddddddddddddddddddddddddddddddddddddddddddddd')
    //     });

    // $(document).ready(function() 
    //         {
    //             load_map();
    //         });

    load_map();

</script>

@stop