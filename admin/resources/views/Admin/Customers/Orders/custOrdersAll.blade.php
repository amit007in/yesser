@extends('Admin.layout')

@section('title')
    Customer Orders
@stop

@section('content')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Customer Orders
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('org_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_custs')}}"> Customers</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_cust_orders',['user_detail_id' => $cust->user_detail_id]) }}">
                                <b>
                                    Customer Orders
                                </b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-md-6 col-lg-offset-3">
            <div class="ibox-content text-center">
                <h2>{{ $cust['user']->name }}</h2>
                    <div class="m-b-sm">
							@if($cust->profile_pic == "")
						<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $cust['user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
						</a>
					 @else
						<a href="{{ env('SOCKET_URL').'/Uploads/'.$cust->profile_pic }}" title="{{ $cust['user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ env('SOCKET_URL').'/Uploads/'.$cust->profile_pic }}"  class="img-circle">
						</a>
					 @endif		
                            
                    </div>
            </div>
        </div>
    </div>
<br>

    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="form-group">
                <label class="pull-left">Status</label>
                    <select class="form-control" id="filter1">
                        <option value="">All</option>
                        <option value="Ongoing">Ongoing</option>
                        <option value="Customer Cancelled">Customer Cancelled</option>
                        <option value="Completed">Completed</option>
                        <option value="Scheduled">Scheduled</option>
                        <option value="Confirmation Pending">Confirmation Pending</option>
                        <option value="Driver Cancelled">Yesser Man Cancelled</option>
                        <option value="Timeout">Timeout</option>
                        
                    </select>                 
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6">
            <label>
                Filter Updated At
            </label>

            <center>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
            </center>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="form-group">
                <label class="pull-left">Type</label>
                    <select class="form-control" id="filter2">
                        <option value="">All</option>
                        <option value="Service">Service</option>
                        <!--<option value="Support">Support</option>-->
                    </select>                 
            </div>
        </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Type</th>
                                    <th>Timing</th>
                                    <th>Pickup Location</th>
                                    <th>Product</th>
                                    <th>Yesser Man</th>
                                    <th>Yesser Man Pic</th>
                                    <th>Payment</th>
                                    <th>Actions</th>
                                    <th>Booked At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($orders as $order)
                <tr class="gradeA footable-odd" style="display: table-row;">

                    <td>{{ $order->order_id }}</td>

                    <td>
                        @include('Common.Partials.orderStatus')
                    </td>

                    <td>
                        @if($order->order_type == 'Service')
                            <span class="label label-info">
                                <i class="fa fa-certificate"> Service</i>
                            </span>
                        @else
                            <span class="label label-primary">
                                <i class="fa fa-support"> Support</i>
                            </span>
                        @endif
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order->order_timingsz }} </td>

                    <td>
                        {{ $order->pickup_address }}
                    </td>

                    <td>
                        <span class="label label-info">
                            {{ $order->payment['product_quantity'] }} * {{ $order->category_brand_product['name'] }}
                        </span>
                    </td>

                    <td>
                        @if($order['driver_user_detail'])
                            {{ $order['driver_user']->name }}
                        @endif
                    </td>

                    <td>
                        @if($order['driver_user_detail'])
    <!--<a href="{{ $order['driver_user_detail']->profile_pic_url }}" title="{{ $order['driver_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
        <img src="{{ $order['driver_user_detail']->profile_pic_url }}/150/150"  class="img-circle">
    </a>-->
	

						@if($order['driver_user_detail']->profile_pic == "")
							<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $order['driver_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
								<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
							</a>
						 @else
							<a href="{{ env('SOCKET_URL').'/Uploads/'.$order['driver_user_detail']->profile_pic }}" title="{{ $order['driver_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
								<img src="{{ env('SOCKET_URL').'/Uploads/'.$order['driver_user_detail']->profile_pic }}"  class="img-circle">
							</a>
						 @endif	
                     @endif
                    </td>

                    <td>
                       <b>OMR</b> {{ $order->payment['final_charge'] }}
                    </td>

                    <td>
            <a href="{{ route('admin_order_details', ['order_id' => $order->order_id] ) }}" class="btn btn-primary pull-right" id="Details" title="Details">
                <i class="fa fa-ioxhost"> Order Details</i>
            </a>
<!--                         <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                                <li role="presentation">
    <a role="menuitem" href="{{ route('admin_cust_order_details', ['order_id' => $order->order_id]) }}">
        <i class="fa fa-ioxhost"> Details</i>
    </a>
                                </li>


                            </ul>
                        </div> -->
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

        $("#customers").addClass("active");

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

        $('#daterange').on('apply.daterangepicker', function(ev, picker)
            {
                dt = document.getElementById('daterange').value;

                window.location.href = "{{route('admin_cust_orders')}}?user_detail_id={{$cust->user_detail_id}}&daterange="+dt;
            });

        });

            var table = $('#example').dataTable( 
                {
                   "order": [[ 0, "desc" ]],
                   //"scrollY": 500,
                   "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 7, 9 ] },
                        { "bSearchable": true, "aTargets": [ 4, 6 ] }
                    ],
                    "dom": 'Blfrtip',
                    "buttons": [
                       {
                            extend: 'pdf',
                            footer: true,
                            exportOptions: {
                                columns: [0,1,2,3,4,5,6,8,10]
                            }
                       },
                       {
                           extend: 'csv',
                           footer: false,
                            exportOptions: {
                                columns: [0,1,2,3,4,5,6,8,10]
                            }
                          
                       },
                       {
                           extend: 'excel',
                           footer: false,
                            exportOptions: {
                                columns: [0,1,2,3,4,5,6,8,10]
                            }
                       }         
                    ]
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });

            $("#filter2").on('change', function()
                {
                    table.fnFilter($(this).val(), 2);
                });

    </script>


@stop
