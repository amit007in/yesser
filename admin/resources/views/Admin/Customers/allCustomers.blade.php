@extends('Admin.layout')

@section('title')
    All Customers
@stop

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All Customers
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_custs')}}"><b>All Customers</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

<div class="row">

                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="form-group">
                            <label class="pull-left">Status</label>
                                <select class="form-control" id="filter1">
                                    <option value="e">All</option>
                                    <option value="Blocked">Blocked</option>
                                    <option value="Active">Active</option>
                                </select>                 
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4">
                                    <label>
                                        Filter Registered At
                                    </label>

                                    <center>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
                                    </center>                        
                    </div>

</div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Name</th>
                                    <th>Pic</th>
                                    <th>Actions</th>
                                    <th>Orders</th>
                                    <th>Reviews</th>
                                    <th>Registered At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($custs as $cust)
                <tr class="gradeA footable-odd">

                    <td>{{ $cust->user_detail_id }}</td>

                    <td>
                        <center>
                            @if($cust->blocked == '0')
                                <span class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i> Active</span>
                            @else
                                <span class="label label-danger"><i class="fa fa-eye-slash" aria-hidden="true"></i> Blocked</span>
                            @endif
                        </center>
                    </td>

                    <td>
                        {{ $cust->user['name'] }} ({{ $cust->user['phone_number'] }})
                        <hr>
                        <b>OTP -</b> {{ $cust->otp }}
                    </td>

                    <td>
    <a href="{{ $cust->profile_pic_url }}" title="{{ $cust->user['name'] }}" class="lightBoxGallery" data-gallery="">
        <img src="{{ $cust->profile_pic_url }}/120/120"  class="img-circle">
    </a>                        
                    </td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

        <li role="presentation">
            <a role="menuitem" tabindex="-1" id="admin_cust_orders" href="{{route('admin_cust_orders',['user_detail_id' => $cust->user_detail_id])}}">
                <i class="fa fa-circle-thin" aria-hidden="true"> All Orders - {{ $cust->order_counts }}</i>
            </a>
        </li>

        <li role="presentation">
            <a role="menuitem" tabindex="-1" id="admin_customer_reviews" href="{{route('admin_customer_reviews',['user_detail_id' => $cust->user_detail_id])}}">
                <i class="fa fa-star" aria-hidden="true"> All Reviews - {{ $cust->review_counts }}</i>
            </a>
        </li>
        

        <li role="presentation">
            @if($cust->blocked == '0')
                <a role="menuitem" tabindex="-1" class="block_cust" id="{{$cust->user_detail_id}}">
                    <i class="fa fa-eye-slash"> Block</i>
                </a>
            @else
                <a role="menuitem" tabindex="-1" class="unblock_cust" id="{{$cust->user_detail_id}}">
                    <i class="fa fa-eye"> Unblock</i>
                </a>
            @endif
        </li>

<!--                                 <li role="presentation">
            <a role='menuitem' tabindex="-1" onclick="delete_popup('{{$cust->user_detail_id}}', '{{$cust->user_id}}')">
                <i class="fa fa-bitbucket" aria-hidden="true"> Delete </i>
            </a>
                                </li> -->

                            </ul>
                        </div>
                    </td>

                    <td>
                        <a href="{{ route('admin_cust_orders',['user_detail_id' => $cust->user_detail_id]) }}" class="btn btn-default">
                            {{ $cust->order_counts }}
                        </a>
                    </td>

                    <td>
                        <a href="{{ route('admin_customer_reviews',['user_detail_id' => $cust->user_detail_id]) }}" class="btn btn-default">
                            {{ $cust->review_counts }}
                        </a>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $cust->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>

        </div>
    </div>
</div>

    <script>

    $("#customers").addClass("active");

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

        $('#daterange').on('apply.daterangepicker', function(ev, picker)
            {

                dt = document.getElementById('daterange').value;

                window.location.href = '{{route("admin_custs")}}?daterange='+dt;

            });

        });

        // function delete_popup(user_detail_id, user_id)
        //     {

        //         swal({
        //             title: "Are you sure you want to delete this customer?",
        //             text: "All data will be completely deleted.",
        //             type: "warning",
        //             showCancelButton: true,
        //             confirmButtonColor: "#DD6B55",
        //             confirmButtonText: "Yes, Delete it!",
        //             closeOnConfirm: false
        //             }, function () {
        //                 window.location.href = '{{route("admin_delete")}}?customer_user_detail_id='+user_detail_id+'&customer_user_id='+user_id;
        //             });

        //     }

            var table = $('#example').dataTable( 
                {
                   "order": [[ 0, "desc" ]],
                   "scrollX": true,
//                   "scrollY": 500,
                   "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [ 3, 4 ] },
                                { "bSearchable": true, "aTargets": [ 2 ] }
                    ],
                   "fnDrawCallback": function( oSettings ) 
                        {

                            $('.block_cust').click(function (e) {

                                  var id = $(e.currentTarget).attr("id");

                                    swal({
                                        title: "Are you sure you want to block this customer?",
                                        text: "Customer will not be able to use the app.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Block it!",
                                        closeOnConfirm: false
                                    }, function () {
                                        window.location.href = '{{route("admin_cust_block")}}?block=1&user_detail_id='+id;
                                    });
                                });

                            $('.unblock_cust').click(function (e) {

                                     var id = $(e.currentTarget).attr("id");

                                    swal({
                                        title: "Are you sure you want to unblock this customer?",
                                        text: "Customer will be able to access the app.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Unblock It!",
                                        closeOnConfirm: false
                                    }, function () {
                                window.location.href = '{{route("admin_cust_block")}}?block=0&user_detail_id='+id;
                                    });
                                });

                        }
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });


    </script>


@stop
