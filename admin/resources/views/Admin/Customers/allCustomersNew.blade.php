@extends('Admin.layout')

@section('title')
    All Customers
@stop

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All Customers
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_custs')}}"><b>All Customers</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                <div class="row">

<?php

    $blocked_check = isset($data['blocked_check']) ? $data['blocked_check'] : 'All';
    $search = isset($data['search']) ? $data['search'] : '';

    $daterange =  $fstarting_dt.' - '.$fending_dt;

?>

                <form method="post" action="{{route('admin_custs')}}">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <meta name="csrf-token" content="{{ csrf_token() }}">

                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <div class="form-group">
                            <label class="pull-left">Status</label>
                                <select class="form-control" id="blocked_check" name="blocked_check">
                                    <option value="All">All</option>
                                    <option value="Active">Active</option>
                                    <option value="Blocked">Blocked</option>
                                </select>                 
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <label>Filter Registered At</label>
                        <center>
                            <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$daterange}}"/>
                        </center>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <label>Search</label>
                        <input type="text" name="search" class="form-control" placeholder="Search" value="{{$search}}">
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <label>Filter</label><br>
                        <button type="submit" class="btn btn-primary btn-big">Filter Results</button>
                        <a class="btn btn-success btn-big" onclick="export_excell()">Export Excel</a>
                    </div>

                </form>

                </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Name</th>
                                    <th>Pic</th>
                                    <th>Actions</th>
                                    <th>Orders</th>
                                    <th>Reviews</th>
                                    <th>Registered At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($custs as $cust)
                <tr class="gradeA footable-odd">

                    <td>{{ $cust->user_detail_id }}</td>

                    <td>
                        <center>
                            @if($cust->blocked == '0')
                                <span class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i> Active</span>
                            @else
                                <span class="label label-danger"><i class="fa fa-eye-slash" aria-hidden="true"></i> Blocked</span>
                            @endif
                        </center>
                    </td>

                    <td>
                        {{ $cust->name }} ({{ $cust->phone_number }})
                        <hr>
                        <b>OTP -</b> {{ $cust->otp }}
                    </td>

                    <td>
    <!--<a href="{{ $cust->profile_pic_url }}" title="{{ $cust->name }}" class="lightBoxGallery" data-gallery="">
        <img src="{{ $cust->profile_pic_url }}"  class="img-circle">
    </a> -->                
					@if($cust->profile_pic == "")
						<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $cust->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
						</a>
					 @else
						<a href="{{ env('SOCKET_URL').'/Uploads/'.$cust->profile_pic }}" title="{{ $cust->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ env('SOCKET_URL').'/Uploads/'.$cust->profile_pic }}"  class="img-circle">
						</a>
					 @endif		
                    </td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

        <li role="presentation">
            <a role="menuitem" tabindex="-1" id="admin_cust_orders" href="{{route('admin_cust_orders',['user_detail_id' => $cust->user_detail_id])}}">
                <i class="fa fa-circle-thin" aria-hidden="true"> All Orders - {{ $cust->order_counts }}</i>
            </a>
        </li>

        <li role="presentation">
            <a role="menuitem" tabindex="-1" id="admin_customer_reviews" href="{{route('admin_customer_reviews',['user_detail_id' => $cust->user_detail_id])}}">
                <i class="fa fa-star" aria-hidden="true"> All Reviews - {{ $cust->review_counts }}</i>
            </a>
        </li>
        
        @if($cust->phone_number != '11111111')

            <li role="presentation">
                @if($cust->blocked == '0')
                    <a role="menuitem" tabindex="-1" class="block_driver" id="{{$cust->user_detail_id}}" onclick="show_popup('Block','{{$cust->user_detail_id}}')">
                        <i class="fa fa-eye-slash"></i> Block
                    </a>
                @else
                    <a role="menuitem" tabindex="-1" class="unblock_driver" id="{{$cust->user_detail_id}}" onclick="show_popup('Unblock','{{$cust->user_detail_id}}')">
                        <i class="fa fa-eye"></i> Unblock
                    </a>
                @endif
            </li>

            <li role="presentation">
                <a role='menuitem' tabindex="-1" onclick="delete_popup('{{$cust->user_detail_id}}', '{{$cust->user_id}}')">
                    <i class="fa fa-bitbucket" aria-hidden="true"> Delete </i>
                </a>
            </li>

        @endif

                            </ul>
                        </div>
                    </td>

                    <td>
                        <a href="{{ route('admin_cust_orders',['user_detail_id' => $cust->user_detail_id]) }}" class="btn btn-default">
                            {{ $cust->order_counts }}
                        </a>
                    </td>

                    <td>
                        <a href="{{ route('admin_customer_reviews',['user_detail_id' => $cust->user_detail_id]) }}" class="btn btn-default">
                            {{ $cust->review_counts }}
                        </a>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $cust->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>

                                <tfoot>
                <center>
                    <h2>Total - {{ $custs->total() }}</h2>
                </center>
                <center>
        {{ $custs->appends(['blocked_check' => $blocked_check, 'search'=>$search, 'daterange' => $daterange])->links() }}
                </center>
                                </tfoot>

                            </table>

                            </div>
                        </div>
                    </div>

</div>

        </div>
    </div>
</div>

    <script>

        $("#customers").addClass("active");

/////////////       Filters     ///////////////////////
        $('#blocked_check').val('{{$blocked_check}}');
/////////////       Filters     ///////////////////////


        function export_excell()
            {
                var blocked_check = '{{$blocked_check}}';
                var search = '{{$search}}';
                var daterange = '{{$daterange}}';

                $.ajax({
                    dataType: "json",
                    url: '{{route("admin_custs_preport")}}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    data: {
                        blocked_check,
                        search,
                        daterange
                    },
                    success:  function(result){

                        //$("#div1").html(result);
                        window.location.href = result.link;
                        console.log(result);
                
                    }
                });

            }

        function delete_popup(user_detail_id, user_id)
            {

                swal({
                    title: "Are you sure you want to delete this customer?",
                    text: "All data will be completely deleted.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Delete it!",
                    closeOnConfirm: false
                    }, function () {
                        window.location.href = '{{route("admin_delete")}}?customer_user_detail_id='+user_detail_id+'&customer_user_id='+user_id;
                    });

            }

        function show_popup(type, id)
            {

                if(type == 'Block')
                    {
                        var title = "Are you sure you want to block this customer?";
                        var sub_title = "Customer will not be able to use the app.";
                        var route = '{{route("admin_cust_block")}}?block=1&user_detail_id='+id;
                    }
                else if(type == 'Unblock')
                    {
                        var title = "Are you sure you want to unblock this driver?";
                        var sub_title = "User will be able to view this driver.";
                        var route = '{{route("admin_cust_block")}}?block=0&user_detail_id='+id;
                    }
                
                swal({
                        title: title,
                        text: sub_title,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Do it!",
                        closeOnConfirm: false
                    }, function () {
                        window.location.href = route;
                    });
            }

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                opens: 'right',
                autoApply: true,
                format: 'DD/MM/YYYY',
                minDate: '1/2/2018',
                maxDate: moment().format('DD/MM/YYYY'),
            }, function(start, end, label) {
                //console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });

        });

    </script>


@stop
