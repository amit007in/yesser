    @extends('Admin.layout')

    @section('title')
        All EContacts
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All EContacts
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_econtacts_all')}}"><b>All EContacts</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="form-group">
                            <label class="pull-left">Status</label>
                                <select class="form-control" id="filter1">
                                    <option value="e">All</option>
                                    <option value="Blocked">Blocked</option>
                                    <option value="Active">Active</option>
                                </select>                 
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4"></div>

                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <br>
        <a href="{{ route('admin_econtact_aget') }}" class="btn btn-primary pull-right" id="Add EContact" title="Add EContact">
            <i class="fa fa-plus"></i> Add EContact
        </a>
                    </div>


                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Ranking</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                    <th>Updated At</th>
                                </tr>

                                </thead>
                                <tbody>

            @foreach($econtacts as $econtact)
                <tr class="gradeA footable-odd">

                    <td>{{ $econtact->emergency_contact_id }}</td>

                    <td>
                        <center>
                            @if($econtact->blocked == '0')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye-slash" aria-hidden="true"> Blocked</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            <span class="label label-success"> {{$econtact->sort_order}} </span>    
                        </center>
                    </td>

                    <td>
                        <b>
                            {{ $econtact->emergency_contact_details[0]->name }}
                        </b>(English)<hr>

                       <!-- <b>
                            {{ $econtact->emergency_contact_details[1]->name }}
                        </b>(Hindi)<hr>

                        <b>
                            {{ $econtact->emergency_contact_details[2]->name }}
                        </b>(Urdu)<hr>

                        <b>
                            {{ $econtact->emergency_contact_details[3]->name }}
                        </b>(Chinese)<hr>-->

                        <b>
                            {{ $econtact->emergency_contact_details[4]->name }}
                        </b>(Arabic)<hr>
                    </td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{route('admin_econtact_uget', ['emergency_contact_id'=>$econtact->emergency_contact_id])}}">
        <i class="fa fa-edit"> Update</i>
    </a>
                                </li>

                                <li role="presentation">
                                    @if($econtact->blocked == '0')
    <a role="menuitem" tabindex="-1" class="block_e" id="{{$econtact->emergency_contact_id}}">
        <i class="fa fa-eye-slash"> Block</i>
    </a>
                                    @else
    <a role="menuitem" tabindex="-1" class="unblock" id="{{$econtact->emergency_contact_id}}">
        <i class="fa fa-eye"> Unblock</i>
    </a>
                                    @endif
                                </li>

                            </ul>
                        </div>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $econtact->updated_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

        $("#settings_all").addClass("active");
        $("#emergency_all").addClass("active");

            var table = $('#example').dataTable( 
                {
                   "order": [[ 2, "asc" ]],
                   "scrollX": true,
                   "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [ 4 ] },
                                { "bSearchable": true, "aTargets": [ 3 ] }
                    ],
                   "fnDrawCallback": function( oSettings ) 
                        {

                            $('.block_e').click(function (e) {

                                  var id = $(e.currentTarget).attr("id");

                                    swal({
                                        title: "Are you sure you want to block this emergeny contact?",
                                        text: "No one will not be able to see this emergeny contact.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Block it!",
                                        closeOnConfirm: false
                                    }, function () {
                                        window.location.href = '{{route("admin_econtact_block")}}?block=1&emergency_contact_id='+id;
                                    });
                                });

                            $('.unblock').click(function (e) {

                                     var id = $(e.currentTarget).attr("id");

                                    swal({
                                        title: "Are you sure you want to unblock this emergeny contact?",
                                        text: "Every one will not be able to see this emergeny contact.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Unblock It!",
                                        closeOnConfirm: false
                                    }, function () {
                                window.location.href = '{{route("admin_econtact_block")}}?block=0&emergency_contact_id='+id;
                                    });
                                });

                        }
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });

    </script>


@stop
