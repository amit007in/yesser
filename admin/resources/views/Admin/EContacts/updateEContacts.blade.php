@extends('Admin.layout')

@section('title') 
    Update Emergency Contact
@stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Update Emergency Contact</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_econtacts_all')}}">All Emergency Contacts</a>
                        </li>
                        <li>
                            <a href="{{route('admin_econtact_uget', ['emergency_contact_id' => $econtact->emergency_contact_id] ) }}">
                                <b>Update Emergency Contact</b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

        <br>
            <form method="post" action="{{route('admin_econtact_upost', ['emergency_contact_id' => $econtact->emergency_contact_id]) }}" enctype="multipart/form-data" id="addForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Phone number</label>
                <input type="number" placeholder="Phone number" autocomplete="off" class="form-control" required name="phone_number" value="{!! $econtact->phone_number !!}" autofocus="on" id="phone_number" minlength="3" maxlength="10">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>English Name</label>
                <input type="text" placeholder="English Name" autocomplete="off" class="form-control" required name="english_name" value="{!! $econtact->emergency_contact_details[0]->name !!}" id="english_name">
            </div> 

        </div>
    </div>
<br>
   <!-- <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Hindi Name</label>
                <input type="text" placeholder="Hindi Name" autocomplete="off" class="form-control" required name="hindi_name" value="{!! $econtact->emergency_contact_details[1]->name !!}"id="hindi_name">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Urdu Name</label>
                <input type="text" placeholder="Urdu Name" autocomplete="off" class="form-control" required name="urdu_name" value="{!! $econtact->emergency_contact_details[2]->name !!}" id="urdu_name">
            </div> 

        </div>
    </div>
<br>-->
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Arabic Name</label>
                <input type="text" placeholder="Arabic Name" autocomplete="off" class="form-control" required name="arabic_name" value="{!! $econtact->emergency_contact_details[4]->name !!}" id="arabic_name">
            </div> 

            <!--<div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Arabic Name</label>
                <input type="text" placeholder="Arabic Name" autocomplete="off" class="form-control" required name="arabic_name" value="{!! $econtact->emergency_contact_details[4]->name !!}" id="arabic_name">
            </div> -->

        </div>
    </div>
<br>

    <div class="row">
        <div class="form-group">

            <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1">
                <label for="ranking">Ranking</label>
                <select class="form-control border-info" required name="ranking">
                    @for($i=1;$i<=$ranking;$i++)
                        @if($econtact->sort_order == $i)
                            <option value="{{$i}}" selected="true">{{$i}}</option>
                        @else
                            <option value="{{$i}}">{{$i}}</option>
                        @endif
                    @endfor
                </select>
            </div>

        </div>
    </div>

            <br><br><br>

                <div class="row">
                    <div class="form-group">
                        <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            {!! Form::submit('Update Emergency Contact', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>

<script type="text/javascript">

        $("#settings_all").addClass("active");
        $("#emergency_all").addClass("active");

        $("#addForm").validate({
            rules: {
                english_name: {
                    required: true,
                    maxlength: 255,
                },
                hindi_name: {
                    required: true,
                    maxlength: 255,
                },
                urdu_name: {
                    required: true,
                    maxlength: 255,
                },
                chinese_name: {
                    required: true,
                    maxlength: 255,
                },
                arabic_name: {
                    required: true,
                    maxlength: 255,
                }
 
            },
            messages: {
                phone_number: {
                    required: "Phone number is required"
                },
                english_name: {
                    required: "Please provide the english name"
                },
                hindi_name: {
                    required: "Please provide the hindi name"
                },
                urdu_name: {
                    required: "Please provide the urdu name"
                },
                chinese_name: {
                    required: "Please provide the chinese name"
                },
                arabic_name: {
                    required: "Please provide the arabic name"
                }

            }
        });

</script>


@stop



