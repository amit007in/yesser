
    @extends('Admin.layout')

@section('title')
    Floor Margin
@stop

@section('content')
			
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All Floor Margin
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_floormargin_all')}}"><b>All Floor Margin</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="row">
                <div class="ibox float-e-margins">
				@if(count($FloorMargin) < 1)
                    <div class="col-lg-2 col-md-3 col-sm-3" style="float: right;margin-bottom: 10px;">
                        <br>
                        <a href="{{route('admin_floor_aget')}}" class="btn btn-primary pull-right" id="Add Floor Margin" title="Add Floor Margin">
                            <i class="fa fa-plus"></i> Add Floor Margin
                        </a>
                    </div>
					@endif
                    <div class="ibox-content table-responsive" style="height: 182px !important;">
                        <div class="">
                            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Floor</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($FloorMargin as $FloorMargins)
                                    <tr class="gradeA footable-odd">
                                        <td>
                                            {{ $FloorMargins->floormargin_id }}
                                        </td>

                                        <td>
                                            {{ $FloorMargins->floor_name }}
                                        </td>

                                        <td>
                                            {{ $FloorMargins->price }}
                                        </td>
                                        <td>
                                            <center>
                                                @if($FloorMargins->status == '1')
                                                    <span class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i> Active</span>
                                                @else
                                                    <span class="label label-danger"><i class="fa fa-eye-slash" aria-hidden="true"></i> Blocked</span>
                                                @endif
                                            </center>
                                        </td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                                    Actions
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                    <li role="presentation">
                                                        <a role="menuitem" tabindex="-1" href="{{route('admin_floor_uget', ['floormargin_id'=>$FloorMargins->floormargin_id])}}">
                                                            <i class="fa fa-edit"> Update</i>
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        @if($FloorMargins->status == 'Active')
                                                            <a role="menuitem" tabindex="-1" class="block_cust" id="{{ $FloorMargins->floormargin_id }}">
                                                                <i class="fa fa-eye-slash"> Active</i>
                                                            </a>
                                                        @else
                                                            <a role="menuitem" tabindex="-1" class="unblock_cust" id="{{ $FloorMargins->floormargin_id }}">
                                                                <i class="fa fa-eye"> In Active</i>
                                                            </a>
                                                        @endif
                                                    </li>
                                                    <!--<li role="presentation">
                                                        <a role='menuitem' tabindex="-1" onclick="delete_popup('{{ $FloorMargins->floormargin_id }}')">
                                                            <i class="fa fa-bitbucket" aria-hidden="true"> Delete </i>
                                                        </a>
                                                    </li>-->
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#settings_all").addClass("active");
    $("#floormargin_all").addClass("active");
	$('.block_cust').click(function (e) {
                var id     = $(e.currentTarget).attr("id");
                var status = $('#blocked_check').val();
                swal({
                    title: "Are you sure you want to active this floor margin?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Active It!",
                    closeOnConfirm: false
                }, function () {
                    window.location.href = '{{route("admin_floor_active")}}?block=1&floormargin_id='+id+'&status='+status;
                });
            });
            $('.unblock_cust').click(function (e) {
                var id = $(e.currentTarget).attr("id");
                var status = $('#blocked_check').val();
                swal({
                    title: "Are you sure you want to deactivate this floor margin?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Deactivate It!",
                    closeOnConfirm: false
                }, function () {
                    window.location.href = '{{route("admin_floor_active")}}?block=0&floormargin_id='+id+'&status='+status;
                });
            });

    function delete_popup(floormarginid)
    {

        swal({
            title: "Are you sure you want to delete this floor margin?",
            text: "All data will be completely deleted.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: false
            }, function () {
                window.location.href = '{{route("admin_floor_delete")}}?floormargin_id='+floormarginid;
            });

    }
</script>


@stop
