@extends('Admin.layout')

@section('title') 
    Edit Floor Margin
@stop

@section('content')

    <link href="{{ URL::asset('AdminAssets/css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-xs-12">
                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>Update Floor Margin</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{route('admin_dashboard')}}">Dashboard</a>
                            </li>
                            <li>
                                <a href="{{route('admin_floormargin_all')}}">All Floor Margin</a>
                            </li>
                            <li><b>Update Floor Margin</b></li>
                        </ol>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <br>
                    <form method="post" action="{{route('admin_floor_upost') }}" enctype="multipart/form-data" id="addForm">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="floor_id" name="floor_id" value="{{ $floordetails->floormargin_id }}">
                        <div class="row">
                            <div class="form-group">
                                <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                    <label>Floor</label>
                                    <input type="number" placeholder="Floor Name" autocomplete="off" class="form-control" required name="floor_name" value="1" autofocus="on" id="floor_name" readonly>
                                </div> 
                            </div>
                        </div>
						<div class="row">
                            <div class="form-group">
                                <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                    <label>Price</label>
                                    <input type="number" placeholder="Price" autocomplete="off" class="form-control" required name="price" value="{!! $floordetails->price !!}" id="price">
                                </div> 
                            </div>
                        </div>
						<br>
						<div class="row">
                            <div class="form-group">
                                <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                    <input type="checkbox" name="floor_status" id="floor_status" value="1" <?php if($floordetails->all_floor_price_status == '1'){ echo "checked";} ?>> <label>Should be use for all floors</label>
                                    
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="form-group">
                                <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                    {!! Form::submit('Update Floor Margin', ['class' => 'btn btn-success']) !!}
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript">
        $("#settings_all").addClass("active");
        $("#floormargin_all").addClass("active");
        $("#addForm").validate({
            rules: {
                floor_name: {
                    required: true,
                },
                price: {
                    required: true,
                }
            },
            messages: {
                floor_name: {
                    required: "Please provide the floor name"
                },
                price: {
                    required: "Please provide the price"
                },
            }
        });
    </script>

@stop



