@extends('Admin.layout')

@section('title')
    All Logins
@stop

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All Logins
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_login_history')}}"><b>All Logins</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="form-group">
                            <label class="pull-left">Status</label>
                                <select class="form-control" id="filter1">
                                    <option value="e">All</option>
                                    <option value="LoggedIn">LoggedIn</option>
                                    <option value="LoggedOut">LoggedOut</option>
                                </select>                 
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6">
                                    <label>
                                        Filter Created At
                                    </label>

                                    <center>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
                                    </center>                        
                    </div>

                </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>IP</th>
                                    <th>Location</th>
                                    <th>LoggedIn At</th>
                                    <th>LoggedOut At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($histories as $history)
                <tr class="gradeA footable-odd">

                    <td>{{ $history->admin_user_detail_login_id }}</td>

                    <td>
                        <center>
                            @if($history->access_token != '')
                                <span class="label label-primary"><i class="fa fa-sign-in" aria-hidden="true"></i> LoggedIn</span>
                            @else
                                <span class="label label-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> LoggedOut</span>
                            @endif
                        </center>
                    </td>

                    <td>
                        {{$history->ip_address}}
                    </td>

                    <td>
                        <b>Latitude -</b> @if($history->latitude != 0.0) {{$history->latitude}} @endif
                        <br>
                        <b>Longitude -</b> @if($history->longitude != 0.0) {{$history->longitude}} @endif
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $history->created_atz }} </td>

                    <td>
                        @if($history->access_token == '') <i class="fa fa-clock-o"></i> {{ $history->updated_atz }} @endif
                    </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>
</div>

        </div>
    </div>
</div>

    <script>

    $("#settings_all").addClass("active");
    $("#admin_login_history").addClass("active");

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

        $('#daterange').on('apply.daterangepicker', function(ev, picker)
            {

                dt = document.getElementById('daterange').value;

                window.location.href = '{{route("admin_login_history")}}?daterange='+dt;

            });

        });

            var table = $('#example').dataTable( 
                {
                   "order": [[ 0, "desc" ]],
                   "scrollX": true,
//                   "scrollY": 500,
                   "aoColumnDefs": [
                        //{ "bSortable": false, "aTargets": [ 6 ] },
                        //{ "bSearchable": true, "aTargets": [ 2, 4 ] }
                    ],
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });

    </script>


@stop
