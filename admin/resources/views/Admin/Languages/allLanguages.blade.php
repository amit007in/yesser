    @extends('Admin.layout')

    @section('title')
        All Languages
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All Languages
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_language_all')}}"><b>All Languages</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="form-group">
                            <label class="pull-left">Status</label>
                                <select class="form-control" id="filter2">
                                    <option value="e">All</option>
                                    <option value="Default">Default</option>
                                    <option value="Other">Other</option>
                                </select>                 
                        </div>
                    </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-stripped tablet breakpoint footable-loaded" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
    <th class="footable-visible footable-first-column footable-sortable"><center>Language Id</center></th>
    <th class="footable-visible footable-first-column footable-sortable"><center>Name</center></th>
    <th class="footable-visible footable-first-column footable-sortable"><center>Default</center></th>
    <th class="footable-visible footable-first-column footable-sortable"><center>Action</center></th>
    <th class="footable-visible footable-first-column footable-sortable"><center>Updated At</center></th>
                                </tr>

                                </thead>
                                <tbody>

            @foreach($languages as $language)
                <tr class="gradeA footable-odd">

                    <td>{{ $language->language_id }}</td>

                    <td>{{ $language->language_name }}</td>

                    <td>
                        @if($language->is_default == '1')
                            <span class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i> Default</span>
                        @else
                            <span class="label label-warning"><i class="fa fa-eye-slash" aria-hidden="true"></i> Other</span>
                        @endif
                    </td>

                    <td>
<!--                                     @if($language->is_default == 0)
                                        <a role="menuitem" class="btn btn-primary block_cat" id="{{$language->id}}">
                                            <i class="fa fa-eye"> Make Default</i>
                                        </a>
                                    @endif -->
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                                <li role="presentation">
                                    @if($language->is_default == "0")
                                        <a role="menuitem" tabindex="-1" class="defalt_language" id="{{$language->language_id}}">
                                            <i class="fa fa-eye"> Make Default</i>
                                        </a>
                                    @endif
                                </li>

                            </ul>
                        </div>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $language->updated_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

        $("#settings_all").addClass("active");
        $("#language_all").addClass("active");

            var table = $('#example').dataTable( 
                {
                   //"order": [[ 0, "desc" ]],
                   //"scrollY": 500,
                   "scrollX": true,
                   "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [ 3 ] },
                                { "bSearchable": true, "aTargets": [ 1 ] }
                            ],
                   "fnDrawCallback": function( oSettings ) 
                        {

                            $('.defalt_language').click(function (e) {

                                swal({
                                    title: "Are you sure you want to make this default language?",
                                    text: "This will be set as default language for new Users, Providers, Support.",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Yes, Update it!",
                                    closeOnConfirm: false
                                }, function () {
                                    window.location.href = '{{route("default_lang_change")}}?language_id='+$(e.currentTarget).attr("id");
                                });

                            });

                        }
                });

            $("#filter2").on('change', function()
                {
                    table.fnFilter($(this).val(), 2);
                });

    </script>


@stop
