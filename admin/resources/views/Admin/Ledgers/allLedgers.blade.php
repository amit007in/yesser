    @extends('Admin.layout')

    @section('title')
        All Ledgers
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All Ledgers
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_ledger_all')}}"><b>All Ledgers</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="form-group">
                            <label class="pull-left">Ledger Type</label>
                                <select class="form-control" id="filter1">
                                    <option value="">All</option>
                                    <option value="Credit">Credit</option>
                                    <option value="Payment">Payment</option>
                                </select>                 
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <label>
                            Filter Updated At
                        </label>

                        <center>
                <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
                        </center>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="form-group">
                            <label class="pull-left">User Type</label>
                                <select class="form-control" id="filter4">
                                    <option value="">All</option>
                                    <option value="Company">Company</option>
                                    <option value="Service">Service</option>
                                    <option value="Support">Support</option>
                                </select>                 
                        </div>
                    </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Ledger Type</th>
                                    <th>Amount</th>
                                    <th>Ledger Datetime</th>
                                    <th>Description</th>
                                    <th>User Type</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                    <th>Created At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($ledgers as $ledger)
                <tr class="gradeA footable-odd">

                    <td>{{ $ledger->payment_ledger_id }}</td>

                    <td>
                        @if($ledger->type == "Payment")
                            <span class="label label-info">
                                Payment
                            </span>
                        @else
                            <span class="label label-warning">
                                Credit
                            </span>
                        @endif
                    </td>

                    <td>OMR {{ $ledger->amount }}</td>

                    <td><i class="fa fa-clock-o"></i> {{ $ledger->ledger_dtz }} </td>

                    <td>
                        <div style="height:100px;overflow:auto;">
                            {{ $ledger->description }}
                        </div>
                    </td>

                    <td>
                        <center>
                            @if($ledger->user_type_id == '2')
                                <span class="label label-primary">
                                    <i class="fa fa-certificate" aria-hidden="true"> Service</i>
                                </span>
                            @elseif($ledger->user_type_id == '3')
                                <span class="label label-success">
                                    <i class="fa fa-support" aria-hidden="true"> Support</i>
                                </span>
                            @else
                                <span class="label label-info">
                                    <i class="fa fa-building-o" aria-hidden="true"></i> Company
                                </span>
                            @endif
                        </center>
                    </td>

                    @if($ledger->organisation_id != 0)

                        <td>
                            <center>
                                {{$ledger->organisation['name']}} - ({{$ledger->organisation['phone_number']}})
                            </center>
                        </td>

                        <td>
                                <a href="{{ $ledger->organisation['image_url'] }}" title="{{ $ledger->organisation['name'] }}" class="lightBoxGallery" data-gallery="">
                                    <img src="{{ $ledger->organisation['image_url'] }}/120/120"  class="img-circle">
                                </a>
                        </td>

                    @else

                        <td>
                            <center>
                                {{$ledger->user['name']}} - ({{$ledger->user['phone_number']}})
                            </center>
                        </td>

                        <td>
                            <a href="{{ $ledger->user_detail['profile_pic_url'] }}" title="{{ $ledger->user['name'] }}" class="lightBoxGallery" data-gallery="">
                                <img src="{{ $ledger->user_detail['profile_pic_url'] }}/120/120"  class="img-circle">
                            </a>
                        </td>

                    @endif

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                            </ul>
                        </div>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $ledger->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

        $("#ledgers").addClass("active");

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

        $('#daterange').on('apply.daterangepicker', function(ev, picker)
            {
                dt = document.getElementById('daterange').value;

                window.location.href = "{{route('admin_ledger_all')}}?daterange="+dt;
            });

        });

            var table = $('#example').dataTable( 
                {
                    "order": [[ 2, "asc" ]],
                    "scrollX": true,
                    "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [ 4, 5 ] },
                                { "bSearchable": true, "aTargets": [ 1 ] }
                    ],
                   "fnDrawCallback": function( oSettings ) 
                        {

                            $('.block_support').click(function (e) {

                                  var id = $(e.currentTarget).attr("id");

                                    swal({
                                        title: "Are you sure you want to block this support?",
                                        text: "Users, Drivers will not be able to access this support.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Block it!",
                                        closeOnConfirm: false
                                    }, function () {
                                        window.location.href = '{{route("admin_support_category_block")}}?block=1&category_id='+id;
                                    });
                                });

                            $('.unblock_support').click(function (e) {

                                     var id = $(e.currentTarget).attr("id");

                                    swal({
                                        title: "Are you sure you want to unblock this support?",
                                        text: "Users, Drivers will be able to access this support.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Unblock It!",
                                        closeOnConfirm: false
                                    }, function () {
                                window.location.href = '{{route("admin_support_category_block")}}?block=0&category_id='+id;
                                    });
                                });

                        }
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });
            $("#filter4").on('change', function()
                {
                    table.fnFilter($(this).val(), 4);
                });


    </script>


@stop
