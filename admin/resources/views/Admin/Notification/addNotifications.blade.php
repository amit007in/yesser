@extends('Admin.layout')

@section('title') 
Add Notification
@stop

@section('content')

<link href="{{ URL::asset('AdminAssets/css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add Notification</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_notification_all')}}">All Notification</a>
                        </li>
                        <li>
                            <a href="{{route('admin_notification_aget') }}">
                                <b>Add Notification</b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <br>
                <form method="post" action="{{route('admin_notification_apost') }}" enctype="multipart/form-data" id="addForm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="form-group">
                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Title</label>
                                <input type="text" placeholder="Notification Title" autocomplete="off" class="form-control" required name="title" value="{!! Form::old('title') !!}" autofocus="on" id="title">
                            </div> 

                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>Message</label>
                                <input type="text" placeholder="Message" autocomplete="off" class="form-control" required name="message" value="{!! Form::old('message') !!}" id="message">
                            </div> 

                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">

                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Type</label>
                                <select name="type" id="type"  class="form-control">
                                    <option value="Customer">Customer</option>
                                    <option value="Driver">Driver</option>
                                </select>
                            </div> 

                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>Broadcast Date</label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" placeholder="Broadcast Date" autocomplete="off" class="form-control" required name="date" value="{!! Form::old('date') !!}" id="date" readonly>
                                </div>

                            </div> 

                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">

                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Status</label>&nbsp;&nbsp;
                                <input type="radio" value="Active" required name="status" value="{!! Form::old('status') !!}" id="status"> &nbsp;Active &nbsp;&nbsp;
                                <input type="radio" value="In Active" required name="status" value="{!! Form::old('status') !!}" id="status"> &nbsp;In Active
                            </div> 

                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>Recurring</label>&nbsp;&nbsp;
                                <input type="radio" value="On" required name="recurring" value="{!! Form::old('recurring') !!}" id="recurring"> &nbsp;On &nbsp;&nbsp;
                                <input type="radio" value="Off" required name="recurring" value="{!! Form::old('recurring') !!}" id="recurring"> &nbsp;Off
                            </div> 

                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1">
                                <label for="ranking">Ranking</label>
                                <select class="form-control border-info" required name="ranking">
                                    @for($i=1;$i<=$ranking+1;$i++)
                                    <option value="{{$i}}" selected="true">{{$i}}</option>
                                    @endfor
                                </select>
                            </div>

                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>Image</label>&nbsp;&nbsp;
                                <input type="file" name="notificationImage"  id="notificationImage">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="total" value="{{$ranking}}">
                    <br><br><br>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1">
                            <label for="coupon_id">Add Promocode</label>
                                <select class="form-control border-info" name="coupon_id">
                                    <option value="" selected="true">Please select promocode</option>
                                    @foreach($promocode as $promo)
                                        <option value="{{$promo->coupon_id}}">{{$promo->code}}</option>
                                    @endforeach
                                </select>
                            </div>
                             <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label for="ranking">Service</label>
                               <select class="form-control border-info" name="service_id" id="service_id">
                                   <option value="">Select Service</option>
                                   @foreach($Services as $category)
                                       <option value="{{$category->category_id}}">{{$category->name}}</option>
                                   @endforeach
                               </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                                {!! Form::submit('Add Notification', ['class' => 'btn btn-success', 'id' => 'ordersubmitform']) !!}
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
<script src="{{ URL::asset('AdminAssets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">

$("#settings_all").addClass("active");
$("#notification_all").addClass("active");

$("#addForm").validate({
    rules: {
        title: {
            required: true,
            maxlength: 255
        },
        message: {
            required: true,
            maxlength: 255
        },
        date: {
            required: true
        },
        status: {
            required: true
        },
        recurring: {
            required: true
        }

    },
    messages: {
        title: {
            required: "Please provide the title"
        },
        message: {
            required: "Please provide the notification message"
        },
        date: {
            required: "Please provide the broadcast date"
        },
        status: {
            required: "Please select the status"
        },
        recurring: {
            required: "Please select the recurring"
        }

    }
});

$('#date').datetimepicker({
    startDate: new Date(),
    format: 'yyyy-mm-dd hh:ii:00'
});

$(document).ready(function () {
    $("#addForm").submit(function () {
        if($("#addForm").valid()){
            $("#ordersubmitform").prop('disabled',true);
        return true;
        }
        
    });
});

</script>
<style>
label[for=date]
{
   float: right;
   text-align: center;
   margin-top: -54px;
}
label[for=status]
{
   float: right;
   margin-top: 2px;
}
label[for=recurring]
{
   float: right;
   margin-top: 2px;
}
</style>

@stop



