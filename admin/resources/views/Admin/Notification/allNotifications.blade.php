
    @extends('Admin.layout')

@section('title')
    Notification
@stop

@section('content')

<div class="wrapper wrapper-content animated fadeInRight">

<div class="row">
    <div class="col-xs-12">

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>
                    All Notification
                </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{route('admin_dashboard')}}">Dashboard</a>
                    </li>
                    <li>
                        <a href="{{route('admin_notification_all')}}"><b>All Notification</b></a>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <?php
                    $daterange = "";
                    $blocked_check = isset($data['blocked_check']) ? $data['blocked_check'] : 'All';
                    $daterange =  $fstarting_dt.' - '.$fending_dt;
                    $type_check = isset($data['type_check']) ? $data['type_check'] : 'All';
                ?>
                <form method="post" action="{{route('admin_notification_all')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <meta name="csrf-token" content="{{ csrf_token() }}">
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <div class="form-group">
                            <label class="pull-left">Status</label>
                                <select class="form-control" id="blocked_check" name="blocked_check">
                                    <option value="All" {{ $blocked_check == 'All' ? 'selected="selected"' : '' }}>All</option>
                                    <option value="Active" {{ $blocked_check == 'Active' ? 'selected="selected"' : '' }}>Active</option>
                                    <option value="In Active"{{ $blocked_check == 'In Active' ? 'selected="selected"' : '' }}>In Active</option>
                                </select>                 
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <label>Filter Registered At</label>
                        <center>
                            <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$daterange}}"/>
                        </center>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <div class="form-group">
                            <label class="pull-left">Type</label>
                                <select class="form-control" id="type_check" name="type_check">
                                    <option value="All" {{ $type_check == 'All' ? 'selected="selected"' : '' }}>All</option>
                                    <option value="Customer" {{ $type_check == 'Customer' ? 'selected="selected"' : '' }}>Customer</option>
                                    <option value="Driver" {{ $type_check == 'Driver' ? 'selected="selected"' : '' }}>Driver</option>
                                </select>                 
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3">
                        <label>Filter</label><br>
                        <button type="submit" class="btn btn-primary btn-big">Filter Results</button>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3">
                        <br>
                        <a href="{{ route('admin_notification_aget') }}" class="btn btn-primary pull-right" id="Add Notification" title="Add Notification">
                            <i class="fa fa-plus"></i> Add Notification
                        </a>
                    </div>
                </form>
            </div>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <div class="">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Message</th>
                        <th>Type</th>
                        <th>Action</th>
                        <th>Broadcast Date</th>
                        <th>Status</th>
                        <th>Recurring</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($notifications as $notifications)
                    <tr class="gradeA footable-odd">
                        <td>
                            {{ $notifications->notification_id }}
                        </td>

                        <td>
                            {{ $notifications->title }}
                        </td>

                        <td>
                            {{ $notifications->message }}
                        </td>
                        <td>
                            {{ $notifications->type }}
                        </td>
                        
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                    Actions
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                    <!--<li role="presentation">
                                        <a role="menuitem" tabindex="-1" href="{{route('admin_notification_vget', ['notification_id'=>$notifications->notification_id])}}">
                                            <i class="fa fa-eye"> View</i>
                                        </a>
                                    </li>-->
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1" href="{{route('admin_notification_uget', ['notification_id'=>$notifications->notification_id])}}">
                                            <i class="fa fa-edit"> Update</i>
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        @if($notifications->status != 'Active')
                                            <a role="menuitem" tabindex="-1" class="block_cust" id="{{ $notifications->notification_id }}">
                                                <i class="fa fa-eye-slash"> Active</i>
                                            </a>
                                        @else
                                            <a role="menuitem" tabindex="-1" class="unblock_cust" id="{{ $notifications->notification_id }}">
                                                <i class="fa fa-eye"> In Active</i>
                                            </a>
                                        @endif
                                    </li>
                                    <li role="presentation">
                                        <a role='menuitem' tabindex="-1" onclick="delete_popup('{{ $notifications->notification_id }}')">
                                            <i class="fa fa-bitbucket" aria-hidden="true"> Delete </i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                        <td>
                            {{ $notifications->broadcastDate }}
                        </td>
                        <td>
                            {{ $notifications->status }}
                        </td>
                        <td>
                            {{ $notifications->recurring }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
    </div>
</div>
</div>

<script>
    $("#settings_all").addClass("active");
    $("#notification_all").addClass("active");
    var table = $('#example').dataTable( 
    {
        "order": [[ 0, "desc" ]],
        "scrollX": true,
        "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 3, 4 ,7] },
                    { "bSearchable": true, "aTargets": [ 2 ] }
        ],
        "fnDrawCallback": function( oSettings ) 
        {
            $('.block_cust').click(function (e) {
                var id = $(e.currentTarget).attr("id");
                var block = $('#blocked_check').val();
                swal({
                    title: "Are you sure you want to active this notification?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Active It!",
                    closeOnConfirm: false
                }, function () {
                    window.location.href = '{{route("admin_notification_active")}}?block=0&notification_id='+id +'&type='+block;
                });
            });
            $('.unblock_cust').click(function (e) {
                var id = $(e.currentTarget).attr("id");
                var block = $('#blocked_check').val();
                swal({
                    title: "Are you sure you want to deactivate this notification?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Deactivate It!",
                    closeOnConfirm: false
                }, function () {
                    window.location.href = '{{route("admin_notification_active")}}?block=1&notification_id='+id +'&type='+block;
                });
            });
        }
    });

    function delete_popup(notificationid){
        swal({
            title: "Are you sure you want to delete this notification?",
            text: "All data will be completely deleted.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: false
            }, function () {
                window.location.href = '{{route("admin_notification_delete")}}?notification_id='+notificationid;
            });
    }
    
    $(document).ready(function(){
        $('input[name="daterange"]').daterangepicker({
            opens: 'right',
            autoApply: true,
            format: 'DD/MM/YYYY',
            minDate: '1/2/2018',
            maxDate: moment().format('DD/MM/YYYY'),
        }, function(start, end, label) {
            //console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
    });
</script>


@stop
