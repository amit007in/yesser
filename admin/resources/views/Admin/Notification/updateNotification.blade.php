@extends('Admin.layout')

@section('title') 
    Edit Notification
@stop

    @section('content')

<link href="{{ URL::asset('AdminAssets/css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add Notification</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_notification_all')}}">All Notification</a>
                        </li>
                        <li>
                            <a href="{{route('admin_notification_uget') }}">
                                <b>Edit Notification</b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <br>
                <form method="post" action="{{route('admin_notification_upost') }}" enctype="multipart/form-data" id="addForm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name ="notification_id" id="notification_id" value="{!! $notification->notification_id !!}" >
                    
                    <div class="row">
                        <div class="form-group">
                        
                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Title</label>
                                <input type="text" placeholder="Notification Title" autocomplete="off" class="form-control" required name="title" value="{!! $notification->title !!}" autofocus="on" id="title">
                            </div> 

                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>Message</label>
                                <input type="text" placeholder="Message" autocomplete="off" class="form-control" required name="message" value="{!! $notification->message !!}" id="message">
                            </div> 

                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                        
                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Type</label>
                                <select name="type" id="type"  class="form-control">
                                    <option  value="Customer" {{ $notification->type == 'Customer' ? 'selected="selected"' : '' }}>Customer</option>
                                    <option  value="Driver" {{ $notification->type == 'Driver' ? 'selected="selected"' : '' }}>Driver</option>
                                </select>
                            </div>
                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>Broadcast Date</label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" placeholder="Broadcast Date" autocomplete="off" class="form-control" required name="date" value="{!! $notification->broadcastDate !!}" id="date" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Status</label>&nbsp;&nbsp;
                                <input type="radio" value="Active" required name="status" id="status" {{ $notification->status == 'Active' ? 'checked="checked"' : '' }}> &nbsp;Active &nbsp;&nbsp;
                                <input type="radio" value="In Active" required name="status" id="status" {{ $notification->status == 'In Active' ? 'checked="checked"' : '' }}> &nbsp;In Active
                            </div> 
                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>Recurring</label>&nbsp;&nbsp;
                                <input type="radio" value="On" required name="recurring" id="recurring" {{ $notification->recurring == 'On' ? 'checked="checked"' : '' }}> &nbsp;On &nbsp;&nbsp;
                                <input type="radio" value="Off" required name="recurring" id="recurring" {{ $notification->recurring == 'Off' ? 'checked="checked"' : '' }}> &nbsp;Off
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Image</label>&nbsp;&nbsp;
                                <input type="file" accept="image/*" name="notificationImage"  id="notificationImage">
                            </div>
                            <img alt="image" class="img-circle" style="max-width:100px !important;max-height:70px !important;margin-left: -6%;margin-bottom: 13px;" src="{!! $notification->image !!}">
                        </div>
                    </div>
                    <br>
                    
                     <div class="row">
                        <div class="form-group">
                            <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1">
                            <label for="coupon_id">Add Promocode</label>
                                <select class="form-control border-info" name="coupon_id">
                                    <option value="">Please select promocode</option>
                                    @foreach($promocode as $promo)
                                        @if($promo->coupon_id == $notification->coupon_id)
                                            <option value="{{$promo->coupon_id}}" selected="true">{{$promo->code}}</option>
                                        @else
                                            <option value="{{$promo->coupon_id}}">{{$promo->code}}</option>
                                        @endif    
                                    @endforeach
                                </select>
                            </div>
                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                               <label>Service</label>
                               <select class="form-control border-info" name="service_id" id="service_id">
                                   @if($notification->service_id == 0)
                                   <option value="">Select Service</option>
                                   @endif
                                   @foreach($Services as $category)
                                       @if($notification->service_id == $category->category_id)
                                       <option value="{{$category->category_id}}" selected="true">{{$category->name}}</option>
                                       @else
                                       <option value="{{$category->category_id}}" >{{$category->name}}</option>
                                       @endif
                                   @endforeach
                               </select>
                           </div>
                        </div>
                    </div>
                    <br><br><br>
                    <div class="row">
                        <div class="form-group">
                            <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            {!! Form::submit('Update Notification', ['class' => 'btn btn-success']) !!}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">

$("#settings_all").addClass("active");
    $("#notification_all").addClass("active");

        $("#addForm").validate({
            rules: {
                title: {
                    required: true,
                    maxlength: 255,
                },
                message: {
                    required: true,
                    maxlength: 255,
                },
                date: {
                    required: true,
                },
                status: {
                    required: true,
                },
                recurring: {
                    required: true,
                }
 
            },
            messages: {
                title: {
                    required: "Please provide the title"
                },
                message: {
                    required: "Please provide the notification message"
                },
                date: {
                    required: "Please provide the broadcast date"
                },
                status: {
                    required: "Please select the status"
                },
                recurring: {
                    required: "Please select the recurring"
                }

            }
        });

        $('#date').datetimepicker({
            startDate: new Date(),
            format: 'yyyy-mm-dd hh:ii:00'
        });

</script>
<style>
label[for=date]
{
   float: right;
   text-align: center;
   margin-top: -54px;
}
label[for=status]
{
   float: right;
   margin-top: 2px;
}
label[for=recurring]
{
   float: right;
   margin-top: 2px;
}
</style>
@stop



