@extends('Admin.layout')

@section('title') 
    View Notification
@stop

    @section('content')

<link href="{{ URL::asset('AdminAssets/css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add Notification</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_notification_all')}}">All Notification</a>
                        </li>
                        <li>
                            <a href="{{route('admin_notification_aget') }}">
                                <b>View Notification</b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <br>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <img alt="image" class="img-circle" style="max-width:100px !important;max-height:70px !important;margin-left: 47%;margin-bottom: 13px;" src="{{ $notification->image }}">
                   
                    <div class="row">
                        <div class="form-group">
                        
                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Title</label>
                                <input type="text" placeholder="Notification Title" autocomplete="off" class="form-control" required name="title" value="{!! $notification->title !!}" autofocus="on" id="title" readonly>
                            </div> 

                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>Message</label>
                                <input type="text" placeholder="Message" autocomplete="off" class="form-control" required name="message" value="{!! $notification->message !!}" id="message" readonly>
                            </div> 

                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                        
                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Type</label>
                                <input type="text" placeholder="Message" autocomplete="off" class="form-control" required name="message" value="{!! $notification->type !!}" id="message" readonly>
                            </div> 

                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>Broadcast Date</label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" placeholder="Broadcast Date" autocomplete="off" class="form-control" required name="date" value="{!! $notification->broadcastDate  !!}" id="date" readonly>
                                </div>
                                
                            </div> 

                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Status</label>
                                <input type="text" placeholder="Message" autocomplete="off" class="form-control" required name="message" value="{!! $notification->status !!}" id="message" readonly>
                            </div> 
                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>Recurring</label>
                                <input type="text" placeholder="Message" autocomplete="off" class="form-control" required name="message" value="{!! $notification->recurring !!}" id="message" readonly>
                            </div> 
                        </div>
                    </div>
                    <br>
                    <br><br><br>
                    <div class="row">
                        <div class="form-group">
                            <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            <a href="{{route('admin_notification_all')}}"><button class="btn btn-success"> Back </button> </a>
                            </div>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</div>
    <script>
    $("#settings_all").addClass("active");
    $("#notification_all").addClass("active");
    </script>
@stop



