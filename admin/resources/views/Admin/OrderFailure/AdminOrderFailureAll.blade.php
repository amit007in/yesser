
@extends('Admin.layout')

@section('title')
    All Orders
@stop

@section('content')
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All Failure Orders
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_orders_failure') }}">
                                <b>
                                    All Failure Orders
                                </b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
           <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
                
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Status</th>
                                            <th>Category</th>
                                            <th>Customer Name</th>
                                            <th>Product</th>
                                            <th>Booked At</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($OrderArray as $order)
                                        <tr class="gradeA footable-odd" >
                                            <td>{{ $order['Id'] }}</td>

                                            <td>
                                                {{ $order['Status'] }}
                                            </td>
                                            <td>
                                                {{ $order['CategoryName'] }}
                                            </td>
                                            <td>
                                                {{ $order['CustomerName'] }}
                                            </td>
                                            <td>
                                                {{ $order['OrderProduct'] }}
                                            </td>
                                            <td><i class="fa fa-clock-o"></i> {{ $order['Created_at'] }} </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
									<tfoot>
										<center>
											<h2>Total - {{ count($OrderArray) }}</h2>
										</center>
									</tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
        </div>
    </div>

    <script>

        $("#orderfailure").addClass("active");
		var table = $('#example').dataTable( 
            {
                "order": [[ 0, "desc" ]],
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 4 ] },
                    { "bSearchable": true, "aTargets": [ 3 ] }
                ],

                    dom: 'Blfrtip',
                    buttons: [
                       {
                            extend: 'pdf',
                            footer: true,
                            exportOptions: {
                                columns: [0,1,2,3,4]
                            }
                       },
                       {
                           extend: 'csv',
                           footer: false,
                            exportOptions: {
                               columns: [0,1,2,3,4]
                            }
                          
                       },
                       {
                           extend: 'excel',
                           footer: false,
                            exportOptions: {
                                columns: [0,1,2,3,4]
                            }
                       }         
                    ]

            });

    </script>


@stop
