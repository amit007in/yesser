@extends('Admin.layout')

@section('title') 
    Order Details
@stop

@section('content')
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="row">

                <div class="col-lg-10">
                    <h2>
                        Order Details
                    </h2>
                    
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_orders')}}"> Orders</a>
                        </li>
                        <li>
                            <a href="{{route('admin_order_details',['order_id'=>$order->order_id])}}">
                                <b>
                                    Order Details
                                </b>                            
                            </a>
                        </li>
                    </ol>
                </div>
                
                <div class="col-lg-2">

                </div>
            
            </div>
        </div>

<style type="text/css">
    #legend {
        background: #FFF;
        padding: 10px;
        margin: 5px;
        font-size: 12px;
        font-family: Arial, sans-serif;
      }

</style>

        <div class="row">
<!--            Map Canvas                            -->
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div id="map1" style="width:100%; height:450px;"></div>
            </div>
<!--            Map Canvas                            -->
        </div>
<br>
        <div class="row">

<!--            Order Details                         -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Order Info</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Status</th>
                            <th>Timing</th>
                            <th>Pickup Location</th>
                            <th>Dropoff Location</th>
                            <th>Last Updated At</th>
                        </tr>
                    </thead>
                    <tbody>
                <tr>

                    <td>{{ $order->order_id }}</td>

                    <td>
                        @include('Common.Partials.orderStatus')
                        <br>
                        {{ $order->cancel_reason }}
                    </td>


                    <td><i class="fa fa-clock-o"></i> {{ $order->order_timingsz }} </td>

                    <td>
                        {{ $order->pickup_address }}
                    </td>

                    <td>
                        {{ $order->dropoff_address }}
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order->updated_atz }} </td>

                </tr>

                    </tbody>
                </table>

        </div>

            </div>

        </div>

    </div>
<!--            Order Details                         -->

<!--            Product Details                      -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Product Info</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Service</th>
                            <th>Category</th>
                            <th>Product</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
					@foreach($orderProduct['Products'] as $Product)
					<tr>

						<td>{{ $Product['Id'] }}</td>

						<td>{{ $Product['CategoryName'] }}</td>

						<td>{{ $Product['BrandName'] }}</td>

						<td>{{ $Product['productName'] }}</td>

						<td>{{ $Product['Quantity'] }}</td>

					</tr>
					@endforeach
                    </tbody>
                </table>

        </div>

            </div>

        </div>

    </div>
<!--            Product Details                      -->

<!--            Payment Details                         -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Payment Info</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Transaction Id</th>
                            <th>Status</th>
                            <th>Method</th>
                            <th>Alpha Charges</th>
							<th>Product Quantity </th>
                            <th>Quantity Charges</th>
                            <th>Admin Charges</th>
                            <th>Bank Charges</th>
                            <!--<th>Floor Charges</th>
                            <th>Reward Price</th>-->
                            <th>Total Charges</th>
                        </tr>
                    </thead>
                    <tbody>
                <tr>

                    <td>
                        {{ $order->payment['transaction_id'] }}
                    </td>

                    <td>
                            @if($order->payment['payment_status'] == "Completed")
                                <span class="label label-primary">
                                    Completed
                                </span>
                            @elseif($order->payment['payment_status'] == "Pending")
                                <span class="label label-warning">
                                    Pending
                                </span>
                            @elseif($order->payment['payment_status'] == "Failed")
                                <span class="label label-info">
                                    Failed
                                </span>
                            @elseif($order->payment['payment_status'] == "Refunded")
                                <span class="label label-success">
                                    Refunded
                                </span>
    @elseif($order->payment['payment_status'] == "CustCancel" || $order->payment['payment_status'] == "DriverCancel" || $order->payment['payment_status'] == "SerReject" || $order->payment['payment_status'] == "SupReject" || $order->payment['payment_status'] == "DSchCancelled" || $order->payment['payment_status'] == "SysSchCancelled" || $order->payment['payment_status'] == "DSchTimeout" || $order->payment['payment_status'] == "CTimeout" || $order->payment['payment_status'] == "SerCustCancel")
                                <span class="label label-danger">
                                    Cancelled
                                </span>
                            @else
                                <span class="label label-success">
                                    {{$order->payment['payment_status']}}
                                </span>
                            @endif
                    </td>

                    <td>
                        @if($order->payment['payment_type']  == "Cash")
                            <span class="label label-success">
                                Cash
                            </span>
                        @elseif($order->payment['payment_type']  == "Card")
                            <span class="label label-primary">
                                Card
                            </span>
                        @elseif($order->payment['payment_type']  == "Coupon")
                            <span class="label label-warning">
                                Coupon
                            </span>
                        @else
                            <span class="label label-info">
                                EToken
                            </span>
                        @endif
                    </td>

                    <td>
                        <b>OMR</b> {{ $order->payment['product_alpha_charge'] }}
                    </td>
					<td>
					@foreach($orderProduct['Products'] as $Product)
					<span class="label label-info" style="padding: 1px 8px; !important">
						{{ $Product['Quantity'] }}
					</span></br>
					@endforeach
					</td>
					<td>
					@foreach($orderProduct['Products'] as $Product)
					<span class="label label-info" style="padding: 1px 8px; !important">
						 {{ number_format($Product['price_per_item'], 2) }}
					</span></br>
					@endforeach
					</td>
                    <!--<td>
                        <b>OMR</b> {{ $order->payment['product_per_quantity_charge'] }}
                    </td>-->
                    <td>
                        <b>OMR</b> {{ $order->payment['admin_charge'] }}
                    </td>

                    <td>
                        <b>OMR</b> {{ $order->payment['bank_charge'] }}
                    </td>
					<!-- M3 Part 3.2 
                    <td>
                        <b>OMR</b> {{ $order->floor_charge }}
                    </td>
					
                    <td>
                        <b>OMR</b> {{ $order->reward_discount_price }}
                    </td>-->
					<td> <!-- M3 Part 3.2 --->
					<?php 
						$totalPrice       = $orderProduct['OrderPayment'];
						$aPriceProduct    = str_replace(',', '', $orderProduct['OrderPayment']);
						$floorCharge      = str_replace(',', '', $order->floor_charge);
						$reward_discount_price  = str_replace(',', '', $order->reward_discount_price);
						$totalPriceReward = $floorCharge - $reward_discount_price;
						$total            = $aPriceProduct + $totalPriceReward;	
					?>
                        <b>OMR</b> <?php echo number_format($total,2); ?>
                    </td>
                </tr>

                    </tbody>
                </table>

        </div>

            </div>

        </div>

    </div>
<!--            Payment Details                         -->

<!--            Etokens Details                         -->
@if($order->organisation_coupon_user)
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Etoken Info</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>eToken Total</th>
                            <th>Paid Amount</th>
                            <th>Pending Amount</th>
                            <th>eToken Left</th>
                            <th>Purchased At</th>
                        </tr>
                    </thead>
                    <tbody>
                <tr>

                    <td>
                        {{ $order['organisation_coupon_user']->organisation_coupon_id }}
                    </td>

                    <td>
                        {{ $order['organisation_coupon_user']->quantity }}
                    </td>

                    <td>
                       <b>OMR</b> {{ $order['organisation_coupon_user']->successPaymentsAmount }}
                    </td>

                    <td>
                       <b>OMR</b> {{ $order['organisation_coupon_user']->pendingPaymentsAmount }}
                    </td>

                    <td>
                        {{ $order['organisation_coupon_user']->quantity_left }}
                    </td>

                    <td>
                        <i class="fa fa-clock-o"> {{ $order['organisation_coupon_user']->created_atz }}</i>
                    </td>

                </tr>

                    </tbody>
                </table>

        </div>

            </div>

        </div>

    </div>
@endif
<!--            Etokens Details                          -->

<!--            Coupons Details                          -->
@if($order->coupon_user)
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Coupons Info</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Purchased At</th>
                            <th>Coupon Code</th>
                            <th>Coupon Type</th>
                            <th>Value</th>
                            <th>Rides Left</th>
                            <th>Expires At</th>
                        </tr>
                    </thead>
                    <tbody>
                <tr>

                    <td>
                        {{ $order['coupon_user']->coupon_user_id }}
                    </td>

                    <td>
                        <i class="fa fa-clock-o"> {{ $order['coupon_user']->created_atz }}</i>
                    </td>

                    <td>
                        {{ $order['coupon_user']->code }}
                    </td>

                    <td>
                        <center>
                            @if($order['coupon_user']->coupon_type == 'Percentage')
                                <span class="label label-warning">
                                    Percentage
                                </span>
                            @else
                                <span class="label label-success">
                                    Value
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                       <b>OMR</b> {{ $order['coupon_user']->coupon_value }}
                    </td>

                    <td>
                        {{ $order['coupon_user']->rides_left }}
                    </td>

                    <td>
                        <i class="fa fa-clock-o"> {{ $order['coupon_user']->expires_atz }}</i>
                    </td>

                </tr>

                    </tbody>
                </table>

        </div>

            </div>

        </div>

    </div>
@endif
<!--            Coupons Details                          -->


<!--            Customer Details                         -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Customer Info</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Customer Name</th>
                            <th>Customer Pic</th>
                            <th>Ordered At</th>
                        </tr>
                    </thead>
                    <tbody>
                <tr>

                    <td>
                        {{ $order['customer_user_detail']->user_detail_id }}
                    </td>

                    <td>
                        {{ $order['customer_user']->name }} - ({{ $order['customer_user']->phone_number }})
                    </td>

                    <td>
    <!--<a href="{{ $order['customer_user_detail']->profile_pic_url }}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
        <img src="{{ $order['customer_user_detail']->profile_pic_url }}/150/150"  class="img-circle">
    </a>-->
					@if($order['customer_user_detail']->profile_pic == "")
					<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
						<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
					</a>
					@else
					<a href="{{ env('SOCKET_URL').'/Uploads/'.$order['customer_user_detail']->profile_pic }}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
						<img src="{{ env('SOCKET_URL').'/Uploads/'.$order['customer_user_detail']->profile_pic }}"  class="img-circle">
					</a>
					@endif
                    </td>
                    </td>

                    <td>
                        <i class="fa fa-clock-o"> {{ $order->created_atz }}</i>
                    </td>

                </tr>

                    </tbody>
                </table>

        </div>

            </div>

        </div>

    </div>
<!--            Customer Details                         -->


<!--            Driver Details                           -->
@if($order->driver_user_detail)
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Yesser Man Info</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Yesser Man Name</th>
                            <th>Yesser Man Pic</th>
                        </tr>
                    </thead>
                    <tbody>
                <tr>

                    <td>
                        {{ $order['driver_user_detail']->user_detail_id }}
                    </td>

                    <td>
                        {{ $order['driver_user']->name }} - ({{ $order['driver_user']->phone_number }})
                    </td>

                    <td>
    <!--<a href="{{ $order['driver_user_detail']->profile_pic_url }}" title="{{ $order['driver_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
        <img src="{{ $order['driver_user_detail']->profile_pic_url }}/150/150"  class="img-circle">
    </a>-->
					@if($order['driver_user_detail']->profile_pic != "")
						@if($order['driver_user_detail']->created_by == 'Admin')
							 <a href="{{ URL::asset('BuraqExpress/Uploads/'.$order['driver_user_detail']->profile_pic) }}" title="{{ $order['driver_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
								<img src="{{ URL::asset('BuraqExpress/Uploads/'.$order['driver_user_detail']->profile_pic) }}"  class="img-circle">
							</a>
						@else
							<a href="{{ env('SOCKET_URL').'/Uploads/'.$order['driver_user_detail']->profile_pic }}" title="{{ $order['driver_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
								<img src="{{ env('SOCKET_URL').'/Uploads/'.$order['driver_user_detail']->profile_pic }}"  class="img-circle">
							</a>
						@endif
					@else
						<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $order['driver_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
						</a>
					@endif

                    </td>

                </tr>

                    </tbody>
                </table>

        </div>

            </div>

        </div>

    </div>

@endif
<!--            Driver Details                           -->

<!--            Ratings                                  -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Ratings Given By</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">

            <div class="col-sm-6 col-md-6 col-lg-6">
                <center><h4>Customer</h4></center>
                <table class="table table-hover">
                    <thead>
                        <tr>
<!--                             <th>Id</th> -->
                            <th width="5%">Ratings</th>
                            <th width="80%">Comments</th>
                            <th width="15%">Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($order['customer_rating'])

                <tr>

<!--                     <td>
                        {{ $order['customer_rating']->order_rating_id }}
                    </td>
 -->
                     <td>
                        <i class="fa fa-star-half-empty"></i> {{ $order['customer_rating']->ratings }}
                    </td>

                    <td>
                        {{ $order['customer_rating']->comments }}
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order['customer_rating']->created_atz }} </td>

                </tr>


                        @endif
                    </tbody>
                </table>                
            </div>


            <div class="col-sm-6 col-md-6 col-lg-6">
                <center><h4>Yesser Man</h4></center>
                <table class="table table-hover">
                    <thead>
                        <tr>
<!--                             <th>Id</th> -->
                            <th width="5%">Ratings</th>
                            <th width="80%">Comments</th>
                            <th width="15%">Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($order['driver_rating'])

                <tr>

<!--                     <td>
                        {{ $order['driver_rating']->order_rating_id }}
                    </td>
 -->
                     <td>
                        <i class="fa fa-star-half-empty"></i> {{ $order['driver_rating']->ratings }}
                    </td>

                    <td>
                        {{ $order['driver_rating']->comments }}
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order['driver_rating']->created_atz }} </td>

                </tr>


                        @endif
                    </tbody>
                </table>                
            </div>

        </div>

            </div>

        </div>

    </div>
<!--            Ratings                              -->

        </div>

<br><br><br>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key={{env('GOOGLE_MAP_KEY')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.dev.js"></script>

<script type="text/javascript">

    $("#orders").addClass("active");

    var socket = io.connect("{{env('SOCKET_URL')}}", { query: { admin_access_token: "{{$admin->access_token}}" } } );

    var pickup_address = "{{$order->pickup_address}}";
    var pickup_latitude = parseFloat("{{$order->pickup_latitude}}");
    var pickup_longitude = parseFloat("{{$order->pickup_longitude}}");

    var dropoff_address = "{{$order->dropoff_address}}";
    var dropoff_latitude = parseFloat("{{$order->dropoff_latitude}}");
    var dropoff_longitude = parseFloat("{{$order->dropoff_longitude}}");

    var driver_latitude, driver_longitude, myMap, infowindow, bounds, pickupMarker, dropoffMarker, driverMarker, mapOptions, order_status, driver_icon, driver_new_longitude, driver_new_longitude, driver_rotation;

    order_status = "{{$order->order_status}}";
//console.log(pickup_latitude, pickup_longitude, dropoff_latitude, dropoff_longitude);

    function driver_icon_image(category_id)
        {
            if(category_id == 1)////////////       Gas     ////////////////
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_gas.png";
            else if(category_id == 2)////////////       Drinking Water  ///
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_drink_water.png";
            else if(category_id == 3)////////////       Water Tanker    ///
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_water_tank.png";
            else if(category_id == 4)////////////       Freight        /////////
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_freights.png";
            else if(category_id == 5)////////////       Tow        /////////////
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_tow.png";
            else if(category_id == 6)////////////       Heavy        ///////////
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_heavy.png";
            else
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/sup_icon.png";
        }

        driver_icon_image("{{$order->category_id}}");
        //console.log('{{$order->category_id}}',driver_icon);

    function load_map()
        {

            mapOptions = {
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoom: 7,
                    center:new google.maps.LatLng(dropoff_latitude,dropoff_longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false
                };

            myMap = new google.maps.Map(document.getElementById('map1'), mapOptions);

            var legend = document.createElement('div');
            legend.id = 'legend';

            infowindow = new google.maps.InfoWindow();
            bounds = new google.maps.LatLngBounds();

/////////////// Pick up Marker  ///////////////////////

            if(pickup_latitude != 0.000000 && pickup_longitude != 0.000000)
                {
                    pickupMarker = new google.maps.Marker({
                        animation: google.maps.Animation.DROP,
                        position: {"lat": pickup_latitude,"lng": pickup_longitude},
                        title: 'Pickup - '+pickup_address,
                        map: myMap,
                        icon: "{{env('ADMIN_ASSET')}}Tracking/marker_pickup.png",
                    });

                    div= document.createElement('div');
                    div.innerHTML = '<span><img src="{{env('ADMIN_ASSET')}}Tracking/marker_pickup.png"> Pickup</span>';
                    legend.appendChild(div);
                }

/////////////// Pick up Marker  ///////////////////////

/////////////// DropOff Marker  ///////////////////////
            // if(dropoff_latitude != 0.000000 && dropoff_longitude != 0.000000)
            //     {

                    dropoffMarker = new google.maps.Marker({
                        animation: google.maps.Animation.DROP,
                        position: {"lat": dropoff_latitude,"lng": dropoff_longitude},
                        title: 'Dropoff - '+dropoff_address,
                        map: myMap,
                        icon: "{{env('ADMIN_ASSET')}}Tracking/marker_drop.png"
                    });

                    div= document.createElement('div');
                    div.innerHTML = '<span><img src="{{env('ADMIN_ASSET')}}Tracking/marker_drop.png"> Dropoff</span>';
                    legend.appendChild(div);

                    bounds.extend(dropoffMarker.position);

                    console.log('dropoffMarker.position', dropoff_latitude, dropoff_longitude);

//                }
/////////////// DropOff Marker  ///////////////////////

            if(order_status == "Ongoing" || order_status == "Confirmed" || order_status == "DApproved" || order_status == "SerCustPending")
                {
                    setTimeout(pollingFunction,15000);
                }
            else if(order_status == "SerComplete" || order_status == "SupComplete")
                {
                    var pausecontent = <?php echo json_encode($order->CRequest['full_track']); ?>;

                    draw_polyLine(pausecontent);
                }

            myMap.controls[google.maps.ControlPosition.RIGHT_TOP].push(legend);

        }

    function pollingFunction()
        {

            if(order_status == "Ongoing" || order_status == "Confirmed")
                {
                    setTimeout(pollingFunction,15000);//setInterval( pollingFunction, 15000);
                }

            trackDriver();
        }

    function trackDriver()
        {
            console.log("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");

            if(order_status == "Ongoing" ||  order_status == "Confirmed")
                {

                    socket.emit("PanelCommonEvent", { type:"TrackDriver", order_id: parseInt("{{$order->order_id}}") }, (response) => {
                        //console.log(response);
                        if(response.success == 0)
                            {
                                toastr.error('Not able to track',response.msg);
                            }
                        else if(response.order && response.order.CRequest)
                            {
                                order_status = response.order.order_status;

                                //driver_latitude = parseFloat(response.order.CRequest.driver_current_latitude)+(parseFloat(0.09)*increment);
                                driver_latitude = parseFloat(response.order.CRequest.driver_current_latitude);

                                //driver_longitude = parseFloat(response.order.CRequest.driver_current_longitude)+(parseFloat(0.09)*increment);
                                driver_longitude = parseFloat(response.order.CRequest.driver_current_longitude);

                                //driver_rotation = parseFloat(response.order.CRequest.rotation)+parseFloat(10*increment);
                                driver_rotation = parseFloat(response.order.CRequest.rotation);
                                //increment++;

                                add_driver_marker(response.order.CRequest);
                                draw_polyLine(response.order.CRequest.full_track);
                            }

                    });

                }

        }

    function add_driver_marker(request)
        {
            if (driverMarker && driverMarker.setMap) {
                driverMarker.setMap(null);
            }

            driverMarker = new google.maps.Marker({
                position: new google.maps.LatLng(driver_latitude, driver_longitude),  //myMap.getCenter(),
                map: myMap,
                title: "driver",
                icon: {
                    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,   //url: driver_icon,
                    scale: 4,
                    strokeColor: '#00F',
                    rotation: driver_rotation,
                }
            });

            myMap.panTo(driverMarker.position);
            bounds.extend(driverMarker.position);
        }

    function draw_polyLine(full_tracks)
        {
            //console.log(full_tracks);

            var ridePlanCoordinates = [];

            for(i=0; i< full_tracks.length; i++ )
                {
                    ridePlanCoordinates.push( new google.maps.LatLng(full_tracks[i].latitude, full_tracks[i].longitude ) );
                }

            if(ridePlanCoordinates.length != 0)
                {

                    ridepath = new google.maps.Polyline({
                        path: ridePlanCoordinates,
                        strokeColor: '#2E3849',
                        strokeOpacity: 1.5,
                        strokeWeight: 3.5
                    });

                    ridepath.setMap(myMap);

                }
        }

    load_map();

</script>

@stop