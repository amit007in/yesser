
@extends('Admin.layout')

@section('title')
    All Orders
@stop

@section('content')

    <link href="{{ URL::asset('AdminAssets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('AdminAssets/js/plugins/select2/select2.full.min.js') }}"></script>

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All Orders
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_orders') }}">
                                <b>
                                    All Orders
                                </b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

<?php

    $search = isset($data['search']) ? $data['search'] : '';
    $daterange =  $data['daterange'];

?>

                <form method="post" action="{{ route('admin_orders') }}">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <meta name="csrf-token" content="{{ csrf_token() }}">

                    <div class="row">

                        <div class="col-lg-4 col-md-4">
                            <div class="form-group">
                                <label class="pull-left">Status</label>
                                    <select class="form-control" id="status_check" name="status_check" value="{{$data['status_check']}}">
                                        <option value="All">All</option>

                                        <option value="Searching">Searching</option>
                                        <option value="Confirmed">Confirmed</option>
                                        <option value="Scheduled">Scheduled</option>

                                        <option value="Ongoing">Ongoing</option>
                                        <option value="Rejected">Rejected</option>
                                        <option value="Timeout">Timeout</option>

                                        <option value="Customer Cancelled">Customer Cancelled</option>
                                        <option value="Driver Cancelled">Yesser Man Cancelled</option>

                                        <option value="Completed">Completed</option>
                                        <option value="Confirmation Pending">Confirmation Pending</option>
										<option value="Driver not found">Yesser Man Not Found</option>
                                    </select>
                            </div>
                        </div>


                        <div class="col-lg-4 col-md-4">
                            <label>
                                Filter Booking Timing
                            </label>

                            <center>
                    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$data['daterange']}}"/>
                            </center>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <label>Search</label>
                            <input type="text" name="search" class="form-control" placeholder="Search" value="{{$search}}">
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label class="pull-left">Services</label>
                                <select class="form-control categories" id="categories" name="categories[]" multiple="true" required="true">
                                    @foreach($categories as $category)
                                        @if($category->selected == 1)
                                            <option value="{{$category->category_id}}" title="{{$category->name}}" selected>
                                        @else
                                            <option value="{{$category->category_id}}" title="{{$category->name}}">
                                        @endif
                                                {{ $category->name }}
                                            </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3 col-lg-3">
                            <div class="form-group">
                                <label class="pull-left">Ordering</label>
                                <select class="form-control ordering_filter" id="ordering_filter" name="ordering_filter">
                                    <option value="0">Created Time ASC</option>
                                    <option value="1">Created Time DESC</option>
                                    <option value="2">Booking Time ASC</option>
                                    <option value="3">Booking Time DESC</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3">
                            <label class="pull-left">Filter</label><br>
                            <button type="submit" class="btn btn-primary btn-big">Filter</button>
                            <a class="btn btn-success btn-big" id="export_excell" onclick="export_excell()">Export Excel</a>
                        </div>

                    </div>


                </form>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Type</th>
                                    <th>Customer</th>
                                    <th>CustomerPic</th>
                                    <th>Timing</th>
                                    <th>Pickup Location</th>
                                    <th>DropOff Location</th>
                                    <th>Services</th>
                                    <th>Category</th>
                                    <th>Product</th>
                                    <th>Payment</th>
                                    <th>Yesser Man</th>
                                    <th>Yesser Man Pic</th>
									<th>Re-Assign</th>
                                    <th>Details</th>
                                    <th>Booked At</th>
                                </tr>

                                </thead>
                                <tbody>

            @foreach($orders as $order)
                <tr class="gradeA footable-odd">

                    <td>{{ $order->order_id }}</td>

                    <td>
                        @include('Common.Partials.orderStatus')
                    </td>

                    <td>
                        @if($order->order_type == 'Service')
                            <span class="label label-info">
                                <i class="fa fa-certificate"> Service</i>
                            </span>
                        @else
                            <span class="label label-primary">
                                <i class="fa fa-support"> Support</i>
                            </span>
                        @endif
                    </td>

                    <td>
                        {{ $order['customer_user']->name }} <p> ({{ $order->phone_number }}) </p>
                        <hr>
                        <b>OTP -</b> {{ $order->otp }}
                    </td>

                    <td>
					<!--<a href="{{ $order['customer_user_detail']->profile_pic_url }}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
						<img src="{{ $order['customer_user_detail']->profile_pic_url }}/150/150"  class="img-circle">
					</a>-->
					@if($order['customer_user_detail']->profile_pic == "")
						<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
						</a>
					 @else
						<a href="{{ env('SOCKET_URL').'/Uploads/'.$order['customer_user_detail']->profile_pic }}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ env('SOCKET_URL').'/Uploads/'.$order['customer_user_detail']->profile_pic }}"  class="img-circle">
						</a>
					 @endif
                    </td>

                    <td>
                        <i class="fa fa-clock-o"></i> {{ $order->order_timingsz }}
                        @if($order->future == '1')
                            <span class="label label-primary">Scheduled</span>
                        @else
                            <span class="label label-success">Current</span>
                        @endif
                    </td>

                    <td style="height:100px;overflow:auto;">
                        {!! $order->pickup_address !!}
                    </td>

                    <td style="height:100px;overflow:auto;">
                        {{ $order->dropoff_address }}
                    </td>

                    <td>
                       {{ $order['category']->name }}
                    </td>

                    <td>
                        @if(isset($order['category_brand']))
                            {{ $order['category_brand']->name }}
                        @endif
                    </td>

                    <td>
					<span class="label label-info" style="padding: 1px 8px; !important">
						{{ $order->payment['product_quantity'] }} *
						<?php for($i = 0;$i<count($order->order_products);$i++){ 
							if(count($order->order_products) == $i +1){ ?>
						{{ $order->order_products[$i]['productName'] }}
						<?php }elseif(count($order->order_products) == 1){ ?>
						{{ $order->order_products[$i]['productName'] }}
						<?php }else{ ?>
						{{ $order->order_products[$i]['productName'] }} <b>,</b> </br>
						<?php } } ?>
					</span>
                    </td>
                    <td>
					   <?php $totalPrice = array(); for($i = 0;$i<count($order->order_products);$i++){
							$totalPrice[] = $order->order_products[$i]['price_per_item'];
						 } // Part M3 3.2
						$totalPrice[] = $order->floor_charge;
						$total = array_sum($totalPrice) - $order->reward_discount_price ;	
						?>
						<b>OMR</b>  <?php echo number_format($total,2); ?>
                    </td>

                    @if(isset($order['driver_user_detail']))
                        <td>
                            {{ $order['driver_user_detail']->name }}
                        </td>

                        <td>
        <!--<a href="{{ $order['driver_user_detail']->profile_pic_url }}" title="{{ $order['driver_user_detail']->name }}" class="lightBoxGallery img-circle" data-gallery="">
            <img src="{{ $order['driver_user_detail']->profile_pic_url }}/150/150"  class="img-circle">
        </a>
					@if($order['driver_user_detail']->profile_pic == "")
						<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $order['driver_user_detail']->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
						</a>
					 @else
						<a href="{{ env('SOCKET_URL').'/Uploads/'.$order['driver_user_detail']->profile_pic }}" title="{{ $order['driver_user_detail']->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ env('SOCKET_URL').'/Uploads/'.$order['driver_user_detail']->profile_pic }}"  class="img-circle">
						</a>
					 @endif-->
					 @if($order['driver_user_detail']->profile_pic != "")
						@if($order['driver_user_detail']->created_by == 'Admin')
							 <a href="{{ URL::asset('BuraqExpress/Uploads/'.$order['driver_user_detail']->profile_pic) }}" title="{{ $order['driver_user_detail']->name }}" class="lightBoxGallery img-circle" data-gallery="">
								<img src="{{ URL::asset('BuraqExpress/Uploads/'.$order['driver_user_detail']->profile_pic) }}"  class="img-circle">
							</a>
						@else
							<a href="{{ env('SOCKET_URL').'/Uploads/'.$order['driver_user_detail']->profile_pic }}" title="{{ $order['driver_user_detail']->name }}" class="lightBoxGallery img-circle" data-gallery="">
								<img src="{{ env('SOCKET_URL').'/Uploads/'.$order['driver_user_detail']->profile_pic }}"  class="img-circle">
							</a>
						@endif
					@else
						<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $order['driver_user_detail']->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
						</a>
					@endif
                        </td>

                    @else
                        <td></td>
                        <td></td>
                    @endif
					
					@if($order->category['reassign_status'] == '0') 
						@if($order->order_status == "Driver not found" || $order->order_status == "SerTimeout" || $order->order_status == "SupTimeout" || $order->order_status == "DSchTimeout" || $order->order_status == "CTimeout" || $order->order_status == "SerReject" || $order->order_status == "SupReject" || $order->order_status == "DriverCancel" || $order->order_status == "DSchCancelled" )
							<td><a onclick="ShowDrivers({{ $order->order_id }})" class="btn btn-primary pull-right"> Assign Yesser Man </a></td>
						@elseif($order->order_status == "Confirmed")
							<td><a onclick="ShowDriversShuffle({{ $order->order_id }})" class="btn btn-primary pull-right"> Re-Shuffle Order </a></td>
						@else
							<td></td>
						@endif
					@else
						<td></td>
					@endif
                    <td>
            <a href="{{ route('admin_order_details', ['order_id' => $order->order_id] ) }}" class="btn btn-primary pull-right" id="Details" title="Details">
                <i class="fa fa-ioxhost"> Order Details</i>
            </a>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>


                                <tfoot>
                <center>
                    <h2>Total - {{ $orders->total() }}</h2>
                </center>
                <center>
                    {{ $orders->appends([ 'status_check' => $data['status_check'], 'daterange' => $data['daterange'], 'categories' => $data['categories'], 'ordering_filter' => $data['ordering_filter'] ])->links() }}
                </center>
                                </tfoot>


                            </table>

                            </div>
                        </div>
                    </div>

</div>

        </div>
    </div>
</div>
<!-- Modal: modalCart -->
<div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header" style="background-color: black;">
        <h4 class="modal-title" style="color: white;" id="myModalLabel">Yesser Man List</h4>
        <button type="button" style="margin-top: -20px;opacity: 5;color: white;" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <!--Body-->
      <div class="modal-body">

        <table class="table table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>Yesser Man name</th>
			  <th>Phone number</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody id="appendList">
            
          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>
<!-- Modal: modalCart -->
    <script>

        $("#orders").addClass("active");

/////////////       Filters     ///////////////////////
        $('#status_check').val('{{$data['status_check']}}');
        $('#ordering_filter').val('{{$data['ordering_filter']}}');
/////////////       Filters     ///////////////////////

        var $select = $(".categories").select2({
            placeholder: "Select atleast one category"
        });

        function export_excell()
            {
                var status_check = '{{$data['status_check']}}';
                var search = '{{$search}}';
                var daterange = '{{$daterange}}';
                var ordering_filter = document.getElementById('ordering_filter').value;

                var categories = [];
                $("select.categories").each(function(i, sel){
                    var selectedVal = $(sel).val();
                    categories.push(selectedVal);
                });
                //console.log(status_check, search, daterange, ordering_filter, categories);

                toastr.info('Info.', 'Excell is getting prepared. Please wait...');
                $('#export_excell').attr("disabled", "disabled");

                $.ajax({
                    dataType: "json",
                    url: '{{route("admin_orders_preport")}}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    data: {
                        status_check,
                        search,
                        daterange,
                        ordering_filter,
                        categories
                    },
                    success:  function(result){

                        $('#export_excell').attr("disabled", false);

                        window.location.href = result.link;
                        console.log(result);
                
                    }
                });

            }
		function ShowDrivers(id)
		{
			route = '{!! route('admin_reassign_driver') !!}?orderId='+id;
			console.log(route);
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					$('#appendList').empty();
					var HTML = "";
					for(var i =0;i<data.length;i++)
					{
						var cusername           = data[i].name;
						var DriverUserId        = data[i].user_id;
						var DriverUserDetailsId = data[i].user_detail_id;
						var Driverphonenumber   = data[i].phone_number;
						HTML += "<tr>";
						HTML += "<td>"+DriverUserId+"</td>";
						HTML += "<td>"+cusername+"</td>";
						HTML += "<td>"+Driverphonenumber+"</td>";
						HTML += "<td><a onclick='sendRequest("+DriverUserDetailsId+","+id+")' class='btn btn-primary pull-right'> Send Request </a></td>";
						HTML += "</tr>";
					}
					if(HTML == "")
					{
						HTML += "<tr>";
						HTML += "<td colspan='3'>No Yesser Man Found</td>";
						HTML += "</tr>";
					}
					$('#appendList').html(HTML);
					$('#modalCart').modal('show');
				}
			});
			
		}
		
		function sendRequest(driverId,orderId)
		{
			console.log(driverId);
			console.log(orderId);
			route = '{!! route('admin_assign_order_driver') !!}?orderId='+orderId+'&driverId='+driverId;
			console.log(route);
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					console.log(data);
					$('#modalCart').modal('hide');
					swal({
						title: "Send Request Successfully",
						type: "success",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Okay",
						closeOnConfirm: false
					}, function(){  
						window.location.reload();
					});
				}
			});
		}
		
		function ShowDriversShuffle(id)
		{
			route = '{!! route('admin_reassign_driver') !!}?orderId='+id;
			console.log(route);
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					$('#appendList').empty();
					var HTML = "";
					for(var i =0;i<data.length;i++)
					{
						var cusername           = data[i].name;
						var DriverUserId        = data[i].user_id;
						var DriverUserDetailsId = data[i].user_detail_id;
						var AccptedStatus       = data[i].accepted_status;
						var Driverphonenumber   = data[i].phone_number;
						HTML += "<tr>";
						HTML += "<td>"+DriverUserId+"</td>";
						HTML += "<td>"+cusername+"</td>";
						HTML += "<td>"+Driverphonenumber+"</td>";
						if(AccptedStatus == '1'){
							HTML += "<td><a class='btn btn-primary pull-right'> Already Accepted Request </a></td>";
						}else{
							HTML += "<td><a onclick='sendAcceptRequest("+DriverUserDetailsId+","+id+")' class='btn btn-primary pull-right'> Assign Request </a></td>";
						}
						HTML += "</tr>";
					}
					if(HTML == "")
					{
						HTML += "<tr>";
						HTML += "<td colspan='3'>No Yesser Man Found</td>";
						HTML += "</tr>";
					}
					$('#appendList').html(HTML);
					$('#modalCart').modal('show');
				}
			});
			
		}
		
		function sendAcceptRequest(driverId,orderId)
		{
			route = '{!! route('admin_shuffle_order_driver') !!}?orderId='+orderId+'&driverId='+driverId;
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					$('#modalCart').modal('hide');
					swal({
						title: "Driver Assigned Successfully",
						type: "success",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Okay",
						closeOnConfirm: false
					});
				}
			});
		}
		
        $(document).ready(function()
            {

                $('input[name="daterange"]').daterangepicker({
                    opens: 'center',
                    autoApply: true,
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    //maxDate: moment().format('DD/MM/YYYY'),
                });

            });

    </script>


@stop
