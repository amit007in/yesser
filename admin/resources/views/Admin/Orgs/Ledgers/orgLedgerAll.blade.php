    @extends('Admin.layout')

    @section('title')
        Company Ledgers
    @stop

    @section('content')

<link href="{{ URL::asset('AdminAssets/css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Company Ledgers
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_org_all')}}">All Companies</a>
                        </li>
                        <li>
                            <a href="{{route('admin_org_ledger_all', ['organisation_id' => $organisation->organisation_id])}}"><b>Company Ledgers</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-md-6 col-lg-offset-3">
            <div class="ibox-content text-center">
                <h2>{{ $organisation->name }}</h2>
                    <div class="m-b-sm">
                            <a href="{{ $organisation->image_url }}" title="{{ $organisation->name }}" class="lightBoxGallery" data-gallery="">
                                <img src="{{ $organisation->image_url }}/120/120" class="img-circle " alt="profile" >
                            </a>
                    </div>
            </div>
        </div>
    </div>
    <br>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="form-group">
                            <label class="pull-left">Ledger Type</label>
                                <select class="form-control" id="filter1">
                                    <option value="">All</option>
                                    <option value="Credit">Credit</option>
                                    <option value="Payment">Payment</option>
                                </select>                 
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <label>
                            Filter Created At
                        </label>

                        <center>
                <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
                        </center>
                    </div>

                <div class="col-lg-2 col-md-2 col-sm-2"></div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <br>
                    <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#modal-form2" id="add_ledger">
                        <i class="fa fa-edit" aria-hidden="true"> Add New Ledger</i>
                    </a>
                </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Ledger Type</th>
                                    <th>Amount</th>
                                    <th>Ledger Datetime</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                    <th>Created At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($ledgers as $ledger)
                <tr class="gradeA footable-odd">

                    <td>{{ $ledger->payment_ledger_id }}</td>

                    <td>
                        @if($ledger->type == "Payment")
                            <span class="label label-info">
                                Payment
                            </span>
                        @else
                            <span class="label label-warning">
                                Credit
                            </span>
                        @endif
                    </td>

                    <td><b>OMR</b> {{ $ledger->amount }}</td>

                    <td><i class="fa fa-clock-o"></i> {{ $ledger->ledger_dtz }} </td>

                    <td>
                        <div style="height:100px;overflow:auto;">
                            {{ $ledger->description }}
                        </div>
                    </td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
<li role="presentation">
    <a href="#" data-toggle="modal" data-target="#modal-form1" onclick="update_ledger('{{$ledger->payment_ledger_id}}', '{{$ledger->type}}', '{{$ledger->amount}}', '{{$ledger->ledger_dtzt}}', '{{$ledger->description}}')">
        <i class="fa fa-edit"> Update</i>
    </a>    
</li>

<li role="presentation">
    <a role="menuitem" tabindex="-1" class="unblock_org" id="{{$ledger->payment_ledger_id}}">
        <i class="fa fa-trash"> Delete</i>
    </a>
</li>
                            </ul>
                        </div>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $ledger->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>
</div>

        </div>
    </div>
</div>

    <!--                Update Modal                                -->
    <div class="modal inmodal modal-form1 modal-bg" id="modal-form1" role="dialog" aria-hidden="false">
        <div class="modal-dialog">
            <form method="post" action="{{route('admin_org_ledger_pupdate')}}" enctype="multipart/form-data" >
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <i class="fa fa-edit modal-icon"></i>
                        <h4 class="modal-title">Update Ledger</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Amount</label>
                                <input type="number" name="amount" id="uamount" class="form-control" step="any" min="0" autofocus="on" required value="">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Ledger DateTime</label>
                                <div class="input-group date">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="uledger_dt" id="uledger_dt" value="" placeholder="Ledger DateTime" required>
                                </div>                           
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Ledger Type</label>
                                <select name="type" id="utype" class="form-control">
                                    <option value="Credit">Credit</option>
                                    <option value="Payment">Payment</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Description</label>
                                <textarea name="description" id="udescription" class="form-control"></textarea>
                            </div>
                        </div>
                        <br>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="modal-footer">

                        <input hidden="on" value="" name="payment_ledger_id" id="payment_ledger_id"></input>

                        <button type="submit" class="btn btn-primary" id="add_rform_btn">Update Ledger</button>
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--                Update Modal                                -->

    <!--                Add Modal                                -->
    <div class="modal inmodal modal-form2 modal-bg" id="modal-form2" role="dialog" aria-hidden="false">
        <div class="modal-dialog">
            <form method="post" action="{{route('admin_org_ledger_padd')}}" enctype="multipart/form-data" >
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <i class="fa fa-plus modal-icon"></i>
                        <h4 class="modal-title">Add New Ledger</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Amount</label>
                                <input type="number" name="amount" id="amount" class="form-control" step="any" min="0" autofocus="on" required value="{{Request::old('amount')}}">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Ledger DateTime</label>
                                <div class="input-group date">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="ledger_dt" id="ledger_dt" value="{{Request::old('ledger_dt')}}" placeholder="Ledger DateTime" required>
                                </div>                           
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Ledger Type</label>
                                <select name="type" class="form-control">
                                    <option value="Credit">Credit</option>
                                    <option value="Payment">Payment</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Description</label>
                                <textarea name="description" class="form-control">{{Request::old('description')}}</textarea>
                            </div>
                        </div>
                        <br>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="modal-footer">

                        <input hidden="on" value="{{$organisation->organisation_id}}" name="organisation_id" id="organisation_id"></input>

                        <button type="submit" class="btn btn-primary" id="add_rform_btn">Add New Ledger</button>
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--                Reply Modal                                -->

<script src="{{ URL::asset('AdminAssets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

    <script>

        $("#organisations_all").addClass("active");
        $("#all_organisations").addClass("active");

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

        $('#daterange').on('apply.daterangepicker', function(ev, picker)
            {
                dt = document.getElementById('daterange').value;
                window.location.href = "{{route('admin_org_ledger_all')}}?organisation_id={{$organisation->organisation_id}}&daterange="+dt;
            });

        });

            var table = $('#example').dataTable( 
                {
                    "order": [[ 0, "DESC" ]],
                    "scrollX": true,
                    "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [ 4, 5 ] },
                                { "bSearchable": true, "aTargets": [ 4 ] }
                    ],
                   "fnDrawCallback": function( oSettings ) 
                        {

                            $('.unblock_org').click(function (e) {

                                     var id = $(e.currentTarget).attr("id");

                                    swal({
                                        title: "Are you sure you want to delete this ledger?",
                                        text: "You will not be able to access this ledger",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Delete It!",
                                        closeOnConfirm: false
                                    }, function () {
                            window.location.href = '{{route("admin_org_ledger_delete")}}?payment_ledger_id='+id;
                                    });
                                });

                        }
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });

    $('#ledger_dt').datetimepicker({
        //startDate: new Date(),
        format: 'yyyy-mm-dd hh:ii:ss'
    });
    $('#uledger_dt').datetimepicker({
        //startDate: new Date(),
        format: 'yyyy-mm-dd hh:ii:ss'
    });

    function update_ledger(payment_ledger_id, type, amount, ledger_dtzt, description)
        {

            document.getElementById('payment_ledger_id').value = payment_ledger_id;
            document.getElementById('uamount').value = amount;
            document.getElementById('uledger_dt').value = ledger_dtzt;
            document.getElementById('udescription').value = description;

            $("#utype").val(type).change();

        }

    </script>


@stop
