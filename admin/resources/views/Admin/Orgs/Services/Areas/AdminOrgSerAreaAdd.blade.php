@extends('Admin.layout')

@section('title') 
    Company Add Area
@stop

    @section('content')

    <link href="{{ URL::asset('AdminAssets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('AdminAssets/js/plugins/select2/select2.full.min.js') }}"></script>

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add Area</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_org_all')}}">All Companies</a>
                        </li>
                        <li>
                        <a href="{{route('admin_org_service_all',['organisation_id' =>$organisation->organisation_id])}}">
                            Company Services
                        </a>
                        </li>
                        <li>
                        <a href="{{ route('admin_org_ser_areas',['organisation_id' =>$organisation->organisation_id, 'category_id' =>$category_id]) }}">
                            Company Service Areas
                        </a>
                        </li>
                        <li>
                        <a href="{{route('admin_org_area_add',['organisation_id' =>$organisation->organisation_id, 'category_id' => $category_id ])}}">
                            <b>
                               Company Add Area
                            </b>
                        </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

<?php

if(!Form::old('distance'))
    $distance = 30;
else
    $distance = Form::old('distance');

if(!Form::old('address_latitude'))
    $address_latitude = 21.4735;
else
    $address_latitude = Form::old('address_latitude');

if(!Form::old('address_longitude'))
    $address_longitude = 55.9754;
else
    $address_longitude = Form::old('address_longitude');


?>

    <div class="wrapper wrapper-content animated fadeInRight">

        <br>
            <form method="post" action="{{route('admin_org_area_padd', ['organisation_id' => $organisation->organisation_id, 'category_id' => $category_id ]) }}" enctype="multipart/form-data" id="addForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="row">
        <div class="form-group">           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Label</label>
                <input type="text" placeholder="Label" autocomplete="off" class="form-control" required name="label" value="{!! Form::old('label') !!}" autofocus="on" id="label" maxlength="255">
            </div>
        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Address</label>
                <input type="text" placeholder="Address" autocomplete="off" class="form-control" required name="address" value="{!! Form::old('address') !!}" autofocus="on" id="address" maxlength="255">
            </div>

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Distance</label>

                <input type="number" placeholder="Distance" autocomplete="off" class="form-control" required name="distance" value="{!! $distance !!}" id="distance" minlength="1" onfocusout="load_map()">
            </div>

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Latitude</label>
                <input type="number" step="any" placeholder="Latitude" class="form-control" required name="address_latitude" value="{!! $address_latitude !!}" id="address_latitude" onfocusout="load_map()">
            </div>

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Longitude</label>
                <input type="number" step="any" placeholder="Longitude" class="form-control" required name="address_longitude" value="{!! $address_longitude !!}" id="address_longitude" onfocusout="load_map()">
            </div>

        </div>
    </div>
<br>

    <div class="row">
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
                <label>Days</label>
            <br>
    <label class="checkbox-inline"><input value="sunday" id="days" type="checkbox" checked name="days[]"> Sunday </label> 
    <label class="checkbox-inline"><input value="monday" id="days" type="checkbox" checked name="days[]"> Monday </label>
    <label class="checkbox-inline"><input value="tuesday" id="days" type="checkbox" checked name="days[]"> Tuesday </label>
    <label class="checkbox-inline"><input value="wednesday" id="days" type="checkbox" checked name="days[]"> Wednesday </label>
    <label class="checkbox-inline"><input value="thrusday" id="days" type="checkbox" checked name="days[]"> Thrusday </label>
    <label class="checkbox-inline"><input value="friday" id="days" type="checkbox" checked name="days[]"> Friday </label>
    <label class="checkbox-inline"><input value="saturday" id="days" type="checkbox" checked name="days[]"> Saturday </label>
            </div>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
                <label>Drivers</label>
            <br>

    <select class="form-control drivers" id="drivers" name="drivers[]" multiple="true" required="true">
        @foreach($drivers as $driver)
            <option value="{{$driver->user_detail_id}}" title="{{$driver->name}}">
                {{ $driver->name }}
            </option>
        @endforeach
    </select>

            </div>
        </div>
    </div>

<br><br><br>
    <div class="row">
        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-xs-offset-1 col-xs-10">
                <div id="map1" style="width:100%; height:350px;"></div>
            </div>
        </div>
    </div>

            <br>

                <div class="row">
                    <div class="form-group">
                        <div class='col-lg-12 col-md-12 col-sm-12'>
                            <center>
                                {!! Form::submit('Add Area', ['class' => 'btn btn-success']) !!}
                            </center>                            
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key={{env('GOOGLE_MAP_KEY')}}"></script>

<script type="text/javascript">

        $("#organisations_all").addClass("active");
        $("#all_organisations").addClass("active");

        var $select = $(".drivers").select2({
            placeholder: "Select atleast one driver"
        });

        $("#addForm").validate({
            ignore: 'input[type=hidden], .select2-input, .select2-focusser',
            rules: {
            },
            messages: {
                address: {
                    required: "Please provide the address"
                },
                label: {
                    required: "Please provide the labe for this area"
                }
            }
        });

        var myMap1;
        var cityCircle1;
        var marker1;
        var mapOptions;
        var subjectMarker1;

        var roptions = {
                componentRestrictions: {country: ['om','in']}
            };

    function load_map()
        {

            address_latitude = document.getElementById('address_latitude').value;
            address_longitude = document.getElementById('address_longitude').value;
            distance = document.getElementById('distance').value;

            mapOptions = {
                    zoom: 7,
                    center:new google.maps.LatLng(address_latitude,address_longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false,
                    fullscreenControl: false,
                };

            myMap1 = new google.maps.Map(document.getElementById('map1'), mapOptions);

            if(address_latitude != '' && address_longitude != '' && distance != '')
                {

                    myMap1.setCenter(new google.maps.LatLng(Number(address_latitude), Number(address_longitude)));

                    cityCircle1 = new google.maps.Circle({
                        strokeColor: '#71B371',
                        strokeOpacity: 0.5,
                        strokeWeight: 1.5,
                        fillColor: '#71B371',
                        fillOpacity: 0.4,
                        map: myMap1,
                        center: {"lat":Number(address_latitude),"lng":Number(address_longitude)},
                        radius:  Number(distance)*1000
                    });


                    subjectMarker1 = new google.maps.Marker({
                        position: {"lat":Number(address_latitude),"lng":Number(address_longitude)},
                        title: 'Address',
                        map: myMap1,
                        label:'A'
                    });

                }

        }


        var address = (document.getElementById('address'));
        var autocomplete = new google.maps.places.Autocomplete(address, roptions);
        
        autocomplete.setTypes(['geocode']);
        google.maps.event.addListener(autocomplete, 'place_changed', function () 
            {
                var place = autocomplete.getPlace();
                if (!place.geometry) {  return;  }
                var address = '';
                if (place.address_components) {
                        address = [
                            (place.address_components[0] && place.address_components[0].short_name || ''),
                            (place.address_components[1] && place.address_components[1].short_name || ''),
                            (place.address_components[2] && place.address_components[2].short_name || '')
                        ].join(' ');

                    document.getElementById("address_latitude").value = place.geometry.location.lat();
                    document.getElementById("address_longitude").value = place.geometry.location.lng();
                    console.log(place.geometry.location.lat(), place.geometry.location.lng());

                    load_map();
                                    
                }
                                
            });

        load_map();

</script>


@stop



