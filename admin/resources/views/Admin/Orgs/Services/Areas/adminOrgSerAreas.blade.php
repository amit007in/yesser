@extends('Admin.layout')

@section('title')
    Company Areas
@stop

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Company Areas
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_org_all')}}">All Companies</a>
                        </li>
                        <li>
                        <a href="{{route('admin_org_service_all',['organisation_id' =>$organisation->organisation_id])}}">
                            Company Services
                        </a>
                        </li>
                        <li>
                        <a href="{{route('admin_org_ser_areas',['organisation_id' =>$organisation->organisation_id, 'category_id' => $category_id ])}}">
                            <b>
                                Company Areas
                            </b>
                        </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-md-6 col-lg-offset-3">
            <div class="ibox-content text-center">
                <h2>{{ $organisation->name }}</h2>
                    <div class="m-b-sm">
                            @if($organisation->image == "")
                            <a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
                            </a>
                        @elseif($organisation->created_by == "Admin")
                             <a href="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}"  class="img-circle">
                            </a>
                        @else
                             <a href="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}"  class="img-circle">
                            </a>
                        @endif
                    </div>
            </div>
        </div>
    </div>
<br>

    <div class="row">

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="form-group">
                <label class="pull-left">Status</label>
                    <select class="form-control" id="filter1">
                        <option value="e">All</option>
                        <option value="Active">Active</option>
                        <option value="Blocked">Blocked</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="form-group">
                <label class="pull-left">Created By</label>
                    <select class="form-control" id="filter2">
                        <option value="a">All</option>
                        <option value="Admin">Admin</option>
                        <option value="Company">Company</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <label>
                Filter Updated At
            </label>

            <center>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
            </center>                        
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="form-group">
                <label class="pull-left">Sunday</label>
                    <select class="form-control" id="filter4">
                        <option value="e">All</option>
                        <option value="Active">Active</option>
                        <option value="Blocked">Blocked</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="form-group">
                <label class="pull-left">Monday</label>
                    <select class="form-control" id="filter5">
                        <option value="e">All</option>
                        <option value="Active">Active</option>
                        <option value="Blocked">Blocked</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="form-group">
                <label class="pull-left">Tuesday</label>
                    <select class="form-control" id="filter6">
                        <option value="e">All</option>
                        <option value="Active">Active</option>
                        <option value="Blocked">Blocked</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="form-group">
                <label class="pull-left">Wednesday</label>
                    <select class="form-control" id="filter7">
                        <option value="e">All</option>
                        <option value="Active">Active</option>
                        <option value="Blocked">Blocked</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="form-group">
                <label class="pull-left">Thrusday</label>
                    <select class="form-control" id="filter8">
                        <option value="e">All</option>
                        <option value="Active">Active</option>
                        <option value="Blocked">Blocked</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="form-group">
                <label class="pull-left">Friday</label>
                    <select class="form-control" id="filter9">
                        <option value="e">All</option>
                        <option value="Active">Active</option>
                        <option value="Blocked">Blocked</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="form-group">
                <label class="pull-left">Saturday</label>
                    <select class="form-control" id="filter10">
                        <option value="e">All</option>
                        <option value="Active">Active</option>
                        <option value="Blocked">Blocked</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">

    <br>
        <a href="{{ route('admin_org_area_add', ['organisation_id'=>$organisation->organisation_id, 'category_id' => $category_id ]) }}" class="btn btn-primary pull-right" id="Brand Add" title="Add">
            <i class="fa fa-plus"></i> Add Area
        </a>
    
        </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Created By</th>
                                    <th>Label</th>
                                    <th>Address (Dist)</th>
                                    <th>Sunday</th>
                                    <th>Monday</th>
                                    <th>Tuesday</th>
                                    <th>Wednesday</th>
                                    <th>Thursday</th>
                                    <th>Friday</th>
                                    <th>Saturday</th>
                                    <th>Actions</th>
                                    <th>Created At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($areas as $area)
                <tr class="gradeA footable-odd" style="display: table-row;">

                    <td>{{ $area->organisation_area_id }}</td>

                    <td>
                        <center>
                            @if($area->blocked == '0')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye" aria-hidden="true"> Blocked</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($area->created_by == 'Admin')
                                <span class="label label-primary">
                                    <i class="fa fa-at" aria-hidden="true"></i> Admin
                                </span>
                            @else
                                <span class="label label-info">
                                    <i class="fa fa-building-o" aria-hidden="true"></i> Company
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        {{ $area->label }}
                    </td>

                    <td>
                        {{ $area->address }} - ({{ $area->distance }})
                    </td>

                    <td>
                        <center>
                            @if($area->sunday_service == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye" aria-hidden="true"> Blocked</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($area->monday_service == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye" aria-hidden="true"> Blocked</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($area->tuesday_service == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye" aria-hidden="true"> Blocked</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($area->wednesday_service == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye" aria-hidden="true"> Blocked</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($area->thursday_service == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye" aria-hidden="true"> Blocked</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($area->friday_service == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye" aria-hidden="true"> Blocked</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($area->saturday_service == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye" aria-hidden="true"> Blocked</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

            <li role="presentation">
                <a role="menuitem" tabindex="-1" href="{{route('admin_org_area_update',['organisation_area_id' => $area->organisation_area_id])}}">
                    <i class="fa fa-edit"> Update</i>
                </a>
            </li>

            <li role="presentation">
                @if($area->blocked == '0' || $area->blocked == '2')
                    <a role="menuitem" tabindex="-1" class="block_org" id="{{$area->organisation_area_id}}">
                        <i class="fa fa-eye-slash"> Block</i>
                    </a>
                @else
                    <a role="menuitem" tabindex="-1" class="unblock_org" id="{{$area->organisation_area_id}}">
                        <i class="fa fa-eye"> Unblock</i>
                    </a>
                @endif
            </li>

                            </ul>
                        </div>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $area->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

        $("#organisations_all").addClass("active");
        $("#all_organisations").addClass("active");

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

        $('#daterange').on('apply.daterangepicker', function(ev, picker)
            {
                dt = document.getElementById('daterange').value;

                window.location.href = "{{route('admin_org_ser_areas')}}?organisation_id={{$organisation->organisation_id}}&category_id={{$category_id}}&daterange="+dt;
            });

        });

            var table = $('#example').dataTable( 
                {
                   "order": [[ 0, "desc" ]],
                   //"scrollX": true,
                   //"scrollY": 500,
                   "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 4, 7 ] },
                        { "bSearchable": true, "aTargets": [ 3, 5, 6 ] }
                    ],
                   "fnDrawCallback": function( oSettings ) 
                        {

                            $('.block_org').click(function (e) {

                                  var id = $(e.currentTarget).attr("id");

                                    swal({
                                        title: "Are you sure you want to block service to this area?",
                                        text: "User will not be able to order at this area.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Block it!",
                                        closeOnConfirm: false
                                    }, function () {
                                        window.location.href = '{{route("admin_org_ser_area_block")}}?block=1&organisation_area_id='+id;
                                    });
                                });

                            $('.unblock_org').click(function (e) {

                                     var id = $(e.currentTarget).attr("id");

                                    swal({
                                        title: "Are you sure you want to unblock service to this area?",
                                        text: "User will be able to order at this area.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Unblock It!",
                                        closeOnConfirm: false
                                    }, function () {
                                window.location.href = '{{route("admin_org_ser_area_block")}}?block=0&organisation_area_id='+id;
                                    });
                                });

                        }
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });

            $("#filter2").on('change', function()
                {
                    table.fnFilter($(this).val(), 2);
                });

            $("#filter4").on('change', function()
                {
                    table.fnFilter($(this).val(), 4);
                });

            $("#filter5").on('change', function()
                {
                    table.fnFilter($(this).val(), 5);
                });

            $("#filter7").on('change', function()
                {
                    table.fnFilter($(this).val(), 6);
                });

            $("#filter8").on('change', function()
                {
                    table.fnFilter($(this).val(), 8);
                });

            $("#filter9").on('change', function()
                {
                    table.fnFilter($(this).val(), 9);
                });

            $("#filter10").on('change', function()
                {
                    table.fnFilter($(this).val(), 10);
                });

    </script>


@stop
