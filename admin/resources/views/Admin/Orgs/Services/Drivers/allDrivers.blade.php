    @extends('Admin.layout')

    @section('title')
        Company Service Yesser Man
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Company Service Yesser Man
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_org_all')}}">All Companies</a>
                        </li>
                        <li>
                        <a href="{{route('admin_org_service_all',['organisation_id' =>$organisation->organisation_id])}}">
                            Company Services
                        </a>
                        </li>
                        <li>
                        <a href="{{route('admin_org_service_dall',['organisation_id' =>$organisation->organisation_id, 'category_id' => $category_id ])}}">
                            <b>
                                Company Service Yesser Man
                            </b>
                        </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        <div class="col-md-6 col-lg-offset-3">
            <div class="ibox-content text-center">
                <h2>{{ $organisation->name }}</h2>
                    <div class="m-b-sm">
                            @if($organisation->image == "")
                            <a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
                            </a>
                        @elseif($organisation->created_by == "Admin")
                             <a href="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}"  class="img-circle">
                            </a>
                        @else
                             <a href="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}"  class="img-circle">
                            </a>
                        @endif
                    </div>
            </div>
        </div>       
        
    </div>

<br>

    <div class="row">

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="form-group">
                <label class="pull-left">Status</label>
                    <select class="form-control" id="filter1">
                        <option value="e">All</option>
                        <option value="Active">Active</option>
                        <option value="Blocked">Blocked</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="form-group">
                <label class="pull-left">Approved</label>
                    <select class="form-control" id="filter2">
                        <option value="">All</option>
                        <option value="Approved">Approved</option>
                        <option value="Pending">Pending</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <label>
                Filter Updated At
            </label>

            <center>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
            </center>                        
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="form-group">
                <label class="pull-left">Created By</label>
                    <select class="form-control" id="filter3">
                        <option value="a">All</option>
                        <option value="Admin">Admin</option>
                        <option value="Company">Company</option>
                        <option value="App">App</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">

            <br>
            <a href="{{ route('admin_org_service_dadd', ['organisation_id' => $organisation->organisation_id, 'category_id' => $category_id ] ) }}" class="btn btn-primary pull-right" id="Brand Add" title="Add">
                <i class="fa fa-plus"></i> Add Yesser Man
            </a>
    
        </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Approved</th>
                                    <th>Created By</th>
                                    <th>Name</th>
                                    <th>ProfilePic</th>
                                    <th>Actions</th>
                                    <th>Registered At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($drivers as $driver)
                <tr class="gradeA footable-odd">

                    <td>{{ $driver->user_detail_id }}</td>

                    <td>
                        <center>
                            @if($driver->blocked == '0')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye" aria-hidden="true"> Blocked</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($driver->approved == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-thumbs-up" aria-hidden="true"> Approved</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-thumbs-down" aria-hidden="true"> Pending</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($driver->created_by == 'Admin')
                                <span class="label label-primary">
                                    <i class="fa fa-at" aria-hidden="true"></i> Admin
                                </span>
                            @elseif($driver->created_by == 'Org')
                                <span class="label label-info">
                                    <i class="fa fa-building-o" aria-hidden="true"></i> Company
                                </span>
                            @else
                                <span class="label label-success">
                                    <i class="fa fa-mobile" aria-hidden="true"></i> App
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        {{ $driver->name }} ({{ $driver->phone_number }})
                    </td>

                    <td>
    <a href="{{$driver->profile_pic_url}}" title="{{ $driver->name }}" class="lightBoxGallery" data-gallery="">
        <img src="{{$driver->profile_pic_url}}/120/120"  class="img-circle">
    </a>                        
                    </td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

            @if($driver->category_id != 0)
                <li role="presentation">
                    <a role="menuitem" tabindex="-1" href="{{ route('admin_org_service_dupdate', ['user_detail_id' => $driver->user_detail_id] ) }}">
                        <i class="fa fa-edit"> Update Profile</i>
                    </a>
                </li>
            @endif

            <li role="presentation">
                @if($driver->blocked == '0')
                    <a role="menuitem" tabindex="-1" class="block_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Block',{{$driver->user_detail_id}})">
                        <i class="fa fa-eye-slash"> Block</i>
                    </a>
                @else
                    <a role="menuitem" tabindex="-1" class="unblock_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Unblock',{{$driver->user_detail_id}})">
                        <i class="fa fa-eye"> Unblock</i>
                    </a>
                @endif
            </li>

            <li role="presentation">
                @if($driver->approved == '1')
                    <a role="menuitem" tabindex="-1" class=unapprove_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Unapprove',{{$driver->user_detail_id}})">
                        <i class="fa fa-thumbs-down"> Unapprove</i>
                    </a>
                @else
                    <a role="menuitem" tabindex="-1" class="approve_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Approve',{{$driver->user_detail_id}})">
                        <i class="fa fa-thumbs-up"> Approve</i>
                    </a>
                @endif
            </li>

                            </ul>
                        </div>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $driver->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

        $("#organisations_all").addClass("active");
        $("#all_organisations").addClass("active");

        function show_popup(type, id)
            {

                if(type == 'Block')
                    {
                        var title = "Are you sure you want to block this Yesser Man?";
                        var sub_title = "User will not be view this Yesser Man.";
                        var route = '{{route("admin_driver_block")}}?block=1&user_detail_id='+id;
                    }
                else if(type == 'Unblock')
                    {
                        var title = "Are you sure you want to unblock this Yesser Man?";
                        var sub_title = "User will be able to view this Yesser Man.";
                        var route = '{{route("admin_driver_block")}}?block=0&user_detail_id='+id;
                    }
                else if(type == 'Approve')
                    {
                        var title = "Are you sure you want to approve this Yesser Man?";
                        var sub_title = "Documents will become verified";
                        var route = '{{route("admin_driver_approve")}}?approve=1&user_detail_id='+id;
                    }
                else if(type == 'Unapprove')
                    {
                        var title = "Are you sure you want to unapprove this Yesser Man?";
                        var sub_title = "Documents will become unverified";
                        var route = '{{route("admin_driver_approve")}}?approve=0&user_detail_id='+id;
                    }

//                console.log(title, sub_title, route);
                
                swal({
                        title: title,
                        text: sub_title,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Do it!",
                        closeOnConfirm: false
                    }, function () {
                        window.location.href = route;
                    });
            }

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

        $('#daterange').on('apply.daterangepicker', function(ev, picker)
            {
                dt = document.getElementById('daterange').value;

                window.location.href = "{{route('admin_org_service_dall')}}?organisation_id={{$organisation->organisation_id}}&category_id={{$category_id}}&daterange="+dt;
            });

        });

        var table = $('#example').dataTable( 
            {
//                "scrollY": 500,
                "order": [[ 0, "desc" ]],
                "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 5, 6 ] },
                        { "bSearchable": true, "aTargets": [ 4 ] }
                    ]
            });

        $("#filter1").on('change', function()
            {
                table.fnFilter($(this).val(), 1);
            });

        $("#filter2").on('change', function()
            {
                table.fnFilter($(this).val(), 2);
            });

        $("#filter3").on('change', function()
            {
                table.fnFilter($(this).val(), 3);
            });

    </script>


@stop
