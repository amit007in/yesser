    @extends('Admin.layout')

    @section('title')
        Company Service Yesser Man
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Company Service Yesser Man
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_org_all')}}">All Companies</a>
                        </li>
                        <li>
                        <a href="{{route('admin_org_service_all',['organisation_id' =>$organisation->organisation_id])}}">
                            Company Services
                        </a>
                        </li>
                        <li>
                        <a href="{{route('admin_org_service_dall',['organisation_id' =>$organisation->organisation_id, 'category_id' => $category_id ])}}">
                            <b>
                                Company Service Yesser Man
                            </b>
                        </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                <div class="row">
                    <div class="col-md-6 col-lg-offset-3">
                        <div class="ibox-content text-center">
                            <h2>{{ $organisation->name }}</h2>
                                <div class="m-b-sm">
                                        @if($organisation->image == "")
                            <a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
                            </a>
                        @elseif($organisation->created_by == "Admin")
                             <a href="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}"  class="img-circle">
                            </a>
                        @else
                             <a href="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}"  class="img-circle">
                            </a>
                        @endif
                                </div>
                    <center>
                        <h3>
                            Service - {{$category['category_details'][0]->name}}
                        </h3>
                    </center>
                        </div>
                    </div>                    
                </div>
<br>

    <div class="row">

    <?php

        $blocked_check = isset($data['blocked_check']) ? $data['blocked_check'] : 'All';
        $approved_check = isset($data['approved_check']) ? $data['approved_check'] : 'All';
        $created_by_check = isset($data['created_by_check']) ? $data['created_by_check'] : 'All';
        $online_check = isset($data['online_check']) ? $data['online_check'] : 'All';
        $search = isset($data['search']) ? $data['search'] : '';

        $daterange =  $fstarting_dt.' - '.$fending_dt;

    ?>

        <div class="col-lg-2 col-md-2 col-sm-2">
            <label>Add Yesser Man</label><br>
            <a href="{{ route('admin_org_service_dadd', ['organisation_id' => $organisation->organisation_id, 'category_id' => $category_id ] ) }}" class="btn btn-success" title="Add Yesser Man">
                <i class="fa fa-plus"></i> Add Yesser Man
            </a>    
        </div>

        <form method="post" action="{{route('admin_org_service_dall',['organisation_id' =>$organisation->organisation_id, 'category_id' => $category_id ])}}">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="form-group">
                    <label class="pull-left">Status</label>
                        <select class="form-control" id="blocked_check" name="blocked_check">
                            <option value="All">All</option>
                            <option value="Active">Active</option>
                            <option value="Blocked">Blocked</option>
                        </select>                 
                </div>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form-group">
                    <label class="pull-left">Approved</label>
                        <select class="form-control" id="approved_check" name="approved_check">
                            <option value="All">All</option>
                            <option value="Approved">Approved</option>
                            <option value="Pending">Pending</option>
                        </select>                 
                </div>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form-group">
                    <label class="pull-left">Created By</label>
                        <select class="form-control" id="created_by_check" name="created_by_check">
                            <option value="All">All</option>
                            <option value="Admin">Admin</option>
                            <option value="Company">Company</option>
                            <option value="App">App</option>
                        </select>                 
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="form-group">
                    <label class="pull-left">Online</label>
                        <select class="form-control" id="online_check" name="online_check">
                            <option value="All">All</option>
                            <option value="Online">Online</option>
                            <option value="Offline">Offline</option>
                        </select>                 
                </div>
            </div>

            <div class="col-lg-5 col-md-5 col-sm-5">
                <label>Filter Registered At</label>
                <center>
                    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$daterange}}"/>
                </center>
            </div>

            <div class="col-lg-5 col-md-5 col-sm-5">
                <label>Search</label>
                <input type="text" name="search" class="form-control" placeholder="Search" value="{{$search}}">
            </div>

            <div class="col-lg-2 col-md-2 col-sm-2">
                <label>Filter</label><br>
                <button type="submit" class="btn btn-primary btn-big">Filter Results</button>
            </div>

        </form>

    </div>
    <br>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Approved</th>
                                    <th>Created By</th>
                                    <th>Online</th>
                                    <th>Name</th>
                                    <th>ProfilePic</th>
                                    <th>Actions</th>
                                    <th>Orders</th>
                                  <!--   <th>Reviews</th>
                                    <th>Products</th> -->
                                    <th>Registered At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($drivers as $driver)
                <tr class="gradeA footable-odd">

                    <td>{{ $driver->user_detail_id }}</td>

                    <td>
                        <center>
                            @if($driver->blocked == '0')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye" aria-hidden="true"> Blocked</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($driver->approved == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-thumbs-up" aria-hidden="true"> Approved</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-thumbs-down" aria-hidden="true"> Pending</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($driver->created_by == 'Admin')
                                <span class="label label-primary">
                                    <i class="fa fa-at" aria-hidden="true"></i> Admin
                                </span>
                            @elseif($driver->created_by == 'Org')
                                <span class="label label-info">
                                    <i class="fa fa-building-o" aria-hidden="true"></i> Company
                                </span>
                            @else
                                <span class="label label-success">
                                    <i class="fa fa-mobile" aria-hidden="true"></i> App
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($driver->online_status == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-toggle-on" aria-hidden="true"> Online</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-toggle-off" aria-hidden="true"> Offline</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        {{ $driver->name }} ({{ $driver->phone_number }})
                        <hr>
                        <b>OTP -</b> {{ $driver->otp }}
                    </td>

                    <td>
   <!--  <a href="{{$driver->profile_pic_url}}" title="{{ $driver->name }}" class="lightBoxGallery" data-gallery="">
        <img src="{{$driver->profile_pic_url}}/120/120"  class="img-circle">
    </a> -->
    @if($driver->profile_pic != "")
                        @if($driver->created_by == 'Admin')
                             <a href="{{ URL::asset('BuraqExpress/Uploads/'.$driver->profile_pic) }}" title="{{ $driver->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('BuraqExpress/Uploads/'.$driver->profile_pic) }}"  class="img-circle">
                            </a>
                        @elseif($driver->created_by == 'Org')
                             <a href="{{ URL::asset('BuraqExpress/Uploads/'.$driver->profile_pic) }}" title="{{ $driver->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('BuraqExpress/Uploads/'.$driver->profile_pic) }}"  class="img-circle">
                            </a>
                        @else
                            <a href="{{ env('SOCKET_URL').'/Uploads/'.$driver->profile_pic }}" title="{{ $driver->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ env('SOCKET_URL').'/Uploads/'.$driver->profile_pic }}"  class="img-circle">
                            </a>
                        @endif
                    @else
                        <a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $driver->name }}" class="lightBoxGallery img-circle" data-gallery="">
                            <img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
                        </a>
                    @endif
                    </td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

            <li role="presentation">
                <a role="menuitem" tabindex="-1" href="{{ route('admin_driver_track', ['user_detail_id' => $driver->user_detail_id] ) }}">
                    <i class="fa fa-map-marker"></i>  Track Driver
                </a>
            </li>

            <li role="presentation">
                <a role="menuitem" tabindex="-1" href="{{ route('admin_ser_dupdate', ['user_detail_id' => $driver->user_detail_id] ) }}">
                    <i class="fa fa-edit"></i> Update Profile
                </a>
            </li>

            <li role="presentation">
                <a role="menuitem" tabindex="-1" id="admin_cust_orders" href="{{route('admin_ser_dorders',['driver_user_detail_id' => $driver->user_detail_id])}}">
                    <i class="fa fa-circle-thin" aria-hidden="true"></i> All Orders - {{ $driver->order_counts }}
                </a>
            </li>

            <li role="presentation">
                <a role="menuitem" tabindex="-1" id="admin_customer_reviews" href="{{route('admin_ser_dreviews',['driver_user_detail_id' => $driver->user_detail_id])}}">
                    <i class="fa fa-user-circle" aria-hidden="true"></i> All Reviews - {{ $driver->review_counts }}
                </a>
            </li>

            <li role="presentation">
                <a role="menuitem" tabindex="-1" id="admin_customer_reviews" href="{{route('admin_ser_dproducts',['driver_user_detail_id' => $driver->user_detail_id])}}">
                    <i class="fa fa-product-hunt" aria-hidden="true"></i> All Products - {{ $driver->product_counts }}
                </a>
            </li>

            <li role="presentation">
                @if($driver->blocked == '0')
                    <a role="menuitem" tabindex="-1" class="block_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Block','{{$driver->user_detail_id}}')">
                        <i class="fa fa-eye-slash"></i> Block
                    </a>
                @else
                    <a role="menuitem" tabindex="-1" class="unblock_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Unblock','{{$driver->user_detail_id}}')">
                        <i class="fa fa-eye"></i> Unblock
                    </a>
                @endif
            </li>

            <li role="presentation">
                @if($driver->approved == '1')
                    <a role="menuitem" tabindex="-1" class=unapprove_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Unapprove','{{$driver->user_detail_id}}')">
                        <i class="fa fa-thumbs-down"></i> Unapprove
                    </a>
                @else
                    <a role="menuitem" tabindex="-1" class="approve_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Approve','{{$driver->user_detail_id}}')">
                        <i class="fa fa-thumbs-up"></i> Approve
                    </a>
                @endif
            </li>

            <li role="presentation">
                <a role='menuitem' tabindex="-1" onclick="delete_popup('{{$driver->user_detail_id}}', '{{$driver->user_id}}')">
                    <i class="fa fa-bitbucket" aria-hidden="true"> Delete </i>
                </a>
            </li>

                            </ul>
                        </div>
                    </td>

                    <td>
                        <a href="{{ route('admin_ser_dorders',['driver_user_detail_id' => $driver->user_detail_id]) }}" class="btn btn-default">
                            {{ $driver->order_counts }}
                        </a>
                    </td>

                   <!--  <td>
                        <a href="{{ route('admin_ser_dreviews',['driver_user_detail_id' => $driver->user_detail_id]) }}" class="btn btn-default">
                            {{ $driver->review_counts }}
                        </a>
                    </td>

                    <td>
                        <a href="{{ route('admin_ser_dproducts',['driver_user_detail_id' => $driver->user_detail_id]) }}" class="btn btn-default">
                            {{ $driver->product_counts }}
                        </a>
                    </td> -->

                    <td><i class="fa fa-clock-o"></i> {{ $driver->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                <center>
                    {{ $drivers->appends(['organisation_id' => $organisation->organisation_id, 'category_id' => $category->category_id, 'blocked_check' => $blocked_check, 'approved_check' => $approved_check, 'created_by_check' => $created_by_check, 'online_check'=>$online_check, 'search'=>$search, 'daterange' => $daterange])->links() }}
                </center>


                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>

</div>

    <script>

        $("#organisations_all").addClass("active");
        $("#all_organisations").addClass("active");

/////////////       Filters     ///////////////////////
        $('#blocked_check').val('{{$blocked_check}}');
        $('#approved_check').val('{{$approved_check}}');
        $('#created_by_check').val('{{$created_by_check}}');
        $('#online_check').val('{{$online_check}}');
/////////////       Filters     ///////////////////////

        function delete_popup(driver_user_detail_id, driver_user_id)
            {

                swal({
                    title: "Are you sure you want to delete this Yesser Man?",
                    text: "All data will be completely deleted.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Delete it!",
                    closeOnConfirm: false
                    }, function () {
                        window.location.href = '{{route("admin_delete")}}?driver_user_detail_id='+driver_user_detail_id+'&driver_user_id='+driver_user_id;
                    });

            }

        function show_popup(type, id)
            {

                if(type == 'Block')
                    {
                        var title = "Are you sure you want to block this Yesser Man?";
                        var sub_title = "User will not be view this Yesser Man.";
                        var route = '{{route("admin_driver_block")}}?block=1&user_detail_id='+id;
                    }
                else if(type == 'Unblock')
                    {
                        var title = "Are you sure you want to unblock this Yesser Man?";
                        var sub_title = "User will be able to view this Yesser Man.";
                        var route = '{{route("admin_driver_block")}}?block=0&user_detail_id='+id;
                    }
                else if(type == 'Approve')
                    {
                        var title = "Are you sure you want to approve this Yesser Man?";
                        var sub_title = "Documents will become verified";
                        var route = '{{route("admin_driver_approve")}}?approve=1&user_detail_id='+id;
                    }
                else if(type == 'Unapprove')
                    {
                        var title = "Are you sure you want to unapprove this Yesser Man?";
                        var sub_title = "Documents will become unverified";
                        var route = '{{route("admin_driver_approve")}}?approve=0&user_detail_id='+id;
                    }
                
                swal({
                        title: title,
                        text: sub_title,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Do it!",
                        closeOnConfirm: false
                    }, function () {
                        window.location.href = route;
                    });
            }

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                opens: 'center',
                autoApply: true,
                format: 'DD/MM/YYYY',
                minDate: '1/2/2018',
                maxDate: moment().format('DD/MM/YYYY')
              }, function(start, end, label) {
                //console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
              });

        });



    </script>


@stop
