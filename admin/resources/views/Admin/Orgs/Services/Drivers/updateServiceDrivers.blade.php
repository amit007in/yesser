@extends('Admin.layout')

@section('title') 
    Update Yesser Man
@stop

    @section('content')

    <link href="{{ URL::asset('AdminAssets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('AdminAssets/css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Update Yesser Man</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_org_all')}}">All Companies</a>
                        </li>
                        <li>
                        <a href="{{route('admin_org_service_all',['organisation_id' =>$driver->organisation_id])}}">
                            Company Services
                        </a>
                        </li>
                        <li>
                        <a href="{{route('admin_org_service_dall',['organisation_id' =>$driver->organisation_id, 'category_id' => $driver->category_id])}}">
                            Company Service Yesser Man
                        </a>
                        </li>
                        <li>
                        <a href="{{route('admin_org_service_dupdate',['user_detail_id' =>$driver->user_detail_id ])}}">
                            <b>
                                Update Yesser Man
                            </b>
                        </a>                            
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                @if($driver->organisation)
                   <div class="row">
                        <div class="col-md-6 col-lg-offset-3">
                            <div class="ibox-content text-center">
                                <h2>{{ $driver->organisation['name'] }}</h2>
                                    <div class="m-b-sm">
                                           @if($organisation->image == "")
                            <a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
                            </a>
                        @elseif($organisation->created_by == "Admin")
                             <a href="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}"  class="img-circle">
                            </a>
                        @else
                             <a href="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}"  class="img-circle">
                            </a>
                        @endif
                                            <center>
                                                <h2>Update Driver</h2>
                                            </center>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <br>
                @endif

        <br>
            <form method="post" action="{{route('admin_org_service_dupdate_post', ['user_detail_id' =>$driver->user_detail_id, 'user_id' => $driver->user_id ]) }}" enctype="multipart/form-data" id="addForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Name</label>
                <input type="text" placeholder="Name" autocomplete="off" class="form-control" required name="name" value="{!! $driver->name !!}" autofocus="on" id="name" maxlength="255">
            </div> 

            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <center>
                    <a href="{!! $driver->profile_pic_url !!}" title="{!! $driver->name !!}" class="lightBoxGallery pull-right" data-gallery="">
                        <img src="{!! $driver->profile_pic_url !!}"  class="img-circle">
                    </a>
                </center>
            </div>
             <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>Phone number</label>
                <div class="input-group m-b">
                    <div class="input-group-btn">
                        <select data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button" aria-expanded="false" name="phone_code" id="phone_code" value="{{$driver->phone_code}}">
                            <option value="+968">Oman +968</option>
                            <!-- <option value="+971">UAE +971</option>
                            <option value="+974">Oatar +974</option>
                            <option value="+92">Pakistan +92</option>
                            <option value="+973">Bahrain +973</option>
                            <option value="+966">Saudi Arabia +966</option>
                            <option value="+965">Kuwait +965</option>
                            <option value="+91">India +91</option> -->
                        </select>
                    </div>
                <input type="number" minlength="5" placeholder="Phone number" autocomplete="off" class="form-control" required name="phone_number" value="{!! $driver->phone_number !!}" id="phone_number" minlength="6" maxlength="15">
                </div>
            </div>

          <!--   <div class='col-lg-4 col-md-4 col-sm-4'>
                <label>Profile Pic (150px X 150px)</label>
                <input type="file" name="profile_pic" class="form-control" accept="image/*" id="profile_pic">
            </div>  -->

        </div>
    </div>
<br>
    <!-- <div class="row">
        <div class="form-group">

            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>Phone number</label>
                <div class="input-group m-b">
                    <div class="input-group-btn">
                        <select data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button" aria-expanded="false" name="phone_code" id="phone_code" value="{{$driver->phone_code}}">
                            <option value="+968">Oman +968</option>
                            <option value="+971">UAE +971</option>
                            <option value="+974">Oatar +974</option>
                            <option value="+92">Pakistan +92</option>
                            <option value="+973">Bahrain +973</option>
                            <option value="+966">Saudi Arabia +966</option>
                            <option value="+965">Kuwait +965</option>
                            <option value="+91">India +91</option>
                        </select>
                    </div>
                <input type="number" minlength="5" placeholder="Phone number" autocomplete="off" class="form-control" required name="phone_number" value="{!! $driver->phone_number !!}" id="phone_number" minlength="6" maxlength="15">
                </div>
            </div>

        </div>
    </div><br>
 -->

    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Mulkiya Number</label>
                <input type="text" placeholder="Mulkiya Number" autocomplete="off" class="form-control" required name="mulkiya_number" value="{!! $driver->mulkiya_number !!}" id="mulkiya_number" maxlength="255">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
            <label>Mulkiya Validity date</label>
    <div class="input-group date">
    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="mulkiya_validity" id="mulkiya_validity" value="{{ $driver->mulkiya_validity }}" placeholder="Mulkiya Validity date" required>
    </div>
            </div> 


        </div>
    </div>

<br>
<!--     <div class="row">
        <div class="form-group">

            @if($driver->mulkiya_front != '')

                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                    <center>
                        <a href="{!! $driver->mulkiya_front_url !!}" title="{!! $driver->name !!} - Mulkiya Front" class="lightBoxGallery" data-gallery="">
                            <img src="{!! $driver->mulkiya_front_url !!}"  class="img-circle">
                        </a>
                    </center>
                </div>

                <div class='col-lg-4 col-md-4 col-sm-4'>
                    <label>Mulkiya Front</label>
                    <input type="file" name="mulkiya_front" class="form-control" accept="image/*" id="mulkiya_front">
                </div>

            @else

                <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                    <label>Mulkiya Front</label>
                    <input type="file" name="mulkiya_front" class="form-control" accept="image/*" id="mulkiya_front">
                </div>

            @endif


            @if($driver->mulkiya_back != '')

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                    <center>
                        <a href="{!! $driver->mulkiya_back_url !!}" title="{!! $driver->name !!} - Mulkiya Back" class="lightBoxGallery pull-right" data-gallery="">
                            <img src="{!! $driver->mulkiya_back_url !!}"  class="img-circle">
                        </a>
                    </center>
                </div>

                <div class='col-lg-4 col-md-4 col-sm-4'>
                    <label>Mulkiya Back</label>
                    <input type="file" name="mulkiya_back" class="form-control" accept="image/*" id="mulkiya_back">
                </div>

            @else

                <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                    <label>Mulkiya Back</label>
                    <input type="file" name="mulkiya_back" class="form-control" accept="image/*" id="mulkiya_back">
                </div>

            @endif

        </div>
    </div>

<br> -->
    <div class="row">
        <div class="form-group">

            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Brand</label>
                <select name="category_brand_id" id="category_brand_id" class="form-control border-info" onchange="load_products(this.value)">
                    @foreach($brands as $brand)
                        @if($driver->category_brand_id == $brand->category_brand_id)
                            <option value="{{$brand->category_brand_id}}" selected="true">{{$brand->name}}</option>
                        @else
                            <option value="{{$brand->category_brand_id}}">{{$brand->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div> 

             <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Products</label>
                    <select class="productids form-control border-info" id="productids" multiple="true" required="true" name="productids[]">
                    </select>
            </div>           

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Address</label>
                <input type="text" placeholder="Address" autocomplete="off" class="form-control" required name="address" value="{!! $driver->address !!}" autofocus="on" id="address" maxlength="255">
            </div>

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Latitude</label>
                <input type="number" step="any" placeholder="Latitude" class="form-control" required name="address_latitude" value="{!! $driver->address_latitude !!}" id="address_latitude" onfocusout="load_map()">
            </div>

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Longitude</label>
                <input type="number" step="any" placeholder="Longitude" class="form-control" required name="address_longitude" value="{!! $driver->address_longitude !!}" id="address_longitude" onfocusout="load_map()">
            </div>

        </div>
    </div>

        <br><br>

    <div class="row">

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-xs-offset-1 col-xs-10">

                    <div id="map1" style="width:100%; height:350px;"></div>

            </div>
        </div>

    </div>
<br>

                <div class="row">
                    <div class="form-group">
                        <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            {!! Form::submit('Update Yesser Man', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<!--     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script> -->
    <script src="{{ URL::asset('AdminAssets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key={{env('GOOGLE_MAP_KEY')}}"></script>
<script type="text/javascript">

        $("#organisations_all").addClass("active");
        $("#admin_org_add").addClass("active");

        document.getElementById("phone_code").value = "{{$driver->phone_code}}";

        checkSuccess = function(response) {

            switch (jQuery.parseJSON(response).statuscode) {
                case 200:
                    return true;
                case 400:
                    return false;
            }
            return true;
        };

        $("#addForm").validate({
            rules: {
                phone_number: {
                    remote: {
                            url: "{{route('driver_unique_ucheck')}}",
                            type: "POST",
                            cache: false,
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                user_id: "{{$driver->user_id}}",
                                phone_number: function() { return $("#phone_number").val(); }
                            },
                            dataFilter: function(response) {
                                return checkSuccess(response);
                            }
                        }

                },
                mulkiya_number: {
                    remote: {
                            url: "{{route('driver_unique_ucheck')}}",
                            type: "POST",
                            cache: false,
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                user_id: "{{$driver->user_id}}",
                                mulkiya_number: function() { return $("#mulkiya_number").val(); }
                            },
                            dataFilter: function(response) {
                                return checkSuccess(response);
                            }
                        }

                }

            },
            messages: {
                name: {
                    required: "Please provide the name"
                },
                profile_pic:{
                    accept: "Only Image is allowed"
                },
                email: {
                    email: "Please provide a valid email",
                    required: "Please provide the email",
                    remote: "Sorry, this email already taken"
                },
                phone_number: {
                    remote: "Sorry, this phone number already taken",
                    required: "Phone number is required"
                },
                mulkiya_number: {
                    remote: "Sorry this Mulkiya number is already taken",
                    required: "Mulkiya number is required"
                },
                mulkiya_validity: {
                    required: "Mulkiya Validity date is required"
                }

            }
        });

    $("#productids").select2({
        dropdownCssClass: "bigdrop",
        placeholder: "Select Products"
      });

    $('#mulkiya_validity').datetimepicker({
        startDate: new Date(),
        format: 'yyyy-mm-dd hh:ii:ss'
    });

    function load_products(category_brand_id)
        {

            $.ajax({
                type: "POST",
                cache: false,
                dataType: "json",
                url: '{{route("dupdate_product_list")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    user_detail_id: '{{$driver->user_detail_id}}',
                    category_brand_id: category_brand_id,
                    admin_language_id: '{{$admin->admin['language_id']}}',
                    //category_brand_product_id: '{{$driver->category_brand_product_id}}'
                },
                success:function(result){

                    if(result.success == 1)
                        {
                            $('#productids').html(result.con).change();
                            //$('#productids').html(result.con);
                        }
                    else
                        {
                            toastr.error('Error.', result.msg );
                        }
                },
                error: function(result){
                    toastr.error('Error.', result.msg );
                }
            }); 

        }

    load_products($('#category_brand_id').val());


    var myMap1;
    var mapOptions;
    var subjectMarker1;

    var roptions = {
            componentRestrictions: {country: ['om','in']}
    };

    function load_map()
        {

            address_latitude = document.getElementById('address_latitude').value;
            address_longitude = document.getElementById('address_longitude').value;

            mapOptions = {
                    zoom: 7,
                    center:new google.maps.LatLng(address_latitude,address_longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP  ,
                    scrollwheel: false
                };

            myMap1 = new google.maps.Map(document.getElementById('map1'), mapOptions);

            if(address_latitude != '' && address_longitude != '')
                {

                    myMap1.setCenter(new google.maps.LatLng(Number(address_latitude), Number(address_longitude)));

                    subjectMarker1 = new google.maps.Marker({
                        position: { "lat": Number(address_latitude), "lng": Number(address_longitude) },
                        title: 'Address',
                        map: myMap1,
                        label:'A'
                    });

                }

        }

        var address = (document.getElementById('address'));
        var autocomplete = new google.maps.places.Autocomplete(address, roptions);
        
        autocomplete.setTypes(['geocode']);
        google.maps.event.addListener(autocomplete, 'place_changed', function () 
            {
                var place = autocomplete.getPlace();
                if (!place.geometry) {  return;  }
                var address = '';
                if (place.address_components) {
                        address = [
                            (place.address_components[0] && place.address_components[0].short_name || ''),
                            (place.address_components[1] && place.address_components[1].short_name || ''),
                            (place.address_components[2] && place.address_components[2].short_name || '')
                        ].join(' ');

                    document.getElementById("address_latitude").value = place.geometry.location.lat();
                    document.getElementById("address_longitude").value = place.geometry.location.lng();
                    //console.log(place.geometry.location.lat(), place.geometry.location.lng());

                    load_map();
                                    
                }
                                
            });

        load_map();

</script>


@stop



