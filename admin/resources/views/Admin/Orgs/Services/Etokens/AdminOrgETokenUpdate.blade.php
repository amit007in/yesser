@extends('Admin.layout')

@section('title') 
    Update EToken
@stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Update EToken</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_org_all')}}">All Companies</a>
                        </li>
                        <li>
                        <a href="{{route('admin_org_service_all',['organisation_id' =>$organisation->organisation_id])}}">
                            Company Services
                        </a>
                        </li>
                        <li>
                        <a href="{{ route('admin_org_ser_etokens',['organisation_id' =>$organisation->organisation_id, 'category_id' =>$category->category_id]) }}">
                            ETokens
                        </a>
                        </li>
                        <li>
                        <a href="{{route('admin_org_etoken_update',['organisation_coupon_id' => $coupon->organisation_coupon_id ])}}">
                            <b>
                                Update EToken
                            </b>
                        </a>                            
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-md-6 col-lg-offset-3">
            <div class="ibox-content text-center">
                <h2>{{ $organisation->name }}</h2>
                    <div class="m-b-sm">
                            <a href="{{ $organisation->image_url }}" title="{{ $organisation->name }}" class="lightBoxGallery" data-gallery="">
                                <img src="{{ $organisation->image_url }}/120/120" class="img-circle " alt="profile" >
                            </a>
                    </div>
                    <h2>{{$category->category_details[0]->name}}</h2>
            </div>
        </div>
    </div>

        <br>
            <form method="post" action="{{route('admin_org_etoken_pupdate', ['organisation_coupon_id' => $coupon->organisation_coupon_id ]) }}" enctype="multipart/form-data" id="addForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Price</label>
                <input type="number" placeholder="Price" step="any" autocomplete="off" class="form-control" required name="price" value="{!! $coupon->price !!}" autofocus="on" id="price" min="0">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Quantity</label>
                <input type="number" placeholder="Quantity" autocomplete="off" class="form-control" required name="quantity" value="{!! $coupon->quantity !!}" id="quantity" min="1">
            </div> 

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">

            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Brand</label>
                <select name="category_brand_id" id="category_brand_id" class="form-control border-info" onchange="load_products(this.value)">
                    @foreach($brands as $brand)
                        @if($brand->category_brand_id == $coupon->category_brand_id)
                            <option value="{{$brand->category_brand_id}}" selected>{{$brand->name}}</option>
                        @else
                            <option value="{{$brand->category_brand_id}}">{{$brand->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div> 

             <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Products</label>
                    <select class="category_brand_product_id form-control border-info" id="category_brand_product_id" required="true" name="category_brand_product_id">
                    </select>
            </div>           

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Description</label>
                <textarea class="form-control" name="description" rows="4">{{$coupon->description}}</textarea>
            </div> 

        </div>
    </div>

            <br><br><br>

                <div class="row">
                    <div class="form-group">
                        <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            {!! Form::submit('Update EToken', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

<script type="text/javascript">

        $("#organisations_all").addClass("active");
        $("#all_organisations").addClass("active");

        checkSuccess = function(response) {

            switch (jQuery.parseJSON(response).statuscode) {
                case 200:
                    return true;
                case 400:
                    return false;
            }
            return true;
        };

        $("#addForm").validate({
            rules: {
            },
            messages: {
                price: {
                    required: "Please provide the price"
                },
                quantity:{
                    required: "Please provide the quantity"
                },
                category_brand_product_id: {
                    required: "Please select a product"
                }
            }
        });

    function load_products(category_brand_id)
        {

            $.ajax({
                type: "POST",
                cache: false,
                dataType: "json",
                url: '{{route("brand_products")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    category_brand_id: category_brand_id,
                    admin_language_id: "{{$admin->admin['language_id']}}",
                    category_brand_product_id: "{{$coupon->category_brand_product_id}}"
                },
                success:function(result){

                    if(result.success == 1)
                        $('#category_brand_product_id').html(result.con);
                    else
                        {
                            toastr.error('Error.', result.msg );
                        }
                },
                error: function(result){
                    toastr.error('Error.', result.msg );
                }
            }); 

        }

    load_products($('#category_brand_id').val());

</script>


@stop



