@extends('Admin.layout')

@section('title')
    ETokens
@stop

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        ETokens
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_org_all')}}">All Companies</a>
                        </li>
                        <li>
                        <a href="{{route('admin_org_service_all',['organisation_id' =>$organisation->organisation_id])}}">
                            Company Services
                        </a>
                        </li>
                        <li>
                        <a href="{{ route('admin_org_ser_etokens',['organisation_id' =>$organisation->organisation_id, 'category_id' =>$category_id]) }}">
                            <b>ETokens</b>
                        </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-md-6 col-lg-offset-3">
            <div class="ibox-content text-center">
                <h2>{{ $organisation->name }}</h2>
                    <div class="m-b-sm">
                            <a href="{{ $organisation->image_url }}" title="{{ $organisation->name }}" class="lightBoxGallery" data-gallery="">
                                <img src="{{ $organisation->image_url }}/120/120" class="img-circle " alt="profile" >
                            </a>
                    </div>
                    <h2>{{$category->category_details[0]->name}}</h2>
            </div>
        </div>
    </div>

<br>

    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="form-group">
                <label class="pull-left">Status</label>
                    <select class="form-control" id="filter1">
                        <option value="e">All</option>
                        <option value="Active">Active</option>
                        <option value="Blocked">Blocked</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <label>
                Filter Updated At
            </label>

            <center>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
            </center>                        
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="form-group">
                <label class="pull-left">Created By</label>
                    <select class="form-control" id="filter2">
                        <option value="a">All</option>
                        <option value="Admin">Admin</option>
                        <option value="Organisation">Organisation</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">

    <br>
        <a href="{{ route('admin_org_etoken_add', ['organisation_id' => $organisation->organisation_id, 'category_id' => $category_id ]) }}" class="btn btn-primary pull-right" id="Brand Add" title="Add">
            <i class="fa fa-plus"></i> Add EToken
        </a>
    
        </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Created By</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Product</th>
                                    <th>Actions</th>
                                    <th>Purchases</th>
                                    <th>Created At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($coupons as $coupon)
                <tr class="gradeA footable-odd" style="display: table-row;">

                    <td>{{ $coupon->organisation_coupon_id }}</td>

                    <td>
                        <center>
                            @if($coupon->blocked == '0')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye" aria-hidden="true"> Blocked</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($coupon->created_by == 'Admin')
                                <span class="label label-primary">
                                    <i class="fa fa-at" aria-hidden="true"></i> Admin
                                </span>
                            @else
                                <span class="label label-info">
                                    <i class="fa fa-building-o" aria-hidden="true"></i> Organisation
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <b>OMR</b> {{ $coupon->price }}
                    </td>

                    <td>
                        {{ $coupon->quantity }}
                    </td>

                    <td>
                        {{ $coupon->category_brand_product['name'] }}                        
                    </td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

            <li role="presentation">
                <a role="menuitem" tabindex="-1" href="#">
                    <i class="fa fa-edit"> All Purchases </i> - {{$coupon->purchase_counts}}
                </a>
            </li>


            <li role="presentation">
                <a role="menuitem" tabindex="-1" href="{{route('admin_org_etoken_update',['organisation_coupon_id' => $coupon->organisation_coupon_id])}}">
                    <i class="fa fa-edit"> Update</i>
                </a>
            </li>

            <li role="presentation">
                @if($coupon->blocked == '0' || $coupon->blocked == '2')
                    <a role="menuitem" tabindex="-1" class="block_org" id="{{$coupon->organisation_coupon_id}}">
                        <i class="fa fa-eye-slash"> Block</i>
                    </a>
                @else
                    <a role="menuitem" tabindex="-1" class="unblock_org" id="{{$coupon->organisation_coupon_id}}">
                        <i class="fa fa-eye"> Unblock</i>
                    </a>
                @endif
            </li>

                            </ul>
                        </div>
                    </td>

                    <td>
                        <a href="#" class="btn btn-default">
                            {{$coupon->purchase_counts}}
                        </a>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $coupon->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

        $("#organisations_all").addClass("active");
        $("#all_organisations").addClass("active");

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

        $('#daterange').on('apply.daterangepicker', function(ev, picker)
            {
                dt = document.getElementById('daterange').value;

                window.location.href = "{{route('admin_org_ser_etokens')}}?organisation_id={{$organisation->organisation_id}}&category_id={{$category_id}}&daterange="+dt;
            });

        });

            var table = $('#example').dataTable( 
                {
                   "order": [[ 0, "desc" ]],
//                   "scrollY": 500,
                   "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 5, 8 ] },
                        { "bSearchable": true, "aTargets": [ 4, 6, 7 ] }
                    ],
                   "fnDrawCallback": function( oSettings ) 
                        {

                            $('.block_org').click(function (e) {

                                  var id = $(e.currentTarget).attr("id");

                                    swal({
                                        title: "Are you sure you want to block this coupon?",
                                        text: "User will not be able to view this coupon.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Block it!",
                                        closeOnConfirm: false
                                    }, function () {
                                        window.location.href = '{{route("admin_org_etoken_block")}}?block=1&organisation_coupon_id='+id;
                                    });
                                });

                            $('.unblock_org').click(function (e) {

                                     var id = $(e.currentTarget).attr("id");

                                    swal({
                                        title: "Are you sure you want to unblock this coupon?",
                                        text: "User will be able to view this coupon.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Unblock It!",
                                        closeOnConfirm: false
                                    }, function () {
                                window.location.href = '{{route("admin_org_etoken_block")}}?block=0&organisation_coupon_id='+id;
                                    });
                                });

                        }
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });

            $("#filter2").on('change', function()
                {
                    table.fnFilter($(this).val(), 2);
                });

    </script>


@stop
