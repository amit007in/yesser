    @extends('Admin.layout')

    @section('title')
        Company Services
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Company Services
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_org_all')}}">All Companies</a>
                        </li>
                        <li>
                        <a href="{{route('admin_org_service_all',['organisation_id' =>$organisation->organisation_id])}}">
                            <b>
                                Company Services
                            </b>                            
                        </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-md-6 col-lg-offset-3">
            <div class="ibox-content text-center">
                <h2>{{ $organisation->name }}</h2>
                    <div class="m-b-sm">
                           @if($organisation->image == "")
                            <a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
                            </a>
                        @elseif($organisation->created_by == "Admin")
                             <a href="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}"  class="img-circle">
                            </a>
                        @else
                             <a href="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}"  class="img-circle">
                            </a>
                        @endif
                    </div>
            </div>
        </div>
    </div>
<br>
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Service Name</th>
                                    <!-- <th>Buraq Percentage</th> -->
                                    <th>Actions</th>
<!--                                     <th>Products</th> -->
                                    <th>Yesser Man</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($services as $service)
                <tr class="gradeA footable-odd" style="display: table-row;">

                    <td>{{ $service->organisation_category_id }}</td>

                    <td>
                        <b>
                            {{ $service->category['category_details'][0]->name }}
                        </b>(English)<hr>

                       <!--  <b>
                            {{ $service->category['category_details'][1]->name }}
                        </b>(Hindi)<hr>

                        <b>
                            {{ $service->category['category_details'][2]->name }}
                        </b>(Urdu)<hr>

                        <b>
                            {{ $service->category['category_details'][3]->name }}
                        </b>(Chinese)<hr>

                        <b> -->
                            {{ $service->category['category_details'][4]->name }}
                        </b>(Arabic)<hr>
                    </td>
<!-- 
                    <td>
                        {{ $service->buraq_percentage }}%
                    </td> -->

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#modal-form2" onclick="update_price('{{$service->organisation_category_id}}', '{{$service->buraq_percentage}}', '{{$organisation->bottle_charge}}', '{{$service->category_id}}')">
        <i class="fa fa-edit"> Update Percentage</i>
    </a>
                                </li>

<!--                                 <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('admin_org_service_products', ['organisation_id' => $organisation->organisation_id, 'category_id' => $service->category_id ] ) }}">
        <i class="fa fa-product-hunt"> All Products</i> - {{ $service->product_counts }}
    </a>
                                </li> -->

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('admin_org_service_dadd', ['organisation_id' => $organisation->organisation_id, 'category_id' => $service->category_id ] ) }}">
        <i class="fa fa-plus"> Add Yesser Man</i>
    </a>
                                </li>

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('admin_org_service_dall', ['organisation_id' => $organisation->organisation_id, 'category_id' => $service->category_id ] ) }}">
        <i class="fa fa-child"> All Yesser Man</i> - {{ $service->driver_counts }}
    </a>
                                </li>

                                @if($service->category_id == 2)

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('admin_org_ser_areas', ['organisation_id' => $organisation->organisation_id, 'category_id' => $service->category_id ]) }}">
        <i class="fa fa-map-marker"> All Areas</i> - {{$service->area_counts}}
    </a>
                                </li>

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('admin_org_area_add', ['organisation_id' => $organisation->organisation_id, 'category_id' => $service->category_id ]) }}">
        <i class="fa fa-plus"> Add Area</i>
    </a>
                                </li>

                                <!-- <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('admin_org_ser_etokens', ['organisation_id' => $organisation->organisation_id, 'category_id' => $service->category_id ]) }}">
        <i class="fa fa-tag"> All ETokens</i> - {{$service->coupon_counts}}
    </a>
                                </li>

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('admin_org_etoken_add', ['organisation_id' => $organisation->organisation_id, 'category_id' => $service->category_id ]) }}">
        <i class="fa fa-plus"> Add EToken</i>
    </a>
                                </li> -->

                                @endif

                            </ul>
                        </div>
                    </td>

<!--                     <td>
                        <a href="{{ route('admin_org_service_products', ['organisation_id' => $organisation->organisation_id, 'category_id' => $service->category_id ]) }}" class="btn btn-default">
                            {{ $service->product_counts }}
                        </a>
                    </td>
 -->
                    <td>
                        <a href="{{ route('admin_org_service_dall', ['organisation_id' => $organisation->organisation_id, 'category_id' => $service->category_id ]) }}" class="btn btn-default">
                            {{ $service->driver_counts }}
                        </a>
                    </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <!--                Reply Modal                                -->
    <div class="modal inmodal modal-form2 modal-bg" id="modal-form2" role="dialog" aria-hidden="false">
        <div class="modal-dialog">
            <form method="post" action="{{route('admin_org_cat_percent')}}" enctype="multipart/form-data" >
                <div class="modal-content animated swing">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <i class="fa fa-edit modal-icon"></i>
                        <h4 class="modal-title">Update Percentage</h4>
                    </div>
                    <div class="modal-body">

<!--                         <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Buraq Percentage</label>
    <input type="number" name="buraq_percentage" id="buraq_percentage" class="form-control" step="any" min="0" max="100" autofocus="on">
                            </div>
                        </div> -->
                        <br>
                        <div id="ubottle_charge" class="row" style="display: none;;">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Bottle Charges</label>
    <input type="number" name="bottle_charge" id="bottle_charge" class="form-control" step="any" min="0" autofocus="on">
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="modal-footer">

                        <input hidden="on" value="" name="organisation_category_id" id="organisation_category_id"></input>
                        <input type="hidden" name="organisation_id" id="organisation_id" value="{{$organisation->organisation_id}}">

                        <button type="submit" class="btn btn-primary" id="add_rform_btn">Update</button>
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--                Reply Modal                                -->

    <script>

        $("#organisations_all").addClass("active");
        $("#all_organisations").addClass("active");

            var table = $('#example').dataTable( 
                {
                   //"order": [[ 0, "desc" ]],
//                 "scrollX": true,
                   "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [ 2 ] },
                                { "bSearchable": true, "aTargets": [ 1 ] }
                    ]
                });

    // $('#update_price').click(function (e)
    //     {

        function update_price(organisation_category_id, buraq_percentage, bottle_charge, category_id)
            {

                //document.getElementById('buraq_percentage').value = buraq_percentage;
                document.getElementById('organisation_category_id').value = organisation_category_id;
                
                if(category_id == 2)
                {
                    document.getElementById('bottle_charge').value = bottle_charge;
                    $("#ubottle_charge").css({'display':'block'});  //or
                }
                else
                {
                    $("#ubottle_charge").css({'display':'none'});  //or
                }
                

                //console.log(buraq_percentage, organisation_category_id, bottle_charge, category_id);

            }


//        });

    </script>


@stop
