    @extends('Admin.layout')

    @section('title')
        Company Supports
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Company Supports
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_org_all')}}">All Companies</a>
                        </li>
                        <li>
                        <a href="{{route('admin_org_support_all',['organisation_id' =>$organisation->organisation_id])}}">
                            <b>
                                Company Supports
                            </b>                            
                        </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-md-6 col-lg-offset-3">
            <div class="ibox-content text-center">
                <h2>{{ $organisation->name }}</h2>
                    <div class="m-b-sm">
                            <a href="{{ $organisation->image_url }}" title="{{ $organisation->name }}" class="lightBoxGallery" data-gallery="">
                                <img src="{{ $organisation->image_url }}/120/120" class="img-circle " alt="profile" >
                            </a>
                    </div>
            </div>
        </div>
    </div>
<br>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th width="15%">Id</th>
                                    <th width="45%">Support Name</th>
                                    <th width="10%">Support Image</th>
                                    <th width="15%">Actions</th>
                                    <th width="15%">Drivers</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($supports as $support)
                <tr class="gradeA footable-odd">

                    <td>{{ $support->category_id }}</td>

                    <td>
                        <b>
                            {{ $support->category['category_details'][0]->name }}
                        </b>(English)<hr>

                        <b>
                            {{ $support->category['category_details'][1]->name }}
                        </b>(Hindi)<hr>

                        <b>
                            {{ $support->category['category_details'][2]->name }}
                        </b>(Urdu)<hr>

                        <b>
                            {{ $support->category['category_details'][3]->name }}
                        </b>(Chinese)<hr>

                        <b>
                            {{ $support->category['category_details'][4]->name }}
                        </b>(Arabic)<hr>
                    </td>

                    <td>
                            <a href="{{ $support->image_url }}" title="{{ $support->category['category_details'][0]->name }}" class="lightBoxGallery" data-gallery="">
                                <img src="{{ $support->image_url }}/120/120"  class="img-circle">
                            </a>
                    </td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('admin_org_support_dadd', ['organisation_id' => $organisation->organisation_id, 'category_id' => $support->category_id ] ) }}">
        <i class="fa fa-plus"> Add Driver</i>
    </a>
                                </li>

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('admin_org_support_dall', ['organisation_id' => $organisation->organisation_id, 'category_id' => $support->category_id ] ) }}">
        <i class="fa fa-child"> All Drivers</i> - {{ $support->driver_counts }}
    </a>
                                </li>




                            </ul>
                        </div>
                    </td>

                    <td>
                        <a href="{{ route('admin_org_support_dall', ['organisation_id' => $organisation->organisation_id, 'category_id' => $support->category_id ]) }}" class="btn btn-default">
                            {{ $support->driver_counts }}
                        </a>
                    </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

        $("#organisations_all").addClass("active");
        $("#all_organisations").addClass("active");

            var table = $('#example').dataTable( 
                {
                   "order": [[ 0, "desc" ]],
                   "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [ 2 ] },
                                { "bSearchable": true, "aTargets": [ 1 ] }
                    ]
                });

    </script>


@stop
