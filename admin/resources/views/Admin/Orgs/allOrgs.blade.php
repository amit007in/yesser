

    @extends('Admin.layout')

    @section('title')
        All Companies
    @stop

    @section('content')
	
    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All Companies
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_org_all')}}"><b>All Companies</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                <div class="row">

                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="form-group">
                            <label class="pull-left">Status</label>
                                <select class="form-control" id="filter1">
                                    <option value="e">All</option>
                                    <option value="Blocked">Blocked</option>
                                    <option value="Active">Active</option>
                                </select>                 
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3">
                                    <label>
                                        Filter Registered At
                                    </label>

                                    <center>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
                                    </center>                        
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="form-group">
                            <label class="pull-left">Services</label>
                                <select class="form-control" id="filter6">
                                    <option value="">All</option>
                                    @foreach($services as $service)
                                        <option>{{$service->cat_name}}</option>
                                    @endforeach
                                </select>                 
                        </div>
                    </div>

                    <!-- <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="form-group">
                            <label class="pull-left">Supports</label>
                                <select class="form-control" id="filter7">
                                    <option value="">All</option>
                                    @foreach($supports as $support)
                                        <option>{{$support->cat_name}}</option>
                                    @endforeach
                                </select>                 
                        </div>
                    </div> -->
    
</div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Username</th>
                                    <th>Ranking</th>
                                    <th>Actions</th>
                                    <th>Services</th>
                                    <!-- <th>Supports</th>
                                    <th>Reviews</th>
                                    <th>Ledgers</th>
                                    <th>Logins</th> -->
                                    <th>Registered At</th>
                                </tr>

                                </thead>
                                <tbody>

            @foreach($organisations as $organisation)
                <tr class="gradeA footable-odd">

                    <td>{{ $organisation->organisation_id }}</td>

                    <td>
                        <center>
                            @if($organisation->blocked == '0')
                                <span class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i> Active</span>
                            @else
                                <span class="label label-danger"><i class="fa fa-eye-slash" aria-hidden="true"></i> Blocked</span>
                            @endif
                        </center>
                    </td>

                    <td>
                        {{ $organisation->name }} ({{ $organisation->phone_number }})
                    </td>

                    <td>
    <!--<a href="{{$organisation->image_url}}" title="{{ $organisation->name }}" class="lightBoxGallery" data-gallery="">
        <img src="{{$organisation->image_url}}/120/120"  class="img-circle">
    </a> -->                       
						@if($organisation->image == "")
							<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
								<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
							</a>
						@elseif($organisation->created_by == "Admin")
							 <a href="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
								<img src="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}"  class="img-circle">
							</a>
						@else
							 <a href="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}" title="{{ $organisation->name }}" class="lightBoxGallery img-circle" data-gallery="">
								<img src="{{ URL::asset('BuraqExpress/Uploads/'.$organisation->image) }}"  class="img-circle">
							</a>
						@endif
                    </td>

                    <td>
                        {{ $organisation->username }}
                    </td>

                    <td>
                        <center>
                            <span class="label label-success"><i class="fa fa-line-chart" aria-hidden="true"></i> {{$organisation->sort_order}}</span>
                        </center>
                    </td>


                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                                <li role="presentation">
            <a role="menuitem" tabindex="-1" id="admin_org_update" href="{{route('admin_org_update',['organisation_id' => $organisation->organisation_id])}}">
                <i class="fa fa-edit" aria-hidden="true"> Update</i>
            </a>
                                </li>


                                <li role="presentation">
            <a role="menuitem" tabindex="-1" id="services" href="{{route('admin_org_service_all', ['organisation_id'=>$organisation->organisation_id] )}}">
                <i class="fa fa-certificate" aria-hidden="true"> All Services - {{ count($organisation->organisation_services) }} </i>
            </a>
                                </li>

                               <!--  <li role="presentation">
            <a role="menuitem" tabindex="-1" id="support" href="{{route('admin_org_support_all', ['organisation_id'=>$organisation->organisation_id] )}}">
                <i class="fa fa-support" aria-hidden="true"> All Supports - {{ count($organisation->organisation_supports) }} </i>
            </a>
                                </li>

                                <li role="presentation">
            <a role="menuitem" tabindex="-1" id="support" href="{{route('admin_org_reviews', ['organisation_id'=>$organisation->organisation_id] )}}">
                <i class="fa fa-star" aria-hidden="true"> All Reviews - {{ $organisation->review_counts }} </i>
            </a>
                                </li>

                                <li role="presentation">
            <a role="menuitem" tabindex="-1" id="support" href="{{route('admin_org_ledger_all', ['organisation_id'=>$organisation->organisation_id] )}}">
                <i class="fa fa-book" aria-hidden="true"> All Ledgers - {{ $organisation->ledger_counts }} </i>
            </a>
                                </li>

                                <li role="presentation">
            <a role="menuitem" tabindex="-1" id="support" href="{{route('admin_org_login_history', ['organisation_id'=>$organisation->organisation_id] )}}">
                <i class="fa fa-keyboard-o" aria-hidden="true"> All Logins - {{ $organisation->login_counts }} </i>
            </a>
                                </li> -->

            <li role="presentation">
                @if($organisation->blocked == '0')
                    <a role="menuitem" tabindex="-1" class="block_org" id="{{$organisation->organisation_id}}">
                        <i class="fa fa-eye-slash"> Block</i>
                    </a>
                @else
                    <a role="menuitem" tabindex="-1" class="unblock_org" id="{{$organisation->organisation_id}}">
                        <i class="fa fa-eye"> Unblock</i>
                    </a>
                @endif
            </li>

<!--                                 <li role="presentation">
            <a role='menuitem' tabindex="-1" onclick="delete_popup('{{$organisation->organisation_id}}')">
                <i class="fa fa-bitbucket" aria-hidden="true"> Delete </i>
            </a>
                                </li> -->

                            </ul>
                        </div>
                    </td>

                    <td>
                        @foreach($organisation->organisation_services as $service)
                            <a href="{{route('admin_org_service_all', ['organisation_id'=>$organisation->organisation_id] )}}" class="btn btn-xs btn-default">#{{$service->cat_name}}</a>
                        @endforeach
                    </td>
<!-- 
                    <td>
                        @foreach($organisation->organisation_supports as $support)
                            <a href="{{route('admin_org_support_all', ['organisation_id'=>$organisation->organisation_id] )}}" class="btn btn-xs btn-default">#{{$support->cat_name}}</a>
                        @endforeach
                    </td> -->

                    <!-- <td>
                        <a href="{{ route('admin_org_reviews',['organisation_id' => $organisation->organisation_id]) }}" class="btn btn-default">
                            {{ $organisation->review_counts }}
                        </a>
                    </td>

                    <td>
                        <a href="{{ route('admin_org_ledger_all',['organisation_id' => $organisation->organisation_id]) }}" class="btn btn-default">
                            {{ $organisation->ledger_counts }}
                        </a>
                    </td>

                    <td>
                        <a href="{{ route('admin_org_login_history',['organisation_id' => $organisation->organisation_id]) }}" class="btn btn-default">
                            {{ $organisation->login_counts }}
                        </a>
                    </td> -->

                    <td><i class="fa fa-clock-o"></i> {{ $organisation->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>


    <script>

        $("#organisations_all").addClass("active");
        $("#all_organisations").addClass("active");

        $(document).ready(function()
            {

                $('input[name="daterange"]').daterangepicker({
                        format: 'DD/MM/YYYY',
                        minDate: '1/2/2018',
                        maxDate: moment().format('DD/MM/YYYY'),
                        opens: 'center',
                        drops: 'down',
                        buttonClasses: ['btn', 'btn-sm'],
                        applyClass: 'btn-primary',
                        cancelClass: 'btn-default'
                            });

                $('#daterange').on('apply.daterangepicker', function(ev, picker)
                    {

                        dt = document.getElementById('daterange').value;

                        window.location.href = '{{route("admin_org_all")}}?daterange='+dt;

                    });

            });

        // function delete_popup(organisation_id)
        //     {

        //         swal({
        //             title: "Are you sure you want to delete this Company?",
        //             text: "All data will be completely deleted.",
        //             type: "warning",
        //             showCancelButton: true,
        //             confirmButtonColor: "#DD6B55",
        //             confirmButtonText: "Yes, Delete it!",
        //             closeOnConfirm: false
        //             }, function () {
        //                 window.location.href = '{{route("admin_delete")}}?organisation_id='+organisation_id;
        //             });

        //     }

            var table = $('#example').dataTable( 
                {
                   "order": [[ 0, "desc" ]],
                   // "scrollX": true,
                   // "scrollY": 500,
                   "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 3, 6, 7, 8 ] },
                        { "bSearchable": true, "aTargets": [ 2, 4, 6, 7 ] }
                    ],
                   "fnDrawCallback": function( oSettings ) 
                        {

                            $('.block_org').click(function (e) {

                                var id = $(e.currentTarget).attr("id");

                                swal({
                                        title: "Are you sure you want to block this Company?",
                                        text: "Company will not be able to access the panel.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Block it!",
                                        closeOnConfirm: false
                                    }, function () {
                                        window.location.href = '{{route("admin_org_block")}}?block=1&organisation_id='+id;
                                    });
                                
                            });

                            $('.unblock_org').click(function (e) {

                                var id = $(e.currentTarget).attr("id");

                                swal({
                                        title: "Are you sure you want to unblock this Company?",
                                        text: "Company will be able to access the panel.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Unblock It!",
                                        closeOnConfirm: false
                                    }, function () {
                                window.location.href = '{{route("admin_org_block")}}?block=0&organisation_id='+id;
                                    });
                                
                            });

                        }
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });
            $("#filter6").on('change', function()
                {
                    table.fnFilter($(this).val(), 6);
                });
            $("#filter7").on('change', function()
                {
                    table.fnFilter($(this).val(), 7);
                });


    </script>


@stop
