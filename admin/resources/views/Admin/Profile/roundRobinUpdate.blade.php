@extends('Admin.layout')

@section('title') 
     Password Update
@stop

    @section('content')
		<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet"><!--   Add M3 Part 3.2   -->
		<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script><!--   Add M3 Part 3.2   -->
    <div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-xs-12">
				<div class="row wrapper border-bottom white-bg page-heading">
					<div class="col-lg-10">
						<h2> Round-Robin Update</h2>
						<ol class="breadcrumb">
							<li>
								<a href="{{route('admin_dashboard')}}">Dashboard</a>
							</li>
							<li>
								<a href="{{route('aroundrobin_update_get')}}"><b> Round-Robin</b></a>
							</li>                        
						</ol>
					</div>
				</div>
				<div class="wrapper wrapper-content animated fadeInRight">
					 <div class="row">
						<!--   Add M3 Part 3.2   -->
						<div class="col-lg-4 col-md-4" style="float: right;">
							<center>
								<label>Round Robin Scheduling</label>  
								@if($admin['admin']->round_robin == "false")
									<input id="toggle-event" type="checkbox" data-toggle="toggle">
								@else
									<input id="toggle-event" type="checkbox" checked data-toggle="toggle" >
								@endif
							</center>
							<br>
						</div>  			
					</div>
					<form method="post" action="{{route('aroundrobin_update_post')}}" id="roundrobinForm">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="row">
							<div class="form-group">
							   <div class='col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4'>
									<label>Request Time In Seconds</label>
									<input type="number" placeholder="Scheduling Time" value="{{ $admin['admin']->round_robin_time }}" autocomplete="off" class="form-control" required name="round_robin" autofocus="on" id="round_robin">
								</div>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="form-group">
								<div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>                    
									{!! Form::submit('Update', ['class' => 'btn btn-success']) !!}
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
	<script type="text/javascript">
		$("#settings_all").addClass("active");
		$("#admin_round_robin").addClass("active");
		/*
		*Round Robin Enable/Disable
		*Add M3 Part 3.2
		*/
		$('#toggle-event').change(function() {
			var value = $(this).prop('checked');
			console.log(value);
			route = '{!! route('admin_roundrobin_enabledisable') !!}?value='+value;
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					console.log("success");
				}
			});
		})
	</script>


@stop



