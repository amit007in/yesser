@extends('Admin.layout')

@section('title') 
    Add Promocode
@stop

    @section('content')

<link href="{{ URL::asset('AdminAssets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('AdminAssets/css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add Promocode</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_promocode_all')}}">All Promocode</a>
                        </li>
                        <li>
                            <a href="{{route('admin_promocode_aget') }}">
                                <b>Add Promocode</b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <br>
                <form method="post" action="{{route('admin_promocode_apost') }}" enctype="multipart/form-data" id="addForm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <meta name="csrf-token" content="{{ csrf_token() }}">
                    <div class="row">
                        <div class="form-group">
                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Promo Title </label>
                                <input type="text" placeholder="Promo Title" class="form-control" required name="title" value="{!! Form::old('title') !!}" autofocus="on" id="title" maxlength="30">
                            </div> 
                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>Promo Description</label>
                                <input type="text" placeholder="Promo Descirption" class="form-control" required name="description" value="{!! Form::old('description') !!}" autofocus="on" id="description" maxlength="255">
                            </div> 
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Promo Code</label>
                                <input type="text" placeholder="Coupon Code" class="form-control" required name="code" value="{!! Form::old('code') !!}" autofocus="on" id="code" maxlength="30">
                            </div> 
                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>Maximum Rides</label>
                                <input type="number" placeholder="Maximum Rides" class="form-control" required name="rides_value" value="{!! Form::old('rides_value') !!}" autofocus="on" id="rides_value" min="1">
                            </div> 
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Category</label>
                                <select name="category_id" id="category_id" class="form-control border-info" onchange="load_brands(this.value)">
                                <option value="" >Select Category</option>
                                    @foreach($category as $category)
                                        <option value="{{$category->category_id}}" >{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div> 
                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>Brands</label>
                                <select class="brandids form-control border-info" id="brandids" required="true" name="brandids" onchange="load_products(this.value)">
                                </select>
                            </div> 
                                    
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Products</label>
                                
                                <select class="productids form-control border-info" id="productids" multiple="true" required="true" name="productids[]">
                                </select>
                            </div> 
                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>Image</label>
                                <input type="file" name="image" id="image">
                            </div>           
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Start Date</label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="start_at" id="start_at" value="{{Request::old('start_at')}}" placeholder="Start At" readonly>
                                </div>
                            </div>
                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>End Date</label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="expires_at" id="expires_at" value="{{Request::old('expires_at')}}" placeholder="End At" readonly>
                                </div>
                            </div> 

                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Type</label>
                                <select name="coupon_type" class="form-control" onchange="change_value_input(this.value)">
                                    <option value="Percentage">Percentage</option>
                                    <option value="Value">Value</option>
                                </select>
                            </div> 
                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>Type Value</label>
                                <input type="number" placeholder="Type Value" class="form-control" required name="amount_value" value="{!! Form::old('amount_value') !!}" id="amount_value" step="any" min="0">
                            </div> 

                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                        
                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Promo Price (OMR)</label>
                                <input type="number" placeholder="Coupon Price" class="form-control" required name="price" id="price" step="any" min="0">
                            </div>  

                            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                <label>Promo Expiry Days</label>
                                <input type="number" placeholder="Coupon Expiry Days" class="form-control" required name="expiry_days" id="expiry_days" min="1" readonly>
            
                            </div> 

                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                <label>Select Users</label>
                                <select class="usersId form-control border-info" autocomplete="true" id="usersId" multiple="true" required="true" name="usersId[]">                                 
                                </select>
                            </div>
                        </div>
						
						<input type="checkbox" name="all_users" id="all_users" value="1"> Select All Users
                    </div>
                    <br>
                    <br><br><br>
                    <div class="row">
                        <div class="form-group">
                            <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                                {!! Form::submit('Add Promocode', ['class' => 'btn btn-success', 'id' => 'ordersubmitform']) !!}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="{{ URL::asset('AdminAssets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ URL::asset('AdminAssets/js/plugins/select2/select2.full.min.js') }}"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">

    $("#settings_all").addClass("active");
    $("#promocode_all").addClass("active");
	//Check Select all users
    $('#all_users').change(function(){
        if($(this).is(":checked")) {
           $('#usersId').prop('disabled',true);
        }else{
            $('#usersId').prop('disabled',false);
        }
    })
    checkSuccess = function(response) {

            switch (jQuery.parseJSON(response).statuscode) {
                case 200:
                    return true;
                case 400:
                    return false;
            }
            return true;
        };

        $("#addForm").validate({
            rules: {
                code: {
                    remote: {
                            url: "{{ROUTE("coupon_unique_check")}}",
                            type: "POST",
                            cache: false,
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                code: function() { return $("#code").val(); }
                            },
                            dataFilter: function(response) {
                                return checkSuccess(response);
                            }
                        }

                } 
            },
            messages: {
                code: {
                    required: "Promo Code is required",
                    remote: "Sorry, this promo code is already taken"
                },
                rides_value: {
                    required: "Maximum rides parameter is required"
                },
                amount_value: {
                    required: "Promo value is required"
                },
                title: {
                    required: "Title is required"
                },
                expires_at: {
                    required: "End day is required"
                },
                start_at: {
                    required: "Start day is required"
                },
                productids: {
                    required: "Kindly select product"
                },
                brandids: {
                    required: "Kindly select Brand"
                },
                category_id: {
                    required: "Kindly select Category"
                }

            }
        });

    $('select').on('change', function() {
       $(this).valid();
   });
   $('#expires_at').datetimepicker({
       startDate: new Date(),
       format: 'yyyy-mm-dd hh:ii:00',
       pickerPosition: "top-right"
   });
   $('#start_at').datetimepicker({
       startDate: new Date(),
       format: 'yyyy-mm-dd hh:ii:00',
       pickerPosition: "top-right"
   });

    function change_value_input(value)
        {
            if(value == "Percentage")
            {
                document.getElementById("amount_value").max = 100;
            }
            else
            {
                $("#amount_value").removeAttr("max");
            }
        }

        if("{{Form::old('coupon_type')}}")
            change_value_input("{{Form::old('coupon_type')}}");
        else
            change_value_input("Percentage");

        $("#productids").select2({
          dropdownCssClass: "bigdrop",
          placeholder: "Select Products"
        });
        $("#brandids").select2({
          dropdownCssClass: "bigdrop",
          placeholder: "Select Brands"
        });
        
        /*$("#usersId").select2({
          dropdownCssClass: "bigdrop",
          placeholder: "Select Users"
        });*/


        $('#usersId').select2({
            placeholder: 'Search Users',
            ajax: {
              url: '{{route("autocomplete")}}',
              headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                
              dataType: 'json',
              delay: 250,
              processResults: function (data) {
                  console.log(data);
                return {
                  results:  $.map(data, function (item) {
                      console.log(item);
                        return {
                            text: item.name,
                            id: item.user_id
                        }
                    })
                };
              },
              cache: true
            }
        });


        
        

        function load_products(category_brand_id)
        {
            if(category_brand_id == ""){
               return false;
           }
            $.ajax({
                type: "POST",
                cache: false,
                dataType: "json",
                url: '{{route("brand_products")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    category_brand_id: category_brand_id,
                    admin_language_id: '{{$admin->admin['language_id']}}'
                },
                success:function(result){
                   
                    if(result.success == 1)
                        {
                            $("#productids").select2("val", "");
                            $('#productids').html(result.con);
                        }
                    else
                        {
                            toastr.error('Error.', result.msg );
                        }
                },
                error: function(result){
                    toastr.error('Error.', result.msg );
                }
            }); 

        }

        function load_brands(category_id)
        {
            $.ajax({
                type: "POST",
                cache: false,
                dataType: "json",
                url: '{{route("brands_listings")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    category_id: category_id,
                    admin_language_id: '{{$admin->admin['language_id']}}'
                },
                success:function(result){
                    if(result.success == 1)
                        {
                            $("#brandids").select2("val", "");
                            $('#brandids').html(result.con);
                        }
                    else
                        {
                            toastr.error('Error.', result.msg );
                        }
                },
                error: function(result){
                    toastr.error('Error.', result.msg );
                }
            }); 

        }
        
        $('#expires_at').change(function() {
           var start = $('#start_at').val();
           var end   = $('#expires_at').val();
           var startDate = moment(start, "YYYY-MM-DD HH:mm:s");
           var endDate = moment(end, "YYYY-MM-DD HH:mm:s");
           var result = endDate.diff(startDate, 'days');
           $('#expiry_days').val(result);
       });
    //load_products($('#category_brand_id').val());
        $(document).ready(function () {
            $("#addForm").submit(function () {
                if($("#addForm").valid()){
                    $("#ordersubmitform").prop('disabled',true);
                return true;
                }

            });
        });
</script>


@stop



