
@extends('Admin.layout')

@section('title')
All Coupons
@stop

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All Promocodes
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_promocode_all')}}"><b>All Promocodes</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                <div class="row">

                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="form-group">
                            <label class="pull-left">Status</label>
                            <select class="form-control" id="filter1">
                                <option value="e">All</option>
                                <option value="Blocked">Blocked</option>
                                <option value="Active">Active</option>
                            </select>                 
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <label>
                            Filter Created At
                        </label>

                        <center>
                            <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
                        </center>                        
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="form-group">
                            <label class="pull-left">Type</label>
                            <select class="form-control" id="filter3">
                                <option value="">All</option>
                                <option value="Percentage">Percentage</option>
                                <option value="Value">Value</option>
                            </select>                 
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <br>
                        <a href="{{ route('admin_promocode_aget') }}" class="btn btn-primary pull-right" title="Add Coupon">
                            <i class="fa fa-plus"></i> Add Promocode
                        </a>
                    </div>

                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="">
                            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                    <tr>
                                        <th>Id</th>
                                        <th>Status</th>
                                        <th>Code</th>
                                        <th>Type</th>
                                        <th>Value</th>
                                        <th>Price (OMR)</th>
                                        <th>Actions</th>
                                        <th>Purchased</th>
                                        <th>Users</th>
                                        <th>Category</th>
                                        <th>Brand</th>
                                        <th>Product</th>
                                        <th>Expires At</th>
                                        <th>Created At</th>
                                    </tr>

                                </thead>
                                <tbody>


                                    @foreach($coupons as $coupon)
                                    <tr class="gradeA footable-odd">

                                        <td>{{ $coupon->coupon_id }}</td>

                                        <td>
                                <center>
                                    @if($coupon->blocked == '0')
                                    <span class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i> Active</span>
                                    @else
                                    <span class="label label-danger"><i class="fa fa-eye-slash" aria-hidden="true"></i> Blocked</span>
                                    @endif
                                </center>
                                </td>

                                <td>
                                    {{ $coupon->code }}
                                </td>

                                <td>
                                <center>
                                    @if($coupon->coupon_type == 'Percentage')
                                    <span class="label label-warning">
                                        Percentage
                                    </span>
                                    @else
                                    <span class="label label-success">
                                        Value
                                    </span>
                                    @endif
                                </center>
                                </td>

                                <td>
                                    {{ $coupon->amount_value }}
                                </td>

                                <td>
                                    {{ $coupon->price }}
                                </td>

                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                            Actions
                                            <span class="caret"></span>
                                        </button>

                                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                                            <li role="presentation">
                                                <a role="menuitem" tabindex="-1" href="{{route('admin_promocode_uget',['coupon_id' => $coupon->coupon_id])}}">
                                                    <i class="fa fa-edit" aria-hidden="true"> Update</i>
                                                </a>
                                            </li>


                                            <li role="presentation">
                                                <a role="menuitem" tabindex="-1" id="admin_cust_orders" href="#">
                                                    <i class="fa fa-bookmark" aria-hidden="true"> All Purchases - {{ $coupon->purchase_counts }}</i>
                                                </a>
                                            </li>

                                            <li role="presentation">
                                                @if($coupon->blocked == '0')
                                                <a role="menuitem" tabindex="-1" class="block_cust" id="{{$coupon->coupon_id}}">
                                                    <i class="fa fa-eye-slash"> Block</i>
                                                </a>
                                                @else
                                                <a role="menuitem" tabindex="-1" class="unblock_cust" id="{{$coupon->coupon_id}}">
                                                    <i class="fa fa-eye"> Unblock</i>
                                                </a>
                                                @endif
                                            </li>        

                                        </ul>
                                    </div>
                                </td>

                                <td>
                                    <a href="#" class="btn btn-default" style="background-color: #23c6c8;border-color: #23c6c8;color: #FFFFFF;">
                                        {{ $coupon->purchase_counts }} <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="#" onclick="getusers({{ $coupon->coupon_id }})" class="btn btn-default" style="background-color: #23c6c8;border-color: #23c6c8;color: #FFFFFF;">
                                        {{ $coupon->promousers }} <i class="fa fa-eye"></i>
                                    </a>

                                    

                                </td>
                                <td>
                                    {{ $coupon->categoryname }}
                                </td>
                                <td>
                                    {{ $coupon->categorybrand }}
                                </td>
                                <td>
                                    {{ $coupon->categoryproduct }}
                                </td>

                                <td><i class="fa fa-clock-o"></i> {{ $coupon->expires_atz }} </td>

                                <td><i class="fa fa-clock-o"></i> {{ $coupon->created_atz }} </td>

                                </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Users Maximum Rides</h5>
                <button style="opacity: .5; margin-top: -18px;" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">          
                    <table class="table">
                        <thead>
                            <tr>
                                <th>User Name</th>
                                <th>Ride Usage</th>
                            </tr>
                        </thead>
                        <tbody id="sasd"> 
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer"></div>
    </div>
</div>
<script>

    $("#settings_all").addClass("active");
    $("#promocode_all").addClass("active");

    $(document).ready(function ()
    {

        $('input[name="daterange"]').daterangepicker({
            format: 'DD/MM/YYYY',
            minDate: '1/2/2018',
            maxDate: moment().format('DD/MM/YYYY'),
            opens: 'center',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default'
        });

        $('#daterange').on('apply.daterangepicker', function (ev, picker)
        {

            dt = document.getElementById('daterange').value;

            window.location.href = '{{route("admin_promocode_all")}}?daterange=' + dt;

        });

    });

    var table = $('#example').dataTable(
            {
                "order": [[0, "desc"]],
                "scrollX": true,
                "scrollY": 500,
                "aoColumnDefs": [
                    {"bSortable": false, "aTargets": [6]},
                    {"bSearchable": true, "aTargets": [2, 4]}
                ],
                "fnDrawCallback": function (oSettings)
                {

                    $('.block_cust').click(function (e) {

                        var id = $(e.currentTarget).attr("id");

                        swal({
                            title: "Are you sure you want to block this promocode?",
                            text: "Customer will not be able to see the promocode.",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes, Block it!",
                            closeOnConfirm: false
                        }, function () {
                            window.location.href = '{{route("admin_promocode_block")}}?block=1&coupon_id=' + id;
                        });
                    });

                    $('.unblock_cust').click(function (e) {

                        var id = $(e.currentTarget).attr("id");

                        swal({
                            title: "Are you sure you want to unblock this promocode?",
                            text: "Customer will be able to see the promocode.",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes, Unblock It!",
                            closeOnConfirm: false
                        }, function () {
                            window.location.href = '{{route("admin_promocode_block")}}?block=0&coupon_id=' + id;
                        });
                    });

                }
            });

    $("#filter1").on('change', function ()
    {
        table.fnFilter($(this).val(), 1);
    });

    $("#filter3").on('change', function ()
    {
        table.fnFilter($(this).val(), 3);
    });

    function getusers(id)
    {
        //alert(id);
            $.ajax({
                type: "POST",
                cache: false,
                dataType: "json",
                url: '{{route("getpromousersrides")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    coupon_id: id
                },
                success:function(result){
                    console.log(result);
                    $("#sasd").html("");
                    $('#sasd').html(result);
                    $('#exampleModalScrollable').modal('show');
                },
                error: function(result){
                    toastr.error('Error.', result.msg );
                }
            });
    }
</script>


@stop
