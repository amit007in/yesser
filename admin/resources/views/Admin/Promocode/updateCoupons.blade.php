@extends('Admin.layout')

@section('title') 
    Update Promocode
@stop

    @section('content')

<link href="{{ URL::asset('AdminAssets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('AdminAssets/css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Update Promocode</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_promocode_all')}}">All Promocodes</a>
                        </li>
                        <li>
                            <a href="{{route('admin_promocode_uget',['coupon_id'=>$coupon->coupon_id]) }}">
                                <b>Update Promocode</b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

        <br>
        <form method="post" action="{{route('admin_promocode_upost',['coupon_id'=>$coupon->coupon_id]) }}" enctype="multipart/form-data" id="addForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="coupanId" id="coupanId" value="{{ $coupon->coupon_id }}">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <div class="row">
                <div class="form-group">
                    <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                        <label>Promo Title </label>
                        <input type="text" placeholder="Promo Title" class="form-control" required name="title" value="{!! $coupon->title !!}" autofocus="on" id="title" maxlength="30">
                    </div> 
                    <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                        <label>Promo Description</label>
                        <input type="text" placeholder="Promo Descirption" class="form-control" required name="description" value="{!! $coupon->description !!}" autofocus="on" id="description" maxlength="255">
                    </div> 
                </div>
            </div>
            <br>
            <div class="row">
                <div class="form-group">
                
                    <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                        <label>Promo Code</label>
                        <input type="text" placeholder="Promo Code" class="form-control" required name="code" value="{!! $coupon->code !!}" autofocus="on" id="code" maxlength="30">
                    </div> 

                    <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                        <label>Maximum Rides</label>
                        <input type="number" placeholder="Maximum Rides" class="form-control" required name="rides_value" value="{!! $coupon->rides_value !!}" autofocus="on" id="rides_value" min="1">
                    </div> 

                </div>
            </div>
            <br>
            <div class="row">
                <div class="form-group">
                    <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                        <label>Category</label>
                        <select name="category_id" id="category_id" class="form-control border-info" onchange="load_brands(this.value)">
                        <option value="" >Select Category</option>
                            @foreach($category as $category)
                                @if($category->category_id == $coupon->category_id)
                                <option value="{{$category->category_id}}" selected="true">{{$category->name}}</option>
                                @else
                                <option value="{{$category->category_id}}">{{$category->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div> 
                    <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                        <label>Brands</label>
                        <select class="brandids form-control border-info" id="brandids" required="true" name="brandids" onchange="load_products(this.value)">
                        </select>
                    </div> 
                            
                </div>
            </div>
            <br>
            <div class="row">
                <div class="form-group">
                    <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                        <label>Products</label>
                        <select class="productids form-control border-info" id="productids" required="true" multiple="true" name="productids[]">
                        </select>
                    </div> 
                    <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                        <label>Image</label>
                        <input type="file" name="image" id="image">
                        <img  class="img-circle" style="margin-top: -38px;float: right;" src="{{ ($coupon->image) ? $coupon->image : '' }}">
                    </div>           
                </div>
            </div>
            <br>
            <div class="row">
                <div class="form-group">
                    <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                        <label>Start Date</label>
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="start_at" id="start_at" value="{{ $coupon->start_at }}" placeholder="Start At" readonly>
                        </div>
                    </div>
                    <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                        <label>End Date</label>
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="expires_at" id="expires_at" value="{{ $coupon->expires_at }}" placeholder="End Date" readonly>
                        </div>
                    </div> 
                </div>
            </div>
            <br>
            <div class="row">
                <div class="form-group">
                    <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                        <label>Type</label>
                        <select name="coupon_type" class="form-control" onchange="change_value_input(this.value)">
                            @if($coupon->coupon_type == "Percentage")
                                <option value="Percentage" selected="true">Percentage</option>
                                <option value="Value">Value</option>
                            @else
                                <option value="Percentage">Percentage</option>
                                <option value="Value" selected="true">Value</option>
                            @endif
                        </select>
                    </div> 
                    <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                        <label>Type Value</label>
                        <input type="number" placeholder="Type Value" class="form-control" required name="amount_value" value="{!! $coupon->amount_value !!}" id="amount_value" step="any" min="0">
                    </div> 
                </div>
            </div>
            <br>
            <div class="row">
                <div class="form-group">
                <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                        <label>Promo Price (OMR)</label>
                        <input type="number" placeholder="Promo Price" class="form-control" required name="price" value="{!! $coupon->price !!}" id="price" step="any" min="0">
                    </div>
                    <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                        <label>Promo Expiry Days</label>
                        <input type="number" placeholder="Promo Expiry Days" class="form-control" required name="expiry_days" value="{!! $coupon->expiry_days !!}" id="expiry_days" min="1" readonly>
                    </div> 
                </div>
            </div>
            <br>
            <div class="row">
                <div class="form-group">
                    <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                        <label>Users</label>
						@if($coupon->all_users_status == '1')
                        <select class="usersId form-control border-info" id="usersId" multiple="true"  name="usersId[]" disabled>
                        @else
                        <select class="usersId form-control border-info" id="usersId" multiple="true" required="true" name="usersId[]">
                        @endif
                        <option value="" >Select User</option>
                            
                        </select>
                        
                    </div>
                
                </div>
				<br>
				@if($coupon->all_users_status == '1')
                    <input type="checkbox" name="all_users" id="all_users" value="1" checked> Select All Users
                @else
                    <input type="checkbox" name="all_users" id="all_users" value="1"> Select All Users
                @endif
            </div>
            <br>
            <br><br><br>
            <div class="row">
                <div class="form-group">
                    <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                        {!! Form::submit('Update Promocode', ['class' => 'btn btn-success']) !!}
                    </div>
                </div>
            </div>
        </form>
        </div>
        </div>

    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="{{ URL::asset('AdminAssets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ URL::asset('AdminAssets/js/plugins/select2/select2.full.min.js') }}"></script>
<script type="text/javascript">

    $("#settings_all").addClass("active");
    $("#promocode_all").addClass("active");
	$('#all_users').change(function(){
        if($(this).is(":checked")) {
           $('#usersId').prop('disabled',true);
        }else{
            $('#usersId').prop('disabled',false);
        }
    })
    checkSuccess = function(response) {

            switch (jQuery.parseJSON(response).statuscode) {
                case 200:
                    return true;
                case 400:
                    return false;
            }
            return true;
        };

        $("#addForm").validate({
            rules: {
                code: {
                    remote: {
                            url: "{{ROUTE("coupon_uunique_check")}}",
                            type: "POST",
                            cache: false,
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                coupon_id: "{{$coupon->coupon_id}}",
                                code: function() { return $("#code").val(); }
                            },
                            dataFilter: function(response) {
                                return checkSuccess(response);
                            }
                        }

                }
 
            },
            messages: {
                code: {
                    required: "Promo Code is required",
                    remote: "Sorry, this promo code is already taken"
                },
                rides_value: {
                    required: "Maximum rides parameter is required"
                },
                amount_value: {
                    required: "Promo value is required"
                },
                title: {
                    required: "Title is required"
                },
                expires_at: {
                    required: "End day is required"
                },
                start_at: {
                    required: "Start day is required"
                },
                productids: {
                    required: "Kindly select product"
                },
                brandids: {
                    required: "Kindly select Brand"
                },
                category_id: {
                    required: "Kindly select Category"
                }

            }
        });

    $('select').on('change', function() {
       $(this).valid();
   });
   $('#expires_at').datetimepicker({
       startDate: new Date(),
       format: 'yyyy-mm-dd hh:ii:ss',
       pickerPosition: "top-right"
   });
   $('#start_at').datetimepicker({
       startDate: new Date(),
       format: 'yyyy-mm-dd hh:ii:ss',
       pickerPosition: "top-right"
   });

    function change_value_input(value)
        {
            if(value == "Percentage")
            {
                document.getElementById("amount_value").max = 100;
            }
            else
            {
                $("#amount_value").removeAttr("max");
            }
        }

        change_value_input("{{$coupon->coupon_type}}");

        $("#productids").select2({
          dropdownCssClass: "bigdrop",
          placeholder: "Select Products"
        });
        $("#brandids").select2({
          dropdownCssClass: "bigdrop",
          placeholder: "Select Brands"
        });
        
        $("#usersId").select2({
          dropdownCssClass: "bigdrop",
          placeholder: "Select Users"
        });

        /** List Products */
        function load_products(category_brand_id)
        {
            if(category_brand_id == ""){
               return false;
           }
            $.ajax({
                type: "POST",
                cache: false,
                dataType: "json",
                url: '{{route("dupdate_coupon_brand_products")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    category_brand_id: category_brand_id,
                    admin_language_id: '{{$admin->admin['language_id']}}',
                    coupon_id: '{{ $coupon->coupon_id }}'
                },
                success:function(result){
                   
                    if(result.success == 1)
                        {
                            $('#productids').html(result.con).change();
                        }
                    else
                        {
                            toastr.error('Error.', result.msg );
                        }
                },
                error: function(result){
                    toastr.error('Error.', result.msg );
                }
            }); 

        }

        /** List Brands */
        function load_brands(category_id)
        {
            $.ajax({
                type: "POST",
                cache: false,
                dataType: "json",
                url: '{{route("dupdate_coupon_brands_listings")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    category_id: category_id,
                    admin_language_id: '{{$admin->admin['language_id']}}',
                    coupon_id: '{{ $coupon->coupon_id }}'
                },
                success:function(result){
                    if(result.success == 1)
                        {
                            $('#brandids').html(result.con).change();
                        }
                    else
                        {
                            toastr.error('Error.', result.msg );
                        }
                },
                error: function(result){
                    toastr.error('Error.', result.msg );
                }
            }); 

        }
        load_brands($('#category_id').val());
       
        /** List Users */
        function load_users(userId)
        {
            
            $.ajax({
                type: "POST",
                cache: false,
                dataType: "json",
                url: '{{route("dupdate_users_list")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    user_detail_id: userId
                },
                success:function(result){

                    if(result.success == 1)
                        {
                            $('#usersId').html(result.con).change();
                        }
                    else
                        {
                            toastr.error('Error.', result.msg );
                        }
                },
                error: function(result){
                    toastr.error('Error.', result.msg );
                }
            }); 

        }

        load_users($('#coupanId').val());
        
        $('#expires_at').change(function() {
           var start = $('#start_at').val();
           var end   = $('#expires_at').val();
           var startDate = moment(start, "YYYY-MM-DD HH:mm:s");
           var endDate = moment(end, "YYYY-MM-DD HH:mm:s");
           var result = endDate.diff(startDate, 'days');
           $('#expiry_days').val(result);
       });
</script>


@stop



