@extends('Admin.layout')

@section('title') 
    Order Reports
@stop

    @section('content')

    <link href="{{ URL::asset('AdminAssets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('AdminAssets/js/plugins/select2/select2.full.min.js') }}"></script>


    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Order Reports</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_order_report') }}">
                                <b>Order Reports</b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

        <br>
            <form method="post" action="{{route('admin_order_preport') }}" enctype="multipart/form-data" id="addForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="row">
        <div class="form-group">
            <div class='col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3'>
                    <label class="pull-left">Status</label>
                        <select class="form-control" id="status_check" name="status_check" value="{{$data['status_check']}}">
                            <option value="All">All</option>

                            <option value="Searching">Searching</option>
                            <option value="Confirmed">Confirmed</option>
                            <option value="Scheduled">Scheduled</option>

                            <option value="Ongoing">Ongoing</option>
                            <option value="Rejected">Rejected</option>
                            <option value="Timeout">Timeout</option>

                            <option value="Customer Cancelled">Customer Cancelled</option>
                            <option value="Driver Cancelled">Driver Cancelled</option>

                            <option value="Completed">Completed</option>
                            <option value="Confirmation Pending">Confirmation Pending</option>
                        </select>                 
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="form-group">
            <div class='col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3'>
                <label>Filter Booking Timing</label>
                <center>
                    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$data['daterange']}}"/>
                </center>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="form-group">

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>

            <label class="pull-left">Services Categories</label>

<select class="form-control tokenizationSelect1" id="tokenizationSelect1" name="services[]" multiple="true" required="false">

              @foreach($services as $service)
        <option value="{{$service->category_id}}" selected="true" title="{{$service['category_details'][0]->name}}">
            {{$service['category_details'][0]->name}}
        </option>
              @endforeach

</select>

            </div>

            <div class='col-lg-4 col-md-4 col-sm-4'>
                <label class="pull-left">Support Categories</label>

<select class="form-control tokenizationSelect2" id="tokenizationSelect2" name="supports[]" multiple="true" required="false">

              @foreach($supports as $support)
        <option value="{{$support->category_id}}" selected="true" title="{{$support['category_details'][0]->name}}">
            {{$support['category_details'][0]->name}}
        </option>
              @endforeach

</select>

            </div>

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">

            <div class='col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3'>
                <label class="pull-left">Customers</label>
                <select class="js-data-example-ajax" name="customers" multiple="true" class="form-control" style="min-width: 100% !important;"></select>
            </div>

        </div>
    </div>

            <br><br><br>

                <div class="row">
                    <div class="form-group">
                        <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            {!! Form::submit('Export Reports', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>

<script type="text/javascript">

    $("#reports_all").addClass("active");
    $("#admin_order_report").addClass("active");

/////////////       Filters     ///////////////////////
        $('#status_check').val('{{$data['status_check']}}');
        $('#order_type_check').val('{{$data['order_type_check']}}');
/////////////       Filters     ///////////////////////

        // $("#addForm").validate({
        //     rules: {
        //         // code: {
        //         //     remote: {
        //         //             url: "{{ROUTE("coupon_unique_check")}}",
        //         //             type: "POST",
        //         //             cache: false,
        //         //             dataType: "json",
        //         //             headers: {
        //         //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         //             },
        //         //             data: {
        //         //                 code: function() { return $("#code").val(); }
        //         //             },
        //         //             dataFilter: function(response) {
        //         //                 return checkSuccess(response);
        //         //             }
        //         //         }

        //         // } 
        //     },
        //     messages: {
        //         // code: {
        //         //     required: "Coupon Code is required",
        //         //     remote: "Sorry, this coupon code is already taken"
        //         // },
        //     }
        // });

        $(document).ready(function()
            {

                $('input[name="daterange"]').daterangepicker({
                    opens: 'center',
                    autoApply: true,
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                }, function(start, end, label) {
                //console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                });

            });

        $(".tokenizationSelect1").select2({});
        $(".tokenizationSelect2").select2({});


        $('.js-data-example-ajax').select2({
          ajax: {
            url: '{{route('admin_custs_ajax')}}',
            dataType: 'json',

            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
          }
        });


</script>


@stop



