@extends('Admin.layout')

@section('title')
   All Reviews
@stop

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                       All Reviews
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_all_reviews')}}">
                                <b>
                                   All Reviews
                                </b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="form-group">
                <label class="pull-left">Order Type</label>
                    <select class="form-control" id="filter1">
                        <option value="">All</option>
                        <option value="Service">Service</option>
                        <option value="Support">Support</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6">
            <label>
                Filter Updated At
            </label>

            <center>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
            </center>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="form-group">
                <label class="pull-left">CreatedBy</label>
                    <select class="form-control" id="filter2">
                        <option value="">All</option>
                        <option value="Customer">Customer</option>
                        <option value="Driver">Driver</option>
                    </select>                 
            </div>
        </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Order Type</th>
                                    <th>CreatedBy</th>
                                    <th>Customer</th>
                                    <th>CustomerPic</th>
                                    <th>Driver</th>
                                    <th>DriverPic</th>
                                    <th>Ratings</th>
                                    <th>Comments</th>
                                    <th>Actions</th>
                                    <th>Reviewed At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($reviews as $review)
                <tr class="gradeA footable-odd" style="display: table-row;">

                    <td>{{ $review->order_rating_id }}</td>

                    <td>
                        @if($review->order['order_type'] == 'Service')
                            <span class="label label-info">
                                <i class="fa fa-certificate"> Service</i>
                            </span>
                        @else
                            <span class="label label-primary">
                                <i class="fa fa-support"> Support</i>
                            </span>
                        @endif
                    </td>

                    <td>
                        @if($review->created_by == 'Customer')
                            <span class="label label-info">
                                <i class="fa fa-user"> Customer</i>
                            </span>
                        @else
                            <span class="label label-primary">
                                <i class="fa fa-user-circle"> Driver</i>
                            </span>
                        @endif
                    </td>

                    <td>
                        {{ $review['customer_user']->name }} ({{$review['customer_user']->phone_number}})
                    </td>

                    <td>
    <a href="{{ $review['customer_user_detail']->profile_pic_url }}" title="{{ $review['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
        <img src="{{$review['customer_user_detail']->profile_pic_url}}/150/150"  class="img-circle">
    </a>
                    </td>

                    <td>
                        {{ $review['driver_user']->name }} ({{$review['driver_user']->phone_number}})
                    </td>

                    <td>
    <a href="{{ $review['driver_user_detail']->profile_pic_url }}" title="{{ $review['driver_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
        <img src="{{$review['driver_user_detail']->profile_pic_url}}/150/150"  class="img-circle">
    </a>
                    </td>

                    <td>
                        {{ $review->ratings }}
                    </td>

                    <td>
                        <div style="height:100px;overflow:auto;">
                            {{ $review->comments }}
                        </div>
                    </td>

                    <td>
            <a href="{{ route('admin_order_details', ['order_id' => $review->order_id] ) }}" class="btn btn-primary pull-right" id="Details" title="Details">
                <i class="fa fa-ioxhost"> Order Details</i>
            </a>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $review->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>

        </div>
    </div>
</div>

    <script>

    $("#reviews").addClass("active");

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

        $('#daterange').on('apply.daterangepicker', function(ev, picker)
            {
                dt = document.getElementById('daterange').value;

                window.location.href = "{{route('admin_all_reviews')}}?daterange="+dt;
            });

        });

            var table = $('#example').dataTable( 
                {
                   "order": [[ 0, "desc" ]],
                   "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 4, 6, 9 ] },
                        { "bSearchable": true, "aTargets": [ 3, 6 ] }
                    ]
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });
            $("#filter2").on('change', function()
                {
                    table.fnFilter($(this).val(), 2);
                });

    </script>


@stop
