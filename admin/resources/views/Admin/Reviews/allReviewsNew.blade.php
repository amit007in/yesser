@extends('Admin.layout')

@section('title')
   All Reviews
@stop

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                       All Reviews
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_all_reviews')}}">
                                <b>
                                   All Reviews
                                </b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        <form method="post" action="{{route('admin_all_reviews')}}">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <meta name="csrf-token" content="{{ csrf_token() }}">

        <div class="col-lg-2 col-md-2">
            <div class="form-group">
                <label class="pull-left">Order Type</label>
                    <select class="form-control" id="order_filter" name="order_filter">
                        <option value="All">All</option>
                        <option value="Service">Service</option>
                        <!--<option value="Support">Support</option>-->
                    </select>                 
            </div>
        </div>

        <div class="col-lg-4 col-md-4">
            <label>Filter Reviewed At</label>
            <center>
                <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="<?php echo $data['fstarting_dt'].' - '.$data['fending_dt'] ?>"/>
            </center>
        </div>

        <div class="col-lg-2 col-md-2">
            <div class="form-group">
                <label class="pull-left">CreatedBy</label>
                    <select class="form-control" id="createdby_filter" name="createdby_filter">
                        <option value="All">All</option>
                        <option value="Customer">Customer</option>
                        <option value="Driver">Yesser Man</option>
                    </select>
            </div>
        </div>

        <div class="col-lg-2 col-md-2">
            <label>Search</label>
                <input type="text" name="search" class="form-control" placeholder="Search" value="{{$data['search']}}">
        </div>

        <div class="col-lg-2 col-md-2">
            <label>Filter</label><br>
                <button type="submit" class="btn btn-primary btn-big">Filter Results</button>
                <!-- <a class="btn btn-success btn-big" onclick="export_excell()">Export Excel</a> -->
        </div>

        </form>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Order Type</th>
                                    <th>CreatedBy</th>
                                    <th>Customer</th>
                                    <th>CustomerPic</th>
                                    <th>Yesser Man</th>
                                    <th>Yesser Man Pic</th>
                                    <th>Ratings</th>
                                    <th>Comments</th>
                                    <th>Details</th>
                                    <th>Reviewed At</th>
                                </tr>

                                </thead>
                                <tbody>

            @foreach($reviews as $review)
                <tr class="gradeA footable-odd" style="display: table-row;">

                    <td>{{ $review->order_rating_id }}</td>

                    <td>
                        @if($review->order_type == 'Service')
                            <span class="label label-info">
                                <i class="fa fa-certificate"> Service</i>
                            </span>
                        @else
                            <span class="label label-primary">
                                <i class="fa fa-support"> Support</i>
                            </span>
                        @endif
                    </td>

                    <td>
                        @if($review->created_by == 'Customer')
                            <span class="label label-info">
                                <i class="fa fa-user"> Customer</i>
                            </span>
                        @else
                            <span class="label label-primary">
                                <i class="fa fa-user-circle"> Yesser Man</i>
                            </span>
                        @endif
                    </td>

                    <td>
                        {{ $review->cust_name }} ({{$review->cust_phone_number}})
                    </td>

                    <td>
					@if($review->cust_profile_pic == "")
						<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $review->cust_name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
						</a>
					 @else
						<a href="{{ env('SOCKET_URL').'/Uploads/'.$review->cust_profile_pic }}" title="{{ $review->cust_name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ env('SOCKET_URL').'/Uploads/'.$review->cust_profile_pic }}"  class="img-circle">
						</a>
					 @endif
   
                    </td>

                    <td>
                        {{ $review->driver_name }} ({{$review->driver_phone_number}})
                    </td>

                    <td>
					@if($review->driver_profile_pic == "")
						<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $review->driver_name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
						</a>
					 @else
						<a href="{{ env('SOCKET_URL').'/Uploads/'.$review->driver_profile_pic }}" title="{{ $review->driver_name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ env('SOCKET_URL').'/Uploads/'.$review->driver_profile_pic }}"  class="img-circle">
						</a>
					 @endif

                    </td>

                    <td>
                        {{ $review->ratings }}
                    </td>

                    <td>
                        <div style="height:100px;overflow:auto;">
                            {{ $review->comments }}
                        </div>
                    </td>

                    <td>
            <a href="{{ route('admin_order_details', ['order_id' => $review->order_id] ) }}" class="btn btn-primary pull-right" id="Details" title="Details">
                <i class="fa fa-ioxhost"> Order Details</i>
            </a>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $review->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                                
                                <tfoot>
                <center>
                    <h2>Total - {{ $reviews->total() }}</h2>
                </center>
                <center>
        {{ $reviews->appends(['order_filter' => $data['order_filter'], 'createdby_filter' => $data['createdby_filter'], 'search'=>$data['search'], 'daterange' => $data['fstarting_dt'].' - '.$data['fending_dt']  ])->links() }}
                </center>
                                </tfoot>

                            </table>

                            </div>
                        </div>
                    </div>

</div>

        </div>
    </div>
</div>

    <script>

    $("#reviews").addClass("active");

/////////////       Filters     ///////////////////////
        $('#order_filter').val('{{$data["order_filter"]}}');
        $('#createdby_filter').val('{{$data["createdby_filter"]}}');
/////////////       Filters     ///////////////////////

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                opens: 'right',
                autoApply: true,
                format: 'DD/MM/YYYY',
                minDate: '1/2/2018',
                maxDate: moment().format('DD/MM/YYYY'),
            }, function(start, end, label) {
                //console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });

        });

    </script>


@stop
