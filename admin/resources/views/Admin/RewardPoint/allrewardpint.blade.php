
    @extends('Admin.layout')

@section('title')
    All Reward Point
@stop

@section('content')

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All Reward Point
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_rewardpoint_all')}}"><b>All Reward Point</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="row">
                <div class="ibox float-e-margins" >
				@if(count($RewardPoint) < 1)
                <div class="row">
                    <div class="col-lg-2 col-md-3 col-sm-3" style="float: right;margin-bottom: 10px;">
                        <br>
                        <a href="{{route('admin_rewardpoint_aget')}}" class="btn btn-primary pull-right" id="Add Reward Point" title="Add Reward Point">
                            <i class="fa fa-plus"></i> Add Reward Point
                        </a>
                    </div>
                </div>
                @endif
                    <div class="ibox-content table-responsive" style="height: 182px !important;">
                        <div class="">
                            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Status</th>
                                        <!--<th>Send Invite</th>-->
										<th>Action</th>
                                        <th>Reward point per invitation</th>
                                        <th>Reedemption Reward Point</th>
                                        <th>Reedemption Reward Price (OMR)</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($RewardPoint as $RewardPoint)
                                    <tr class="gradeA footable-odd" style="display: table-row;">
                                        <td>
                                            {{ $RewardPoint->reward_id }}
                                        </td>
                                        <td>
                                            <center>
                                                @if($RewardPoint->status != '0')
                                                    <span class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i> Active</span>
                                                @else
                                                    <span class="label label-danger"><i class="fa fa-eye-slash" aria-hidden="true"></i> Blocked</span>
                                                @endif
                                            </center>
                                        </td>
                                        <!--<td>
                                            {{ $RewardPoint->send_invite }}
                                        </td>-->
										<td>
                                            <div class="dropdown">
                                                <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                                    Actions
                                                <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                    <li role="presentation">
                                                        <a role="menuitem" tabindex="-1" href="{{ route('admin_rewardpoint_uget', ['reward_id'=>$RewardPoint->reward_id]) }}">
                                                            <i class="fa fa-edit"> Update</i>
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        @if($RewardPoint->status != '0')
                                                            <a role="menuitem" tabindex="-1" class="block_cust" id="{{ $RewardPoint->reward_id }}">
                                                                <i class="fa fa-eye-slash"> Block</i>
                                                            </a>
                                                        @else
                                                            <a role="menuitem" tabindex="-1" class="unblock_cust" id="{{ $RewardPoint->reward_id }}">
                                                                <i class="fa fa-eye"> Unblock</i>
                                                            </a>
                                                        @endif
                                                    </li>      
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            {{ $RewardPoint->add_point }}
                                        </td>
										<td>
                                            {{ $RewardPoint->reedemption_reward }}
                                        </td>
										<td>
                                            {{ $RewardPoint->reedemption_price }}
                                        </td>
                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#settings_all").addClass("active");
    $("#rewardpoint_all").addClass("active");
	$('.block_cust').click(function (e) {
		var id     = $(e.currentTarget).attr("id");
		var status = 'dd';
		swal({
			title: "Are you sure you want to de-activate this reward point?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, De-Active It!",
			closeOnConfirm: false
		}, function () {
			window.location.href = '{{route("admin_rewardpoint_active")}}?block=0&rewardId='+id+'&status='+status;
		});
	});
	$('.unblock_cust').click(function (e) {
		var id = $(e.currentTarget).attr("id");
		var status = 'dd';
		swal({
			title: "Are you sure you want to activate this reward point?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, Active It!",
			closeOnConfirm: false
		}, function () {
			window.location.href = '{{route("admin_rewardpoint_active")}}?block=1&rewardId='+id+'&status='+status;
		});
	});
  
</script>


@stop
