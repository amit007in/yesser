@extends('Admin.layout')

@section('title') 
    Edit Reward Point
@stop

@section('content')
    <link href="{{ URL::asset('AdminAssets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('AdminAssets/css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-xs-12">
                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>Edit Reward Point</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{route('admin_dashboard')}}">Dashboard</a>
                            </li>
                            <li>
                                <a href="{{route('admin_rewardpoint_all')}}">All Reward Point</a>
                            </li>
                            <li>
                                <a href="#">
                                    <b>Edit Reward Point</b>
                                </a>
                            </li>
                        </ol>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <br>
                    <form method="post" action="{{route('admin_rewardpoint_upost') }}" enctype="multipart/form-data" id="addForm">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="reward_id" id="reward_id" value="{!! $rewarddetails->reward_id !!}">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
						<label>For Invitation Reward Point</label>
						<br>
                        <div class="row">
                            <div class="form-group">
                                <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                    <label>Send Invitation Per Reward Point</label>
                                    <input type="number" placeholder="Send Invitation" autocomplete="off" class="form-control" required name="send_invite" value="1" autofocus="on" id="send_invite" readonly>
                                </div> 
                                <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                    <label>Reward Point</label>
                                    <input type="number" placeholder="Reward Point" autocomplete="off" class="form-control" required name="reward_point" value="{!! $rewarddetails->add_point !!}" id="reward_point">
                                </div> 
                            </div>
                        </div>
                        <br>
						 <br>
						<label>For Reedemption Reward Point</label>
						<br>
						<div class="row">
                            <div class="form-group">
                                <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                                    <label>Reedemption Reward Point</label>
                                    <input type="number" placeholder="Reedemption Reward Point" autocomplete="off" class="form-control" required name="reedemption_reward_point" value="{!! $rewarddetails->reedemption_reward !!}" autofocus="on" id="reedemption_reward_point">
                                </div> 
                                <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                                    <label>Reward Price Value</label>
                                    <input type="number" placeholder="Reward Price" autocomplete="off" class="form-control" required name="reward_price" value="{!! $rewarddetails->reedemption_price !!}" id="reward_price">
                                </div> 
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="form-group">
                                <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                                    {!! Form::submit('Update Reward Point', ['class' => 'btn btn-success']) !!}
                                </div>
                            </div>
                        </div>                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script type="text/javascript">
        $("#settings_all").addClass("active");
        $("#rewardpoint_all").addClass("active");
        $("#addForm").validate({
            rules: {
                send_invite: {
                    required: true,
                },
                reward_point: {
                    required: true,
                },
                category_id: {
                    required: true,
                },
                brandids: {
                    required: true,
                },
                productids: {
                    required: true,
                }
            },
            messages: {
                send_invite: {
                    required: "Please provide the send invitation"
                },
                reward_point: {
                    required: "Please provide the reward point"
                },
                category_id: {
                    required: "Please select the category"
                },
                brandids: {
                    required: "Please select the category brand"
                },
                productids: {
                    required: "Please select the category brand product"
                }
            }
        });
    </script>

@stop



