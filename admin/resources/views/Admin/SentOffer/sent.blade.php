@extends('Admin.layout')

@section('title')
    All Sent Offers
@stop

@section('content')

<link href="{{ URL::asset('AdminAssets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
<script src="{{ URL::asset('AdminAssets/js/plugins/select2/select2.full.min.js') }}"></script>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-xs-12">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All Sent Offers
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_release_offer') }}">
                                <b>
                                    All Sent Offers
                                </b>
                            </a>
                        </li>
                    </ol>
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Status</th>
                                        <th>Driver</th>
                                        <th>DriverPic</th>
                                        <th>Release Timing</th>
                                        <th>Action</th>
                                        <!--<th>Margin</th>-->
                                        <th>Offer Price</th>
                                        <th>Pickup Location</th>
                                        <th>DropOff Location</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($offers as $offer)
                                            <tr class="gradeA footable-odd">
                                                <td>{{ $offer->release_offer_id }}</td>
                                                <td>
                                                    <span class="label label-primary">
                                                        {{ $offer->status }}
                                                    </span>
                                                </td>
                                                <td>{{ $offer->name }}</td>
												<td>
                                                @if($offer->profile_pic == "")
												<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $offer->name }}" class="lightBoxGallery img-circle" data-gallery="">
													<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
												</a>
											 @else
												<a href="{{ "http://phytotherapy.in:8051/Uploads/".$offer->profile_pic }}" title="{{ $offer->name }}" class="lightBoxGallery img-circle" data-gallery="">
													<img src="{{ "http://phytotherapy.in:8051/Uploads/".$offer->profile_pic }}"  class="img-circle">
												</a>
											 @endif
											 </td>
                                                <td><i class="fa fa-clock-o"></i> {{ $offer->created_atz }}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                                            Actions
                                                        <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                            <li role="presentation">
                                                                <a role="menuitem" tabindex="-1" id="admin_approve_offer" onclick="show_popup('Approve','{{$offer->release_offer_id}}')">
                                                                    <i class="fa fa-thumbs-up"></i> Approved
                                                                </a>
                                                            </li>
                                                            <li role="presentation">
                                                                <a role="menuitem" tabindex="-1" id="admin_reject_offer" onclick="show_popup('Reject','{{$offer->release_offer_id}}')">
                                                                    <i class="fa fa-thumbs-down"></i> Reject
                                                                </a>
                                                            </li>
                                                            <!--<li role="presentation">
                                                                <a role="menuitem" tabindex="-1" id="admin_reject_offer" onclick="Writemargin('{{$offer->release_offer_id}}')">
                                                                    <i class="fa fa-plus"></i> Add Margin
                                                                </a>
                                                            </li>-->
                                                        </ul>
                                                    </div>
                                                </td>
                                               <!-- <td>{{ $offer->margin }}</td>-->
                                                <td><b>OMR</b> {{ $offer->offer_price }}</td>
                                                <td style="height:100px;overflow:auto;">
                                                    {!! $offer->start_address !!}
                                                </td>
                                                <td style="height:100px;overflow:auto;">
                                                    {{ $offer->end_address }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <center>
                                            <h2>Total - {{ $offers->total() }}</h2>
                                        </center>
                                        <center>
                                            {{ $offers->links() }}
                                        </center>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $("#releaseoffer").addClass("active");
        function show_popup(type, id)
        {
            if(type == 'Approve')
            {
                var title = "Are you sure you want to approve this offer?";
                var route = '{{route("admin_approve_offer")}}?approve=Approve&id='+id;
            }
            else
            {
                var title = "Are you sure you want to reject this offer?";
                var route = '{{route("admin_approve_offer")}}?approve=Reject&id='+id;
            }
            
            swal({
                title: title,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Do it!",
                closeOnConfirm: false
            }, function () {
                window.location.href = route;
            });
        }

        function Writemargin(id)
        {
            swal({
                title: "Add Margin",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Add Margin"
            },
            function(inputValue){
				console.log(inputValue);
				if (inputValue === false) return false;
				if($.isNumeric(inputValue))
				{
					if (inputValue === "") {
						swal.showInputError("You need to write something!");
						return false;
					}
					var route = '{{route("admin_update_margin")}}?margin='+inputValue+'&id='+id;
					window.location.href = route;
				}else
				{
					swal.showInputError("You need to write only number!");
				}
			});
        }
    </script>
@stop
