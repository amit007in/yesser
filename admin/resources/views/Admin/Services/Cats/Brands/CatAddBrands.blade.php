@extends('Admin.layout')

@section('title') 
    Add Category
@stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add Category</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_service_cat_all')}}">All Service Categories</a>
                        </li>
                        <li>
                            <a href="{{route('admin_service_cat_brands', ['category_id'=>$category->category_id] )}}">
                                Category
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin_service_cat_add', ['category_id'=>$category->category_id] ) }}">
                                <b>Add Category</b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

            <form method="post" action="{{route('admin_service_cat_add_post', ['category_id'=>$category->category_id] ) }}" enctype="multipart/form-data" id="addForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="row">
        <div class="col-md-6 col-lg-offset-3">
            <div class="ibox-content text-center">
                <h2> Service - {{ $category->category_details[0]->name }}</h2>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>English Name</label>
                <input type="text" placeholder="English Name" autocomplete="off" class="form-control" required name="english_name" value="{!! Form::old('english_name') !!}" autofocus="on" id="english_name">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>English Image (150px X 150px)</label>
                <input type="file" name="english_image" class="form-control" accept="image/*" required id="english_image">
            </div> 

        </div>
    </div>
<!--<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Hindi Name</label>
                <input type="text" placeholder="Hindi Name" autocomplete="off" class="form-control" required name="hindi_name" value="{!! Form::old('hindi_name') !!}"id="hindi_name">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Hindi Image (150px X 150px)</label>
                <input type="file" name="hindi_image" class="form-control" accept="image/*" id="hindi_image">
            </div> 

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Urdu Name</label>
                <input type="text" placeholder="Urdu Name" autocomplete="off" class="form-control" required name="urdu_name" value="{!! Form::old('urdu_name') !!}" id="urdu_name">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Urdu Image (150px X 150px)</label>
                <input type="file" name="urdu_image" class="form-control" accept="image/*" id="urdu_image">
            </div> 

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Chinese Name</label>
                <input type="text" placeholder="Chinese Name" autocomplete="off" class="form-control" required name="chinese_name" value="{!! Form::old('chinese_name') !!}" id="chinese_name">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Chinese Image (150px X 150px)</label>
                <input type="file" name="chinese_image" class="form-control" accept="image/*" id="chinese_image">
            </div> 

        </div>
    </div>-->
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Arabic Name</label>
                <input type="text" placeholder="Arabic Name" autocomplete="off" class="form-control" required name="arabic_name" value="{!! Form::old('arabic_name') !!}" id="arabic_name">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Arabic Image (150px X 150px)</label>
                <input type="file" name="arabic_image" class="form-control" accept="image/*" id="arabic_image">
            </div> 

        </div>
    </div>
	<br>
    <div class="row">
        <div class="form-group">
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Start Time</label>
                <input date-range-picker type="text" step="any" required placeholder="Start Time" class="form-control" name="start_time" value="{!! Form::old('start_time') !!}" id="start_time" readonly options="dateRangeOptions">
            </div>
            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>End Time</label>
                <input date-range-picker id="end_time" value="{!! Form::old('end_time') !!}" required placeholder="End Time" name="end_time" class="form-control" type="text" readonly options="dateRangeOptions"/>
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="form-group">

            <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1">
                <label for="ranking">Ranking</label>
                <select class="form-control border-info" required name="ranking">
                    @for($i=1;$i<=$ranking+1;$i++)
                        <option value="{{$i}}" selected="true">{{$i}}</option>
                    @endfor
                </select>
            </div>
			<div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Maximum Radius</label><br>
                <label>Should Use YesSer Radius by Default ?</label>  <input type="checkbox" id="myCheck" name="myCheck" value="1"  onclick="myFunction()">
                <input type="number" step="any"  min="1" placeholder="Maximum Radius" class="form-control" name="maximum_radius" id="maximum_radius">
            </div> 
<!--             <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Buraq Charges Percentage</label>
                <input type="number" step="any" max="100" min="0.1" placeholder="Buraq Charges%" class="form-control" required name="buraq_percentage" value="{!! Form::old('buraq_percentage') !!}" id="buraq_percentage">
            </div> -->
            <input type="hidden" name="buraq_percentage" value="0">

        </div>
    </div>

<input type="hidden" name="total" value="{{$ranking}}">

            <br><br><br>

                <div class="row">
                    <div class="form-group">
                        <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            {!! Form::submit('Add Category', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>

<script type="text/javascript">

        $("#services_all").addClass("active");
        $("#service_cat_all").addClass("active");

        $("#addForm").validate({
            rules: {
                english_name: {
                    required: true,
                    maxlength: 255,
                },
                english_image: {
                    required: true,
                    accept: "image/*"
                },
               /*hindi_name: {
                    required: true,
                    maxlength: 255,
                },
                hindi_image: {
                    required: true,
                    accept: "image/*"
                },
                urdu_name: {
                    required: true,
                    maxlength: 255,
                },
                urdu_image: {
                    required: true,
                    accept: "image/*"
                },
                chinese_name: {
                    required: true,
                    maxlength: 255,
                },
                chinese_image: {
                    required: true,
                    accept: "image/*"
                },*/
                arabic_name: {
                    required: true,
                    maxlength: 255,
                },
                arabic_image: {
                    required: true,
                    accept: "image/*"
                },
                buraq_percentage: {
                    required: true,
                    minimun: 0.01,
                    maximum: 100
                },
                start_time: {
                    required: true
                },
                end_time: {
                    required: true
                },
                maximum_radius: {
                    required: true
                }
 
            },
            messages: {
                english_name: {
                    required: "Please provide the english name for this category"
                },
                english_image:{
                    required: "Please provide the english image for this category",
                    accept: "Only Image is allowed"
                },
                /*hindi_name: {
                    required: "Please provide the hindi name for this brand"
                },
                hindi_image:{
                    required: "Please provide the hindi image for this brand",
                    accept: "Only Image is allowed"
                },
                urdu_name: {
                    required: "Please provide the urdu name for this brand"
                },
                urdu_image:{
                    required: "Please provide the urdu image for this brand",
                    accept: "Only Image is allowed"
                },
                chinese_name: {
                    required: "Please provide the chinese name for this brand"
                },
                chinese_image:{
                    required: "Please provide the chinese image for this brand",
                    accept: "Only Image is allowed"
                },*/
                arabic_name: {
                    required: "Please provide the arabic name for this category"
                },
                arabic_image:{
                    required: "Please provide the arabic image for this category",
                    accept: "Only Image is allowed"
                },
                buraq_percentage: {
                    required: "Buraq Percentage charge is required"
                },
                start_time: {
                    required: "Please provide the start time"
                },
                end_time: {
                    required: "Please provide the end time"
                },
                maximum_radius: {
                    required: "Please provide the maximum radius"
                }


            }
        });
		$(document).ready(function()
        {
            $('input[name="start_time"]').daterangepicker({
                timePicker : true,
                format : 'hh:mm A',
                singleDatePicker: true
            });
            $('input[name="end_time"]').daterangepicker({
                timePicker : true,
                format : 'hh:mm A',
                singleDatePicker: true
            });
        });
		function myFunction() 
        {
            var checkBox = document.getElementById("myCheck");
            if (checkBox.checked == true)
            {
                $('#maximum_radius').attr('disabled',true);
            } else {
                $('#maximum_radius').attr('disabled',false);
            }
        }
</script>
<style>
    .calendar-date{
        display: none;
    }
</style>

@stop



