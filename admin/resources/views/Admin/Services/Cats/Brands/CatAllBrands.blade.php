    @extends('Admin.layout')

    @section('title')
        Category Brands
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Category
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_service_cat_all')}}">All Service Categories</a>
                        </li>
                        <li>
                            <a href="{{route('admin_service_cat_brands', ['category_id'=>$category->category_id] )}}">
                                <b>Category</b>
                            </a>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-md-6 col-lg-offset-3">
            <div class="ibox-content text-center">
                <h2> Service - {{ $category->category_details[0]->name }}</h2>
            </div>
        </div>
    </div>
<br>

                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="form-group">
                            <label class="pull-left">Status</label>
                                <select class="form-control" id="filter1">
                                    <option value="e">All</option>
                                    <option value="Blocked">Blocked</option>
                                    <option value="Active">Active</option>
                                </select>                 
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6">
                                    <label>
                                        Filter Updated At
                                    </label>

                                    <center>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
                                    </center>                        
                    </div>

                    @if($category->default_brands == "0")
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <br>
                            <a href="{{ route('admin_service_cat_add', ['category_id'=>$category->category_id] ) }}" class="btn btn-primary pull-right" id="Brand Add" title="Add">
                                <i class="fa fa-plus"></i> Add New Category
                            </a>
                        </div>
                    @endif

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Service Id</th>
                                    <th>Status</th>
                                    <th>Ranking</th>
                                    <th>English Image</th>
                                    <th>Name</th>
                                    <!-- <th>Buraq Charges%</th> -->
									<th>Radius</th>
                                    <th>Action</th>
                                    <th>Products</th>
									<th>Start Time</th>
                                    <th>End Time</th>
                                    <th>Updated At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($brands as $brand)
                <tr class="gradeA footable-odd">

                    <td>{{ $brand->category_brand_id }}</td>

                    <td>
                        <center>
                            @if($brand->blocked == '0')
                                <span class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i> Active</span>
                            @else
                                <span class="label label-danger"><i class="fa fa-eye-slash" aria-hidden="true"></i> Blocked</span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            <span class="label label-success"> {{$brand->sort_order}} </span>    
                        </center>
                    </td>

                    <td>

                        @if($brand->category_brand_details[0]->image != '')
                            <a href="{{ $brand->category_brand_details[0]->image_url }}" title="{{ $brand->category_brand_details[0]->name }}" class="lightBoxGallery" data-gallery="">
                                <img src="{{ $brand->category_brand_details[0]->image_url }}"  class="img-circle">
                            </a>
                        @endif
                    </td>

                    <td>
                        <b>
                            {{ $brand->category_brand_details[0]->name }}
                        </b>(English)<hr>

                        <!--<b>
                            {{ $brand->category_brand_details[1]->name }}
                        </b>(Hindi)<hr>

                        <b>
                            {{ $brand->category_brand_details[2]->name }}
                        </b>(Urdu)<hr>

                        <b>
                            {{ $brand->category_brand_details[3]->name }}
                        </b>(Chinese)<hr>-->

                        <b>
                            {{ $brand->category_brand_details[4]->name }}
                        </b>(Arabic)<hr>
                    </td>

<!--                     <td>{{ $brand->buraq_percentage }}%</td>                     -->
					<td>{{ $brand->maximum_radius }}</td>
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="{{ route('admin_service_brand_update', ['category_brand_id' => $brand->category_brand_id] ) }}">
                                        <i class="fa fa-edit"> Update</i>
                                    </a>
                                </li>

                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="{{ route('admin_brand_products_all', ['category_brand_id' => $brand->category_brand_id] ) }}">
                                        <i class="fa fa-product-hunt"> All Products</i> - {{$brand->product_counts}}
                                    </a>
                                </li>

                                <li role="presentation">
                                    @if($brand->blocked == '0')
                                        <a role="menuitem" tabindex="-1" class="block_brand" id="{{$brand->category_brand_id}}">
                                            <i class="fa fa-eye-slash"> Block</i>
                                        </a>
                                    @else
                                        <a role="menuitem" tabindex="-1" class="unblock_brand" id="{{$brand->category_brand_id}}">
                                            <i class="fa fa-eye"> Unblock</i>
                                        </a>
                                    @endif
                                </li>

                            </ul>
                        </div>
                    </td>

                    <td>
                        <a class="btn btn-default" href="{{ route('admin_brand_products_all', ['category_brand_id' => $brand->category_brand_id] ) }}">
                            {{$brand->product_counts}}
                        </a>                        
                    </td>
					<td><i class="fa fa-clock-o"></i> {{$brand->start_time}}</td>
                    <td><i class="fa fa-clock-o"></i> {{$brand->end_time}} </td>

                    <td><i class="fa fa-clock-o"></i> {{ $brand->updated_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

        $("#services_all").addClass("active");
        $("#service_cat_all").addClass("active");

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

        $('#daterange').on('apply.daterangepicker', function(ev, picker)
            {

                dt = document.getElementById('daterange').value;

                window.location.href = '{{route("admin_service_cat_brands")}}?category_id={{$category->category_id}}&daterange='+dt;

            });

        });

    //$('#reportrange span').html('{{$fstarting_dt}}' + ' - ' + '{{$fending_dt}}');
 

            var table = $('#example').dataTable( 
                {
                   "order": [[ 2, "asc" ]],
                   //"scrollY": 1000,
                   //"scrollX": true,
                   "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [ 3, 6 ] },
                                { "bSearchable": true, "aTargets": [ 0, 4 ] }
                            ],
                   "fnDrawCallback": function( oSettings ) 
                        {


        $('.block_brand').click(function (e) {

              var id = $(e.currentTarget).attr("id");

                swal({
                    title: "Are you sure you want to block this brand?",
                    text: "Users will not be able to access this brand.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Block it!",
                    closeOnConfirm: false
                }, function () {
                    window.location.href = '{{route("admin_brand_block")}}?block=1&category_brand_id='+id;
                });
            });

        $('.unblock_brand').click(function (e) {

                 var id = $(e.currentTarget).attr("id");

                swal({
                    title: "Are you sure you want to unblock this brand?",
                    text: "Users will be able to access this brand.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Unblock It!",
                    closeOnConfirm: false
                }, function () {
            window.location.href = '{{route("admin_brand_block")}}?block=0&category_brand_id='+id;
                });
            });

                        }
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });

    </script>


@stop
