@extends('Admin.layout')

@section('title') 
    Add Category Product
@stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add Category Product</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_service_cat_all')}}">All Service Categories</a>
                        </li>
                        <li>
                            <a href="{{route('admin_service_cat_brands', ['category_id'=>$brand->category_id] )}}">
                                Category
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin_brand_products_all', ['category_brand_id'=>$brand->category_brand_id] )}}">
                                Category Products
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin_product_add', ['category_id'=>$brand->category_brand_id, 'category_brand_id'=>$brand->category_brand_id] ) }}">
                                <b>Add Category Product</b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-md-6 col-lg-offset-3">
            <div class="ibox-content text-center">
                <h2>{{ $brand->category_brand_details[0]->name }}</h2>
                    @if($brand->category_brand_details[0]->image != '')
                        <div class="m-b-sm">
                            <a href="{{ $brand->category_brand_details[0]->image_url }}" title="{{ $brand->category_brand_details[0]->name }}" class="lightBoxGallery" data-gallery="">
                                <img src="{{ $brand->category_brand_details[0]->image_url }}" class="img-circle " alt="profile" >
                            </a>
                        </div>
                    @endif
            </div>
        </div>
    </div>
<br>

            <form method="post" action="{{route('admin_product_add_post', ['category_brand_id'=>$brand->category_brand_id] ) }}" enctype="multipart/form-data" id="addForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="category_id" value="{{$brand->category_id}}">
                <input type="hidden" name="total" value="{{$ranking}}">

    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>English Name</label>
                <input type="text" placeholder="English Name" maxlength="255" class="form-control" required name="english_name" value="{!! Form::old('english_name') !!}" autofocus="on" id="english_name" maxlength="255">
            </div>

            <div class='col-lg-5 col-md-5 col-sm-5'>
                <label>English Description</label>
                <textarea class="form-control" rows="2" placeholder="English Description" name="english_description">{{Form::old('english_description')}}</textarea>
            </div>

        </div>
    </div>
<!--<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>Hindi Name</label>
                <input type="text" placeholder="Hindi Name" class="form-control" required name="hindi_name" value="{!! Form::old('hindi_name') !!}" id="hindi_name">
            </div>

            <div class='col-lg-5 col-md-5 col-sm-5'>
                <label>Hindi Description</label>
                <textarea class="form-control" rows="2" placeholder="Hindi Description" name="hindi_description">{{Form::old('hindi_description')}}</textarea>
            </div>

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>Urdu Name</label>
                <input type="text" placeholder="Urdu Name" class="form-control" required name="urdu_name" value="{!! Form::old('urdu_name') !!}" id="urdu_name">
            </div>

            <div class='col-lg-5 col-md-5 col-sm-5'>
                <label>Urdu Description</label>
                <textarea class="form-control" rows="2" placeholder="Urdu Description" name="urdu_description">{{Form::old('urdu_description')}}</textarea>
            </div>

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>Chinese Name</label>
                <input type="text" placeholder="Chinese Name" class="form-control" required name="chinese_name" value="{!! Form::old('chinese_name') !!}" id="chinese_name">
            </div>

            <div class='col-lg-5 col-md-5 col-sm-5'>
                <label>Chinese Description</label>
                <textarea class="form-control" rows="2" placeholder="Chinese Description" name="chinese_description">{{Form::old('chinese_description')}}</textarea>
            </div>

        </div>
    </div>-->
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>Arabic Name</label>
                <input type="text" placeholder="Arabic Name" maxlength="255" class="form-control" required name="arabic_name" value="{!! Form::old('arabic_name') !!}" id="arabic_name">
            </div>

            <div class='col-lg-5 col-md-5 col-sm-5'>
                <label>Arabic Description</label>
                <textarea class="form-control" rows="2" placeholder="Arabic Description" name="arabic_description">{{Form::old('arabic_description')}}</textarea>
            </div>

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">

            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>Actual Unit</label>
                <input type="number" step="any" min="0" placeholder="Actual Unit" class="form-control" required name="actual_value" value="{!! Form::old('actual_value') !!}" id="actual_value">
            </div>

            <div class='col-lg-5 col-md-5 col-sm-5'>
                <label for="ranking">Ranking</label>
                <select class="form-control border-info" required name="ranking">
                    @for($i=1;$i<=$ranking+1;$i++)
                        <option value="{{$i}}" selected="true">{{$i}}</option>
                    @endfor
                </select>
            </div>

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">

            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>Alpha Charges</label>
                <input type="number" step="any" min="0" placeholder="Alpha Charges" class="form-control" required name="alpha_price" value="{!! Form::old('alpha_price') !!}" id="alpha_price">
            </div>


            <div class='col-lg-5 col-md-5 col-sm-5'>
                <label>Price Per Quantity</label>
                <input type="number" step="any" min="0" placeholder="Price Per Quantity" class="form-control" required name="price_per_quantity" value="{!! Form::old('price_per_quantity') !!}" id="price_per_quantity">
            </div>

        </div>
    </div>

    <br>
    <div class="row">
        <div class="form-group">

            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>Price Per Distance</label>
                <input type="number" step="any" min="0" required placeholder="Price Per Distance" class="form-control" name="price_per_distance" value="{!! Form::old('price_per_distance') !!}" id="price_per_distance">
            </div>

            <div class='col-lg-5 col-md-5 col-sm-5'>
                <label>Price Per Weight</label>
                <input type="number" step="any" min="0" required placeholder="Price Per Weight" class="form-control" name="price_per_weight" value="{!! Form::old('price_per_weight') !!}" id="price_per_weight">
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="form-group">

            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>Price Per Hour</label>
                <input type="number" step="any" min="0" required placeholder="Price Per Hour" class="form-control" name="price_per_hr" value="{!! Form::old('price_per_hr') !!}" id="price_per_hr">
            </div>

            <div class='col-lg-5 col-md-5 col-sm-5'>
                <label>Price Per Sq Meter</label>
                <input type="number" step="any" min="0" required placeholder="Price Per Sq Meter" class="form-control" name="price_per_sq_mt" value="{!! Form::old('price_per_sq_mt') !!}" id="price_per_sq_mt">
            </div>

        </div>
    </div>
	<!--@if($brand->category_id == '2')
    <br>
    <div class="row">
        <div class="form-group">
            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>MOQ For Gift (Cannot be less than 1)</label>
                <input type="number" step="any" min="1" placeholder="Minimum Order Quantity for Gift to be eligible" class="form-control" name="gift_quantity" value="{!! Form::old('gift_quantity ') !!}" id="gift_quantity">
            </div>
            <div class='col-lg-5 col-md-5 col-sm-5'>
				<label>Gift Offer (English)</label>
                <input type="text" step="any" placeholder="Gift Pack to Offer" class="form-control" name="gift_offer" value="{!! Form::old('gift_offer') !!}" id="gift_offer">
			</div>
        </div>
    </div>
	<br>
    <div class="row">
        <div class="form-group">
            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>Gift Offer (Hindi)</label>
                <input type="text" step="any" placeholder="Gift Offer (Hindi)" class="form-control" name="gift_offer_hindi" value="{!! Form::old('gift_offer_hindi') !!}" id="gift_offer_hindi">
            </div>
            <div class='col-lg-5 col-md-5 col-sm-5'>
                <label>Gift Offer (Urdu)</label>
                <input type="text" step="any" placeholder="Gift Offer (Urdu)" class="form-control" name="gift_offer_urdu" value="{!! Form::old('gift_offer_urdu') !!}" id="gift_offer_urdu">
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="form-group">
            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>Gift Offer (Chinese)</label>
                <input type="text" step="any" placeholder="Gift Offer (Chinese)" class="form-control" name="gift_offer_chinese" value="{!! Form::old('gift_offer_chinese') !!}" id="gift_offer_chinese">
            </div>
            <div class='col-lg-5 col-md-5 col-sm-5'>
                <label>Gift Offer (Arabic)</label>
                <input type="text" step="any" placeholder="Gift Offer (Arabic)" class="form-control" name="gift_offer_arabic" value="{!! Form::old('gift_offer_arabic') !!}" id="gift_offer_arabic">
            </div>
        </div>
    </div>
    @endif-->
    <br>
    <div class="row">
        <div class="form-group">

            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>Image (150px X 150px)</label>
                <input type="file" name="image" class="form-control" accept="image/*" id="image">
            </div>
			<!--- Part M3 --->
			<div class='col-lg-5 col-md-5 col-sm-5'>
                <label>Product Margin Percentage</label></br>
				<label>Should Use yesSer Margin by Default ?</label>  <input type="checkbox" id="should_use_buraq_margin" name="should_use_buraq_margin" value="1"  onclick="myFunction()">
                <input type="number" step="any" min="0" required placeholder="Product Margin" class="form-control" name="product_margin" value="{!! Form::old('product_margin') !!}" id="product_margin">
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="form-group">

            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>Minimum Quantity</label>
                <input type="number" step="any" min="1" required placeholder="Minimum Quantity" class="form-control" name="min_quantity" value="{!! Form::old('min_quantity') !!}" id="min_quantity">
            </div>

            <div class='col-lg-5 col-md-5 col-sm-5'>
                <label>Maximum Quantity</label>
                <input type="number" step="any" min="0" required placeholder="Maximum Quantity" class="form-control" name="max_quantity" value="{!! Form::old('max_quantity') !!}" id="max_quantity">
            </div>

        </div>
    </div>
	<div id="append_geoinputs"></div>
	</br>
	<!--<div class="row">
		<div class="form-group">
			<div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Add Area Price Map</button>
			</div>
		</div>
	</div>-->
	<!--<button style="margin-left: 96px;float: left;" type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Add Area Price Map</button>-->
            <br><br>
                <div class="row">
                    <div class="form-group">
                        <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            {!! Form::submit('Add Product', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Select Location</h4>
			</div>
			<form id="geo_form">
				<div class="modal-body">
				<input type="hidden" name="geo_radius" id="geo_radius">
				<input type="hidden" name="geo_lat" id="geo_lat">
				<input type="hidden" name="geo_long" id="geo_long">
					<div class="row">
						<div class="col-md-12 modal_body_map">
							<div class="location-map" id="location-map">
								<div style="width: 600px; height: 400px;" id="map_canvas"></div>
							</div>
						</div>
					</div>
					</br>
					<div class="row">
						<div class="col-4 col-sm-6">
						<label>Area Name: </label> <input required type="text" name="geo_areaname" id="geo_areaname">
						</div>
					</div>
					</br>
					<div class="row">
						<div class="col-4 col-sm-6">
						<label>Price: </label> <input step="any" required type="number" name="geo_price" id="geo_price">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" id="model_cancel" data-dismiss="modal">Close</button>
					<button type="button" id="geo_fenceadd" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,drawing&key={{env('GOOGLE_MAP_KEY')}}"></script>
<script type="text/javascript">

        $("#services_all").addClass("active");
        $("#service_cat_all").addClass("active");

        $("#addForm").validate({

            messages: {
                english_name: {
                    required: "Please provide the english name",
                },
                /*hindi_name: {
                    required: "Please provide the hindi name",
                },
                urdu_name: {
                    required: "Please provide the urdu name",
                },
                chinese_name: {
                    required: "Please provide the chinese name",
                },*/
                arabic_name: {
                    required: "Please provide the arabic name",
                },
                actual_value: {
                    required: "Please provide the Unit",
                },
                alpha_price: {
                    required: "Please provice the alpha price",
                },
                price_per_quantity: {
                  required: "Please provide the price per quantity",
                },
                price_per_distance: {
                    required: "Please provide the price per distance",
                },
                price_per_weight: {
                    required: "Please provide the price per weight",
                },
                price_per_hr: {
                    required: "Please provide the price per hour",
                },
                price_per_sq_mt: {
                    required: "Please provide the price per square meter"
                },
                min_quantity: {
                    required: "Please provide minimun quantity for a order"
                },
                max_quantity: {
                    required: "Please provide maximum quantity for a order"
                }

            }
        });

</script>
<script type="text/javascript">
$(document).ready(function() {
	var map = null;
	var myMarker;
	var myLatlng;
	var circle;
	function initializeGMap(lat, lng) 
	{
		myLatlng = new google.maps.LatLng(lat, lng);
		var myOptions = {
			zoom: 5,
			zoomControl: true,
			center: myLatlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			scrollwheel: true,
			fullscreenControl: true
		};
		map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		//Draw Circle
		var drawingManager = new google.maps.drawing.DrawingManager({
			drawingControl: true,
			drawingControlOptions: {
				position: google.maps.ControlPosition.TOP_CENTER,
				drawingModes: [
				google.maps.drawing.OverlayType.CIRCLE]
			},
			circleOptions: {
				fillColor: '#FF0000',
				fillOpacity: 5,
				strokeWeight: 2,
				clickable: false,
				editable: true,
				zIndex: 1
			}
		});
		drawingManager.setMap(map);
		google.maps.event.addListener(drawingManager, 'circlecomplete', onCircleComplete);
	}
	function onCircleComplete(shape) 
	{
        if (shape == null || (!(shape instanceof google.maps.Circle))) return;

        if (circle != null) {
            circle.setMap(null);
            circle = null;
        }
        circle = shape;
		var radius = circle.getRadius();
		var km = radius / 1000;
		//alert(km.toFixed(1) + " km");
		var radius = Math.round(km);
		var lat = circle.getCenter().lat();
		var lng = circle.getCenter().lng();
        console.log('radius', radius);
        //console.log('lat', circle.getCenter().lat());
        //console.log('lng', circle.getCenter().lng());
		$('#geo_radius').val(radius);
		$('#geo_lat').val(lat);
		$('#geo_long').val(lng);
    }
  // Re-init map before show modal
  $('#myModal').on('show.bs.modal', function(event) {
	// $("#geo_fenceadd").trigger("reset");
    var button = $(event.relatedTarget);
    initializeGMap(21.4735, 55.9754);
    $("#location-map").css("width", "100%");
    $("#map_canvas").css("width", "100%");
  });
  // Trigger map resize event after modal shown
  $('#myModal').on('shown.bs.modal', function() {
	  $("#geo_form").trigger("reset");
    google.maps.event.trigger(map, "resize");
    map.setCenter(myLatlng);
  });
  
	$( "#geo_fenceadd" ).click(function() 
	{
		//var radius = Math.round($('#geo_radius').val());
		var radius = $('#geo_radius').val();
		if(radius == ""){
			alert('Please select location');
			return false;
		}
		var lat    = $('#geo_lat').val();
		var lng    = $('#geo_long').val();
		var price  = $('#geo_price').val();
		if(price == ""){
			alert('Please provide the location price');
			return false;
		}
		var area   = $('#geo_areaname').val();
		if(area == ""){
			alert('Please provide the location name');
			return false;
		}
		/*console.log(radius);
		var km = radius / 1000;
		alert(km.toFixed(1) + " km"); // 1613.8 km*/
		var TotalDv = $("#append_geoinputs > div").length; 
		var i = TotalDv + 1;
		$( "#model_cancel" ).trigger( "click" );
		var html = "";
		html += "<div id='remove_"+i+"' class='row'><label style='margin-left: 96px;'>Location Prices: </label>";
		html += "<div class='form-group'>";
		html += "<div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>";
		html += "<label>Area name</label>";
		html += "<input type='text' class='form-control' name='geofencing_area[]' value='"+area+"' id='geofencing_area[]' readonly>";
		html += "<input type='hidden' class='form-control' name='geofencing_lat[]' value='"+lat+"' id='geofencing_lat[]'>";
		html += "<input type='hidden' class='form-control' name='geofencing_lng[]' value='"+lng+"' id='geofencing_lng[]'>";
		html += "<input type='hidden' class='form-control' name='geofencing_radius[]' value='"+radius+"' id='geofencing_radius[]'>";
		html += "</div>";
		html += "<div class='col-lg-5 col-md-5 col-sm-5'>";
		html += "<label>Area Price</label>";
		html += "<input type='text' class='form-control' name='geofencing_price[]' value='"+price+"' id='geofencing_price[]' readonly>";
		html += "</div></div><button style='margin-top: 7px;' type='button' class='removeButton btn btn-danger' data-button="+i+"  id='remove' >Delete</button></div></br>";
		$('#append_geoinputs').append(html);
	});

});
$(document).on('click', '.removeButton', function(){
	var data = $.parseJSON($(this).attr('data-button'));
	   $("#remove_"+data).remove();
});
function myFunction() 
	{
		var checkBox = document.getElementById("should_use_buraq_margin");
		if (checkBox.checked == true)
		{
			$('#product_margin').attr('disabled',true);
		} else {
			$('#product_margin').attr('disabled',false);
		}
	}
</script>

@stop



