    @extends('Admin.layout')

    @section('title')
        Category Products
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Category Products
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_service_cat_all')}}">All Service Categories</a>
                        </li>
                        <li>
                            <a href="{{route('admin_service_cat_brands', ['category_id'=>$brand->category_id] )}}">
                                Categories
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin_brand_products_all', ['category_brand_id'=>$brand->category_brand_id] )}}">
                                <b>Category Products</b>
                            </a>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-md-6 col-lg-offset-3">
                <div class="ibox-content text-center">
                    <h2>{{ $brand->category_brand_details[0]->name }}</h2>
                        <div class="m-b-sm">
                            @if($brand->category_brand_details[0]->image != "")
    <a href="{{ $brand->category_brand_details[0]->image_url }}" title="{{ $brand->category_brand_details[0]->name }}" class="lightBoxGallery" data-gallery="">
        <img src="{{ $brand->category_brand_details[0]->image_url }}" class="img-circle " alt="profile" >
    </a>
                            @endif
                        </div>
                </div>
            </div>
        </div>
    <br>

                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="form-group">
                            <label class="pull-left">Status</label>
                                <select class="form-control" id="filter1">
                                    <option value="e">All</option>
                                    <option value="Blocked">Blocked</option>
                                    <option value="Active">Active</option>
                                </select>                 
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6">
                                    <label>
                                        Filter Updated At
                                    </label>

                                    <center>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
                                    </center>                        
                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-2">
                    <br>
                        <a href="{{route('admin_product_add', ['category_brand_id'=> $brand->category_brand_id] ) }}" class="btn btn-primary pull-right" id="Add New Product" title="Add New Product">
                            <i class="fa fa-plus"></i> Add New Product
                        </a>
                    </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-stripped tablet breakpoint footable-loaded" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th width="5%">Product Id</th>
                                    <th width="5%">Status</th>
                                    <th width="5%">Ranking</th>
                                    <th width="50%">Name</th>
                                    <th width="5%">Value</th>
                                    <th width="5%">Alpha Price</th>
                                    <th width="5%">Price Per Quantity</th>
									<!--<th width="5%">MOQ For Gift</th>
                                    <th width="5%">Gift Offer</th>-->
                                    <th width="10%">Action</th>
                                    <th width="10%">Updated At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($products as $product)
                <tr class="gradeA footable-odd">

                    <td>{{ $product->category_brand_product_id }}</td>

                    <td>
                        @if($product->blocked == '0')
                            <span class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i> Active</span>
                        @else
                            <span class="label label-danger"><i class="fa fa-eye-slash" aria-hidden="true"></i> Blocked</span>
                        @endif
                    </td>

                    <td>
                        <span class="label label-success"> {{$product->sort_order}} </span>
                    </td>

                    <td>
                        <b>
                            {{ $product->category_brand_product_details[0]->name }}
                        </b>(English)<hr>

                        <!--<b>
                            {{ $product->category_brand_product_details[1]->name }}
                        </b>(Hindi)<hr>

                        <b>
                            {{ $product->category_brand_product_details[2]->name }}
                        </b>(Urdu)<hr>

                        <b>
                            {{ $product->category_brand_product_details[3]->name }}
                        </b>(Chinese)<hr>-->

                        <b>
                            {{ $product->category_brand_product_details[4]->name }}
                        </b>(Arabic)<hr>
                    </td>

                    <td>
                        {{$product->actual_value}}
                    </td>

                    <td>
                       OMR {{$product->alpha_price}}
                    </td>

                    <td>
                        OMR {{$product->price_per_quantity}}
                    </td>
					<!--<td>
                        {{$product->gift_quantity}}
                    </td>

                    <td>
                        <b>
                            {{ $product->category_brand_product_details[0]->gift_offer }}
                        </b><hr>

                        <b>
                            {{ $product->category_brand_product_details[1]->gift_offer }}
                        </b><hr>

                        <b>
                            {{ $product->category_brand_product_details[2]->gift_offer }}
                        </b><hr>

                        <b>
                            {{ $product->category_brand_product_details[3]->gift_offer }}
                        </b><hr>

                        <b>
                            {{ $product->category_brand_product_details[4]->gift_offer }}
                        </b><hr>
                    </td>-->
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="{{ route('admin_product_update', ['category_brand_product_id' => $product->category_brand_product_id] ) }}">
                                        <i class="fa fa-edit"> Update</i>
                                    </a>
                                </li>

                                 <li role="presentation">
                                    @if($product->blocked == '0')
                                        <a role="menuitem" tabindex="-1" class="block_brand" id="{{$product->category_brand_product_id}}">
                                            <i class="fa fa-eye-slash"> Block</i>
                                        </a>
                                    @else
                                        <a role="menuitem" tabindex="-1" class="unblock_brand" id="{{$product->category_brand_product_id}}">
                                            <i class="fa fa-eye"> Unblock</i>
                                        </a>
                                    @endif
                                </li>
								<!--<li role="presentation">
                                    @if($product->geofencing_status == '0')
                                        <a role="menuitem" tabindex="-1" class="block_geofencing" id="{{$product->category_brand_product_id}}">
                                            <i class="fa fa-eye-slash">Geo-Fencing Disable</i>
                                        </a>
                                    @else
                                        <a role="menuitem" tabindex="-1" class="unblock_geofencing" id="{{$product->category_brand_product_id}}">
                                            <i class="fa fa-eye">Geo-Fencing Enable</i>
                                        </a>
                                    @endif
                                </li>-->

                            </ul>
                        </div>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $product->updated_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

        $("#services_all").addClass("active");
        $("#service_cat_all").addClass("active");

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

        $('#daterange').on('apply.daterangepicker', function(ev, picker)
            {

                dt = document.getElementById('daterange').value;

        window.location.href = '{{route("admin_brand_products_all")}}?category_brand_id={{$brand->category_brand_id}}&daterange='+dt;

            });

        });


            var table = $('#example').dataTable( 
                {
                   "order": [[ 2, "asc" ]],
                   // "scrollY": 1000,
                   // "scrollX": true,
                   "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [ 3, 5 ] },
                                { "bSearchable": true, "aTargets": [ 0, 4 ] }
                            ],
                   "fnDrawCallback": function( oSettings ) 
                        {


        $('.block_brand').click(function (e) {

              var id = $(e.currentTarget).attr("id");

                swal({
                    title: "Are you sure you want to block this product?",
                    text: "Users will not be able to access this product.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Block it!",
                    closeOnConfirm: false
                }, function () {
                    window.location.href = '{{route("admin_products_block")}}?block=1&category_brand_product_id='+id;
                });
            });

        $('.unblock_brand').click(function (e) {

                 var id = $(e.currentTarget).attr("id");

                swal({
                    title: "Are you sure you want to unblock this product?",
                    text: "Users will be able to access this product.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Unblock It!",
                    closeOnConfirm: false
                }, function () {
            window.location.href = '{{route("admin_products_block")}}?block=0&category_brand_product_id='+id;
                });
            });
			
				//M3
				

                        }
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });

    </script>


@stop
