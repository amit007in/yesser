    @extends('Admin.layout')

    @section('title')
        All Services
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All Services
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_service_cat_all')}}"><b>All Services</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Service Id</th>
                                    <th>Name</th>
                                    <th>Radius</th>
                                    <th>YesSer Percentage</th>
									<th>ReAssign Status</th>
                                    <th>Actions</th>
                                    <th>Yesser Man</th>
                                    <th>Category</th>
                                    <th>Orders</th>
                                    <th>Updated At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($categories as $category)
                <tr class="gradeA footable-odd">

                    <td>{{ $category->category_id }}</td>

                    <td>
                        <b>
                            {{ $category->category_details[0]->name }}
                        </b>(English)<hr>

                        <!--<b>
                            {{ $category->category_details[1]->name }}
                        </b>(Hindi)<hr>

                        <b>
                            {{ $category->category_details[2]->name }}
                        </b>(Urdu)<hr>

                        <b>
                            {{ $category->category_details[3]->name }}
                        </b>(Chinese)<hr>-->

                        <b>
                            {{ $category->category_details[4]->name }}
                        </b>(Arabic)<hr>
                    </td>

                    <td>{{ $category->maximum_distance }} Kms</td>

                    <td>{{ $category->buraq_percentage }} %</td>
					@if($category->reassign_status == 0 )
					<td><span class="label label-primary">Enable</span></td>
					@else
					<td><span class="label label-primary">Disable</span></td>
					@endif
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

            <li role="presentation">
                <a role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#modal-form2" onclick="update_dist('{{$category->category_id}}', '{{$category->maximum_distance}}', '{{$category->buraq_percentage}}','{{$category->interval_time}}','{{$category->transit_offer_percentage}}','{{$category->transit_buraq_margin}}','{{$category->driver_quotation_timeout}}')">
                    <i class="fa fa-edit"></i> Update Details
                </a>
            </li>

                                <li role="presentation">
            <a role="menuitem" tabindex="-1" id="admin_single_ser_dadd" href="{{route('admin_single_ser_dadd', ['category_id'=>$category->category_id])}}">
                <i class="fa fa-plus" aria-hidden="true"> Add Yesser Man</i>
            </a>
                                </li>

                                <li role="presentation">
            <a role="menuitem" tabindex="-1" id="admin_single_ser_dall_new" href="{{route('admin_single_ser_dall_new', ['category_id'=>$category->category_id])}}">
                <i class="fa fa-user-circle" aria-hidden="true"> All Yesser Man - {{ $category->driver_counts }}</i>
            </a>
                                </li>

                                <li role="presentation">
            <a role="menuitem" tabindex="-1" id="admin_service_cat_brands" href="{{route('admin_service_cat_brands', ['category_id'=>$category->category_id])}}">
                <i class="fa fa-bitcoin" aria-hidden="true"> All Categories - {{ $category->brand_counts }}</i>
            </a>
                                </li>

        <li role="presentation">
            <a role="menuitem" tabindex="-1" id="category_id" href="{{route('admin_ser_orders',['category_id' => $category->category_id])}}">
                <i class="fa fa-circle-thin" aria-hidden="true"> All Orders - {{ $category->order_counts }}</i>
            </a>
        </li>
								@if($category->reassign_status == 0 )
								<li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="{{route('admin_disable_ser_orders',['category_id' => $category->category_id])}}">
                                        <i class="fa fa-eye"> Disable Re-assign Order</i>
                                    </a>
                                </li>
								@else
								<li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="{{route('admin_enable_ser_orders',['category_id' => $category->category_id])}}">
                                        <i class="fa fa-eye"> Enable Re-assign Order</i>
                                    </a>
                                </li>
								@endif
								<!--<li role="presentation">
                                    @if($category->geofencing_status == '0')
                                        <a role="menuitem" tabindex="-1" class="block_geofencing" id="{{$category->category_id}}">
                                            <i class="fa fa-eye-slash">Geo-Fencing Disable</i>
                                        </a>
                                    @else
                                        <a role="menuitem" tabindex="-1" class="unblock_geofencing" id="{{$category->category_id}}">
                                            <i class="fa fa-eye">Geo-Fencing Enable</i>
                                        </a>
                                    @endif
                                </li>-->
<!--                                 <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="#">
                                        <i class="fa fa-eye"> Details</i>
                                    </a>
                                </li> -->

                            </ul>
                        </div>
                    </td>

                    <td>
                        <a href="{{route('admin_single_ser_dall_new', ['category_id'=>$category->category_id])}}" class="btn btn-default">
                            {{ $category->driver_counts }}
                        </a>
                    </td>


                    <td>
                        <a href="{{route('admin_service_cat_brands', ['category_id'=>$category->category_id])}}" class="btn btn-default">
                            {{ $category->brand_counts }}
                        </a>
                    </td>

                    <td>
                        <a href="{{route('admin_ser_orders', ['category_id'=>$category->category_id])}}" class="btn btn-default">
                            {{ $category->order_counts }}
                        </a>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $category->updated_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>


    <!--                Distance Modal                                -->
    <div class="modal inmodal modal-form2 modal-bg" id="modal-form2" role="dialog" aria-hidden="false">
        <div class="modal-dialog">
            <form method="post" action="{{route('admin_ser_dist_pupdate')}}" enctype="multipart/form-data" >
                <div class="modal-content animated swing">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <i class="fa fa-edit modal-icon"></i>
                        <h4 class="modal-title">Update Details</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Distance in Kms</label>
    <input type="number" name="maximum_distance" id="maximum_distance" class="form-control maximum_distance" step="any" min="0" autofocus="on" max="999999999">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>YesSer Percentage</label>
    <input type="number" name="buraq_percentage" id="buraq_percentage" class="form-control buraq_percentage" step="any" min="0" autofocus="on" max="100">
                            </div>                            
                        </div>
                        <div class="row" id="setinterval" style="display: none">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Set Quotation Interval</label>
                                <select class="form-control" id="Interval" name="Interval">
									<option value="1">1 Hour</option>
									<option value="2">2 Hour</option>
									<option value="3">3 Hour</option>
                                    <option value="5">5 Hour</option>
                                    <option value="10">10 Hour</option>
                                    <option value="15">15 Hour</option>
                                    <option value="20">20 Hour</option>
                                </select>
                            </div>                            
                        </div>
                        <br>
						<div class="row" id="driversetinterval" style="display: none">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Driver Quotation Interval</label>
                                <select class="form-control" id="DriverInterval" name="DriverInterval">
									<option value="2">2 minute</option>
									<option value="3">3 minute</option>
                                    <option value="5">5 minute</option>
                                    <option value="10">10 minute</option>
                                    <option value="15">15 minute</option>
                                    <option value="20">20 minute</option>
                                </select>
                            </div>                            
                        </div>
                        <br>
						<div class="row" id="buraqmarginper" style="display: none">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Transit Offer Percentage</label>
                                <input type="number" name="transit_offer_percentage" id="transit_offer_percentage" class="form-control transit_offer_percentage" step="any" min="0" autofocus="on" max="100">
                            </div>                            
                        </div>
						<div class="row" id="buraqmargin" style="display: none">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Transit Buraq Margin</label>
                                <input type="number" name="transit_buraq_margin" id="transit_buraq_margin" class="form-control transit_buraq_margin" step="any" min="0" autofocus="on" max="100">
                            </div>                            
                        </div>
						<br/>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="category_id" id="category_idz" class="category_id">
                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary" id="add_rform_btn">Update</button>
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
        <!--                Percentage Modal                                -->

    <script>

        $("#services_all").addClass("active");
        $("#service_cat_all").addClass("active");

            var table = $('#example').dataTable( 
                {
                   "scrollX": true,
                   "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 2 ] },
                        { "bSearchable": true, "aTargets": [ 1 ] }
                    ]
                });


			/*function update_dist(category_id, maximum_distance, buraq_percentage)
            {
                document.getElementById('category_idz').value = category_id;
                document.getElementById('maximum_distance').value = maximum_distance;
                document.getElementById('buraq_percentage').value = buraq_percentage;
            }*/
		    function update_dist(category_id, maximum_distance, buraq_percentage,interval_time,offer_percentage,buraq_margin,driversetinterval)
			{
				
				document.getElementById('category_idz').value = category_id;
				document.getElementById('maximum_distance').value = maximum_distance;
				document.getElementById('buraq_percentage').value = buraq_percentage;
				/*if(category_id == '4')
				{
					document.getElementById("setinterval").style.display         = "block";
					document.getElementById("driversetinterval").style.display   = "block";
					document.getElementById("Interval").disabled                 = false;
					document.getElementById("DriverInterval").disabled           = false;
					document.getElementById('Interval').value                    = interval_time;
					document.getElementById('DriverInterval').value              = driversetinterval;
					document.getElementById("transit_buraq_margin").disabled     = false;
					document.getElementById('transit_buraq_margin').value        = buraq_margin;
					document.getElementById("transit_offer_percentage").disabled = false;
					document.getElementById('transit_offer_percentage').value    = offer_percentage;
					document.getElementById("buraqmarginper").style.display      = "block";
					document.getElementById("buraqmargin").style.display         = "block";
				}
				else
				{*/
					document.getElementById("setinterval").style.display         = "none";
					document.getElementById("driversetinterval").style.display   = "none";
					document.getElementById("Interval").disabled                 = true;
					document.getElementById("DriverInterval").disabled           = true;
					document.getElementById("transit_offer_percentage").disabled = true;
					document.getElementById("transit_buraq_margin").disabled     = true;
					document.getElementById("buraqmarginper").style.display      = "none";
					document.getElementById("buraqmargin").style.display         = "none";
				//}
			}
				$('.block_geofencing').click(function (e) {
					var id = $(e.currentTarget).attr("id");
					swal({
						title: "Are you sure you want to disable this geo-fencing?",
						text: "Users will not be able to access this product.",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Yes, Disable it!",
						closeOnConfirm: false
					}, function () {
						window.location.href = '{{route("admin_products_geo_fencing_block")}}?block=1&category_id='+id;
					});
				});

				$('.unblock_geofencing').click(function (e) {
					var id = $(e.currentTarget).attr("id");
					swal({
						title: "Are you sure you want to Enable this product?",
						text: "Users will be able to access this product.",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Yes, Enable It!",
						closeOnConfirm: false
					}, function () {
						window.location.href = '{{route("admin_products_geo_fencing_block")}}?block=0&category_id='+id;
					});
				});

    </script>


@stop
