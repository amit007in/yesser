    @extends('Admin.layout')

    @section('title')
        Service Yesser Man
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Service Yesser Man
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_ser_dall_new')}}">
                                <b>
                                     Service Yesser Man
                                </b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        <div class="col-lg-2 col-md-2 col-sm-2">
            <label class="pull-left">Add Yesser Man</label><br>
            <div class="dropdown pull-left">
                <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                    Add Yesser Man
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                    @foreach($services as $service)
                        <li role="presentation">
                            <a role="menuitem" tabindex="-1" href="{{route('admin_single_ser_dadd', ['category_id'=>$service->category_id])}}">
                                <i class="fa fa-plus"></i>  {{$service['category_details'][0]->name}}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>

<?php

    $blocked_check = isset($data['blocked_check']) ? $data['blocked_check'] : 'All';
    $approved_check = isset($data['approved_check']) ? $data['approved_check'] : 'All';
    $created_by_check = isset($data['created_by_check']) ? $data['created_by_check'] : 'All';
    $type_check = isset($data['type_check']) ? $data['type_check'] : 'All';
    $online_check = isset($data['online_check']) ? $data['online_check'] : 'All';
    $category_id_check = isset($data['category_id_check']) ? $data['category_id_check'] : 'All';
    $search = isset($data['search']) ? $data['search'] : '';

    $daterange =  $fstarting_dt.' - '.$fending_dt;

?>

        <form method="post" action="{{route('admin_ser_dall_new')}}">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <meta name="csrf-token" content="{{ csrf_token() }}">

            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form-group">
                    <label class="pull-left">Status</label>
                        <select class="form-control" id="blocked_check" name="blocked_check">
                            <option value="All">All</option>
                            <option value="Active">Active</option>
                            <option value="Blocked">Blocked</option>
                        </select>                 
                </div>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form-group">
                    <label class="pull-left">Approved</label>
                        <select class="form-control" id="approved_check" name="approved_check">
                            <option value="All">All</option>
                            <option value="Approved">Approved</option>
                            <option value="Pending">Pending</option>
                        </select>                 
                </div>
            </div>

            <!--<div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form-group">
                    <label class="pull-left">Created By</label>
                        <select class="form-control" id="created_by_check" name="created_by_check">
                            <option value="All">All</option>
                            <option value="Admin">Admin</option>
                            <option value="Company">Company</option>
                            <option value="App">App</option>
                        </select>                 
                </div>
            </div>-->

            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form-group">
                    <label class="pull-left">Type</label>
                        <select class="form-control" id="type_check" name="type_check">
                            <option value="All">All</option>
                            <option value="Independent">Independent</option>
                            <!--<option value="Company">Company</option>-->
                        </select>                 
                </div>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form-group">
                    <label class="pull-left">Online</label>
                        <select class="form-control" id="online_check" name="online_check">
                            <option value="All">All</option>
                            <option value="Online">Online</option>
                            <option value="Offline">Offline</option>
                        </select>                 
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3">
                <label>Services</label>
                <select class="form-control" id="category_id_check" name="category_id_check">
                    <option value="All" selected>All</option>
                    @foreach($services as $service)
                        <option value="{{$service['category_details'][0]->category_id}}" title="{{$service['category_details'][0]->name}}">
                            {{$service['category_details'][0]->name}}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4">
                <label>Filter Registered At</label>
                <center>
                    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$daterange}}"/>
                </center>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-2">
                <label>Search</label>
                <input type="text" name="search" class="form-control" placeholder="Search" value="{{$search}}">
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3">
                <label>Filter</label><br>
                <button type="submit" class="btn btn-primary btn-big">Filter Results</button>
                <a class="btn btn-success btn-big" id="export_excell" onclick="export_excell()">Export Excel</a>
            </div>

    </form>

    </div>

<br>
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Active</th>
                                    <th>Blocked</th>
                                    <th>Approved</th>
                                    <!--<th>Created By</th>-->
                                    <th>Type</th>
                                    <th>Service</th>
                                    <th>Online</th>
                                    <th>Name</th>
                                    <th>ProfilePic</th>
                                    <th>Actions</th>
                                    <th>Orders</th>
<!--                                     <th>Supports</th> -->
                                    <th>Reviews</th>
<!--                                     <th>Products</th> -->
                                    <!--<th>Logins</th>-->
                                    <th>Registered At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($drivers as $driver)
                <tr class="gradeA footable-odd">

                    <td>{{ $driver->user_detail_id }}</td>

                    <td>
                        <center>
                            @if($driver->online_status == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye" aria-hidden="true"> Inactive</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($driver->blocked == '0')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye" aria-hidden="true"> Blocked</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($driver->approved == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-thumbs-up" aria-hidden="true"> Approved</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-thumbs-down" aria-hidden="true"> Pending</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <!--<td>
                        <center>
                            @if($driver->created_by == 'Admin')
                                <span class="label label-primary">
                                    <i class="fa fa-at" aria-hidden="true"></i> Admin
                                </span>
                            @elseif($driver->created_by == 'Org')
                                <span class="label label-info">
                                    <i class="fa fa-building-o" aria-hidden="true"></i> Company
                                </span>
                            @else
                                <span class="label label-success">
                                    <i class="fa fa-mobile" aria-hidden="true"></i> App
                                </span>
                            @endif
                        </center>
                    </td>-->

                    <td>
                        <center>
                            @if($driver->organisation_id == '0')
                                <span class="label label-primary">
                                    <i class="fa fa-user-secret" aria-hidden="true"> Independent</i>
                                </span>
                            @else
                                <span class="label label-info">
                                    <i class="fa fa-building-o" aria-hidden="true"></i> Company
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <span class="label label-success">{{$driver->category['name']}}</span>
                    </td>


                    <td>
                        <center>
                            @if($driver->online_status == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-toggle-on" aria-hidden="true"> Online</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-toggle-off" aria-hidden="true"> Offline</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        {{ $driver->name }} ({{ $driver->phone_number }})
                        <hr style="margin-top:4px !important;margin-bottom:4px !important;">
                        <b>OTP -</b> {{ $driver->otp }}
                        @if($driver->organisation != null)
                        <hr style="margin-top:4px !important;margin-bottom:4px !important;">
                            <b>Company - </b> {{$driver->organisation['name']}}
                        @endif
                    </td>

                    <td>
					<!-- Add M2 Part2  -->
					@if($driver->profile_pic != "")
						@if($driver->created_by == 'Admin')
							 <a href="{{ URL::asset('BuraqExpress/Uploads/'.$driver->profile_pic) }}" title="{{ $driver->name }}" class="lightBoxGallery img-circle" data-gallery="">
								<img src="{{ URL::asset('BuraqExpress/Uploads/'.$driver->profile_pic) }}"  class="img-circle">
							</a>
						@elseif($driver->created_by == 'Org')
							 <a href="{{ URL::asset('BuraqExpress/Uploads/'.$driver->profile_pic) }}" title="{{ $driver->name }}" class="lightBoxGallery img-circle" data-gallery="">
								<img src="{{ URL::asset('BuraqExpress/Uploads/'.$driver->profile_pic) }}"  class="img-circle">
							</a>
						@else
							<a href="{{ env('SOCKET_URL').'/Uploads/'.$driver->profile_pic }}" title="{{ $driver->name }}" class="lightBoxGallery img-circle" data-gallery="">
								<img src="{{ env('SOCKET_URL').'/Uploads/'.$driver->profile_pic }}"  class="img-circle">
							</a>
						@endif
					@else
						<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $driver->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
						</a>
					@endif
                    </td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

        @if($driver->organisation_id == 0)

<!--             <li role="presentation">
                <a role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#modal-form2" onclick="update_price('{{$driver->user_id}}', '{{$driver->buraq_percentage}}', '{{$driver->bottle_charge}}', '{{$driver->category_id}}')">
                    <i class="fa fa-edit"></i> Update Percentage
                </a>
            </li>
 
            <li role="presentation">
                <a role="menuitem" tabindex="-1" id="admin_customer_reviews" href="{{ route('admin_ser_ledger_all',['user_detail_id' => $driver->user_detail_id]) }}">
                    <i class="fa fa-book" aria-hidden="true"></i> All Ledgers - {{ $driver->ledger_counts }}
                </a>
            </li>-->

        @endif

            <li role="presentation">
                <a role="menuitem" tabindex="-1" id="admin_ser_dorders" href="{{route('admin_ser_dorders',['driver_user_detail_id' => $driver->user_detail_id])}}">
                    <i class="fa fa-circle-thin" aria-hidden="true"></i> All Orders - {{ $driver->order_counts }}
                </a>
            </li>

            <li role="presentation">
                <a role="menuitem" tabindex="-1" id="admin_ser_dreviews" href="{{route('admin_ser_dreviews',['driver_user_detail_id' => $driver->user_detail_id])}}">
                    <i class="fa fa-user-circle" aria-hidden="true"></i> All Reviews - {{ $driver->review_counts }}
                </a>
            </li>

<!--             <li role="presentation">
                <a role="menuitem" tabindex="-1" id="driver_user_detail_id" href="{{route('admin_ser_dproducts',['driver_user_detail_id' => $driver->user_detail_id])}}">
                    <i class="fa fa-product-hunt" aria-hidden="true"></i> All Products - {{ $driver->product_counts }}
                </a>
            </li>
 
            <li role="presentation">
                <a role="menuitem" tabindex="-1" id="support" href="{{route('admin_ser_dlogin_history', ['driver_user_detail_id'=>$driver->user_detail_id] )}}">
                    <i class="fa fa-keyboard-o" aria-hidden="true"> All Logins - {{ $driver->login_counts }} </i>
                </a>
            </li>-->

        @if($driver->phone_number != '11111111')

            @if($driver->category_id != 0)
                <li role="presentation">
                    <a role="menuitem" tabindex="-1" href="{{ route('admin_ser_dupdate', ['user_detail_id' => $driver->user_detail_id] ) }}">
                        <i class="fa fa-edit"></i> Update Profile
                    </a>
                </li>
            @endif


            <li role="presentation">
                <a role="menuitem" tabindex="-1" href="{{ route('admin_driver_track', ['user_detail_id' => $driver->user_detail_id] ) }}">
                    <i class="fa fa-map-marker"></i>  Track Yesser Man
                </a>
            </li>

            <li role="presentation">
                @if($driver->blocked == '0')
                    <a role="menuitem" tabindex="-1" class="block_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Block','{{$driver->user_detail_id}}')">
                        <i class="fa fa-eye-slash"></i> Block
                    </a>
                @else
                    <a role="menuitem" tabindex="-1" class="unblock_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Unblock','{{$driver->user_detail_id}}')">
                        <i class="fa fa-eye"></i> Unblock
                    </a>
                @endif
            </li>

            <li role="presentation">
                @if($driver->approved == '1')
                    <a role="menuitem" tabindex="-1" class=unapprove_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Unapprove','{{$driver->user_detail_id}}')">
                        <i class="fa fa-thumbs-down"></i> Unapprove
                    </a>
                @else
                    <a role="menuitem" tabindex="-1" class="approve_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Approve','{{$driver->user_detail_id}}')">
                        <i class="fa fa-thumbs-up"></i> Approve
                    </a>
                @endif
            </li>

            <li role="presentation">
                <a role='menuitem' tabindex="-1" onclick="delete_popup('{{$driver->user_detail_id}}', '{{$driver->user_id}}')">
                    <i class="fa fa-bitbucket" aria-hidden="true"> Delete </i>
                </a>
            </li>

        @endif

                            </ul>
                        </div>
                    </td>

                    <td>
                        <a href="{{ route('admin_ser_dorders',['driver_user_detail_id' => $driver->user_detail_id]) }}" class="btn btn-default">
                            {{ $driver->order_counts }}
                        </a>
                    </td>

                    <td>
                        <a href="{{ route('admin_ser_dreviews',['driver_user_detail_id' => $driver->user_detail_id]) }}" class="btn btn-default">
                            {{ $driver->review_counts }}
                        </a>
                    </td>

<!--                     <td>
                        <a href="{{ route('admin_ser_dproducts',['driver_user_detail_id' => $driver->user_detail_id]) }}" class="btn btn-default">
                            {{ $driver->product_counts }}
                        </a>
                    </td>
 
                    <td>
                        <a href="{{ route('admin_ser_dlogin_history',['driver_user_detail_id' => $driver->user_detail_id]) }}" class="btn btn-default">
                            {{ $driver->login_counts }}
                        </a>
                    </td>-->

                    <td><i class="fa fa-clock-o"></i> {{ $driver->created_atz }} </td>

                    </tr>
                @endforeach
                            </tbody>

                            <tfoot>
                <center>
                    <h2>Total - {{ $drivers->total() }}</h2>
                </center>
                <center>
                    {{ $drivers->appends(['blocked_check' => $blocked_check, 'approved_check' => $approved_check, 'created_by_check' => $created_by_check, 'type_check'=>$type_check, 'online_check'=>$online_check, 'category_id_check'=>$category_id_check, 'search'=>$search, 'daterange' => $daterange])->links() }}
                </center>                                
                            </tfoot>

                            </table>

                            </div>
                        </div>
                    </div>

        </div>
    </div>
</div>

    <!--                Percentage Modal                                -->
    <div class="modal inmodal modal-form2 modal-bg" id="modal-form2" role="dialog" aria-hidden="false">
        <div class="modal-dialog">
            <form method="post" action="{{route('admin_driver_cat_percent')}}" enctype="multipart/form-data" >
                <div class="modal-content animated swing">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <i class="fa fa-edit modal-icon"></i>
                        <h4 class="modal-title">Update Percentage</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Buraq Percentage</label>
    <input type="number" name="buraq_percentage" id="buraq_percentage" class="form-control" step="any" min="0" max="100" autofocus="on">
                            </div>

                        </div>
                        <br>
                        <div id="ubottle_charge" class="row" style="display: none;;">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Bottle Charges</label>
    <input type="number" name="bottle_charge" id="bottle_charge" class="form-control" step="any" min="0" autofocus="on">
                            </div>

                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="modal-footer">

                        <input type="hidden" name="user_id" value="" id="user_id">

                        <button type="submit" class="btn btn-primary" id="add_rform_btn">Update</button>
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
        <!--                Percentage Modal                                -->

</div>

    <script>

        $("#services_all").addClass("active");
        $("#admin_ser_dall").addClass("active");

/////////////       Filters     ///////////////////////
        $('#blocked_check').val('{{$blocked_check}}');
        $('#approved_check').val('{{$approved_check}}');
        $('#created_by_check').val('{{$created_by_check}}');
        $('#type_check').val('{{$type_check}}');
        $('#online_check').val('{{$online_check}}');
        $('#category_id_check').val('{{$category_id_check}}');
/////////////       Filters     ///////////////////////

    function export_excell()
    {
        var blocked_check = '{{$blocked_check}}';
        var approved_check = '{{$approved_check}}';
        var created_by_check = '{{$created_by_check}}';
        var type_check = '{{$type_check}}';
        var online_check = '{{$online_check}}';
        var category_id_check = '{{$category_id_check}}';
        var search = '{{$search}}';
        var daterange = '{{$daterange}}';

        toastr.info('Info.', 'Excell is getting prepared. Please wait...');
        $('#export_excell').attr("disabled", "disabled");

        $.ajax({
            dataType: "json",
            url: '{{route("admin_drivers_preport")}}',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            data: {
                blocked_check,
                approved_check,
                created_by_check,
                type_check,
                online_check,
                category_id_check,
                search,
                daterange
            },
            success:  function(result){

                $('#export_excell').attr("disabled", false);
                //$("#div1").html(result);
                window.location.href = result.link;
                console.log(result);
        
            }
        });

        //console.log(blocked_check, approved_check, created_by_check, type_check);

    }


        function update_price(user_id, buraq_percentage, bottle_charge, category_id)
            {

                document.getElementById('buraq_percentage').value = buraq_percentage;
                document.getElementById('user_id').value = user_id;
                
                if(category_id == 2)
                    {
                        document.getElementById('bottle_charge').value = bottle_charge;
                        $("#ubottle_charge").css({'display':'block'});  //or
                    }
                else
                    {
                        $("#ubottle_charge").css({'display':'none'});  //or
                    }

            }

        function delete_popup(driver_user_detail_id, driver_user_id)
            {

                swal({
                    title: "Are you sure you want to delete this Yesser Man?",
                    text: "All data will be completely deleted.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Delete it!",
                    closeOnConfirm: false
                    }, function () {
                        window.location.href = '{{route("admin_delete")}}?driver_user_detail_id='+driver_user_detail_id+'&driver_user_id='+driver_user_id;
                    });

            }

        function show_popup(type, id)
            {

                if(type == 'Block')
                    {
                        var title = "Are you sure you want to block this Yesser Man?";
                        var sub_title = "User will not be view this Yesser Man.";
                        var route = '{{route("admin_driver_block")}}?block=1&user_detail_id='+id;
                    }
                else if(type == 'Unblock')
                    {
                        var title = "Are you sure you want to unblock this Yesser Man?";
                        var sub_title = "User will be able to view this Yesser Man.";
                        var route = '{{route("admin_driver_block")}}?block=0&user_detail_id='+id;
                    }
                else if(type == 'Approve')
                    {
                        var title = "Are you sure you want to approve this Yesser Man?";
                        var sub_title = "Documents will become verified";
                        var route = '{{route("admin_driver_approve")}}?approve=1&user_detail_id='+id;
                    }
                else if(type == 'Unapprove')
                    {
                        var title = "Are you sure you want to unapprove this Yesser Man?";
                        var sub_title = "Documents will become unverified";
                        var route = '{{route("admin_driver_approve")}}?approve=0&user_detail_id='+id;
                    }
                
                swal({
                        title: title,
                        text: sub_title,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Do it!",
                        closeOnConfirm: false
                    }, function () {
                        window.location.href = route;
                    });
            }

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                opens: 'center',
                autoApply: true,
                format: 'DD/MM/YYYY',
                minDate: '1/2/2018',
                maxDate: moment().format('DD/MM/YYYY')
              }, function(start, end, label) {
                //console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
              });

        });

    </script>


@stop
