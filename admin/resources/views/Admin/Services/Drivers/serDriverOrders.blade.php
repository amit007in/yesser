@extends('Admin.layout')

@section('title')
    Service Yesser Man Orders
@stop

@section('content')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Service Yesser Man Orders
                    </h2>
                    <ol class="breadcrumb">
                        <li><a href="{{route('org_dashboard')}}">Dashboard</a></li>
                        <li><a href="{{route('admin_ser_dall_new')}}">Service Yesser Man</a></li>
                        <li>
                <a href="{{ route('admin_ser_dorders',['driver_user_detail_id' => $driver->user_detail_id]) }}">
                    <b>Service Yesser Man Orders</b>
                </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-md-6 col-lg-offset-3">
            <div class="ibox-content text-center">
                <h2>{{ $driver['user']->name }}</h2>
                    <div class="m-b-sm">
					@if($driver->profile_pic != "")
						@if($driver->created_by == 'Admin')
							 <a href="{{ URL::asset('BuraqExpress/Uploads/'.$driver->profile_pic) }}" title="{{$driver['user']->name  }}" class="lightBoxGallery img-circle" data-gallery="">
								<img src="{{ URL::asset('BuraqExpress/Uploads/'.$driver->profile_pic) }}"  class="img-circle">
							</a>
						@elseif($driver->created_by == 'Org')
							 <a href="{{ URL::asset('BuraqExpress/Uploads/'.$driver->profile_pic) }}" title="{{ $driver['user']->name  }}" class="lightBoxGallery img-circle" data-gallery="">
								<img src="{{ URL::asset('BuraqExpress/Uploads/'.$driver->profile_pic) }}"  class="img-circle">
							</a>
						@else
							<a href="{{ env('SOCKET_URL').'/Uploads/'.$driver->profile_pic }}" title="{{ $driver['user']->name  }}" class="lightBoxGallery img-circle" data-gallery="">
								<img src="{{ env('SOCKET_URL').'/Uploads/'.$driver->profile_pic }}"  class="img-circle">
							</a>
						@endif
					@else
						<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $driver['user']->name  }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
						</a>
					@endif
                           
                    </div>
            </div>
        </div>
    </div>
<br>

    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="form-group">
                <label class="pull-left">Status</label>
                    <select class="form-control" id="filter1">
                        <option value="">All</option>

                        <option value="Searching">Searching</option>
                        <option value="Confirmed">Confirmed</option>
                        <option value="Scheduled">Scheduled</option>

                        <option value="Ongoing">Ongoing</option>
                        <option value="Rejected">Rejected</option>
                        <option value="Timeout">Timeout</option>

                        <option value="Customer Cancelled">Customer Cancelled</option>
                        <option value="Driver Cancelled">Yesser Man Cancelled</option>

                        <option value="Completed">Completed</option>
                        <option value="Confirmation Pending">Confirmation Pending</option>
						<option value="Driver not found">Yesser Man not found</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6">
            <label>
                Filter Timing
            </label>

            <center>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
            </center>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="form-group">
                <label class="pull-left">Type</label>
                    <select class="form-control" id="filter2">
                        <option value="">All</option>
                        <option value="Service">Service</option>
                        <!--<option value="Support">Support</option>-->
                    </select>                 
            </div>
        </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Type</th>
                                    <th>Customer</th>
                                    <th>CustomerPic</th>
                                    <th>Timing</th>
                                    <th>Pickup Location</th>
                                    <th>Product</th>
                                    <th>Payment</th>
                                    <th>Actions</th>
                                    <th>Booked At</th>
                                </tr>

                                </thead>
                                <tbody>

            @foreach($orders as $order)
                <tr class="gradeA footable-odd" style="display: table-row;">

                    <td>{{ $order->order_id }}</td>

                    <td>
                        @include('Common.Partials.orderStatus')
                    </td>


                    <td>
                        @if($order->order_type == 'Service')
                            <span class="label label-info">
                                <i class="fa fa-certificate"> Service</i>
                            </span>
                        @else
                            <span class="label label-primary">
                                <i class="fa fa-support"> Support</i>
                            </span>
                        @endif
                    </td>

                    <td>
                        {{ $order['customer_user']->name }}
                    </td>

                    <td>
					@if($order['customer_user_detail']->profile_pic == "")
						<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
						</a>
					 @else
						<a href="{{ env('SOCKET_URL').'/Uploads/'.$order['customer_user_detail']->profile_pic }}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ env('SOCKET_URL').'/Uploads/'.$order['customer_user_detail']->profile_pic }}"  class="img-circle">
						</a>
					 @endif	
                    </td>


                    <td><i class="fa fa-clock-o"></i> {{ $order->order_timingsz }} </td>

                    <td>
                        {{ $order->pickup_address }}
                    </td>

                    <td>
                        <span class="label label-info" style="padding: 1px 8px; !important">
						{{ $order->payment['product_quantity'] }} *
						<?php for($i = 0;$i<count($order->order_products);$i++){ 
							if(count($order->order_products) == $i +1){ ?>
						{{ $order->order_products[$i]['productName'] }}
						<?php }elseif(count($order->order_products) == 1){ ?>
						{{ $order->order_products[$i]['productName'] }}
						<?php }else{ ?>
						{{ $order->order_products[$i]['productName'] }} <b>,</b> </br>
						<?php } } ?>
					</span>
                    </td>

                    <td>
					   <?php $totalPrice = array(); for($i = 0;$i<count($order->order_products);$i++){
							$totalPrice[] = $order->order_products[$i]['price_per_item'];
						 } // Part M3 3.2
						$totalPrice[] = $order->floor_charge;
						$total = array_sum($totalPrice) - $order->reward_discount_price ;	
						?>
						<b>OMR</b>  <?php echo number_format($total,2); ?>
                    </td>

                    <td>

            <a href="{{ route('admin_order_details', ['order_id' => $order->order_id] ) }}" class="btn btn-primary pull-right" id="Details" title="Details">
                <i class="fa fa-ioxhost"> Order Details</i>
            </a>
<!--                         <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                                <li role="presentation">
    <a role="menuitem" href="{{ route('admin_order_details', ['order_id' => $order->order_id]) }}">
        <i class="fa fa-ioxhost"> Details</i>
    </a>
                                </li>


                            </ul>
                        </div> -->
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

        $("#services_all").addClass("active");
        $("#service_cat_all").addClass("active");

        $(document).ready(function()
            {

                $('input[name="daterange"]').daterangepicker({
                        format: 'DD/MM/YYYY',
                        minDate: '1/2/2018',
                        maxDate: moment().format('DD/MM/YYYY'),
                        opens: 'center',
                        drops: 'down',
                        buttonClasses: ['btn', 'btn-sm'],
                        applyClass: 'btn-primary',
                        cancelClass: 'btn-default'
                            });

            $('#daterange').on('apply.daterangepicker', function(ev, picker)
                {
                    dt = document.getElementById('daterange').value;

                    window.location.href = "{{route('admin_ser_dorders')}}?driver_user_detail_id={{$driver->user_detail_id}}&daterange="+dt;
                });

            });

        var table = $('#example').dataTable( 
            {
                "order": [[ 0, "desc" ]],
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 4, 7, 9 ] },
                    { "bSearchable": true, "aTargets": [ 3, 6 ] }
                ],
                    "dom": 'Blfrtip',
                    "buttons": [
                       {
                            extend: 'pdf',
                            footer: true,
                            exportOptions: {
                                columns: [0,1,2,3,5,6,7,8,10]
                            }
                       },
                       {
                           extend: 'csv',
                           footer: false,
                            exportOptions: {
                                columns: [0,1,2,3,5,6,7,8,10]
                            }
                          
                       },
                       {
                           extend: 'excel',
                           footer: false,
                            exportOptions: {
                                columns: [0,1,2,3,5,6,7,8,10]
                            }
                       }         
                    ]
            });

        $("#filter1").on('change', function()
            {
                table.fnFilter($(this).val(), 1);
            });

        $("#filter2").on('change', function()
            {
                table.fnFilter($(this).val(), 2);
            });

    </script>


@stop
