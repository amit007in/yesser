    @extends('Admin.layout')

    @section('title')
        Service Drivers
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Service Drivers
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_service_cat_all')}}">All Services</a>
                        </li>
                        <li>
                            <a href="{{route('admin_single_ser_dall',['category_id'=>$category->category_id])}}">
                                <b>
                                     Service Drivers
                                </b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

       <div class="row">
            <div class="col-md-6 col-lg-offset-3">
                <div class="ibox-content text-center">
                    <h2>{{ $category->category_details[0]->name }}</h2>
                </div>
            </div>
        </div>
        <br>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="form-group">
                <label class="pull-left">Status</label>
                    <select class="form-control" id="filter1">
                        <option value="e">All</option>
                        <option value="Active">Active</option>
                        <option value="Blocked">Blocked</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="form-group">
                <label class="pull-left">Approved</label>
                    <select class="form-control" id="filter2">
                        <option value="">All</option>
                        <option value="Approved">Approved</option>
                        <option value="Pending">Pending</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="form-group">
                <label class="pull-left">Created By</label>
                    <select class="form-control" id="filter3">
                        <option value="a">All</option>
                        <option value="Admin">Admin</option>
                        <option value="Company">Company</option>
                        <option value="App">App</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="form-group">
                <label class="pull-left">Type</label>
                    <select class="form-control" id="filter4">
                        <option value="n">All</option>
                        <option value="Independent">Independent</option>
                        <option value="Company">Company</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <label>
                Filter Updated At
            </label>

            <center>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
            </center>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="form-group">
                <label class="pull-left">Online/Offline</label>
                    <select class="form-control" id="filter5">
                        <option value="">All</option>
                        <option value="Online">Online</option>
                        <option value="Offline">Offline</option>
                    </select>                 
            </div>
        </div>


        <div class="col-lg-2 col-md-2 col-sm-2">
            <br>
<a class="btn btn-flat btn-info btn-sm" role="menuitem" tabindex="-1" href="{{route('admin_single_ser_dadd', ['category_id'=>$category->category_id])}}">
    <i class="fa fa-plus"></i>  Add Driver
</a>            

        </div>


<br>
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Approved</th>
                                    <th>Created By</th>
                                    <th>Type</th>
                                    <th>Online</th>
                                    <th>Name</th>
                                    <th>ProfilePic</th>
                                    <th>Actions</th>
                                    <th>Orders</th>
                                    <th>Reviews</th>
                                    <th>Products</th>
                                    <th>Logins</th>
                                    <th>Registered At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($drivers as $driver)
                <tr class="gradeA footable-odd">

                    <td>{{ $driver->user_detail_id }}</td>

                    <td>
                        <center>
                            @if($driver->blocked == '0')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye" aria-hidden="true"> Blocked</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($driver->approved == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-thumbs-up" aria-hidden="true"> Approved</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-thumbs-down" aria-hidden="true"> Pending</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($driver->created_by == 'Admin')
                                <span class="label label-primary">
                                    <i class="fa fa-at" aria-hidden="true"></i> Admin
                                </span>
                            @elseif($driver->created_by == 'Org')
                                <span class="label label-info">
                                    <i class="fa fa-building-o" aria-hidden="true"></i> Company
                                </span>
                            @else
                                <span class="label label-success">
                                    <i class="fa fa-mobile" aria-hidden="true"></i> App
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($driver->organisation_id == '0')
                                <span class="label label-primary">
                                    <i class="fa fa-user-secret" aria-hidden="true"> Independent</i>
                                </span>
                            @else
                                <span class="label label-info">
                                    <i class="fa fa-building-o" aria-hidden="true"></i> Company
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($driver->online_status == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-toggle-on" aria-hidden="true"> Online</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-toggle-off" aria-hidden="true"> Offline</i>
                                </span>
                            @endif
                        </center>
                    </td>


                    <td>
                        {{ $driver->name }} ({{ $driver->phone_number }})
                        <hr>
                        <b>OTP -</b> {{ $driver->otp }}
                    </td>

                    <td>
    <a href="{{$driver->profile_pic_url}}" title="{{ $driver->name }}" class="lightBoxGallery" data-gallery="">
        <img src="{{$driver->profile_pic_url}}/120/120"  class="img-circle">
    </a>
                    </td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

        <li role="presentation">
            <a role="menuitem" tabindex="-1" href="{{ route('admin_driver_track', ['user_detail_id' => $driver->user_detail_id] ) }}">
                <i class="fa fa-map-marker"></i>  Track Driver
            </a>
        </li>

        @if($driver->organisation_id == 0)

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#modal-form2" onclick="update_price('{{$driver->user_id}}', '{{$driver->buraq_percentage}}', '{{$driver->bottle_charge}}', '{{$driver->category_id}}')">
        <i class="fa fa-edit"></i> Update Percentage
    </a>
                                </li>

        <li role="presentation">
            <a role="menuitem" tabindex="-1" id="admin_customer_reviews" href="{{ route('admin_ser_ledger_all',['user_detail_id' => $driver->user_detail_id]) }}">
                <i class="fa fa-book" aria-hidden="true"></i> All Ledgers - {{ $driver->ledger_counts }}
            </a>
        </li>

        @endif

        @if($driver->category_id != 0)
            <li role="presentation">
                <a role="menuitem" tabindex="-1" href="{{ route('admin_ser_dupdate', ['user_detail_id' => $driver->user_detail_id] ) }}">
                    <i class="fa fa-edit"></i> Update Profile
                </a>
            </li>
        @endif

        <li role="presentation">
            <a role="menuitem" tabindex="-1" id="admin_cust_orders" href="{{route('admin_ser_dorders',['driver_user_detail_id' => $driver->user_detail_id])}}">
                <i class="fa fa-circle-thin" aria-hidden="true"> All Orders - {{ $driver->order_counts }}</i>
            </a>
        </li>

        <li role="presentation">
            <a role="menuitem" tabindex="-1" id="admin_customer_reviews" href="{{route('admin_ser_dreviews',['driver_user_detail_id' => $driver->user_detail_id])}}">
                <i class="fa fa-star" aria-hidden="true"> All Reviews - {{ $driver->review_counts }}</i>
            </a>
        </li>

        @if($driver->category_id != 0)
            <li role="presentation">
                <a role="menuitem" tabindex="-1" id="admin_customer_reviews" href="{{route('admin_ser_dproducts',['driver_user_detail_id' => $driver->user_detail_id])}}">
                    <i class="fa fa-product-hunt" aria-hidden="true"></i> All Products - {{ $driver->product_counts }}
                </a>
            </li>
        @endif

        <li role="presentation">
            <a role="menuitem" tabindex="-1" id="support" href="{{route('admin_ser_dlogin_history', ['driver_user_detail_id'=>$driver->user_detail_id] )}}">
                <i class="fa fa-keyboard-o" aria-hidden="true"> All Logins - {{ $driver->login_counts }} </i>
            </a>
        </li>

            <li role="presentation">
                @if($driver->blocked == '0')
                    <a role="menuitem" tabindex="-1" class="block_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Block','{{$driver->user_detail_id}}')">
                        <i class="fa fa-eye-slash"> Block</i>
                    </a>
                @else
                    <a role="menuitem" tabindex="-1" class="unblock_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Unblock','{{$driver->user_detail_id}}')">
                        <i class="fa fa-eye"> Unblock</i>
                    </a>
                @endif
            </li>

            <li role="presentation">
                @if($driver->approved == '1')
                    <a role="menuitem" tabindex="-1" class=unapprove_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Unapprove','{{$driver->user_detail_id}}')">
                        <i class="fa fa-thumbs-down"> Unapprove</i>
                    </a>
                @else
                    <a role="menuitem" tabindex="-1" class="approve_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Approve','{{$driver->user_detail_id}}')">
                        <i class="fa fa-thumbs-up"> Approve</i>
                    </a>
                @endif
            </li>

<!--                                 <li role="presentation">
            <a role='menuitem' tabindex="-1" onclick="delete_popup('{{$driver->user_detail_id}}', '{{$driver->user_id}}')">
                <i class="fa fa-bitbucket" aria-hidden="true"> Delete </i>
            </a>
                                </li> -->

                            </ul>
                        </div>
                    </td>

                    <td>
                        <a href="{{ route('admin_ser_dorders',['driver_user_detail_id' => $driver->user_detail_id]) }}" class="btn btn-default">
                            {{ $driver->order_counts }}
                        </a>
                    </td>

                    <td>
                        <a href="{{ route('admin_ser_dreviews',['driver_user_detail_id' => $driver->user_detail_id]) }}" class="btn btn-default">
                            {{ $driver->review_counts }}
                        </a>
                    </td>

                    <td>
                        @if($driver->category_id != 0)
                            <a href="{{ route('admin_ser_dproducts',['driver_user_detail_id' => $driver->user_detail_id]) }}" class="btn btn-default">
                                {{ $driver->product_counts }}
                            </a>
                        @endif
                    </td>

                    <td>
                        <a href="{{ route('admin_ser_dlogin_history',['driver_user_detail_id' => $driver->user_detail_id]) }}" class="btn btn-default">
                            {{ $driver->login_counts }}
                        </a>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $driver->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>

        </div>
    </div>
</div>

    <!--                Percentage Modal                                -->
    <div class="modal inmodal modal-form2 modal-bg" id="modal-form2" role="dialog" aria-hidden="false">
        <div class="modal-dialog">
            <form method="post" action="{{route('admin_driver_cat_percent')}}" enctype="multipart/form-data" >
                <div class="modal-content animated swing">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <i class="fa fa-edit modal-icon"></i>
                        <h4 class="modal-title">Update Percentage</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Buraq Percentage</label>
    <input type="number" name="buraq_percentage" id="buraq_percentage" class="form-control" step="any" min="0" max="100" autofocus="on">
                            </div>

                        </div>
                        <br>
                        <div id="ubottle_charge" class="row" style="display: none;;">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Bottle Charges</label>
    <input type="number" name="bottle_charge" id="bottle_charge" class="form-control" step="any" min="0" autofocus="on">
                            </div>

                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="modal-footer">

                        <input type="hidden" name="user_id" value="" id="user_id">

                        <button type="submit" class="btn btn-primary" id="add_rform_btn">Update</button>
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
        <!--                Percentage Modal                                -->

    <script>

        $("#services_all").addClass("active");
        $("#service_cat_all").addClass("active");

        function update_price(user_id, buraq_percentage, bottle_charge, category_id)
            {

                document.getElementById('buraq_percentage').value = buraq_percentage;
                document.getElementById('user_id').value = user_id;
                
                if(category_id == 2)
                    {
                        document.getElementById('bottle_charge').value = bottle_charge;
                        $("#ubottle_charge").css({'display':'block'});  //or
                    }
                else
                    {
                        $("#ubottle_charge").css({'display':'none'});  //or
                    }

            }

        function delete_popup(driver_user_detail_id, driver_user_id)
            {

                swal({
                    title: "Are you sure you want to delete this driver?",
                    text: "All data will be completely deleted.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Delete it!",
                    closeOnConfirm: false
                    }, function () {
                        window.location.href = '{{route("admin_delete")}}?driver_user_detail_id='+driver_user_detail_id+'&driver_user_id='+driver_user_id;
                    });

            }

        function show_popup(type, id)
            {

                if(type == 'Block')
                    {
                        var title = "Are you sure you want to block this driver?";
                        var sub_title = "User will not be view this driver.";
                        var route = '{{route("admin_driver_block")}}?block=1&user_detail_id='+id;
                    }
                else if(type == 'Unblock')
                    {
                        var title = "Are you sure you want to unblock this driver?";
                        var sub_title = "User will be able to view this driver.";
                        var route = '{{route("admin_driver_block")}}?block=0&user_detail_id='+id;
                    }
                else if(type == 'Approve')
                    {
                        var title = "Are you sure you want to approve this driver?";
                        var sub_title = "Documents will become verified";
                        var route = '{{route("admin_driver_approve")}}?approve=1&user_detail_id='+id;
                    }
                else if(type == 'Unapprove')
                    {
                        var title = "Are you sure you want to unapprove this driver?";
                        var sub_title = "Documents will become unverified";
                        var route = '{{route("admin_driver_approve")}}?approve=0&user_detail_id='+id;
                    }
                
                swal({
                        title: title,
                        text: sub_title,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Do it!",
                        closeOnConfirm: false
                    }, function () {
                        window.location.href = route;
                    });
            }

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

        $('#daterange').on('apply.daterangepicker', function(ev, picker)
            {
                dt = document.getElementById('daterange').value;

                window.location.href = "{{route('admin_single_ser_dall')}}?category_id={{$category->category_id}}&daterange="+dt;
            });

        });

        var table = $('#example').dataTable( 
            {
//              "scrollY": 500,
                "order": [[ 0, "desc" ]],
                "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 8, 9 ] },
                        { "bSearchable": true, "aTargets": [ 7 ] }
                    ]
            });

        $("#filter1").on('change', function()
            {
                table.fnFilter($(this).val(), 1);
            });

        $("#filter2").on('change', function()
            {
                table.fnFilter($(this).val(), 2);
            });

        $("#filter3").on('change', function()
            {
                table.fnFilter($(this).val(), 3);
            });
        $("#filter4").on('change', function()
            {
                table.fnFilter($(this).val(), 4);
            });
        $("#filter5").on('change', function()
            {
                table.fnFilter($(this).val(), 5);
            });

    </script>


@stop
