@extends('Admin.layout')

@section('title') 
    Track Yesser Man
@stop

@section('content')

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="row">

                <div class="col-lg-10">
                    <h2>
                        Track Yesser Man
                    </h2>
                    
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('admin_dashboard') }}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_ser_dall_new') }}">Service Yesser Man</a>
                        </li>
                        <li>
                            <a href="{{route('admin_driver_track',['user_detail_id'=>$driver->user_detail_id])}}">
                                <b>
                                    Track Yesser Man
                                </b>                            
                            </a>
                        </li>
                    </ol>
                </div>
            
            </div>
        </div>

<style type="text/css">
    #legend {
        background: #FFF;
        padding: 10px;
        margin: 5px;
        font-size: 12px;
        font-family: Arial, sans-serif;
      }

</style>

        <div class="row">
<!--            Map Canvas                            -->
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div id="map1" style="width:100%; height:450px;"></div>
            </div>
<!--            Map Canvas                            -->
        </div>
<br>

    <div class="row">
        <div class="col-md-6 col-lg-offset-3">
            <div class="ibox-content text-center">
                <h2>{{ $driver['user']->name }} - {{$driver['user']->phone_number}}</h2>
                    <div class="m-b-sm">
                            
							@if($driver->profile_pic != "")
								@if($driver->created_by == 'Admin')
									 <a href="{{ URL::asset('BuraqExpress/Uploads/'.$driver->profile_pic) }}" title="{{ $driver['user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
										<img src="{{ URL::asset('BuraqExpress/Uploads/'.$driver->profile_pic) }}"  class="img-circle">
									</a>
								@elseif($driver->created_by == 'Org')
									 <a href="{{ URL::asset('BuraqExpress/Uploads/'.$driver->profile_pic) }}" title="{{ $driver['user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
										<img src="{{ URL::asset('BuraqExpress/Uploads/'.$driver->profile_pic) }}"  class="img-circle">
									</a>
								@else
									<a href="{{ env('SOCKET_URL').'/Uploads/'.$driver->profile_pic }}" title="{{ $driver['user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
										<img src="{{ env('SOCKET_URL').'/Uploads/'.$driver->profile_pic }}"  class="img-circle">
									</a>
								@endif
							@else
								<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $driver['user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
									<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
								</a>
							@endif
                    </div>
            </div>
        </div>
    </div>


<br><br><br>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key={{env('GOOGLE_MAP_KEY')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.dev.js"></script>

<script type="text/javascript">

    $("#services_all").addClass("active");
    $("#admin_ser_dall").addClass("active");

    var socket = io.connect("{{env('SOCKET_URL')}}", { query: { admin_access_token: "{{$admin->access_token}}" } } );

    var driver_latitude = parseFloat("{{$driver->latitude}}");
    var driver_longitude = parseFloat("{{$driver->longitude}}");
    var driver_rotation = parseFloat("{{$driver->bearing}}");
    var default_latitude = 21.4735;
    var default_longitude = 55.9754;
    var driverMarker;

    function driver_icon_image(category_id)
        {
            if(category_id == 1)////////////       Gas     ////////////////
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_gas.png";
            else if(category_id == 2)////////////       Drinking Water  ///
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_drink_water.png";
            else if(category_id == 3)////////////       Water Tanker    ///
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_water_tank.png";
            else if(category_id == 4)////////////       Freight        /////////
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_freights.png";
            else if(category_id == 5)////////////       Tow        /////////////
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_tow.png";
            else if(category_id == 6)////////////       Heavy        ///////////
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_heavy.png";
            else
                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/sup_icon.png";
        }

        driver_icon_image("{{$driver->category_id}}");

    function load_map()
        {

            mapOptions = {
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoom: 7,
                    center:new google.maps.LatLng(default_latitude,default_longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false
                };

            myMap = new google.maps.Map(document.getElementById('map1'), mapOptions);

            /* Adding Map Legends */
            var legend = document.createElement('div');
            legend.id = 'legend';

            div= document.createElement('div');
            div.innerHTML = '<span><img src='+driver_icon+'> {{$driver->name}}</span>';
            legend.appendChild(div);

            infowindow = new google.maps.InfoWindow();
            bounds = new google.maps.LatLngBounds();

/////////////// Pick up Marker  ///////////////////////
            driverMarker = new google.maps.Marker({
                animation: google.maps.Animation.DROP,
                position: {"lat": driver_latitude,"lng": driver_longitude},
                title: 'Driver - {{$driver->name}} ',
                map: myMap,
                icon: driver_icon,
                rotation: driver_rotation
            });

            if(driver_latitude == 0.0)
                driverMarker.setMap(null);

            bounds.extend(driverMarker.position);
/////////////// Pick up Marker  ///////////////////////

//            pollingFunction();
            
            trackDriver();
            setInterval( trackDriver, 15000);

            myMap.controls[google.maps.ControlPosition.RIGHT_TOP].push(legend);

        }

    function trackDriver()
        {
            //console.log('Ongoing');
            socket.emit("PanelCommonEvent", { type:"SingleDTrack", user_detail_id: parseInt("{{$driver->user_detail_id}}") }, (response) => {

                //console.log('Ongoing response', response);

                if(response.success == 0)
                    {
                        toastr.error('Not able to track', response.msg);
                    }
                else
                    {
                        driver_latitude = parseFloat(response.driver.latitude);
                        driver_longitude = parseFloat(response.driver.longitude);
                        driver_rotation = parseFloat(response.driver.bearing);

                        add_driver_marker();
                    }

            });

        }

    function add_driver_marker()
        {
            if (driverMarker && driverMarker.setMap) {
                driverMarker.setMap(null);
            }

            //console.log(driver_latitude, driver_longitude, driver_rotation, 'driver_longitude');

            if(driver_latitude != 0.0 && driver_longitude != 0.0)
                {

                    driverMarker = new google.maps.Marker({
                        //animation: google.maps.Animation.DROP,
                        position: {"lat": driver_latitude,"lng": driver_longitude},
                        title: 'Driver - {{$driver->name}} ',
                        map: myMap,
                        icon: driver_icon,
                        rotation: driver_rotation
                    });

                    myMap.panTo(driverMarker.position);
                    bounds.extend(driverMarker.position);

                }

        }

    load_map();

</script>

@stop