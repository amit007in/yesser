
@extends('Admin.layout')

@section('title')
    Service Orders
@stop

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Service Orders
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_service_cat_all')}}">All Services</a>
                        </li>
                        <li>
                            <a href="{{route('admin_ser_orders',['category_id' =>$category->category_id] )}}">
                                <b>Service Orders</b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-md-6 col-lg-offset-3">
            <div class="ibox-content text-center">
                <h2>{{ $category->category_details[0]->name }}</h2>
            </div>
        </div>
    </div>
    <br>

    <div class="row">

        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="form-group">
                <label class="pull-left">Status</label>
                    <select class="form-control" id="filter1">
                        <option value="">All</option>
                        <option value="Searching">Searching</option>
                        <option value="Confirmed">Confirmed</option>
                        <option value="Scheduled">Scheduled</option>
                        <option value="Ongoing">Ongoing</option>
                        <option value="Rejected">Rejected</option>
                        <option value="Timeout">Timeout</option>
                        <option value="Customer Cancelled">Customer Cancelled</option>
                        <option value="Driver Cancelled">Yesser Man Cancelled</option>
                        <option value="Completed">Completed</option>
                        <option value="Confirmation Pending">Confirmation Pending</option>
						<option value="Driver not found">Yesser Man not found</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6">
            <label>
                Filter Timing
            </label>

            <center>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
            </center>
        </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Customer</th>
                                    <th>CustomerPic</th>
                                    <th>Timing</th>
                                    <th>Pickup Location</th>
                                    <th>Product</th>
                                    <th>Yesser Man</th>
                                    <th>Yesser Man Pic</th>
                                    <th>Payment</th>
                                    <th>Actions</th>
                                    <th>Booked At</th>
                                </tr>

                                </thead>
                                <tbody>

            @foreach($orders as $order)
                <tr class="gradeA footable-odd" style="display: table-row;">

                    <td>{{ $order->order_id }}</td>

                    <td>
                        @include('Common.Partials.orderStatus')
                    </td>


                    <td>
                        {{ $order['customer_user']->name }}
                    </td>

                    <td>
			<!--<a href="{{ $order['customer_user_detail']->profile_pic_url }}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
				<img src="{{ $order['customer_user_detail']->profile_pic_url }}/150/150"  class="img-circle">
			</a>-->
					@if($order['customer_user_detail']->profile_pic == "")
						<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
						</a>
					 @else
						<a href="{{ env('SOCKET_URL').'/Uploads/'.$order['customer_user_detail']->profile_pic }}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ env('SOCKET_URL').'/Uploads/'.$order['customer_user_detail']->profile_pic }}"  class="img-circle">
						</a>
					 @endif	
                    </td>


                    <td><i class="fa fa-clock-o"></i> {{ $order->order_timingsz }} </td>

                    <td>
                        {{ $order->pickup_address }}
                    </td>

                    <td>
                        <span class="label label-info" style="padding: 1px 8px; !important">
						{{ $order->payment['product_quantity'] }} *
						<?php for($i = 0;$i<count($order->order_products);$i++){ 
							if(count($order->order_products) == $i +1){ ?>
						{{ $order->order_products[$i]['productName'] }}
						<?php }elseif(count($order->order_products) == 1){ ?>
						{{ $order->order_products[$i]['productName'] }}
						<?php }else{ ?>
						{{ $order->order_products[$i]['productName'] }} <b>,</b> </br>
						<?php } } ?>
					</span>
                    </td>

                    <td>
                        @if($order['driver_user_detail'])
                            {{ $order['driver_user']->name }}
                        @endif
                    </td>

                    <td>
                        @if($order['driver_user_detail'])
    <!--<a href="{{ $order['driver_user_detail']->profile_pic_url }}" title="{{ $order['driver_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
        <img src="{{ $order['driver_user_detail']->profile_pic_url }}/150/150"  class="img-circle">
    </a>-->
					@if($order['driver_user_detail']->profile_pic == "")
						<a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $order['driver_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
						</a>
					 @else
						<a href="{{ env('SOCKET_URL').'/Uploads/'.$order['driver_user_detail']->profile_pic }}" title="{{ $order['driver_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
							<img src="{{ env('SOCKET_URL').'/Uploads/'.$order['driver_user_detail']->profile_pic }}"  class="img-circle">
						</a>
					 @endif
                        @endif
                    </td>

                     <td>
					   <?php $totalPrice = array(); for($i = 0;$i<count($order->order_products);$i++){
							$totalPrice[] = $order->order_products[$i]['price_per_item'];
						 } // Part M3 3.2
						$totalPrice[] = $order->floor_charge;
						$total = array_sum($totalPrice) - $order->reward_discount_price ;	
						?>
						<b>OMR</b>  <?php echo number_format($total,2); ?>
                    </td>
                    <td>
            <a href="{{ route('admin_order_details', ['order_id' => $order->order_id] ) }}" class="btn btn-primary pull-right" id="Details" title="Details">
                <i class="fa fa-ioxhost"> Order Details</i>
            </a>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

        $("#services_all").addClass("active");
        $("#service_cat_all").addClass("active");

        $(document).ready(function()
            {

                $('input[name="daterange"]').daterangepicker({
                        format: 'DD/MM/YYYY',
                        minDate: '1/2/2018',
                        maxDate: moment().format('DD/MM/YYYY'),
                        opens: 'center',
                        drops: 'down',
                        buttonClasses: ['btn', 'btn-sm'],
                        applyClass: 'btn-primary',
                        cancelClass: 'btn-default'
                            });

            $('#daterange').on('apply.daterangepicker', function(ev, picker)
                {
                    dt = document.getElementById('daterange').value;

                    window.location.href = "{{route('admin_ser_orders')}}?category_id={{$category->category_id}}&daterange="+dt;
                });

            });

        var table = $('#example').dataTable( 
            {
                "order": [[ 0, "desc" ]],
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 4, 7, 9 ] },
                    { "bSearchable": true, "aTargets": [ 3, 5, 6 ] }
                ]
            });

        $("#filter1").on('change', function()
            {
                table.fnFilter($(this).val(), 1);
            });

        $("#filter2").on('change', function()
            {
                table.fnFilter($(this).val(), 2);
            });

    </script>


@stop
