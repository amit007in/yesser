    @extends('Admin.layout')

    @section('title')
        All Products
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All Products
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_ser_prods_all')}}">
                                <b>All Products</b>
                            </a>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label class="pull-left">Status</label>
                                <select class="form-control" id="filter1">
                                    <option value="e">All</option>
                                    <option value="Blocked">Blocked</option>
                                    <option value="Active">Active</option>
                                </select>                 
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label class="pull-left">Service</label>
                                <select class="form-control" id="filter2">
                                    <option value="" selected="true">All</option>
                                    @foreach($categories as $category)
    <option value="{{ $category->category_details[$admin->admin['language_id']-1]->name }}">{{ $category->category_details[$admin->admin['language_id']-1]->name }}</option>
                                    @endforeach
                                </select>
                        </div>
                    </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-stripped tablet breakpoint footable-loaded" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Service</th>
                                    <th>Category</th>
                                    <th>Product</th>
                                    <th>Value</th>
                                    <th>Alpha Price</th>
                                    <th>Price Per Quantity</th>
                                    <th>Price Per Distance</th>
                                    <th>Price Per Weight</th>
                                    <th>Price Per Hour</th>
                                    <th>Price Per Sq Meters</th>
                                    <!--<th>MOQ For Gift</th>
                                    <th>Gift Offer</th>-->
                                    <th>Action</th>
                                    <th>Updated At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($products as $product)
                <tr class="gradeA footable-odd">

                    <td>{{ $product->category_brand_product_id }}</td>

                    <td>
                        @if($product->blocked == '0')
                            <span class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i> Active</span>
                        @else
                            <span class="label label-danger"><i class="fa fa-eye-slash" aria-hidden="true"></i> Blocked</span>
                        @endif
                    </td>

                    <td>
                        @if($product->category_id == 1)
                            <span class="label label-success">
                        @elseif($product->category_id == 2)
                            <span class="label label-primary">
                        @elseif($product->category_id == 3)
                            <span class="label label-warning">
                        @elseif($product->category_id == 4)
                            <span class="label label-danger">
                        @else
                            <span class="label label-info">
                        @endif
                            {{ $product->category['category_details'][$admin->admin['language_id']-1]->name }}
                            </span>
                    </td>

                    <td><span class="label label-info"> {{ $product->category_brand['category_brand_details'][$admin->admin['language_id']-1]->name }} </span></td>

                    <td>
                        {{ $product->category_brand_product_details[$admin->admin['language_id']-1]->name }}
                    </td>

                    <td>
                        {{$product->actual_value}}
                    </td>

                    <td>
                       <b>OMR</b> {{$product->alpha_price}}
                    </td>

                    <td>
                        <b>OMR</b> {{$product->price_per_quantity}}
                    </td>

                    <td>
                        <b>OMR</b> {{$product->price_per_distance}}
                    </td>

                    <td>
                        <b>OMR</b> {{$product->price_per_weight }}
                    </td>

                    <td>
                        <b>OMR</b> {{$product->price_per_hr}}
                    </td>

                    <td>
                        <b>OMR</b> {{$product->price_per_sq_mt}}
                    </td>
					<!--<td>
                        {{$product-> gift_quantity}}
                    </td>

                    <td>
                        {{$product->gift_offer}}
                    </td>-->
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="{{route('admin_product_new_update',['category_brand_product_id'=>$product->category_brand_product_id])}}">
                                        <i class="fa fa-edit" aria-hidden="true"> Update</i>
                                    </a>
                                    
                                </li>

<!--                                 <li role="presentation">
<a role="menuitem" tabindex="-1" data-toggle="modal" data-target="#modal-form2" id="update_price" onclick="update_values('{{$product->category_brand_product_id}}', '{{$product->alpha_price}}', '{{$product->price_per_quantity}}', '{{$product->price_per_distance}}', '{{$product->price_per_weight}}', '{{$product->price_per_hr}}', '{{$product->price_per_sq_mt}}' )" >
    <i class="fa fa-edit" aria-hidden="true"> Update</i>
</a>
                                </li> -->

                                 <li role="presentation">
                                    @if($product->blocked == '0')
                                        <a role="menuitem" tabindex="-1" class="block_brand" id="{{$product->category_brand_product_id}}">
                                            <i class="fa fa-eye-slash"> Block</i>
                                        </a>
                                    @else
                                        <a role="menuitem" tabindex="-1" class="unblock_brand" id="{{$product->category_brand_product_id}}">
                                            <i class="fa fa-eye"> Unblock</i>
                                        </a>
                                    @endif
                                </li>

                            </ul>
                        </div>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $product->updated_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>

        </div>
    </div>
</div>

    <!--                Reply Modal                                -->
<!--     <div class="modal inmodal modal-form2 modal-bg" id="modal-form2" role="dialog" aria-hidden="false">
        <div class="modal-dialog">
            <form method="post" action="{{route('admin_ser_prod_update')}}" enctype="multipart/form-data" >
                <div class="modal-content animated swing">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <i class="fa fa-edit modal-icon"></i>
                        <h4 class="modal-title">Update</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                                <label>Alpha Price</label>
                                <input type="number" name="alpha_price" id="alpha_price" class="form-control" step="any" min="0" autofocus="on" required>
                            </div>

                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                               <label>Price Per Quantity</label>
                                <input type="number" name="price_per_quantity" id="price_per_quantity" class="form-control" step="any" min="0" required>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                                <label>Price Per Distance</label>
                                <input type="number" name="price_per_distance" id="price_per_distance" class="form-control" step="any" min="0" required>
                            </div>

                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                               <label>Price Per Weight</label>
                                <input type="number" name="price_per_weight" id="price_per_weight" class="form-control" step="any" min="0" required>
                            </div>
                        </div>

                        <br><br>
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                                <label>Price Per Hour</label>
                                <input type="number" name="price_per_hr" id="price_per_hr" class="form-control" step="any" min="0" required>
                            </div>

                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                               <label>Price Per Sq Meter</label>
                                <input type="number" name="price_per_sq_mt" id="price_per_sq_mt" class="form-control" step="any" min="0" required>
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="modal-footer">

                        <input hidden="on" value="" name="category_brand_product_id" id="category_brand_product_id"></input>

                        <button type="submit" class="btn btn-primary" id="add_rform_btn">Update</button>
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
 -->    <!--                Reply Modal                                -->

    <script>

        $("#services_all").addClass("active");
        $("#admin_ser_prods_all").addClass("active");

    // $(document).ready(function()
    //     {


    //     });


            var table = $('#example').dataTable( 
                {
                   //"order": [[ 2, "asc" ]],
                   // "scrollY": 1000,
                   // "scrollX": true,
                   "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [ 8 ] },
                                { "bSearchable": true, "aTargets": [ 2, 3, 4, 5, 6 ] }
                            ],
                   "fnDrawCallback": function( oSettings ) 
                        {


        $('.block_brand').click(function (e) {

              var id = $(e.currentTarget).attr("id");

                swal({
                    title: "Are you sure you want to block this product?",
                    text: "Users will not be able to access this product.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Block it!",
                    closeOnConfirm: false
                }, function () {
                    window.location.href = '{{route("admin_products_block")}}?block=1&category_brand_product_id='+id;
                });
            });

        $('.unblock_brand').click(function (e) {

                 var id = $(e.currentTarget).attr("id");

                swal({
                    title: "Are you sure you want to unblock this product?",
                    text: "Users will be able to access this product.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Unblock It!",
                    closeOnConfirm: false
                }, function () {
            window.location.href = '{{route("admin_products_block")}}?block=0&category_brand_product_id='+id;
                });
            });

                        }
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });
            $("#filter2").on('change', function()
                {
                    table.fnFilter($(this).val(), 2);
                });

        // function update_values(category_brand_product_id, alpha_price, price_per_quantity, price_per_distance, price_per_weight, price_per_hr, price_per_sq_mt)
        //     {

        //         document.getElementById('category_brand_product_id').value = category_brand_product_id;

        //         document.getElementById('alpha_price').value = alpha_price;
        //         document.getElementById('price_per_quantity').value = price_per_quantity;
        //         document.getElementById('price_per_distance').value = price_per_distance;
        //         document.getElementById('price_per_weight').value = price_per_weight;
        //         document.getElementById('price_per_hr').value = price_per_hr;
        //         document.getElementById('price_per_sq_mt').value = price_per_sq_mt;
                
        //     }

    </script>


@stop
