@extends('Admin.layout')

@section('title') 
    Update Support Category
@stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Update Support Category</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_support_cat_all')}}">All Support Categories</a>
                        </li>
                        <li>
                            <a href="{{route('admin_support_cat_update', ['category_id' => $category->category_id ] ) }}">
                                <b>Update Support Category</b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                <center>
                    <a href="{{$category->image_url}}" title="{{ $category->category_details[0]->name }}" class="lightBoxGallery" data-gallery="">
                        <img src="{{$category->image_url}}/120/120"  class="img-circle">
                    </a>
                </center>  



        <br>
            <form method="post" action="{{route('admin_support_cat_update_post', ['category_id' => $category->category_id ] ) }}" enctype="multipart/form-data" id="addForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>English Name</label>
                <input type="text" placeholder="English Name" autocomplete="off" class="form-control" required name="english_name" value="{{ $category->category_details[0]->name }}" autofocus="on" id="english_name">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Icon (150px X 150px)</label>
                <input type="file" name="image" class="form-control" accept="image/*" id="image">
            </div> 

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Hindi Name</label>
                <input type="text" placeholder="Hindi Name" autocomplete="off" class="form-control" required name="hindi_name" value="{{ $category->category_details[1]->name }}"id="hindi_name">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Urdu Name</label>
                <input type="text" placeholder="Urdu Name" autocomplete="off" class="form-control" required name="urdu_name" value="{{ $category->category_details[2]->name }}" id="urdu_name">
            </div> 

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Chinese Name</label>
                <input type="text" placeholder="Chinese Name" autocomplete="off" class="form-control" required name="chinese_name" value="{{ $category->category_details[3]->name }}" id="chinese_name">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Arabic Name</label>
                <input type="text" placeholder="Arabic Name" autocomplete="off" class="form-control" required name="arabic_name" value="{{ $category->category_details[4]->name }}" id="arabic_name">
            </div> 

        </div>
    </div>
<br>

    <div class="row">
        <div class="form-group">

            <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1">
                <label for="ranking">Ranking</label>
                <select class="form-control border-info" required name="ranking">
                    @for($i=1;$i<=$ranking;$i++)
                        @if($category->sort_order == $i)
                            <option value="{{$i}}" selected="true">{{$i}}</option>
                        @else
                            <option value="{{$i}}">{{$i}}</option>
                        @endif
                    @endfor
                </select>
            </div>

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Buraq Charges Percentage</label>
                <input type="number" step="any" max="100" min="0.1" placeholder="Buraq Charges%" class="form-control" required name="buraq_percentage" value="{!! $category->buraq_percentage !!}" id="buraq_percentage">
            </div>

        </div>
    </div>

<input type="hidden" name="total" value="{{$ranking}}">

            <br><br><br>

                <div class="row">
                    <div class="form-group">
                        <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            {!! Form::submit('Add Support Category', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>

<script type="text/javascript">

        $("#supports_all").addClass("active");
        $("#support_cat_all").addClass("active");

        $("#addForm").validate({
            rules: {
                english_name: {
                    required: true,
                    maxlength: 255,
                },
                image: {
                    accept: "image/*"
                },
                hindi_name: {
                    required: true,
                    maxlength: 255,
                },
                urdu_name: {
                    required: true,
                    maxlength: 255,
                },
                chinese_name: {
                    required: true,
                    maxlength: 255,
                },
                arabic_name: {
                    required: true,
                    maxlength: 255,
                },
                buraq_percentage: {
                    required: true,
                    minimun: 0.01,
                    maximum: 100
                }
 
            },
            messages: {
                english_name: {
                    required: "Please provide the english name"
                },
                image:{
                    accept: "Only Image is allowed"
                },
                hindi_name: {
                    required: "Please provide the hindi name"
                },
                urdu_name: {
                    required: "Please provide the urdu name"
                },
                chinese_name: {
                    required: "Please provide the chinese name"
                },
                arabic_name: {
                    required: "Please provide the arabic name"
                },
                buraq_percentage: {
                    required: "Buraq Percentage charge is required"
                }


            }
        });

</script>


@stop



