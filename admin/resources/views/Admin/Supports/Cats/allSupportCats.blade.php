    @extends('Admin.layout')

    @section('title')
        All Support Categories
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All Support Categories
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('admin_support_cat_all')}}"><b>All Support Categories</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="form-group">
                            <label class="pull-left">Status</label>
                                <select class="form-control" id="filter1">
                                    <option value="e">All</option>
                                    <option value="Blocked">Blocked</option>
                                    <option value="Active">Active</option>
                                </select>                 
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4"></div>

                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <br>
        <a href="{{ route('admin_support_cat_add') }}" class="btn btn-primary pull-right" id="Add Support Category" title="Add Support Category">
            <i class="fa fa-plus"></i> Add Support Category
        </a>
                    </div>


                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th width="5%">Support Id</th>
                                    <th width="5%">Status</th>
                                    <th width="5%">Ranking</th>
                                    <th width="50%">Name</th>
                                    <th width="10%">Image</th>
                                    <th width="10%">Action</th>
                                    <th width="15%">Updated At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($categories as $category)
                <tr class="gradeA footable-odd">

                    <td>{{ $category->category_id }}</td>

                    <td>
                        <center>
                            @if($category->blocked == '0')
                                <span class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i> Active</span>
                            @else
                                <span class="label label-danger"><i class="fa fa-eye-slash" aria-hidden="true"></i> Blocked</span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            <span class="label label-success"> {{$category->sort_order}} </span>    
                        </center>
                    </td>


                    <td>
                        <b>
                            {{ $category->category_details[0]->name }}
                        </b>(English)<hr>

                        <b>
                            {{ $category->category_details[1]->name }}
                        </b>(Hindi)<hr>

                        <b>
                            {{ $category->category_details[2]->name }}
                        </b>(Urdu)<hr>

                        <b>
                            {{ $category->category_details[3]->name }}
                        </b>(Chinese)<hr>

                        <b>
                            {{ $category->category_details[4]->name }}
                        </b>(Arabic)<hr>
                    </td>

                    <td>
                            <a href="{{ $category->image_url }}" title="{{ $category->category_details[0]->name }}" class="lightBoxGallery" data-gallery="">
                                <img src="{{ $category->image_url }}/120/120"  class="img-circle">
                            </a>
                    </td>


                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="{{route('admin_support_cat_update', ['category_id'=>$category->category_id])}}">
                                        <i class="fa fa-edit"> Update</i>
                                    </a>
                                </li>

                                <li role="presentation">
                                    @if($category->blocked == '0')
                                        <a role="menuitem" tabindex="-1" class="block_support" id="{{$category->category_id}}">
                                            <i class="fa fa-eye-slash"> Block</i>
                                        </a>
                                    @else
                                        <a role="menuitem" tabindex="-1" class="unblock_support" id="{{$category->category_id}}">
                                            <i class="fa fa-eye"> Unblock</i>
                                        </a>
                                    @endif
                                </li>

                            </ul>
                        </div>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $category->updated_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

        $("#supports_all").addClass("active");
        $("#support_cat_all").addClass("active");

            var table = $('#example').dataTable( 
                {
                    "order": [[ 2, "asc" ]],
                    "scrollX": true,
                    "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [ 4, 5 ] },
                                { "bSearchable": true, "aTargets": [ 1 ] }
                    ],
                   "fnDrawCallback": function( oSettings ) 
                        {

                            $('.block_support').click(function (e) {

                                  var id = $(e.currentTarget).attr("id");

                                    swal({
                                        title: "Are you sure you want to block this support?",
                                        text: "Users, Drivers will not be able to access this support.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Block it!",
                                        closeOnConfirm: false
                                    }, function () {
                                        window.location.href = '{{route("admin_support_category_block")}}?block=1&category_id='+id;
                                    });
                                });

                            $('.unblock_support').click(function (e) {

                                     var id = $(e.currentTarget).attr("id");

                                    swal({
                                        title: "Are you sure you want to unblock this support?",
                                        text: "Users, Drivers will be able to access this support.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Unblock It!",
                                        closeOnConfirm: false
                                    }, function () {
                                window.location.href = '{{route("admin_support_category_block")}}?block=0&category_id='+id;
                                    });
                                });

                        }
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });



    </script>


@stop
