@extends('Admin.layout')

@section('title') 
     Dashboard
@stop

@section('content')

    <link href="{{ URL::asset('AdminAssets/css/mystyle.min.css') }}" rel="stylesheet">

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <script src="{{ URL::asset('AdminAssets/js/plugins/fullcalendar/moment.min.js') }}"></script>

    <link href="{{ URL::asset('AdminAssets/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('AdminAssets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
		<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet"><!--   Add M3 Part 3.2   -->
		<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script><!--   Add M3 Part 3.2   -->
    <div class="wrapper wrapper-content animated" data-animation="rotateInUpLeft">

        <div class="row">
            <div class="col-md-offset-2 col-lg-6 col-lg-offset-2 col-md-6 col-sm-offset-2  col-sm-6">
                                        
                <center>
                    <label>Filter Data</label>
                </center>
                <center>
                    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$starting_dt}} - {{$ending_dt}}"/>
                </center>
                <br>
            </div>    
			<!--   Add M3 Part 3.2   -->
			<!--<div class="col-lg-4 col-md-4" style="margin-top: 22px;">
                <center>
                    <label>Round Robin Scheduling</label>  
					@if($admin['admin']->round_robin == "false")
						<input id="toggle-event" type="checkbox" data-toggle="toggle">
					@else
						<input id="toggle-event" type="checkbox" checked data-toggle="toggle" >
					@endif
                </center>
                <br>
            </div> --> 			
        </div>


<!--             <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="widget widget-stats bg-red">
                        <div class="stats-icon"><i class="fa fa-desktop"></i></div>
                        <div class="stats-info">
                            <h4>TOTAL VISITORS</h4>
                            <p>3,291,922</p>    
                        </div>
                        <div class="stats-link">
                            <a href="javascript:;">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
 -->
        <div class="row">

            <div class="col-lg-3 col-md-6">
                <div class="small-box bg-teal">
                    <div class="inner">
                        <h3 id="order_counts" class="order_counts">&nbsp;</h3>
                            <p>Orders</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-circle-thin"></i>
                    </div>
                    <a href="{{route('admin_orders')}}" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6" onclick="#">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3 id="revenue_counts" class="revenue_counts">&nbsp;</h3>
                            <p>Revenue</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-paypal"></i>
                    </div>
                    <a href="javascript:;" class="small-box-footer">
                        &nbsp;
                        <!-- More info <i class="fa fa-arrow-circle-right"></i> -->
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="small-box bg-maroon">
                    <div class="inner">
                        <h3 id="product_counts" class="product_counts">&nbsp;</h3>
                            <p>Products</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-product-hunt"></i>
                    </div>
                    <a href="{{route('admin_ser_prods_all')}}" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3 id="cancelled_counts" class="cancelled_counts">&nbsp;</h3>
                            <p>Cancelled Orders</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-bullhorn"></i>
                    </div>
                    <a href="javascript:;" class="small-box-footer">
                        &nbsp;
                        <!-- More info <i class="fa fa-arrow-circle-right"></i> -->
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="small-box bg-orange">
                    <div class="inner">
                        <h3 id="customer_cancelled_counts" class="customer_cancelled_counts">&nbsp;</h3>
                            <p>Customer Cancelled</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-bullseye"></i>
                    </div>
                    <a href="javascript:;" class="small-box-footer">
                        &nbsp;
                        <!-- More info <i class="fa fa-arrow-circle-right"></i> -->
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="small-box bg-fuchsia">
                    <div class="inner">
                        <h3 id="provider_cancelled_counts" class="provider_cancelled_counts">&nbsp;</h3>
                            <p>Provider Cancelled</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-ban"></i>
                    </div>
                    <a href="javascript:;" class="small-box-footer">
                        &nbsp;
                        <!-- More info <i class="fa fa-arrow-circle-right"></i> -->
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3 id="scheduled_counts" class="scheduled_counts">&nbsp;</h3>
                            <p>Scheduled Orders</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <a href="javascript:;" class="small-box-footer">
                        &nbsp;
                        <!-- More info <i class="fa fa-arrow-circle-right"></i> -->
                    </a>
                </div>
            </div>

            <!--<div class="col-lg-3 col-md-6" onclick="javascript:location.href='{{route('admin_org_all')}}'">
                <div class="small-box bg-light-blue">
                    <div class="inner">
                        <h3 id="org_counts" class="org_counts">&nbsp;</h3>
                            <p>Companies</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-building-o"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6" onclick="javascript:location.href='#'">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3 id="dsupport_counts" class="dsupport_counts">&nbsp;</h3>
                            <p>Support Drivers</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>-->
     

            <div class="col-lg-3 col-md-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3 id="customer_counts" class="customer_counts">&nbsp;</h3>
                            <p>Customers</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="{{route('admin_custs')}}" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        

            <div class="col-lg-3 col-md-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3 id="dservice_counts" class="dservice_counts">&nbsp;</h3>
                            <p>Service Yesser Man</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user-circle"></i>
                    </div>
                    <a href="{{route('admin_ser_dall_new')}}" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            

            <div class="col-lg-3 col-md-6">
                <div class="small-box bg-black">
                    <div class="inner">
                        <h3 id="review_counts" class="review_counts">&nbsp;</h3>
                            <p>Reviews</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-star"></i>
                    </div>
                    <a href="{{route('admin_all_reviews')}}" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

        <br>


        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Orders</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
<!--                             <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a> -->
                        </div>
                    </div>
                    <div class="ibox-content table-responsive">


           <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Type</th>
                                    <th>Customer</th>
                                    <th>CustomerPic</th>
                                    <th>Timing</th>
                                    <th>Pickup Location</th>
                                    <th>DropOff Location</th>
                                    <th>Payment</th>
									<th>Re-Assign</th>
                                    <th>Details</th>
                                    <th>Booked At</th>
                                </tr>

                                </thead>
                                <tbody>

            @foreach($orders as $order)
                <tr class="gradeA footable-odd" style="display: table-row;">

                    <td>{{ $order->order_id }}</td>

                    <td>
                        @include('Common.Partials.orderStatus')
                    </td>

                    <td>
                        @if($order->order_type == 'Service')
                            <span class="label label-info">
                                <i class="fa fa-certificate"> Service</i>
                            </span>
                        @else
                            <span class="label label-primary">
                                <i class="fa fa-support"> Support</i>
                            </span>
                        @endif
                    </td>

                    <td>
                        {{ $order['customer_user']->name }}
                    </td>

                    <td>
                        @if($order['customer_user_detail']->profile_pic == "")
                            <a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
                            </a>  
                         @else
                            <a href="{{ env('SOCKET_URL').'/Uploads/'.$order['customer_user_detail']->profile_pic }}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ env('SOCKET_URL').'/Uploads/'.$order['customer_user_detail']->profile_pic }}"  class="img-circle">
                            </a>
                         @endif
                    </td>


                    <td><i class="fa fa-clock-o"></i> {{ $order->order_timingsz }} </td>

                    <td style="height:100px;overflow:auto;">
                        {!! $order->pickup_address !!}
                    </td>

                    <td style="height:100px;overflow:auto;">
                        {{ $order->dropoff_address }}
                    </td>

                    <!--<td>
                       <b>OMR</b> {{ $order->payment['final_charge'] }}
                    </td>-->
					<td>
					   <?php $totalPrice = array(); for($i = 0;$i<count($order->order_products);$i++){
							$totalPrice[] = $order->order_products[$i]['price_per_item'];
						 } // Part M3 3.2
							$totalPrice[] = $order->floor_charge; 
							$total = array_sum($totalPrice) - $order->reward_discount_price ; ?>
						<b>OMR</b>  <?php echo number_format($total,2); ?>
                    </td>
					@if($order->category['reassign_status'] == '0') 
						@if($order->order_status == "Driver not found" || $order->order_status == "SerTimeout" || $order->order_status == "SupTimeout" || $order->order_status == "DSchTimeout" || $order->order_status == "CTimeout" || $order->order_status == "SerReject" || $order->order_status == "SupReject" || $order->order_status == "DriverCancel" || $order->order_status == "DSchCancelled" )
							<td><a onclick="ShowDrivers({{ $order->order_id }})" class="btn btn-primary pull-right"> Assign Yesser Man </a></td>
						@elseif($order->order_status == "Confirmed")
							<td><a onclick="ShowDriversShuffle({{ $order->order_id }})" class="btn btn-primary pull-right"> Re-Shuffle Order </a></td>
						@else
							<td></td>
						@endif
					@else
						<td></td>
					@endif
                    <td>
            <a href="{{ route('admin_order_details', ['order_id' => $order->order_id] ) }}" class="btn btn-primary pull-right" id="Details" title="Details">
                <i class="fa fa-ioxhost"> Order Details</i>
            </a>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
								<tfoot>
                <center>
                    <h2>Total - {{ $orders->total() }}</h2>
                </center>
                <center>
                    {{ $orders->links() }}
                </center>
                                </tfoot>
                            </table>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Yearly Stats</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
<!--                             <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a> -->
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="row">
                            <center>
                            <div class="col-md-9 col-sm-9 col-lg-9">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" value="<?php echo date("Y"); ?>" id="starting_date" name="starting_date">
                                </div>                                
                            </div>
                            <div class="col-md-3 col-sm-3 col-lg-3">
                                <button class="button btn btn-primary btn-lg" onclick="new_get_dashboard_data()">
                                    Get Details
                                </button>
                            </div>                                
                            </center>
                        </div>
                        <div id="container1" style="min-width: 100%; height: 100%; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
        </div>

        </div>

    </div>
<!-- Modal: modalCart -->
<div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header" style="background-color: black;">
        <h4 class="modal-title" style="color: white;" id="myModalLabel">Yesser Man List</h4>
        <button type="button" style="margin-top: -20px;opacity: 5;color: white;" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <!--Body-->
      <div class="modal-body">

        <table class="table table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>Yesser Man name</th>
              <th>Yesser Man Phone Number</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody id="appendList">
            
          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>
<!-- Modal: modalCart -->
    <br><br>

    <script>

    $("#dashboard").addClass("active");

    var latitude, longitude;

    $(document).ready(function() 
        {

            navigator.geolocation.getCurrentPosition(function(position) {

                //toastr.success('Success.', 'Location access is allowed.');
                
                latitude = position.coords.latitude;
                longitude = position.coords.longitude;

                new_get_dashboard_data();

            }, function() {

                toastr.warning('Warning.', 'Location access is denied.');

                latitude = null;
                longitude = null;

                new_get_dashboard_data();

            });

            toastr.success('Welcome to {{env("APP_NAME")}} Dashboard', '{{ $admin->name }}');

                $("#starting_date").datepicker( {
                    format: "yyyy",
                    viewMode: "years", 
                    minViewMode: "years"
                        });

                $('input[name="daterange"]').daterangepicker({
                        format: 'DD/MM/YYYY',
                        minDate: '1/2/2018',
//                        maxDate: moment().format('DD/MM/YYYY'),
                        opens: 'center',
                        drops: 'down',
                        buttonClasses: ['btn', 'btn-sm'],
                        applyClass: 'btn-primary',
                        cancelClass: 'btn-default'
                            });

                setInterval(function()
                    {
                        new_get_dashboard_data();
                    }, 60000);

                $('#daterange').on('apply.daterangepicker', function(ev, picker) 
                    {
                        new_get_dashboard_data();
                    });

            });

        function new_get_dashboard_data()
            {

                dt = document.getElementById('daterange').value;
                yr = document.getElementById('starting_date').value;

                dt = encodeURIComponent(dt);
                route = '{!! route('admin_dash_data') !!}?daterange='+dt+'&yr_only='+yr+'&latitude='+latitude+'&longitude='+longitude;
                console.log(route);
                $.ajax
                    ({
                        url: route,
                        type: 'GET',
                        async: true,
                        dataType: "json",
                        success: function (data) 
                            {
                                if(data.success == '0')
                                    toastr.error('Error.',data.message);
                                else
                                    {
                                        assign_dashboard_data(data.stats);
                                        graph_data(data);
                                    }
                            }
                    });

            }

        function assign_dashboard_data(stats)
            {

                document.getElementById('order_counts').innerHTML = stats.order_counts;
                document.getElementById('revenue_counts').innerHTML = stats.revenue_counts;
                document.getElementById('product_counts').innerHTML = stats.product_counts;

                document.getElementById('cancelled_counts').innerHTML = stats.cancelled_counts;
                document.getElementById('customer_cancelled_counts').innerHTML = stats.customer_cancelled_counts;
                document.getElementById('provider_cancelled_counts').innerHTML = stats.provider_cancelled_counts;
                document.getElementById('scheduled_counts').innerHTML = stats.scheduled_counts;                

                //document.getElementById('org_counts').innerHTML = stats.org_counts;
                document.getElementById('customer_counts').innerHTML = stats.customer_counts;
                document.getElementById('dservice_counts').innerHTML = stats.dservice_counts;
                //document.getElementById('dsupport_counts').innerHTML = stats.dsupport_counts;
                
                document.getElementById('review_counts').innerHTML = stats.review_counts;
                
                return '1';

            }

      function graph_data(data)
        {
            document.getElementById('starting_date').value = data.graph_data.dt;

            $('#container1').highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Monthly Average Stats'
                },
                xAxis: {
                    categories: [
                        'January',
                        'February',
                        'March',
                        'April',
                        'May',
                        'June',
                        'July',
                        'August',
                        'September',
                        'October',
                        'November',
                        'December'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    allowDecimals: false,
                    title: {
                        text: 'COUNT'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [
                /*{
                    name: 'Companies',
                    data: data.graph_data.orgs,
                    color: "#3c8dbc"
                },*/
                {
                    name: 'Customers',
                    data: data.graph_data.customers,
                    color: "#00a65a"
                }, 
                {
                    name: 'Service Yesser Man',
                    data: data.graph_data.dservices,
                    color: "#B16B1E"
                }, 
                /*{
                    name: 'Support Drivers',
                    data: data.graph_data.dsupports,
                    color: "#f56954"
                },*/
                {
                    name: 'Orders',
                    data: data.graph_data.orders,
                    color: "#39cccc"
                },
                {
                    name: 'Reviews',
                    data: data.graph_data.reviews,
                    color: "#222222"
                }

                ]
            });

        }
		
		function ShowDrivers(id)
		{
			route = '{!! route('admin_reassign_driver') !!}?orderId='+id;
			console.log(route);
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					console.log(data);
					$('#appendList').empty();
					var HTML = "";
					for(var i =0;i<data.length;i++)
					{
						var cusername           = data[i].name;
						var DriverUserId        = data[i].user_id;
						var DriverUserDetailsId = data[i].user_detail_id;
						var Driverphonenumber   = data[i].phone_number;
						HTML += "<tr>";
						HTML += "<td>"+DriverUserId+"</td>";
						HTML += "<td>"+cusername+"</td>";
						HTML += "<td>"+Driverphonenumber+"</td>";
						HTML += "<td><a onclick='sendRequest("+DriverUserDetailsId+","+id+")' class='btn btn-primary pull-right'> Send Request </a></td>";
						HTML += "</tr>";
					}
					if(HTML == "")
					{
						HTML += "<tr>";
						HTML += "<td colspan='3'>No Yesser Man Found</td>";
						HTML += "</tr>";
					}
					$('#appendList').html(HTML);
					$('#modalCart').modal('show');
				}
			});
			
		}
		
		function sendRequest(driverId,orderId)
		{
			route = '{!! route('admin_assign_order_driver') !!}?orderId='+orderId+'&driverId='+driverId;
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					$('#modalCart').modal('hide');
					swal({
						title: "Send Request Successfully",
						type: "success",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Okay",
						closeOnConfirm: false
					}, function(){  
						window.location.reload();
					});
				}
			});
		}
		
		function ShowDriversShuffle(id)
		{
			route = '{!! route('admin_reassign_driver') !!}?orderId='+id;
			console.log(route);
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					$('#appendList').empty();
					var HTML = "";
					for(var i =0;i<data.length;i++)
					{
						var cusername           = data[i].name;
						var DriverUserId        = data[i].user_id;
						var DriverUserDetailsId = data[i].user_detail_id;
						var AccptedStatus       = data[i].accepted_status;
						var Driverphonenumber   = data[i].phone_number;
						HTML += "<tr>";
						HTML += "<td>"+DriverUserId+"</td>";
						HTML += "<td>"+cusername+"</td>";
						HTML += "<td>"+Driverphonenumber+"</td>";
						if(AccptedStatus == '1'){
							HTML += "<td><a class='btn btn-primary pull-right'> Already Accepted Request </a></td>";
						}else{
							HTML += "<td><a onclick='sendAcceptRequest("+DriverUserDetailsId+","+id+")' class='btn btn-primary pull-right'> Assign Request </a></td>";
						}
						HTML += "</tr>";
					}
					if(HTML == "")
					{
						HTML += "<tr>";
						HTML += "<td colspan='3'>No Yesser Man Found</td>";
						HTML += "</tr>";
					}
					$('#appendList').html(HTML);
					$('#modalCart').modal('show');
				}
			});
			
		}
		
		function sendAcceptRequest(driverId,orderId)
		{
			route = '{!! route('admin_shuffle_order_driver') !!}?orderId='+orderId+'&driverId='+driverId;
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					$('#modalCart').modal('hide');
					swal({
						title: "Driver Assigned Successfully",
						type: "success",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Okay",
						closeOnConfirm: false
					});
				}
			});
		}
		/*
		*Round Robin Enable/Disable
		*Add M3 Part 3.2
		*/
		$('#toggle-event').change(function() {
			var value = $(this).prop('checked');
			console.log(value);
			route = '{!! route('admin_roundrobin_enabledisable') !!}?value='+value;
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					console.log("success");
				}
			});
		})
    </script>

@stop



