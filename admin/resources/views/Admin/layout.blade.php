<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="{{ URL::asset('AdminAssets/Images/favicon_yesser.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ URL::asset('AdminAssets/Images/favicon_yesser.ico') }}" type="image/x-icon">

    <title>{{env('APP_NAME')}} - @yield('title')</title>

    <script src="{{ URL::asset('AdminAssets/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/bootstrap.min.js') }}"></script>

    <link href="{{ URL::asset('AdminAssets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="{{ URL::asset('AdminAssets/css/animate.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('AdminAssets/css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('AdminAssets/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('AdminAssets/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('AdminAssets/css/plugins/blueimp/css/blueimp-gallery.min.css') }}" rel="stylesheet">

           <!-- Data Tables -->
    <link href="{{ URL::asset('AdminAssets/css/plugins/dataTables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('AdminAssets/css/plugins/dataTables/dataTables.responsive.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('AdminAssets/css/plugins/dataTables/dataTables.tableTools.min.css') }}" rel="stylesheet">

    <script src="{{ URL::asset('AdminAssets/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/dataTables/dataTables.responsive.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/dataTables/dataTables.tableTools.min.js') }}"></script>

    <link href="{{ URL::asset('AdminAssets/css/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">

    <script src="{{ URL::asset('AdminAssets/js/plugins/toastr/toastr.min.js') }}"></script>

    <style>

        body
            {
                font-size:12px !important;
            }
        .btn
            {
                font-size: 12px !important;
            }
        .lightBoxGallery
            {
                text-align: center;
            }
        .lightBoxGallery img
            {
                margin: 5px;
            }
        .img-circle
            {
                width: 50px !important;
                height: 50px !important;
            }
        .tableclass
            {
                    overflow-x: overlay;
            }

        th,tr
            {
                text-align: center !important;
            }

        td
            {
                padding-top:15px !important;
            }
        .low_padding
            {
                padding-top:5px !important;
            }

        input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button 
            {
                -webkit-appearance: none; 
                margin: 0;
            }
			#notification-latest {
				color: black;
				position: absolute;
				right: 0px;
				background: #F3F3F4;
				box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.20);
				text-align: left;
				height: 170px;
				overflow: auto;
				width: 500px;
			}
			.notification-item {
				padding:10px;
				border-bottom: #3ae2cb 1px solid;
				cursor:pointer;
				border: 2px solid whitesmoke;
				position: relative;
				background: #e6e6e6;
				font-weight: 600;
				text-transform: capitalize;
			}
			.notification-item span{
				position: absolute;
				width: 7px;
				height: 34px;
				top: 0px;
				left: 2px;
			}	
			.notification-subject {		
			  white-space: nowrap;
			  overflow: hidden;
			  text-overflow: ellipsis;
			}
			.notification-comment {		
			  white-space: nowrap;
			  overflow: hidden;
			  text-overflow: ellipsis;
			  font-style:italic;
			}
			.num{
				position: absolute;
				color: #ffffff;
				right: 17px;
				top: 7px;
				background: none 0% 0% repeat scroll red;
				border-radius: 50%;
				height: 18px;
				display: block;
				width: 18px;
				height: 18px;
				text-align: center;
			}
			#notificationTitle
			{
				font-weight: bold;
				padding: 8px;
				font-size: 13px;
				background-color: #ffffff;
				z-index: 1000;
				width: 384px;
				border-bottom: 1px solid #dddddd;
				width: 500px;
			}
			.lightBoxGallery .logo {
                max-height: 100px;
                margin: 0 auto;
                display: block;
			}
			.profile-element a.dropdown-toggle, .nav-header .text-muted {
					color: #fff;
					text-align: center;
					font-size: 14px;
				}
				.nav > li.active {
					border: none;
				}
			
    </style>

</head>

<body class="pace-done" cz-shortcut-listen="true">

<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                
                <li class="nav-header">
                    <div class="dropdown profile-element">

                        <span>
                            <a href="{{ URL::asset('BuraqExpress/Uploads/'.$admin['admin']->profile_pic) }}" title="{{ $admin['admin']->name }}" class="lightBoxGallery" data-gallery="">
                                <img alt="image" class="logo" src="{{ URL::asset('BuraqExpress/Uploads/'.$admin['admin']->profile_pic) }}" />
                            </a>
                        </span>

                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                                    <strong class="font-bold">{{ $admin['admin']->name }}</strong>
                                </span>
                                <span class="text-muted text-xs block">
                                    {{ $admin['admin']->job }}
                                    <b class="caret"></b>
                                </span>
                            </span>
                         </a>

                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li>
                                <a href="{{route('aprofile_update_get')}}"><i class="fa fa-rouble"></i> Profile Update</a>
                            </li>
                            <li>
                                <a href="{{route('apassword_update_get')}}"><i class="fa fa-cog"></i> Password Update</a>
                            </li>
							
<!--                             <li>
                                <a href="{{route('setting_update_get')}}"><i class="fa fa-random"></i> Setting Update</a>
                            </li> -->
                            <li class="divider"></li>
                            <li>
                                <a id="demo331" class="demo331" ><i class="fa fa-sign-out"></i> Logout</a>
                            </li>
                        </ul>

                    </div>
                    <div class="logo-element">
                        YS
                    </div>
                </li>

                <li id="dashboard" title="Dashboard">
                    <a href="{!! route('admin_dashboard') !!}">
                        <i class="fa fa-th-large"></i>
                        <span class="nav-label">Dashboard</span>
                    </a>
                </li>
				<li id="emergency_all"  title="EContacts">
					<a href="{{route('admin_econtacts_all')}}">
						<i class="fa fa-tty"></i>  <span class="nav-label">EContacts</span>
					</a>
				</li>
                <!--<li id="settings_all" title="Settings" onclick="javascript:location.href='#' ">
                    <a href="#">
                        <i class="fa fa-wrench"></i>
                        <span class="nav-label">Settings</span>
                        <span class="fa arrow"></span>
                    </a>

                    <ul class="nav nav-second-level collapse" style="height: 0px;">
                        <li id="emergency_all">
                            <a href="{{route('admin_econtacts_all')}}" title="EContacts">
                                <i class="fa fa-tty"></i> EContacts
                            </a>
                        </li>
                    </ul>

                    <ul class="nav nav-second-level collapse" style="height: 0px;">
                        <li id="coupons_all">
                            <a href="{{route('admin_coupons_all')}}" title="Coupons">
                                <i class="fa fa-tags"></i> Coupons
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-second-level collapse" style="height: 0px;">
                       <li id="promocode_all">
                           <a href="{{route('admin_promocode_all')}}" title="Promocode">
                               <i class="fa fa-tags"></i> Promocode
                           </a>
                       </li>
                   </ul>
                    <ul class="nav nav-second-level collapse" style="height: 0px;">
                       <li id="notification_all">
                           <a href="{{route('admin_notification_all')}}" title="Notification">
                               <i class="fa fa-bell "></i> Notification
                           </a>
                       </li>
                   </ul>
					<ul class="nav nav-second-level collapse" style="height: 0px;">
                        <li id="floormargin_all">
                            <a href="{{route('admin_floormargin_all')}}" title="Floor Margin">
                                <i class="fa fa-building-o "></i>Floor Margin
                            </a>
                        </li>
                    </ul>
					<ul class="nav nav-second-level collapse" style="height: 0px;">
                        <li id="rewardpoint_all">
                            <a href="{{route('admin_rewardpoint_all')}}" title="Reward Point">
                                <i class="fa fa-star"></i> Reward Point
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-second-level collapse" style="height: 0px;">
                        <li id="admin_login_history">
                            <a href="{{route('admin_login_history')}}" title="Logins">
                                <i class="fa fa-keyboard-o"></i> Logins
                            </a>
                        </li>
                    </ul>
					<ul class="nav nav-second-level collapse" style="height: 0px;">
                        <li id="admin_round_robin">
                            <a href="{{route('aroundrobin_update_get')}}" title="Round-Robin">
                                <i class="fa fa-cog"></i> Round-Robin
                            </a>
                        </li>
                    </ul>
					
                    <ul class="nav nav-second-level collapse" style="height: 0px;">
                        <li>
                            <a href="{{route('admin_language_all')}}" id="language_all" title="Languages">
                                <i class="fa fa-language"></i> Languages
                            </a>
                        </li>
                    </ul> 
                </li>-->

                <li id="drivers_maps" title="Yesser Man Map">
                    <a href="{!! route('admin_drivers_map') !!}">
                        <i class="fa fa-map"></i>
                        <span class="nav-label">Yesser Man Map</span>
                    </a>
                </li>

                <li id="services_all" title="Services" onclick="javascript:location.href='#' ">
                    <a href="javascript:;">
                        <i class="fa fa-certificate"></i>
                        <span class="nav-label">Services</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="height: 0px;">
                        <li id="service_cat_all">
                            <a href="{{route('admin_service_cat_all')}}" title="Services">
                                <i class="fa fa-certificate"></i> Services
                            </a>
                        </li>
<!--                         <li id="admin_ser_dall">
                            <a href="{{route('admin_ser_dall')}}" title="Drivers">
                                <i class="fa fa-user-circle"></i> Drivers
                            </a>
                        </li> -->
                        <li id="admin_ser_dall">
                            <a href="{{route('admin_ser_dall_new')}}" title="Yesser Man">
                                <i class="fa fa-user-circle"></i> Yesser Man
                            </a>
                        </li>

                        <li id="admin_ser_prods_all">
                            <a href="{{route('admin_ser_prods_all')}}" title="Products">
                                <i class="fa fa-product-hunt"></i> Products
                            </a>
                        </li>
                        <li id="admin_product_new_add">
                            <a href="{{route('admin_product_new_add')}}" title="Product Add">
                                <i class="fa fa-plus"></i> Product Add
                            </a>
                        </li>
                    </ul>
                </li>

                <!--<li id="supports_all" title="Supports" onclick="javascript:location.href='#' ">
                    <a href="#">
                        <i class="fa fa-support"></i>
                        <span class="nav-label">Supports</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="height: 0px;">
                        <li id="support_cat_all">
                            <a href="{{route('admin_support_cat_all')}}" title="Supports">
                                <i class="fa fa-support"></i> Supports
                            </a>
                        </li>
                        <li id="service_cat_drivers">
                            <a href="#" title="Drivers">
                                <i class="fa fa-user"></i> Drivers
                            </a>
                        </li>
                    </ul>
                </li>

                <li id="organisations_all" title="Companies" onclick="javascript:location.href='#' ">
                    <a href="#">
                        <i class="fa fa-building-o"></i>
                        <span class="nav-label">Companies</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="height: 0px;">
                        <li id="all_organisations">
                            <a href="{{route('admin_org_all')}}" title="Companies">
                                <i class="fa fa-building-o"></i> Companies
                            </a>
                        </li>
                        <li id="add_organisations">
                            <a href="{{route('admin_org_add')}}" title="Add Company">
                                <i class="fa fa-plus"></i> Add Company
                            </a>
                        </li>
                    </ul>
                </li>
				<li id="releaseoffer" title="releaseoffer">
                    <a href="{!! route('admin_release_offer') !!}">
                        <i class="fa fa-circle-thin"></i>
                        <span class="nav-label">Sent Offer</span>
                    </a>
                </li>
				<li id="ledgers" title="Ledgers">
                    <a href="{!! route('admin_ledger_all') !!}">
                        <i class="fa fa-book"></i>
                        <span class="nav-label">Ledgers</span>
                    </a>
                </li>
				<li id="chartanalysis" title="Chart Analysis">
                    <a href="{!! route('admin_chartanalysis') !!}">
                        <i class="fa fa-bar-chart"></i>
                        <span class="nav-label">Chart Analysis</span>
                    </a>
                </li>
				-->

                <li id="payments_all" title="Payments" onclick="javascript:location.href='#' ">
                    <a href="javascript:;">
                        <i class="fa fa-paypal"></i>
                        <span class="nav-label">Payments</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="height: 0px;">
                        <li id="admin_ser_pay_all">
                            <a href="{{route('admin_ser_pay_all')}}" title="Service Payments">
                                <i class="fa fa-certificate"></i> Service Payments
                            </a>
                        </li>
                       <!-- <li id="admin_sup_pay_all">
                            <a href="{{route('admin_sup_pay_all')}}" title="Support Payments">
                                <i class="fa fa-support"></i> Support Payments
                            </a>
                        </li>
                        <li id="admin_coupon_pay_all">
                            <a href="{{route('admin_coupon_pay_all')}}" title="Coupons Payments">
                                <i class="fa fa-tags"></i> Coupons Payments
                            </a>
                        </li>
                        <li id="admin_etoken_pay_all">
                            <a href="{{route('admin_etoken_pay_all')}}" title="eTokens Payments">
                                <i class="fa fa-eraser"></i> eTokens Payments
                            </a>
                        </li>-->
                    </ul>
                </li>
                <li id="organisations_all" title="Companies" onclick="javascript:location.href='#' ">
                    <a href="#">
                        <i class="fa fa-building-o"></i>
                        <span class="nav-label">Companies</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="height: 0px;">
                        <li id="all_organisations">
                            <a href="{{route('admin_org_all')}}" title="Companies">
                                <i class="fa fa-building-o"></i> Companies
                            </a>
                        </li>
                        <li id="add_organisations">
                            <a href="{{route('admin_org_add')}}" title="Add Company">
                                <i class="fa fa-plus"></i> Add Company
                            </a>
                        </li>
                    </ul>
                </li>
                <li id="customers" title="Customers">
                    <a href="{!! route('admin_custs') !!}">
                        <i class="fa fa-users"></i>
                        <span class="nav-label">Customers</span>
                    </a>
                </li>

                <li id="orders" title="Orders">
                    <a href="{!! route('admin_orders') !!}">
                        <i class="fa fa-circle-thin"></i>
                        <span class="nav-label">Orders</span>
                    </a>
                </li>
				
				
				
                <li id="reviews" title="Reviews">
                    <a href="{!! route('admin_all_reviews') !!}">
                        <i class="fa fa-star"></i>
                        <span class="nav-label">Reviews</span>
                    </a>
                </li>

                

                <li id="contactus" title="Contact Us">
                    <a href="{!! route('admin_contactus') !!}">
                        <i class="fa fa-wechat"></i>
                        <span class="nav-label">Contact Us</span>
                    </a>
                </li>
				

            </ul>

        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">

        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#" style="background-color: #E47834; border-color: #E47834;">
        <i class="fa fa-bars"></i>
    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
				
					<li class="clickable">
                        <!--<a class="notification-click" onclick="showNotification()" style="position: relative;display: block;height: 50px;width: 50px;background-size: contain;text-decoration: none;">
                            <span class="num" style="display:none;"></span>
                            <i class="fa fa-bell"></i>
                        </a>-->
						<meta name="csrf-token" content="{{ csrf_token() }}">
						
						<div id="notification-latest" style="display:none;"></div>
                    </li>
                    <li>
                        <a id="demo331" class="demo331">
                            <i class="fa fa-sign-out"></i> Logout
                        </a>
                    </li>

                    <li>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="text-muted text-xs block">
                                    <b class="caret"></b>
                                </span>
                            </span>
                        </a>   

                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li>
                                <a href="{{route('aprofile_update_get')}}"><i class="fa fa-rouble"></i> Profile Update</a>
                            </li>
                            <li>
                                <a href="{{route('apassword_update_get')}}"><i class="fa fa-cog"></i> Password Update</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a id="demo331" class="demo331" ><i class="fa fa-sign-out"></i> Logout</a>
                            </li>
                        </ul>

                    </li>


                </ul>

            </nav>
        </div>

        @foreach($errors->all() as $error)
            <div class="alert alert-dismissable alert-danger">
                {!! $error !!}
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            </div>
        @endforeach

        @if(session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            </div>
        @endif

       @yield('content')

        <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

    </div>

            <div class="footer fixed">
                <span class="pull-right">
                    <div id="google_translate_element"></div>
                </span>
                {{env('APP_NAME')}} &copy {{ now()->year }}
            </div>

    <script src="{{ URL::asset('AdminAssets/js/plugins/fullcalendar/moment.min.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/daterangepicker/daterangepicker.js') }}"></script>

    <script src="{{ URL::asset('AdminAssets/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/inspinia.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/pace/pace.min.js') }}"></script>

    <script src="{{ URL::asset('AdminAssets/js/plugins/blueimp/jquery.blueimp-gallery.min.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/idle-timer/idle-timer.min.js') }}"></script>

    <script>

        $(document).ready(function()
            {
            ///////////////////////////////////  Logout Function /////////////////////////////////////////////////
                $('.demo331').click(function ()
                    {

                        swal({
                            title: "Are you sure?",
                            text: "You want to logout!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes, Logout!",
                            closeOnConfirm: false
                        },
                        function(){
                            window.location.href = '{{route('admin_logout')}}';
                        });

                    });
                        ///////////////////////////////////////         Toaster  //////////////////////////////////////////////////

                // $(document).idleTimer(300000);

                // $(document).bind("idle.idleTimer", function(){
                //     window.location.href = '';
                // });

                $(document).on( "active.idleTimer", function(event, elem, obj, triggerevent)
                    {
                        toastr.clear();
                        $('.success-alert').fadeOut();
                        toastr.success('Great.','You are back. ');

                    });
				/*$('#notification-latest').click(function()
				{
					$('#notification-latest').css('display','none');
				})*/
				$(document).on("click",".hidenotification",function() {
					setTimeout(function(){
						$("#notification-latest").fadeOut("slow");
						$('.clickable .notification-click').removeClass("disabled").prop("disabled", false);
						$(".clickable .notification-click").css('pointer-events','auto');
					},500);
				});
            });
			
			function showNotification()
			{
				$.ajax({
					type: "POST",
					cache: false,
					dataType: "json",
					url: '{{route("notification_listing")}}',
					headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: {
						category_id: 1,
						admin_language_id: '1',
						is_read: '1'
					},
					success: function(data){
						//console.log(data.con);
						$(".num").css('display','none');					
						$("#notification-latest").show();
						$("#notification-latest").html(data.con);
						if(data.NotificationStatus == "No Notification")
						{
							$("#notification-latest").css('height','80px');
							setTimeout(function(){
								$("#notification-latest").fadeOut("slow");
								$('.clickable .notification-click').removeClass("disabled").prop("disabled", false);
							},2000);
						}else{
							$('.clickable .notification-click').addClass("disabled").prop("disabled", true);
							$(".disabled").css('pointer-events','none');
							$("#notification-latest").css('height','170px');
							setTimeout(function(){
								$("#notification-latest").fadeOut("slow");
								$('.clickable .notification-click').removeClass("disabled").prop("disabled", false);
								$(".clickable .notification-click").css('pointer-events','auto');
							},30000);
						}
					}          
				});
			}
			//setInterval(UpdateNotification,'10000');
			function UpdateNotification()
			{
				$.ajax({
					type: "POST",
					cache: false,
					dataType: "json",
					url: '{{route("notification_listing")}}',
					headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: {
						category_id: 1,
						admin_language_id: '1',
						is_read: '0'
					},
					success: function(data){
						$(".num").css('display','none');
						$(".num").html('');
						if(data.totalNotification != '0'){
							$(".num").html(data.totalNotification);	
							$(".num").css('display','block');
						}
						
					}          
				});
			}
			window.setTimeout(function() {
				$(".alert").fadeTo(500, 0).slideUp(500, function(){
					$(this).remove(); 
				});
			}, 3000);
			
    </script>

</div>

    <div id="blueimp-gallery" class="blueimp-gallery">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>

</body>

</html>
