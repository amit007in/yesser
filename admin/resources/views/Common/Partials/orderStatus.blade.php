@if($order->order_status  == "Searching")
    <span class="label label-warning">
        Searching
    </span>
@elseif($order->order_status  == "Confirmed")
    <span class="label label-warning">
        Confirmed
    </span>
@elseif($order->order_status == "Scheduled" || $order->order_status == "DApproved")
    <span class="label label-default">
        Scheduled
    </span>
@elseif($order->order_status  == "Ongoing" || $order->order_status == 'SerCustPending')
    <span class="label label-warning">
        Ongoing
    </span>
@elseif($order->order_status == "SerReject" || $order->order_status == "SupReject")
    <span class="label label-danger">
        Rejected
    </span>
@elseif($order->order_status == "SerTimeout" || $order->order_status == "SupTimeout" || $order->order_status == "DSchTimeout" || $order->order_status == "CTimeout")
    <span class="label label-danger">
        Timeout
    </span>
@elseif($order->order_status == "CustCancel" || $order->order_status == "SerCustCancel")
    <span class="label label-danger">
        Customer Cancelled
    </span>
@elseif($order->order_status == "DriverCancel" || $order->order_status == "DSchCancelled")
    <span class="label label-danger">
        Yesser Man Cancelled
    </span>
@elseif(($order->order_status == "SerComplete") || $order->order_status == "SupComplete" || $order->order_status == 'SerCustConfirm')
    <span class="label label-primary">
        Completed
    </span>
@elseif($order->order_status == "DPending")
    <span class="label label-info">
        Confirmation Pending
    </span>
@elseif($order->order_status == "Driver not found")
    <span class="label label-info">
        Yesser Man not found
    </span>
@else
    <span class="label label-success">
        {{ $order->order_status }}
    </span>
@endif