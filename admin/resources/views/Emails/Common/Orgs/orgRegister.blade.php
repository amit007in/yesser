<!DOCTYPE html>
<html>
<head>
   <title>{{ trans('messages.Welcome to') }} {{env('APP_NAME')}}</title>
</head>
<body>

<div id=":1ct" class="ii gt adP adO"><div id=":1cu" class="a3s aXjCH m1577958eda9b4b31"><u></u>
    
    <div style="height:100%;margin:0;padding:0;width:100%;background-color:#ffffff">
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="m_-308924936971670384bodyTable" style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#ffffff">
                <tbody><tr>
                    <td align="center" valign="top" id="m_-308924936971670384bodyCell" style="height:100%;margin:0;padding:10px;width:100%;border-top:0">
                  
                  
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-308924936971670384templateContainer" style="border-collapse:collapse;border:0;width:100% !important">
                     <tbody>
                     <tr>
                        <td valign="top" id="m_-308924936971670384templateHeader" style="background:#ffffff none no-repeat center/cover;background-color:#ffffff;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:0;padding-bottom:0"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-308924936971670384mcnImageBlock" style="min-width:100%;border-collapse:collapse">
    <tbody class="m_-308924936971670384mcnImageBlockOuter">
            <tr>
                <td valign="top" style="padding:9px" class="m_-308924936971670384mcnImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="m_-308924936971670384mcnImageContentContainer" style="min-width:100%;border-collapse:collapse">
                        <tbody><tr>
                            <td class="m_-308924936971670384mcnImageContent" valign="top" style="padding-right:9px;padding-left:9px;padding-top:0;padding-bottom:0;text-align:center">
                                
                                    
                                        <img align="center" alt="" src="https://gallery.mailchimp.com/a1b8544410046d95502c1b9ba/images/c4a426bc-c94f-419a-ac1e-125f7370208a.png" style="max-width:341px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none" class="m_-308924936971670384mcnImage CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 502.5px; top: 260px;"><div id=":2b9" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="aSK J-J5-Ji aYr"></div></div></div>
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-308924936971670384mcnDividerBlock" style="min-width:100%;border-collapse:collapse;table-layout:fixed!important">
    <tbody class="m_-308924936971670384mcnDividerBlockOuter">
        <tr>
            <td class="m_-308924936971670384mcnDividerBlockInner" style="min-width:100%;padding:18px">
                <table class="m_-308924936971670384mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-top:2px solid #eaeaea;border-collapse:collapse">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>

            </td>
        </tr>
    </tbody>
</table></td>
                     </tr>
                     <tr>
                        <td valign="top">
                           <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="m_-308924936971670384templateColumns" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0;padding-top:0;padding-bottom:9px">
                              <tbody><tr>
                                 <td valign="top">
                                    
                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="400" id="m_-308924936971670384templateBody" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0;padding-top:0;padding-bottom:9px">
                                       <tbody><tr>
                                          <td><table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-308924936971670384mcnTextBlock" style="min-width:100%;border-collapse:collapse">
    <tbody class="m_-308924936971670384mcnTextBlockOuter">
        <tr>
            <td valign="top" class="m_-308924936971670384mcnTextBlockInner" style="padding-top:9px">
               
             
            
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%" class="m_-308924936971670384mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="m_-308924936971670384mcnTextContent" style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left">
                        
                            <h1 style="display:block;margin:0;padding:0;color:#202020;font-family:Helvetica;font-size:26px;font-style:normal;font-weight:bold;line-height:125%;letter-spacing:normal;text-align:left">{{ trans('messages.Howdy') }} {!! $receiver->name !!},</h1>

<p style="margin:10px 0;padding:0;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left">{{env('APP_NAME')}} {{ trans('messages.has added you as as organisation') }}.<br>
<br>
<strong>{{ trans('messages.Login In Details') }} -</strong></p>

<p dir="ltr" style="text-align:left;margin:10px 0;padding:0;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%"><strong>Email - <a href="mailto:{{$data['email']}}" target="_blank">{{$data['email']}}</a><br>
Password - {{$data['password']}}</strong><br>
&nbsp;</p>

                        </td>
                    </tr>
                </tbody></table>


<center>

    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
        <tbody class="mcnButtonBlockOuter">
            <tr>
                <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="center" class="mcnButtonBlockInner">
                    <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #7140B4;">
                        <tbody>
                            <tr>
                                <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Helvetica; font-size: 18px; padding: 18px;">
                                    <a class="mcnButton " title="Organisation Login" href="{{route('org_login_get1')}}" target="_blank" style="font-weight: bold;letter-spacing: -0.5px;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">{{ trans('messages.Organisation Login') }}</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

</center>

            
                
            
            </td>
        </tr>
    </tbody>
</table></td>
                                       </tr>
                                    </tbody></table>
                                    
                                 </td>
                              </tr>
                           </tbody></table>
                        </td>
                     </tr>
                     <tr>
                        <td valign="top" id="m_-308924936971670384templateFooter" style="background:#fafafa none no-repeat center/cover;background-color:#fafafa;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:2px solid #eaeaea;border-bottom:0;padding-top:9px;padding-bottom:9px"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-308924936971670384mcnDividerBlock" style="min-width:100%;border-collapse:collapse;table-layout:fixed!important">
    <tbody class="m_-308924936971670384mcnDividerBlockOuter">
        <tr>
            <td class="m_-308924936971670384mcnDividerBlockInner" style="min-width:100%;padding:10px 18px 25px">
                <table class="m_-308924936971670384mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-top:2px solid #eeeeee;border-collapse:collapse">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>

            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-308924936971670384mcnTextBlock" style="min-width:100%;border-collapse:collapse">
    <tbody class="m_-308924936971670384mcnTextBlockOuter">
        <tr>
            <td valign="top" class="m_-308924936971670384mcnTextBlockInner" style="padding-top:9px">
               
             
            
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%" class="m_-308924936971670384mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="m_-308924936971670384mcnTextContent" style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center">
                        
                            <div style="text-align:center"><em>Copyright © {{env('APP_YEAR')}} {{env('APP_NAME')}}, {{ trans('messages.All Rights Reserved') }}.</em></div>

<div style="text-align:center"><br>
<strong>{{ trans('messages.Admin mailing address is') }} : </strong><a href="mailto:{{ env('MAIL_USERNAME') }}" target="_blank">{{ env('MAIL_USERNAME') }}</a></div>

                        </td>
                    </tr>
                </tbody></table>
            
                
            
            </td>
        </tr>
    </tbody>
</table></td>
                     </tr>
                  </tbody></table>
                  
                  
                    </td>
                </tr>
            </tbody></table>
        </center><div class="yj6qo"></div><div class="adL">
    </div></div><div class="adL">

</div></div></div>

</body>
</html>