@extends('OrgPanel.orgLayout')

@section('title')
    Service Yesser Man Orders
@stop

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Service Yesser Man Orders
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('org_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('org_services_dall')}}">Service Yesser Man</a>
                        </li>
                        <li>
                <a href="{{ route('org_ser_dorders',['driver_user_detail_id' => $driver->user_detail_id]) }}">
                    <b>Service Yesser Man Orders</b>
                </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-md-6 col-lg-offset-3">
            <div class="ibox-content text-center">
                <h2>{{ $driver['user']->name }}</h2>
                    <div class="m-b-sm">
                        <!-- <a href="{{ $driver->profile_pic_url }}" title="{{ $driver['user']->name }}" class="lightBoxGallery" data-gallery="">
                            <img src="{{ $driver->profile_pic_url }}/120/120" class="img-circle " alt="profile" >
                        </a> -->
                         @if($driver->profile_pic != "")
                            <a href="{{ env('SOCKET_URL').'/Uploads/'.$driver->profile_pic }}" title="{{ $driver->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ env('SOCKET_URL').'/Uploads/'.$driver->profile_pic }}"  class="img-circle">
                            </a>
                        
                    @else
                        <a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $driver->name }}" class="lightBoxGallery img-circle" data-gallery="">
                            <img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
                        </a>
                    @endif  
                    </div>
                @if($driver->category_id != 0)
                    <center>
                        <h3>
                            Service - {{$category['category_details'][0]->name}}
                        </h3>
                        <h4>
                            Brand - {{ $driver->category_brand['category_brand_details'][0]->name }}
                        </h4>
                    </center>
                @endif
            </div>
        </div>
    </div>
<br>

    <div class="row">

<?php
    $search = isset($data['search']) ? $data['search'] : '';
?>


        <form method="post" action="{{ route('org_ser_dorders',['driver_user_detail_id' => $driver->user_detail_id]) }}">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="form-group">
                            <label class="pull-left">Status</label>
                                <select class="form-control" id="status_check" name="status_check" value="{{$data['status_check']}}">
                                    <option value="All">All</option>

                                    <option value="Searching">Searching</option>
                                    <option value="Confirmed">Confirmed</option>
                                    <option value="Scheduled">Scheduled</option>

                                    <option value="Ongoing">Ongoing</option>
                                    <option value="Rejected">Rejected</option>
                                    <option value="Timeout">Timeout</option>

                                    <option value="Customer Cancelled">Customer Cancelled</option>
                                    <option value="Driver Cancelled">Driver Cancelled</option>

                                    <option value="Completed">Completed</option>
                                    <option value="Confirmation Pending">Confirmation Pending</option>
                                </select>                 
                        </div>
                    </div>

                    <div class="col-lg-5 col-md-5 col-sm-5">
                        <label>
                            Filter Updated At
                        </label>

                        <center>
                <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$data['daterange']}}"/>
                        </center>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <label>Search</label>
                        <input type="text" name="search" class="form-control" placeholder="Search" value="{{$search}}">
                    </div>

                    <div class="col-lg-1 col-md-1 col-sm-1">
                        <label>Filter</label><br>
                        <button type="submit" class="btn btn-primary btn-big">Filter</button>
                    </div>

        </form>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Type</th>
                                    <th>Timing</th>
                                    <th>Pickup Location</th>
                                    <th>Customer</th>
                                    <th>CustPic</th>
                                    <th>Dropoff Location</th>
                                    <th>Product</th>
                                    <th>Payment</th>
                                    <th>Details</th>
                                    <th>Booked At</th>
                                </tr>

                                </thead>
                                <tbody>

            @foreach($orders as $order)
                <tr class="gradeA footable-odd">

                    <td>{{ $order->order_id }}</td>

                    <td>
                        @include('Common.Partials.orderStatus')
                    </td>


                    <td>
                        @if($order->order_type == 'Service')
                            <span class="label label-info">
                                <i class="fa fa-certificate"> Service</i>
                            </span>
                        @else
                            <span class="label label-primary">
                                <i class="fa fa-support"> Support</i>
                            </span>
                        @endif
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order->order_timingsz }} </td>

                    <td>
                        {{ $order->pickup_address }}
                    </td>

                    <td>
                        {{ $order['customer_user']->name }} ({{ $order->phone_number }})
                        <hr>
                    </td>

                    <td>
    <a href="{{ $order['customer_user_detail']->profile_pic_url }}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
        <img src="{{ $order['customer_user_detail']->profile_pic_url }}/150/150"  class="img-circle">
    </a>
                    </td>

                    <td style="height:100px;overflow:auto;">
                        {{ $order->dropoff_address }}
                    </td>

                    <td>
                        <span class="label label-info">
                            {{ $order->payment['product_quantity'] }} * {{ $order->category_brand_product['name'] }}
                        </span>
                    </td>

                    <td>
                       <b>OMR</b> {{ $order->payment['final_charge'] }}
                    </td>

                    <td>
            <a href="{{ route('org_order_details', ['order_id' => $order->order_id] ) }}" class="btn btn-primary pull-right" id="Details" title="Details">
                <i class="fa fa-ioxhost"> Order Details</i>
            </a>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order->created_atz }} </td>

                    </tr>
            @endforeach
                                </tbody>
                            </table>

                <center>
                    <h2>Total - {{ $orders->total() }}</h2>
                </center>
                <center>
                    {{ $orders->appends(['status_check' => $data['status_check'], 'order_type_check' => $data['order_type_check'],  'daterange' => $data['daterange'], 'driver_user_detail_id' => $driver->user_detail_id ])->links() }}
                </center>
                            </div>
                        </div>
                    </div>

</div>

        </div>
    </div>
</div>

    <script>

        $("#drivers_all").addClass("active");
        $("#org_services_dall").addClass("active");

/////////////       Filters     ///////////////////////
        $('#status_check').val('{{$data['status_check']}}');
        $('#order_type_check').val('{{$data['order_type_check']}}');
/////////////       Filters     ///////////////////////

    $(document).ready(function()
        {

                $('input[name="daterange"]').daterangepicker({
                    opens: 'center',
                    autoApply: true,
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                }, function(start, end, label) {
                //console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                });

        });

        // var table = $('#example').dataTable( 
        //     {
        //         "order": [[ 0, "desc" ]],
        //         "aoColumnDefs": [
        //             { "bSortable": false, "aTargets": [ 3, 8 ] },
        //             { "bSearchable": true, "aTargets": [ 2,4, 5, 6, 7 ] }
        //         ],
        //             "dom": 'Blfrtip',
        //             "buttons": [
        //                {
        //                     extend: 'pdf',
        //                     footer: true,
        //                     exportOptions: {
        //                         columns: [0,1,2,4,5,6,7,9]
        //                     }
        //                },
        //                {
        //                    extend: 'csv',
        //                    footer: false,
        //                     exportOptions: {
        //                         columns: [0,1,2,4,5,6,7,9]
        //                     }
                          
        //                },
        //                {
        //                    extend: 'excel',
        //                    footer: false,
        //                     exportOptions: {
        //                         columns: [0,1,2,4,5,6,7,9]
        //                     }
        //                }         
        //             ]
        //     });

        // $("#filter1").on('change', function()
        //     {
        //         table.fnFilter($(this).val(), 1);
        //     });

        // $("#filter2").on('change', function()
        //     {
        //         table.fnFilter($(this).val(), 2);
        //     });

    </script>


@stop
