@extends('OrgPanel.orgLayout')

@section('title')
    Service Yesser Man
@stop

@section('content')

    <link href="{{ URL::asset('AdminAssets/css/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('AdminAssets/js/plugins/daterangepicker/daterangepicker.js') }}"></script>

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Service Yesser Man
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('org_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('org_services_dall_new')}}"><b>Service Yesser Man</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        <form method="post" action="{{route('org_services_dall_new')}}">

<?php

    $blocked_check = isset($data['blocked_check']) ? $data['blocked_check'] : 'All';
    $approved_check = isset($data['approved_check']) ? $data['approved_check'] : 'All';
    $created_by_check = isset($data['created_by_check']) ? $data['created_by_check'] : 'All';
    $online_check = isset($data['online_check']) ? $data['online_check'] : 'All';
    $category_id_check = isset($data['category_id_check']) ? $data['category_id_check'] : 'All';
    $search = isset($data['search']) ? $data['search'] : '';

    $daterange =  $fstarting_dt.' - '.$fending_dt;
?>

            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="form-group">
                    <label class="pull-left">Status</label>
                        <select class="form-control" id="blocked_check" name="blocked_check">
                            <option value="All">All</option>
                            <option value="Active">Active</option>
                            <option value="Blocked">Blocked</option>
                        </select>                 
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="form-group">
                    <label class="pull-left">Approved</label>
                        <select class="form-control" id="approved_check" name="approved_check">
                            <option value="All">All</option>
                            <option value="Approved">Approved</option>
                            <option value="Pending">Pending</option>
                        </select>                 
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="form-group">
                    <label class="pull-left">Created By</label>
                        <select class="form-control" id="created_by_check" name="created_by_check">
                            <option value="All">All</option>
                            <option value="Admin">Admin</option>
                            <option value="Company">Company</option>
                            <option value="App">App</option>
                        </select>                 
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="form-group">
                    <label class="pull-left">Online</label>
                        <select class="form-control" id="online_check" name="online_check">
                            <option value="All">All</option>
                            <option value="Online">Online</option>
                            <option value="Offline">Offline</option>
                        </select>                 
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3">
                <label>Services</label>
                <select class="form-control" id="category_id_check" name="category_id_check">
                    <option value="All" selected>All</option>
                    @foreach($services as $service)
                        <option value="{{$service['category_details'][0]->category_id}}" title="{{$service['category_details'][0]->name}}">
                            {{$service['category_details'][0]->name}}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4">
                <label>Filter Registered At</label>
                <center>
                    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$daterange}}"/>
                </center>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3">
                <label>Search</label>
                <input type="text" name="search" class="form-control" placeholder="Search" value="{{$search}}">
            </div>

            <div class="col-lg-2 col-md-2 col-sm-2">
                <label>Filter</label><br>
                <button type="submit" class="btn btn-primary btn-big">Filter Results</button>
            </div>

        </form>
    </div>
    <br>
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Approved</th>
                                    <th>Created By</th>
                                    <th>Service</th>
                                    <th>Online</th>
                                    <th>Name</th>
                                    <th>ProfilePic</th>
                                    <th>Actions</th>
                                    <th>Orders</th>
                                    <th>Reviews</th>
                                    <th>Registered At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($drivers as $driver)
                <tr class="gradeA footable-odd" style="display: table-row;">

                    <td>{{ $driver->user_detail_id }}</td>

                    <td>
                        <center>
                            @if($driver->blocked == '0')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye" aria-hidden="true"> Blocked</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($driver->approved == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-thumbs-up" aria-hidden="true"> Approved</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-thumbs-down" aria-hidden="true"> Pending</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($driver->created_by == 'Admin')
                                <span class="label label-primary">
                                    <i class="fa fa-at" aria-hidden="true"></i> Admin
                                </span>
                            @elseif($driver->created_by == 'Org')
                                <span class="label label-info">
                                    <i class="fa fa-building-o" aria-hidden="true"></i> Company
                                </span>
                            @else
                                <span class="label label-success">
                                    <i class="fa fa-mobile" aria-hidden="true"></i> App
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <span class="label label-success">{{$driver->category['name']}}</span>
                    </td>

                    <td>
                        <center>
                            @if($driver->online_status == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-toggle-on" aria-hidden="true"> Online</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-toggle-off" aria-hidden="true"> Offline</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        {{ $driver->name }} ({{ $driver->phone_number }})
                    </td>

                    <td>
  <!--   <a href="{{$driver->profile_pic_url}}" title="{{ $driver->name }}" class="lightBoxGallery" data-gallery="">
        <img src="{{$driver->profile_pic_url}}/120/120"  class="img-circle">
    </a>   -->                 
    @if($driver->profile_pic != "")
                            <a href="{{ env('SOCKET_URL').'/Uploads/'.$driver->profile_pic }}" title="{{ $driver->name }}" class="lightBoxGallery img-circle" data-gallery="">
                                <img src="{{ env('SOCKET_URL').'/Uploads/'.$driver->profile_pic }}"  class="img-circle">
                            </a>
                        
                    @else
                        <a href="{{ URL::asset('Images/ServiceDefaultImage.png') }}" title="{{ $driver->name }}" class="lightBoxGallery img-circle" data-gallery="">
                            <img src="{{ URL::asset('Images/ServiceDefaultImage.png') }}"  class="img-circle">
                        </a>
                    @endif     
                    </td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
            @if($driver->category_id != 0)
                <li role="presentation">
                    <a role="menuitem" tabindex="-1" href="{{ route('org_ser_udriver', ['user_detail_id' => $driver->user_detail_id] ) }}">
                        <i class="fa fa-edit"> Update Profile</i>
                    </a>
                </li>
            @endif

            <li role="presentation">
                <a role="menuitem" tabindex="-1" id="admin_cust_orders" href="{{route('org_ser_dorders',['driver_user_detail_id' => $driver->user_detail_id])}}">
                    <i class="fa fa-circle-thin" aria-hidden="true"> All Orders - {{ $driver->order_counts }}</i>
                </a>
            </li>

            <li role="presentation">
                <a role="menuitem" tabindex="-1" id="org_ser_dreviews" href="{{route('org_ser_dreviews',['driver_user_detail_id' => $driver->user_detail_id])}}">
                    <i class="fa fa-user-circle" aria-hidden="true"> All Reviews - {{ $driver->review_counts }}</i>
                </a>
            </li>

            <li role="presentation">
                @if($driver->blocked == '0')
                    <a role="menuitem" tabindex="-1" class="block_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Block','{{$driver->user_detail_id}}')">
                        <i class="fa fa-eye-slash"></i> Block
                    </a>
                @else
                    <a role="menuitem" tabindex="-1" class="unblock_driver" id="{{$driver->user_detail_id}}" onclick="show_popup('Unblock','{{$driver->user_detail_id}}')">
                        <i class="fa fa-eye"></i> Unblock
                    </a>
                @endif
            </li>

<!--             <li role="presentation">
                <a role='menuitem' tabindex="-1" onclick="delete_popup('{{$driver->user_detail_id}}', '{{$driver->user_id}}')">
                    <i class="fa fa-bitbucket" aria-hidden="true"> Delete </i>
                </a>
            </li> -->

                            </ul>
                        </div>
                    </td>

                    <td>
                        <a href="{{ route('org_ser_dorders',['driver_user_detail_id' => $driver->user_detail_id]) }}" class="btn btn-default">
                            {{ $driver->order_counts }}
                        </a>
                    </td>

                    <td>
                        <a href="{{ route('org_ser_dreviews',['driver_user_detail_id' => $driver->user_detail_id]) }}" class="btn btn-default">
                            {{ $driver->review_counts }}
                        </a>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $driver->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>
                <center>
                    <h2>Total - {{ $drivers->total() }}</h2>
                </center>
                <center>
                    {{ $drivers->appends(['blocked_check' => $blocked_check, 'approved_check' => $approved_check, 'created_by_check' => $created_by_check, 'online_check'=>$online_check, 'category_id_check'=>$category_id_check, 'search'=>$search, 'daterange' => $daterange])->links() }}
                </center>

                            </div>
                        </div>
                    </div>

</div>

        </div>
    </div>
</div>

    <script>

        $("#drivers_all").addClass("active");
        $("#org_services_dall").addClass("active");

/////////////       Filters     ///////////////////////
        $('#blocked_check').val('{{$blocked_check}}');
        $('#approved_check').val('{{$approved_check}}');
        $('#created_by_check').val('{{$created_by_check}}');
        $('#online_check').val('{{$online_check}}');
        $('#category_id_check').val('{{$category_id_check}}');
/////////////       Filters     ///////////////////////

        // function delete_popup(driver_user_detail_id, driver_user_id)
        //     {

        //         swal({
        //             title: "Are you sure you want to delete this driver?",
        //             text: "All data will be completely deleted.",
        //             type: "warning",
        //             showCancelButton: true,
        //             confirmButtonColor: "#DD6B55",
        //             confirmButtonText: "Yes, Delete it!",
        //             closeOnConfirm: false
        //             }, function () {
        //                 window.location.href = '{{route("org_delete")}}?driver_user_detail_id='+driver_user_detail_id+'&driver_user_id='+driver_user_id;
        //             });

        //     }

        function show_popup(type, id)
            {

                if(type == 'Block')
                    {
                        var title = "Are you sure you want to block this Yesser Man?";
                        var sub_title = "User will not be view this Yesser Man.";
                        var route = '{{route("org_ser_bdriver")}}?block=1&user_detail_id='+id;
                    }
                else if(type == 'Unblock')
                    {
                        var title = "Are you sure you want to unblock this Yesser Man?";
                        var sub_title = "User will be able to view this Yesser Man.";
                        var route = '{{route("org_ser_bdriver")}}?block=0&user_detail_id='+id;
                    }
                
                swal({
                        title: title,
                        text: sub_title,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Do it!",
                        closeOnConfirm: false
                    }, function () {
                        window.location.href = route;
                    });
            }

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                opens: 'center',
                autoApply: true,
                format: 'DD/MM/YYYY',
                minDate: '1/2/2018',
                maxDate: moment().format('DD/MM/YYYY')
              }, function(start, end, label) {
                //console.log("A selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
              });

        });

    </script>

@stop
