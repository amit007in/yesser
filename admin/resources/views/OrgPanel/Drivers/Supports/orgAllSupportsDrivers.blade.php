@extends('OrgPanel.orgLayout')

@section('title')
    Support Drivers
@stop

@section('content')

    <link href="{{ URL::asset('AdminAssets/css/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('AdminAssets/js/plugins/daterangepicker/daterangepicker.js') }}"></script>

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Support Drivers
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('org_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('org_supports_dall')}}"><b>Supports</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="form-group">
                <label class="pull-left">Status</label>
                    <select class="form-control" id="filter1">
                        <option value="e">All</option>
                        <option value="Active">Active</option>
                        <option value="Blocked">Blocked</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="form-group">
                <label class="pull-left">Approved</label>
                    <select class="form-control" id="filter2">
                        <option value="">All</option>
                        <option value="Approved">Approved</option>
                        <option value="Pending">Pending</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="form-group">
                <label class="pull-left">Created By</label>
                    <select class="form-control" id="filter3">
                        <option value="a">All</option>
                        <option value="Admin">Admin</option>
                        <option value="Company">Company</option>
                        <option value="App">App</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3">
            <label>
                Filter Updated At
            </label>

            <center>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
            </center>                        
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3">
            <label>
                Supports
            </label>
<select class="form-control" id="filter4">
    <option value="" selected>All</option>
    @foreach($supports as $support)
        <option value="{{$support['category_details'][0]->name}}" title="{{$support['category_details'][0]->name}}">
            {{$support['category_details'][0]->name}}
        </option>
    @endforeach
</select>
        </div>
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Approved</th>
                                    <th>Created By</th>
                                    <th>Support</th>
                                    <th>Name</th>
                                    <th>ProfilePic</th>
                                    <th>Actions</th>
                                    <th>Orders</th>
                                    <th>Reviews</th>
                                    <th>Products</th>
                                    <th>Registered At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($drivers as $driver)
                <tr class="gradeA footable-odd" style="display: table-row;">

                    <td>{{ $driver->user_detail_id }}</td>

                    <td>
                        <center>
                            @if($driver->blocked == '0')
                                <span class="label label-primary">
                                    <i class="fa fa-eye" aria-hidden="true"> Active</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-eye" aria-hidden="true"> Blocked</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($driver->approved == '1')
                                <span class="label label-primary">
                                    <i class="fa fa-thumbs-up" aria-hidden="true"> Approved</i>
                                </span>
                            @else
                                <span class="label label-danger">
                                    <i class="fa fa-thumbs-down" aria-hidden="true"> Pending</i>
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <center>
                            @if($driver->created_by == 'Admin')
                                <span class="label label-primary">
                                    <i class="fa fa-at" aria-hidden="true"></i> Admin
                                </span>
                            @elseif($driver->created_by == 'Org')
                                <span class="label label-info">
                                    <i class="fa fa-building-o" aria-hidden="true"></i> Company
                                </span>
                            @else
                                <span class="label label-success">
                                    <i class="fa fa-mobile" aria-hidden="true"></i> App
                                </span>
                            @endif
                        </center>
                    </td>

                    <td>
                        <span class="label label-success">{{$driver->category['name']}}</span>
                    </td>

                    <td>
                        {{ $driver->name }} ({{ $driver->phone_number }})
                    </td>

                    <td>
    <a href="{{$driver->profile_pic_url}}" title="{{ $driver->name }}" class="lightBoxGallery" data-gallery="">
        <img src="{{$driver->profile_pic_url}}/120/120"  class="img-circle">
    </a>                        
                    </td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

            <li role="presentation">
                <a role="menuitem" tabindex="-1" href="{{ route('org_ser_udriver', ['user_detail_id' => $driver->user_detail_id] ) }}">
                    <i class="fa fa-edit"> Update</i>
                </a>
            </li>

            <li role="presentation">
                @if($driver->blocked == '0' || $driver->blocked == '2')
                    <a role="menuitem" tabindex="-1" class="block_org" id="{{$driver->user_detail_id}}">
                        <i class="fa fa-eye-slash"> Block</i>
                    </a>
                @else
                    <a role="menuitem" tabindex="-1" class="unblock_org" id="{{$driver->user_detail_id}}">
                        <i class="fa fa-eye"> Unblock</i>
                    </a>
                @endif
            </li>

            <li role="presentation">
                <a role='menuitem' tabindex="-1" onclick="delete_popup('{{$driver->user_detail_id}}', '{{$driver->user_id}}')">
                    <i class="fa fa-bitbucket" aria-hidden="true"> Delete </i>
                </a>
            </li>


                            </ul>
                        </div>
                    </td>

                    <td>
                        <a href="{{ route('admin_ser_dorders',['driver_user_detail_id' => $driver->user_detail_id]) }}" class="btn btn-default">
                            {{ $driver->order_counts }}
                        </a>
                    </td>

                    <td>
                        <a href="{{ route('admin_ser_dreviews',['driver_user_detail_id' => $driver->user_detail_id]) }}" class="btn btn-default">
                            {{ $driver->review_counts }}
                        </a>
                    </td>

                    <td>
                        <a href="{{ route('admin_ser_dproducts',['driver_user_detail_id' => $driver->user_detail_id]) }}" class="btn btn-default">
                            {{ $driver->product_counts }}
                        </a>
                    </td>


                    <td><i class="fa fa-clock-o"></i> {{ $driver->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

        $("#drivers_all").addClass("active");
        $("#org_supports_dall").addClass("active");

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

            $('#daterange').on('apply.daterangepicker', function(ev, picker)
                {
                    dt = document.getElementById('daterange').value;

                    window.location.href = "{{route('org_supports_dall')}}?daterange="+dt;
                });

        });

            function delete_popup(driver_user_detail_id, driver_user_id)
                {

                    swal({
                        title: "Are you sure you want to delete this driver?",
                        text: "All data will be completely deleted.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Delete it!",
                        closeOnConfirm: false
                        }, function () {
                            window.location.href = '{{route("org_delete")}}?driver_user_detail_id='+driver_user_detail_id+'&driver_user_id='+driver_user_id;
                        });

                }

            var table = $('#example').dataTable( 
                {
                   "order": [[ 0, "desc" ]],
                   "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 5, 6 ] },
                        { "bSearchable": true, "aTargets": [ 4 ] }
                    ],
                   "fnDrawCallback": function( oSettings ) 
                        {

                            $('.block_org').click(function (e) {

                                  var id = $(e.currentTarget).attr("id");

                                    swal({
                                        title: "Are you sure you want to block this driver?",
                                        text: "User will not be view this driver.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Block it!",
                                        closeOnConfirm: false
                                    }, function () {
                                        window.location.href = '{{route("org_ser_bdriver")}}?block=1&user_detail_id='+id;
                                    });
                                });

                            $('.unblock_org').click(function (e) {

                                     var id = $(e.currentTarget).attr("id");

                                    swal({
                                        title: "Are you sure you want to unblock this driver?",
                                        text: "Users will be able to view this driver.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Unblock It!",
                                        closeOnConfirm: false
                                    }, function () {
                                window.location.href = '{{route("org_ser_bdriver")}}?block=0&user_detail_id='+id;
                                    });
                                });

                        }
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });

            $("#filter2").on('change', function()
                {
                    table.fnFilter($(this).val(), 2);
                });

            $("#filter3").on('change', function()
                {
                    table.fnFilter($(this).val(), 3);
                });

            $("#filter4").on('change', function()
                {
                    table.fnFilter($(this).val(), 4);
                });

    </script>


@stop
