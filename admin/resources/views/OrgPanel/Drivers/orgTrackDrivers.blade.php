@extends('OrgPanel.orgLayout')

@section('title')
    Track Drivers
@stop

@section('content')

<style type="text/css">
    #legend {
        background: #FFF;
        padding: 10px;
        margin: 5px;
        font-size: 12px;
        font-family: Arial, sans-serif;
      }

</style>

    <link href="{{ URL::asset('AdminAssets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('AdminAssets/js/plugins/select2/select2.full.min.js') }}"></script>


<div class="wrapper wrapper-content animated fadeInRight">

<?php

if(!Form::old('address_latitude'))
    $address_latitude = 21.4735;
else
    $address_latitude = Form::old('address_latitude');

if(!Form::old('address_longitude'))
    $address_longitude = 55.9754;
else
    $address_longitude = Form::old('address_longitude');


?>

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Track Yesser Man
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('org_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('org_orders')}}">
                                <b>
                                    Track Yesser Man
                                </b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">


                    <div class="row">

                    <div class="col-lg-5 col-md-5 col-sm-5">
                        <div class="form-group">
                            <label class="pull-left">Services</label>

<select class="form-control tokenizationSelect1" id="tokenizationSelect1" name="services[]" multiple="true" required="true" onchange="trackDrivers()">

            @foreach($services as $service)

              @if($service->is_selected == "1")
        <option value="{{$service->category_id}}" selected="true" title="{{$service['category_details'][0]->name}}">{{$service['category_details'][0]->name}}</option>
              @else
        <option value="{{$service->category_id}}" title="{{$service['category_details'][0]->name}}">{{$service['category_details'][0]->name}}</option>
              @endif

            @endforeach

</select>

                        </div>
                    </div>

                   <!-- <div class="col-lg-5 col-md-5 col-sm-5">
                        <div class="form-group">
                            <label class="pull-left">Supports</label>

<select class="form-control tokenizationSelect2" id="tokenizationSelect2" name="supports[]" multiple="true" required="true" onchange="trackDrivers()">

            @foreach($supports as $support)
                @if($support->is_selected == "1")
            <option value="{{$support->category_id}}" selected="true" title="{{$support['category_details'][0]->name}}">{{$support['category_details'][0]->name}}</option>
                @else
            <option value="{{$support->category_id}}" title="{{$support['category_details'][0]->name}}">{{$support['category_details'][0]->name}}</option>
                @endif
            @endforeach

</select>

                        </div>
                    </div>-->

                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <div class="form-group">
                            <label class="pull-left">Online</label>
                            <select class="form-control" name="online" id="online" onchange="trackDrivers()">
                                <option value="1">Online</option>
                                <option value="0">Offline</option>
                            </select>
                        </div>
                    </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="table-responsive">

                                <div id="map1" style="width:100%; height:500px;"></div>

                            </div>
                        </div>
                    </div>


                    </div>


<!--                 <div class="row">

                    <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8">
                        <div class="form-group">
                            <label class="pull-left">Driver Types</label>

                    <select class="driver_types form-control border-info" id="driver_types" multiple="true" required name="driver_types[]">
                        <option value="Service" selected>Service</option>
                        <option value="Support" selected>Support</option>
                    </select>
          
                        </div>
                    </div>



                </div> -->

            </div>

        </div>
    </div>

</div>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.dev.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;libraries=places&key={{env('GOOGLE_MAP_KEY')}}"></script>

    <script>

    $("#drivers_all").addClass("active");
    $("#org_drivers_track").addClass("active");

    var socket = io.connect("{{env('SOCKET_URL')}}", { query: { admin_access_token: "{{$OrgDetails->access_token}}" } } );

    $(document).ready(function()
        {

            $("#driver_types").select2({
                  dropdownCssClass: "bigdrop",
                  placeholder: "Select driver types"
              });

        });

        var myMap1;
        var mapOptions;
        var subjectMarker1;

        $(".tokenizationSelect1").select2({});
        $(".tokenizationSelect2").select2({});


    function load_map()
        {

            address_latitude = "{{$address_latitude}}";
            address_longitude = "{{$address_longitude}}";

            mapOptions = {
                    zoom: 6,
                    center:new google.maps.LatLng(address_latitude,address_longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP  ,
                    scrollwheel: false,
                    fullscreenControl: false,
                };

            myMap1 = new google.maps.Map(document.getElementById('map1'), mapOptions);

            if(address_latitude != '' && address_longitude != '')
                {
                    myMap1.setCenter(new google.maps.LatLng(Number(address_latitude), Number(address_longitude)));
                }

            var legend = document.createElement('div');
            legend.id = 'legend';

            div= document.createElement('div');
            div.innerHTML = '<span><img src="{{env('ADMIN_ASSET')}}Tracking/ser_gas.png"> Beautician</span>';
            legend.appendChild(div);
            
            var div = document.createElement('div');
            div.innerHTML = '<span><img src="{{env('ADMIN_ASSET')}}Tracking/ser_drink_water.png"> House Services </span>';
            legend.appendChild(div);

            var div = document.createElement('div');
            div.innerHTML = '<span><img src="{{env('ADMIN_ASSET')}}Tracking/ser_water_tank.png"> Furniture Cleaning </span>';
            legend.appendChild(div);

            var div = document.createElement('div');
            div.innerHTML = '<span><img src="{{env('ADMIN_ASSET')}}Tracking/ser_truck.png"> House Shifting </span>';
            legend.appendChild(div);

            var div = document.createElement('div');
            div.innerHTML = '<span><img src="{{env('ADMIN_ASSET')}}Tracking/ser_heavy.png"> Laundry </span>';
            legend.appendChild(div);

            // var div = document.createElement('div');
            // div.innerHTML = '<span><img src="{{env('ADMIN_ASSET')}}Tracking/sup_icon.png"> Support </span>';
            // legend.appendChild(div);

                /* Push Legend to Right Top */
                myMap1.controls[google.maps.ControlPosition.RIGHT_TOP].push(legend);


            trackDrivers();
            setInterval( trackDrivers, 15000);

        }

        load_map();


    function trackDrivers()
        {

            socket.emit("PanelCommonEvent", { 
                type:"OrgDriverMap", 
                services: $('#tokenizationSelect1').val(), 
                supports: $('#tokenizationSelect2').val(), 
                organisation_id: "{{$OrgDetails->organisation_id}}",
                online: $('#online').val()
            }, (response) => {

                //console.log(response);

                        if(response.success == 0)
                            {
                                toastr.error('Not able to track',response.msg);
                            }
                        else
                            {
                                upload_markers(response.drivers);
                            }

            });

        }

            var markers = [];
            function upload_markers(drivers)
                {

                    DeleteMarkers();

                    drivers.map(function(location, i) {

                            if(drivers[i].category_id == 1)////////////       Gas     ////////////////
                                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_gas.png";
                            else if(drivers[i].category_id == 2)////////////       Drinking Water  ///
                                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_drink_water.png";
                            else if(drivers[i].category_id == 3)////////////       Water Tanker    ///
                                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_water_tank.png";
                            else if(drivers[i].category_id == 4)////////////       Freight        /////////
                                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_truck.png";
                            else if(drivers[i].category_id == 5)////////////       Tow        /////////////
                                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_heavy.png";
                            // else if(drivers[i].category_id == 6)////////////       Heavy        ///////////
                            //     driver_icon = "{{env('ADMIN_ASSET')}}Tracking/ser_heavy.png";
                            else
                                driver_icon = "{{env('ADMIN_ASSET')}}Tracking/sup_icon.png";

                        //Create a marker and placed it on the map.
                        var marker = new google.maps.Marker({
                            //animation: google.maps.Animation.DROP,
                            position: {lat: parseFloat(location.latitude), lng: parseFloat(location.longitude) },
                            map: myMap1,
                            icon: driver_icon
                        });

                        if(location.user_type_id == 2)
                            var driver_type = "Service";
                        else if(location.user_type_id == 3)
                            var driver_type = "Support";

                        //Attach click event handler to the marker.
                        google.maps.event.addListener(marker, "click", function (e) {
                            var infoWindow = new google.maps.InfoWindow({
                                content: '</h3> <h3>Name - '+location.user['name']+' ('+location.user['phone_number']+') </h3> <h3> Type -'+driver_type+"</h3>"
                            });
                            infoWindow.open(myMap1, marker);
                        });

                        markers.push(marker);

                    });

                }

    function DeleteMarkers() {
        //Loop through all the markers and remove
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
    };

    </script>


@stop
