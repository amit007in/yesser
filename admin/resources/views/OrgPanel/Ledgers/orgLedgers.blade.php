@extends('OrgPanel.orgLayout')

    @section('title')
        Ledgers
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Ledgers
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('org_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('org_ledgers')}}"><b>Ledgers</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">


                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <label>
                            Filter Created At
                        </label>

                        <center>
                <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
                        </center>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="form-group">
                            <label class="pull-left">Ledger Type</label>
                                <select class="form-control" id="filter1">
                                    <option value="">All</option>
                                    <option value="Credit">Credit</option>
                                    <option value="Payment">Payment</option>
                                </select>                 
                        </div>
                    </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Ledger Type</th>
                                    <th>Amount</th>
                                    <th>Ledger Datetime</th>
                                    <th>Description</th>
                                    <th>Created At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($ledgers as $ledger)
                <tr class="gradeA footable-odd">

                    <td>{{ $ledger->payment_ledger_id }}</td>

                    <td>
                        @if($ledger->type == "Payment")
                            <span class="label label-info">
                                Payment
                            </span>
                        @else
                            <span class="label label-warning">
                                Credit
                            </span>
                        @endif
                    </td>

                    <td><b>OMR</b> {{ $ledger->amount }}</td>

                    <td><i class="fa fa-clock-o"></i> {{ $ledger->ledger_dtz }} </td>

                    <td>
                        <div style="height:100px;overflow:auto;">
                            {{ $ledger->description }}
                        </div>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $ledger->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>
</div>

        </div>
    </div>
</div>

    <script>

        $("#org_ledgers").addClass("active");

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

        $('#daterange').on('apply.daterangepicker', function(ev, picker)
            {
                dt = document.getElementById('daterange').value;
                window.location.href = "{{route('org_ledgers')}}?daterange="+dt;
            });

        });

            var table = $('#example').dataTable( 
                {
                    "order": [[ 0, "DESC" ]],
                    "scrollX": true,
                    "aoColumnDefs": [
                        // { "bSortable": false, "aTargets": [ 4 ] },
                        // { "bSearchable": true, "aTargets": [ 4 ] }
                    ],
                   "fnDrawCallback": function( oSettings ) 
                        {
                        }
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });

    </script>


@stop
