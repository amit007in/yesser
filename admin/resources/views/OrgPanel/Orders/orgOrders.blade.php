@extends('OrgPanel.orgLayout')

@section('title')
    Orders All
@stop

@section('content')

    <link href="{{ URL::asset('AdminAssets/css/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('AdminAssets/js/plugins/daterangepicker/daterangepicker.js') }}"></script>

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Orders All
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('org_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('org_orders')}}">
                                <b>Orders All</b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="form-group">
                <label class="pull-left">Status</label>
                    <select class="form-control" id="filter1">
                        <option value="">All</option>
                        <option value="Searching">Searching</option>
                        <option value="Confirmed">Confirmed</option>
                        <option value="Scheduled">Scheduled</option>

                        <option value="Ongoing">Ongoing</option>
                        <option value="Rejected">Rejected</option>
                        <option value="Timeout">Timeout</option>

                        <option value="Customer Cancelled">Customer Cancelled</option>
                        <option value="Driver Cancelled">Driver Cancelled</option>

                        <option value="Completed">Completed</option>
                        <option value="Confirmation Pending">Confirmation Pending</option>
                    </select>                 
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6">
            <label>
                Filter Updated At
            </label>

            <center>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
            </center>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="form-group">
                <label class="pull-left">Type</label>
                    <select class="form-control" id="filter2">
                        <option value="">All</option>
                        <option value="Service">Service</option>
                        <option value="Support">Support</option>
                    </select>                 
            </div>
        </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Type</th>
                                    <th>Timing</th>
                                    <th>Pickup Location</th>
                                    <th>Customer</th>
                                    <th>CustPic</th>
                                    <th>Product</th>
                                    <th>Driver</th>
                                    <th>DriverPic</th>
                                    <th>Payment</th>
                                    <th>Details</th>
                                    <th>Booked At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($orders as $order)
                <tr class="gradeA footable-odd" style="display: table-row;">

                    <td>{{ $order->order_id }}</td>

                    <td>
                        @include('Common.Partials.orderStatus')
                    </td>


                    <td>
                        @if($order->order_type == 'Service')
                            <span class="label label-info">
                                <i class="fa fa-certificate"> Service</i>
                            </span>
                        @else
                            <span class="label label-primary">
                                <i class="fa fa-support"> Support</i>
                            </span>
                        @endif
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order->order_timingsz }} </td>

                    <td>
                        {{ $order->pickup_address }}
                    </td>

                    <td>
                        {{ $order['customer_user']->name }} ({{$order['customer_user']->phone_number}})
                    </td>

                    <td>
    <a href="{{ $order['customer_user_detail']->profile_pic_url }}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
        <img src="{{$order['customer_user_detail']->profile_pic_url}}/150/150"  class="img-circle">
    </a>
                    </td>

                    <td>
                        <span class="label label-info">
                            {{ $order->payment['product_quantity'] }} * {{ $order->category_brand_product['name'] }}
                        </span>
                    </td>

                    <td>
                        {{ $order['driver_user_detail']->name }}
                    </td>

                    <td>
    <a href="{{ $order['driver_user_detail']->profile_pic_url }}" title="{{ $order['driver_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
        <img src="{{ $order['driver_user_detail']->profile_pic_url }}/150/150"  class="img-circle">
    </a>
                    </td>

                    <td>
                       <b>OMR</b> {{ $order->payment['final_charge'] }}
                    </td>

                    <td>
            <a href="{{ route('org_order_details', ['order_id' => $order->order_id] ) }}" class="btn btn-primary pull-right" id="Details" title="Details">
                <i class="fa fa-ioxhost"> Order Details</i>
            </a>

                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

    $("#orders").addClass("active");

    $(document).ready(function()
        {

            $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

        $('#daterange').on('apply.daterangepicker', function(ev, picker)
            {
                dt = document.getElementById('daterange').value;

                window.location.href = "{{route('org_orders')}}?daterange="+dt;
            });

        });

            var table = $('#example').dataTable( 
                {
                   "order": [[ 0, "desc" ]],
                   "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 5, 8 ] },
                        { "bSearchable": true, "aTargets": [ 3, 5, 6 ] }
                    ],
                   "fnDrawCallback": function( oSettings ) 
                        {


                        }
                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });

            $("#filter2").on('change', function()
                {
                    table.fnFilter($(this).val(), 2);
                });

    </script>


@stop
