@extends('OrgPanel.orgLayout')

@section('title')
    Orders All
@stop

@section('content')

    <link href="{{ URL::asset('AdminAssets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('AdminAssets/js/plugins/select2/select2.full.min.js') }}"></script>

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Orders All
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('org_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('org_orders')}}">
                                <b>Orders All</b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

<?php
    $search = isset($data['search']) ? $data['search'] : '';
?>

    <div class="row">

        <form method="post" action="{{ route('org_orders') }}">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">


                    <div class="row">

                        <div class="col-lg-4 col-md-4">
                            <div class="form-group">
                                <label class="pull-left">Status</label>
                                    <select class="form-control" id="status_check" name="status_check" value="{{$data['status_check']}}">
                                        <option value="All">All</option>

                                        <option value="Searching">Searching</option>
                                        <option value="Confirmed">Confirmed</option>
                                        <option value="Scheduled">Scheduled</option>

                                        <option value="Ongoing">Ongoing</option>
                                        <option value="Rejected">Rejected</option>
                                        <option value="Timeout">Timeout</option>

                                        <option value="Customer Cancelled">Customer Cancelled</option>
                                        <option value="Driver Cancelled">Yesser Man Cancelled</option>

                                        <option value="Completed">Completed</option>
                                        <option value="Confirmation Pending">Confirmation Pending</option>
										<option value="Driver not found">Yesser Man not found</option>
                                    </select>
                            </div>
                        </div>


                        <div class="col-lg-4 col-md-4">
                            <label>
                                Filter Booking Timing
                            </label>

                            <center>
                    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$data['daterange']}}"/>
                            </center>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <label>Search</label>
                            <input type="text" name="search" class="form-control" placeholder="Search" value="{{$search}}">
                        </div>

                    </div>


                    <div class="row">

                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label class="pull-left">Categories</label>
                                <select class="form-control categories" id="categories" name="categories[]" multiple="true" required="true">
                                    @foreach($categories as $category)
                                        @if($category->selected == 1)
                                            <option value="{{$category->category_id}}" title="{{$category->name}}" selected>
                                        @else
                                            <option value="{{$category->category_id}}" title="{{$category->name}}">
                                        @endif
                                                {{ $category->name }}
                                            </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3 col-lg-3">
                            <div class="form-group">
                                <label class="pull-left">Ordering</label>
                                <select class="form-control ordering_filter" id="ordering_filter" name="ordering_filter">
                                    <option value="0">Created Time ASC</option>
                                    <option value="1">Created Time DESC</option>
                                    <option value="2">Booking Time ASC</option>
                                    <option value="3">Booking Time DESC</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3">
                            <label class="pull-left">Filter</label><br>
                            <button type="submit" class="btn btn-primary btn-big">Filter</button>
                            <!-- <a class="btn btn-success btn-big" onclick="export_excell()">Export Excel</a> -->
                        </div>

                    </div>

        </form>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Status</th>
                                    <th>Type</th>
                                    <th>Timing</th>
                                    <th>Pickup Location</th>
                                    <th>Dropoff Location</th>
                                    <th>Customer</th>
                                    <!--<th>CustPic</th> -->                                   
                                    <th>Category</th>
                                    <th>Brand</th>
                                    <th>Product</th>
                                    <th>Yesser Man</th>
                                    <!-- <th>Yesser Man Pic</th> -->
									<!-- <th>Re-Assign</th> -->
                                    <th>Payment</th>
                                    <th>Details</th>
                                    <th>Booked At</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($orders as $order)
                <tr class="gradeA footable-odd">

                    <td>{{ $order->order_id }}</td>

                    <td>
                        @include('Common.Partials.orderStatus')
                    </td>


                    <td>
                        @if($order->order_type == 'Service')
                            <span class="label label-info">
                                <i class="fa fa-certificate"> Service</i>
                            </span>
                        @else
                            <span class="label label-primary">
                                <i class="fa fa-support"> Support</i>
                            </span>
                        @endif
                    </td>

                    <td>
                        <i class="fa fa-clock-o"></i> {{ $order->order_timingsz }}
                        @if($order->future == '1')
                            <span class="label label-primary">
                                Scheduled
                            </span>
                        @else
                            <span class="label label-success">
                                Current
                            </span>
                        @endif
                    </td>

                    <td style="height:100px;overflow:auto;">
                        {{ $order->pickup_address }}
                    </td>

                    <td style="height:100px;overflow:auto;">
                        {{ $order->dropoff_address }}
                    </td>

                   <!-- <td>
                        {{ $order['customer_user']->name }} ({{ $order->phone_number }})
                        <hr>
                    </td>-->
					<td>
                        {{ $order['customer_user']->name }}
                        <hr>
                    </td>

                    <!--<td>
    <a href="{{ $order['customer_user_detail']->profile_pic_url }}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
        <img src="{{ $order['customer_user_detail']->profile_pic_url }}/150/150"  class="img-circle">
    </a>
                    </td>-->

                    <td>
                       {{ $order['category']->name }}
                    </td>

                    <td>
                        @if(isset($order['category_brand']))
                            {{ $order['category_brand']->name }}
                        @endif
                    </td>

                    <td>
                        <span class="label label-info">
                            {{ $order->payment['product_quantity'] }} * {{ $order->category_brand_product['name'] }}
                        </span>
                    </td>

                    <td>
                        {{ $order['driver_user_detail']->name }}
                    </td>

                    <!-- <td>
    <a href="{{ $order['driver_user_detail']->profile_pic_url }}" title="{{ $order['driver_user_detail']->name }}" class="lightBoxGallery img-circle" data-gallery="">
        <img src="{{ $order['driver_user_detail']->profile_pic_url }}/150/150"  class="img-circle">
    </a>
                    </td> -->
					<!-- @if($order->category['reassign_status'] == '0') 
						@if($order->order_status == "Driver not found" || $order->order_status == "SerTimeout" || $order->order_status == "SupTimeout" || $order->order_status == "DSchTimeout" || $order->order_status == "CTimeout" )
							<td><a onclick="ShowDrivers({{ $order->order_id }})" class="btn btn-primary pull-right"> Assign Yesser Man </a></td>
						@else
							<td></td>
						@endif
					@else
						<td></td>
					@endif -->
                    <td>
                       <b>OMR</b> {{ $order->payment['final_charge'] }}
                    </td>
					
                    <td>
            <a href="{{ route('org_order_details', ['order_id' => $order->order_id] ) }}" class="btn btn-primary pull-right" id="Details" title="Details">
                <i class="fa fa-ioxhost"> Order Details</i>
            </a>
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order->created_atz }} </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>
                <center>
                    <h2>Total - {{ $orders->total() }}</h2>
                </center>
                <center>
                    {{ $orders->appends([ 'status_check' => $data['status_check'],  'daterange' => $data['daterange'], 'categories' => $data['categories'], 'ordering_filter' => $data['ordering_filter'] ])->links() }}
                </center>
                            </div>
                        </div>
                    </div>

</div>

        </div>
    </div>
</div>
<!-- Modal: modalCart -->
<div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Your cart</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <!--Body-->
      <div class="modal-body">

        <table class="table table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>Yesser Man name</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody id="appendList">
            
          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>
<!-- Modal: modalCart -->
    <script>

    $("#orders").addClass("active");

/////////////       Filters     ///////////////////////
        $('#status_check').val('{{$data['status_check']}}');
        $('#ordering_filter').val('{{$data['ordering_filter']}}');
/////////////       Filters     ///////////////////////

        var $select = $(".categories").select2({
            placeholder: "Select atleast one category"
        });
	function ShowDrivers(id)
		{
			route = '{!! route('admin_reassign_driver') !!}?orderId='+id;
			console.log(route);
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					$('#appendList').empty();
					var HTML = "";
					for(var i =0;i<data.length;i++)
					{
						var cusername           = data[i].name;
						var DriverUserId        = data[i].user_id;
						var DriverUserDetailsId = data[i].user_detail_id;
						HTML += "<tr>";
						HTML += "<td>"+DriverUserId+"</td>";
						HTML += "<td>"+cusername+"</td>";
						HTML += "<td><a onclick='sendRequest("+DriverUserDetailsId+","+id+")' class='btn btn-primary pull-right'> Send Request </a></td>";
						HTML += "</tr>";
					}
					if(HTML == "")
					{
						HTML += "<tr>";
						HTML += "<td colspan='3'>No Yesser Man Found</td>";
						HTML += "</tr>";
					}
					$('#appendList').html(HTML);
					$('#modalCart').modal('show');
				}
			});
			
		}
		
		function sendRequest(driverId,orderId)
		{
			console.log(driverId);
			console.log(orderId);
			route = '{!! route('admin_assign_order_driver') !!}?orderId='+orderId+'&driverId='+driverId;
			console.log(route);
			$.ajax
			({
				url: route,
				type: 'GET',
				async: true,
				dataType: "json",
				success: function (data) 
				{
					console.log(data);
					$('#modalCart').modal('hide');
					/*$('#appendList').empty();
					var HTML = "";
					for(var i =0;i<data.length;i++)
					{
						var cusername           = data[i].name;
						var DriverUserId        = data[i].user_id;
						var DriverUserDetailsId = data[i].user_detail_id;
						HTML += "<tr>";
						HTML += "<td>"+DriverUserId+"</td>";
						HTML += "<td>"+cusername+"</td>";
						HTML += "<td><a onclick='sendRequest("+DriverUserId+","+id+")' class='btn btn-primary pull-right'> Send Request </a></td>";
						HTML += "</tr>";
					}
					if(HTML == "")
					{
						HTML += "<tr>";
						HTML += "<td colspan='3'>No Driver Found</td>";
						HTML += "</tr>";
					}
					$('#appendList').html(HTML);
					$('#modalCart').modal('show');*/
				}
			});
		}
    $(document).ready(function()
        {

                $('input[name="daterange"]').daterangepicker({
                    opens: 'center',
                    autoApply: true,
                    format: 'DD/MM/YYYY',
                    minDate: '1/2/2018',
                    maxDate: moment().format('DD/MM/YYYY'),
                });

        });

    </script>


@stop
