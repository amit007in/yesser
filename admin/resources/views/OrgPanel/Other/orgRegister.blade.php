<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{env('APP_NAME')}} - Company Register</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="{{ URL::asset('AdminAssets/Images/favicon_yesser.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ URL::asset('AdminAssets/Images/favicon_yesser.ico') }}" type="image/x-icon">

    <link href="{{ URL::asset('AdminAssets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">

    <script src="{{ URL::asset('AdminAssets/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/bootstrap.min.js') }}"></script>

    <link href="{{ URL::asset('AdminAssets/css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('AdminAssets/css/style.css') }}" rel="stylesheet">

<style type="text/css">

    input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button 
            {
                -webkit-appearance: none; 
                margin: 0;
            }

</style>

</head>
<body style="background-color: #ffffff !important;">	

<?php

if(!Form::old('address_latitude'))
    $address_latitude = 21.4735;
else
    $address_latitude = Form::old('address_latitude');

if(!Form::old('address_longitude'))
    $address_longitude = 55.9754;
else
    $address_longitude = Form::old('address_longitude');

?>

		<div class="container">

			<center>
				<img src="{{ URL::asset('AdminAssets/Images/colored.png') }}" style="max-height: 110px; max-height: 110px; ">
			</center>

			<form method="post" action="{{route('org_register_post')}}" enctype="multipart/form-data" class="addForm" id="addForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <meta name="csrf-token" content="{{ csrf_token() }}">

			        @foreach($errors->all() as $error)
			            <div class="alert alert-dismissable alert-danger">
			                {!! $error !!}
			            </div>
			        @endforeach

			        @if (session('status'))
			            <div class="alert alert-success">
			                {{ session('status') }}
			            </div>
			        @endif


    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Name</label>
                <input type="text" placeholder="Name" autocomplete="off" class="form-control" required name="name" value="{!! Form::old('name') !!}" autofocus="on" id="name" maxlength="255">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Image (150px X 150px)</label>
                <input type="file" name="image" class="form-control" accept="image/*" required id="image">
            </div> 

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">


            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>Phone number</label>
                <div class="input-group m-b">
                    <div class="input-group-btn">
                        <select data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button" aria-expanded="false" name="phone_code" id="phone_code">
                            <option value="+968">Oman +968</option>
                            <!-- <option value="+971">UAE +971</option>
                            <option value="+974">Oatar +974</option>
                            <option value="+92">Pakistan +92</option>
                            <option value="+973">Bahrain +973</option>
                            <option value="+966">Saudi Arabia +966</option>
                            <option value="+965">Kuwait +965</option>
                            <option value="+91">India +91</option> -->
                        </select>
                    </div>
                <input type="number" minlength="5" placeholder="Phone number" autocomplete="off" class="form-control" required name="phone_number" value="{!! Form::old('phone_number') !!}" id="phone_number" minlength="6" maxlength="15">
                </div>
            </div>

             <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Username</label>
                <input type="text" placeholder="Username" autocomplete="off" class="form-control" required name="username" value="{!! Form::old('username') !!}" id="username" maxlength="255">
            </div> 

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Password</label>
                <input type="password" placeholder="Password" autocomplete="off" class="form-control" required name="password" id="password">
            </div> 

             <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Password Confirmation</label>
                <input type="password" placeholder="Password Confirmation" autocomplete="off" class="form-control" required name="password_confirmation" id="password_confirmation">
            </div>

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">

            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Licence Number</label>
                <input type="text" minlength="5" placeholder="Licence Number" autocomplete="off" class="form-control" required name="licence_number" value="{!! Form::old('licence_number') !!}" id="licence_number">
            </div> 

             <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label for="servicesid">Services</label>
    <select class="servicesid form-control border-info" id="servicesid" multiple="true" name="servicesid[]">
        @foreach($services as $service)
            <option value="{{$service->category_id}}">{{$service->name}}</option>
        @endforeach
    </select>
            </div> 
        </div>
    </div>
<br>
<input type="hidden" id="supportsid" name="supportsid[]" value="">
  <!--   <div class="row">
        <div class="form-group">
           
           

             <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label for="supportsid">Supports</label>
    <select class="supportsid form-control border-info" id="supportsid" multiple="true" name="supportsid[]">
        @foreach($supports as $support)
            <option value="{{$support->category_id}}">{{$support->name}}</option>
        @endforeach
    </select>
            </div>

        </div>
    </div>
<br> -->
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Address</label>
                <input type="text" placeholder="Address" autocomplete="off" class="form-control" required name="address" value="{!! Form::old('address') !!}" autofocus="on" id="address" maxlength="255">
            </div>

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Latitude</label>
                <input type="number" step="any" placeholder="Latitude" class="form-control" required name="address_latitude" value="{!! $address_latitude !!}" id="address_latitude" onfocusout="load_map()">
            </div>

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Longitude</label>
                <input type="number" step="any" placeholder="Longitude" class="form-control" required name="address_longitude" value="{!! $address_longitude !!}" id="address_longitude" onfocusout="load_map()">
            </div>

        </div>
    </div>
<br><br>
    <div class="row">

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-xs-offset-1 col-xs-10">
                    <div id="map1" style="width:100%; height:250px;"></div>
            </div>
        </div>

    </div>
<br><br>
                <div class="row">
                    <div class="form-group">
                        <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            {!! Form::submit('Register', ['class' => 'btn btn-lg btn-primary']) !!}
                            <a href="{{route('org_panel_login2')}}" class="btn btn-success btn-lg">Login</a>
                        </div>
                    </div>
                </div>
<br><br>
			</form>
			
		</div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key={{env('GOOGLE_MAP_KEY')}}"></script>

<script type="text/javascript">
	
        checkSuccess = function(response) {

            switch (jQuery.parseJSON(response).statuscode) {
                case 200:
                    return true;
                case 400:
                    return false;
            }
            return true;
        };

        $("#addForm").validate({
            rules: {
                password: {
                  required: true,
                  minlength: 6,
                },
                password_confirmation: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                },
                phone_number: {
                    remote: {
                            url: "{{route('org_unique_check')}}",
                            type: "POST",
                            cache: false,
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                phone_number: function() { return $("#phone_number").val(); }
                            },
                            dataFilter: function(response) {
                                return checkSuccess(response);
                            }
                        }

                },
                username: {
                    remote: {
                            url: "{{route('org_unique_check')}}",
                            type: "POST",
                            cache: false,
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                username: function() { return $("#username").val(); }
                            },
                            dataFilter: function(response) {
                                return checkSuccess(response);
                            }
                        }

                },
                licence_number: {
                    remote: {
                            url: "{{route('org_unique_check')}}",
                            type: "POST",
                            cache: false,
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                licence_number: function() { return $("#licence_number").val(); }
                            },
                            dataFilter: function(response) {
                                return checkSuccess(response);
                            }
                        }

                },

            },
            messages: {
                name: {
                    required: "Please provide the name"
                },
                image:{
                    required: "Please provide the image",
                    accept: "Only Image is allowed"
                },
                email: {
                    email: "Please provide a valid email",
                    required: "Please provide the email",
                    remote: "Sorry, this email already taken"
                },
                phone_number: {
                    remote: "Sorry, this phone number already taken",
                    required: "Phone number is required"
                },
                username: {
                    required: "Please provide the username",
                    remote: "Sorry, this username already taken"
                },
                licence_number: {
                    required: "Please provide the licence number",
                    remote: "Sorry, this licence number already taken"
                },
                password: {
                    required: "Please provide new password",
                    minlength: "Your new password must be at least 6 characters long"
                },
                password_confirmation: {
                    required: "Please provide the new password confirmation",
                    equalTo: "Please enter the same password confirmation as password"
                },
                buraq_percentage: {
                    required: "Please provide the yesser percentage"
                },
                address: {
                    required: "Please provide the address"
                }

            }
        });

     $("#servicesid").select2({
          dropdownCssClass: "bigdrop",
          placeholder: "Services"
      });
     $("#supportsid").select2({
          dropdownCssClass: "bigdrop",
          placeholder: "Supports"
      });

///////////////////     Map         ///////////////////////////////
    var myMap1;
    var mapOptions;
    var subjectMarker1;

    var roptions = {
        componentRestrictions: {country: ['om','in']}
    };

    function load_map()
        {

            address_latitude = document.getElementById('address_latitude').value;
            address_longitude = document.getElementById('address_longitude').value;

            mapOptions = {
                    zoom: 7,
                    center:new google.maps.LatLng(address_latitude,address_longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP  ,
                    scrollwheel: false,
                    fullscreenControl: false,
                };

            myMap1 = new google.maps.Map(document.getElementById('map1'), mapOptions);

            if(address_latitude != '' && address_longitude != '')
                {

                    myMap1.setCenter(new google.maps.LatLng(Number(address_latitude), Number(address_longitude)));

                    subjectMarker1 = new google.maps.Marker({
                        position: { "lat": Number(address_latitude), "lng": Number(address_longitude) },
                        title: 'Address',
                        map: myMap1,
                        label:'A'
                    });

                }

        }

        var address = (document.getElementById('address'));
        var autocomplete = new google.maps.places.Autocomplete(address, roptions);
        
        autocomplete.setTypes(['geocode']);
        google.maps.event.addListener(autocomplete, 'place_changed', function () 
            {
                var place = autocomplete.getPlace();
                if (!place.geometry) {  return;  }
                var address = '';
                if (place.address_components) {
                        address = [
                            (place.address_components[0] && place.address_components[0].short_name || ''),
                            (place.address_components[1] && place.address_components[1].short_name || ''),
                            (place.address_components[2] && place.address_components[2].short_name || '')
                        ].join(' ');

                    document.getElementById("address_latitude").value = place.geometry.location.lat();
                    document.getElementById("address_longitude").value = place.geometry.location.lng();
                    //console.log(place.geometry.location.lat(), place.geometry.location.lng());

                    load_map();
                                    
                }
                                
            });

        load_map();



</script>

</body>
</html>