@extends('OrgPanel.orgLayout')

    @section('title')
        eToken Payments
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

    <div class="row">
        <div class="col-xs-12">

<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        eToken Payments
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('org_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('org_etoken_pay_all')}}"><b>eToken Payments</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>

<div class="wrapper wrapper-content animated fadeInRight">

                    <div class="col-lg-4 col-md-4">
                        <div class="widget style1 blue-bg">
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-paypal fa-3x"></i>
                                </div>
                                <div class="col-xs-8 text-right">
                                    <span> Total </span>
                                    <h2 class="font-bold">{{$psums->completed_sum+$psums->pending_sum}}</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="widget style1 navy-bg">
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-thumbs-up fa-3x"></i>
                                </div>
                                <div class="col-xs-8 text-right">
                                    <span> Completed </span>
                                    <h2 class="font-bold">{{$psums->completed_sum}}</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="widget style1 yellow-bg">
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-clock-o fa-3x"></i>
                                </div>
                                <div class="col-xs-8 text-right">
                                    <span> Pending </span>
                                    <h2 class="font-bold">{{$psums->pending_sum}}</h2>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-offset-3 col-md-offset-3 col-lg-6 col-md-6">
                        <div class="form-group">
                            <label>
                                Filter Created At
                            </label>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
                        </div>
                    </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">

                        <div class="table-responsive">

            <table class="footable table table-stripped tablet breakpoint footable-loaded" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
    <th class="footable-visible footable-first-column footable-sortable">Id</th>
    <th class="footable-visible footable-first-column footable-sortable">Status</th>
    <th class="footable-visible footable-first-column footable-sortable">User Name</th>
    <th class="footable-visible footable-first-column footable-sortable">User Pic</th>
    <th class="footable-visible footable-first-column footable-sortable">Transaction Id</th>
    <th class="footable-visible footable-first-column footable-sortable">Admin Fee</th>
    <th class="footable-visible footable-first-column footable-sortable">Amount</th>
    <th class="footable-visible footable-first-column footable-sortable">Product</th>
    <th class="footable-visible footable-first-column footable-sortable">Created At</th>
                                </tr>

                                </thead>
                                <tbody>

                @foreach($payments as $payment)
                <tr class="gradeA footable-odd" style="display: table-row;">

                    <td>{{ $payment->payment_id }}</td>

                    <td>
                    @if($payment->payment_status == "Completed")
                        <span class="label label-primary">Completed</span>
                    @elseif($payment->payment_status == "Pending")
                        <span class="label label-warning">Pending</span>
                    @else
                        <span class="label label-success">{{$payment->payment_status}}</span>
                    @endif
                    </td>

                    <td>
                        {{ $payment['payer']->name }}
                    </td>

                    <td>
                    <a href="{{ $payment['payer_detail']->profile_pic_url }}" title="{{ $payment['payer']->name }}" class="lightBoxGallery img-circle" data-gallery="">
                    <img src="{{ $payment['payer_detail']->profile_pic_url }}/150/150"  class="img-circle">
                    </a>
                    </td>


                    <td>{{ $payment->transaction_id }}</td>

                    <td><b>OMR</b> {{ $payment->admin_charge }}</td>

                    <td><b>OMR</b> {{ $payment->final_charge }}</td>

                    <td>
                        {{ $payment->purchased_etokens->category['name'] }}<hr>
                        {{ $payment->purchased_etokens->category_brand['name'] }}<hr>
                        {{ $payment->purchased_etokens->category_brand_product['name'] }}
                    </td>

                    <td>
                        <i class="fa fa-clock-o"></i> {{ $payment->updated_atz }}
                    </td>

</tr>
                @endforeach

                                </tbody>
                            </table><br><br><br><br>

                            </div>
                        </div>
                    </div>

</div>

        </div>
    </div>
</div>

                <script>

        $("#payments_all").addClass("active");
        $("#org_etoken_pay_all").addClass("active");        

        $(document).ready(function()
            {

                $('input[name="daterange"]').daterangepicker({
                        format: 'DD/MM/YYYY',
                        minDate: '1/2/2018',
                        maxDate: moment().format('DD/MM/YYYY'),
                        opens: 'center',
                        drops: 'down',
                        buttonClasses: ['btn', 'btn-sm'],
                        applyClass: 'btn-primary',
                        cancelClass: 'btn-default'
                            });

                $('#daterange').on('apply.daterangepicker', function(ev, picker)
                    {
                        dt = document.getElementById('daterange').value;

                        window.location.href = '{{route("org_etoken_pay_all")}}?daterange='+dt;

                    });

            });

            var table = $('#example').dataTable( 
                {
                   "order": [[ 0, "desc" ]],
                    // "scrollY": 500,
                    // "scrollX": true,
                    "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 2 ] },
                        { "bSearchable": true, "aTargets": [ 1, 3, 4, 5, 6 ] }
                    ],
                    dom: 'Blfrtip',
                    buttons: [
                       {
                            extend: 'pdf',
                            footer: true,
                            exportOptions: {
                                columns: [0,1,3,4,5,6,7,8]
                            }
                       },
                       {
                           extend: 'csv',
                           footer: false,
                            exportOptions: {
                                columns: [0,1,3,4,5,6,7,8]
                            }
                          
                       },
                       {
                           extend: 'excel',
                           footer: false,
                            exportOptions: {
                                columns: [0,1,3,4,5,6,7,8]
                            }
                       }         
                    ]

                });

    </script>


@stop
