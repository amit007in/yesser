@extends('OrgPanel.orgLayout')

    @section('title')
        Support Payments
    @stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    <!-- <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script> -->

    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

    <div class="row">
        <div class="col-xs-12">

<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Support Payments
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('org_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('org_sup_pay_all')}}"><b>Support Payments</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>

<div class="wrapper wrapper-content animated fadeInRight">

                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="form-group">
                            <label class="pull-left">Status</label>
                                <select class="form-control" id="filter1">
                                    <option value="">All</option>
                                    <option value="Completed">Completed</option>
                                    <option value="Pending">Pending</option>
                                    <option value="Failed">Failed</option>
                                    <option value="Cancelled">Cancelled</option>
                                    <option value="Refunded">Refunded</option>
                                </select>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="form-group">
                            <label>
                                Filter Created At
                            </label>
    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$fstarting_dt}} - {{$fending_dt}}"/>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="form-group">
                            <label class="pull-left">Type</label>
                                <select class="form-control" id="filter2">
                                    <option value="">All</option>
                                    <option value="Cash">Cash</option>
                                    <option value="Card">Card</option>
<!--                                     <option value="Coupon">Coupon</option> -->
                                    <option value="eToken">eToken</option>
                                </select>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="form-group">
                            <label class="pull-left">Receiver Type</label>
                                <select class="form-control" id="filter9">
                                    <option value="">All</option>
                                    <option value="Independent">Independent</option>
                                    <option value="Company">Company</option>
                                </select>
                        </div>
                    </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="widget style1 blue-bg">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-paypal fa-3x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span> Total </span>
                            <h2 class="font-bold">{{$psums->total_sum}}</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="widget style1 navy-bg">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-credit-card fa-3x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span> Completed Card </span>
                            <h2 class="font-bold">{{$psums->completed_card_sum}}</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="widget style1 red-bg">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-money fa-3x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span> Completed Cash </span>
                            <h2 class="font-bold">{{$psums->completed_cash_sum}}</h2>
                        </div>
                    </div>
                </div>
            </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">

                        <div class="table-responsive">

            <table class="footable table table-stripped tablet breakpoint footable-loaded" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
    <th class="footable-visible footable-first-column footable-sortable">Id</th>
    <th class="footable-visible footable-first-column footable-sortable">Status</th>
    <th class="footable-visible footable-first-column footable-sortable">Type</th>
    <th class="footable-visible footable-first-column footable-sortable">User Name</th>
    <th class="footable-visible footable-first-column footable-sortable">User Pic</th>
    <th class="footable-visible footable-first-column footable-sortable">Transaction Id</th>
    <th class="footable-visible footable-first-column footable-sortable">Amount</th>
    <th class="footable-visible footable-first-column footable-sortable">Admin Fee</th>
    <th class="footable-visible footable-first-column footable-sortable">Support</th>
    <th class="footable-visible footable-first-column footable-sortable">Driver</th>
    <th class="footable-visible footable-first-column footable-sortable">Driver Pic</th>
    <th class="footable-visible footable-first-column footable-sortable">Order Details</th>
    <th class="footable-visible footable-first-column footable-sortable">Created At</th>
                                </tr>

                                </thead>
                                <tbody>

                @foreach($payments as $payment)
                    <tr class="gradeA footable-odd" style="display: table-row;">

                        <td>{{ $payment->payment_id }}</td>

                        <td>
                            @if($payment->payment_status == "Completed")
                                <span class="label label-primary">
                                    Completed
                                </span>
                            @elseif($payment->payment_status == "Pending")
                                <span class="label label-warning">
                                    Pending
                                </span>
                            @elseif($payment->payment_status == "Failed")
                                <span class="label label-info">
                                    Failed
                                </span>
                            @elseif($payment->payment_status == "Refunded")
                                <span class="label label-success">
                                    Refunded
                                </span>
    @elseif($payment->payment_status == "CustCancel" || $payment->payment_status == "DriverCancel" || $payment->payment_status == "SerReject" || $payment->payment_status == "SupReject" || $payment->payment_status == "DSchCancelled" || $payment->payment_status == "SysSchCancelled" || $payment->payment_status == "DSchTimeout" )
                                <span class="label label-danger">
                                    Cancelled
                                </span>
                            @endif
                        </td>

                        <td>
                            @if($payment->payment_type == "Cash")
                                <span class="label label-primary">
                                    Cash
                                </span>
                            @elseif($payment->payment_type == "Card")
                                <span class="label label-success">
                                    Card
                                </span>
                            @else
                                <span class="label label-danger">
                                    {{ $payment->payment_type }}
                                </span>
                            @endif
                        </td>

                        <td>
                            {{ $payment['payer']->name }}
                        </td>

                        <td>
        <a href="{{ $payment['payer_detail']->profile_pic_url }}" title="{{ $payment['payer']->name }}" class="lightBoxGallery img-circle" data-gallery="">
            <img src="{{ $payment['payer_detail']->profile_pic_url }}/150/150"  class="img-circle">
        </a>
                        </td>


                        <td>{{ $payment->transaction_id }}</td>

                        <td><b>OMR</b> {{ $payment->final_charge }}</td>

                        <td><b>OMR</b> {{ $payment->admin_charge }}</td>

                        <td>
                            {{ $payment->order['category']['name'] }}
                        </td>
                        
                    <td>
                        @if($payment['seller'])
                            {{ $payment['seller']->name }} - ({{ $payment['seller']->phone_number }})
                        @endif                        
                    </td>

                    <td>
                        @if($payment['seller'])
    <a href="{{ $payment['seller_detail']->profile_pic_url }}" title="{{ $payment['seller']->name }}" class="lightBoxGallery img-circle" data-gallery="">
        <img src="{{ $payment['seller_detail']->profile_pic_url }}/150/150"  class="img-circle">
    </a>
                        @endif
                    </td>


                    <td>
            <a href="{{ route('org_order_details', ['order_id' => $payment->order_id] ) }}" class="btn btn-primary pull-right" id="Details" title="Details">
                <i class="fa fa-ioxhost"> Order Details</i>
            </a>
                    </td>

                        <td><i class="fa fa-clock-o"></i> &nbsp;
                            {{ $payment->updated_atz }}
                        </td>

                    </tr>
                @endforeach

                                </tbody>
                            </table><br><br><br><br>

                            </div>
                        </div>
                    </div>


</div>


        </div>
    </div>
</div>

                <script>

        $("#payments_all").addClass("active");
        $("#org_sup_pay_all").addClass("active");        

        $(document).ready(function()
            {

                $('input[name="daterange"]').daterangepicker({
                        format: 'DD/MM/YYYY',
                        minDate: '1/2/2018',
                        maxDate: moment().format('DD/MM/YYYY'),
                        opens: 'center',
                        drops: 'down',
                        buttonClasses: ['btn', 'btn-sm'],
                        applyClass: 'btn-primary',
                        cancelClass: 'btn-default'
                            });

                $('#daterange').on('apply.daterangepicker', function(ev, picker)
                    {

                        dt = document.getElementById('daterange').value;

                        window.location.href = '{{route("org_sup_pay_all")}}?daterange='+dt;

                    });

            });

            var table = $('#example').dataTable( 
                {
                   "order": [[ 0, "desc" ]],
                    // "scrollY": 500,
                    // "scrollX": true,
                    "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 4, 11, 12 ] },
                        { "bSearchable": true, "aTargets": [ 3, 5, 6, 7, 8, 9, 12 ] }
                    ],
                    dom: 'Blfrtip',
                    buttons: [
                       {
                            extend: 'pdf',
                            footer: true,
                            exportOptions: {
                                columns: [0,1,2,3,5,6,7,8,9,12]
                            }
                       },
                       {
                           extend: 'csv',
                           footer: false,
                            exportOptions: {
                                columns: [0,1,2,3,5,6,7,8,9,12]
                            }
                          
                       },
                       {
                           extend: 'excel',
                           footer: false,
                            exportOptions: {
                                columns: [0,1,2,3,5,6,7,8,9,12]
                            }
                       }         
                    ]

                });

            $("#filter1").on('change', function()
                {
                    table.fnFilter($(this).val(), 1);
                });
            $("#filter2").on('change', function()
                {
                    table.fnFilter($(this).val(), 2);
                });

    </script>


@stop
