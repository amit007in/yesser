@extends('OrgPanel.orgLayout')

@section('title') 
    Profile Update
@stop

@section('content')

    <link href="{{ URL::asset('AdminAssets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('AdminAssets/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Profile Update</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('org_dashboard') }}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ route('org_profile') }}">
                                <b>Profile Update</b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

        <br>
            <form method="post" action="{{route('org_pprofile') }}" enctype="multipart/form-data" id="UpdateForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Name</label>
                <input type="text" placeholder="Name" autocomplete="off" class="form-control" required name="name" value="{!! $organisation->name !!}" autofocus="on" id="name" maxlength="255">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Image (150px X 150px)</label>
                <input type="file" name="image" class="form-control" accept="image/*" id="image">
            </div> 

        </div>
    </div>

<br>

    <div class="row">
        <div class="form-group">

            <div class='col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1'>
                <label>Phone number</label>
                <div class="input-group m-b">
                    <div class="input-group-btn">
                        <select data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button" aria-expanded="false" name="phone_code" id="phone_code" value="{{$organisation->phone_code}}">
                            <option value="+968">Oman +968</option>
                            <option value="+971">UAE +971</option>
                            <option value="+974">Oatar +974</option>
                            <option value="+92">Pakistan +92</option>
                            <option value="+973">Bahrain +973</option>
                            <option value="+966">Saudi Arabia +966</option>
                            <option value="+965">Kuwait +965</option>
                            <option value="+91">India +91</option>
                        </select>
                    </div>
                <input type="number" minlength="5" placeholder="Phone number" autocomplete="off" class="form-control" required name="phone_number" value="{!! $organisation->phone_number !!}" id="phone_number" minlength="6" maxlength="15">
                </div>
            </div>

             <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Licence Number</label>
                <input type="text" minlength="5" placeholder="Licence Number" disabled="true" autocomplete="off" class="form-control" required name="licence_number" value="{!! $organisation->licence_number !!}" id="licence_number">
            </div>

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Username</label>
                <input type="text" placeholder="Username" disabled="true" autocomplete="off" class="form-control" required name="username" value="{!! $organisation->username !!}" id="username" maxlength="255">
            </div> 

             <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
    <label>Credit Starting Date</label>
        <div class="input-group date">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="credit_start_dt" id="credit_start_dt" value="@if($organisation->credit_start_dt != NULL) {{$organisation->credit_start_dt}} @endif" id="credit_day_limit" placeholder="Credit Starting Date" disabled="true">
        </div>
            </div>

        </div>
    </div>

<br>

    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Credit Amount Limit</label>
                @if($organisation->credit_amount_limit == -1)
                    <input type="number" placeholder="Credit Amount Limit" autocomplete="off" class="form-control" name="credit_amount_limit" value="" step="any" id="credit_amount_limit" min="0" disabled="true">
                @else
                    <input type="number" placeholder="Credit Amount Limit" autocomplete="off" step="any" class="form-control" name="credit_amount_limit" value="{{$organisation->credit_amount_limit}}" id="credit_amount_limit" min="0" disabled="true">
                @endif
            </div> 

             <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Credit Days Limit</label>

                @if($organisation->credit_day_limit == -1)
                    <input type="number" placeholder="Credit Days Limit" autocomplete="off" class="form-control" name="credit_day_limit" value="" id="credit_day_limit" min="0" disabled="true">
                @else
                    <input type="number" placeholder="Credit Days Limit" autocomplete="off" class="form-control" name="credit_day_limit" value="{{$organisation->credit_day_limit}}" id="credit_day_limit" min="0" disabled="true">
                @endif
            </div>

        </div>
    </div>

<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label for="servicesid">Services</label>
    <select class="servicesid form-control border-info" id="servicesid" multiple="true" name="servicesid[]">
        @foreach($services as $service)
            @if($service->is_selected == '1')
                <option value="{{$service->category_id}}" selected="true">{{$service->cat_name}}</option>
            @else
                <option value="{{$service->category_id}}">{{$service->cat_name}}</option>
            @endif
        @endforeach
    </select>
            </div> 

             <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label for="servicesid">Supports</label>
    <select class="supportsid form-control border-info" id="supportsid" multiple="true" name="supportsid[]">
        @foreach($supports as $support)
            @if($support->is_selected == '1')
                <option value="{{$support->category_id}}" selected="true">{{$support->cat_name}}</option>
            @else
                <option value="{{$support->category_id}}">{{$support->cat_name}}</option>
            @endif
        @endforeach
    </select>
            </div>

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label for="servicesid">Address</label>
                <input type="text" placeholder="Address" autocomplete="off" class="form-control" required name="address" value="{!! $organisation->address !!}" autofocus="on" id="address" maxlength="255">
            </div> 

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Latitude</label>
                <input type="number" step="any" placeholder="Latitude" class="form-control" required name="address_latitude" value="{!! $organisation->address_latitude !!}" id="address_latitude" onfocusout="load_map()">
            </div>

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Longitude</label>
                <input type="number" step="any" placeholder="Longitude" class="form-control" required name="address_longitude" value="{!! $organisation->address_longitude !!}" id="address_longitude" onfocusout="load_map()">
            </div>

        </div>
    </div>
<br><br>
    <div class="row">

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-xs-offset-1 col-xs-10">

                    <div id="map1" style="width:100%; height:250px;"></div>

            </div>
        </div>

    </div>
<br>
            <br><br><br>

                <div class="row">
                    <div class="form-group">
                        <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            {!! Form::submit('Update Profile', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<!--     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script> -->
    <script src="{{ URL::asset('AdminAssets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key={{env('GOOGLE_MAP_KEY')}}"></script>


<script type="text/javascript">

        $("#profile").addClass("active");

        document.getElementById("phone_code").value = "{{$organisation->phone_code}}";

        checkSuccess = function(response) {

            switch (jQuery.parseJSON(response).statuscode) {
                case 200:
                    return true;
                case 400:
                    return false;
            }
            return true;
        };

        $("#UpdateForm").validate({
            rules: {
                password: {
                  required: true,
                  minlength: 6,
                  notEqual: "#old_password"
                },
                password_confirmation: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                },
                phone_number: {
                    remote: {
                            url: "{{route('org_uunique_check')}}",
                            type: "POST",
                            cache: false,
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                phone_number: function() { return $("#phone_number").val(); },
                                organisation_id: "{{$organisation->organisation_id}}"
                            },
                            dataFilter: function(response) {
                                return checkSuccess(response);
                            }
                        }

                }

            },
            messages: {
                name: {
                    required: "Please provide the name"
                },
                image:{
                    required: "Please provide the image",
                    accept: "Only Image is allowed"
                },
                phone_number: {
                    remote: "Sorry, this phone number already taken",
                    required: "Phone number is required"
                },
                username: {
                    required: "Please provide the username"
                },
                licence_number: {
                    required: "Please provide the licence number",
                    remote: "Sorry, this licence number already taken"
                },
                password: {
                    required: "Please provide new password",
                    minlength: "Your new password must be at least 6 characters long",
                    notEqual: "New password cannot be same as old password"
                },
                password_confirmation: {
                    required: "Please provide the new password confirmation",
                    equalTo: "Please enter the same password confirmation as password"
                },

            }
        });

     $("#servicesid").select2({
          dropdownCssClass: "bigdrop",
          placeholder: "Services"
      });
     $("#supportsid").select2({
          dropdownCssClass: "bigdrop",
          placeholder: "Supports"
      });

           $('#credit_start_dt').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: false,
                autoclose: true,
                startDate: new Date(),
                viewMode: 'years',
                format: 'yyyy-mm-dd'

            });

    var myMap1;
    var mapOptions;
    var subjectMarker1;

    var roptions = {
        componentRestrictions: {country: ['om','in']}
    };

    function load_map()
        {

            address_latitude = document.getElementById('address_latitude').value;
            address_longitude = document.getElementById('address_longitude').value;

            mapOptions = {
                    zoom: 7,
                    center:new google.maps.LatLng(address_latitude,address_longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP  ,
                    scrollwheel: false,
                    fullscreenControl: false,
                };

            myMap1 = new google.maps.Map(document.getElementById('map1'), mapOptions);

            if(address_latitude != '' && address_longitude != '')
                {

                    myMap1.setCenter(new google.maps.LatLng(Number(address_latitude), Number(address_longitude)));

                    subjectMarker1 = new google.maps.Marker({
                        position: { "lat": Number(address_latitude), "lng": Number(address_longitude) },
                        title: 'Address',
                        map: myMap1,
                        label:'A'
                    });

                }

        }

        var address = (document.getElementById('address'));
        var autocomplete = new google.maps.places.Autocomplete(address, roptions);
        
        autocomplete.setTypes(['geocode']);
        google.maps.event.addListener(autocomplete, 'place_changed', function () 
            {
                var place = autocomplete.getPlace();
                if (!place.geometry) {  return;  }
                var address = '';
                if (place.address_components) {
                        address = [
                            (place.address_components[0] && place.address_components[0].short_name || ''),
                            (place.address_components[1] && place.address_components[1].short_name || ''),
                            (place.address_components[2] && place.address_components[2].short_name || '')
                        ].join(' ');

                    document.getElementById("address_latitude").value = place.geometry.location.lat();
                    document.getElementById("address_longitude").value = place.geometry.location.lng();
                    //console.log(place.geometry.location.lat(), place.geometry.location.lng());

                    load_map();
                                    
                }
                                
            });

        load_map();

</script>


@stop



