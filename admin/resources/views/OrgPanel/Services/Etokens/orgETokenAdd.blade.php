@extends('OrgPanel.orgLayout')

@section('title') 
    Add EToken
@stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add EToken</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('org_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('org_services_all')}}">Services</a>
                        </li>
                        <li>
                        <a href="{{ route('org_ser_coupons',['category_id' =>$category_id]) }}">
                            Service ETokens
                        </a>
                        </li>
                        <li>
                        <a href="{{route('org_coupon_add',['category_id' => $category_id ])}}">
                            <b>
                                Add EToken
                            </b>
                        </a>                            
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

    <div class="wrapper wrapper-content animated fadeInRight">

       <div class="row">
            <div class="col-md-6 col-lg-offset-3">
                <div class="ibox-content text-center">
                    <h2>{{$category->category_details[0]->name}}</h2>
                </div>
            </div>
        </div>

        <br>
            <form method="post" action="{{route('org_coupon_padd', ['category_id' => $category_id ]) }}" enctype="multipart/form-data" id="addForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Price</label>
                <input type="number" placeholder="Price" step="any" autocomplete="off" class="form-control" required name="price" value="{!! Form::old('price') !!}" autofocus="on" id="price" min="0">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Quantity</label>
                <input type="number" placeholder="Quantity" autocomplete="off" class="form-control" required name="quantity" value="{!! Form::old('quantity') !!}" id="quantity" min="1">
            </div> 

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">

            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Brand</label>
                <select name="category_brand_id" id="category_brand_id" class="form-control border-info" onchange="load_products(this.value)">
                    @foreach($brands as $brand)
                        <option value="{{$brand->category_brand_id}}" >{{$brand->name}}</option>
                    @endforeach
                </select>
            </div> 

             <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Products</label>
                    <select class="category_brand_product_id form-control border-info" id="category_brand_product_id" required="true" name="category_brand_product_id">
                    </select>
            </div>           

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Description</label>
                <textarea class="form-control" name="description" rows="4">{{Form::old('description')}}</textarea>
            </div> 

        </div>
    </div>

            <br><br><br>

                <div class="row">
                    <div class="form-group">
                        <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            {!! Form::submit('Add EToken', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

<script type="text/javascript">

        $("#services_all").addClass("active");

        checkSuccess = function(response) {

            switch (jQuery.parseJSON(response).statuscode) {
                case 200:
                    return true;
                case 400:
                    return false;
            }
            return true;
        };

        $("#addForm").validate({
            rules: {
            },
            messages: {
                price: {
                    required: "Please provide the price"
                },
                quantity:{
                    required: "Please provide the quantity"
                },
                category_brand_product_id: {
                    required: "Please select a product"
                }
            }
        });

    function load_products(category_brand_id)
        {

            $.ajax({
                type: "POST",
                cache: false,
                dataType: "json",
                url: '{{route("brand_products")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: 'category_brand_id='+category_brand_id+"&admin_language_id={{$OrgDetails->organisation['language_id']}}",
                success:function(result){

                    if(result.success == 1)
                        $('#category_brand_product_id').html(result.con);
                    else
                        {
                            toastr.error('Error.', result.msg );
                        }
                },
                error: function(result){
                    toastr.error('Error.', result.msg );
                }
            }); 

        }

    load_products($('#category_brand_id').val());

</script>


@stop



