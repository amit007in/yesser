    @extends('OrgPanel.orgLayout')

    @section('title')
        Services
    @stop

    @section('content')

    <link href="{{ URL::asset('AdminAssets/css/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('AdminAssets/js/plugins/daterangepicker/daterangepicker.js') }}"></script>

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Services
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('org_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                        <a href="{{route('org_services_all')}}">
                            <b>
                            Services
                            </b>                            
                        </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Service Name</th>
                                    <th>Yesser Percentage</th>
                                    <th>Actions</th>
                                    <th>Products</th>
                                    <th>Yesser Man</th>
<!--                                     <th>Orders</th> -->
                                </tr>

                                </thead>
                                <tbody>


            @foreach($services as $service)
                <tr class="gradeA footable-odd" style="display: table-row;">

                    <td>{{ $service->category_id }}</td>

                    <td>
                        <b>
                            {{ $service->category['category_details'][0]->name }}
                        </b>(English)<hr>

                        <!-- <b>
                            {{ $service->category['category_details'][1]->name }}
                        </b>(Hindi)<hr>

                        <b>
                            {{ $service->category['category_details'][2]->name }}
                        </b>(Urdu)<hr>

                        <b>
                            {{ $service->category['category_details'][3]->name }}
                        </b>(Chinese)<hr>
 -->
                        <b>
                            {{ $service->category['category_details'][4]->name }}
                        </b>(Arabic)<hr>
                    </td>

                    <td>
                        {{ $service->category['buraq_percentage'] }}%
                    </td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('org_service_products', ['category_id' => $service->category_id ] ) }}">
        <i class="fa fa-product-hunt"> All Products</i> - {{ $service->product_counts }}
    </a>
                                </li>

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('org_ser_drivers', ['category_id' => $service->category_id ]) }}">
        <i class="fa fa-child"> All Drivers</i> - {{ $service->driver_counts }}
    </a>
                                </li>

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('org_ser_adriver', ['category_id' => $service->category_id ]) }}">
        <i class="fa fa-plus"> Add Driver</i>
    </a>
                                </li>

                                @if($service->category_id == 2)

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('org_ser_areas', ['category_id' => $service->category_id ]) }}">
        <i class="fa fa-map-marker"> All Areas</i> - {{$service->area_counts}}
    </a>
                                </li>

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('org_area_add', ['category_id' => $service->category_id ]) }}">
        <i class="fa fa-plus"> Add Area</i>
    </a>
                                </li>

                              <!--   <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('org_ser_coupons', ['category_id' => $service->category_id ]) }}">
        <i class="fa fa-tag"> All ETokens</i> - {{$service->coupon_counts}}
    </a>
                                </li>

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('org_coupon_add', ['category_id' => $service->category_id ]) }}">
        <i class="fa fa-plus"> Add EToken</i>
    </a>
                                </li> -->

                                @endif

                            </ul>
                        </div>
                    </td>

                    <td>
                        <a href="{{ route('org_service_products', ['category_id' => $service->category_id ] ) }}" class="btn btn-default">
                            {{ $service->product_counts }}
                        </a>
                    </td>

                    <td>
                        <a href="{{ route('org_ser_drivers', [ 'category_id' => $service->category_id ]) }}" class="btn btn-default">
                            {{ $service->driver_counts }}
                        </a>
                    </td>

<!--                     <td>
                        <a href="#" class="btn btn-default">
                            {{ $service->order_counts }}
                        </a>
                    </td>
 -->

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

        $("#services_all").addClass("active");

            var table = $('#example').dataTable( 
                {
//                   "order": [[ 0, "desc" ]],
                   "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [ 2 ] },
                                { "bSearchable": true, "aTargets": [ 1 ] }
                    ]
                });

    </script>


@stop
