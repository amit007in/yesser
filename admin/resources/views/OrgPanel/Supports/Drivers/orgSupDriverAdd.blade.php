@extends('OrgPanel.orgLayout')

@section('title') 
    Add Driver
@stop

    @section('content')

<link href="{{ URL::asset('AdminAssets/css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Add Driver</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('org_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('org_supports_all')}}">Supports</a>
                        </li>
                        <li>
                        <a href="{{ route('org_sup_drivers',['category_id' =>$category_id]) }}">
                            Support Drivers
                        </a>
                        </li>
                        <li>
                        <a href="{{route('org_sup_adriver',['category_id' => $category_id ])}}">
                            <b>
                                Add Drivers
                            </b>
                        </a>                            
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

    <div class="wrapper wrapper-content animated fadeInRight">

       <div class="row">
            <div class="col-md-6 col-lg-offset-3">
                <div class="ibox-content text-center">
                    <h2>{{$category->category_details[0]->name}}</h2>
                </div>
            </div>
        </div>

        <br>
            <form method="post" action="{{route('org_sup_padriver', ['category_id' => $category_id ]) }}" enctype="multipart/form-data" id="addForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Name</label>
                <input type="text" placeholder="Name" autocomplete="off" class="form-control" required name="name" value="{!! Form::old('name') !!}" autofocus="on" id="name" maxlength="255">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Profile Pic (150px X 150px)</label>
                <input type="file" name="profile_pic" class="form-control" accept="image/*" required id="profile_pic">
            </div> 

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Phone number</label>
                <input type="number" minlength="5" placeholder="Phone number" autocomplete="off" class="form-control" required name="phone_number" value="{!! Form::old('phone_number') !!}" id="phone_number" minlength="6" maxlength="15">
            </div>

        </div>
    </div>

<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Mulkiya Number</label>
                <input type="text" placeholder="Mulkiya Number" autocomplete="off" class="form-control" required name="mulkiya_number" value="{!! Form::old('mulkiya_number') !!}" id="mulkiya_number" maxlength="255">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
            <label>Mulkiya Validity date</label>
    <div class="input-group date">
    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="mulkiya_validity" id="mulkiya_validity" value="{{Request::old('mulkiya_validity')}}" placeholder="Mulkiya Validity date" required>
    </div>
            </div> 


        </div>
    </div>

<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Mulkiya Front</label>
                <input type="file" name="mulkiya_front" class="form-control" accept="image/*" id="mulkiya_front" required>
            </div>

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                <label>Mulkiya Back</label>
                <input type="file" name="mulkiya_back" class="form-control" accept="image/*" id="mulkiya_back" required>
            </div>

        </div>
    </div>

            <br><br><br>

                <div class="row">
                    <div class="form-group">
                        <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            {!! Form::submit('Add Driver', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>

    <script src="{{ URL::asset('AdminAssets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">

        $("#supports").addClass("active");

        checkSuccess = function(response) {

            switch (jQuery.parseJSON(response).statuscode) {
                case 200:
                    return true;
                case 400:
                    return false;
            }
            return true;
        };

        $("#addForm").validate({
            rules: {
                phone_number: {
                    remote: {
                            url: "{{route('driver_unique_acheck')}}",
                            type: "POST",
                            cache: false,
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                phone_number: function() { return $("#phone_number").val(); }
                            },
                            dataFilter: function(response) {
                                return checkSuccess(response);
                            }
                        }

                },
                mulkiya_number: {
                    remote: {
                            url: "{{route('driver_unique_acheck')}}",
                            type: "POST",
                            cache: false,
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                mulkiya_number: function() { return $("#mulkiya_number").val(); }
                            },
                            dataFilter: function(response) {
                                return checkSuccess(response);
                            }
                        }

                }

            },
            messages: {
                name: {
                    required: "Please provide the name"
                },
                profile_pic:{
                    required: "Please provide the profile pic",
                    accept: "Only Image is allowed"
                },
                phone_number: {
                    remote: "Sorry, this phone number already taken",
                    required: "Phone number is required"
                },
                mulkiya_number: {
                    remote: "Sorry this Mulkiya number is already taken",
                    required: "Mulkiya number is required"
                },
                mulkiya_validity: {
                    required: "Mulkiya Validity date is required"
                },
                mulkiya_front: {
                  required: "Mulkiya front image is required"  
                },
                mulkiya_back: {
                  required: "Mulkiya back image is required"  
                }
            }
        });

    $('#mulkiya_validity').datetimepicker({
        startDate: new Date(),
    });


</script>


@stop



