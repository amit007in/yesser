@extends('OrgPanel.orgLayout')

@section('title') 
    Update Driver
@stop

    @section('content')

    <link href="{{ URL::asset('AdminAssets/css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Update Driver</h2>
                    <ol class="breadcrumb">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('org_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('org_supports_all')}}">Supports</a>
                        </li>
                        <li>
                        <a href="{{ route('org_sup_drivers',['category_id' =>$category_id]) }}">
                            Support Drivers
                        </a>
                        </li>
                        <li>
                        <a href="{{route('org_sup_udriver',['user_detail_id' => $driver->user_detail_id ])}}">
                            <b>
                                Update Drivers
                            </b>
                        </a>                            
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        
        <div class="row">
            <div class="col-md-6 col-lg-offset-3">
                <div class="ibox-content text-center">
                    <h2>{{$category->category_details[0]->name}}</h2>
                </div>
            </div>
        </div>

        <br>
            <form method="post" action="{{route('org_sup_pudriver', ['user_detail_id' =>$driver->user_detail_id, 'user_id' => $driver->user_id ]) }}" enctype="multipart/form-data" id="addForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Name</label>
                <input type="text" placeholder="Name" autocomplete="off" class="form-control" required name="name" value="{!! $driver->name !!}" autofocus="on" id="name" maxlength="255">
            </div> 

            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <center>
                    <a href="{!! $driver->profile_pic_image !!}" title="{!! $driver->name !!}" class="lightBoxGallery pull-right" data-gallery="">
                        <img src="{!! $driver->profile_pic_url !!}"  class="img-circle">
                    </a>
                </center>
            </div>


            <div class='col-lg-4 col-md-4 col-sm-4'>
                <label>Profile Pic (150px X 150px)</label>
                <input type="file" name="profile_pic" class="form-control" accept="image/*" id="profile_pic">
            </div> 

        </div>
    </div>
<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Phone number</label>
                <input type="number" minlength="5" placeholder="Phone number" autocomplete="off" class="form-control" required name="phone_number" value="{!! $driver->phone_number !!}" id="phone_number" minlength="6" maxlength="15">
            </div> 

        </div>
    </div>

<br>
    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Mulkiya Number</label>
                <input type="text" placeholder="Mulkiya Number" autocomplete="off" class="form-control" required name="mulkiya_number" value="{!! $driver->mulkiya_number !!}" id="mulkiya_number" maxlength="255">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
            <label>Mulkiya Validity date</label>
    <div class="input-group date">
    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="mulkiya_validity" id="mulkiya_validity" value="{{ $driver->mulkiya_validity }}" placeholder="Mulkiya Validity date" required>
    </div>
            </div> 


        </div>
    </div>

<br>
    <div class="row">
        <div class="form-group">

            @if($driver->mulkiya_front != '')

                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                    <center>
                        <a href="{!! $driver->mulkiya_front_url !!}" title="{!! $driver->name !!} - Mulkiya Front" class="lightBoxGallery" data-gallery="">
                            <img src="{!! $driver->mulkiya_front_url !!}"  class="img-circle">
                        </a>
                    </center>
                </div>

                <div class='col-lg-4 col-md-4 col-sm-4'>
                    <label>Mulkiya Front</label>
                    <input type="file" name="mulkiya_front" class="form-control" accept="image/*" id="mulkiya_front">
                </div>

            @else

                <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                    <label>Mulkiya Front</label>
                    <input type="file" name="mulkiya_front" class="form-control" accept="image/*" id="mulkiya_front">
                </div>

            @endif


            @if($driver->mulkiya_back != '')

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                    <center>
                        <a href="{!! $driver->mulkiya_back_url !!}" title="{!! $driver->name !!} - Mulkiya Back" class="lightBoxGallery pull-right" data-gallery="">
                            <img src="{!! $driver->mulkiya_back_url !!}"  class="img-circle">
                        </a>
                    </center>
                </div>

                <div class='col-lg-4 col-md-4 col-sm-4'>
                    <label>Mulkiya Back</label>
                    <input type="file" name="mulkiya_back" class="form-control" accept="image/*" id="mulkiya_back">
                </div>

            @else

                <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                    <label>Mulkiya Back</label>
                    <input type="file" name="mulkiya_back" class="form-control" accept="image/*" id="mulkiya_back">
                </div>

            @endif

        </div>
    </div>

            <br><br><br>

                <div class="row">
                    <div class="form-group">
                        <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            {!! Form::submit('Update Driver', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>


<script type="text/javascript">

        $("#services").addClass("active");

        checkSuccess = function(response) {

            switch (jQuery.parseJSON(response).statuscode) {
                case 200:
                    return true;
                case 400:
                    return false;
            }
            return true;
        };

        $("#addForm").validate({
            rules: {
                phone_number: {
                    remote: {
                            url: "{{route('driver_unique_ucheck')}}",
                            type: "POST",
                            cache: false,
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                user_id: "{{$driver->user_id}}",
                                phone_number: function() { return $("#phone_number").val(); }
                            },
                            dataFilter: function(response) {
                                return checkSuccess(response);
                            }
                        }

                },
                mulkiya_number: {
                    remote: {
                            url: "{{route('driver_unique_ucheck')}}",
                            type: "POST",
                            cache: false,
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                user_id: "{{$driver->user_id}}",
                                mulkiya_number: function() { return $("#mulkiya_number").val(); }
                            },
                            dataFilter: function(response) {
                                return checkSuccess(response);
                            }
                        }

                }

            },
            messages: {
                name: {
                    required: "Please provide the name"
                },
                profile_pic:{
                    accept: "Only Image is allowed"
                },
                email: {
                    email: "Please provide a valid email",
                    required: "Please provide the email",
                    remote: "Sorry, this email already taken"
                },
                phone_number: {
                    remote: "Sorry, this phone number already taken",
                    required: "Phone number is required"
                },
                mulkiya_number: {
                    remote: "Sorry this Mulkiya number is already taken",
                    required: "Mulkiya number is required"
                },
                mulkiya_validity: {
                    required: "Mulkiya Validity date is required"
                }

            }
        });

    $('#mulkiya_validity').datetimepicker({
        startDate: new Date(),
        format: 'yyyy-mm-dd hh:ii:ss'
    });

</script>


@stop



