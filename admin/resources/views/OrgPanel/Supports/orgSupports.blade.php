    @extends('OrgPanel.orgLayout')

    @section('title')
        Supports
    @stop

    @section('content')

    <link href="{{ URL::asset('AdminAssets/css/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('AdminAssets/js/plugins/daterangepicker/daterangepicker.js') }}"></script>

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        Supports
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('org_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                        <a href="{{route('org_supports_all')}}">
                            <b>
                            Supports
                            </b>                            
                        </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                        <div class="table-responsive">
            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th width="15%">Id</th>
                                    <th width="55%">Service Name</th>
                                    <th width="15%">Actions</th>
                                    <th width="15%">Drivers</th>
                                </tr>

                                </thead>
                                <tbody>


            @foreach($supports as $support)
                <tr class="gradeA footable-odd" style="display: table-row;">

                    <td>{{ $support->category_id }}</td>

                    <td>
                        <b>
                            {{ $support->category['category_details'][0]->name }}
                        </b>(English)<hr>

                        <b>
                            {{ $support->category['category_details'][1]->name }}
                        </b>(Hindi)<hr>

                        <b>
                            {{ $support->category['category_details'][2]->name }}
                        </b>(Urdu)<hr>

                        <b>
                            {{ $support->category['category_details'][3]->name }}
                        </b>(Chinese)<hr>

                        <b>
                            {{ $support->category['category_details'][4]->name }}
                        </b>(Arabic)<hr>
                    </td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-flat btn-info dropdown-toggle btn-sm" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Actions
                            <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('org_sup_drivers', ['category_id' => $support->category_id ]) }}">
        <i class="fa fa-child"> All Drivers</i> - {{ $support->driver_counts }}
    </a>
                                </li>

                                <li role="presentation">
    <a role="menuitem" tabindex="-1" href="{{ route('org_sup_adriver', ['category_id' => $support->category_id ]) }}">
        <i class="fa fa-plus"> Add Driver</i>
    </a>
                                </li>

                            </ul>
                        </div>
                    </td>

                    <td>
                        <a href="{{ route('org_sup_drivers', [ 'category_id' => $support->category_id ]) }}" class="btn btn-default">
                            {{ $support->driver_counts }}
                        </a>
                    </td>

                    </tr>
                @endforeach
                                </tbody>
                            </table>

                            </div>
                        </div>
                    </div>

</div>


        </div>
    </div>
</div>

    <script>

        $("#supports").addClass("active");

            var table = $('#example').dataTable( 
                {
                   "order": [[ 0, "desc" ]],
                   "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [ 2 ] },
                                { "bSearchable": true, "aTargets": [ 1 ] }
                    ]
                });

    </script>


@stop
