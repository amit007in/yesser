@extends('OrgPanel.orgLayout')

@section('title') 
     Dashboard
@stop

@section('content')

    <link href="{{ URL::asset('AdminAssets/css/mystyle.min.css') }}" rel="stylesheet">

    <script src="{{ URL::asset('AdminAssets/js/plugins/highchats/highcharts.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/fullcalendar/moment.min.js') }}"></script>

    <link href="{{ URL::asset('AdminAssets/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('AdminAssets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

    <div class="wrapper wrapper-content animated" data-animation="rotateInUpLeft">

        <div class="row">
            <div class="col-md-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-sm-offset-2  col-sm-8">
                                        
                <center>
                    <label>Filter Data</label>
                </center>
                <center>
                    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$starting_dt}} - {{$ending_dt}}"/>
                </center>
                <br>
            </div>                        
        </div>

        <div class="row">

            <div class="col-lg-3 col-md-6" onclick="javascript:location.href='{{route('org_services_all')}}'">
                <div class="small-box bg-light-blue">
                    <div class="inner">
                        <h3 id="service_counts" class="service_counts">&nbsp;</h3>
                            <p>Services</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-certificate"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

           <!-- <div class="col-lg-3 col-md-6" onclick="javascript:location.href='{{route('org_supports_all')}}'">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3 id="support_counts" class="support_counts">&nbsp;</h3>
                            <p>Support</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-support"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div> -->

            <div class="col-lg-3 col-md-6" onclick="javascript:location.href='{{route('org_orders')}}'">
                <div class="small-box bg-teal">
                    <div class="inner">
                        <h3 id="order_counts" class="order_counts">&nbsp;</h3>
                            <p>Orders</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-circle-thin"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6" onclick="javascript:location.href='{{route('org_services_dall_new')}}'">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3 id="dservice_counts" class="dservice_counts">&nbsp;</h3>
                            <p>Service Yesser Man</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user-circle"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <!--<div class="col-lg-3 col-md-6" onclick="javascript:location.href='{{route('org_supports_dall')}}'">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3 id="dsupport_counts" class="dsupport_counts">&nbsp;</h3>
                            <p>Support Drivers</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>-->

        <br>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="form-group" id="data_1">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" value="<?php echo date("Y"); ?>" id="starting_date" name="starting_date">
                                </div><br>
                                <center>
                                    <button class="button btn btn-primary btn-lg" onclick="new_get_dashboard_data()">
                                        Get Details
                                    </button>
                                </center>
                            </div>
                        <div id="container1" style="min-width: 100%; height: 100%; margin: 0 auto"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <script>

    $("#dashboard").addClass("active");

    var latitude, longitude;

    $(document).ready(function() 
        {

            navigator.geolocation.getCurrentPosition(function(position) {

                //toastr.success('Success.', 'Location access is allowed.');
                
                latitude = position.coords.latitude;
                longitude = position.coords.longitude;

                new_get_dashboard_data();

            }, function() {

                toastr.warning('Warning.', 'Location access is denied.');

                latitude = null;
                longitude = null;

                new_get_dashboard_data();

            });

            toastr.success('Welcome to {{env("APP_NAME")}} Dashboard', '{{$OrgDetails->organisation['username']}}');

                $("#starting_date").datepicker( {
                    format: "yyyy",
                    viewMode: "years", 
                    minViewMode: "years"
                        });

                setInterval(function()
                    {
                        new_get_dashboard_data();
                    }, 60000);

                $('input[name="daterange"]').daterangepicker({
                        format: 'DD/MM/YYYY',
                        minDate: '1/2/2018',
                        opens: 'center',
                        drops: 'down',
                        buttonClasses: ['btn', 'btn-sm'],
                        applyClass: 'btn-primary',
                        cancelClass: 'btn-default'
                            });

                $('#daterange').on('apply.daterangepicker', function(ev, picker) 
                    {
                        new_get_dashboard_data();
                    });

            });

        function new_get_dashboard_data()
            {

                dt = document.getElementById('daterange').value;
                yr = document.getElementById('starting_date').value;

                dt = encodeURIComponent(dt);
                route = '{!! route('org_dashboard_data') !!}?daterange='+dt+'&yr_only='+yr+'&latitude='+latitude+'&longitude='+longitude;
                console.log(route);
                $.ajax
                    ({
                        url: route,
                        type: 'GET',
                        async: true,
                        dataType: "json",
                        success: function (data) 
                            {
                                if(data.success == '0')
                                    toastr.error('Error.',data.message);
                                else                                    
                                    {
                                        assign_dashboard_data(data.stats);
                                        graph_data(data);
                                    }
                            }
                    });

            }

        function assign_dashboard_data(stats)
            {     

                document.getElementById('service_counts').innerHTML = stats.service_counts;
                document.getElementById('support_counts').innerHTML = stats.support_counts;
                document.getElementById('order_counts').innerHTML = stats.order_counts;
                document.getElementById('dservice_counts').innerHTML = stats.dservice_counts;
                document.getElementById('dsupport_counts').innerHTML = stats.dsupport_counts;

                return '1';
            }

      function graph_data(data)
        {
            document.getElementById('starting_date').value = data.graph_data.dt;

            $('#container1').highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Monthly Stats'
                },
                xAxis: {
                    categories: [
                        'January',
                        'February',
                        'March',
                        'April',
                        'May',
                        'June',
                        'July',
                        'August',
                        'September',
                        'October',
                        'November',
                        'December'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    allowDecimals: false,
                    title: {
                        text: 'COUNT'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [
                    {
                        name: 'Orders',
                        data: data.graph_data.orders,
                        color: "#39cccc"
                    },
                    {
                        name: 'Service Yesser Man',
                        data: data.graph_data.dservices,
                        color: "#B16B1E"
                    }, 
                    // {
                    //     name: 'Support Drivers',
                    //     data: data.graph_data.dsupports,
                    //     color: "#f56954"
                    // },

                ]
            });

        }

    </script>

@stop



