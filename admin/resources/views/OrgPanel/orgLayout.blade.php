<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="{{ URL::asset('AdminAssets/Images/favicon_yesser.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ URL::asset('AdminAssets/Images/favicon_yesser.ico') }}" type="image/x-icon">

    <title>{{env('APP_NAME')}} - @yield('title')</title>

    <script src="{{ URL::asset('AdminAssets/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/bootstrap.min.js') }}"></script>

    <link href="{{ URL::asset('AdminAssets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="{{ URL::asset('AdminAssets/css/animate.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('AdminAssets/css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('AdminAssets/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('AdminAssets/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('AdminAssets/css/plugins/blueimp/css/blueimp-gallery.min.css') }}" rel="stylesheet">

           <!-- Data Tables -->
    <link href="{{ URL::asset('AdminAssets/css/plugins/dataTables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('AdminAssets/css/plugins/dataTables/dataTables.responsive.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('AdminAssets/css/plugins/dataTables/dataTables.tableTools.min.css') }}" rel="stylesheet">

    <script src="{{ URL::asset('AdminAssets/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/dataTables/dataTables.responsive.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/dataTables/dataTables.tableTools.min.js') }}"></script>

    <link href="{{ URL::asset('AdminAssets/css/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">

    <style>

        body
            {
                font-size:12px !important;
            }
        .btn
            {
                font-size: 12px !important;
            }
        .lightBoxGallery
            {
                text-align: center;
            }
        .lightBoxGallery img
            {
                margin: 5px;
            }
        .img-circle
            {
                width: 50px !important;
                height: 50px !important;
            }
        .tableclass
            {
                    overflow-x: overlay;
            }

        th,tr
            {
                text-align: center !important;
            }

        td
            {
                padding-top:15px !important;
            }
        .low_padding
            {
                padding-top:5px !important;
            }
        input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button 
            {
                -webkit-appearance: none; 
                margin: 0;
            }

    </style>

</head>

<body class="pace-done" cz-shortcut-listen="true">

<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                
                <li class="nav-header">
                    <div class="dropdown profile-element">

                        <span>
                            <a href="{{ URL::asset('BuraqExpress/Uploads/'.$OrgDetails->organisation['image']) }}" title="{{ $OrgDetails->organisation['name'] }}" class="lightBoxGallery" data-gallery="">
                                <img alt="image" class="img-circle" style="max-width:100px !important;max-height:70px !important;" src=" {{ URL::asset('BuraqExpress/Uploads/'.$OrgDetails->organisation['image']) }} " />
                            </a>
                        </span>

                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                                    <strong class="font-bold">{{ $OrgDetails->organisation['username'] }}</strong>
                                </span>
                                <span class="text-muted text-xs block">
                                    {{ $OrgDetails->organisation['phone_number'] }}
                                    <b class="caret"></b>
                                </span>
                            </span>
                         </a>

                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li>
    <a href="{{route('org_profile')}}"><i class="fa fa-rouble"></i> Profile Update</a>
                            </li>
                            <li>
    <a href="{{route('org_password')}}"><i class="fa fa-pinterest"></i> Password Update</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a id="demo331" class="demo331" ><i class="fa fa-sign-out"></i> Logout</a>
                            </li>
                        </ul>

                    </div>
                    <div class="logo-element">
                        YS
                    </div>
                </li>

                <li id="dashboard" title="Dashboard">
                    <a href="{!! route('org_dashboard') !!}">
                        <i class="fa fa-th-large"></i>
                        <span class="nav-label">Dashboard</span>
                    </a>
                </li>

<!--                 <li id="profile" title="Profile">
                    <a href="{!! route('org_profile') !!}">
                        <i class="fa fa-rouble"></i>
                        <span class="nav-label">Profile</span>
                    </a>
                </li>
 -->
<!--                 <li id="track_drivers" title="Track Drivers">
                    <a href="{!! route('org_drivers_track') !!}">
                        <i class="fa fa-child"></i>
                        <span class="nav-label">Track Drivers</span>
                    </a>
                </li>
 -->

                <li id="drivers_all" title="Yesser Man" onclick="javascript:location.href='#' ">
                    <a href="#">
                        <i class="fa fa-user-secret"></i>
                        <span class="nav-label">Yesser Man</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="height: 0px;">
                        <li id="org_services_dall">
                            <a href="{{route('org_services_dall_new')}}" title="Services">
                                <i class="fa fa-certificate"></i> Services
                            </a>
                        </li>
                        <!--<li id="org_supports_dall">
                            <a href="{{route('org_supports_dall')}}" title="Supports">
                                <i class="fa fa-user-circle"></i> Supports
                            </a>
                        </li>-->
                        <li id="org_drivers_track">
                            <a href="{!! route('org_drivers_track') !!}">
                                <i class="fa fa-child"></i> Track Yesser Man
                            </a>
                        </li>
                    </ul>
                </li>

                <li id="payments_all" title="Payments" onclick="javascript:location.href='#' ">
                    <a href="#">
                        <i class="fa fa-paypal"></i>
                        <span class="nav-label">Payments</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="height: 0px;">
                        <li id="org_ser_pay_all">
                            <a href="{{route('org_ser_pay_all')}}" title="Service Payments">
                                <i class="fa fa-certificate"></i> Service Payments
                            </a>
                        </li>
                        <!--<li id="org_sup_pay_all">
                            <a href="{{route('org_sup_pay_all')}}" title="Support Payments">
                                <i class="fa fa-support"></i> Support Payments
                            </a>
                        </li>
                        <li id="org_etoken_pay_all">
                            <a href="{{route('org_etoken_pay_all')}}" title="eTokens Payments">
                                <i class="fa fa-eraser"></i> eTokens Payments
                            </a>
                        </li>-->
                    </ul>
                </li>

                <li id="services_all" title="services_all">
                    <a href="{!! route('org_services_all') !!}">
                        <i class="fa fa-certificate"></i>
                        <span class="nav-label">Services</span>
                    </a>
                </li>

                <!--<li id="supports" title="Supports">
                    <a href="{!! route('org_supports_all') !!}">
                        <i class="fa fa-support"></i>
                        <span class="nav-label">Supports</span>
                    </a>
                </li>-->

                <li id="reviews" title="Reviews">
                    <a href="{!! route('org_review') !!}">
                        <i class="fa fa-star"></i>
                        <span class="nav-label">Reviews</span>
                    </a>
                </li>

                <li id="orders" title="Orders">
                    <a href="{!! route('org_orders') !!}">
                        <i class="fa fa-circle-thin"></i>
                        <span class="nav-label">Orders</span>
                    </a>
                </li>

                <!--<li id="org_ledgers" title="Ledgers">
                    <a href="{!! route('org_ledgers') !!}">
                        <i class="fa fa-book"></i>
                        <span class="nav-label">Ledgers</span>
                    </a>
                </li>-->

            </ul>

        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">

        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0; z-index: 1;">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a id="demo331" class="demo331">
                            <i class="fa fa-sign-out"></i> Logout
                        </a>
                    </li>

                    <li>

                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="text-muted text-xs block">
                                    <b class="caret"></b>
                                </span>
                            </span>
                         </a>

                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li>
                                <a href="{{route('org_profile')}}"><i class="fa fa-rouble"></i> Profile Update</a>
                            </li>
                            <li>
                                <a href="{{route('org_password')}}"><i class="fa fa-pinterest"></i> Password Update</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a id="demo331" class="demo331" ><i class="fa fa-sign-out"></i> Logout</a>
                            </li>
                        </ul>

                    </li>

                </ul>

            </nav>
        </div>


        @foreach($errors->all() as $error)
            <div class="alert alert-dismissable alert-danger" style="margin-bottom: 0px !important;">
                {!! $error !!}
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            </div>
        @endforeach

        @if(session('status'))
            <div class="alert alert-success" style="margin-bottom: 0px !important;">
                {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            </div>
        @endif

       @yield('content')

        <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

        <footer class="footer">
            <span class="pull-right">
                <div id="google_translate_element"></div>
            </span>
            {{env('APP_NAME')}} &copy 2017
        </footer>


    </div>

    <script src="{{ URL::asset('AdminAssets/js/plugins/fullcalendar/moment.min.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/daterangepicker/daterangepicker.js') }}"></script>

    <script src="{{ URL::asset('AdminAssets/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/inspinia.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/pace/pace.min.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/toastr/toastr.min.js') }}"></script>

    <script src="{{ URL::asset('AdminAssets/js/plugins/blueimp/jquery.blueimp-gallery.min.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/idle-timer/idle-timer.min.js') }}"></script>

    <script>

        $(document).ready(function()
            {
            ///////////////////////////////////  Logout Function /////////////////////////////////////////////////
                $('.demo331').click(function ()
                    {

                        swal({
                            title: "Are you sure?",
                            text: "You want to logout!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes, Logout!",
                            closeOnConfirm: false
                        },
                        function(){
                            window.location.href = '{{route('org_logout')}}';
                        });

                    });
                        ///////////////////////////////////////         Toaster  //////////////////////////////////////////////////

                // $(document).idleTimer(300000);

                // $(document).bind("idle.idleTimer", function(){
                //     window.location.href = '';
                // });

                $(document).on( "active.idleTimer", function(event, elem, obj, triggerevent)
                    {

                        toastr.clear();
                        $('.success-alert').fadeOut();
                        toastr.success('Great.','You are back. ');

                    });

            });

    </script>

</div>

    <div id="blueimp-gallery" class="blueimp-gallery">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>

</body>

</html>
