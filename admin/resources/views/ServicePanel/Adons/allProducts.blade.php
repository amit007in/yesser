@extends('ServicePanel.serviceLayout')

@section('title')
    All Products
@stop

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        All Products
                    </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('service_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('service_products_all')}}">
                                <b>All Products</b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">

        <div class="row">
            <div class="col-md-6 col-lg-offset-3">
                <div class="ibox-content text-center">
                    <h2>{{ $category->category_details[0]->name }}</h2>
                        <div class="m-b-sm">

                            @if($brand->category_brand_details[0]->image != "")
    <a href="{{ $brand->category_brand_details[0]->image_url }}" title="{{ $brand->category_brand_details[0]->name }}" class="lightBoxGallery" data-gallery="">
        <img src="{{ $brand->category_brand_details[0]->image_url }}/120/120" class="img-circle " alt="profile" >
    </a>
                            @endif                        
                        </div>
                        <h3>{{ $brand->category_brand_details[$SDetails->language_id-1]->name }}</h3>
                </div>
            </div>
        </div>
    <br>

        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="table-responsive">

            <table class="footable table table-striped" id="example" data-page-size="10" data-filter="#filter">
                                <thead>

                                <tr>
                                    <th>Id</th>
                                    <th>Product Name</th>
                                    <th>Alpha Price</th>
                                    <th>Price Per Quantity</th>
                                    <th>Price Per Distance</th>
                                    <th>Price Per Weight</th>
                                    <th>Price Per Hour</th>
                                    <th>Price Per Sq Meters</th>
                                    <!-- <th>Update</th> -->
                                </tr>

                                </thead>
                                <tbody>

            @foreach($products as $product)
                <tr class="gradeA footable-odd" style="display: table-row;">

                    <td>{{ $product->user_driver_detail_product_id }}</td>

                    <td>
                        <b>
                            {{ $product['category_brand_product_details'][0]->name }}
                        </b>(English)<hr>

                        <b>
                            {{ $product['category_brand_product_details'][1]->name }}
                        </b>(Hindi)<hr>

                        <b>
                            {{ $product['category_brand_product_details'][2]->name }}
                        </b>(Urdu)<hr>

                        <b>
                            {{ $product['category_brand_product_details'][3]->name }}
                        </b>(Chinese)<hr>

                        <b>
                            {{ $product['category_brand_product_details'][4]->name }}
                        </b>(Arabic)<hr>
                    </td>

                    <td>
                       <b>OMR</b> {{$product->alpha_price}}
                    </td>

                    <td>
                        <b>OMR</b> {{$product->price_per_quantity}}
                    </td>

                    <td>
                        <b>OMR</b> {{$product->price_per_distance}}
                    </td>

                    <td>
                        <b>OMR</b> {{$product->price_per_weight }}
                    </td>

                    <td>
                        <b>OMR</b> {{$product->price_per_hr}}
                    </td>

                    <td>
                        <b>OMR</b> {{$product->price_per_sq_mt}}
                    </td>

<!--                     <td>
 <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#modal-form2" onclick="update_values('{{$product->user_driver_detail_product_id}}', '{{$product->alpha_price}}', '{{$product->price_per_quantity}}', '{{$product->price_per_distance}}', '{{$product->price_per_weight}}', '{{$product->price_per_hr}}', '{{$product->price_per_sq_mt}}' )" id="update_price">
        <i class="fa fa-edit" aria-hidden="true"> Update</i>
    </a>
                    </td>
 -->
                    </tr>
                @endforeach
                                </tbody>
                            </table>
                </div>
            </div>
        </div>

    </div>

        </div>
    </div>
</div>

    <!--                Reply Modal                                -->
    <div class="modal inmodal modal-form2 modal-bg" id="modal-form2" role="dialog" aria-hidden="false">
        <div class="modal-dialog">
            <form method="post" action="{{route('service_panel_product_update')}}" enctype="multipart/form-data" >
                <div class="modal-content animated swing">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <i class="fa fa-edit modal-icon"></i>
                        <h4 class="modal-title">Update</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                                <label>Alpha Price</label>
                                <input type="number" name="alpha_price" id="alpha_price" class="form-control" step="any" min="0" autofocus="on" required placeholder="Alpha Price">
                            </div>

                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                               <label>Price Per Quantity</label>
                                <input type="number" name="price_per_quantity" id="price_per_quantity" class="form-control" step="any" min="0" required placeholder="Price Per Quantity">
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                                <label>Price Per Distance</label>
                                <input type="number" name="price_per_distance" id="price_per_distance" class="form-control" step="any" min="0" required placeholder="Price Per Distance">
                            </div>

                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                               <label>Price Per Weight</label>
                                <input type="number" name="price_per_weight" id="price_per_weight" class="form-control" step="any" min="0" required placeholder="Price Per Weight">
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                                <label>Price Per Hour</label>
                                <input type="number" name="price_per_hr" id="price_per_hr" class="form-control" step="any" min="0" required placeholder="Price Per Hour">
                            </div>

                            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
                               <label>Price Per Sq Meter</label>
                                <input type="number" name="price_per_sq_mt" id="price_per_sq_mt" class="form-control" step="any" min="0" required placeholder="Price Per Sq Meter">
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="modal-footer">

                        <input hidden="on" value="" name="user_driver_detail_product_id" id="user_driver_detail_product_id"></input>

                        <button type="submit" class="btn btn-primary" id="add_rform_btn">Update</button>
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--                Reply Modal                                -->



    <script>

        $("#service_products_all").addClass("active");

        // $(document).ready(function()
        //     {

        //     });

            var table = $('#example').dataTable( 
                {
                   "order": [[ 0, "desc" ]],
                   "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 5 ] },
                        { "bSearchable": true, "aTargets": [ 3, 5, 6 ] }
                    ],
                   "fnDrawCallback": function( oSettings ) 
                        {


                        }
                });

        function update_values(user_driver_detail_product_id, alpha_price, price_per_quantity, price_per_distance, price_per_weight, price_per_hr, price_per_sq_mt)
            {

                document.getElementById('user_driver_detail_product_id').value = user_driver_detail_product_id;

                document.getElementById('alpha_price').value = alpha_price;
                document.getElementById('price_per_quantity').value = price_per_quantity;
                document.getElementById('price_per_distance').value = price_per_distance;
                document.getElementById('price_per_weight').value = price_per_weight;
                document.getElementById('price_per_hr').value = price_per_hr;
                document.getElementById('price_per_sq_mt').value = price_per_sq_mt;
                
            }

    </script>


@stop
