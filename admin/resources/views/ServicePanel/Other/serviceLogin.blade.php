<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{env('APP_NAME')}} - Service Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="{{ URL::asset('AdminAssets/Images/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ URL::asset('AdminAssets/Images/favicon.ico') }}" type="image/x-icon">

<!--===============================================================================================-->	

    <script src="{{ URL::asset('AdminAssets/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/bootstrap.min.js') }}"></script>
    <link href="{{ URL::asset('AdminAssets/css/bootstrap.min.css') }}" rel="stylesheet">

<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('AdminAssets/Login/fonts/iconic/css/material-design-iconic-font.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('AdminAssets/Login/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('AdminAssets/Login/css/main.css') }}">
<!--===============================================================================================-->

	<style type="text/css">

        input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button 
            {
                -webkit-appearance: none; 
                margin: 0;
            }

	</style>

</head>
<body>	
	
	<div class="container-login100" style="background-image: url('{{ URL::asset('AdminAssets/Login/images/bg-01.jpg') }}');">

		<div class="wrap-login100 p-l-35 p-r-35 p-t-50 p-b-20" style="padding-top: 25px !important;">
			<form class="login100-form validate-form" method="post" action="{{route('service_panel_plogin')}}">

				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="timezone" value="" id="timezone">

				<span class="login100-form-title p-b-37">
					<center>
						<img src="{{ URL::asset('AdminAssets/Images/colored.png') }}" style="max-height: 120px; max-height: 120px; ">
					</center>

					<h2>Service Login</h2>
				<!-- Admin Sign In -->
			</span>
				<br>
				<div class="wrap-input100 validate-input m-b-20" data-validate="Enter Phone number">
					<input class="input100" type="number" name="phone_number" required autocomplete placeholder="Phone number" value="{{Request::old('phone_number')}}" autofocus="on" min="99999" minlength="6">
					<span class="focus-input100"></span>
				</div>

			        @foreach($errors->all() as $error)
			            <div class="alert alert-dismissable alert-danger">
			                {!! $error !!}
			            </div>
			        @endforeach

			        @if(session('status'))
			            <div class="alert alert-success">
			                {{ session('status') }}
			            </div>
			        @endif

				<div class="container-login100-form-btn">
					<button class="login100-form-btn">
						SEND OTP
					</button>
				</div>
				<br>

			</form>
			
		</div>
	</div>	

<!--===============================================================================================-->
	<script src="{{ URL::asset('AdminAssets/Login/js/main.js') }}"></script>

</body>
</html>