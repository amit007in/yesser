@extends('SupportPanel.supportLayout')

@section('title') 
    Order Details
@stop

@section('content')

<style>
  html, body, #map-canvas {
    height: 400px;
    margin: 0px;
    padding: 0px
  }
</style>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="row">

                <div class="col-lg-10">
                    <h2>
                        Order Details
                    </h2>
                    
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('support_order_all')}}">
                                Orders All
                            </a>
                        </li>
                        <li>
                            <a href="{{route('support_order_all')}}">
                                <b>
                                    Order Details
                                </b>                            
                            </a>
                        </li>
                    </ol>
                </div>
                
                <div class="col-lg-2">

                </div>
            
            </div>
        </div>

        <div class="row">
<!--            Map Canvas                            -->
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div id="map-canvas"></div>
            </div>
<!--            Map Canvas                            -->
        </div>

        <div class="row">

<!--            Order Details                         -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Order Info</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Status</th>
                            <th>Timing</th>
                            <th>Pickup Location</th>
                            <th>Dropoff Location</th>
                            <th>Booked At</th>
                        </tr>
                    </thead>
                    <tbody>
                <tr>

                    <td>{{ $order->order_id }}</td>

                    <td>
                        @include('Common.Partials.orderStatus')
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order->order_timingsz }} </td>

                    <td>
                        {{ $order->pickup_address }}
                    </td>

                    <td>
                        {{ $order->dropoff_address }}
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order->created_atz }} </td>

                </tr>

                    </tbody>
                </table>

        </div>

            </div>

        </div>

    </div>
<!--            Order Details                         -->

<!--            Product Details                      -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Product Info</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Product</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                <tr>

                    <td>{{ $order['category_brand_product']->category_brand_product_id }}</td>

                    <td>{{ $order['category']->name }}</td>

                    <td>{{ $order['category_brand']->name }}</td>

                    <td>{{ $order['category_brand_product']->name }}</td>

                    <td>
                        {{ $order->product_quantity }}
                    </td>

                </tr>

                    </tbody>
                </table>

        </div>

            </div>

        </div>

    </div>
<!--            Product Details                      -->

<!--            Payment Details                         -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Payment Info</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Status</th>
                            <th>Method</th>
                            <th>Admin Charge</th>
                            <th>Charge</th>
                            <th>Charge</th>
                        </tr>
                    </thead>
                    <tbody>
                <tr>

                    <td>
                        {{ $order->transaction_id }}
                    </td>

                    <td>
                        <span class="label label-success">
                            {{ $order->payment_status }}
                        </span>
                    </td>


                    <td>
                        @if($order->payment_type  == "Cash")
                            <span class="label label-success">
                                Cash
                            </span>
                        @elseif($order->payment_type  == "Card")
                            <span class="label label-primary">
                                Card
                            </span>
                        @else
                            <span class="label label-info">
                                EToken
                            </span>
                        @endif
                    </td>

                    <td>
    <a href="{{$order['customer_user_detail']->profile_pic_url}}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
        <img src="{{$order['customer_user_detail']->profile_pic_url}}"  class="img-circle">
    </a>                        
                    </td>

                    <td>
                        {{ $order['customer_user']->phone_number }}
                    </td>

                </tr>

                    </tbody>
                </table>

        </div>

            </div>

        </div>

    </div>
<!--            Payment Details                         -->

<!--            User Details                         -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>User Info</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">


                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>ProfilePic</th>
                            <th>Phone No</th>
                        </tr>
                    </thead>
                    <tbody>
                <tr>

                    <td>
                        {{ $order['customer_user_detail']->user_detail_id }}
                    </td>

                     <td>
                        {{ $order['customer_user']->name }}
                    </td>

                    <td>
    <a href="{{$order['customer_user_detail']->profile_pic_url}}" title="{{ $order['customer_user']->name }}" class="lightBoxGallery img-circle" data-gallery="">
        <img src="{{$order['customer_user_detail']->profile_pic_url}}"  class="img-circle">
    </a>                        
                    </td>

                    <td>
                        {{ $order['customer_user']->phone_number }}
                    </td>

                </tr>

                    </tbody>
                </table>

        </div>

            </div>

        </div>

    </div>
<!--            User Details                         -->


<!--            Ratings                              -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Ratings Given By</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>                    
                </div>
        </div>
        <div class="ibox-content">
            <div>

        <div class="table-responsive">

            <div class="col-sm-6 col-md-6 col-lg-6">
                <center><h4>Customer</h4></center>
                <table class="table table-hover">
                    <thead>
                        <tr>
<!--                             <th>Id</th> -->
                            <th width="5%">Ratings</th>
                            <th width="80%">Comments</th>
                            <th width="15%">Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($order['customer_rating'])

                <tr>

<!--                     <td>
                        {{ $order['customer_rating']->order_rating_id }}
                    </td>
 -->
                     <td>
                        <i class="fa fa-star-half-empty"></i> {{ $order['customer_rating']->ratings }}
                    </td>

                    <td>
                        {{ $order['customer_rating']->comments }}
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order['customer_rating']->created_atz }} </td>

                </tr>


                        @endif
                    </tbody>
                </table>                
            </div>


            <div class="col-sm-6 col-md-6 col-lg-6">
                <center><h4>Driver</h4></center>
                <table class="table table-hover">
                    <thead>
                        <tr>
<!--                             <th>Id</th> -->
                            <th width="5%">Ratings</th>
                            <th width="80%">Comments</th>
                            <th width="15%">Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($order['driver_rating'])

                <tr>

<!--                     <td>
                        {{ $order['driver_rating']->order_rating_id }}
                    </td>
 -->
                     <td>
                        <i class="fa fa-star-half-empty"></i> {{ $order['driver_rating']->ratings }}
                    </td>

                    <td>
                        {{ $order['driver_rating']->comments }}
                    </td>

                    <td><i class="fa fa-clock-o"></i> {{ $order['driver_rating']->created_atz }} </td>

                </tr>


                        @endif
                    </tbody>
                </table>                
            </div>



        </div>

            </div>

        </div>

    </div>
<!--            Ratings                              -->

            
        </div>

<br><br><br>

<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key={{env('GOOGLE_MAP_KEY')}}"></script> -->

<script type="text/javascript">
         
    $("#support_orders").addClass("active");


</script>

@stop