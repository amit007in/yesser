@extends('SupportPanel.supportLayout')

@section('title') 
    Update Details
@stop

    @section('content')

    <link href="{{ URL::asset('AdminAssets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('AdminAssets/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Update Details</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('support_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('support_profile')}}">
                                <b>
                                    Update Details
                                </b>
                            </a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

        <br>
            <form method="post" action="{{route('support_pprofile', ['user_detail_id' =>$driver->user_detail_id, 'user_id' => $driver->user_id ]) }}" enctype="multipart/form-data" id="addForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Name</label>
                <input type="text" placeholder="Name" autocomplete="off" class="form-control" required name="name" value="{!! $driver->name !!}" autofocus="on" id="name" maxlength="255">
            </div> 


            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <center>
                @if($driver->profile_pic != '')
                    <a href="{!! $driver->profile_pic_image !!}" title="{!! $driver->name !!}" class="lightBoxGallery pull-right" data-gallery="">
                        <img src="{!! $driver->profile_pic_url !!}"  class="img-circle">
                    </a>
                @endif
                </center>
            </div>


            <div class='col-lg-4 col-md-4 col-sm-4'>
                <label>Profile Pic (150px X 150px)</label>
                <input type="file" name="profile_pic" class="form-control" accept="image/*" id="profile_pic">
            </div>

        </div>
    </div>

<br><br>

    <div class="row">
        <div class="form-group">
           
            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Mulkiya Number</label>
                <input type="text" placeholder="Mulkiya Number" autocomplete="off" class="form-control" required name="mulkiya_number" value="{!! $driver->mulkiya_number !!}" id="mulkiya_number" maxlength="255">
            </div> 

            <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
            <label>Mulkiya Validity date</label>
    <div class="input-group date">
    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="mulkiya_validity" id="mulkiya_validity" value="{{ $driver->mulkiya_validity }}" placeholder="Mulkiya Validity date" required>
    </div>
            </div> 


        </div>
    </div>

<br><br>

    <div class="row">
        <div class="form-group">

            @if($driver->mulkiya_front != '')

                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                    <center>
                        <a href="{!! $driver->mulkiya_front_url !!}" title="{!! $driver->name !!}" class="lightBoxGallery" data-gallery="">
                            <img src="{!! $driver->mulkiya_front_url !!}"  class="img-circle">
                        </a>
                    </center>
                </div>

                <div class='col-lg-4 col-md-4 col-sm-4'>
                    <label>Mulkiya Front</label>
                    <input type="file" name="mulkiya_front" class="form-control" accept="image/*" id="mulkiya_front">
                </div>

            @else

                <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                    <label>Mulkiya Front</label>
                    <input type="file" name="mulkiya_front" class="form-control" accept="image/*" id="mulkiya_front">
                </div>

            @endif


            @if($driver->mulkiya_back != '')

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                    <center>
                        <a href="{!! $driver->mulkiya_back_url !!}" title="{!! $driver->name !!}" class="lightBoxGallery pull-right" data-gallery="">
                            <img src="{!! $driver->mulkiya_back_url !!}"  class="img-circle">
                        </a>
                    </center>
                </div>

                <div class='col-lg-4 col-md-4 col-sm-4'>
                    <label>Mulkiya Back</label>
                    <input type="file" name="mulkiya_back" class="form-control" accept="image/*" id="mulkiya_back">
                </div>

            @else

                <div class='col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2'>
                    <label>Mulkiya Back</label>
                    <input type="file" name="mulkiya_back" class="form-control" accept="image/*" id="mulkiya_back">
                </div>

            @endif

        </div>
    </div>

<br>
    <div class="row">
        <div class="form-group">

            <div class='col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1'>
                <label>Products</label>
                <select name="products[]" id="products" class="form-control border-info" multiple="true" required>
                    @foreach($products as $product)
                        @if($product->selected == "1")
                            <option value="{{$product->category_brand_product_id}}" selected>{{$product->name}}</option>
                        @else
                            <option value="{{$product->category_brand_product_id}}" >{{$product->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div> 

        </div>
    </div>

            <br><br><br>

                <div class="row">
                    <div class="form-group">
                        <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            {!! Form::submit('Update Details', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/select2/select2.full.min.js') }}"></script>


<script type="text/javascript">

        $("#support_dashboard").addClass("active");

        checkSuccess = function(response) {

            switch (jQuery.parseJSON(response).statuscode) {
                case 200:
                    return true;
                case 400:
                    return false;
            }
            return true;
        };

        $("#addForm").validate({
            rules: {
                password: {
                  required: true,
                  minlength: 6,
                  notEqual: "#old_password"
                },
                password_confirmation: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                },
                mulkiya_number: {
                    remote: {
                            url: "{{route('support_profile_uunique')}}",
                            type: "POST",
                            cache: false,
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                user_id: "{{$driver->user_id}}",
                                mulkiya_number: function() { return $("#mulkiya_number").val(); }
                            },
                            dataFilter: function(response) {
                                return checkSuccess(response);
                            }
                        }

                }

            },
            messages: {
                name: {
                    required: "Please provide the name"
                },
                profile_pic:{
                    accept: "Only Image is allowed"
                },
                mulkiya_number: {
                    remote: "Sorry this mulkiya number is already taken",
                    required: "Mulkiya number is required"
                },
                mulkiya_validity: {
                    required: "Mulkiya Validity date is required"
                },
                products: {
                    required: "Please select atleast one product"
                }

            }
        });

    $('#mulkiya_validity').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,
            startDate: new Date(),
            viewMode: 'years',
            format: 'yyyy-mm-dd'
        });

    $("#products").select2({
          dropdownCssClass: "bigdrop",
          placeholder: "Select Products"
      });

</script>


@stop



