@extends('SupportPanel.supportLayout')

@section('title') 
     Dashboard
@stop

@section('content')

    <link href="{{ URL::asset('AdminAssets/css/mystyle.min.css') }}" rel="stylesheet">

    <script src="{{ URL::asset('AdminAssets/js/plugins/highchats/highcharts.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/fullcalendar/moment.min.js') }}"></script>

    <link href="{{ URL::asset('AdminAssets/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('AdminAssets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

    <div class="wrapper wrapper-content animated" data-animation="rotateInUpLeft">

        <div class="row">
            <div class="col-md-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-sm-offset-2  col-sm-8">
                                        
                <center>
                    <label>Filter Data</label>
                </center>
                <center>
                    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$starting_dt}} - {{$ending_dt}}"/>
                </center>
                <br>
            </div>                        
        </div>

        <div class="row">

            <div class="col-lg-4 col-xs-6" onclick="javascript:location.href='#'">
                <div class="small-box bg-light-blue">
                    <div class="inner">
                        <h3 id="user_counts" class="user_counts">&nbsp;</h3>
                            <p>Orders</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        
        <br>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="form-group" id="data_1">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" value="<?php echo date("Y"); ?>" id="starting_date" name="starting_date">
                                </div><br>
                                <center>
                                    <button class="button btn btn-primary btn-lg" onclick="new_get_dashboard_data()">
                                        Get Details
                                    </button>
                                </center>
                            </div>
                        <div id="container1" style="min-width: 100%; height: 100%; margin: 0 auto"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <script>

    $("#dashboard").addClass("active");

    $(document).ready(function() 
            {

                toastr.success('Welcome to {{env("APP_NAME")}} Dashboard', '{{ $SDetails->user['name'] }}');

                $("#starting_date").datepicker( {
                    format: "yyyy",
                    viewMode: "years", 
                    minViewMode: "years"
                        });

                new_get_dashboard_data();

                // setInterval(function()
                //     {
                //         new_get_dashboard_data();
                //     }, 60000);

                $('input[name="daterange"]').daterangepicker();

                $('#daterange').on('apply.daterangepicker', function(ev, picker) 
                    {
                        new_get_dashboard_data();
                    });

            });

        function new_get_dashboard_data()
            {

                dt = document.getElementById('daterange').value;
                yr = document.getElementById('starting_date').value;

                dt = encodeURIComponent(dt);
                route = '{!! route('support_dash_data') !!}?daterange='+dt+'&yr_only='+yr;
                //console.log(route);
                // $.ajax
                //     ({
                //         url: route,
                //         type: 'GET',
                //         async: true,
                //         dataType: "json",
                //         success: function (data) 
                //             {
                //                 if(data.success == '0')
                //                     toastr.error('Error.',data.message);
                //                 else                                    
                //                     {
                //                         assign_dashboard_data(data.stats);
                //                         graph_data(data);
                //                     }
                //             }
                //     });

            }

        function assign_dashboard_data(stats)
            {     

                document.getElementById('user_counts').innerHTML = stats.user_counts;

                return '1';
            }

      function graph_data(data)
        {
            document.getElementById('starting_date').value = data.graph_data.dt;

            $('#container1').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Monthly Average Stats'
                },
                xAxis: {
                    categories: [
                        'January',
                        'February',
                        'March',
                        'April',
                        'May',
                        'June',
                        'July',
                        'August',
                        'September',
                        'October',
                        'November',
                        'December'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    allowDecimals: false,
                    title: {
                        text: 'COUNT'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [
                {
                    name: 'Users',
                    data: data.graph_data.users,
                    color: "#3c8dbc"
                }

                ]
            });

        }

    </script>

@stop



