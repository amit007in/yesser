<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group([ 'namespace' => 'Admin'] , function()
{

	Route::get('test', [ 'as' => 'admin_test', 'uses' => 'Admin\AdminCont@admin_test' ]);
	
	Route::get('/', [ 'as' => 'admin_login_get', 'uses' => 'Admin\AdminCont@admin_login_get' ]);
	Route::get('login', [ 'as' => 'admin_login_get', 'uses' => 'Admin\AdminCont@admin_login_get' ]);
	Route::post('login', [ 'as' => 'admin_login_post', 'uses' => 'Admin\AdminCont@admin_login_post' ]);
        
Route::group([ 'middleware' => 'admin_middleware' ] , function()
	{/////////////////////////		Logged In Admin 		////////////////////////////

	Route::get('logout', [ 'as' => 'admin_logout', 'uses' => 'Admin\AdminCont@admin_logout' ]);

	Route::get('dashboard', [ 'as' => 'admin_dashboard', 'uses' => 'Admin\AdminCont@admin_dashboard' ]);
	Route::get('dash_data', [ 'as' => 'admin_dash_data', 'uses' => 'Admin\AdminCont@admin_dash_data' ]);

	Route::get('profile/update', [ 'as' => 'aprofile_update_get', 'uses' => 'Admin\AdminCont@aprofile_update_get' ]);
	Route::post('profile/update', [ 'as' => 'aprofile_update_post', 'uses' => 'Admin\AdminCont@aprofile_update_post' ]);

	Route::get('password/update', [ 'as' => 'apassword_update_get', 'uses' => 'Admin\AdminCont@apassword_update_get' ]);
	Route::post('password/update', [ 'as' => 'apassword_update_post', 'uses' => 'Admin\AdminCont@apassword_update_post' ]);
	
	Route::get('roundrobin/update', [ 'as' => 'aroundrobin_update_get', 'uses' => 'Admin\AdminCont@aroundrobin_update_get' ]);// Add 3.2
	Route::post('roundrobin/update', [ 'as' => 'aroundrobin_update_post', 'uses' => 'Admin\AdminCont@aroundrobin_update_post' ]);// Add 3.2
	
	Route::get('setting/update', [ 'as' => 'setting_update_get', 'uses' => 'Admin\AdminCont@setting_update_get' ]);
	Route::post('setting/update', [ 'as' => 'setting_update_post', 'uses' => 'Admin\AdminCont@setting_update_post' ]);

	Route::get('delete', [ 'as' => 'admin_delete', 'uses' => 'Admin\AdminCont@admin_delete' ]);

	Route::group([ 'prefix' => 'language', 'namespace' => 'Admin'] , function()
		{
			
		Route::get('all', [ 'as' => 'admin_language_all', 'uses' => 'AdminOtherCont@admin_language_all' ]);

		Route::get('default', [ 'as' => 'default_lang_change', 'uses' => 'AdminOtherCont@default_lang_change' ]);

		});

	Route::group([ 'prefix' => 'emergency', 'namespace' => 'Admin'] , function()
		{
			
		Route::get('all', [ 'as' => 'admin_econtacts_all', 'uses' => 'AdminOtherCont@admin_econtacts_all' ]);

		Route::get('block', [ 'as' => 'admin_econtact_block', 'uses' => 'AdminOtherCont@admin_econtact_block' ]);

		Route::get('add', [ 'as' => 'admin_econtact_aget', 'uses' => 'AdminOtherCont@admin_econtact_aget' ]);
		Route::post('add', [ 'as' => 'admin_econtact_apost', 'uses' => 'AdminOtherCont@admin_econtact_apost' ]);

		Route::get('update', [ 'as' => 'admin_econtact_uget', 'uses' => 'AdminOtherCont@admin_econtact_uget' ]);
		Route::post('update', [ 'as' => 'admin_econtact_upost', 'uses' => 'AdminOtherCont@admin_econtact_upost' ]);
			
		});
                
            Route::group([ 'prefix' => 'notifications', 'namespace' => 'Admin'] , function()
            {
                Route::any('all', [ 'as' => 'admin_notification_all', 'uses' => 'AdminOtherCont@admin_notification' ]);

                //Route::get('all', [ 'as' => 'admin_notification_all', 'uses' => 'AdminOtherCont@admin_notification_all' ]);

                Route::get('add', [ 'as' => 'admin_notification_aget', 'uses' => 'AdminOtherCont@admin_notification_aget' ]);
                Route::post('add', [ 'as' => 'admin_notification_apost', 'uses' => 'AdminOtherCont@admin_notification_apost' ]);

                Route::get('update', [ 'as' => 'admin_notification_uget', 'uses' => 'AdminOtherCont@admin_notification_uget' ]);
                Route::post('update', [ 'as' => 'admin_notification_upost', 'uses' => 'AdminOtherCont@admin_notification_upost' ]);

                Route::get('view', [ 'as' => 'admin_notification_vget', 'uses' => 'AdminOtherCont@admin_notification_vget' ]);

                Route::get('block', [ 'as' => 'admin_notification_active', 'uses' => 'AdminOtherCont@admin_notification_active' ]);

                Route::get('delete', [ 'as' => 'admin_notification_delete', 'uses' => 'AdminOtherCont@admin_notification_delete' ]);

            });

	///////////////		Login History 		///////////////////////////
	Route::group(['prefix' => 'login', 'namespace' => 'Admin' ] , function()
		{
			Route::get('history', [ 'as' => 'admin_login_history', 'uses' => 'AdminOtherCont@admin_login_history' ]);
		});
	///////////////		Login History 		///////////////////////////

	////////////		Coupons 	/////////////////////////////
	Route::group([ 'prefix' => 'coupon', 'namespace' => 'Admin'] , function()
		{
		Route::get('testnotfication', [ 'as' => 'admin_testnotfication_all', 'uses' => 'AdminOtherCont@admin_testnotfication_all' ]);	
		Route::get('all', [ 'as' => 'admin_coupons_all', 'uses' => 'AdminOtherCont@admin_coupons_all' ]);

		Route::get('add', [ 'as' => 'admin_coupons_aget', 'uses' => 'AdminOtherCont@admin_coupons_aget' ]);
		Route::post('add', [ 'as' => 'admin_coupons_apost', 'uses' => 'AdminOtherCont@admin_coupons_apost' ]);

		Route::get('update', [ 'as' => 'admin_coupons_uget', 'uses' => 'AdminOtherCont@admin_coupons_uget' ]);
		Route::post('update', [ 'as' => 'admin_coupons_upost', 'uses' => 'AdminOtherCont@admin_coupons_upost' ]);

		Route::get('block', [ 'as' => 'admin_coupon_block', 'uses' => 'AdminOtherCont@admin_coupon_block' ]);

		});
	////////////		Promocode 	/////////////////////////////

        Route::group([ 'prefix' => 'promocode', 'namespace' => 'Admin'] , function()
        {
            
            Route::get('all', [ 'as' => 'admin_promocode_all', 'uses' => 'AdminOtherCont@admin_promocode_all' ]);

            Route::get('add', [ 'as' => 'admin_promocode_aget', 'uses' => 'AdminOtherCont@admin_promocode_aget' ]);
            Route::post('add', [ 'as' => 'admin_promocode_apost', 'uses' => 'AdminOtherCont@admin_promocode_apost' ]);

            Route::get('update', [ 'as' => 'admin_promocode_uget', 'uses' => 'AdminOtherCont@admin_promocode_uget' ]);
            Route::post('update', [ 'as' => 'admin_promocode_upost', 'uses' => 'AdminOtherCont@admin_promocode_upost' ]);

            Route::get('block', [ 'as' => 'admin_promocode_block', 'uses' => 'AdminOtherCont@admin_promocode_block' ]);

        });
                
                
	Route::group([ 'prefix' => 'service', 'namespace' => 'Service'] , function(){///////////////		Services 		///////////////////////////

	Route::get('cat/all', [ 'as' => 'admin_service_cat_all', 'uses' => 'AdminServiceCont@admin_service_cat_all' ]);

	Route::post('update/distance', [ 'as' => 'admin_ser_dist_pupdate', 'uses' => 'AdminServiceCont@admin_ser_dist_pupdate' ]);

	Route::get('orders', [ 'as' => 'admin_ser_orders', 'uses' => 'AdminServiceCont@admin_ser_orders' ]);

	Route::get('products', [ 'as' => 'admin_ser_prods_all', 'uses' => 'AdminServiceCont@admin_ser_prods_all' ]);
	Route::post('product/update', [ 'as' => 'admin_ser_prod_update', 'uses' => 'AdminServiceCont@admin_ser_prod_update' ]);

	Route::get('product/new/add', [ 'as' => 'admin_product_new_add', 'uses' => 'AdminServiceCont@admin_product_new_add']);
	Route::get('product/new/update', [ 'as' => 'admin_product_new_update', 'uses' => 'AdminServiceCont@admin_product_new_update']);
	Route::get('enable/order', [ 'as' => 'admin_enable_ser_orders', 'uses' => 'AdminServiceCont@admin_enable_ser_orders']); //Add M2 Part 2
	Route::get('disable/order', [ 'as' => 'admin_disable_ser_orders', 'uses' => 'AdminServiceCont@admin_disable_ser_orders']); //Add M2 Part 2

	Route::group([ 'prefix' => 'cat/brand' ] , function(){

		Route::get('block', [ 'as' => 'admin_brand_block', 'uses' => 'AdminServiceCont@admin_brand_block' ]);

		Route::get('all', [ 'as' => 'admin_service_cat_brands', 'uses' => 'AdminServiceCont@admin_service_cat_brands' ]);

		Route::get('add', [ 'as' => 'admin_service_cat_add', 'uses' => 'AdminServiceCont@admin_service_cat_add' ]);
		Route::post('add', [ 'as' => 'admin_service_cat_add_post', 'uses' => 'AdminServiceCont@admin_service_cat_add_post' ]);

		Route::get('update', [ 'as' => 'admin_service_brand_update', 'uses' => 'AdminServiceCont@admin_service_brand_update' ]);
		Route::post('update', [ 'as' => 'admin_service_brand_update_post', 'uses' => 'AdminServiceCont@admin_service_brand_update_post' ]);

		Route::group([ 'prefix' => 'product'] , function(){

			Route::get('all', [ 'as' => 'admin_brand_products_all', 'uses' => 'AdminServiceCont@admin_brand_products_all' ]);

			Route::get('block', [ 'as' => 'admin_products_block', 'uses' => 'AdminServiceCont@admin_products_block' ]);

			Route::get('add', [ 'as' => 'admin_product_add', 'uses' => 'AdminServiceCont@admin_product_add' ]);
			Route::post('add', [ 'as' => 'admin_product_add_post', 'uses' => 'AdminServiceCont@admin_product_add_post' ]);

			Route::get('update', [ 'as' => 'admin_product_update', 'uses' => 'AdminServiceCont@admin_product_update' ]);
			Route::post('update', [ 'as' => 'admin_product_update_post', 'uses' => 'AdminServiceCont@admin_product_update_post' ]);
			
			Route::get('enable', [ 'as' => 'admin_products_geo_fencing_block', 'uses' => 'AdminServiceCont@admin_products_geo_fencing_block' ]); //M3

			});

		});

		Route::get('cat/driver/all', [ 'as' => 'admin_single_ser_dall', 'uses' => 'AdminSerDriverCont@admin_single_ser_dall' ]);
		Route::any('cat/driver/all/new', [ 'as' => 'admin_single_ser_dall_new', 'uses' => 'AdminSerDriverCont@admin_single_ser_dall_new' ]);

		Route::get('cat/driver/add', [ 'as' => 'admin_single_ser_dadd', 'uses' => 'AdminSerDriverCont@admin_single_ser_dadd' ]);
		Route::post('cat/driver/add', [ 'as' => 'admin_single_ser_pdadd', 'uses' => 'AdminSerDriverCont@admin_single_ser_pdadd' ]);

		Route::group([ 'prefix' => 'driver' ] , function(){

			Route::get('all', [ 'as' => 'admin_ser_dall', 'uses' => 'AdminSerDriverCont@admin_ser_dall' ]);
			Route::any('new/all', [ 'as' => 'admin_ser_dall_new', 'uses' => 'AdminSerDriverCont@admin_ser_dall_new' ]);

			Route::get('track', [ 'as' => 'admin_driver_track', 'uses' => 'AdminSerDriverCont@admin_driver_track' ]);

			Route::get('login/history', [ 'as' => 'admin_ser_dlogin_history', 'uses' => 'AdminSerDriverCont@admin_ser_dlogin_history' ]);

				//addSerDriver
			Route::post('update/percentage', [ 'as' => 'admin_driver_cat_percent', 'uses' => 'AdminSerDriverCont@admin_driver_cat_percent' ]);

			Route::get('update', [ 'as' => 'admin_ser_dupdate', 'uses' => 'AdminSerDriverCont@admin_ser_dupdate' ]);
			Route::post('update', [ 'as' => 'admin_ser_pdupdate', 'uses' => 'AdminSerDriverCont@admin_ser_pdupdate' ]);

			Route::get('orders', [ 'as' => 'admin_ser_dorders', 'uses' => 'AdminSerDriverCont@admin_ser_dorders' ]);
			Route::get('supports', [ 'as' => 'admin_ser_dsupports', 'uses' => 'AdminSerDriverCont@admin_ser_dsupports' ]);
			Route::any('reviews', [ 'as' => 'admin_ser_dreviews', 'uses' => 'AdminSerDriverCont@admin_ser_dreviews' ]);

			Route::get('products', [ 'as' => 'admin_ser_dproducts', 'uses' => 'AdminSerDriverCont@admin_ser_dproducts' ]);
			Route::post('products', [ 'as' => 'admin_ser_dproduct_update', 'uses' => 'AdminSerDriverCont@admin_ser_dproduct_update' ]);

			});

///////////////		Ledgers 		///////////////////////////
		Route::group(['prefix' => 'ledger' ] , function()
			{

			Route::get('all', [ 'as' => 'admin_ser_ledger_all', 'uses' => 'AdminSerDriverCont@admin_ser_ledger_all' ]);

			Route::post('add', [ 'as' => 'admin_ser_ledger_padd', 'uses' => 'AdminSerDriverCont@admin_ser_ledger_padd' ]);

			Route::post('update', [ 'as' => 'admin_ser_ledger_pupdate', 'uses' => 'AdminSerDriverCont@admin_ser_ledger_pupdate' ]);

			});
	///////////////		Ledgers 		///////////////////////////

			// Route::get('category/details', [ 'as' => 'admin_service_cat_details', 'uses' => 'AdminServiceCont@admin_service_cat_details' ]);
		});///////////////		Services 		///////////////////////////


	Route::group([ 'prefix' => 'support', 'namespace' => 'Support'] , function()
		{///////////////		Support 		///////////////////////////

		Route::get('cat/all', [ 'as' => 'admin_support_cat_all', 'uses' => 'AdminSupportCont@admin_support_cat_all' ]);

		Route::get('block', [ 'as' => 'admin_support_category_block', 'uses' => 'AdminSupportCont@admin_support_category_block' ]);

		Route::get('add', [ 'as' => 'admin_support_cat_add', 'uses' => 'AdminSupportCont@admin_support_cat_add' ]);
		Route::post('add', [ 'as' => 'admin_support_cat_add_post', 'uses' => 'AdminSupportCont@admin_support_cat_add_post' ]);

		Route::get('update', [ 'as' => 'admin_support_cat_update', 'uses' => 'AdminSupportCont@admin_support_cat_update' ]);
		Route::post('update', [ 'as' => 'admin_support_cat_update_post', 'uses' => 'AdminSupportCont@admin_support_cat_update_post' ]);

		});///////////////		Support 		///////////////////////////

///////////////		Organization 		///////////////////////////
	Route::group(['prefix' => 'org', 'namespace' => 'Org' ] , function()
		{

		Route::get('all', [ 'as' => 'admin_org_all', 'uses' => 'AdminOrgCont@admin_org_all' ]);

		Route::get('block', [ 'as' => 'admin_org_block', 'uses' => 'AdminOrgCont@admin_org_block' ]);

		Route::get('add', [ 'as' => 'admin_org_add', 'uses' => 'AdminOrgCont@admin_org_add' ]);
		Route::post('add', [ 'as' => 'admin_org_add_post', 'uses' => 'AdminOrgCont@admin_org_add_post' ]);

		Route::get('update', [ 'as' => 'admin_org_update', 'uses' => 'AdminOrgCont@admin_org_update' ]);
		Route::post('update', [ 'as' => 'admin_org_update_post', 'uses' => 'AdminOrgCont@admin_org_update_post' ]);

		Route::any('reviews', [ 'as' => 'admin_org_reviews', 'uses' => 'AdminOrgCont@admin_org_reviews' ]);

		Route::get('login/history', [ 'as' => 'admin_org_login_history', 'uses' => 'AdminOrgCont@admin_org_login_history' ]);

		Route::group([ 'prefix' => 'services'] , function()
			{///////////////		Service 		///////////////////////////

			Route::get('all', [ 'as' => 'admin_org_service_all', 'uses' => 'AdminOrgServiceCont@admin_org_service_all' ]);

			Route::post('brand/products/list', [ 'as' => 'admin_brand_products', 'uses' => 'AdminOrgServiceCont@admin_brand_products' ]);

			Route::post('update/pecentage', [ 'as' => 'admin_org_cat_percent', 'uses' => 'AdminOrgServiceCont@admin_org_cat_percent' ]);

			/////////////	Products 	//////////////////////
			Route::group(["prefix" => "product"] , function()
				{

				Route::get('all', [ 'as' => 'admin_org_service_products', 'uses' => 'AdminOrgServiceCont@admin_org_service_products' ]);

				Route::post('price/update', [ 'as' => 'admin_org_ser_product_update', 'uses' => 'AdminOrgServiceCont@admin_org_ser_product_update' ]);

				});
			/////////////	Products 	//////////////////////

			////////////	Area 	//////////////////////////
			Route::group(["prefix" => "area"] , function()
				{

				Route::get('all', [ 'as' => 'admin_org_ser_areas', 'uses' => 'AdminOrgServiceCont@admin_org_ser_areas' ]);

				Route::get('add', [ 'as' => 'admin_org_area_add', 'uses' => 'AdminOrgServiceCont@admin_org_area_add' ]);
				Route::post('add', [ 'as' => 'admin_org_area_padd', 'uses' => 'AdminOrgServiceCont@admin_org_area_padd' ]);

				Route::get('update', [ 'as' => 'admin_org_area_update', 'uses' => 'AdminOrgServiceCont@admin_org_area_update' ]);
				Route::post('update', [ 'as' => 'admin_org_area_pupdate', 'uses' => 'AdminOrgServiceCont@admin_org_area_pupdate' ]);

				Route::get('block', [ 'as' => 'admin_org_ser_area_block', 'uses' => 'AdminOrgServiceCont@admin_org_ser_area_block' ]);

				});
	////////////	Area 	//////////////////////////

	////////////	ETokens 	//////////////////////////
			Route::group(["prefix" => "etoken"] , function()
				{

				Route::get('all', [ 'as' => 'admin_org_ser_etokens', 'uses' => 'AdminOrgServiceCont@admin_org_ser_etokens' ]);

				Route::get('add', [ 'as' => 'admin_org_etoken_add', 'uses' => 'AdminOrgServiceCont@admin_org_etoken_add' ]);
				Route::post('add', [ 'as' => 'admin_org_etoken_padd', 'uses' => 'AdminOrgServiceCont@admin_org_etoken_padd' ]);

				Route::get('update', [ 'as' => 'admin_org_etoken_update', 'uses' => 'AdminOrgServiceCont@admin_org_etoken_update' ]);
				Route::post('update', [ 'as' => 'admin_org_etoken_pupdate', 'uses' => 'AdminOrgServiceCont@admin_org_etoken_pupdate' ]);

				Route::get('block', [ 'as' => 'admin_org_etoken_block', 'uses' => 'AdminOrgServiceCont@admin_org_etoken_block' ]);

				});
	////////////	Etokens 	//////////////////////////

	///////////////		 Org Service Driver 		///////////////////////////
			Route::group([ 'prefix' => 'driver'] , function()
				{

					Route::post('unique', [ 'as' => 'admin_driver_unique_check', 'uses' => 'AdminOrgServiceCont@admin_driver_unique_check' ]);
					Route::post('update/unique', [ 'as' => 'admin_driver_uunique_check', 'uses' => 'AdminOrgServiceCont@admin_driver_uunique_check' ]);

					Route::post('update/brands', [ 'as' => 'admin_dupdate_brands', 'uses' => 'AdminOrgServiceCont@admin_dupdate_brands' ]);


					Route::get('add', [ 'as' => 'admin_org_service_dadd', 'uses' => 'AdminOrgServiceCont@admin_org_service_dadd' ]);
					Route::post('add', [ 'as' => 'admin_org_service_dadd_post', 'uses' => 'AdminOrgServiceCont@admin_org_service_dadd_post' ]);

					Route::get('update', [ 'as' => 'admin_org_service_dupdate', 'uses' => 'AdminOrgServiceCont@admin_org_service_dupdate' ]);
					Route::post('update', [ 'as' => 'admin_org_service_dupdate_post', 'uses' => 'AdminOrgServiceCont@admin_org_service_dupdate_post' ]);

					Route::any('all', [ 'as' => 'admin_org_service_dall', 'uses' => 'AdminOrgServiceCont@admin_org_service_dall' ]);

				//Route::get('block', [ 'as' => 'admin_driver_block', 'uses' => 'AdminOrgCont@admin_driver_block' ]);

				});
	///////////////		 Org Service Driver 		///////////////////////////

	});///////////////		Service 		///////////////////////////

	///////////////		Support 		///////////////////////////
	Route::group([ 'prefix' => 'supports'] , function()
		{

			Route::get('all', [ 'as' => 'admin_org_support_all', 'uses' => 'AdminOrgSupportCont@admin_org_support_all' ]);
		///////////////		 Org Support Driver 		///////////////////////////
			Route::group([ 'prefix' => 'driver'] , function()
				{
					
				Route::get('all', [ 'as' => 'admin_org_support_dall', 'uses' => 'AdminOrgSupportCont@admin_org_support_dall' ]);

				Route::get('add', [ 'as' => 'admin_org_support_dadd', 'uses' => 'AdminOrgSupportCont@admin_org_support_dadd' ]);
				Route::post('add', [ 'as' => 'admin_org_support_dadd_post', 'uses' => 'AdminOrgSupportCont@admin_org_support_dadd_post' ]);

				Route::get('update', [ 'as' => 'admin_org_support_dupdate', 'uses' => 'AdminOrgSupportCont@admin_org_support_dupdate' ]);
				Route::post('update', [ 'as' => 'admin_org_support_dupdate_post', 'uses' => 'AdminOrgSupportCont@admin_org_support_dupdate_post' ]);

				});///////////////		 Org Support Driver 		///////////////////////////

		});///////////////		Support 		///////////////////////////


///////////////		Ledgers 		///////////////////////////
	Route::group(['prefix' => 'ledger' ] , function()
		{

		Route::get('all', [ 'as' => 'admin_org_ledger_all', 'uses' => 'AdminOrgCont@admin_org_ledger_all' ]);

		Route::post('add', [ 'as' => 'admin_org_ledger_padd', 'uses' => 'AdminOrgCont@admin_org_ledger_padd' ]);

		Route::post('update', [ 'as' => 'admin_org_ledger_pupdate', 'uses' => 'AdminOrgCont@admin_org_ledger_pupdate' ]);

		Route::get('delete', [ 'as' => 'admin_org_ledger_delete', 'uses' => 'AdminOrgCont@admin_org_ledger_delete' ]);

		});
	///////////////		Ledgers 		///////////////////////////


	});
///////////////		Organization 		///////////////////////////

	///////////////		Drivers 		///////////////////////////
	Route::group(['prefix' => 'driver', 'namespace' => 'Driver' ] , function()
		{

		Route::get('all', [ 'as' => 'admin_drivers', 'uses' => 'AdminDriverCont@admin_drivers_list' ]);

		Route::get('block', [ 'as' => 'admin_driver_block', 'uses' => 'AdminDriverCont@admin_driver_block' ]);

		Route::get('approve', [ 'as' => 'admin_driver_approve', 'uses' => 'AdminDriverCont@admin_driver_approve' ]);

		Route::get('map', [ 'as' => 'admin_drivers_map', 'uses' => 'AdminDriverCont@admin_drivers_map' ]);
		Route::get('data', [ 'as' => 'admin_drivers_map_data', 'uses' => 'AdminDriverCont@admin_drivers_map_data' ]);

		});
	///////////////		Drivers 		///////////////////////////

	///////////////		Drivers 		///////////////////////////
	Route::group(['prefix' => 'customer', 'namespace' => 'Customer' ] , function()
		{

		Route::any('all', [ 'as' => 'admin_custs', 'uses' => 'AdminCustCont@admin_custs' ]);
		Route::any('ajax', [ 'as' => 'admin_custs_ajax', 'uses' => 'AdminCustCont@admin_custs_ajax' ]);

		Route::get('order/all', [ 'as' => 'admin_cust_orders', 'uses' => 'AdminCustCont@admin_cust_orders' ]);
		Route::get('order/details', [ 'as' => 'admin_cust_order_details', 'uses' => 'AdminCustCont@admin_cust_order_details' ]);

		Route::get('block', [ 'as' => 'admin_cust_block', 'uses' => 'AdminCustCont@admin_cust_block' ]);

		Route::any('reviews', [ 'as' => 'admin_customer_reviews', 'uses' => 'AdminCustCont@admin_customer_reviews' ]);

		});
	///////////////		Drivers 		///////////////////////////


	///////////////		Orders 		///////////////////////////
	Route::group(['prefix' => 'order', 'namespace' => 'Order' ] , function()
		{

		Route::any('all', [ 'as' => 'admin_orders', 'uses' => 'AdminOrderCont@admin_orders' ]);

		Route::get('details', [ 'as' => 'admin_order_details', 'uses' => 'AdminOrderCont@admin_order_details' ]);
		
		Route::any('allOFailure', [ 'as' => 'admin_orders_failure', 'uses' => 'AdminOrderCont@admin_orders_failure' ]);

		});
	///////////////		Orders 		///////////////////////////

	///////////////		Orders 		///////////////////////////
	Route::group(['prefix' => 'contactus', 'namespace' => 'Admin' ] , function()
		{

			Route::get('all', [ 'as' => 'admin_contactus', 'uses' => 'AdminOtherCont@admin_contactus' ]);

			Route::get('history', [ 'as' => 'admin_contactus_history', 'uses' => 'AdminOtherCont@admin_contactus_history' ]);

			Route::post('reply', [ 'as' => 'admin_contactus_preply', 'uses' => 'AdminOtherCont@admin_contactus_preply' ]);
			

		});
	///////////////		Orders 		///////////////////////////

	Route::any('reviews', [ 'as' => 'admin_all_reviews', 'uses' => 'Admin\AdminOtherCont@admin_all_reviews' ]);

	///////////////		Ledgers 		///////////////////////////
	Route::group(['prefix' => 'ledger', 'namespace' => 'Admin' ] , function()
		{

			Route::get('all', [ 'as' => 'admin_ledger_all', 'uses' => 'AdminOtherCont@admin_ledger_all' ]);

			Route::get('add', [ 'as' => 'admin_ledger_add', 'uses' => 'AdminOtherCont@admin_ledger_add' ]);
			Route::post('add', [ 'as' => 'admin_ledger_padd', 'uses' => 'AdminOtherCont@admin_ledger_padd' ]);

			Route::get('update', [ 'as' => 'admin_ledger_add', 'uses' => 'AdminOtherCont@admin_ledger_add' ]);
			Route::post('update', [ 'as' => 'admin_ledger_padd', 'uses' => 'AdminOtherCont@admin_ledger_padd' ]);

		});
	///////////////		Ledgers 		///////////////////////////


	Route::group([ 'prefix' => 'payment', 'namespace' => 'Payment' ] , function()
		{

			Route::get('service/all', [ 'as' => 'admin_ser_pay_all', 'uses' => 'AdminPaymentCont@admin_ser_pay_all' ]);

			Route::get('support/all', [ 'as' => 'admin_sup_pay_all', 'uses' => 'AdminPaymentCont@admin_sup_pay_all' ]);

			Route::get('coupon/all', [ 'as' => 'admin_coupon_pay_all', 'uses' => 'AdminPaymentCont@admin_coupon_pay_all']);

			Route::get('eToken/all', [ 'as' => 'admin_etoken_pay_all', 'uses' => 'AdminPaymentCont@admin_etoken_pay_all']);

		});


	///////////////		Orders 		///////////////////////////
	Route::group(['prefix' => 'reports', 'namespace' => 'Admin' ] , function()
		{
			Route::post('customers', [ 'as' => 'admin_custs_preport', 'uses' => 'AdminExportCont@admin_custs_preport' ]);

			Route::post('drivers', [ 'as' => 'admin_drivers_preport', 'uses' => 'AdminExportCont@admin_drivers_preport' ]);

			Route::post('orders', [ 'as' => 'admin_orders_preport', 'uses' => 'AdminExportCont@admin_orders_preport' ]);
			//Add M2 PArt 2
			Route::post('contacts', [ 'as' => 'admin_contacts_preport', 'uses' => 'AdminExportCont@admin_contacts_preport' ]);

		});
	///////////////		Orders 		///////////////////////////

		//Add M2 Part 2
		///////////////		Sent Offer 		///////////////////////////
		Route::group(['prefix' => 'release', 'namespace' => 'Release' ] , function()
		{
			Route::any('all', [ 'as' => 'admin_release_offer', 'uses' => 'AdminReleaseCont@admin_release_offer' ]);
			Route::get('offerapprove', [ 'as' => 'admin_approve_offer', 'uses' => 'AdminReleaseCont@admin_approve_offer' ]);
			Route::get('updatemargin', [ 'as' => 'admin_update_margin', 'uses' => 'AdminReleaseCont@admin_update_margin' ]);
			Route::any('chart/all', [ 'as' => 'admin_chartanalysis', 'uses' => 'AdminChartCont@admin_chartanalysis' ]);
			Route::get('chart/filter', [ 'as' => 'admin_chartfilter', 'uses' => 'AdminChartCont@admin_chartfilter' ]);
			Route::get('orderType/filter', [ 'as' => 'admin_orderTypeChart', 'uses' => 'AdminChartCont@admin_orderTypeChart' ]);
			Route::get('userType/filter', [ 'as' => 'newRegistrationRate', 'uses' => 'AdminChartCont@newRegistrationRate' ]);
			Route::get('userType/OrderMonth/filter', [ 'as' => 'admin_userMonthlyOrderData', 'uses' => 'AdminChartCont@getUserTotalOrdersMonths' ]);
			Route::get('getUserOrderCustomerList', [ 'as' => 'admin_UserCustomerLIst', 'uses' => 'AdminChartCont@getUserOrderCustomerList' ]);
		});
		///////////////		Sent Offer 		///////////////////////////
		
		////////// Floor Margin //////////////// 
		Route::group([ 'prefix' => 'floormargin', 'namespace' => 'Admin'] , function()
		{
			Route::get('all', [ 'as' => 'admin_floormargin_all', 'uses' => 'AdminOtherCont@admin_floormargin_all' ]);
			Route::get('add', [ 'as' => 'admin_floor_aget', 'uses' => 'AdminOtherCont@admin_floor_aget' ]);
			Route::post('add',[ 'as' => 'admin_floor_apost','uses' => 'AdminOtherCont@admin_floor_apost' ]);
			Route::get('update',[ 'as' => 'admin_floor_uget', 'uses' => 'AdminOtherCont@admin_floor_uget' ]);
			Route::post('update',[ 'as' => 'admin_floor_upost','uses' => 'AdminOtherCont@admin_floor_upost' ]);
			Route::get('block', [ 'as' => 'admin_floor_active', 'uses' => 'AdminOtherCont@admin_floor_active' ]);
			Route::get('delete', [ 'as' => 'admin_floor_delete', 'uses' => 'AdminOtherCont@admin_floor_delete' ]);
		}); 
		////////// Floor Margin ///////////
		////////// Reward Point ////////////////
		Route::group([ 'prefix' => 'rewardpoint', 'namespace' => 'Admin'] , function()
		{
			Route::get('all', [ 'as' => 'admin_rewardpoint_all', 'uses' => 'AdminOtherCont@admin_rewardpoint_all' ]);
			Route::get('add', [ 'as' => 'admin_rewardpoint_aget', 'uses' => 'AdminOtherCont@admin_rewardpoint_aget' ]);
			Route::post('add',[ 'as' => 'admin_rewardpoint_apost','uses' => 'AdminOtherCont@admin_rewardpoint_apost' ]);
			Route::get('block', [ 'as' => 'admin_rewardpoint_active', 'uses' => 'AdminOtherCont@admin_rewardpoint_active' ]);
			Route::get('update',[ 'as' => 'admin_rewardpoint_uget', 'uses' => 'AdminOtherCont@admin_rewardpoint_uget' ]);
			Route::post('update',[ 'as' => 'admin_rewardpoint_upost','uses' => 'AdminOtherCont@admin_rewardpoint_upost' ]);
		
		});

		});/////////////////////////		Logged In Admin 		////////////////////////////

	});