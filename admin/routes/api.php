<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::any('app_cat_lists',['as'=>'app_cat_lists','uses'=>'CommonController@app_cat_lists']);


Route::any('test',['as'=>'test_api','uses'=>'CommonController@test_api']);
Route::any('payment_order_detail', [ 'as' => 'payment_order_detail', 'uses' => 'CommonController@payment_order_detail' ]);
Route::any('payment_confirm_order_detail', [ 'as' => 'payment_confirm_order_detail', 'uses' => 'CommonController@payment_confirm_order_detail' ]);
Route::post('unserialize', [ 'as' => 'coupon_users', 'uses' => 'CommonController@unserializeUsers' ]);
//$category = Category::admin_single_cat_details($request->all());