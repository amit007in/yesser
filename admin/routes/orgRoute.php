<?php

Route::get('/', [ 'as' => 'org_panel_login1', 'uses' => 'OrgPanelController@org_panel_login' ]);
Route::get('login', [ 'as' => 'org_panel_login2', 'uses' => 'OrgPanelController@org_panel_login' ]);

Route::post('login', [ 'as' => 'org_panel_plogin', 'uses' => 'OrgPanelController@org_panel_plogin' ]);

Route::get('register', [ 'as' => 'org_register', 'uses' => 'OrgPanelController@org_register' ]);
Route::post('register', [ 'as' => 'org_register_post', 'uses' => 'OrgPanelController@org_register_post' ]);

Route::group(["middleware" => "org_middleware"] , function()
{

	Route::get('dashboard', [ 'as' => 'org_dashboard', 'uses' => 'OrgPanelController@org_dashboard' ]);
	Route::get('dashboard_data', [ 'as' => 'org_dashboard_data', 'uses' => 'OrgPanelController@org_dashboard_data' ]);

	Route::get('profile', [ 'as' => 'org_profile', 'uses' => 'OrgPanelController@org_profile' ]);
	Route::post('profile', [ 'as' => 'org_pprofile', 'uses' => 'OrgPanelController@org_pprofile' ]);

	Route::get('password', [ 'as' => 'org_password', 'uses' => 'OrgPanelController@org_password' ]);
	Route::post('password', [ 'as' => 'org_ppassword', 'uses' => 'OrgPanelController@org_ppassword' ]);

	Route::get('logout', [ 'as' => 'org_logout', 'uses' => 'OrgPanelController@org_logout' ]);

	Route::get('delete', [ 'as' => 'org_delete', 'uses' => 'OrgPanelController@org_delete' ]);

///////////////		Services 	///////////////////////////////
	Route::group(["prefix" => "service"] , function()
	{

		Route::get('all', [ 'as' => 'org_services_all', 'uses' => 'OrgPanelServiceController@org_services_all' ]);

		Route::get('products', [ 'as' => 'org_service_products', 'uses' => 'OrgPanelServiceController@org_service_products' ]);
		Route::post('update', [ 'as' => 'org_ser_product_update', 'uses' => 'OrgPanelServiceController@org_ser_product_update' ]);		

	////////////	Drivers 	//////////////////////
	Route::group(["prefix" => "drivers"] , function()
		{

			Route::get('all', [ 'as' => 'org_ser_drivers', 'uses' => 'OrgPanelServiceController@org_ser_drivers' ]);

			Route::get('add', [ 'as' => 'org_ser_adriver', 'uses' => 'OrgPanelServiceController@org_ser_adriver' ]);
			Route::post('add', [ 'as' => 'org_ser_padriver', 'uses' => 'OrgPanelServiceController@org_ser_padriver' ]);

			Route::get('update', [ 'as' => 'org_ser_udriver', 'uses' => 'OrgPanelServiceController@org_ser_udriver' ]);
			Route::post('update', [ 'as' => 'org_ser_pudriver', 'uses' => 'OrgPanelServiceController@org_ser_pudriver' ]);

			Route::get('block', [ 'as' => 'org_ser_bdriver', 'uses' => 'OrgPanelServiceController@org_ser_bdriver' ]);

		});
	////////////	Drivers 	//////////////////////

	////////////	Area 	//////////////////////////
	Route::group(["prefix" => "area"] , function()
		{
			Route::get('all', [ 'as' => 'org_ser_areas', 'uses' => 'OrgPanelServiceController@org_ser_areas' ]);

			Route::get('add', [ 'as' => 'org_area_add', 'uses' => 'OrgPanelServiceController@org_area_add' ]);
			Route::post('add', [ 'as' => 'org_area_padd', 'uses' => 'OrgPanelServiceController@org_area_padd' ]);

			Route::get('update', [ 'as' => 'org_area_update', 'uses' => 'OrgPanelServiceController@org_area_update' ]);
			Route::post('update', [ 'as' => 'org_area_pupdate', 'uses' => 'OrgPanelServiceController@org_area_pupdate' ]);

			Route::get('block', [ 'as' => 'org_ser_area_block', 'uses' => 'OrgPanelServiceController@org_ser_area_block' ]);

		});
	////////////	Area 	//////////////////////////

	////////////	ETokens 	//////////////////////////
	Route::group(["prefix" => "etoken"] , function()
		{

			Route::get('all', [ 'as' => 'org_ser_coupons', 'uses' => 'OrgOtherController@org_ser_coupons' ]);

			Route::get('add', [ 'as' => 'org_coupon_add', 'uses' => 'OrgOtherController@org_coupon_add' ]);
			Route::post('add', [ 'as' => 'org_coupon_padd', 'uses' => 'OrgOtherController@org_coupon_padd' ]);

			Route::get('update', [ 'as' => 'org_coupon_update', 'uses' => 'OrgOtherController@org_coupon_update' ]);
			Route::post('update', [ 'as' => 'org_coupon_pupdate', 'uses' => 'OrgOtherController@org_coupon_pupdate' ]);

			Route::get('block', [ 'as' => 'org_coupon_block', 'uses' => 'OrgOtherController@org_coupon_block' ]);

		});
	////////////	Etokens 	//////////////////////////


	});
///////////////		Services 	///////////////////////////////

	Route::get('ledgers', [ 'as' => 'org_ledgers', 'uses' => 'OrgOtherController@org_ledgers' ]);

///////////////		Supports 	///////////////////////////////
Route::group(["prefix" => "support"] , function()
	{

		Route::get('all', [ 'as' => 'org_supports_all', 'uses' => 'OrgPanelSupportController@org_supports_all' ]);

	////////////	Drivers 	//////////////////////
	Route::group(["prefix" => "drivers"] , function()
		{

			Route::get('all', [ 'as' => 'org_sup_drivers', 'uses' => 'OrgPanelSupportController@org_sup_drivers' ]);

			Route::get('add', [ 'as' => 'org_sup_adriver', 'uses' => 'OrgPanelSupportController@org_sup_adriver' ]);
			Route::post('add', [ 'as' => 'org_sup_padriver', 'uses' => 'OrgPanelSupportController@org_sup_padriver' ]);

			Route::get('update', [ 'as' => 'org_sup_udriver', 'uses' => 'OrgPanelSupportController@org_sup_udriver' ]);
			Route::post('update', [ 'as' => 'org_sup_pudriver', 'uses' => 'OrgPanelSupportController@org_sup_pudriver' ]);

			Route::get('block', [ 'as' => 'org_sup_bdriver', 'uses' => 'OrgPanelSupportController@org_sup_bdriver' ]);

		});
	////////////	Drivers 	//////////////////////		


	});
///////////////		Supports 	///////////////////////////////

///////////////		Orders 	///////////////////////////////////
	Route::group(["prefix" => "order"] , function()
	{

		Route::any('all', [ 'as' => 'org_orders', 'uses' => 'OrgOrderController@org_orders' ]);

		Route::get('details', [ 'as' => 'org_order_details', 'uses' => 'OrgOrderController@org_order_details' ]);

	});
///////////////		Orders 	///////////////////////////////////

///////////////		Orders 	///////////////////////////////////
	Route::group(["prefix" => "reviews"] , function()
	{
		Route::any('all', [ 'as' => 'org_review', 'uses' => 'OrgOtherController@org_review' ]);
	});
///////////////		Orders 	///////////////////////////////////

///////////////		Orders 	///////////////////////////////////
	Route::group(["prefix" => "driver"] , function()
	{
		Route::get('track', [ 'as' => 'org_drivers_track', 'uses' => 'OrgDriverController@org_drivers_track' ]);

		Route::group(["prefix" => "service"] , function()
		{

			Route::get('all', [ 'as' => 'org_services_dall', 'uses' => 'OrgDriverController@org_services_dall' ]);
			Route::any('all/new', [ 'as' => 'org_services_dall_new', 'uses' => 'OrgDriverController@org_services_dall_new' ]);

			Route::any('reviews', [ 'as' => 'org_ser_dreviews', 'uses' => 'OrgDriverController@org_ser_dreviews' ]);
			Route::any('orders', [ 'as' => 'org_ser_dorders', 'uses' => 'OrgDriverController@org_ser_dorders' ]);

		});

		Route::group(["prefix" => "support"] , function()
		{

			Route::get('supports', [ 'as' => 'org_supports_dall', 'uses' => 'OrgDriverController@org_supports_dall' ]);

		});

	});
///////////////		Orders 	///////////////////////////////////


	Route::group([ 'prefix' => 'payment'] , function()
		{

			Route::get('service/all', [ 'as' => 'org_ser_pay_all', 'uses' => 'OrgPaymentController@org_ser_pay_all' ]);

			Route::get('support/all', [ 'as' => 'org_sup_pay_all', 'uses' => 'OrgPaymentController@org_sup_pay_all' ]);

			Route::get('eToken/all', [ 'as' => 'org_etoken_pay_all', 'uses' => 'OrgPaymentController@org_etoken_pay_all']);

		});



});