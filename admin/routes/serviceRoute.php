<?php

Route::get('/', [ 'as' => 'service_panel_login', 'uses' => 'ServicePanelController@service_panel_login' ]);
Route::get('login', [ 'as' => 'service_panel_login1', 'uses' => 'ServicePanelController@service_panel_login' ]);
Route::post('login', [ 'as' => 'service_panel_plogin', 'uses' => 'ServicePanelController@service_panel_plogin' ]);

Route::get('verify/otp', [ 'as' => 'service_panel_verify_otp', 'uses' => 'ServicePanelController@service_panel_verify_otp' ]);
Route::post('verify/otp', [ 'as' => 'service_panel_verify_potp', 'uses' => 'ServicePanelController@service_panel_verify_potp' ]);


Route::group(["middleware" => "service_middleware"] , function()
{

	Route::post('uunique', [ 'as' => 'service_profile_uunique', 'uses' => 'ServicePanelController@service_profile_uunique' ]);

	Route::get('dashboard', ['as' => 'service_dashboard', 'uses' => 'ServicePanelController@service_dashboard']);
	Route::get('service_dash_data', ['as' => 'service_dash_data', 'uses' => 'ServicePanelController@service_dash_data']);

	//Route::get('service_details', ['as' => 'service_details', 'uses' => 'ServicePanelController@service_details']);

	Route::get('logout', ['as' => 'service_logout', 'uses' => 'ServicePanelController@service_logout']);

	Route::get('profile', ['as' => 'service_profile', 'uses' => 'ServicePanelController@service_profile']);
	Route::post('profile', ['as' => 'service_pprofile', 'uses' => 'ServicePanelController@service_pprofile']);

	Route::get('reviews', ['as' => 'service_reviews_all', 'uses' => 'ServicePanelOtherController@service_reviews_all']);
	
	Route::get('products', ['as' => 'service_products_all', 'uses' => 'ServicePanelOtherController@service_products_all']);
	Route::post('product/update', ['as' => 'service_panel_product_update', 'uses' => 'ServicePanelOtherController@service_panel_product_update']);

	Route::group(["prefix" => "order"] , function()
		{

		Route::get('all', ['as' => 'service_oall', 'uses' => 'ServicePanelOrderController@service_oall']);
		Route::get('completed', ['as' => 'ser_ocompleted', 'uses' => 'ServicePanelOrderController@ser_ocompleted']);
		Route::get('scheduled', ['as' => 'ser_oscheduled', 'uses' => 'ServicePanelOrderController@ser_oscheduled']);
		Route::get('cancelled', ['as' => 'ser_ocancelled', 'uses' => 'ServicePanelOrderController@ser_ocancelled']);

		Route::get('details', ['as' => 'service_order_details', 'uses' => 'ServicePanelOrderController@service_order_details']);

		});

	Route::group(["prefix" => "payment"] , function()
		{

			Route::get('service/all', [ 'as' => 'ser_pay_all', 'uses' => 'ServicePanelPaymentCont@driver_ser_pay_all' ]);

			Route::get('support/all', [ 'as' => 'sup_pay_all', 'uses' => 'ServicePanelPaymentCont@driver_sup_pay_all' ]);


		});

	Route::get('ledgers', [ 'as' => 'ser_ledgers', 'uses' => 'ServicePanelOtherController@ser_ledgers' ]);

});