<?php

// Route::group(["middleware" => "support_cookie_middleware"] , function()
// {



Route::get('/', [ 'as' => 'support_panel_login', 'uses' => 'SupportPanelController@support_panel_login' ]);
Route::get('login', [ 'as' => 'support_panel_login1', 'uses' => 'SupportPanelController@support_panel_login' ]);
Route::post('login', [ 'as' => 'support_panel_plogin', 'uses' => 'SupportPanelController@support_panel_plogin' ]);

Route::get('verify/otp', [ 'as' => 'support_panel_verify_otp', 'uses' => 'SupportPanelController@support_panel_verify_otp' ]);
Route::post('verify/otp', [ 'as' => 'support_panel_verify_potp', 'uses' => 'SupportPanelController@support_panel_verify_potp' ]);


Route::group(["middleware" => "support_middleware"] , function()
{

	Route::post('uunique', [ 'as' => 'support_profile_uunique', 'uses' => 'SupportPanelController@support_profile_uunique' ]);

	Route::get('dashboard', ['as' => 'support_dashboard', 'uses' => 'SupportPanelController@support_dashboard']);
	Route::get('support_dash_data', ['as' => 'support_dash_data', 'uses' => 'SupportPanelController@support_dash_data']);

	//Route::get('support_details', ['as' => 'support_details', 'uses' => 'SupportPanelController@support_details']);

	Route::get('logout', ['as' => 'support_logout', 'uses' => 'SupportPanelController@support_logout']);

	Route::get('profile', ['as' => 'support_profile', 'uses' => 'SupportPanelController@support_profile']);
	Route::post('profile', ['as' => 'support_pprofile', 'uses' => 'SupportPanelController@support_pprofile']);

Route::group(["prefix" => "order"] , function()
	{

	Route::get('all', ['as' => 'support_order_all', 'uses' => 'SupportPanelOrderController@support_order_all']);

	Route::get('details', ['as' => 'support_order_details', 'uses' => 'SupportPanelOrderController@support_order_details']);
	

	});


});


//});