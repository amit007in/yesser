<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
}); 


//////////////////		Image resizing 	//////////////////////////////////
Route::get('Resize/{id}', function($id) {
    $img = Image::make(env('IMAGE_URL') . $id)->resize(function($constraint) {
        $constraint->aspectRatio();
    });
    return $img->response();
});

Route::get('Resize/{id}/{width?}/{height?}', function($id, $width = null, $height = null) {
    $img = Image::make(env('IMAGE_URL') . $id)->resize($width, $height, function($constraint) {
        $constraint->aspectRatio();
    });
    return $img->response();
});
//////////////////		Image resizing 	//////////////////////////////////

///		Driver Unique Checks 	//////////////
Route::post('adriver/unique', [ 'as' => 'driver_unique_acheck', 'uses' => 'CommonController@driver_unique_acheck' ]);
Route::post('udriver/unique', [ 'as' => 'driver_unique_ucheck', 'uses' => 'CommonController@driver_unique_ucheck' ]);
///		Driver Unique Checks 	//////////////

///		Org Unique Checks 	//////////////////
Route::post('org/unique', [ 'as' => 'org_unique_check', 'uses' => 'CommonController@org_unique_check' ]);
Route::post('org/uunique', [ 'as' => 'org_uunique_check', 'uses' => 'CommonController@org_uunique_check' ]);
///		Org Unique Checks 	//////////////////

Route::post('brand/products', [ 'as' => 'brand_products', 'uses' => 'CommonController@brand_products' ]);
Route::post('dupdate/product/list', [ 'as' => 'dupdate_product_list', 'uses' => 'CommonController@dupdate_product_list' ]);

///		Coupon Checks 	//////////////////
Route::post('coupon/unique', [ 'as' => 'coupon_unique_check', 'uses' => 'CommonController@coupon_unique_check' ]);
Route::post('coupon/uunique', [ 'as' => 'coupon_uunique_check', 'uses' => 'CommonController@coupon_uunique_check' ]);
///		Coupon Checks 	//////////////////

Route::any('brands', [ 'as' => 'brands_listings', 'uses' => 'CommonController@brands_listings']);
Route::any('notifications', [ 'as' => 'notification_listing', 'uses' => 'CommonController@notification_listing']); // Part 3.2
Route::any('brand/ranking', [ 'as' => 'brands_ranking_listings', 'uses' => 'CommonController@brands_ranking_listings']);

/////////Add New Route ///////////
Route::post('dupdate/user/list', [ 'as' => 'dupdate_users_list', 'uses' => 'CommonController@dupdate_users_list' ]);
Route::post('dupdate/brand/list', [ 'as' => 'dupdate_coupon_brands_listings', 'uses' => 'CommonController@dupdate_coupon_brands_listings' ]);
Route::post('dupdate/product/listt', [ 'as' => 'dupdate_coupon_brand_products', 'uses' => 'CommonController@dupdate_coupon_brand_products' ]);
//Route::get('autocomplete', 'CommonController@autocomplete')->name('autocomplete');
Route::get('dupdate/user/listt', [ 'as' => 'autocomplete', 'uses' => 'CommonController@autocomplete' ]);
//Add M2 Part 2
Route::group([ 'namespace' => 'Admin'] , function()
{
	Route::group(['prefix' => 'order', 'namespace' => 'Order' ] , function()
	{
		Route::get('dupdate/driver/list', [ 'as' => 'admin_reassign_driver', 'uses' => 'AdminOrderCont@getDrivers' ]);
		Route::get('dupdate/driver/assign/order', [ 'as' => 'admin_assign_order_driver', 'uses' => 'AdminOrderCont@assignDriver' ]);
		Route::get('dupdate/driver/shuffle/order', [ 'as' => 'admin_shuffle_order_driver', 'uses' => 'AdminOrderCont@shuffleDriver' ]);
		Route::get('roundrobin/enabledisbale', [ 'as' => 'admin_roundrobin_enabledisable', 'uses' => 'AdminOrderCont@roundRobinEnableDisable' ]);// Part 3.2
	});
});
Route::post('dupdate/promoridesuser/listt', [ 'as' => 'getpromousersrides', 'uses' => 'CommonController@getpromousersrides' ]);

