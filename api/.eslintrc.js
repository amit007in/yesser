module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "globals": {
        "appRoot": true,
        "trans": true,
        "_": true,
        "io": true
    },
    "rules": {
        "indent": [
            "error",
            "tab"
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "double"
        ],
        "semi": [
            "error",
            "always"
        ],
        "no-redeclare": 0,
        "no-unused-vars": 0,
        "no-console": 0,
        "no-mixed-spaces-and-tabs": 0,
        "no-undef": 0
    }
};