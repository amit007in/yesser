require("dotenv").config();

var createError = require("http-errors");
var express = require("express");
var i18n = require("i18n");// Language
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
// var winston = require("winston");
// var expressWinston = require("express-winston");
var swaggerJSDoc = require("swagger-jsdoc");
var bodyParser = require('body-parser');
const expressValidator = require("express-validator");
var CronJob = require('cron').CronJob;

//////////////  Global Parameters   //////////////////////////
global.appRoot = path.resolve(__dirname);
//////////////  Global Parameters   //////////////////////////
var {commonFunctions, cronFunctions} = require(appRoot+"/libs");//////////////////    Libraries

var multer = require('multer');
const storage = multer.diskStorage({

  destination: function (req, file, cb) {
    cb(null, appRoot+"/public/Uploads");
  },
  filename: function (req, file, cb) {
    cb(null, commonFunctions.generateAccessToken(90)+'.'+file.originalname.split('.')[1]);
  }
  
});
global.upload = multer({storage: storage})
/////////////////////////////     Images  Upload  /////////////////////////

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");

/////////////////// Internationalization Added    /////////////////////////////////////////
i18n.configure({

    lng: "1En",
    defaultLocale: "1En",
    directory: __dirname + "/locales",
    register: global,
    locales: ["1En", "2Hi", "3Ur", "4Ch", "5Ar"],
    preload: ["1En", "2Hi", "3Ur", "4Ch", "5Ar"],
    fallbackLng: "1En",
    saveMissing: true,
    sendMissingTo: "1En",
    useCookie: false,
    detectLngFromHeaders: false,
    syncFiles: true,
    api: {
      "__": "trans",  //now req.__ becomes req.t
      "__n": "tn" //and req.__n can be called as req.tn
    }

  });
/////////////////// Internationalization Added    /////////////////////////////////////////

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use(bodyParser.json({ type: 'application/json' }));
//app.use(bodyParser.urlencoded({ extended: true }));
//app.use(bodyParser.json());
app.use(expressValidator() ); // Add this after the bodyParser middleware!
app.use(i18n.init);


app.use(function(req, res, next) {

  //console.log('Languageeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', req.header('language_id'), req.body);
  req.setTimeout(0) // no timeout
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

///////////////////////////   Logger    ///////////////////////////////////////////
// app.use(expressWinston.logger({
//   transports: [

//     new (winston.transports.Console)({
//       json: true,
//       colorize: true
//     }),

//     // new winston.transports.File({
//     //   filename: "logs/success.log"
//     // })

//   ]
// }));
///////////////////////////   Logger    ///////////////////////////////////////////

app.use("/", require("./routes/index"));
app.use("/common", require("./routes/commonRoute"));

app.use("/user", require("./routes/Users/userRoute"));
app.use("/user/service", require("./routes/Users/userServiceRoute"));
app.use("/user/other", require("./routes/Users/userOtherRoute"));
app.use("/user/water", require("./routes/Users/userWaterRoute"));


app.use("/service", require("./routes/Services/serviceRoute"));
app.use("/service/order", require("./routes/Services/serviceOrderRoute"));
app.use("/service/other", require("./routes/Services/serviceOtherRoute"));
app.use("/service/water", require("./routes/Services/serviceWaterRoute"));

app.use("/support", require("./routes/Supports/supportRoute"));

var options = {
  swaggerDefinition: {
    info: {
      title: process.env.APP_NAME, // Title (required)
      version: "1.0.0", // Version (required)
    },
  },
  apis: [
          "./routes/index.js", 
          "./routes/commonRoute.js",

          "./routes/Users/userRoute.js",
          "./routes/Users/userServiceRoute.js",
          "./routes/Users/userOtherRoute.js",
          "./routes/Users/userWaterRoute.js",

          "./routes/Services/serviceOrderRoute.js",
          "./routes/Services/serviceRoute.js",
          "./routes/Services/serviceOtherRoute.js",
          "./routes/Services/serviceWaterRoute.js",

          "./routes/Supports/supportRoute.js"
    ], // Path to the API docs
};

// Initialize swagger-jsdoc -> returns validated swagger spec in json format
var swaggerSpec = swaggerJSDoc(options);

if(process.env.NODE_ENV == 'development')
  {
    app.get("/api-docs.json", function(req, res) {
      res.setHeader("Content-Type", "application/json");
      res.send(swaggerSpec);
    });    
  }


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});



// new CronJob('* * * * * *', function() {
//   console.log('One Hr Cron Runned');
// }, null, true, 'America/Los_Angeles');


console.log('Before job instantiation');
const job1 = new CronJob('*/55 * * * * *', function() {

  cronFunctions.oneHrOrdersLeft();
  cronFunctions.ReachingLocation();

});

const job2 = new CronJob('*/51 * * * * *', function() {
  cronFunctions.searchingOrdersCronCancel();
  
  //cronFunctions.SendCronNotification();
});

const job3 = new CronJob('1 1 * * * * *', function() {
  cronFunctions.fiftyMinutesPast();
  cronFunctions.fiftyFiveMinutesPast();
 // cronFunctions.oneHrOrdersLeft();
  cronFunctions.changeOnlineOffline();///   Offline One Cron
});


console.log('After job instantiation');
job1.start();
job2.start();
job3.start();



module.exports = app;