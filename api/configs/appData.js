// module.exports.Versioning = {
//    ANDROID: {
//      force: 1.0,
//      normal: 1.0
//    },
//    IOS: {
//      force: 1.0,
//      normal: 1.0
//    }
//   };

module.exports = {

	"Versioning": {
		ANDROID: {
			user: {
				force:  1.00,
				normal: 1.00,
			},
			driver: {
				force: 1.00,
				normal: 1.00,
			},
		},
		IOS: {
			user: {
				force: "1.0",
				normal: "1.0",
			},
			driver: {
				force: "1.0",
				normal: "1.0",
			},
		}
	},

	"order_atts": { exclude: ["order_type", "created_by", "continouos_startdt", "continuous_enddt", "continuous_time", "continuous","created_at", "updated_at" ] },

	"order_request_atts": { exclude:  ["order_id", "driver_request_latitude", "driver_request_longitude", "driver_confirmed_latitude", "driver_confirmed_longitude", "accepted_at", "driver_started_latitude", "driver_started_longitude", "created_at", "updated_at"] },

	"payment_atts": { exclude: ["customer_user_id", "customer_user_detail_id", "customer_user_type_id", "user_card_id", "customer_organisation_id", "order_id", "seller_user_id", "seller_user_detail_id", "seller_user_type_id", "seller_organisation_id", "created_at", "updated_at"] },


	"coupon_user_atts": { exclude: ["deleted", "blocked", "created_at", "updated_at", "user_id", "user_detail_id", "user_type_id", "payment_id", "rides_value" ] },

	"organisation_coupon_users_atts": { exclude: ["category_id","category_brand_id","category_brand_product_id", "seller_user_id", "seller_user_detail_id", "seller_user_type_id", "seller_organisation_id", "customer_user_id", "customer_user_type_id", "customer_organisation_id", "quantity", "expiry_days", "created_at", "updated_at"] },

	"driver_cust_atts": { exclude: ["stripe_connect_id", "stripe_connect_token", "address", "address_latitude", "address_longitude", "blocked", "created_at", "updated_at"] },

	"driver_cust_detail_atts": { exclude: ["category_id", "category_brand_id", "online", "otp", "password", "access_token", "latitude", "longitude", "timezone", "timezonez", "mulkiya_number", "mulkiya_front", "mulkiya_back", "mulkiya_validity", "created_by", "blocked", "forgot_token", "forgot_token_validity", "created_at", "updated_at"] },

	"driver_coupon_user_atts": ["coupon_user_id", "coupon_value", "coupon_type"],

	sms_numbers: ["96898133781", "96890630609", "96894704604", "96891313728", "96892076909"],

};