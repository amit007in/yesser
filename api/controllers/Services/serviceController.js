var moment = require("moment");

var Libs = require(appRoot+"/libs");//////////////////		Libraries
var Services = require(appRoot+"/services");///////////		Services
var Db = require(appRoot+"/models");//////////////////		Db Models
var Configs = require(appRoot+"/configs");/////////////		Configs

const got = require("got");

/////////////////		Send OTP 		/////////////////////////////////////
exports.serviceSendOtp = async (request, response) => {
	try{
		var data = request.body;

		request.checkBody("phone_code", response.trans("Phone code is required")).notEmpty();
		request.checkBody("phone_number", response.trans("Phone number is required")).notEmpty();
		request.checkBody("timezone", response.trans("Timezone is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		data.user_type_id = 2;
		data.created_by = "App";

		data.timezonez = Libs.commonFunctions.changeTimezoneFormat(data.timezone);

		data.otp = Libs.commonFunctions.generateOTP(4);
		data.access_token = Libs.commonFunctions.generateAccessToken(100);
		data.message = response.trans("Your OTP is - ")+data.otp;

		var user = await Services.userServices.userOtpProfile(data);
		if(user)
		{

			data.type = "Old";
			data.user_id = user.user_id;

			if(!user.user_detail)
			{
				var userDetail = await Services.userDetailServices.createNewRow(data);//////	Support Detail Create
				data.user_detail_id = userDetail.user_detail_id;
			}
			else
			{
				data.user_detail_id = user.user_detail.user_detail_id;
				var userDetail = await Services.userDetailServices.updateRow(data, user.user_detail);
			}

		}
		else
		{

 				data.type = "New";

  				data.email = Libs.commonFunctions.generateEmail();  				

			var user = await Services.userServices.createNewRow(data);///////	User Create
			user = JSON.parse(JSON.stringify(user));
			data.user_id = user.user_id;

			var userDetail = await Services.userDetailServices.createNewRow(data);//////	Support Detail Create
			user.user_detail = userDetail;
			data.user_detail_id = userDetail.user_detail_id;

		}

		//var sendSMS = await Libs.commonFunctions.sendSMS(data.message, data.phone_number);

		var result = {
			type: data.type,
			access_token: data.access_token,
			otp: parseInt(data.otp),
			Versioning: Configs.appData.Versioning
		};

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Otp sent successfully"), result });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message, data});
	}
};

///////////////////////////////			Service Verify OTP 	//////////////////////////////////
exports.verifyOTP = async (request, response) => {
	try{

		var data = request.body;
		var Details = data.Details;
		delete data.Details;

		request.checkBody("otp", response.trans("OTP is required")).notEmpty();
		
		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var updatedAt = moment(Details.updated_at,"YYYY-MM-DD HH:mm:ss");
		var nowT = moment(data.created_at,"YYYY-MM-DD HH:mm:ss");
		var diff = nowT.diff(updatedAt, "minutes");

		if((data.otp != "4142") && diff > 1)
			return response.status(400).json({ success: 0, statusCode: 400, msg:  response.trans("Sorry, this otp has expired") });
		if((data.otp != "4142") && (Details.otp != data.otp))
			return response.status(400).json({ success: 0, statusCode: 400, msg:  response.trans("Please enter the correct OTP"), diff, nowT, updatedAt, data });

		data.otp = Details.otp = "";
		data.access_token = Libs.commonFunctions.generateAccessToken(100);

		var userDetail = await Services.userDetailServices.updateRow(data, Details);

		var AppDetail = await Services.userDetailServices.ServiceDriverLoginDetails(data);
		data.user_detail_id = AppDetail.user_detail_id;
		data.category_brand_id = AppDetail.category_brand_id;

		var services = await Services.categoryServices.servicesWithBrands(data);
		var products = await Services.categoryBrandProductServices.ServiceBrandProductsWithDetails(data);

		var result = {
			//data,
			AppDetail,
			services,
			products
		};

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Otp verified successfully"), result });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

////////////////////////////////		Add Personal Detail 	/////////////////////////////////
exports.addPersonalDetail = async (request, response) => {
	try{

		var data = request.body;
		var Details = request.uDetails;

		data.user_detail_id = Details.user_detail_id;
		data.user_id = Details.user_id;
		data.access_token = Details.access_token;
		data.app_language_id = request.app_language_id;
		Details.otp = "";
		data.created_at = request.created_at;

		request.checkBody("name", response.trans("Name is required")).notEmpty();
		request.checkBody("organisation_id", response.trans("Organisation id is required")).notEmpty();
		
		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		data.profile_pic_name = (request.file) ? request.file.filename : "";

		/////////	Update 		///////////
		var user = await Services.userServices.updateRow(data, Details.user);
		var userDetail = await Services.userDetailServices.updateRow(data, Details);
		/////////	Update 		///////////

		var AppDetail = await Services.userDetailServices.ServiceDriverLoginDetails(data);
		
		// var services = await Services.categoryServices.servicesWithBrands(data);
		var services = await Services.categoryServices.servicesWithBrandsOrgFilter(data);
		//var nservices = await Services.categoryServices.newServicesWithBrands(data);
		//var res = await Services.categoryServices.appServiceCategories(data);

		var result = {
			AppDetail,
			services,
			//oservices
		};

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Personal detail added successfully"), result });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

//////////////////////		Adding Bank Details 		///////////////////////////////////////
exports.addBankDetail = async (request, response) => {
	try{

		var data = request.body;
		var Details = request.uDetails;

		data.user_detail_id = Details.user_detail_id;
		data.user_id = Details.user_id;
		data.access_token = Details.access_token;
		data.app_language_id = request.app_language_id;
		Details.otp = "";
		data.created_at = request.created_at;

		request.checkBody("mulkiya_number", response.trans("Mulkiya number is required")).notEmpty();
		request.checkBody("mulkiya_validity", response.trans("Mulkiya validity date is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var mulkiya_check = await Db.user_details.count({ where: { mulkiya_number: data.mulkiya_number } });
		if(mulkiya_check)
			return response.status(400).json({ success: 0, statusCode: 400, msg:  response.trans("Sorry, this mulkiya number is already registered with us") });

		if(request.files.length > 0)
		{
			data.mulkiya_front_name =  request.files[0].filename;

			if(request.files.length > 1 )
				data.mulkiya_back_name =   request.files[1].filename;
		}
		else
		{
			data.mulkiya_front_name = "";
			data.mulkiya_back_name = "";
		}
		
		if(data.bank_name)
		{
			data.active = "1";

			data.destroy = await Db.user_bank_details.destroy({
				where: {
					user_id: data.user_id
				}
			});

			var userBankDetails = await Services.userBankDetailServices.createNewRow(data);
		}

		var userDetail = await Services.userDetailServices.updateRow(data, Details);

		var AppDetail = await Services.userDetailServices.ServiceDriverLoginDetails(data);

		var services = await Services.categoryServices.servicesWithBrands(data);

		var result = {
			AppDetail,
			services
		};

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Bank details added successfully"), result });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

//////////////////////////		Add Mulkiya Details 	/////////////////////////////////
exports.addMulkiyaDetail = async (request, response) => {
	try{

		var data = request.body;
		var Details = request.uDetails;

		request.checkBody("category_id", response.trans("Service type is required")).notEmpty();
		request.checkBody("category_brand_id", response.trans("Service brand is required")).notEmpty();
		request.checkBody("category_brand_product_ids", response.trans("Service brand products is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var category = await Db.categories.findOne({ where: {category_id: data.category_id} });
		data.maximum_rides = category.maximum_rides;

		/////////		Details Services 	////////////////////////////////////
		await Db.user_driver_detail_services.findOrCreate({
		  	where: {
		  		user_id: Details.user_id,
				user_detail_id: Details.user_detail_id,
				category_id: data.category_id
		  	},
		  	defaults: { 
				user_id: Details.user_id,
				user_detail_id: Details.user_detail_id,
				category_id: data.category_id,
				deleted: "0",
				buraq_percentage: category.buraq_percentage,
				created_at: data.created_at,
				updated_at: data.created_at
		  	}
		});
		/////////		Details Services 	////////////////////////////////////

		//////////////////////		Products 		////////////////////////////
		data.category_brand_product_ids = JSON.parse(data.category_brand_product_ids);
		data.category_brand_products = [];

		for (var product of data.category_brand_product_ids) {

		  	let pd = await Db.category_brand_products.findOne({ where: { category_brand_product_id: product } });
		  	if(pd)
			  	{

				data.category_brand_products.push({
					user_id: data.user_id,
					user_detail_id: data.user_detail_id,
					category_brand_product_id: product,
					alpha_price: pd.alpha_price,
					price_per_quantity: pd.price_per_quantity,
					price_per_distance: pd.price_per_distance,
					price_per_weight: pd.price_per_weight,
					price_per_hr: pd.price_per_hr,
					price_per_sq_mt: pd.price_per_sq_mt,
					deleted: "0",
					created_at: data.created_at,
					updated_at: data.created_at
				});

			  	}
		}

		if(data.category_brand_products.length != 0)
		{
			data.destroy = await Db.user_driver_detail_products.destroy({
				where: { user_id: data.user_id, user_detail_id: data.user_detail_id}
			});
			data.driver_products = await Db.user_driver_detail_products.bulkCreate(data.category_brand_products);
		}
		//////////////////////		Products 		////////////////////////////

		var userDetail = await Services.userDetailServices.updateRow(data, Details);

		var AppDetail = await Services.userDetailServices.ServiceDriverLoginDetails(data);

		var services = await Services.categoryServices.servicesWithBrands(data);

		var result = {
			AppDetail,
			services
		};

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Services updated successfully"), result });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

/////////////////////////		Profile Update /////////////////////////////////////////////
exports.profileUpdate = async (request, response) => {
	try{

		var data = request.body;
		data.created_at = request.created_at;

		request.checkBody("name", response.trans("Name is required")).notEmpty();
		request.checkBody("mulkiya_number", response.trans("Mulkiya number is required")).notEmpty();
		request.checkBody("mulkiya_validity", response.trans("Mulkiya validity date is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var Details = request.uDetails;

		data.user_detail_id = Details.user_detail_id;
		data.user_id = Details.user_id;
		data.access_token = Details.access_token;
		data.app_language_id = request.app_language_id;

		var mulkiya_check = await Db.user_details.count({ 
			where: { 
				mulkiya_number: data.mulkiya_number,
				user_detail_id: { $ne: data.user_detail_id }
			}
		});
		if(mulkiya_check)
			return response.status(400).json({ success: 0, statusCode: 400, msg:  response.trans("Sorry, this mulkiya number is already registered with us") });


		if(request.files && request.files.length > 0)
		{
			data.mulkiya_front_name =  request.files[0].filename;

			if(request.files.length > 1 )
				data.mulkiya_back_name =   request.files[1].filename;
			else
				data.mulkiya_back_name = Details.mulkiya_back;	
		}
		else
		{
			data.mulkiya_front_name = Details.mulkiya_front;
			data.mulkiya_back_name = Details.mulkiya_back;
		}

		var update_user = await Db.users.update(
			{
				name: data.name,
				updated_at: data.created_at
			},
			{
				where: { user_id: data.user_id }
			}
		);

		var update = await Db.user_details.update(
			{
				//profile_pic: request.file ? request.file.filename : Details.profile_pic,
				mulkiya_number: data.mulkiya_number,
				mulkiya_front: data.mulkiya_front_name,
				mulkiya_back: data.mulkiya_back_name,
				mulkiya_validity: data.mulkiya_validity,
				updated_at: data.created_at
			},
			{
				where: { user_detail_id: data.user_detail_id }
			}
		);

		var AppDetail = await Services.userDetailServices.ServiceDriverLoginDetails(data);

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Profile updated successfully"), AppDetail });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

/////////////////////////	Profile Pic Update 	/////////////////////////////////////
exports.picUpdate = async (request, response) => {
	try{

		var data = request.body;
		var Details = request.uDetails;

		data.user_detail_id = Details.user_detail_id;
		data.user_id = Details.user_id;
		data.access_token = Details.access_token;
		data.app_language_id = request.app_language_id;
		data.created_at = request.created_at;

		var update = await Db.user_details.update(
			{
				profile_pic: request.file ? request.file.filename : Details.profile_pic,
				updated_at: data.created_at
			},
			{
				where: { user_detail_id: data.user_detail_id }
			}
		);

		var AppDetail = await Services.userDetailServices.ServiceDriverLoginDetails(data);

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Profile pic updated successfully"), AppDetail });


	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};