//var moment = require("moment");

var Services = require(appRoot+"/services");///////////		Services
var Db = require(appRoot+"/models");//////////////////		Db Models
var Libs = require(appRoot + "/libs");//////////////////		Libraries
///////////////////		User History 	/////////////////////////////////////////////////
exports.driverHistory = async (request, response) => {
	try{

		var data = request.body;

		request.checkBody("type", response.trans("Type id is required")).notEmpty();
		request.checkBody("skip", response.trans("Skip parameter is required")).notEmpty();
		request.checkBody("take", response.trans("Take parameter is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});
		
		data.skip = parseInt(data.skip);
		data.take = parseInt(data.take);

		if(data.type == "1")
		{//////////		Past 	//////////////////////////
			data.order_status_array = ["CustCancel", "DriverCancel", "SerComplete", "SerCustCancel", "SerCustConfirm", "CTimeout"];
		}//////////		Past 	//////////////////////////
		else
		{//////////		Upcoming 	//////////////////////
			data.order_status_array = ["SerAccept", "Scheduled", "DPending", "DApproved", "Confirmed", "SerCustPending", "Ongoing"];
			//data.order_status_array = '("Scheduled", "DPending", "DApproved")';
		}//////////		Upcoming 	//////////////////////

		var orders = await Services.orderServices.driverPastUpcomingOListings(data.user_detail_id, data.skip, data.take, data.order_status_array, "Service",data);
		//var orders = await Services.orderServices.appRidesListings(data);

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Orders listing"), result: orders });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

//////////////////			Order Details 		///////////////////////////////////////////////
exports.driverOrderDetails = async (request, response) => {
	try{

		var data = request.body;

		request.checkBody("order_id", response.trans("Order id is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var orders = await Services.orderServices.driverCompleteRideDetails(data.order_id, data.app_language_id);
		if(orders.length == 0 )
			return response.status(400).json({ success: 0, statusCode: 400, msg:  response.trans("Sorry, this order is currently not available") });

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Order details"), order: orders[0] });
	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

/////////////////		Service Order Earnings 	////////////////////////////////////////////////
exports.driverEarnings = async (request, response) => {
	try{
		var data = request.body;

		request.checkBody("start_dt", response.trans("Start dt required")).notEmpty();
		request.checkBody("end_dt", response.trans("End dt is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var earnings = await Services.paymentServices.driverEarningHistory(data.user_detail_id, data.start_dt, data.end_dt, data.app_language_id);

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Driver earnings"), result: earnings, data });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

///////////////// M2 Part2		Add Sent Offer 	////////////////////////////////////////////////
exports.addReleaseOffer = async (request, response) => {
	try{
		var data      = request.body;
		var Details   = data.Details;
        var user_name = Details.user.name;
		request.checkBody("start_address", response.trans("Pickup address is required")).notEmpty();
		request.checkBody("end_address", response.trans("Dropoff address is required")).notEmpty();
		request.checkBody("offer_price", response.trans("Offer price is required")).notEmpty();
		request.checkBody("start_latitude", response.trans("Pickup latitude is required")).notEmpty();
		request.checkBody("start_longitude", response.trans("Pickup longitude is required")).notEmpty();
		request.checkBody("end_latitude", response.trans("Dropoff latitude is required")).notEmpty();
		request.checkBody("end_longitude", response.trans("Dropoff longitude is required")).notEmpty();
		request.checkBody("category_brand_product_id", response.trans("Category brand product is required")).notEmpty();
		request.checkBody("expire_date", response.trans("Offer Expire date is required")).notEmpty();
		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var result = await Services.driverListingServices.addReleaseOffer(data);
		return response.status(200).json({ success: 1, statusCode: 200, result: result, data });
	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

///////////////// M2 Part2		Add Sent Offer 	////////////////////////////////////////////////
exports.getDriverOffers = async (request, response) => {
	try{
		var data    = request.body;
        var Details = data.Details;
		var DriverId = Details.user_id;
		//var SentOfferDetails = await Db.release_offers.findAll({ where: {driver_id: DriverId},order: [["release_offer_id", "DESC"]]});
		var SentOfferDetails = await Services.driverListingServices.getDriverOffers(DriverId,data.app_language_id);
        return response.status(200).json({success: 1, statusCode: 200,result: SentOfferDetails,Details});
	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

///////////////// M2 Part2		Get Driver Product List 	////////////////////////////////////////////////
exports.getDriverProductList = async (request, response) => {
	try{
		var data    = request.body;
        var Details = data.Details;
		var DriverId = Details.user_id;
		var ProductList = await Services.driverListingServices.getDriverProductList(DriverId,data.app_language_id);
        return response.status(200).json({success: 1, statusCode: 200,result: ProductList,Details});
	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

///////////////// M2 Part2		Delete Sent Offer 	////////////////////////////////////////////////
exports.deleteDriverOffer = async (request, response) => {
	try{
		var data    = request.body;
        var Details = data.Details;
		var OfferId = data.release_offer_id;
		var SentOfferDetails = await Db.release_offers.destroy({ where: {release_offer_id: OfferId}});
        return response.status(200).json({success: 1, statusCode: 200, msg: "Offfer Delete Successfully",result: Details});
	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

///////////////// M2 Part2		Delete Sent Offer 	////////////////////////////////////////////////
exports.SendOffertoCustomer = async (request, response) => {
	try{
		var req              = request.body;
		var data             = Array();
		data.start_latitude  = req.latitude;
		data.start_longitude = req.longitude;
		var Customers = await Services.driverListingServices.getNearByCustomer(data);
		console.log("Customers",Customers);
		var push_data = {
			type: "SerReleaseOffer", //SerTimeout
			message: "Driver has released a new offer."
		};
		var push = Libs.commonFunctions.SendMultipleNotificationtousers(Customers, push_data);
		return response.status(200).json({success: 0, statusCode: 200});
	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};
