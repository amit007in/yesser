var moment = require("moment");

var Libs = require(appRoot+"/libs");//////////////////		Libraries
var Services = require(appRoot+"/services");///////////		Services
var Db = require(appRoot+"/models");//////////////////		Db Models

//////////////////		Accept Request 	//////////////////////////
exports.acceptRequest = async (request, response) => {
	try{

		var data = request.body;
		var Details = data.Details;
		delete data.Details;

		request.checkBody("order_id", response.trans("Order id is required")).notEmpty();
		request.checkBody("latitude", response.trans("Latitude is required")).notEmpty();
		request.checkBody("longitude", response.trans("Longitude is required")).notEmpty();
                
		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var order = await Services.orderServices.driverOrderBasicDetails(data);
                
		if(!order  || !order.CRequest || order.driver_user_detail_id != 0)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not available"), result: order });
		else if(order.organisation_coupon_user_id != 0)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this facility is only available for non etoken orders"), result: order });
		else if((order.order_status == "CustCancel") || (order.order_status == "SerCancel") || (order.order_status == "SupCancel") || (order.order_status == "SerReject") || (order.order_status == "SerTimeout") || (order.order_status == "DSchCancelled") || (order.order_status == "DSchTimeout") || (order.order_status == "SysSchCancelled") )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already cancelled"), result: order });
		else if((order.order_status == "Ongoing") || (order.order_status == "Confirmed") || (order.order_status == "Scheduled") )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is no longer pending"), result: order });
		else if(order.order_status != "Searching")
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is no longer pending"), result: order });

		data.update_order = await Db.orders.update(
			{
				order_status:  (order.future == "0") ? "Confirmed" : "Scheduled",
				driver_user_id: order.CRequest.driver_user_id,
				driver_user_detail_id: order.CRequest.driver_user_detail_id,
				driver_organisation_id: order.CRequest.driver_organisation_id,
				updated_at: data.created_at
			},
			{
				where: { order_id: data.order_id, order_status: "Searching" }
			}
		);

		data.current_order_request = await Db.order_requests.update(
			{
				order_request_status: "Accepted",
				driver_current_latitude: data.latitude,
				driver_current_longitude: data.longitude,
				accepted_at: data.created_at,
				driver_confirmed_latitude: data.latitude,
				driver_confirmed_longitude: data.longitude,
				updated_at: data.created_at
			},
			{
				where: {
					order_request_id: order.CRequest.order_request_id
				}
			}
		);

		data.other_order_request = await Db.order_requests.update(
			{
				order_request_status: "SerOAccept",
				updated_at: data.created_at
			},
			{
				where: {
					order_request_status: "Searching",
					driver_user_detail_id: {$ne: order.CRequest.driver_user_detail_id }
				}
			}
		);

		///////////		Order Time 	//////////////////////

		data.update_payment = await Db.payments.update(
			{
				seller_user_id: Details.user_id,
				seller_user_detail_id: Details.user_detail_id,
				seller_user_type_id: Details.organisation_id == 0 ? 4 : 2,
				seller_organisation_id: Details.organisation_id,
		                updated_at: data.created_at
			},
			{
				where: { order_id: data.order_id }
			}
		);
                var customer_id = order.customer_user_id;
                var Coupon_id   = order.payment.coupon_id;
                if (Coupon_id != '0') {
                    var max_rides = await Services.userServices.checkPromoCodeMaxRides(Coupon_id);
                    var max_ridess = JSON.parse(JSON.stringify(max_rides));
                    //console.log("max_rides==============",max_ridess.max_rides);
                    var OldRides = max_ridess.max_rides;
                    if (OldRides > 0) {
                        var NewRides = parseInt(OldRides) - 1;
                        console.log("max_rides==============", NewRides);
                        await Services.userServices.PromoCodeMaxRidesUpdate(Coupon_id,customer_id, NewRides);
                    }
                }
                //console.log("accept order ==", order.customer_user_id);
                //console.log("accept order ==", order.payment.coupon_id);
		///////////////////		Update 	Db 	/////////////////////////////

		///////////////////////////////////////////////////
		////////////////////		Customer Push Socket
		const message = Libs.commonFunctions.generate_lang_message(" has accepted your service", order.CustomerDetails.language_id);

		////////////////		Socket Event 	///////////////////////////
		var temp_order = await Services.orderServices.driverAcceptOrderDetails(data, order.CustomerDetails.language_id);

		data.socket_data = {
			type: "SerAccept",
			order: temp_order,
			message: Details.user.name+message
		};

		Libs.commonFunctions.emitToDevice("OrderEvent", order.CustomerDetails.user_detail_id, data.socket_data );
		////////////////		Socket Event 	///////////////////////////

		////////////////		Push Notifications 	///////////////////////
		if(order.CustomerDetails.fcm_id != "" && order.CustomerDetails.notifications == "1")
		{

			data.push_data = {
				order_id: (data.order_id).toString(),
				type: "SerAccept",
				driver_user_id: (order.CRequest.driver_user_id).toString(),
				driver_user_detail_id: (order.CRequest.driver_user_detail_id).toString(),
				message: Details.user.name+message
			};					
			var push = Libs.commonFunctions.latestSendPushNotification([order.CustomerDetails], data.push_data);

		}
		////////////////		Push Notifications 	///////////////////////

		////////////////////		Customer Push Socket
		///////////////////////////////////////////////////

		///////////////////////////////////////////////////
		////////////////////		Other Drivers 
		if(order.ORequests.length != 0)
		{

			data.ofcm_ids = [];
			data.osocket_ids = [];
			data.oorder_request_ids = [];
			order.ORequests.forEach(function(odriver) {

				data.osocket_ids.push(odriver.driver_user_detail_id);

				data.oorder_request_ids.push(odriver.order_request_id);

			});

			////////////////		Socket Event 	///////////////////////////
			data.socket_data = {
				type: "SerOAccept",
				order: {
					order_id: order.order_id,
					driver_user_detail_id: order.driver_user_detail_id,
					user: {
						user_id: order.customer_user_id,
						user_detail_id: order.customer_user_detail_id
					}
				}
			};

			var event = Libs.commonFunctions.emitDynamicEvent("OrderEvent", data.osocket_ids, data.socket_data );

			////////////////		Socket Event 	///////////////////////////

		}
		////////////////////		Other Drivers
		///////////////////////////////////////////////////

		///////////////////////////////////////////////////
		//////////////////////			SMS Service 		////////////////////////////////////
		///////////////////////////////////////////////////

		/////////////////	Customer 	////////////////////////
		// var cust_sms = Libs.commonFunctions.generate_lang_message("Your order has been confirmed by the driver. Order Id - ", order.CustomerDetails.language_id)+order.order_token;

		// var sendSMS = Libs.commonFunctions.sendSMS(cust_sms, order.Customer.phone_number.toString());
		/////////////////	Customer 	////////////////////////

		/////////////////	Driver 	////////////////////////
		// var driver_sms = response.trans('Your order has been confirmed for Order Id - ')+order.order_token;

		// sendSMS = Libs.commonFunctions.sendSMS(driver_sms, Details.user.phone_number.toString());
		/////////////////	Driver 	////////////////////////

		///////////////////////////////////////////////////
		//////////////////////			SMS Service 		////////////////////////////////////
		///////////////////////////////////////////////////

		var user = order.user;
		var brand = order.brand;

		var order = await Services.orderServices.driverOrderDetails(data);
		order = JSON.parse(JSON.stringify(order));

		if(order.CustomerDetails.profile_pic == "")
			order.CustomerDetails.profile_pic_url = "";
		else
			order.CustomerDetails.profile_pic_url = process.env.RESIZE_URL+order.CustomerDetails.profile_pic;

		order.brand = brand;
		order.user= user;

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Order accepted successfully"), result: order });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

////////////////////////		Reject Request 		///////////////////////////////////////
exports.rejectRequest = async (request, response) => {
	try{

		var data = request.body;

		request.checkBody("order_id", response.trans("Order id is required")).notEmpty();
		request.checkBody("latitude", response.trans("Latitude is required")).notEmpty();
		request.checkBody("longitude", response.trans("Longitude is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var order = await Services.orderServices.driverOrderBasicDetails(data);
		if(!order  || !order.CRequest )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not available"), result: order });
		else if(order.organisation_coupon_user_id != 0)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this facility is only available for non etoken orders"), result: order });
		else if((order.order_status == "CustCancel") || (order.order_status == "SerCancel") || (order.order_status == "SupCancel") || (order.order_status == "SerReject") || (order.order_status == "SerTimeout") || (order.order_status == "DSchCancelled") || (order.order_status == "DSchTimeout") || (order.order_status == "SysSchCancelled") )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already cancelled"), result: order });
		else if((order.order_status == "Ongoing") || (order.order_status == "Confirmed")|| (order.order_status == "Scheduled") )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is no longer pending"), result: order });
		else if(order.order_status != "Searching")
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is no longer pending"), result: order });

		order = JSON.parse(JSON.stringify(order));

		//////////////	Update Current Driver Db 	//////////////////////////////
		data.current_order_request = await Db.order_requests.update(
			{
				order_request_status: "SerReject",
				driver_confirmed_latitude: data.latitude,
				driver_confirmed_longitude: data.longitude,
				updated_at: data.created_at
			},
			{
				where: {
					order_request_id: order.CRequest.order_request_id
				}
			}
		);
		//////////////	Update Current Driver Db 	//////////////////////////////

		///////////////////////////////////////////////////
		////////////////////		Last Driver Rejection 
		if(order.ORequests.length == 0)
		{

			data.order_request_status = "SerReject";

			data.update_order = await Db.orders.update(
				{
					order_status: data.order_request_status,
					cancelled_by: data.order_request_status,
					updated_at: data.created_at
				},
				{
					where: { order_id: data.order_id }
				}
			);

			data.update_payment = await Db.payments.update(
				{
					payment_status: data.order_request_status,
					updated_at: data.created_at
				},
				{
					where: {order_id: data.order_id}
				}
			);

			///////////////////////////////////////////////////
			////////////////////		Customer Push Socket

			////////////////		Socket Event 	///////////////////////////
			data.socket_data = {
				type: data.order_request_status,
				order: {
					order_id: order.order_id,
					driver_user_detail_id: order.driver_user_detail_id,
				}
			};

			var event = Libs.commonFunctions.emitToDevice("OrderEvent", order.CustomerDetails.user_detail_id, data.socket_data);
			////////////////		Socket Event 	///////////////////////////

			////////////////		Push Notifications 	///////////////////////
			if(order.CustomerDetails.fcm_id != "" && order.CustomerDetails.notifications == "1")
			{

				var message = Libs.commonFunctions.generate_lang_message("Sorry, no driver is currently available", order.CustomerDetails.language_id);
		
				data.push_data = {
					order_id: (data.order_id).toString(),
					type: data.order_request_status,//SerReject
					message: message
				};

				var push = Libs.commonFunctions.latestSendPushNotification([order.CustomerDetails], data.push_data);
		
			}
			////////////////		Push Notifications 	///////////////////////

			////////////////////		Customer Push Socket
			///////////////////////////////////////////////////


			//////////////////	Coupons or Etokens 	////////////////////////////
			if(order.coupon_user)
			{

				await Db.coupon_users.update(
					{
						rides_left: order.coupon_user.rides_left+1,
						updated_at: data.created_at
					},
					{
						where: {coupon_user_id: order.coupon_user_id}
					}
				);

			}
			// else if(order.organisation_coupon_user)
			// 	{

			// 		await Db.organisation_coupon_users.update(
			// 					{
			// 						quantity_left: order.organisation_coupon_user.quantity_left+order.payment.product_quantity,
			// 						updated_at: data.created_at
			// 					},
			// 					{
			// 						where: {organisation_coupon_user_id: order.organisation_coupon_user_id}
			// 					}
			// 		);

			// 	}
			//////////////////	Coupons or Etokens 	////////////////////////////

			// if(process.env.NODE_ENV != 'development')
			// 	var sendSMS = Libs.commonFunctions.multipleSendSMS(data.sms_message+", Status: Timeout", Configs.sms_numbers);

		}
		////////////////////		Last Driver Rejection 
		///////////////////////////////////////////////////

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Order rejected successfully"), order });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

/////////////////////		Ongoing 		///////////////////////////////////////////
exports.servicesOngoing = async (request, response) => {
	try{

		var data = request.body;
		//console.log("sadsdfsfds",data);
		var currentOrders = await Services.orderServices.driverCurrentOrders(data);
		var pendingOrders = await Services.orderServices.driverPendingOrders(data);

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Ongoing orders"), result: currentOrders, pendingOrders });

	}
	catch(e)
	{
		//console.log("errrr",e);
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

////////////////////////	Driver Ends Ride 		///////////////////////////////////
exports.startRequest = async (request, response) => {
	try{
		var data = request.body;
		var Details = data.Details;
		delete data.Details;

		request.checkBody("order_id", response.trans("Order id is required")).notEmpty();
		request.checkBody("latitude", response.trans("Latitude is required")).notEmpty();
		request.checkBody("longitude", response.trans("Longitude is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		//////////////////		Truck Check 		////////////////////////////////////
		if(Details.category_id == 4){
			var oCheck = await Db.orders.findOne({ where: {order_id: {$ne: data.order_id}, driver_user_detail_id: Details.user_detail_id, order_status: { $in: ["DApproved", "DPending", "Ongoing", "Confirmed"] } } });
			if(oCheck)
			   return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, you already have an ongoing order"), result: oCheck });
		}
		//////////////////		Truck Check 		////////////////////////////////////

		/////////////		Order Status Check 	//////////////////////
		var order = await Services.orderServices.driverOrderBasicDetails(data);
		if(!order  || !order.CRequest )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not available"), result: order });
		else if(order.driver_user_detail_id != Details.user_detail_id)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order do not belong to you"), result: order });
		else if(order.order_status == "SerComplete" || order.order_status == "SerCustConfirm")
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already completed"), result: order });
		else if((order.order_status == "CustCancel") || (order.order_status == "SerCancel") || (order.order_status == "SupCancel") || (order.order_status == "SerReject") || (order.order_status == "SerTimeout") || (order.order_status == "DSchCancelled") || (order.order_status == "DSchTimeout") || (order.order_status == "SysSchCancelled") )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already cancelled"), result: order });
		else if( (order.order_status != "Confirmed") && (order.order_status != "DApproved") )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is not confirmed"), result: order });
		/////////////		Order Status Check 	//////////////////////

		order = JSON.parse(JSON.stringify(order));

		var oOrderCheck = await Db.orders.findOne({ where: { driver_user_detail_id: Details.user_detail_id, order_status: "Ongoing" } });
		if(oOrderCheck)
			var my_turn = "0";
		else
			var my_turn = "1";

		order.CRequest.full_track = JSON.parse(order.CRequest.full_track);
		order.CRequest.full_track.push({"Dt":data.created_at, "latitude": parseFloat(data.latitude), "longitude": parseFloat(data.longitude)});

		data.order_status = "Ongoing";

		///////////////////////		DB update 	////////////////////////////
		data.update_order_request = await Db.order_requests.update(
			{
				//order_request_status: data.order_request_status,
				driver_current_latitude: data.latitude,
				driver_current_longitude: data.longitude,
				driver_started_latitude: data.driver_started_latitude,
				driver_started_longitude: data.driver_started_longitude,
				full_track: JSON.stringify(order.CRequest.full_track),
				started_at: data.created_at,
				updated_at: data.created_at
			},
			{
				where: {
					order_request_id: order.CRequest.order_request_id
				}
			}
		);

		data.update_order = await Db.orders.update(
			{
				order_status: data.order_status,
				my_turn: my_turn,
				updated_at: data.created_at
			},
			{
				where: {
					order_id: order.order_id
				}
			}
		);
		//////////////////////		DB update 	////////////////////////////

		///////////////////////////////////////////////////
		////////////////////		Customer Push Socket

		////////////////		Socket Event 	///////////////////////////
		var temp_order = await Services.orderServices.driverAcceptOrderDetails(data, order.CustomerDetails.language_id);
                
		data.socket_data = {
			type: data.order_status,
			order: temp_order
		};
                console.log("order event 1==", data.socket_data);
		var event = await Libs.commonFunctions.emitDynamicEvent("OrderEvent", [order.CustomerDetails.user_detail_id], data.socket_data);
		////////////////		Socket Event 	///////////////////////////

		////////////////		Push Notifications 	///////////////////////
		if(order.CustomerDetails.fcm_id != ""&& order.CustomerDetails.notifications == "1"){
			var message = Libs.commonFunctions.generate_lang_message(" has started your service", order.CustomerDetails.language_id);
		
			data.push_data = {
				order_id: (data.order_id).toString(),
				type: data.order_status,//Ongoing
				message: Details.user.name+message
			};
			var push = Libs.commonFunctions.latestSendPushNotification([order.CustomerDetails], data.push_data);
		}
		////////////////		Push Notifications 	///////////////////////

		////////////////////		Customer Push Socket
		///////////////////////////////////////////////////

		var order = await Services.orderServices.driverOrderDetails(data);
		if(order.CustomerDetails.profile_pic == "")
			order.CustomerDetails.profile_pic_url = "";
		else
			order.CustomerDetails.profile_pic_url = process.env.RESIZE_URL+order.CustomerDetails.profile_pic;

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Order started successfully"), result: order });
	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};
////////////////////////	Change Turn 	///////////////////////////////////////////
exports.driverOrderChangeTurn = async (request, response) => {
	try{

		var data = request.body;
		var Details = data.Details;
		delete data.Details;

		request.checkBody("order_id", response.trans("Order id is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		/////////////		Order Status Check 	//////////////////////
		var order = await Services.orderServices.driverOrderBasicDetails(data);
		if(!order  || !order.CRequest )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not available"), result: order });
		else if(order.driver_user_detail_id != Details.user_detail_id)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order do not belong to you"), result: order });
		else if(order.order_status == "SerComplete" || order.order_status == "SerCustConfirm")
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already completed"), result: order });
		else if((order.order_status == "CustCancel") || (order.order_status == "SerCancel") || (order.order_status == "SupCancel") || (order.order_status == "SerReject") || (order.order_status == "SerTimeout") || (order.order_status == "DSchCancelled") || (order.order_status == "DSchTimeout") || (order.order_status == "SysSchCancelled") )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already cancelled"), result: order });
		else if( (order.order_status != "Ongoing") )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not ongoing"), result: order });
		/////////////		Order Status Check 	//////////////////////

		order = JSON.parse(JSON.stringify(order));

		var update_orides = await Db.orders.update(
			{
				my_turn: "0",
				updated_at: data.created_at
			},
			{
				where: {
					order_id: {
						$ne: data.order_id
					},
					driver_user_detail_id: Details.user_detail_id,
					order_status: "Ongoing"
				}
			}
		);

		var update_orides = await Db.orders.update(
			{
				my_turn: "1",
				updated_at: data.created_at
			},
			{
				where: {
					order_id: data.order_id
				}
			}
		);

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Order updated successfully") });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

////////////////////////	Driver Ends Ride 		///////////////////////////////////
exports.endRequest = async (request, response) => {
	try{
		var data = request.body;
		var Details = data.Details;
		delete data.Details;

		request.checkBody("order_id", response.trans("Order id is required")).notEmpty();
		request.checkBody("latitude", response.trans("Latitude is required")).notEmpty();
		request.checkBody("longitude", response.trans("Longitude is required")).notEmpty();
		request.checkBody("payment_type", response.trans("Payment type is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		/////////////		Order Status Check 	//////////////////////
		var order = await Services.orderServices.driverOrderBasicDetails(data);
		if(!order  || !order.CRequest)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not available"), result: order });
		else if(order.organisation_coupon_user_id != 0)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this facility is only available for non etoken orders"), result: order });
		else if(order.driver_user_detail_id != Details.user_detail_id)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order do not belong to you"), result: order });
		else if(order.order_status == "SerComplete" || order.order_status == "SerCustConfirm")
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already completed"), result: order });
		else if((order.order_status == "CustCancel") || (order.order_status == "SerCancel") || (order.order_status == "SupCancel") || (order.order_status == "SerReject") || (order.order_status == "SerTimeout") || (order.order_status == "DSchCancelled") || (order.order_status == "DSchTimeout") || (order.order_status == "SysSchCancelled") )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already cancelled"), result: order });
		else if(order.order_status != "Ongoing")
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not ongoing"), result: order });
		/////////////		Order Status Check 	//////////////////////

		order = JSON.parse(JSON.stringify(order));

		data.payment_status = data.order_status = data.order_request_status = "SerComplete";

		///////////		Order Time 	//////////////////////
		data.order_time = 0;

		var a = moment(data.created_at, "YYYY-MM-DD HH:mm:ss");
		var b = moment(order.updated_at, "YYYY-MM-DD HH:mm:ss");
		data.order_time = a.diff(b, "hours");
		///////////		Order Time 	//////////////////////

		data.coupon_charge = 0;

		if(!data.order_distance)
			data.order_distance = parseFloat(order.payment.order_distance);

		// data.initial_charge = parseFloat(prod.alpha_price) + (parseFloat(prod.price_per_quantity)*parseFloat(order.payment.product_quantity)) + (parseFloat(prod.price_per_distance)*parseFloat(data.order_distance)) + (parseFloat(prod.price_per_hr)*parseFloat(data.order_time)) + (parseFloat(prod.price_per_sq_mt)*parseFloat(order.payment.product_sq_mt)) + (data.bottle_charge);

		data.final_charge = order.payment.final_charge;
		// if(order.coupon_user)
		// 	{
		// 		if(order.coupon_user.coupon_type == "Percentage")
		// 			{
		// 				data.coupon_charge = parseFloat(((data.initial_charge*parseFloat(order.coupon_user.coupon_value))/100).toFixed(2));
		// 			}
		// 		else
		// 			{
		// 				data.coupon_charge = parseFloat(order.coupon_user.coupon_value);
		// 			}

		// 		data.final_charge = (data.initial_charge - data.coupon_charge) < 0 ? 0 : (data.initial_charge - data.coupon_charge);
		// 	}
		// else
		// 	{
		// 		data.final_charge = data.initial_charge;
		// 	}



		// if(data.payment_type == "Card" && data.final_charge > 0)
		// 	{
		// 		order.payment.user_card_id = order.Customer.Default.user_card_id;
		// 		if(!order.Customer.Default)
		// 			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, Please ask the customer to add a defualt card first for card payment") });

		// 		data.metadata = {
		// 	  		order_id: order.order_id,
		// 	  		customer_user_id: order.customer_user_id,
		// 	  		customer_user_detail_id: order.customer_user_detail_id,
		// 	  		customer_organisation_id: order.customer_organisation_id,
		// 	  		user_card_id: order.payment.user_card_id,
		// 	  		driver_user_id: order.driver_user_id,
		// 	  		driver_user_detail_id: order.driver_user_detail_id,
		// 	  		driver_organisation_id: order.driver_organisation_id,
		// 	  		driver_user_type_id: order.driver_user_type_id,
		// 	  		category_id: order.category_id,
		// 	  		category_brand_id: order.category_brand_id,
		// 	  		category_brand_product_id: order.category_brand_product_id,
		// 	  		organisation_coupon_user_id: order.organisation_coupon_user_id,
		// 	  		coupon_user_id: order.coupon_user_id
		// 		}

		// 		data.payment = await Libs.stripeFunctions.makePayment(data.final_charge, data.description, data.metadata, order.Customer.stripe_customer_id);///	Stripe Pay
		// 		data.transaction_id = data.payment.id;
		// 	}
		// else
		// 	{
		data.transaction_id = "P-"+Libs.commonFunctions.uniqueId();
		order.payment.user_card_id = 0;
		// }

		//////////////////////		Buraq Percentage 		////////////////////////

		// 	data.buraq_percentage = parseFloat(percent.buraq_percentage);
		// //////////////////////		Buraq Percentage 		////////////////////////

		// 	data.admin_charge = parseFloat(((data.buraq_percentage*data.final_charge)/100).toFixed(3));
		// 	data.final_charge = parseFloat((data.final_charge+data.admin_charge).toFixed(3));

		///////////////////////		DB update 	////////////////////////////
		order.CRequest.full_track = JSON.parse(order.CRequest.full_track);
		order.CRequest.full_track.push({"Dt":data.created_at, "latitude": parseFloat(data.latitude), "longitude": parseFloat(data.longitude)});

		data.update_order_request = await Db.order_requests.update(
			{
				driver_current_latitude: data.latitude,
				driver_current_longitude: data.longitude,
				full_track: JSON.stringify(order.CRequest.full_track),
				updated_at: data.created_at
			},
			{
				where: {
					order_request_id: order.CRequest.order_request_id
				}
			}
		);

		data.update_order = await Db.orders.update(
			{
				order_status: data.order_status,
				updated_at: data.created_at
			},
			{
				where: {
					order_id: order.order_id
				}
			}
		);

		data.update_payment = await Db.payments.update(
			{
				user_card_id: order.payment.user_card_id,
				payment_type: data.payment_type,
				payment_status: "Completed",
				transaction_id: data.transaction_id,
				//buraq_percentage: data.buraq_percentage,
				order_distance: data.order_distance,
				order_time: data.order_time,
				updated_at: data.created_at
				// product_alpha_charge: prod.alpha_price,
				// product_per_quantity_charge: prod.price_per_quantity,
				// product_per_weight_charge: prod.price_per_weight,
				// product_per_distance_charge: prod.price_per_distance,
				// product_per_hr_charge: prod.price_per_hr,
				// product_per_sq_mt_charge: prod.price_per_sq_mt,

				// initial_charge: data.initial_charge,
				// admin_charge: data.admin_charge,
				// bank_charge: 0,
				// final_charge: data.final_charge,

				//bottle_charge: data.bottle_charge
			},
			{
				where: { payment_id: order.payment.payment_id }
			}
		);
		//////////////////////		DB update 	////////////////////////////

		///////////////////////////////////////////////////
		////////////////////		Customer Push Socket

		////////////////		Socket Event 	///////////////////////////
		var temp_order = await Services.orderServices.driverAcceptOrderDetails(data, order.CustomerDetails.language_id);
               
		data.socket_data = {
			type: data.order_request_status,
			order: temp_order
		};
                console.log("order event 2==", data.socket_data);
		var event = await Libs.commonFunctions.emitDynamicEvent("OrderEvent", [order.CustomerDetails.user_detail_id], data.socket_data);
		////////////////		Socket Event 	///////////////////////////

		////////////////		Push Notifications 	///////////////////////
		if(order.CustomerDetails.fcm_id != ""&& order.CustomerDetails.notifications == "1")
		{

			var message = Libs.commonFunctions.generate_lang_message(" has completed your order", order.CustomerDetails.language_id);
		
			data.push_data = {
				order_id: (data.order_id).toString(),
				type: data.order_request_status,//SerComplete
				message: Details.user.name+message
			};
                        console.log("push_data 3==", data.push_data);
			var push = Libs.commonFunctions.latestSendPushNotification([order.CustomerDetails], data.push_data);
		
		}
		////////////////		Push Notifications 	///////////////////////

		////////////////////		Customer Push Socket
		///////////////////////////////////////////////////

		var brand = order.brand;

		var order = await Services.orderServices.driverOrderDetails(data);
		order = JSON.parse(JSON.stringify(order));
		if(order.CustomerDetails.profile_pic == "")
			order.CustomerDetails.profile_pic_url = "";
		else
			order.CustomerDetails.profile_pic_url = process.env.RESIZE_URL+order.CustomerDetails.profile_pic;

		order.brand = brand;

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Order completed successfully"), result: order });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message, data});
	}
};

/////////////////////	Driver Cancel Ride 		////////////////////////////////////////
exports.CancelRequest = async (request, response) => {
	try{

		var data = request.body;
		var Details = data.Details;
		delete data.Details;

		request.checkBody("order_id", response.trans("Order id is required")).notEmpty();
		request.checkBody("cancel_reason", response.trans("Cancel reason is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		/////////////		Order Status Check 	//////////////////////
		var order = await Services.orderServices.driverOrder4Cancellations(data);
		if(!order  || !order.CRequest)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not available"), result: order });
		else if(order.driver_user_detail_id != Details.user_detail_id)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order do not belong to you"), result: order });
		else if(order.order_status == "SerComplete" || order.order_status == "SerCustConfirm")
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already completed"), result: order });
		else if((order.order_status == "CustCancel") || (order.order_status == "SerCancel") || (order.order_status == "SupCancel") || (order.order_status == "SerReject") || (order.order_status == "SerTimeout") || (order.order_status == "DSchCancelled") || (order.order_status == "DSchTimeout") || (order.order_status == "SysSchCancelled") )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already cancelled"), result: order });
		else if(order.order_status == "Ongoing")
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already started"), result: order });
		else if(order.order_status != "Confirmed" && order.order_status != "Scheduled" && order.order_status != "DPending" && order.order_status != "DApproved")
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, only accepted and scheduled rides can be cancelled by driver"), result: order });
		/////////////		Order Status Check 	//////////////////////

		data.order_request_status = "DriverCancel";
		data.cancelled_by = "Driver";

		////////////////	Update DB 		///////////////////////////////
		order.CRequest.full_track = JSON.parse(order.CRequest.full_track);
		order.CRequest.full_track.push({"Dt":data.created_at, "latitude": Details.latitude, "longitude": Details.longitude});

		data.update_order_request = await Db.order_requests.update(
			{
				driver_current_latitude: data.latitude,
				driver_current_longitude: data.longitude,
				full_track: JSON.stringify(order.CRequest.full_track),
				updated_at: data.created_at
			},
			{
				where: {
					order_request_id: order.CRequest.order_request_id
				}
			}
		);

		data.update_order = await Db.orders.update(
			{
				order_status: data.order_request_status,
				cancel_reason: data.cancel_reason,
				cancelled_by: data.cancelled_by,
				updated_at: data.created_at
			},
			{
				where: { order_id: data.order_id }
			}
		);

		data.update_payment = await Db.payments.update(
			{
				payment_status: data.order_request_status,
				updated_at: data.created_at
			},
			{
				where: { order_id: data.order_id }
			}
		);
		////////////////	Update DB 		///////////////////////////////

		///////////////////////////////////////////////////
		////////////////////		Customer Push Socket

		////////////////		Socket Event 	///////////////////////////
		var temp_order = await Services.orderServices.driverAcceptOrderDetails(data, order.CustomerDetails.language_id);
                
		data.socket_data = {
			type: data.order_request_status,
			order: temp_order
		};
                console.log("order event 3==", data.socket_data);
		var event = await Libs.commonFunctions.emitDynamicEvent("OrderEvent", [order.CustomerDetails.user_detail_id], data.socket_data);
		////////////////		Socket Event 	///////////////////////////

		////////////////		Push Notifications 	///////////////////////
		if(order.CustomerDetails.fcm_id != ""&& order.CustomerDetails.notifications == "1")
		{
			var message = Libs.commonFunctions.generate_lang_message(" has cancelled your order", order.CustomerDetails.language_id);

			data.push_data = {
				order_id: (data.order_id).toString(),
				type: data.order_request_status,//DriverCancel
				message: Details.user.name+message
			};

			var push = Libs.commonFunctions.latestSendPushNotification([order.CustomerDetails], data.push_data);
		
		}
		////////////////		Push Notifications 	///////////////////////

		////////////////////		Customer Push Socket
		///////////////////////////////////////////////////

		//////////////////	Coupons or Etokens 	////////////////////////////
		if(order.coupon_user)
		{

			await Db.coupon_users.update(
				{
					rides_left: order.coupon_user.rides_left+1,
					updated_at: data.created_at
				},
				{
					where: {coupon_user_id: order.coupon_user_id}
				}
			);

		}
		// else if(order.organisation_coupon_user)
		// 	{

		// 		await Db.organisation_coupon_users.update(
		// 					{
		// 						quantity_left: order.organisation_coupon_user.quantity_left+order.payment.product_quantity,
		// 						updated_at: data.created_at
		// 					},
		// 					{
		// 						where: {organisation_coupon_user_id: order.organisation_coupon_user_id}
		// 					}
		// 		);

		// 	}
		//////////////////	Coupons or Etokens 	////////////////////////////

		var order = await Services.orderServices.driverOrder4Cancellations(data);

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Order cancelled successfully"), result: order });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};
/////////////////////	Driver Rate a Order 	////////////////////////////////////////
exports.driverServiceRate = async (request, response) => {
	try{
		var data = request.body;
		var Details = data.Details;
		delete data.Details;

		request.checkBody("order_id", response.trans("Order id is required")).notEmpty();
		request.checkBody("ratings", response.trans("Rating is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var order = await Services.orderServices.driverOrder4Push(data);
		if(!order)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not available"), result: order });
		else if(order.driver_user_detail_id != Details.user_detail_id)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order do not belong to you"), result: order });
		else if((order.order_status == "CustCancel") || (order.order_status == "SerCancel") || (order.order_status == "SupCancel") || (order.order_status == "SerReject") || (order.order_status == "SerTimeout") || (order.order_status == "DSchCancelled") || (order.order_status == "DSchTimeout") || (order.order_status == "SysSchCancelled") )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already cancelled"), result: order });
		// else if( ((order.order_status != 'SupComplete') || (order.order_status != 'SerCustConfirm')) && order.order_type == 'Service')
		else if(order.order_type == "Service" &&  !( (order.order_status == "SerCustConfirm") || (order.order_status == "SerComplete") ) )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is not completed yet"), result: order });
		else if(order.order_status != "SupComplete" && order.order_type == "Support")
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is not completed yet"), result: order });
		else if(order.DriverRatings)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, you have already rated this order"), result: order });

		order.DriverRatings = await Db.order_ratings.create({
			order_id: data.order_id,
			customer_user_id: order.customer_user_id,
			customer_user_detail_id: order.customer_user_detail_id,
			customer_organisation_id: order.customer_organisation_id,
			driver_user_id: order.driver_user_id,
			driver_user_detail_id: order.driver_user_detail_id,
			driver_organisation_id: order.driver_organisation_id,
			ratings: data.ratings,
			comments: data.comments ? data.comments : "",
			created_by: "Driver",
			deleted: "0",
			blocked: "0",
			created_at: data.created_at,
			updated_at: data.created_at
		});
		//data.order_rating_id = order.DriverRatings.order_rating_id;

		data.type = "DriverRatedService";
		data.message = Details.user.name+Libs.commonFunctions.generate_lang_message(" has rated your service", order.CustomerDetails.language_id);
		//data.message = Details.user.name+" has rated your service";

		data.user_notification = await Db.user_detail_notifications.create({
			user_id: order.customer_user_id,
			user_detail_id: order.customer_user_detail_id,
			user_type_id: order.customer_user_type_id,
			user_organisation_id: order.customer_organisation_id,
			sender_user_id: order.driver_user_id,
			sender_user_detail_id: order.driver_user_detail_id,
			sender_type_id: order.driver_user_type_id,
			sender_organisation_id: order.driver_organisation_id,
			order_id: order.order_id,
			order_request_id: order.CRequest.order_request_id,
			order_rating_id: order.DriverRatings.order_rating_id,
			message: data.message,
			type: data.type,
			is_read: "0",
			deleted: "0",
			created_at: data.created_at,
			updated_at: data.created_at
		});
                 var products = await Services.orderServices.AllOrderProducts(data);
		////////////////		Socket Event 	///////////////////////////
		data.socket_data = {
			type: data.type,//DriverRatedService
			message: data.message,
			order: {
				order_id: order.order_id,
				driver_user_detail_id: order.driver_user_detail_id,
				driver: {
					driver_id: order.driver_user_id,
					driver_detail_id: order.driver_user_detail_id
				},
                                products: products
			}
		};
                console.log("order event 4==", data.socket_data);
		var event = await Libs.commonFunctions.emitDynamicEvent("OrderEvent", [order.CustomerDetails.user_detail_id], data.socket_data);
		////////////////		Socket Event 	///////////////////////////

		////////////////		Push Notifications 	///////////////////////
		if(order.CustomerDetails.fcm_id != "" && order.CustomerDetails.notifications == "1")
		{
			data.push_data = {
				order_id: (data.order_id).toString(),
				type: data.type,//DriverRatedService
				driver_id: (order.driver_user_id).toString(), 
				driver_detail_id: (order.driver_user_detail_id).toString(),
				message: data.message
			};

			var push = Libs.commonFunctions.latestSendPushNotification([order.CustomerDetails], data.push_data);
		}
		////////////////		Push Notifications 	///////////////////////

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Order rated successfully") });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

////////////////////		Driver Schedules Order Confirmed Rejects 	//////////////////////////////
exports.driverScheduledServiceConfirm = async (request, response) => {
	try{

		var data = request.body;
		var Details = data.Details;
		delete data.Details;

		request.checkBody("order_id", response.trans("Order id is required")).notEmpty();
		request.checkBody("approved", response.trans("Approved is required")).notEmpty();

		request.checkBody("latitude", response.trans("Latitude is required")).notEmpty();
		request.checkBody("longitude", response.trans("Longitude is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var order = await Services.orderServices.driverOrder4Cancellations(data);
		if(!order  || !order.CRequest)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not available"), result: order });
		else if((order.order_status == "CustCancel") || (order.order_status == "SerCancel") || (order.order_status == "SupCancel") || (order.order_status == "SerReject") || (order.order_status == "SerTimeout") || (order.order_status == "DSchCancelled") || (order.order_status == "DSchTimeout") || (order.order_status == "SysSchCancelled") )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already cancelled"), result: order });
		else if((order.order_status != "DPending"))
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is no longer pending for approval or disapproval"), result: order });

		//////////////////		Truck Check 		////////////////////////////////////
		if(Details.category_id == 4)
		{
			var oCheck = await Db.orders.findOne({ where: {order_id: {$ne: data.order_id}, driver_user_detail_id: Details.user_detail_id, order_status: { $in: ["DApproved", "DPending", "Ongoing", "Confirmed"] } } });
			if(oCheck)
				return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, you already have an ongoing order"), result: oCheck });
		}
		//////////////////		Truck Check 		////////////////////////////////////

		if(data.approved == "1")
		{///////////////		Approved 	//////////////////////////

			var msg = response.trans("Service approved successfully");

			data.order_status = "DApproved";

		}///////////////		Approved 	//////////////////////////
		else
		{///////////////		UnApproved 	//////////////////////////

			var msg = response.trans("Service disapproved successfully");

			data.order_status = "DSchCancelled";

		}///////////////		UnApproved 	//////////////////////////

		Db.orders.update(
			{
				order_status: data.order_status,
				updated_at: data.created_at
			},
			{
				where: { order_id: order.order_id }
			}
		);

		Db.order_requests.update(
			{
				driver_current_latitude: data.latitude,
				driver_current_longitude: data.longitude,
				driver_confirmed_latitude: data.latitude,
				driver_confirmed_longitude: data.longitude,
				confirmed_at: data.created_at,
				updated_at: data.created_at
			},
			{
				where: { order_request_id: order.CRequest.order_request_id }
			}
		);

		///////////////////////////////////////////////////
		////////////////////		Customer Push Socket

		const message = Libs.commonFunctions.generate_lang_message(" has confirmed your service", order.CustomerDetails.language_id);

		////////////////		Socket Event 	///////////////////////////
		var temp_order = await Services.orderServices.driverAcceptOrderDetails(data, order.CustomerDetails.language_id);

		data.socket_data = {
			type: data.order_status,// DApproved, DSchCancelled
			order: temp_order,
			message: Details.user.name+message
		};

		Libs.commonFunctions.emitToDevice("OrderEvent", order.CustomerDetails.user_detail_id, data.socket_data );
		////////////////		Socket Event 	///////////////////////////

		////////////////		Push Notifications 	///////////////////////
		if(order.CustomerDetails.fcm_id != "" && order.CustomerDetails.notifications == "1")
		{

			data.push_data = {
				order_id: (data.order_id).toString(),
				type: data.order_status,// DApproved, DSchCancelled
				driver_user_id: (order.CRequest.driver_user_id).toString(),
				driver_user_detail_id: (order.CRequest.driver_user_detail_id).toString(),
				message: Details.user.name+message
			};

			var push = Libs.commonFunctions.latestSendPushNotification([order.CustomerDetails], data.push_data);

		}
		////////////////		Push Notifications 	///////////////////////

		////////////////////		Customer Push Socket
		///////////////////////////////////////////////////

		if(data.approved == "0")
		{

			//////////////////	Coupons or Etokens 	////////////////////////////
			if(order.coupon_user)
			{

				await Db.coupon_users.update(
					{
						rides_left: order.coupon_user.rides_left+1,
						updated_at: data.created_at
					},
					{
						where: {coupon_user_id: order.coupon_user_id}
					}
				);

			}
			// else if(order.organisation_coupon_user)
			// 	{

			// 		await Db.organisation_coupon_users.update(
			// 					{
			// 						quantity_left: order.organisation_coupon_user.quantity_left+order.payment.product_quantity,
			// 						updated_at: data.created_at
			// 					},
			// 					{
			// 						where: {organisation_coupon_user_id: order.organisation_coupon_user_id}
			// 					}
			// 		);

			// 	}
			//////////////////	Coupons or Etokens 	////////////////////////////

		}

		var order = await Services.orderServices.driverOrder4Cancellations(data);

		return response.status(200).json({ success: 1, statusCode: 200, msg, order });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};