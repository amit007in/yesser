//var moment = require("moment");

var Services = require(appRoot+"/services");///////////		Services
var Db = require(appRoot+"/models");//////////////////		Db Models

///////////////////		User History 	/////////////////////////////////////////////////
exports.driverHistory = async (request, response) => {
	try{

		var data = request.body;

		request.checkBody("type", response.trans("Type id is required")).notEmpty();
		request.checkBody("skip", response.trans("Skip parameter is required")).notEmpty();
		request.checkBody("take", response.trans("Take parameter is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});
		
		data.skip = parseInt(data.skip);
		data.take = parseInt(data.take);

		if(data.type == "1")
		{//////////		Past 	//////////////////////////
			data.order_status_array = ["CustCancel", "DriverCancel", "SerComplete", "SerCustCancel", "SerCustConfirm", "CTimeout"];
		}//////////		Past 	//////////////////////////
		else
		{//////////		Upcoming 	//////////////////////
			data.order_status_array = ["SerAccept", "Scheduled", "DPending", "DApproved", "Confirmed", "SerCustPending"];
			//data.order_status_array = '("Scheduled", "DPending", "DApproved")';
		}//////////		Upcoming 	//////////////////////

		var orders = await Services.orderServices.driverPastUpcomingOListings(data.user_detail_id, data.skip, data.take, data.order_status_array, "Service");
		//var orders = await Services.orderServices.appRidesListings(data);

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Orders listing"), result: orders });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

//////////////////			Order Details 		///////////////////////////////////////////////
exports.driverOrderDetails = async (request, response) => {
	try{

		var data = request.body;

		request.checkBody("order_id", response.trans("Order id is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var orders = await Services.orderServices.driverCompleteRideDetails(data.order_id, data.app_language_id);
		if(orders.length == 0 )
			return response.status(400).json({ success: 0, statusCode: 400, msg:  response.trans("Sorry, this order is currently not available") });

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Order details"), order: orders[0] });
	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

/////////////////		Service Order Earnings 	////////////////////////////////////////////////
exports.driverEarnings = async (request, response) => {
	try{
		var data = request.body;

		request.checkBody("start_dt", response.trans("Start dt required")).notEmpty();
		request.checkBody("end_dt", response.trans("End dt is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var earnings = await Services.paymentServices.driverEarningHistory(data.user_detail_id, data.start_dt, data.end_dt, data.app_language_id);

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Driver earnings"), result: earnings, data });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};
