//'use strict';
var moment = require("moment");

var Libs = require(appRoot+"/libs");//////////////////		Libraries
var Services = require(appRoot+"/services");///////////		Services
var Db = require(appRoot+"/models");//////////////////		Db Models
//var Configs = require(appRoot+"/configs");/////////////		Configs

///////////////////     Drivers Areas List      /////////////////////////////////
exports.driverAreasList = async (request, response) => {
	try{
		var data = request.body;

		request.checkBody("skip", response.trans("Skip is required")).notEmpty();
		request.checkBody("take", response.trans("Take is required")).notEmpty();

		request.checkBody("latitude", response.trans("Latitude is required")).notEmpty();
		request.checkBody("longitude", response.trans("Longitude is required")).notEmpty();

		request.checkBody("day", response.trans("day is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var areas = await Services.waterServices.driverAreasListings(data.user_organisation_id, data.user_detail_id, parseInt(data.skip), parseInt(data.take), data.day, data.latitude, data.longitude);

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Areas listings"), result: areas });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

/////////////////////       Single Driver Customers Listings        //////////////////////////
exports.driverAreaCustomersList = async (request, response) => {
	try{
		var data = request.body;

		request.checkBody("skip", response.trans("Skip is required")).notEmpty();
		request.checkBody("take", response.trans("Take is required")).notEmpty();

		request.checkBody("latitude", response.trans("Latitude is required")).notEmpty();
		request.checkBody("longitude", response.trans("Longitude is required")).notEmpty();

		request.checkBody("organisation_area_id", response.trans("organisation_area_id is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var area = await Db.organisation_areas.findOne({
			where: { organisation_area_id: data.organisation_area_id, organisation_id: data.user_organisation_id }
		});
		if(!area)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this area is currently not available") });

		data.driverPIds = await Services.userDetailServices.driverProductsIds(data.user_detail_id);// Driver Product Ids Get
		data.driverPIdsJoin = data.driverPIds.join();

		var customers = await Services.waterServices.driverAreaWiseCustomersListings(data.user_id, data.user_detail_id, data.user_organisation_id, area.organisation_area_id,data.Details.category_brand_id, data.driverPIdsJoin, data.latitude, data.longitude, parseInt(data.skip), parseInt(data.take), data.app_language_id, data.created_at);

		var result = {
			area,
			customers,
			//data
		};
            
		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Customers listings"), result });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

//////////////////      Driver Initiate Water Order       ///////////////////////////
exports.driverInitiateOrder = async (request, response) => {
	try{

		var data = request.body;

		request.checkBody("organisation_coupon_user_id", response.trans("organisation_coupon_user_id is required")).notEmpty();

		request.checkBody("latitude", response.trans("Latitude is required")).notEmpty();
		request.checkBody("longitude", response.trans("Longitude is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var userEtoken4StartingOrder = await Services.waterServices.userEtoken4StartingOrder(data.organisation_coupon_user_id, data.created_at, data.user_id, data.user_detail_id, data.user_organisation_id, data.Details.category_brand_id);
		if(!userEtoken4StartingOrder)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this customer is currently not available") });
    
		userEtoken4StartingOrder = JSON.parse(JSON.stringify(userEtoken4StartingOrder));
		if(userEtoken4StartingOrder.todayOrderId > 0)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this customer order for today has already been initiated"), userEtoken4StartingOrder });

		/////////////////////       Order Creation      /////////////////////////////
		data.order_token = "OR-"+Libs.commonFunctions.uniqueId();

		var order = await Db.orders.create({
			order_token: data.order_token,
			/////////		Customer 	/////////////////
			customer_user_id: userEtoken4StartingOrder.customer_user_id,
			customer_user_detail_id: userEtoken4StartingOrder.customer_user_detail_id,
			customer_organisation_id: userEtoken4StartingOrder.customer_organisation_id,
			customer_user_type_id: userEtoken4StartingOrder.customer_user_type_id,
			/////////		Customer 	/////////////////
			///////////		Driver 	/////////////////////
			driver_user_id: data.user_id,
			driver_user_detail_id: data.user_detail_id,
			driver_organisation_id: data.user_organisation_id,
			driver_user_type_id: data.user_type_id,
			///////////		Driver 	/////////////////////
			///////////		Category 	/////////////////
			category_id: userEtoken4StartingOrder.category_id,
			category_brand_id: userEtoken4StartingOrder.category_brand_id,
			category_brand_product_id: userEtoken4StartingOrder.category_brand_product_id,
			///////////		Category 	/////////////////
			continuous_order_id: 0,
			order_status: "Confirmed",
			pickup_address: "",
			pickup_latitude: 0.0,
			pickup_longitude: 0.0,
			dropoff_address: userEtoken4StartingOrder.address,
			dropoff_latitude: userEtoken4StartingOrder.address_latitude,
			dropoff_longitude: userEtoken4StartingOrder.address_longitude,
			order_type: "Service",
			created_by: "Driver",
			order_timings: data.created_at,
			future: "0",
			continouos_startdt: null,
			continuous_enddt: null,
			continuous_time: null,
			continuous: "0",
			cancel_reason: "",
			cancelled_by: "",
			my_turn: "0",
			organisation_coupon_user_id: userEtoken4StartingOrder.organisation_coupon_user_id,
			coupon_user_id: userEtoken4StartingOrder.organisation_coupon_id,
			track_path: "",
			track_image: "",
			details: "",
			material_details: "",
			created_at: data.created_at,
			updated_at: data.created_at
		});

		order = JSON.parse(JSON.stringify(order));
		/////////////////////       Order Creation      /////////////////////////////

		/////////////////////       Payment Creation    /////////////////////////////
		order.payment = await Db.payments.create({
			/////////		Customer 	/////////////////
			customer_user_id: userEtoken4StartingOrder.customer_user_id,
			customer_user_detail_id: userEtoken4StartingOrder.customer_user_detail_id,
			user_card_id: 0,
			customer_organisation_id: userEtoken4StartingOrder.customer_organisation_id,
			customer_user_type_id: userEtoken4StartingOrder.customer_user_type_id,
			/////////		Customer 	/////////////////
			order_id: order.order_id,
			///////////		Driver 	/////////////////////
			seller_user_id: data.user_id,
			seller_user_detail_id: data.user_detail_id,
			seller_organisation_id: data.user_organisation_id,
			seller_user_type_id: data.user_type_id,
			///////////		Driver 	/////////////////////
			payment_type: "eToken",
			payment_status: "Pending",
			refund_status: "NoNeed",
			transaction_id: "",
			refund_id: "",
			buraq_percentage: 0,
			product_actual_value: 0,
			product_quantity: userEtoken4StartingOrder.bottle_quantity,
			product_weight: "0",
			product_sq_mt: "0",
			order_distance: "0",
			order_time: "0",
			product_alpha_charge: "0",
			product_per_quantity_charge: "0",
			product_per_weight_charge: "0",
			product_per_distance_charge: "0",
			product_per_hr_charge: "0",
			product_per_sq_mt_charge: "0",
			initial_charge: "0",
			admin_charge: "0",
			bank_charge: "0",
			final_charge: "0",
			bottle_charge: "0",
			bottle_returned_value: "0",
			created_at: data.created_at,
			updated_at: data.created_at,
		});

		order = JSON.parse(JSON.stringify(order));
		/////////////////////       Payment Creation    /////////////////////////////

		/////////////////////  Order Request Creation    ////////////////////////////
		order.CRequest = await Db.order_requests.create({
			order_id: order.order_id,
			driver_user_id: data.user_id,
			driver_user_detail_id: data.user_detail_id,
			driver_organisation_id: data.user_organisation_id,
			order_request_status: "Accepted",
			driver_request_latitude: data.latitude,
			driver_request_longitude: data.longitude,
			driver_current_latitude: data.latitude,
			driver_current_longitude: data.longitude,
			accepted_at: data.created_at,
			driver_confirmed_latitude: data.latitude,
			driver_confirmed_longitude: data.latitude,
			driver_started_latitude: 0.0,
			driver_started_longitude: 0.0,
			full_track: "[]",
			created_at: data.created_at,
			updated_at: data.created_at
		});

		order = JSON.parse(JSON.stringify(order));
		/////////////////////  Order Request Creation    ////////////////////////////

		///////////////////////////////////////////////////
		////////////////////		Customer Push Socket
		const message = Libs.commonFunctions.generate_lang_message(" has intiated your drinking water service", userEtoken4StartingOrder.CustomerDetails.language_id);

		////////////////		Socket Event 	///////////////////////////
		data.order_id = order.order_id;
    
		var temp_order = await Services.orderServices.driverAcceptOrderDetails(data, userEtoken4StartingOrder.CustomerDetails.language_id);

		data.socket_data = {
			type: "eTokenSerStart",
			order: temp_order,
			message: data.Details.user.name+message
		};

		Libs.commonFunctions.emitToDevice("OrderEvent", order.customer_user_detail_id, data.socket_data);
		////////////////		Socket Event 	///////////////////////////

		////////////////		Push Notifications 	///////////////////////
		if(userEtoken4StartingOrder.CustomerDetails.fcm_id != "" && userEtoken4StartingOrder.CustomerDetails.notifications == "1")
		{

			data.cust_push_data = {
				order_id: (data.order_id).toString(),
				type: "eTokenSerStart",
				message: data.Details.user.name+message
			};

			var push = Libs.commonFunctions.latestSendPushNotification([userEtoken4StartingOrder.CustomerDetails], data.cust_push_data);

		}
		////////////////		Push Notifications 	///////////////////////

		////////////////////		Customer Push Socket
		///////////////////////////////////////////////////

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Order started successfully"), result: order });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

/////////////////////       Driver End Service Water        ////////////////////////
exports.driverEndOrder = async (request, response) => {
	try{
		var data = request.body;

		request.checkBody("order_id", response.trans("order_id is required")).notEmpty();

		request.checkBody("latitude", response.trans("latitude is required")).notEmpty();
		request.checkBody("longitude", response.trans("longitude is required")).notEmpty();

		request.checkBody("product_quantity", response.trans("product_quantity is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var order = await Services.orderServices.driveretokenOrderDetails(data.order_id, data.user_detail_id);
		if(!order)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not available"), result: order });
		else if(order.organisation_coupon_user_id == 0)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this facility is only available for etoken orders"), result: order });
		else if(order.order_status == "SerComplete" || order.order_status == "SerCustConfirm")
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already completed"), result: order });
		else if((order.order_status == "CustCancel") || (order.order_status == "SerCancel") || (order.order_status == "SupCancel") || (order.order_status == "SerReject") || (order.order_status == "SerTimeout") || (order.order_status == "DSchCancelled") || (order.order_status == "DSchTimeout") || (order.order_status == "SysSchCancelled") )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already cancelled"), result: order });
		else if(order.order_status == "SerCustPending")
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already wating for customer confirmation"), result: order });
		else if(order.order_status != "Ongoing")
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not ongoing"), result: order });

		order = JSON.parse(JSON.stringify(order));

		order.order_status = "SerCustPending";

		///////////////     Update Db       ///////////////////////////////////
		data.update_order = await Db.orders.update(
			{
				order_status: order.order_status,
				updated_at: data.created_at
			},
			{
				where: {
					order_id: order.order_id
				}
			}
		);

		data.transaction_id = "P-"+Libs.commonFunctions.uniqueId();
    
		if(!data.order_distance)
		    data.order_distance = parseFloat(order.payment.order_distance);

		///////////		Order Time 	//////////////////////
		data.order_time = 0;

		var a = moment(data.created_at, "YYYY-MM-DD HH:mm:ss");
		var b = moment(order.created_at, "YYYY-MM-DD HH:mm:ss");
		data.order_time = a.diff(b, "hours");
		///////////		Order Time 	//////////////////////

		data.update_payment = await Db.payments.update(
			{
				//payment_status: "Completed",
				transaction_id: data.transaction_id,
				order_distance: data.order_distance,
				order_time: data.order_time,
				product_quantity: data.product_quantity,
				updated_at: data.created_at
			},
			{
				where: { payment_id: order.payment.payment_id }
			}
		);

		order.CRequest.full_track = JSON.parse(order.CRequest.full_track);
		order.CRequest.full_track.push({"Dt":data.created_at, "latitude": parseFloat(data.latitude), "longitude": parseFloat(data.longitude)});

		data.update_order_request = await Db.order_requests.update(
			{
				driver_current_latitude: data.latitude,
				driver_current_longitude: data.longitude,
				full_track: JSON.stringify(order.CRequest.full_track),
				updated_at: data.created_at
			},
			{
				where: {
					order_request_id: order.CRequest.order_request_id
				}
			}
		);
        
		delete order.CRequest.full_track;
		///////////////     Update Db       ///////////////////////////////////

		////////////////		Socket Event 	///////////////////////////
		var message = Libs.commonFunctions.generate_lang_message(" has completed your etoken order and waiting for your approval", order.CustomerDetails.language_id);

		var temp_order = await Services.orderServices.driverAcceptOrderDetails(data, order.CustomerDetails.language_id);
                 
		data.socket_data = {
			type: order.order_status,//SerCustPending
			order: temp_order,
			message: data.Details.user.name+message
		};
                console.log("order event 5==", data.socket_data);
		Libs.commonFunctions.emitToDevice("OrderEvent", order.customer_user_detail_id, data.socket_data);
		////////////////		Socket Event 	///////////////////////////

		////////////////		Push Notifications 	///////////////////////
		if(order.CustomerDetails.fcm_id != "" && order.CustomerDetails.notifications == "1")
		{
			data.cust_push_data = {
				order_id: (data.order_id).toString(),
				type: order.order_status,//SerCustPending
				message: data.Details.user.name+message
			};

			var push = Libs.commonFunctions.latestSendPushNotification([order.CustomerDetails], data.cust_push_data);
		}
		////////////////		Push Notifications 	///////////////////////

		//////////		No Response Timeout After 2 Minutes 	///////////
		data.timeOutMessage = response.trans(" order has been cancelled because of no response from customer");

		setTimeout(function(){
			orderRequestedTimeout(data);// This code will only run when time has ellapsed    
		}, 120*1000);
		//////////		No Response Timeout After 2 Minutes 	///////////

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Please wait for customer to confirm the order"), result: order });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

//////////////////////          Request Times Out       ////////////////////////////
var orderRequestedTimeout = async (data) => {
	try{
		var order = await Services.orderServices.etokenOrderDetails4AutoCancellation(data.order_id, data.user_detail_id);
		if(!order || order.order_status != "SerCustPending")
			return { success: 1, statusCode: 200, msg: "No need for automatic timeout", order };

		data.order_status = "CTimeout";

		////////////////////        Db Update   ////////////////////////////////
		data.update_order = await Db.orders.update(
			{
				order_status: data.order_status,//CTimeout
				payment_status: data.order_status,
				cancelled_by: data.order_status,
				updated_at: data.created_at
			},
			{
				where: {
					order_id: data.order_id
				}
			}
		);

		data.update_order_requests = await Db.payments.update(
			{
				payment_status: data.order_status,//CTimeout
				updated_at: data.created_at
			},
			{
				where: {
					order_id: data.order_id
				}
			}
		);
		////////////////////        Db Update   ////////////////////////////////

		//////////////////////////////////////////////
		////////////////		Driver Push Notifications 	///////////////////////
		if(order.DriverDetails.fcm_id != "" && order.DriverDetails.notifications == "1")
		{

			let message = order.Customer.name+data.timeOutMessage;

			data.push_data = {
				order_id: (data.order_id).toString(),
				type: data.order_status,//CTimeout
				message: message
			};

			var dPush = Libs.commonFunctions.latestSendPushNotification([order.DriverDetails], data.push_data);

		}
		////////////////		Driver Push Notifications 	///////////////////////

		////////////////		Driver Socket Event 	///////////////////////////
		data.socket_data = {
			type: data.order_status,
			order_id: order.order_id,
			order: {
				order_id: order.order_id,
				driver_user_detail_id: order.driver_user_detail_id
			}
		};

		var dEvent = Libs.commonFunctions.emitDynamicEvent("OrderEvent", [order.DriverDetails.user_detail_id], data.socket_data);
		////////////////		Driver Socket Event 	///////////////////////////

		////////////////		Customer Push Notifications 	///////////////////////
		if(order.CustomerDetails.fcm_id != "" && order.CustomerDetails.notifications == "1")
		{

			let message = order.Driver.name+Libs.commonFunctions.generate_lang_message(" etoken order has been cancelled because of your no response", order.CustomerDetails.language_id);

			data.push_data = {
				order_id: (data.order_id).toString(),
				type: data.order_status,//CTimeout
				message: message
			};

			var cPush = Libs.commonFunctions.latestSendPushNotification([order.CustomerDetails], data.push_data);

		}
		////////////////		Driver Push Notifications 	///////////////////////

		////////////////		Driver Socket Event 	///////////////////////////
		data.socket_data = {
			type: data.order_status,
			order_id: order.order_id,
			order: {
				order_id: order.order_id,
				driver_user_detail_id: order.driver_user_detail_id
			}
		};

		var cEvent = Libs.commonFunctions.emitDynamicEvent("OrderEvent", [order.CustomerDetails.user_detail_id], data.socket_data);
		////////////////		Driver Socket Event 	///////////////////////////

		console.log("Automatic No Customer Success Response - ", data.order_id);

	}
	catch(e)
	{
		delete data.Details;
		console.log("Automatic No Response Error Response - ", data, e.message);
		return { success: 0, statusCode: 500, msg: e.message};
	}
};