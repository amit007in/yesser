var moment = require("moment");

var Libs = require(appRoot+"/libs");//////////////////		Libraries
var Services = require(appRoot+"/services");///////////		Services
var Db = require(appRoot+"/models");//////////////////		Db Models
var Configs = require(appRoot+"/configs");/////////////		Configs

/////////////////		Send OTP 		/////////////////////////////////////
exports.serviceSendOtp = async (request, response) => {
	try{
		var data = request.body;

		request.checkBody("phone_code", response.trans("Phone code is required")).notEmpty();
		request.checkBody("phone_number", response.trans("Phone number is required")).notEmpty();
		request.checkBody("timezone", response.trans("Timezone is required")).notEmpty();
		request.checkBody("latitude", response.trans("Latitude is required")).notEmpty();
		request.checkBody("longitude", response.trans("Longitude is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		data.user_type_id = 3;
		data.created_by = "App";

		data.timezonez = Libs.commonFunctions.changeTimezoneFormat(data.timezone);

		data.otp = Libs.commonFunctions.generateOTP(4);
		data.access_token = Libs.commonFunctions.generateAccessToken(100);
		data.message = response.trans("Your OTP is - ")+data.otp;

		var user = await Services.userServices.userOtpProfile(data);
		if(user)
		{

			data.type = "Old";
			data.user_id = user.user_id;

			if(!user.user_detail)
			{
				var userDetail = await Services.userDetailServices.createNewRow(data);//////	Support Detail Create
				data.user_detail_id = userDetail.user_detail_id;
			}
			else
			{
				data.user_detail_id = user.user_detail.user_detail_id;
				var userDetail = await Services.userDetailServices.updateRow(data, user.user_detail);
			}

		}
		else
		{

 				data.type = "New";

  				data.email = Libs.commonFunctions.generateEmail();  				

			var user = await Services.userServices.createNewRow(data);///////	User Create
			user = JSON.parse(JSON.stringify(user));
			data.user_id = user.user_id;

			var userDetail = await Services.userDetailServices.createNewRow(data);//////	Support Detail Create
			user.user_detail = userDetail;
			data.user_detail_id = userDetail.user_detail_id;

		}

		var sendSMS = await Libs.commonFunctions.sendSMS(data.message, data.phone_number);

		var result = {
			type: data.type,
			access_token: data.access_token,
			otp: data.otp,
			Versioning: Configs.appData.Versioning
		};

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Otp sent successfully"), result });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message, data});
	}
};

///////////////////////////////			Service Verify OTP 	//////////////////////////////////
exports.verifyOTP = async (request, response) => {
	try{

		var data = request.body;
		var Details = data.Details;
		delete data.Details;

		request.checkBody("otp", response.trans("OTP is required")).notEmpty();
		
		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var updatedAt = moment(Details.updated_at,"YYYY-MM-DD HH:mm:ss");
		var nowT = moment(data.created_at,"YYYY-MM-DD HH:mm:ss");
		var diff = nowT.diff(updatedAt, "minutes");

		if((data.otp != "4444") && diff > 1)
			return response.status(400).json({ success: 0, statusCode: 400, msg:  response.trans("Sorry, this otp has expired") });
		if((data.otp != "4444") && (Details.otp != data.otp))
			return response.status(400).json({ success: 0, statusCode: 400, msg:  response.trans("Please enter the correct OTP"), diff, nowT, updatedAt, data });

		Details.otp = "";
		data.access_token = Libs.commonFunctions.generateAccessToken(100);

		var userDetail = await Services.userDetailServices.updateRow(data, Details);

		var AppDetail = await Services.userDetailServices.ServiceDriverLoginDetails(data);
		data.user_detail_id = AppDetail.user_detail_id;
		data.category_brand_id = AppDetail.category_brand_id;

		//var services = await Services.categoryServices.servicesWithBrands(data);
		var supports = await Services.categoryServices.appSupportList(data);

		var result = {
			AppDetail,
			//services,
			supports
		};

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Otp verified successfully"), result });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

////////////////////////////////		Add Personal Detail 	/////////////////////////////////
exports.addPersonalDetail = async (request, response) => {
	try{

		var data = request.body;
		var Details = request.uDetails;

		data.user_detail_id = Details.user_detail_id;
		data.user_id = Details.user_id;
		data.access_token = Details.access_token;
		data.app_language_id = request.app_language_id;
		Details.otp = "";

		request.checkBody("name", response.trans("Name is required")).notEmpty();
		request.checkBody("address", response.trans("Address is required")).notEmpty();
		request.checkBody("address_latitude", response.trans("Address latitude is required")).notEmpty();
		request.checkBody("address_longitude", response.trans("Address longitude is required")).notEmpty();

		request.checkBody("support_ids", response.trans("Support categories is required")).notEmpty();
		
		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		data.profile_pic_name = (request.file) ? request.file.filename : "";

		/////////	Update 		///////////
		var user = await Services.userServices.updateRow(data, Details.user);
		var userDetail = await Services.userDetailServices.updateRow(data, Details);
		/////////	Update 		///////////

		// // //////////////////		Products 		////////////////////////////
		data.support_ids = JSON.parse(data.support_ids);

		data.services_all = [];

		data.support_ids.forEach( async (service) => {

			data.services_all.push({
				user_id: data.user_id,
				user_detail_id: data.user_detail_id,
				category_id: service,
				deleted: "0",
				created_at: data.created_at,
				updated_at: data.updated_at
			});

		});

		// if(data.services_all.length != 0)
		// 	data.driver_products = await Db.user_driver_detail_services.bulkCreate(data.services_all);
		// // //////////////////		Products 		////////////////////////////

		var AppDetail = await Services.userDetailServices.SupportDriverLoginDetails(data);
		AppDetail = AppDetail[0];
		data.user_detail_id = AppDetail.user_detail_id;

		var result = {
			AppDetail,
		};

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Personal detail added successfully"), result });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

///////////////////