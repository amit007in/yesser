var moment = require("moment");
var laravel_hit = require('request');

var Libs = require(appRoot + "/libs");//////////////////		Libraries
var Services = require(appRoot + "/services");///////////		Services
var Db = require(appRoot + "/models");//////////////////		Db Models
var Configs = require(appRoot + "/configs");/////////////		Configs

///////////////////		User Email Username Check 		//////////////////////////////////
exports.userSendOtp = async (request, response) => {
    try {
        var data = request.body;

        request.checkBody("phone_code", response.trans("Phone code is required")).notEmpty();
        request.checkBody("phone_number", response.trans("Phone number is required")).notEmpty();
        request.checkBody("timezone", response.trans("Timezone is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        data.user_type_id = 1;
        data.created_by = "App";

        data.timezonez = Libs.commonFunctions.changeTimezoneFormat(data.timezone);

        data.otp = Libs.commonFunctions.generateOTP(4);
        data.access_token = Libs.commonFunctions.generateAccessToken(100);
        data.message = response.trans("Your OTP is - ") + data.otp;

        var user = await Services.userServices.userOtpProfile(data);
        if (user)
        {

            data.type = "Old";
            data.user_id = user.user_id;
            data.maximum_rides = 1;

            if (!user.user_detail)
            {
                var userDetail = await Services.userDetailServices.createNewRow(data);//////	User Detail Create
                data.user_detail_id = userDetail.user_detail_id;
            } else
            {
                data.user_detail_id = user.user_detail.user_detail_id;
                var userDetail = await Services.userDetailServices.updateRow(data, user.user_detail);
            }

        } else
        {

            data.type = "New";

            data.email = Libs.commonFunctions.generateEmail();

            data.stripe = await Libs.stripeFunctions.createCustomer(data);///////	Stripe Customer
            data.stripe_customer_id = data.stripe.id;

            var user = await Services.userServices.createNewRow(data);///////	User Create
            user = JSON.parse(JSON.stringify(user));
            data.user_id = user.user_id;

            var userDetail = await Services.userDetailServices.createNewRow(data);//////	User Detail Create
            user.user_detail = userDetail;
            data.user_detail_id = userDetail.user_detail_id;

            /////////////	Saving Card 	/////////////////////////////////
            var userCard = await Db.user_cards.create({
                user_id: data.user_id,
                card_id: data.stripe.sources.data[0].id,
                fingerprint: data.stripe.sources.data[0].fingerprint,
                last4: data.stripe.sources.data[0].last4,
                brand: data.stripe.sources.data[0].brand,
                is_default: "1",
                deleted: "0",
                created_at: data.created_at,
                updated_at: data.created_at
            });
            /////////////	Saving Card 	/////////////////////////////////

        }

       // var sendSMS = Libs.commonFunctions.sendSMS(data.message, data.phone_number);

        var result = {
            //data,
            type: data.type,
            access_token: data.access_token,
            otp: data.otp,
            Versioning: Configs.appData.Versioning
        };

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Otp sent successfully"), result});

    } catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

///////////////////////////////			User Verify OTP 	/////////////////////////////////////////////
exports.verifyOTP = async (request, response) => {
    try {

        var data = request.body;
        var Details = data.Details;
        delete data.Details;

        request.checkBody("otp", response.trans("OTP is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        var updatedAt = moment(Details.updated_at, "YYYY-MM-DD HH:mm:ss");
        var nowT = moment(data.created_at, "YYYY-MM-DD HH:mm:ss");
        var diff = nowT.diff(updatedAt, "minutes");

        if ((data.otp != "4142") && diff > 1)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this otp has expired")});
        if ((data.otp != "4142") && (Details.otp != data.otp))
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Please enter the correct OTP"), diff, nowT, updatedAt, data});

        data.user_type_id = 1;

        data.otp = Details.otp = "";
        data.access_token = Libs.commonFunctions.generateAccessToken(100);

        var userDetail = await Services.userDetailServices.updateRow(data, Details);

        var AppDetail = await Services.userDetailServices.UserGetAppDetails(data);

        /////////////		Profile Pic 	///////////////////////////////
        var AppDetail = JSON.parse(JSON.stringify(AppDetail));
        if (AppDetail.profile_pic != "")
            AppDetail.profile_pic_url = process.env.RESIZE_URL + AppDetail.profile_pic;
        else
            AppDetail.profile_pic_url = "";
        /////////////		Profile Pic 	///////////////////////////////

        data.user_detail_id = AppDetail.user_detail_id;

        var services = await Services.categoryServices.servicesWithBrands(data);
        var supports = await Services.categoryServices.supportsListings(data);
        //var res = await Services.categoryServices.appServiceCategories(data); // Laravel Api

        var result = {
            AppDetail,
            services: services,
            supports: supports
        };

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Otp verified successfully"), result});

    } catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

////////////////////////////		Add Name 		/////////////////////////////////////////////////////
exports.addName = async (request, response) => {
    try {

        var data = request.body;
        var Details = data.Details;
        delete data.Details;

        request.checkBody("name", response.trans("Name is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        data.user_type_id = 1;
        data.access_token = Libs.commonFunctions.generateAccessToken(100);

        var user = await Services.userServices.updateRow(data, Details.user);
        var userDetail = await Services.userDetailServices.updateRow(data, Details);

        var AppDetail = await Services.userDetailServices.UserGetAppDetails(data);

        /////////////		Profile Pic 	///////////////////////////////
        var AppDetail = JSON.parse(JSON.stringify(AppDetail));
        if (AppDetail.profile_pic != "")
            AppDetail.profile_pic_url = process.env.RESIZE_URL + AppDetail.profile_pic;
        else
            AppDetail.profile_pic_url = "";
        /////////////		Profile Pic 	///////////////////////////////
        // var result = {
        // 	AppDetail
        // }

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Name added successfully"), result: AppDetail});

    } catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};


///////////////////////////////		Profile Update 		////////////////////////////////////////
exports.profileUpdate = async (request, response) => {
    try {

        var data = request.body;
        var Details = request.uDetails;

        data.user_detail_id = Details.user_detail_id;
        data.user_id = Details.user_id;
        data.access_token = Details.access_token;
        data.app_language_id = request.app_language_id;
        data.created_at = request.created_at;

        request.checkBody("name", response.trans("Name is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        if (request.file)
        {

            var update = await Db.user_details.update(
                    {
                        profile_pic: request.file.filename,
                        updated_at: data.created_at
                    },
                    {
                        where: {user_detail_id: data.user_detail_id}
                    }
            );

        }

        var update_user = await Db.users.update(
                {
                    name: data.name,
                    updated_at: data.created_at
                },
                {
                    where: {user_id: data.user_id}
                }
        );

        var AppDetail = await Services.userDetailServices.UserGetAppDetails(data);

        var AppDetail = JSON.parse(JSON.stringify(AppDetail));
        if (AppDetail.profile_pic != "")
            AppDetail.profile_pic_url = process.env.RESIZE_URL + AppDetail.profile_pic;
        else
            AppDetail.profile_pic_url = "";

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Profile updated successfully"), result: AppDetail});

    } catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

////////////////////////////	Notification History 	/////////////////////////////////////////////////////
exports.notificationHistory = async (request, response) => {
    try {
        var data = request.body;
        data.access_token = Libs.commonFunctions.generateAccessToken(100);
        var list = await Services.userServices.GetAllNotification(data);
        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Success"), result: list});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

////////////////////////////	Notification Detail 	/////////////////////////////////////////////////////
exports.getNotificationById = async (request, response) => {
    try {
        var data = request.body;
        data.access_token = Libs.commonFunctions.generateAccessToken(100);
        var notification_detail = await Services.userServices.GetNotificationById(data);
        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Success"), result: notification_detail});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};
////////////////////////////	Get All Promocodes	/////////////////////////////////////////////////////
exports.getPromocodeList = async (request, response) => {
    try {
        var data = request.body;
        var where = {
            user_id: data.user_id
        };
        
        if (data.category_id)
            where.category_id = data.category_id;
        if (data.brand_id)
            where.brand_id = data.brand_id;

        var coupon_ids = await Services.userServices.getAllCouponIds(where);
        console.log("coupon_ids ==", coupon_ids); 
        
        data.access_token = Libs.commonFunctions.generateAccessToken(100);
        var promocode_list = await Services.userServices.getAllPromocodes(data, coupon_ids);
        
      
        
        if (promocode_list) {
            return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Success"), result: promocode_list});
        } else {
            return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Success"), result: [] });
        }
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

////////////////////////////	Get All Promocodes	/////////////////////////////////////////////////////
exports.getPromocodeLists = async (request, response) => {
    try {
        var data = request.body;
        var where = {
            user_id: data.user_id
        };
        
        if (data.category_id)
            where.category_id = data.category_id;
        if (data.brand_id)
            where.brand_id = data.brand_id;

        var coupon_ids = await Services.userServices.getAllCouponIds(where);
        console.log("coupon_ids ==", coupon_ids); 
        
        //var promoproducts = await Services.userServices.getAllPromoProducts(where);
        
        data.access_token = Libs.commonFunctions.generateAccessToken(100);
//         var promocode_list = new Array();
         var listttnewArrayy = new Array();
        var promocode_list = await Services.userServices.getAllPromocodes(data, coupon_ids);
        
        if (promocode_list) { console.log("tt 2==", promocode_list);
            return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Success"), result: promocode_list});
        } else {
            return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Success"), result: []});
        }
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

////////////////////////////	Notification Detail 	/////////////////////////////////////////////////////
exports.getPromocodeById = async (request, response) => {
    try {
        var data = request.body;
        data.access_token = Libs.commonFunctions.generateAccessToken(100);
        var promocode_detail = await Services.userServices.GetPromocodeById(data);
        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Success"), result: promocode_detail});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

////////////////////////////	Notification Detail 	/////////////////////////////////////////////////////
exports.applyPromocode = async (request, response) => {
    try {
        var data = request.body;
        console.log(data);
        var promocode_detail_rides = await Services.userServices.checkPromoUserRides(data);
        console.log("promocode_detail_rides====",promocode_detail_rides);
        if (promocode_detail_rides != null) {
            console.log("promocode_detail_rides====",promocode_detail_rides.promo_users_rides.length );
            if(promocode_detail_rides.promo_users_rides.length > 0){
                return response.status(406).json({success: 0, statusCode: 406, msg:response.trans("Coupon maximum limit reached."), result: {} });
            }
        }else{
            return response.status(200).json({success: 1, statusCode: 400, msg: response.trans("No Promocode Found."), result: {} });
        }
        
        data.access_token = Libs.commonFunctions.generateAccessToken(100);
        var promocode_detail = await Services.userServices.checkPromocode(data);
        console.log("apply --", promocode_detail);
        if (typeof promocode_detail === "undefined" || promocode_detail == null) {
            return response.status(200).json({success: 1, statusCode: 400, msg: response.trans("No Promocode Found."), result: {} });
        } else { //console.log(moment(promocode_detail.expires_at).format() + ">" + moment().format());
            if (moment(promocode_detail[0].expires_at).format() > moment().format()) {
                if (promocode_detail) {
                    return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Success"), result: promocode_detail[0]});
                } else {
                    return response.status(500).json({success: 0, statusCode: 500, msg: "This coupon is not applicable to this user."});
                }
            } else {
                return response.status(200).json({success: 1, statusCode: 401, msg: response.trans("This code is expired."), result: {} });
            }
        }
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};
///////////////////////////