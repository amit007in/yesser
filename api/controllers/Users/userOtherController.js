var moment = require("moment");
var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;

var Libs = require(appRoot+"/libs");//////////////////		Libraries
var Services = require(appRoot+"/services");///////////		Services
var Db = require(appRoot+"/models");//////////////////		Db Models

////////////////////		User Coupons Listings 	////////////////////////////////
exports.userCoupons = async (request, response) => {
	try{

		var data = request.body;

		var history = await Services.couponUserServices.userCouponsHistory(data);
		var coupons = await Services.couponServices.CouponsListings(data);

		var result = {
			history,
			coupons
		};

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Token details"), result });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

/////////////////		User Buys Coupon 	./////////////////////////////////////
exports.userCouponBuy = async (request, response) => {
	try{

		var data = request.body;

		request.checkBody("coupon_id", response.trans("Coupon id is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var coupon = await Services.couponServices.couponDetails(data);
		if(!coupon)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this coupon is no longer available") });

		var user_card = await Db.user_cards.findOne({ where: {user_id: data.user_id} });
		data.user_card_id = user_card.user_card_id;

		data.order_id = 0;
		data.organisation_coupon_user_id = 0;
		data.seller_user_id = 0;
		data.seller_user_detail_id = 0;
		data.seller_user_type_id = 5;
		data.seller_organisation_id = 0;
		data.payment_status = "Completed";
		data.initial_charge = data.final_charge = data.product_actual_value = coupon.price;
		data.bank_charge = 0;
		data.admin_charge = 0;

		data.metadata = {
			coupon_id: data.coupon_id
		};

		data.message = "Charge for Coupon "+data.coupon_id;
		
		data.payment = await Libs.stripeFunctions.CouponPayment(data);///////	Stripe Payment
		if(!data.payment || !data.payment.id)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, payment was not successfully"), payment: data.payment });

		data.transaction_id = data.payment.id;

		data.payment = await Services.paymentServices.CreateNewRow(data);
		data.payment_id = data.payment.payment_id;

		data.rides_value = coupon.rides_value;
		data.coupon_value = coupon.amount_value;
		data.rides_left = coupon.rides_value;
		data.price = coupon.price;
		data.coupon_type = coupon.coupon_type;
		data.expires_at = moment(data.created_at, "YYYY-MM-DD HH:mm:ss").add(coupon.expiry_days, "days").format("YYYY-MM-DD HH:mm:ss");

		data.coupon_user = await Services.couponUserServices.CreateNewRow(data);
		data.coupon_user_id = data.coupon_user.coupon_user_id;

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Coupon purchased successfully"), result: data.coupon_user_id });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};


/////////////////////		User Payment Details 	//////////////////////////////////////////
exports.userPaymentDetails = async (request, response) => {
	try{

		var data = request.body;
		delete data.Details;
		// request.checkBody("category_brand_id", response.trans("Category brand id is required")).notEmpty();
		// request.checkBody("category_brand_product_id", response.trans("Category brand product is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var brand = null;

		if(data.category_brand_product_id && data.category_brand_id)
		{

			var etoken_sum = await Db.organisation_coupon_users.sum("quantity_left", {

				where: {
					category_brand_product_id: data.category_brand_product_id,
					//category_brand_id: data.category_brand_id,
					customer_user_detail_id: data.user_detail_id
				}

			});
			if(!etoken_sum)
				etoken_sum = 0;

			var brands = await Db.sequelize.query("SELECT cbd.category_brand_detail_id, cbd.name, cbd.image, cbd.image, (CONCAT('"+process.env.RESIZE_URL+"',cbd.image)) as image_url FROM category_brand_details as cbd WHERE cbd.category_brand_id="+data.category_brand_id+" AND cbd.language_id="+data.app_language_id+" ",{ type: Sequelize.QueryTypes.SELECT});
			brand = brands[0];

		}
		else
		{

			var etoken_sum = await Db.organisation_coupon_users.sum("quantity_left", {

				where: {
					customer_user_detail_id: data.user_detail_id
				}					

			});
			if(!etoken_sum)
				etoken_sum = 0;


		}

		var result = {
			etoken_sum,
			brand
		};

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Payment details"), result });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

///////////////////		User History 	/////////////////////////////////////////////////
exports.userHistory = async (request, response) => {
	try{

		var data = request.body;

		request.checkBody("type", response.trans("Type id is required")).notEmpty();
		request.checkBody("skip", response.trans("Skip parameter is required")).notEmpty();
		request.checkBody("take", response.trans("Take parameter is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		data.skip = parseInt(data.skip);
		data.take = parseInt(data.take);

		if(data.type == "1")
		{//////////		Past 	//////////////////////////
			data.order_status_array = ["CustCancel", "DriverCancel", "SupComplete", "SerComplete", "SerCustCancel", "SerCustConfirm", "CTimeout"];
		}//////////		Past 	//////////////////////////
		else
		{//////////		Upcoming 	//////////////////////
			data.order_status_array = ["SerAccept", "Scheduled", "DPending", "DApproved", "Confirmed", "SerCustPending", "eTokenSerStart", "Ongoing"];
		}//////////		Upcoming 	//////////////////////

		var orders = await Services.orderServices.userPastUpcomingOListings(data.user_detail_id, data.skip, data.take, data.order_status_array, "Service",data);

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Orders listing"), result: orders });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

//////////////////			Order Details 		///////////////////////////////////////////////
exports.userOrderDetails = async (request, response) => {
	try{
		var data = request.body;
		request.checkBody("order_id", response.trans("Order id is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
		 return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var orders = await Services.orderServices.customerRideDetails(data);
		 console.log(orders.length + "== 0" + orders[0].customer_user_detail_id+ " != " + data.user_detail_id);
		
		if(orders.length == 0 || orders[0].customer_user_detail_id != data.user_detail_id )
		 return response.status(400).json({ success: 0, statusCode: 400, msg:  response.trans("Sorry, this order is currently not available") });

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Order details"), result: orders[0] });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

////////////////////