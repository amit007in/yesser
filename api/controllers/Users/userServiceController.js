var moment = require("moment");
var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;

var Libs = require(appRoot + "/libs");//////////////////		Libraries
var Services = require(appRoot + "/services");///////////		Services
var Db = require(appRoot + "/models");//////////////////		Db Models
var Configs = require(appRoot + "/configs");/////////////		Configs

//var commonFunctions = require(appRoot+"/libs/commonFunctions");//////////////////		Libraries
/////////////////////////////		Service Drivers Listings		///////////////////////////////
exports.homeApi = async (request, response) => {
    try {
        var data = request.body;
        request.checkBody("category_id", response.trans("Service type is required")).notEmpty();
        request.checkBody("latitude", response.trans("Latitude is required")).notEmpty();
        request.checkBody("longitude", response.trans("Longitude is required")).notEmpty();
        request.checkBody("distance", response.trans("Distance is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});
        // get category type from database
        var category_type = await Services.categoryServices.getCategoryTypeById(data);
        if (category_type[0]["category_type"] == "Service") {
            data.user_type_id = 2;
            data.category_type = "Service";
			data.quotation_timeout = category_type[0]["interval_time"]; //Add M2 Part 2
			data.geofencing_status = category_type[0]["geofencing_status"]; //Add M2 Part 2
            var categories = await Services.categoryServices.singleServiceCatDetails(data);
        } else {
            data.user_type_id = 3;
            data.category_type = "Support";
			data.quotation_timeout = "";
			data.geofencing_status = 0;
            var categories = [];
        }
        data.past_5Mins = moment(data.created_at, "YYYY-MM-DD HH:mm:ss").subtract(2, "days").format("YYYY-MM-DD HH:mm:ss");
        var drivers             = await Services.driverListingServices.userHomeMapDrivers(data);
		var floors              = await Db.floor_margins.findOne(); //M3
		var rewards             = await Db.reward_points.findOne(); //M3
        var currentOrders       = await Services.orderServices.customerCurrentOrders(data);
        var lastCompletedOrders = await Services.orderServices.customerLastCompletedOrders(data.user_detail_id, data.app_language_id);
		//console.log("categories======",JSON.stringify(categories));
        var result = {
            data,
            categories,
            drivers,
            //Details,
            floors, //M3
            rewards, //M3
            currentOrders,
            lastCompletedOrders,
            Versioning: Configs.appData.Versioning
        };
        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Service drivers listings"), result});

    } catch (e) {
        console.log("error home api ==", e);
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

////////////////////////		Order Timeout 		/////////////////////////////////////
exports.userTesting = async (request, response) => {
    try {

        var data = request.body;

        return response.status(200).json({success: 1, statusCode: 200, msg: data});

    } catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

//////////////////////		User Requests Service 		////////////////////////////////////
exports.userServiceRequest = async (request, response) => {
    try {
        var data = request.body;
		
        var Details = request.uDetails;
		console.log("Details=====",Details);
        data.user_detail_id = Details.user_detail_id; 
        data.user_id = Details.user_id;
        data.access_token = Details.access_token;
        data.app_language_id = request.app_language_id;
        data.user_latitude  = Details.latitude; //Add M3 Part 3.2
        data.user_longitude = Details.longitude; //Add M3 Part 3.2
        data.user_name = Details.user.name;
        data.created_at = request.created_at;
		
		data.request_type = data.request_type ? data.request_type : 'SingleRequest';
		
        request.checkBody("category_id", response.trans("Category id is required")).notEmpty();
        request.checkBody("category_brand_id", response.trans("Category brand id is required")).notEmpty();
        //request.checkBody("products.*.category_brand_product_id", response.trans("Category brand product is required")).notEmpty();
        //request.checkBody("products.*.product_quantity", response.trans("Product quantity is required")).notEmpty();
        //if (data.category_id != 5) {
            request.checkBody("dropoff_address", response.trans("Dropoff address is required")).notEmpty();
            request.checkBody("dropoff_latitude", response.trans("Dropoff latitude is required")).notEmpty();
            request.checkBody("dropoff_longitude", response.trans("Dropoff longitude is required")).notEmpty();
        //}
        if (data.category_id && data.category_id == 4 && data.category_id == 5) {
            request.checkBody("pickup_address", response.trans("Pickup address is required")).notEmpty();
            request.checkBody("pickup_latitude", response.trans("Pickup latitude is required")).notEmpty();
            request.checkBody("pickup_longitude", response.trans("Pickup longitude is required")).notEmpty();
        }
		if(data.request_type == "SingleRequest")
		{
			request.checkBody("order_timings", response.trans("Order timings is required")).notEmpty();
		}
        console.log("data=====",data);
        request.checkBody("future", response.trans("Future parameter is required")).notEmpty();
        request.checkBody("payment_type", response.trans("Payment type is required")).notEmpty();
        request.checkBody("payment_type", response.trans("Payment type can either be 'Cash' or 'Card' or 'eToken'")).isIn(["Card", "Cash", "eToken"]);
        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});
        data.category_brand_product_id = data.category_brand_product_id;
        if (data.category_id != 1 && data.category_id != 3 && data.category_id != 2 && data.category_id != 4 && data.category_id != 5 && data.category_id != 16)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, currently only gas, Drinking water, Water tanker and Truck services are available")});
		
		/*if (data.category_id == 1)
		{
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, gas service is temporarily stopped !!. we will be back with something new")});
		}*/
        var coupon_id = data.coupon_id ? data.coupon_id : '0';
        if (coupon_id != '0') {
            var max_rides = await Services.userServices.checkPromoUserMaxRides(data);
            if (max_rides != null) {
                var max_ridess = JSON.parse(JSON.stringify(max_rides));
                var OldRides = max_ridess.max_rides;
                if (OldRides == 0) {
                    return response.status(406).json({success: 0, statusCode: 406, msg: response.trans("Coupon maximum limit reached."), result: {}});
                }
            }
        }
		
        ////////////        Cat Details         ///////////////////////
        var CateGoryProducts = Array();
        var JsonProducts = JSON.parse(data.products);
        var orderQuzntity = '0';
        for (let j = 0; j < JsonProducts.length; j++) {
            orderQuzntity = parseInt(orderQuzntity) + parseInt(JsonProducts[j].product_quantity);
            CateGoryProducts.push(JsonProducts[j].category_brand_product_id);
        }
        data.CateGoryProducts = CateGoryProducts;
		console.log(data.CateGoryProducts);
        data.category = await Services.categoryBrandProductServices.CatDetailsAllLang(data);
        if (!data.category)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, Please check your parameters")});
        //data.distance = parseFloat(data.category.maximum_distance);
		//Add M2 Part 2
		if(parseFloat(data.category.Brand.is_default) == 1)
        {
            data.distance = parseFloat(data.category.maximum_distance);
        }
        else
        {
            data.distance = parseFloat(data.category.Brand.maximum_radius);
        }
        ////////////        Cat Details         ///////////////////////
        data.user_type_id = data.driver_user_type_id = 2;
        data.order_type = "Service";
        data.product_quantity = parseInt(orderQuzntity);
        data.seller_user_type_id = 2;
        ///////////////////////         Future Ride Check Add M3 Part 3.2       ///////////////////////////////////////
        if (data.future == "1" && data.request_type == "SingleRequest") {
            data.or_1hr = moment(data.now, "YYYY-MM-DD HH:mm:ss").add(55, "minutes");
            var isBefore = moment(data.order_timings, "YYYY-MM-DD HH:mm:ss").isBefore(data.or_1hr);
            if (isBefore && data.category_id == 2)
                return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Please select timing one hour after from now for scheduling a service")});

            var scheduled_check = await Db.orders.findOne({
                where: {
                    customer_user_id: data.user_id,
                    customer_user_detail_id: data.user_detail_id,
                    order_status: {$in: ["Scheduled", "DPending", "DApproved"]}
                }
            });
            //if(scheduled_check && scheduled_check.order_type == "Service")
            //return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, you already have scheduled a service"), scheduled_check });
            //  else if(scheduled_check && scheduled_check.order_type == "Support")
            if (scheduled_check && scheduled_check.order_type == "Support")
                return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, you already have scheduled a support"), scheduled_check});
        }
		var newsOrderarry = [];
		if(data.request_type == "ContinueRequest")
		{
			data.order_timings = data.continouos_startdt;
		}
		if(data.request_type == "NonContinueRequest")
		{
			 var NonCOrders = JSON.parse(data.NonContinueSOrder);
			 data.order_timings = NonCOrders[0];
		}
		
        ///////////////////////         Future Ride Check       ///////////////////////////////////////
        data.organisation_coupon_user_id = 0;
        // //////////////////////// Coupon Check    //////////////////////////////////
        if (data.coupon_user_id) {
            var coupon_users = await Services.couponUserServices.purchasedCouponDetail4Order(data);
            if (!coupon_users)
                return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this coupon is no longer valid")});
        } else {
            data.coupon_user_id = 0;
        }
		////////////////////////    Reward Point Update Part M3.2 ////////////////////////////
		/*var userDetails      = await Db.user_details.findOne({ where: {user_detail_id: Details.user_detail_id}});
		var rewardpoint      = userDetails.total_reward_point; 
		var redemmedpoint    = data.reward_discount_point ? data.reward_discount_point : 0;
		if(parseInt(rewardpoint) < parseInt(redemmedpoint))
		{
			return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, you do not have enough reward point to reedem")});
		}*/
		///////////////////////   Reward Point ////////////////////////////
        ////////////////////////    Coupon Check    //////////////////////////////////
        //Start 25-April-2019
        var cc = Array();
        var final_charge = '0.0';
        for (let i = 0; i < JsonProducts.length; i++) {
            final_charge = parseFloat(final_charge) + parseFloat(JsonProducts[i].price_per_item);
            data.product_actual_value = JsonProducts[i].price_per_item;
            data.product_alpha_charge = data.category.Brand.Product.alpha_price;
            data.product_per_quantity_charge = data.category.Brand.Product.price_per_quantity;
            data.product_per_weight_charge = data.category.Brand.Product.price_per_weight;
            data.product_per_distance_charge = data.category.Brand.Product.price_per_distance;
            data.product_per_hr_charge = data.category.Brand.Product.price_per_hr;
            data.product_per_sq_mt_charge = data.category.Brand.Product.price_per_sq_mt;
            data.bottle_returned_value = data.bottle_returned_value ? data.bottle_returned_value : 0;
            cc.push(JsonProducts[i].category_brand_product_id);
        }
        var CategoryProductsID = cc.toString();
        data.CategoryProductsID = CategoryProductsID;
        //End 25-April-2019
		if(data.driver_user_detail_id == "null")
		{
			var driver_user_detail_id = '0';
		}else{
			var driver_user_detail_id = data.driver_user_detail_id ? data.driver_user_detail_id : '0';
		}
		
		if(driver_user_detail_id == '0')
		{
			if (data.category_id == 2) { ////////////       Drinking water Service  ///////////////
				data.day_string_check = Libs.commonFunctions.DayString(data.order_timings, data.timezone);
				data.drivers = await Services.driverListingServices.AllDrivers4OrdersDrinkingWater(data);
				data.coupon_user_id = 0;
			} else {////////////       Other Service   ///////////////////////
				if (data.category_id && (data.category_id == 4 || data.category_id == "4")) {
					data.drivers = await Services.driverListingServices.Driver4TruckOrders(data);
					console.log('singledoubledrivers=====',data.drivers);
				} else {
					data.drivers = await Services.driverListingServices.Driver4OrdersExceptDrinkingWater(data);
				}
			}
		}
		else
		{
			data.drivers = await Services.driverListingServices.SingleDriver4TruckOrders(driver_user_detail_id);
			console.log('singledrivers=====',data.drivers);
		}
		
        ///////////////////         No Driver Found SMS     ////////////////////////
        data.timings = moment(data.order_timings, "YYYY-MM-DD HH:mm:ss").tz("Asia/Muscat").format("YYYY-MM-DD hh:mm:ss");
        data.sms_message = "Alert:  New " + data.category.category_details[0].name + " Order, Product: " + data.category.Brand.Product.category_brand_product_details[0].name + " x " + data.product_quantity + ", Name: " + Details.user.name + ", Mobile: " + Details.user.phone_number + ", Order date: " + data.timings;
        data.order_token = "OR-" + Libs.commonFunctions.uniqueId();
        ///////////     Customer    /////////////////
        data.customer_user_id = Details.user_id;
        data.customer_user_detail_id = Details.user_detail_id;
        data.customer_organisation_id = Details.organisation_id;
        data.customer_user_type_id = Details.user_type_id;
        data.user_card_id = 0;
        ///////////     Customer    /////////////////
        data.order_status = "Searching",
        data.payment_status = "Pending",
        data.refund_status = "NoNeed",
        data.created_by = "User";
        data.order_distance = data.order_distance ? parseFloat(data.order_distance) : 0;
        data.order_time = 0;
        data.product_sq_mt = 0;
        data.product_weight = data.product_weight ? parseFloat(data.product_weight) : 0;
        data.buraq_percentage = parseFloat(data.category.buraq_percentage);
        //////////////////////      YesSer Percentage        ////////////////////////
        data.product_actual_value = data.category.Brand.Product.actual_value;
        data.product_alpha_charge = data.category.Brand.Product.alpha_price;
        data.product_per_quantity_charge = data.category.Brand.Product.price_per_quantity;
        data.product_per_weight_charge = data.category.Brand.Product.price_per_weight;
        data.product_per_distance_charge = data.category.Brand.Product.price_per_distance;
        data.product_per_hr_charge = data.category.Brand.Product.price_per_hr;
        data.product_per_sq_mt_charge = data.category.Brand.Product.price_per_sq_mt;
        data.bottle_returned_value = data.bottle_returned_value ? data.bottle_returned_value : 0;
		
       
		
        data.bottle_charge = 0;
        //////////////////      Pricing Category Wise       ///////////////////////////////////////
        if (data.category_id == 1)
        {/////////////////////      Gas         ///////////////////////////////////////////////
            //data.initial_charge = parseFloat(data.product_alpha_charge) + (parseFloat(data.product_per_quantity_charge) * parseFloat(data.product_quantity));
			data.initial_charge = parseFloat(data.product_alpha_charge) + parseFloat(final_charge);
        }/////////////////////      Gas         ///////////////////////////////////////////////
        else
        {
            //data.initial_charge = parseFloat(data.product_alpha_charge) + (parseFloat(data.product_per_quantity_charge) * parseFloat(data.product_quantity)) + (parseFloat(data.product_per_distance_charge) * parseFloat(data.order_distance)) + (parseFloat(data.product_per_hr_charge) * parseFloat(data.order_time)) + (parseFloat(data.product_per_sq_mt_charge) * parseFloat(data.product_sq_mt)) + (data.bottle_charge);
			data.initial_charge = parseFloat(data.product_alpha_charge) + parseFloat(final_charge) + (parseFloat(data.product_per_distance_charge) * parseFloat(data.order_distance)) + (parseFloat(data.product_per_hr_charge) * parseFloat(data.order_time)) + (parseFloat(data.product_per_sq_mt_charge) * parseFloat(data.product_sq_mt)) + (data.bottle_charge);
	    }
        data.coupon_charge = 0;
        data.final_charge = data.initial_charge;
        data.admin_charge = parseFloat(((data.buraq_percentage * data.final_charge) / 100).toFixed(3));
        data.final_charge = parseFloat((data.final_charge + data.admin_charge).toFixed(3));
		
        var order = await Services.orderServices.latestCreateNewRow(data);
        data.order_id = order.order_id;
        var pp = Array();
        for (let i = 0; i < JsonProducts.length; i++)
        {
            var productss = Array();
            productss.order_id = data.order_id;
            productss.category_id = data.category_id;
            productss.category_brand_id = data.category_brand_id;
            productss.category_brand_product_id = JsonProducts[i].category_brand_product_id;
            productss.product_weight = JsonProducts[i].product_weight;
            productss.price_per_item = JsonProducts[i].price_per_item;
            //productss.image_url = JsonProducts[i].image_url;
            productss.image_url = "";
            productss.productName = JsonProducts[i].productName;
            productss.product_quantity = JsonProducts[i].product_quantity;
			productss.gift_offer      = JsonProducts[i].gift_offer;
            productss.created_at = data.created_at;
            productss.updated_at = data.created_at;
            productss.app_language_id = data.app_language_id;
            await Services.orderServices.CreateProducts(productss);
        }
        //End 25-April-2019
        var order_images = [];
        var order_images_url = [];
        if (request.files && request.files.length > 0) {
            request.files.forEach(async (image) => {
                order_images.push({
                    order_id: data.order_id,
                    image: image.filename,
                    created_at: data.created_at,
                    updated_at: data.created_at
                });
                order_images_url.push({
                    order_id: data.order_id,
                    image: image.filename,
                    image_url: process.env.RESIZE_URL + image.filename
                });
            });
        }
        if (order_images.length > 0)
            await Db.order_images.bulkCreate(order_images);
		
		
		//Start Create Multiple Orders //Add M3 Part 3.2
		if(data.request_type == "ContinueRequest")
		{
			data.SchulderdOrderToken = "SR-" + Libs.commonFunctions.uniqueId();
			await Db.orders.update(
			{
				schulderd_order_token: data.SchulderdOrderToken
			},
			{
				where: {
					order_id: data.order_id
				}
			});
			var startDate = moment(data.continouos_startdt);
			var endDate   = moment(data.continuous_enddt);
			var result    = endDate.diff(startDate, 'days',true);
			var newsdate = data.continouos_startdt;
			for(let s = 1; s <= result; s++)
			{
				var newsr         = Array();
				newsr.order_token = "OR-" + Libs.commonFunctions.uniqueId();
				var new_date      = moment(newsdate, "YYYY-MM-DD HH:mm:ss").add(1, 'days').format("YYYY-MM-DD hh:mm:ss");
				newsr.timings     = new_date;
				data.nonSorder    = "[]";
				var Sorder        = await Services.orderServices.ScheduleCreateNewRow(data,newsr);
				var SorderId      = Sorder.order_id;
				var pp = Array();
				for (let i = 0; i < JsonProducts.length; i++)
				{
					var productss                       = Array();
					productss.order_id                  = SorderId;
					productss.category_id               = data.category_id;
					productss.category_brand_id         = data.category_brand_id;
					productss.category_brand_product_id = JsonProducts[i].category_brand_product_id;
					productss.product_weight            = JsonProducts[i].product_weight;
					productss.price_per_item            = JsonProducts[i].price_per_item;
					productss.image_url                 = "";
					productss.productName      			= JsonProducts[i].productName;
					productss.product_quantity 			= JsonProducts[i].product_quantity;
					productss.gift_offer       			= JsonProducts[i].gift_offer;
					productss.created_at      			= data.created_at;
					productss.updated_at      			= data.created_at;
					productss.app_language_id  			= data.app_language_id;
					await Services.orderServices.CreateProducts(productss);
				}
				var Sorder_images = [];
				var Sorder_images_url = [];
				if (request.files && request.files.length > 0) {
					request.files.forEach(async (image) => {
						Sorder_images.push({
							order_id: SorderId,
							image: image.filename,
							created_at: data.created_at,
							updated_at: data.created_at
						});
						Sorder_images_url.push({
							order_id: SorderId,
							image: image.filename,
							image_url: process.env.RESIZE_URL + image.filename
						});
					});
				}
				if (Sorder_images.length > 0)
					await Db.order_images.bulkCreate(Sorder_images);
				
				newsdate = new_date;
			}
		}
		//End Create Multiple Orders
		
		//Start Non Continue Create Multiple Orders //Add M3 Part 3.2
		var NonCOrders = [];
		if(data.request_type == "NonContinueRequest")
		{
			data.SchulderdOrderToken = "SR-" + Libs.commonFunctions.uniqueId();
			await Db.orders.update(
			{
				schulderd_order_token: data.SchulderdOrderToken
			},
			{
				where: {
					order_id: data.order_id
				}
			});
			
			var NonCOrders = JSON.parse(data.NonContinueSOrder);
			for(var e = 0; e < NonCOrders.length; e++)
			{
				newsOrderarry.push({
					order_id: data.order_id,
					schudled_date: NonCOrders[e],
					scheduled_token: data.SchulderdOrderToken,
					created_at: data.created_at
				});
			}
			if (newsOrderarry.length > 0)
				await Db.order_noncontinues_dates.bulkCreate(newsOrderarry);

			for(let s = 1; s < NonCOrders.length; s++)
			{
				var newsr         = Array();
				newsr.order_token = "OR-" + Libs.commonFunctions.uniqueId();
				var new_date      = NonCOrders[s];
				newsr.timings     = new_date;
				var Sorder        = await Services.orderServices.ScheduleCreateNewRow(data,newsr);
				var SorderId      = Sorder.order_id;
				var pp = Array();
				for (let i = 0; i < JsonProducts.length; i++)
				{
					var productss                       = Array();
					productss.order_id                  = SorderId;
					productss.category_id               = data.category_id;
					productss.category_brand_id         = data.category_brand_id;
					productss.category_brand_product_id = JsonProducts[i].category_brand_product_id;
					productss.product_weight            = JsonProducts[i].product_weight;
					productss.price_per_item            = JsonProducts[i].price_per_item;
					productss.image_url                 = "";
					productss.productName      			= JsonProducts[i].productName;
					productss.product_quantity 			= JsonProducts[i].product_quantity;
					productss.gift_offer       			= JsonProducts[i].gift_offer;
					productss.created_at      			= data.created_at;
					productss.updated_at      			= data.created_at;
					productss.app_language_id  			= data.app_language_id;
					await Services.orderServices.CreateProducts(productss);
				}
				var Sorder_images = [];
				var Sorder_images_url = [];
				if (request.files && request.files.length > 0) {
					request.files.forEach(async (image) => {
						Sorder_images.push({
							order_id: SorderId,
							image: image.filename,
							created_at: data.created_at,
							updated_at: data.created_at
						});
						Sorder_images_url.push({
							order_id: SorderId,
							image: image.filename,
							image_url: process.env.RESIZE_URL + image.filename
						});
					});
				}
				if (Sorder_images.length > 0)
					await Db.order_images.bulkCreate(Sorder_images);

			}
		}
		//End Non Continue Create Multiple Orders
		
		if (data.drivers.length == 0 && process.env.NODE_ENV != "development") {
            data.status = "Driver not found";
			await Db.orders.update(
				{
					order_status: data.status,
					payment_status: data.status,
					cancelled_by: "Not Found",
					updated_at: data.created_at
				},
				{
					where: {
						order_id: data.order_id
					}
				}
			 );
		 
			//Add M3 Part 3.2
			if(data.request_type != "SingleRequest")
			{
				var stoken   = data.SchulderdOrderToken;
				await Db.orders.update(
					{
						order_status: data.status,
						cancel_reason: data.status,
						cancelled_by: "Not Found",
						updated_at: data.created_at
					},
					{
						where: {schulderd_order_token: stoken}
					}
				);
			}

				//return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, no driver is currently available. Admin will soon assign a driver.")});
				return response.status(400).json({success: 0, statusCode: 400, msg: "Your order is approved by YesSer . kindly wait for a moment until YesSer assigns you a driver . Feel free to contact +968-24 453336 for any more assistance."});
			
            
        }
		var adminRoundRobin = await Db.admins.findOne({ where: { admin_id: '1' } });
		if(adminRoundRobin.round_robin == "true")
		{
			data.order_request_timeout = adminRoundRobin.round_robin_time;
			var total_driveer = order.fcm_socket_ids.length;
			await Db.orders.update({
					order_request_timeout: data.order_request_timeout,
					total_request_driver: order.fcm_socket_ids.length
			},
			{
				where: {order_id: data.order_id}
			});
			if(data.request_type != "SingleRequest")
			{ 
				var stoken   = data.SchulderdOrderToken;
				await Db.orders.update(
					{
						order_request_timeout: data.order_request_timeout,
						total_request_driver: order.fcm_socket_ids.length
					},
					{
						where: {schulderd_order_token: stoken}
					}
				);
			}
		}
		else
		{
			data.order_request_timeout = 47;
			var total_driveer = 1;
			await Db.orders.update({
					order_request_timeout: data.order_request_timeout,
					total_request_driver: 1
			},
			{
				where: {order_id: data.order_id}
			});
		}
		//Update order Timeout
		
		
		//Add Order Notification
		var orderServiceMessage = response.trans(" has requested a new service");
		var user_order_notification = await Db.user_order_notifications.create({
			order_id: data.order_id,
			sender_user_detail_id: data.customer_user_detail_id,
			receiver_user_detail_id: 0,
			is_read: "0",
			message: Details.user.name+orderServiceMessage,
			type: "SerRequest",
			created_at: data.created_at
		});
		//End Order Notification
        //////////////////      Drivers Push Plus Socket    ////////////////////////////
        if (order.fcm_socket_ids.length > 0) {
			var Driverusers = await Db.user_details.findOne({where: {user_detail_id: order.fcm_socket_ids[0].driver_user_detail_id}});
            var push_data = {
                order_id: (data.order_id).toString(),
                type: "OrderEvent",
                user_id: (data.customer_user_id).toString(),
                user_detail_id: (data.customer_user_detail_id).toString(),
                expiry: moment().utc().add(45, "seconds").format("YYYY-MM-DD HH:mm:ss")
            };
			var quotation_id = data.quotation_id ? data.quotation_id : '';
			var reward_discount_price = data.reward_discount_price ? data.reward_discount_price : 0.0; //Add M3 Part 3.2
			var floor_charge          = data.floor_charge ? data.floor_charge : 0.0; //Add M3 Part 3.2
            var products = await Services.orderServices.AllOrderProducts(data);
            var socket_data = {
                type: "SerRequest",
                order: {
					release_offer_id: '', 
					quotation_id: quotation_id,  
					reward_discount_price: reward_discount_price,  //Add M3 Part 3.2
					floor_charge: floor_charge,  //Add M3 Part 3.2
                    order_id: data.order_id,
                    driver_user_detail_id: order.driver_user_detail_id,
                    order_timings: data.order_timings,
                    continouos_startdt: data.continouos_startdt ? data.continouos_startdt : "", //Add M3 Part 3.2
                    continuous_enddt: data.continuous_enddt ? data.continuous_enddt : "", //Add M3 Part 3.2
                    request_type: data.request_type ? data.request_type : "", //Add M3 Part 3.2
                    NonContinueSOrder: NonCOrders, //Add M3 Part 3.2
                    order_request_timeout: data.order_request_timeout, //Add M3 Part 3.2
                    total_request_driver: total_driveer, //Add M3 Part 3.2
                    pickup_address: order.pickup_address,
                    pickup_latitude: parseFloat(order.pickup_latitude),
                    pickup_longitude: parseFloat(order.pickup_longitude),
                    dropoff_address: order.dropoff_address,
                    dropoff_latitude: parseFloat(order.dropoff_latitude),
                    dropoff_longitude: parseFloat(order.dropoff_longitude),
                    payment: order.payment,
                    organisation_coupon_user_id: data.organisation_coupon_user_id,
                    future: data.future,
                    category_id: data.category_id,
                    brand: {
                        category_id: data.category_id,
                        category_brand_id: data.category_brand_id,
                        category_brand_product_id: data.category_brand_product_id,
                    },
                    user: {
                        user_id: data.customer_user_id,
                        user_detail_id: data.customer_user_detail_id,
                        name: Details.user.name,
                        rating_count: Details.user.rating_count,
                        rating_avg: Details.user.ratings_avg,
                        profile_pic: Details.profile_pic,
                        profile_pic_url: Details.profile_pic_url,
                        phone_number: Details.user.phone_number,
                        phone_code: Details.user.phone_code
                    },
                    coupon_users: coupon_users ? coupon_users : null,
                    order_images_url,
                    details: order.details,
                    material_details: order.material_details,
                    products: products
                }
            };
			var adminRoundRobin = await Db.admins.findOne({ where: { admin_id: '1' } });
			if(adminRoundRobin.round_robin == "true")
			{
				Libs.commonFunctions.pushReassignOrder(Driverusers, push_data, socket_data, order, data.category, "OrderEvent");
			}
			else
			{
				Libs.commonFunctions.pushPlusEventMultipleUsers(order.fcm_socket_ids, push_data, socket_data, order, data.category, "OrderEvent");
			}
            
        }
        //////////////////      Drivers Push Plus Socket    ////////////////////////////
        //////////      No Response Timeout After 2 Minutes     ///////////
        if (data.future == "0")
        {
			console.log("future zero");
            //data.timeOutMessage = response.trans("Sorry, no driver is currently available. Admin will soon assign a driver.");
            data.timeOutMessage = "Your order is approved by YesSer . kindly wait for a moment until YesSer assigns you a driver . Feel free to contact +968-24 453336 for any more assistance.";
			//var TorderId;
            global.TorderId = setTimeout(function () {
                orderRequestedTimeout(data);// this code will only run when time has ellapsed
            }, data.order_request_timeout * 1000);
        }
		if(adminRoundRobin.round_robin == "true" && data.future == "1")
		{
			console.log("future one");
			//data.timeOutMessage = response.trans("Sorry, no driver is currently available. Admin will soon assign a driver.");
			data.timeOutMessage = "Your order is approved by YesSer . kindly wait for a moment until YesSer assigns you a driver . Feel free to contact +968-24 453336 for any more assistance.";
            global.TorderId = setTimeout(function () {
                orderRequestedTimeout(data);
            }, data.order_request_timeout * 1000);
		}
        //////////      No Response Timeout After 2 Minutes     ///////////
        data.socket_ids = order.socket_ids;
        data.fcm_ids = order.fcm_ids;
        var order = await Services.orderServices.latestUserBookedOrderDetails(data);
        order = order[0];
        ///////////////////     Deduct Etokens, Coupon Users    ////////////////////////////////
        if (order.organisation_coupon_user)
        {
            order.organisation_coupon_user.quantity_left = order.organisation_coupon_user.quantity_left - data.product_quantity;
            Services.orgCouponUserServices.updateOrgCouponUser(order.organisation_coupon_user.organisation_coupon_user_id, order.organisation_coupon_user.quantity_left, data.created_at);
        }
		order.release_offer_id = '';
		order.quotation_id = data.quotation_id ? data.quotation_id : '';
        
        ///////////////////     Deduct Etokens, Coupon Users    ////////////////////////////////
        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Service requested successfully"), result: order, data});

    } catch (e)
    {
        console.log("e ===", e);
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};



exports.userServiceRequests = async (request, response) => {
    try {
        var data = request.body;
        console.log("Request api =>", request.files);
    } catch (e)
    {
        console.log("e ===", e);
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

exports.timeordercase = async (data) => {
	orderRequestedTimeout(data);
};


async function orderRequestedIntervalTime(data)
{
	orderRequestedTimeout(data);
}

///////////////////////		Order Request Timeout 		/////////////////////////////////
async function orderRequestedTimeout(data)
{
    try {
		console.log("asdsdfsdfsdfsfdsf=========================");
        var order = await Services.orderServices.currentOrderTimeoutDetails(data.order_id);
        if (!order || order.order_status != "Searching")
            return {success: 1, statusCode: 200, msg: "No need for automatic timeout", order};

        order = JSON.parse(JSON.stringify(order));

		//Round Robin Enable/Disable Functionality 
		var adminRoundRobin = await Db.admins.findOne({ where: { admin_id: '1' } });
		console.log("round robin false",adminRoundRobin.round_robin);
		if(adminRoundRobin.round_robin == "true")
		{
			console.log("round robin true");
			data.status = "SerTimeout";
			var PrevoiusDriverId = await Db.order_requests.findOne({where: {order_id: order.order_id,order_request_status: {$in: ["Searching"]}},order: [["distance_customer", "ASC"]]});
			await Db.order_requests.update(
				{
					order_request_status: data.status,
					driver_request_status: '1',
					updated_at: data.created_at
				},
				{
					where: {
						order_request_id: PrevoiusDriverId.order_request_id						
					}
				}
			);
			var DriverId = await Db.order_requests.findOne({where: {order_id: data.order_id,driver_request_status: '0',order_request_status: {$in: ["Searching","SerTimeout"]}},order: [["distance_customer", "ASC"]]});
			if(DriverId)
			{
				global.TorderId = setTimeout(function () {
					orderRequestedIntervalTime(data);
				}, adminRoundRobin.round_robin_time * 1000);
				
				var order            = await Db.orders.findOne({where: {order_id: data.order_id}});
				var Driverusers      = await Db.user_details.findOne({where: {user_detail_id: DriverId.driver_user_detail_id}});
				var orderProducts    = await Db.order_products.findAll({ where: {order_id: data.order_id}});
				var orderprodducts   = JSON.stringify(orderProducts);
				var CateGoryProducts = Array();
				for(var i =0;i<orderProducts.length;i++)
				{
					CateGoryProducts.push(orderProducts[i].category_brand_product_id);
				}
				order.CateGoryProducts = CateGoryProducts;
				order.category         = await Services.categoryBrandProductServices.CatDetailsAllLang(order);
				var customerdetails    = await Db.user_details.findOne({ 
					where: {"user_detail_id": order.customer_user_detail_id},
					attributes: [
						"user_detail_id", "user_id", "user_type_id", "language_id", "category_id", "category_brand_id", "profile_pic", "access_token", "latitude", "longitude", "mulkiya_number", "mulkiya_front", "mulkiya_back", "mulkiya_validity","approved",
						[Sequelize.literal("CASE WHEN mulkiya_front='' THEN '' ELSE CONCAT('"+process.env.RESIZE_URL+"', mulkiya_front) END"), "mulkiya_front_url"],
						[Sequelize.literal("CASE WHEN mulkiya_back='' THEN '' ELSE CONCAT('"+process.env.RESIZE_URL+"', mulkiya_back) END"), "mulkiya_back_url"],
						[Sequelize.literal("CASE WHEN profile_pic='' THEN '' ELSE CONCAT('"+process.env.RESIZE_URL+"', profile_pic) END"), "profile_pic_url"]
					],
					include: [
						{
							model: Db.users,
							attributes: { exclude: ["blocked", "created_by", "created_at", "updated_at"] }
						}
					]
				});
				var payment     = await Db.payments.findOne({where: {order_id: order.order_id}});
				var orderImages = await Db.order_images.findOne({where: {order_id: order.order_id}});
				if(!orderImages)
				{
					var orderImages = [];
				}
				var push_data = {
	                order_id: (order.order_id).toString(),
	                type: "OrderEvent",
	                user_id: (order.customer_user_id).toString(),
	                user_detail_id: (order.customer_user_detail_id).toString(),
	                expiry: moment().utc().add(45, "seconds").format("YYYY-MM-DD HH:mm:ss")
	            };
				var quotation_id          = order.quotation_id ? order.quotation_id : '';
				var reward_discount_price = order.reward_discount_price ? order.reward_discount_price : 0.0; 
				var floor_charge          = order.floor_charge ? order.floor_charge : 0.0; 
	            var products              = await Services.orderServices.AllOrderProducts(order);
	            var socket_data = {
	                type: "SerRequest",
	                order: { 
						release_offer_id: '',
						quotation_id: quotation_id,
						reward_discount_price: reward_discount_price,
						floor_charge: floor_charge,
	                    order_id: order.order_id,
	                    driver_user_detail_id: DriverId.driver_user_detail_id,
	                    order_timings: order.order_timings,
	                    continouos_startdt: order.continouos_startdt ? order.continouos_startdt : "",
	                    continuous_enddt: order.continuous_enddt ? order.continuous_enddt : "",
	                    request_type: order.request_type ? order.request_type : "",
	                    NonContinueSOrder: [],
						order_request_timeout: order.order_request_timeout, //Add M3 Part 3.2
						total_request_driver: order.total_request_driver, //Add M3 Part 3.2
	                    pickup_address: order.pickup_address,
	                    pickup_latitude: parseFloat(order.pickup_latitude),
	                    pickup_longitude: parseFloat(order.pickup_longitude),
	                    dropoff_address: order.dropoff_address,
	                    dropoff_latitude: parseFloat(order.dropoff_latitude),
	                    dropoff_longitude: parseFloat(order.dropoff_longitude),
	                    payment: payment,
	                    organisation_coupon_user_id: order.organisation_coupon_user_id,
	                    future: order.future,
	                    category_id: order.category_id,
	                    brand: {
	                        category_id: order.category_id,
	                        category_brand_id: order.category_brand_id,
	                        category_brand_product_id: order.category_brand_product_id,
	                    },
	                    user: {
							user_id: order.customer_user_id,
							user_detail_id: order.customer_user_detail_id,
							name: customerdetails.user.name,
							rating_count: 0,
							rating_avg: 0,
							profile_pic: customerdetails.profile_pic,
							profile_pic_url: customerdetails.profile_pic_url,
							phone_number: customerdetails.user.phone_number,
							phone_code: customerdetails.user.phone_code
						},
	                    coupon_users: null,
	                    orderImages,
	                    details: order.details,
	                    material_details: order.material_details,
	                    products: products
	                }
	            };
	            Libs.commonFunctions.pushReassignOrder(Driverusers, push_data, socket_data, order, order.category, "OrderEvent");
			}
			else
			{
				console.log("round robin second false");
				data.timeOutMessage = "Sorry, no driver is currently available. Admin will soon assign a driver.";
				data.status = "SerTimeout";
				data.update_order = await Db.orders.update(
						{
							order_status: data.status,
							payment_status: data.status,
							cancelled_by: data.status,
							updated_at: data.created_at
						},
						{
							where: {
								order_id: data.order_id
							}
						}
				);
				if (order.order_request_ids.length != 0)
				{/////////	Order Request Update 		///////////////////
					data.update_order_requests = Db.order_requests.update(
						{
							order_request_status: data.status,
							driver_request_status: '1',
							updated_at: data.created_at
						},
						{
							where: {
								order_request_id: {$in: order.order_request_ids}
							}
						}
					);
				}/////////	Order Request Update 		///////////////////
				////////////////		Driver Push Notifications 	///////////////////////
				if (order.CRequest && order.CRequest.DriverDetails.fcm_id != "" && order.CRequest.DriverDetails.notifications == "1")
				{
					var message = Libs.commonFunctions.generate_lang_message(" has cancelled the service order", order.CRequest.DriverDetails.language_id);

					data.push_data = {
						order_id: (data.order_id).toString(),
						type: data.status, //SerTimeout
						message: data.timeOutMessage,
						future: order.future
					};
					var push = Libs.commonFunctions.latestSendPushNotification([order.CRequest.DriverDetails], data.push_data);
				}
				////////////////		Driver Push Notifications 	///////////////////////
				////////////////		Socket Event 	///////////////////////////
				var temp_order = await Services.orderServices.driverAcceptOrderDetails(data, order.CustomerDetails.language_id);
				data.socket_data = {
					type: data.status,
					order: temp_order//order_id
				};
				var event = Libs.commonFunctions.emitToDevice("OrderEvent", order.CustomerDetails.user_detail_id, data.socket_data);
				return {success: 1, statusCode: 200, msg: "Service automatically rejected successfully", result: order};
			}
		}//End Round Robin Functionality
		else
		{
			//console.log("round robin first false");
			data.status = "SerTimeout";

			data.update_order = await Db.orders.update(
					{
						order_status: data.status,
						payment_status: data.status,
						cancelled_by: data.status,
						updated_at: data.created_at
					},
					{
						where: {
							order_id: data.order_id
						}
					}
			);

			if (order.order_request_ids.length != 0)
			{/////////	Order Request Update 		///////////////////
				data.update_order_requests = Db.order_requests.update(
						{
							order_request_status: data.status,
							driver_request_status: '1',
							updated_at: data.created_at
						},
						{
							where: {
								order_request_id: {$in: order.order_request_ids}
							}
						}
				);

			}/////////	Order Request Update 		///////////////////
			///////////////////////////////////////////////////
			////////////////////		Customer Push Socket
			////////////////		Driver Push Notifications 	///////////////////////
			if (order.CRequest && order.CRequest.DriverDetails.fcm_id != "" && order.CRequest.DriverDetails.notifications == "1")
			{

				var message = Libs.commonFunctions.generate_lang_message(" has cancelled the service order", order.CRequest.DriverDetails.language_id);

				data.push_data = {
					order_id: (data.order_id).toString(),
					type: data.status, //SerTimeout
					message: data.timeOutMessage,
					future: order.future
				};
				var push = Libs.commonFunctions.latestSendPushNotification([order.CRequest.DriverDetails], data.push_data);
			}
			////////////////		Driver Push Notifications 	///////////////////////
			////////////////		Socket Event 	///////////////////////////
			var temp_order = await Services.orderServices.driverAcceptOrderDetails(data, order.CustomerDetails.language_id);

			data.socket_data = {
				type: data.status,
				order: temp_order//order_id
			};
			
			var event = Libs.commonFunctions.emitToDevice("OrderEvent", order.CustomerDetails.user_detail_id, data.socket_data);

			return {success: 1, statusCode: 200, msg: "Service automatically rejected successfully", result: order};
		}
    } catch (e)
    {
        return {success: 0, statusCode: 500, msg: e.message};
    }
}

////////////////////////		Cancel A Service 	/////////////////////////////////////
exports.userServiceCancel = async (request, response) => {
    try {

        var data = request.body;
        var Details = data.Details;
        delete data.Details;

        request.checkBody("order_id", response.trans("Order id is required")).notEmpty();
        request.checkBody("cancel_reason", response.trans("Cancel reason is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        ////////////////	Order Get And status check 	//////////////////////
        var order = await Services.orderServices.customerOrder4Cancellations(data);
		console.log("order=======",order);
        if (order.length == 0)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not available")});
        order = order[0];
        if (order.customer_user_detail_id != Details.user_detail_id)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order do not belong to you"), result: order});
        else if ((order.order_status == "CustCancel") || (order.order_status == "SerCancel") || (order.order_status == "SupCancel") || (order.order_status == "SerReject") || (order.order_status == "SerTimeout") || (order.order_status == "DSchCancelled") || (order.order_status == "DSchTimeout") || (order.order_status == "SysSchCancelled"))
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already cancelled"), result: order});
        else if (order.order_status == "SerComplete" || order.order_status == "SerCustConfirm")
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already completed"), result: order});
        // else if(order.order_status == 'SerComplete' && order.order_type == 'Service')
        // 	return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already completed"), result: order });
        // ////////////////	Order Get And status check 	//////////////////////
		
		
		
		
        data.order_request_status = "CustCancel";
        data.cancelled_by = "Cust";
		
		
        data.update_order = await Db.orders.update(
                {
                    order_status: data.order_request_status,
                    cancel_reason: data.cancel_reason,
                    cancelled_by: data.cancelled_by,
                    updated_at: data.created_at
                },
                {
                    where: {order_id: data.order_id}
                }
        );
        data.update_payment = await Db.payments.update(
                {
                    payment_status: data.order_request_status,
                    updated_at: data.created_at
                },
                {
                    where: {order_id: data.order_id}
                }
        );
		//Add M3 Part 3.2
		if(order.schulderd_order_token != "")
		{
			var stoken   = order.schulderd_order_token;
			await Db.orders.update(
					{
						order_status: data.order_request_status,
						cancel_reason: data.cancel_reason,
						cancelled_by: data.cancelled_by,
						updated_at: data.created_at
					},
					{
						where: {schulderd_order_token: stoken}
					}
			);
		}

        data.fcm_ids = [];
        data.socket_ids = [];
        data.order_request_ids = [];
        order.drivers.forEach(function (driver) {
            if (driver.fcm_id != "")
                data.fcm_ids.push(driver.fcm_id);
            data.socket_ids.push(driver.driver_user_detail_id);
            data.order_request_ids.push(driver.order_request_id);
        });

        ////////////////	Update DB 		///////////////////////////////

        if (data.order_request_ids.length != 0) {
            data.update_order_request = await Db.order_requests.update({
                order_request_status: data.order_request_status,
                updated_at: data.created_at
            },
                    {
                        where: {
                            order_request_id: {$in: data.order_request_ids}
                        }
                    });
        }
        ////////////////	Update DB 		///////////////////////////////

        ////////////////		Push Notifications 	///////////////////////
        // if(data.fcm_ids.length != 0)
        // 	{
        // 		var message = generate_lang_message(" has cancelled the service order", order.CRequest.DriverDetails.language_id);

        // 		data.push_data = {
        // 			order_id: (data.order_id).toString(),
        // 			type: data.order_request_status,//CustCancel
        // 			user_id: (order.customer_user_id).toString(), 
        // 			user_detail_id: (order.customer_user_detail_id).toString(),
        // 			message: Details.user.name+response.trans(" has cancelled the service order")
        // 		};
        // 		var push = Libs.commonFunctions.newSendPushNotifications(data.fcm_ids, data.push_data);
        // 	}

        if (order.CRequest && order.CRequest.DriverDetails.fcm_id != "" && order.CRequest.DriverDetails.notifications == "1") {

            var message = Libs.commonFunctions.generate_lang_message(" has cancelled the service order", order.CRequest.DriverDetails.language_id);

            data.push_data = {
                order_id: (data.order_id).toString(),
                type: data.order_request_status, //CustCancel
                message: Details.user.name + message
            };

            var push = Libs.commonFunctions.latestSendPushNotification([order.CRequest.DriverDetails], data.push_data);

        }
        ////////////////		Push Notifications 	///////////////////////

        ////////////////		Socket Event 	///////////////////////////
        var products = await Services.orderServices.AllOrderProducts(data);
        data.socket_data = {
            type: "SerCancel",
            order_id: order.order_id,
            order: {
                order_id: order.order_id,
                driver_user_detail_id: order.driver_user_detail_id,
                user: {
                    user_id: order.customer_user_id,
                    user_detail_id: order.customer_user_detail_id
                },
                products: products
            }
        };

        var event = Libs.commonFunctions.emitDynamicEvent("OrderEvent", data.socket_ids, data.socket_data);
        ////////////////		Socket Event 	///////////////////////////

        //////////////////	Coupons or Etokens 	////////////////////////////
        if (order.coupon_user) {
            await Db.coupon_users.update(
                    {
                        rides_left: order.coupon_user.rides_left + 1,
                        updated_at: data.created_at
                    },
                    {
                        where: {coupon_user_id: order.coupon_user_id}
                    }
            );

        }
        // else if(order.organisation_coupon_user)
        // 	{

        // 		await Db.organisation_coupon_users.update(
        // 					{
        // 						quantity_left: order.organisation_coupon_user.quantity_left+order.payment.product_quantity,
        // 						updated_at: data.created_at
        // 					},
        // 					{
        // 						where: {organisation_coupon_user_id: order.organisation_coupon_user_id}
        // 					}
        // 		);

        // 	}
        //////////////////	Coupons or Etokens 	////////////////////////////

        var order = await Services.orderServices.latestUserBookedOrderDetails(data);

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Order Cancelled successfully"), result: order[0]});

    } catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

///////////////////////		Service Ongoings 	/////////////////////////////////////////////
exports.userServiceOngoing = async (request, response) => {
    try {

        var data = request.body;
        // console.log("hjhhjhjhjf",data);
        var currentOrders = await Services.orderServices.customerCurrentOrders(data);

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Ongoing orders"), result: currentOrders});

    } catch (e)
    {
        //console.log("err ==",e);
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

///////////////////////		User Service Rate 		//////////////////////////////////////////
exports.userServiceRate = async (request, response) => {
    try {

        var data = request.body;
        var Details = data.Details;
        delete data.Details;

        request.checkBody("order_id", response.trans("Order id is required")).notEmpty();
        request.checkBody("ratings", response.trans("Ratings is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});
        // console.log('Rate Service =',data);
        ////////////////	Order Get And status check 	//////////////////////
        var order = await Services.orderServices.customerOrder4Push(data);
        if (!order)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not available")});
        else if (order.customer_user_detail_id != Details.user_detail_id)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order do not belong to you"), result: order});
        else if ((order.order_status == "CustCancel") || (order.order_status == "SerCancel") || (order.order_status == "SupCancel") || (order.order_status == "SerReject") || (order.order_status == "SerTimeout") || (order.order_status == "DSchCancelled") || (order.order_status == "DSchTimeout") || (order.order_status == "SysSchCancelled"))
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already cancelled"), result: order});
        // else if(order.order_status != 'SerComplete' && order.order_type == 'Service')
        else if (order.order_type == "Service" && !((order.order_status == "SerCustConfirm") || (order.order_status == "SerComplete")))
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order is not completed yet"), result: order});
        else if (order.order_status != "SupComplete" && order.order_type == "Support")
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order is not completed yet"), result: order});
        else if (order.CustRatings)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, you have already rated this order"), result: order});
        ////////////////	Order Get And status check 	//////////////////////

        order.CustRatings = await Db.order_ratings.create({
            order_id: data.order_id,
            customer_user_id: order.customer_user_id,
            customer_user_detail_id: order.customer_user_detail_id,
            customer_organisation_id: order.customer_organisation_id,
            driver_user_id: order.driver_user_id,
            driver_user_detail_id: order.driver_user_detail_id,
            driver_organisation_id: order.driver_organisation_id,
            ratings: data.ratings,
            comments: data.comments ? data.comments : "",
            created_by: "Customer",
            deleted: "0",
            blocked: "0",
            created_at: data.created_at,
            updated_at: data.created_at
        });
        data.order_rating_id = order.CustRatings.order_rating_id;

        data.type = "CustRatedService";
        data.message = Details.user.name + " has rated your service";

        data.driver_notification = await Db.user_detail_notifications.create({
            user_id: order.driver_user_id,
            user_detail_id: order.driver_user_detail_id,
            user_type_id: order.driver_user_type_id,
            user_organisation_id: order.driver_organisation_id,
            sender_user_id: order.customer_user_id,
            sender_user_detail_id: order.customer_user_detail_id,
            sender_type_id: order.customer_user_type_id,
            sender_organisation_id: order.customer_organisation_id,
            order_id: order.order_id,
            order_request_id: order.CRequest.order_request_id,
            order_rating_id: order.CustRatings.order_rating_id,
            message: data.message,
            type: data.type,
            is_read: "0",
            deleted: "0",
            created_at: data.created_at,
            updated_at: data.created_at
        });

        ////////////////		Push Notifications 	///////////////////////
        if (order.DriverDetails.fcm_id != "" && order.DriverDetails.notifications == "1")
        {
            var message = Libs.commonFunctions.generate_lang_message(" has rated your service", order.DriverDetails.language_id);

            data.push_data = {
                order_id: (data.order_id).toString(),
                type: data.type, //CustRatedService
                user_id: (order.customer_user_id).toString(),
                user_detail_id: (order.customer_user_detail_id).toString(),
                message: Details.user.name + message,
                future: order.future
            };

            var push = Libs.commonFunctions.latestSendPushNotification([order.DriverDetails], data.push_data);
        }
        ////////////////		Push Notifications 	///////////////////////

        var products = await Services.orderServices.AllOrderProducts(data);
        ////////////////		Socket Event 	///////////////////////////
        data.socket_data = {
            type: data.type,
            message: data.message,
            order: {
                order_id: order.order_id,
                driver_user_detail_id: order.driver_user_detail_id,
                user: {
                    user_id: order.customer_user_id,
                    user_detail_id: order.customer_user_detail_id
                },
                products: products
            }
        };
        var event = await Libs.commonFunctions.emitDynamicEvent("OrderEvent", [order.DriverDetails.driver_user_detail_id], data.socket_data);
        //io.sockets.emit('CustRatedService', data.socket_data );
        ////////////////		Socket Event 	///////////////////////////

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Order rated successfully")});

    } catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

////////////////////////		User Etokens 	//////////////////////////////////////////
exports.userServiceETokens = async (request, response) => {
    try {

        var data = request.body;

        //console.log("dataaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", data);

        request.checkBody("category_id", response.trans("Service type is required")).notEmpty();

        request.checkBody("latitude", response.trans("Latitude is required")).notEmpty();
        request.checkBody("longitude", response.trans("Longitude is required")).notEmpty();
        request.checkBody("distance", response.trans("Distance is required")).notEmpty();
        request.checkBody("order_timings", response.trans("Order timings is required")).notEmpty();

        request.checkBody("take", response.trans("Take is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        data.day_string_check = Libs.commonFunctions.DayString(data.order_timings, data.timezone);

        data.skip = 0;

        var history = await Services.eTokensServices.userETokenHistory(data);

        //var brands = await Services.eTokensServices.OffersListings(data);
        var brands = await Services.eTokensServices.filterBrandEtokens(data);

        var result = {
            //data,
            history,
            brands
        };

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Token details"), result});

    } catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

///////////////////////		Etokens 	Paginate 		////////////////////////////////////
exports.userETokensPaginate = async (request, response) => {
    try {

        var data = request.body;

        request.checkBody("category_id", response.trans("Service type is required")).notEmpty();
        request.checkBody("category_brand_id", response.trans("Category brand id is required")).notEmpty();
        request.checkBody("latitude", response.trans("Latitude is required")).notEmpty();
        request.checkBody("longitude", response.trans("Longitude is required")).notEmpty();
        request.checkBody("distance", response.trans("Distance is required")).notEmpty();
        request.checkBody("skip", response.trans("Skip is required")).notEmpty();
        request.checkBody("take", response.trans("Take is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        var etokens = await Services.eTokensServices.PaginateETokens(data);

        var result = {
            etokens
        };

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Tokens pagination"), result});

    } catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};


///////////////////////////		Buy Tokens 	//////////////////////////////////////////
exports.userETokenBuy = async (request, response) => {
    try {

        var data = request.body;

        //		console.log("dataaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", data);

        request.checkBody("organisation_coupon_id", response.trans("Organisation coupon id is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        data.category_id = 3;

        var eToken = await Services.eTokensServices.eTokenDetails(data);
        if (!eToken || (!eToken.Org && !eToken.user_detail))
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this eToken is no longer available"), eToken});

        if (eToken.Org)
            data.buraq_percentage = eToken.Org.OrgCat.buraq_percentage;
        else
            data.buraq_percentage = eToken.user_detail.UCat.buraq_percentage;

        var user_card = await Db.user_cards.findOne({where: {user_id: data.user_id}});
        if (!user_card)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, no card is added for this user")});

        data.user_card_id = user_card.user_card_id;

        data.order_id = 0;
        data.coupon_id = 0;
        data.seller_user_id = eToken.user_id;
        data.seller_user_detail_id = eToken.user_detail_id;
        data.seller_user_type_id = eToken.user_type_id;
        data.seller_organisation_id = eToken.organisation_id;
        data.payment_status = "Completed";
        data.initial_charge = data.final_charge = data.product_actual_value = eToken.price;
        data.bank_charge = 0;
        data.admin_charge = ((data.initial_charge / 100) * data.buraq_percentage).toFixed(3);
        //( (data.initial_charge) - ((data.initial_charge/100)*data.buraq_percentage) )//.toFixed(2);

        data.metadata = {
            organisation_coupon_id: data.organisation_coupon_id,
            organisation_id: eToken.organisation_id
        };

        data.message = "Charge for EToken " + data.organisation_coupon_id;

        data.payment = await Libs.stripeFunctions.CouponPayment(data);///////	Stripe Payment
        data.transaction_id = data.payment.id;

        data.payment = await Services.paymentServices.CreateNewRow(data);
        data.payment_id = data.payment.payment_id;

        data.quantity = eToken.quantity;
        data.category_brand_product_id = eToken.category_brand_product_id;
        data.category_id = eToken.category_id;
        data.category_brand_id = eToken.category_brand_id;

        data.organisation_coupon_user = await Services.orgCouponUserServices.CreateNewRow(data);
        data.organisation_coupon_user_id = data.organisation_coupon_user.organisation_coupon_user_id;

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("eToken purchased successfully"), result: data.organisation_coupon_user_id});

    } catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};


exports.addAddress = async (request, response) => {
    try {
        var data = request.body;
        data.user_id = data.user_id;
        data.title = data.title;
        data.flat_no = data.flat_no;
        data.floor_no = data.floor_no;
        data.building_name = data.building_name;
        data.address = data.address;
        data.address_latitude = data.address_latitude;
        data.address_longitude = data.address_longitude;

        request.checkBody("title", response.trans("Title is required")).notEmpty();
        request.checkBody("flat_no", response.trans("Flat number is required")).notEmpty();
        request.checkBody("user_id", response.trans("User is required")).notEmpty();
        request.checkBody("address", response.trans("Address id is required")).notEmpty();
        request.checkBody("address_latitude", response.trans("Address Lat is required")).notEmpty();
        request.checkBody("address_longitude", response.trans("Address Lng is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        var result = await Services.userServices.addNewAddress(data);

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Address added successfully"), result});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

exports.getAddress = async (request, response) => {
    try {
        var data = request.body;
        data.user_id = data.user_id;

        request.checkBody("user_id", response.trans("User is required")).notEmpty();
        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        var addresses = await Services.userServices.GetAllUserAddress(data);

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Address received successfully"), addresses});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};



exports.deleteAddress = async (request, response) => {
    try {
        var data = request.body;
        data.address_id = data.address_id;

        request.checkBody("address_id", response.trans("Address id is required")).notEmpty();
        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        var delete_address = await Services.userServices.deleteUserAddressById(data);

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Address deleted successfully"), delete_address});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};



exports.editAddress = async (request, response) => {
    try {

        var data = request.body;
        data.address_id = data.address_id;
        data.title = data.title;
        data.flat_no = data.flat_no;
        data.floor_no = data.floor_no;
        data.building_name = data.building_name;
        data.address = data.address;
        data.address_latitude = data.address_latitude;
        data.address_longitude = data.address_longitude;

        request.checkBody("address_id", response.trans("Address id is required")).notEmpty();
        request.checkBody("title", response.trans("Title is required")).notEmpty();
        request.checkBody("flat_no", response.trans("Flat number is required")).notEmpty();
        request.checkBody("address", response.trans("Address is required")).notEmpty();
        request.checkBody("address_latitude", response.trans("Address Lat is required")).notEmpty();
        request.checkBody("address_longitude", response.trans("Address Lng is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        var updated_address = await Services.userServices.updateUserAddress(data);

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Address updated successfully"), updated_address});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

exports.SendMultipleNotificationtousers = async (request, response) => {
    console.log('request', request);
    var req = request.body;
    var data = Array();
    data.cust_push_data = {
        type: "BulkNotification",
        message: req.message,
        future: "0",
        title: req.title,
        couponId: req.couponId,
        serviceId: req.serviceId,
        image: req.image,
        mediaType: "Image"
    };
    var users = await Db.user_details.findAll({where: {blocked: '0', user_type_id: req.type}});
    var push = Libs.commonFunctions.SendMultipleNotificationtousers(users, data.cust_push_data);
    return response.status(200).json({success: 0, statusCode: 500});
};

exports.updatePromotionNotification = async (request, response) => {
    try {

        var data = request.body;
        console.log(data);
        data.status = data.status;

        request.checkBody("status", response.trans("Status is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        var updated_status = await Services.userServices.updateUserPromotionNotification(data);

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Updated successfully")});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};
//Add M2 Part 2
//////////////////////      User Requests Service       ////////////////////////////////////
exports.userSendQuotation = async (request, response) => {
    try {
        var data             = request.body;
        var Details          = request.uDetails;
        data.user_detail_id  = Details.user_detail_id;
        data.user_id         = Details.user_id;
        data.access_token    = Details.access_token;
        data.app_language_id = request.app_language_id;
        data.user_name       = Details.user.name;
        data.created_at      = request.created_at;
        request.checkBody("category_id", response.trans("Category id is required")).notEmpty();
		request.checkBody("quotation_id", response.trans("Quotation id is required")).notEmpty();
        request.checkBody("category_brand_id", response.trans("Category brand id is required")).notEmpty();
        request.checkBody("dropoff_address", response.trans("Dropoff address is required")).notEmpty();
        request.checkBody("dropoff_latitude", response.trans("Dropoff latitude is required")).notEmpty();
        request.checkBody("dropoff_longitude", response.trans("Dropoff longitude is required")).notEmpty();
        if (data.category_id && data.category_id == 4 && data.category_id == 5) {
            request.checkBody("pickup_address", response.trans("Pickup address is required")).notEmpty();
            request.checkBody("pickup_latitude", response.trans("Pickup latitude is required")).notEmpty();
            request.checkBody("pickup_longitude", response.trans("Pickup longitude is required")).notEmpty();
        }
        request.checkBody("order_timings", response.trans("Order timings is required")).notEmpty();
        request.checkBody("future", response.trans("Future parameter is required")).notEmpty();
        request.checkBody("payment_type", response.trans("Payment type is required")).notEmpty();
        request.checkBody("payment_type", response.trans("Payment type can either be 'Cash' or 'Card' or 'eToken'")).isIn(["Card", "Cash", "eToken"]);
        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});
        data.category_brand_product_id = data.category_brand_product_id;
        if (data.category_id != 1 && data.category_id != 3 && data.category_id != 2 && data.category_id != 4 && data.category_id != 5 && data.category_id != 16)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, currently only gas, Drinking water, Water tanker and Truck services are available")});

        ////////////        Cat Details         ///////////////////////
        var CateGoryProducts = Array();
		data.OriginalProductsArray = data.products;
        var JsonProducts     = JSON.parse(data.products);
		data.products    = JsonProducts;
        var orderQuzntity = '0';
        for (let j = 0; j < JsonProducts.length; j++) {
            orderQuzntity = parseInt(orderQuzntity) + parseInt(JsonProducts[j].product_quantity);
            CateGoryProducts.push(JsonProducts[j].category_brand_product_id);
        }
        data.CateGoryProducts = CateGoryProducts;
        data.category         = await Services.categoryBrandProductServices.CatDetailsAllLang(data);
		
        if (!data.category)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, Please check your parameters")});
        
        data.distance            = parseFloat(data.category.maximum_distance);
        data.user_type_id        = data.driver_user_type_id = 2;
        data.order_type          = "Service";
        data.product_quantity    = parseInt(orderQuzntity);
        data.seller_user_type_id = 2;
        data.organisation_coupon_user_id = 0;
        var cc = Array();
        for (let i = 0; i < JsonProducts.length; i++) {
            cc.push(JsonProducts[i].category_brand_product_id);
        }
        var CategoryProductsID  = cc.toString();
        data.CategoryProductsID = CategoryProductsID;
        data.drivers = await Services.driverListingServices.Driver4TruckOrders(data);
        if (data.drivers.length == 0 && process.env.NODE_ENV != "development") 
        {
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, no driver is currently available")});
        }
        data.timings = moment(data.order_timings, "YYYY-MM-DD HH:mm:ss").tz("Asia/Muscat").format("YYYY-MM-DD hh:mm:ss");
        data.customer_user_id = Details.user_id;
        data.customer_user_detail_id = Details.user_detail_id;
		var order_images_url = [];
		if (request.files && request.files.length > 0) {
            request.files.forEach(async (image) => {
                order_images_url.push({
                    order_id: 0,
                    image: image.filename,
                    image_url: process.env.RESIZE_URL + image.filename
                });
            });
        }
		if (order_images_url.length != 0){
			data.order_images = JSON.stringify(order_images_url);
		}else{
			data.order_images  = "";
		}
		
		var order    = await Services.orderServices.latestGetDrivers(data);
        
        if (order.fcm_socket_ids.length > 0) 
        {
            var push_data = {
                //order_id: "0",
                type: "SerQuoteRequest",
				message: Details.user.name+" has requested a new quotation",
                user_id: (data.customer_user_id).toString(),
                user_detail_id: (data.customer_user_detail_id).toString(),
                expiry: moment().utc().add(45, "seconds").format("YYYY-MM-DD HH:mm:ss")
            };
			
            var socket_data = {
                type: "SerQuoteRequest",
                order: {
					quotation_id: data.quotation_id,
					driver_quotation_timeout: data.category.driver_quotation_timeout,
                    order_id: 0,
                    driver_user_detail_id: 0,
                    //order_timings: data.timings,
                    order_timings: data.order_timings,
                    pickup_address: data.pickup_address,
                    pickup_latitude: parseFloat(data.pickup_latitude),
                    pickup_longitude: parseFloat(data.pickup_longitude),
                    dropoff_address: data.dropoff_address,
                    dropoff_latitude: parseFloat(data.dropoff_latitude),
                    dropoff_longitude: parseFloat(data.dropoff_longitude),
                    payment: null,
                    organisation_coupon_user_id: data.organisation_coupon_user_id,
                    future: data.future,
                    category_id: data.category_id,
                    brand: {
                        category_id: data.category_id,
                        category_brand_id: data.category_brand_id,
                        category_brand_product_id: data.category_brand_product_id,
                    },
                    user: {
                        user_id: data.customer_user_id,
                        user_detail_id: data.customer_user_detail_id,
                        name: Details.user.name,
                        rating_count: Details.user.rating_count,
                        rating_avg: Details.user.ratings_avg,
                        profile_pic: Details.profile_pic,
                        profile_pic_url: Details.profile_pic_url,
                        phone_number: Details.user.phone_number,
                        phone_code: Details.user.phone_code
                    },
                    coupon_users: null,
                    order_images_url,
                    details: data.details ? data.details : "",
                    material_details: data.material_details ? data.material_details : "",
                    products: JsonProducts
                }
            };
			console.log("data.category",socket_data);
            Libs.commonFunctions.pushPlusEventsQuotation(order.fcm_socket_ids, push_data, socket_data,data.category, "OrderEvent");
        }
		if (data.future == "0")
        {
            data.timeOutMessage = response.trans("Sorry, no driver is currently available");
            setTimeout(function () {
                //data.order_request_status = "Searching";
                quotationRequestedTimeout(data);// this code will only run when time has ellapsed
            }, 120 * 1000);
        }
        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Send Quotation requested successfully"), result: data});
    } 
    catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

///////////////////////		Order Request Timeout 		/////////////////////////////////
async function quotationRequestedTimeout(data)
{
    try {
        data.status = "QuotationTimeOut";

        data.update_order = await Db.user_quotations.update(
                {
                    quotation_status: data.status
                },
                {
                    where: {
                        quotation_unique_id: data.quotation_id,
						quotation_status: "Searching"
                    }
                }
        );
		var order    = await Services.orderServices.latestGetDriversold(data);
		data.push_data = {
			order_id: (data.quotation_id).toString(),
			type: data.status, //SerTimeout
			message: data.timeOutMessage,
			future: '0'
		};

		//var push = Libs.commonFunctions.latestSendPushNotification(order.fcm_socket_ids, data.push_data);

        

        return {success: 1, statusCode: 200, msg: "Service automatically rejected successfully"};

    } catch (e)
    {
        return {success: 0, statusCode: 500, msg: e.message};
    }
}

//////////////////////		User Requests Service 		////////////////////////////////////
exports.userQuotationServiceRequest = async (request, response) => {
    try {
        var data                       = request.body;
        var Details                    = request.uDetails;
        data.user_detail_id            = Details.user_detail_id;
        data.user_id                   = Details.user_id;
        data.access_token              = Details.access_token;
        data.app_language_id           = request.app_language_id;
        data.user_name                 = Details.user.name;
        data.created_at                = request.created_at;
        data.category_brand_product_id = 0;
		var quotationdetails           = await Db.user_quotations.findOne({ where: {quotation_unique_id: data.quotation_unique_id,driver_user_detail_id: data.driver_user_detail_id}});
		data.products                  = quotationdetails.category_products;
		data.category_id               = quotationdetails.category_id;
		data.category_brand_id         = quotationdetails.category_brand_id;
		data.pickup_address            = quotationdetails.pickup_address;
		data.pickup_latitude           = quotationdetails.pickup_latitude;
		data.pickup_longitude          = quotationdetails.pickup_longitude;
		data.dropoff_address           = quotationdetails.dropoff_address;
		data.dropoff_latitude          = quotationdetails.dropoff_latitude;
		data.dropoff_longitude         = quotationdetails.dropoff_longitude;
		data.details                   = quotationdetails.details;
		data.material_details          = quotationdetails.material_details;
		data.order_distance            = quotationdetails.order_distance;
		data.payment_type              = quotationdetails.payment_type;
		data.quotation_price           = quotationdetails.quotation_price;
		data.quotation_id              = data.quotation_unique_id;
		data.floor_charge              = quotationdetails.floor_charge; //Add M3 Part 3.2
		data.reward_discount_point     = quotationdetails.reward_discount_point; //Add M3 Part 3.2
		data.reward_discount_price     = quotationdetails.reward_discount_price; //Add M3 Part 3.2
		
		var driverOngoing = await Services.driverListingServices.CheckDriverOngoingOrder(data.driver_user_detail_id);
		
		if(driverOngoing[0].totalorder > 0)
		{
			return response.status(400).json({success: 0, statusCode: 400, msg: "Driver has already ongoing order."});
		}
		////////////        Cat Details         ///////////////////////
        var CateGoryProducts = Array();
        var JsonProducts     = JSON.parse(data.products);
        var orderQuzntity    = '0';
        for (let j = 0; j < JsonProducts.length; j++) {
            orderQuzntity = parseInt(orderQuzntity) + parseInt(JsonProducts[j].product_quantity);
            CateGoryProducts.push(JsonProducts[j].category_brand_product_id);
        }
        data.CateGoryProducts = CateGoryProducts;
        data.category         = await Services.categoryBrandProductServices.CatDetailsAllLang(data);
        if (!data.category)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, Please check your parameters")});
		
		if(parseFloat(data.category.Brand.is_default) == 1)
        {
            data.distance = parseFloat(data.category.maximum_distance);
        }
        else
        {
            data.distance = parseFloat(data.category.Brand.maximum_radius);
        }
        ////////////        Cat Details         ///////////////////////
        data.user_type_id                = data.driver_user_type_id = 2;
        data.order_type                  = "Service";
        data.product_quantity            = parseInt(orderQuzntity);
        data.seller_user_type_id         = 2;
        data.organisation_coupon_user_id = 0;
        data.coupon_user_id              = 0;
       var coupon_users = "";
        var cc = Array();
        var final_charge = '0.0';
        for (let i = 0; i < JsonProducts.length; i++) {
            final_charge = parseFloat(final_charge) + parseFloat(JsonProducts[i].price_per_item);
            data.product_actual_value = JsonProducts[i].price_per_item;
            data.product_alpha_charge = data.category.Brand.Product.alpha_price;
            data.product_per_quantity_charge = data.category.Brand.Product.price_per_quantity;
            data.product_per_weight_charge = data.category.Brand.Product.price_per_weight;
            data.product_per_distance_charge = data.category.Brand.Product.price_per_distance;
            data.product_per_hr_charge = data.category.Brand.Product.price_per_hr;
            data.product_per_sq_mt_charge = data.category.Brand.Product.price_per_sq_mt;
            data.bottle_returned_value = data.bottle_returned_value ? data.bottle_returned_value : 0;
            cc.push(JsonProducts[i].category_brand_product_id);
        }
        var CategoryProductsID = cc.toString();
        data.CategoryProductsID = CategoryProductsID;
		var driver_user_detail_id = data.driver_user_detail_id ? data.driver_user_detail_id : '0';
		data.drivers = await Services.driverListingServices.SingleDriver4TruckOrders(driver_user_detail_id);
        data.order_timings = moment().tz("Asia/Muscat").format("YYYY-MM-DD hh:mm:ss");
        data.timings = moment(data.order_timings, "YYYY-MM-DD HH:mm:ss").tz("Asia/Muscat").format("YYYY-MM-DD hh:mm:ss");
        data.order_token = "OR-" + Libs.commonFunctions.uniqueId();
        ///////////     Customer    /////////////////
        data.customer_user_id         = Details.user_id;
        data.customer_user_detail_id  = Details.user_detail_id;
        data.customer_organisation_id = Details.organisation_id;
        data.customer_user_type_id    = Details.user_type_id;
        data.user_card_id             = 0;
        ///////////     Customer    /////////////////
        data.order_status     = "Searching",
		data.payment_status   = "Pending",
		data.refund_status    = "NoNeed",
		data.created_by       = "User";
        data.order_distance   = data.order_distance ? parseFloat(data.order_distance) : 0;
        data.order_time       = 0;
        data.product_sq_mt    = 0;
        data.product_weight   = data.product_weight ? parseFloat(data.product_weight) : 0;
        data.buraq_percentage = parseFloat(data.category.buraq_percentage);
        //////////////////////      YesSer Percentage        ////////////////////////
        data.product_actual_value        = data.category.Brand.Product.actual_value;
        data.product_alpha_charge        = data.category.Brand.Product.alpha_price;
        data.product_per_quantity_charge = data.category.Brand.Product.price_per_quantity;
        data.product_per_weight_charge   = data.category.Brand.Product.price_per_weight;
        data.product_per_distance_charge = data.category.Brand.Product.price_per_distance;
        data.product_per_hr_charge       = data.category.Brand.Product.price_per_hr;
        data.product_per_sq_mt_charge    = data.category.Brand.Product.price_per_sq_mt;
        data.bottle_returned_value       = data.bottle_returned_value ? data.bottle_returned_value : 0;
        data.bottle_charge               = 0;
        data.initial_charge = parseFloat(data.product_alpha_charge) + parseFloat(final_charge) + (parseFloat(data.product_per_distance_charge) * parseFloat(data.order_distance)) + (parseFloat(data.product_per_hr_charge) * parseFloat(data.order_time)) + (parseFloat(data.product_per_sq_mt_charge) * parseFloat(data.product_sq_mt)) + (data.bottle_charge);
        data.coupon_charge  = 0;
        data.final_charge   = data.initial_charge;
        data.admin_charge   = parseFloat(((data.buraq_percentage * data.final_charge) / 100).toFixed(3));
        //data.final_charge   = parseFloat((data.final_charge + data.admin_charge).toFixed(3));
		data.final_charge   = data.quotation_price;
		//return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Service requested successfully"), result: data});
        var order = await Services.orderServices.latestCreateNewRow(data);
        data.order_id = order.order_id;
        var pp = Array();
        for (let i = 0; i < JsonProducts.length; i++)
        {
            var productss                       = Array();
            productss.order_id                  = data.order_id;
            productss.category_id               = data.category_id;
            productss.category_brand_id         = data.category_brand_id;
            productss.category_brand_product_id = JsonProducts[i].category_brand_product_id;
            productss.product_weight            = JsonProducts[i].product_weight;
            //productss.price_per_item            = JsonProducts[i].price_per_item;
            productss.price_per_item            = data.quotation_price;
            productss.image_url                 = "";
            productss.productName               = JsonProducts[i].productName;
            productss.product_quantity          = JsonProducts[i].product_quantity;
			productss.gift_offer                = JsonProducts[i].gift_offer;
            productss.created_at                = data.created_at;
            productss.updated_at                = data.created_at;
            productss.app_language_id           = data.app_language_id;
            await Services.orderServices.CreateProducts(productss);
        }

        var order_images = [];
        var order_images_url = [];
		if(quotationdetails.order_images != "")
		{
			order_images_url = JSON.parse(quotationdetails.order_images);
			var images = JSON.parse(quotationdetails.order_images);
			images.forEach(async (imagePro) => {
				console.log(imagePro);
                order_images.push({
                    order_id: data.order_id,
                    image: imagePro.image,
                    created_at: data.created_at,
                    updated_at: data.created_at
                });
            });
		}
        if (order_images.length > 0)
            await Db.order_images.bulkCreate(order_images);
		
        //////////////////      Drivers Push Plus Socket    ////////////////////////////
        if (order.fcm_socket_ids.length > 0) {
            var push_data = {
                order_id: (data.order_id).toString(),
                type: "OrderEvent",
                user_id: (data.customer_user_id).toString(),
                user_detail_id: (data.customer_user_detail_id).toString(),
                expiry: moment().utc().add(45, "seconds").format("YYYY-MM-DD HH:mm:ss")
            };
			var quotation_unique_id = data.quotation_unique_id ? data.quotation_unique_id : '';
            var products = await Services.orderServices.AllOrderProducts(data);
            var socket_data = {
                type: "SerRequest",
                order: {
					release_offer_id: '',
					quotation_id: quotation_unique_id, 
					floor_charge: data.floor_charge,  //Add M3 Part 3.2
					reward_discount_price: data.reward_discount_price,  //Add M3 Part 3.2
                    order_id: data.order_id,
                    driver_user_detail_id: order.driver_user_detail_id,
                    order_timings: data.order_timings,
                    pickup_address: order.pickup_address,
                    pickup_latitude: parseFloat(order.pickup_latitude),
                    pickup_longitude: parseFloat(order.pickup_longitude),
                    dropoff_address: order.dropoff_address,
                    dropoff_latitude: parseFloat(order.dropoff_latitude),
                    dropoff_longitude: parseFloat(order.dropoff_longitude),
					continouos_startdt: order.continouos_startdt ? order.continouos_startdt : "",//Add M3 Part 3.2
					continuous_enddt: order.continuous_enddt ? order.continuous_enddt : "",//Add M3 Part 3.2
					request_type: order.request_type ? order.request_type : "",//Add M3 Part 3.2
					NonContinueSOrder: [], //Add M3 Part 3.2
					order_request_timeout: 47, //Add M3 Part 3.2
					total_request_driver: 1, //Add M3 Part 3.2
                    payment: order.payment,
                    organisation_coupon_user_id: data.organisation_coupon_user_id,
                    future: data.future,
                    category_id: data.category_id,
                    brand: {
                        category_id: data.category_id,
                        category_brand_id: data.category_brand_id,
                        category_brand_product_id: data.category_brand_product_id,
                    },
                    user: {
                        user_id: data.customer_user_id,
                        user_detail_id: data.customer_user_detail_id,
                        name: Details.user.name,
                        rating_count: Details.user.rating_count,
                        rating_avg: Details.user.ratings_avg,
                        profile_pic: Details.profile_pic,
                        profile_pic_url: Details.profile_pic_url,
                        phone_number: Details.user.phone_number,
                        phone_code: Details.user.phone_code
                    },
                    coupon_users: coupon_users ? coupon_users : null,
                    order_images_url,
                    details: order.details,
                    material_details: order.material_details,
                    products: products
                }
            };
			console.log('socket_data',socket_data);
            Libs.commonFunctions.pushPlusEventMultipleUsers(order.fcm_socket_ids, push_data, socket_data, order, data.category, "OrderEvent");
        }
        //////////////////      Drivers Push Plus Socket    ////////////////////////////
        //////////      No Response Timeout After 2 Minutes     ///////////
        if (data.future == "0")
        {
            data.timeOutMessage = response.trans("Sorry, no driver is currently available. Admin will soon assign a driver.");
            setTimeout(function () {
                //data.order_request_status = "Searching";
                orderRequestedTimeout(data);// this code will only run when time has ellapsed
            }, 47 * 1000);
        }
        //////////      No Response Timeout After 2 Minutes     ///////////
        data.socket_ids = order.socket_ids;
        data.fcm_ids = order.fcm_ids;
        var order = await Services.orderServices.latestUserBookedOrderDetails(data);
        order = order[0];
        ///////////////////     Deduct Etokens, Coupon Users    ////////////////////////////////
        if (order.organisation_coupon_user)
        {
            order.organisation_coupon_user.quantity_left = order.organisation_coupon_user.quantity_left - data.product_quantity;
            Services.orgCouponUserServices.updateOrgCouponUser(order.organisation_coupon_user.organisation_coupon_user_id, order.organisation_coupon_user.quantity_left, data.created_at);
        }
		order.quotation_id = data.quotation_unique_id ? data.quotation_unique_id : '';
		order.release_offer_id = '';
        //}
        ///////////////////     Deduct Etokens, Coupon Users    ////////////////////////////////
        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Service requested successfully"), result: order, data});

    } catch (e)
    {
        console.log("e ===", e);
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

//////////////////////      User Cancel Quotation Service       ////////////////////////////////////
exports.userCancelQuotation = async (request, response) => {
    try {
        var data             = request.body;
        var Details          = request.uDetails;
        data.user_detail_id  = Details.user_detail_id;
        data.user_id         = Details.user_id;
        data.access_token    = Details.access_token;
        data.app_language_id = request.app_language_id;
        data.user_name       = Details.user.name;
        data.created_at      = request.created_at;
        request.checkBody("category_id", response.trans("Category id is required")).notEmpty();
		request.checkBody("quotation_id", response.trans("Quotation id is required")).notEmpty();
        request.checkBody("category_brand_id", response.trans("Category brand id is required")).notEmpty();
        request.checkBody("dropoff_address", response.trans("Dropoff address is required")).notEmpty();
        request.checkBody("dropoff_latitude", response.trans("Dropoff latitude is required")).notEmpty();
        request.checkBody("dropoff_longitude", response.trans("Dropoff longitude is required")).notEmpty();
        if (data.category_id && data.category_id == 4 && data.category_id == 5) {
            request.checkBody("pickup_address", response.trans("Pickup address is required")).notEmpty();
            request.checkBody("pickup_latitude", response.trans("Pickup latitude is required")).notEmpty();
            request.checkBody("pickup_longitude", response.trans("Pickup longitude is required")).notEmpty();
        }
        request.checkBody("order_timings", response.trans("Order timings is required")).notEmpty();
        request.checkBody("future", response.trans("Future parameter is required")).notEmpty();
        request.checkBody("payment_type", response.trans("Payment type is required")).notEmpty();
        request.checkBody("payment_type", response.trans("Payment type can either be 'Cash' or 'Card' or 'eToken'")).isIn(["Card", "Cash", "eToken"]);
        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});
        data.category_brand_product_id = data.category_brand_product_id;
        if (data.category_id != 1 && data.category_id != 3 && data.category_id != 2 && data.category_id != 4 && data.category_id != 5 && data.category_id != 16)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, currently only gas, Drinking water, Water tanker and Truck services are available")});

        ////////////        Cat Details         ///////////////////////
        var CateGoryProducts = Array();
        var JsonProducts     = JSON.parse(data.products);
		data.products    = JsonProducts;
        var orderQuzntity = '0';
        for (let j = 0; j < JsonProducts.length; j++) {
            orderQuzntity = parseInt(orderQuzntity) + parseInt(JsonProducts[j].product_quantity);
            CateGoryProducts.push(JsonProducts[j].category_brand_product_id);
        }
        data.CateGoryProducts = CateGoryProducts;
        data.category         = await Services.categoryBrandProductServices.CatDetailsAllLang(data);
        if (!data.category)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, Please check your parameters")});
        
		await Db.user_quotations.update(
		{
			quotation_status: "QuoteCusCancel"
		},
		{
			where: { 
				quotation_unique_id: data.quotation_id 
			}
		});
			
        data.distance            = parseFloat(data.category.maximum_distance);
        data.user_type_id        = data.driver_user_type_id = 2;
        data.order_type          = "Service";
        data.product_quantity    = parseInt(orderQuzntity);
        data.seller_user_type_id = 2;
        ///////////////////////         Future Ride Check       ///////////////////////////////////////
        if (data.future == "1") {
            data.or_1hr = moment(data.now, "YYYY-MM-DD HH:mm:ss").add(55, "minutes");
            var isBefore = moment(data.order_timings, "YYYY-MM-DD HH:mm:ss").isBefore(data.or_1hr);
            if (isBefore && data.category_id == 2)
                return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Please select timing one hour after from now for scheduling a service")});

            var scheduled_check = await Db.orders.findOne({
                where: {
                    customer_user_id: data.user_id,
                    customer_user_detail_id: data.user_detail_id,
                    order_status: {$in: ["Scheduled", "DPending", "DApproved"]}
                }
            });
            if (scheduled_check && scheduled_check.order_type == "Support")
                return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, you already have scheduled a support"), scheduled_check});
        }
        ///////////////////////         Future Ride Check       ///////////////////////////////////////
        data.organisation_coupon_user_id = 0;
        //Start 25-April-2019
        var cc = Array();
        for (let i = 0; i < JsonProducts.length; i++) {
            cc.push(JsonProducts[i].category_brand_product_id);
        }
        var CategoryProductsID  = cc.toString();
        data.CategoryProductsID = CategoryProductsID;
        data.drivers = await Services.driverListingServices.Driver4TruckOrders(data);
        if (data.drivers.length == 0 && process.env.NODE_ENV != "development") 
        {
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, no driver is currently available")});
        }
        data.timings = moment(data.order_timings, "YYYY-MM-DD HH:mm:ss").tz("Asia/Muscat").format("YYYY-MM-DD hh:mm:ss");
        data.customer_user_id = Details.user_id;
        data.customer_user_detail_id = Details.user_detail_id;
		 ///////////     Customer    /////////////////
        data.order_status     = "Searching",
        data.payment_status   = "Pending",
        data.refund_status    = "NoNeed",
        data.created_by       = "User";
        data.order_distance   = data.order_distance ? parseFloat(data.order_distance) : 0;
        data.order_time       = 0;
        data.product_sq_mt    = 0;
        data.product_weight   = data.product_weight ? parseFloat(data.product_weight) : 0;
        data.buraq_percentage = parseFloat(data.category.buraq_percentage);
		//var order    = await Services.orderServices.latestGetDrivers(data);
		var order    = await Services.orderServices.latestGetDriversold(data);
        var order_images_url = [];
		if (request.files && request.files.length > 0) {
            request.files.forEach(async (image) => {
                order_images_url.push({
                    order_id: 0,
                    image: image.filename,
                    image_url: process.env.RESIZE_URL + image.filename
                });
            });
        }
        if (order.fcm_socket_ids.length > 0) 
        {
            var push_data = {
                //order_id: "0",
                type: "OrderEvent",
                user_id: (data.customer_user_id).toString(),
                user_detail_id: (data.customer_user_detail_id).toString(),
                expiry: moment().utc().add(45, "seconds").format("YYYY-MM-DD HH:mm:ss")
            };
			
            var socket_data = {
                //type: "SerRequest",
                type: "SerCancelQuoteRequest",
				
                order: {
					quotation_id: data.quotation_id,
                    order_id: 0,
                    driver_user_detail_id: 0,
                    order_timings: data.timings,
                    pickup_address: data.pickup_address,
                    pickup_latitude: parseFloat(data.pickup_latitude),
                    pickup_longitude: parseFloat(data.pickup_longitude),
                    dropoff_address: data.dropoff_address,
                    dropoff_latitude: parseFloat(data.dropoff_latitude),
                    dropoff_longitude: parseFloat(data.dropoff_longitude),
                    payment: null,
                    organisation_coupon_user_id: data.organisation_coupon_user_id,
                    future: data.future,
                    category_id: data.category_id,
                    brand: {
                        category_id: data.category_id,
                        category_brand_id: data.category_brand_id,
                        category_brand_product_id: data.category_brand_product_id,
                    },
                    user: {
                        user_id: data.customer_user_id,
                        user_detail_id: data.customer_user_detail_id,
                        name: Details.user.name,
                        rating_count: Details.user.rating_count,
                        rating_avg: Details.user.ratings_avg,
                        profile_pic: Details.profile_pic,
                        profile_pic_url: Details.profile_pic_url,
                        phone_number: Details.user.phone_number,
                        phone_code: Details.user.phone_code
                    },
                    coupon_users: null,
                    order_images_url,
                    details: "",
                    material_details: "",
                    products: data.products
                }
            };
            Libs.commonFunctions.pushPlusEventsQuotation(order.fcm_socket_ids, push_data, socket_data,data.category, "OrderEvent");
        }
        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Send Quotation requested successfully"), result: data});
    } 
    catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

exports.ReassignOrder = async (request, response) => {
    try {
		var req         = request.body;
		console.log(req);
		var Driverusers = await Db.user_details.findOne({where: {blocked: '0', user_detail_id: req.driverId}});
		var order       = await Db.orders.findOne({where: {order_id: req.orderId}});
		//console.log(order);
		var payment     = await Db.payments.findOne({where: {order_id: req.orderId}});
		var orderImages = await Db.order_images.findOne({where: {order_id: req.orderId}});
		if(!orderImages)
		{
			var orderImages = [];
		}
		var data                         = Array();
		data.order_id                    = req.orderId;
		data.customer_user_id            = order.customer_user_id;
		data.customer_user_detail_id     = order.customer_user_detail_id;
		data.category_id                 = order.category_id;
		data.category_brand_id           = order.category_brand_id;
		data.order_timings               = order.order_timings;
		data.future                      = order.future;
		data.category_brand_product_id   = order.category_brand_product_id;
		data.organisation_coupon_user_id = order.organisation_coupon_user_id;
		var products                     = await Services.orderServices.AllOrderProducts(data);
		var orderProducts = await Db.order_products.findAll({ where: {order_id: req.orderId}});
		var orderprodducts = JSON.stringify(orderProducts);
		var CateGoryProducts = Array();
		for(var i =0;i<orderProducts.length;i++)
		{
			CateGoryProducts.push(orderProducts[i].category_brand_product_id);
		}
        data.CateGoryProducts = CateGoryProducts;
		data.category = await Services.categoryBrandProductServices.CatDetailsAllLang(data);
		var customerdetails = await Db.user_details.findOne({ 
		where: {"user_detail_id": data.customer_user_detail_id},
			attributes: [
				"user_detail_id", "user_id", "user_type_id", "language_id", "category_id", "category_brand_id", "profile_pic", "access_token", "latitude", "longitude", "mulkiya_number", "mulkiya_front", "mulkiya_back", "mulkiya_validity","approved",
				[Sequelize.literal("CASE WHEN mulkiya_front='' THEN '' ELSE CONCAT('"+process.env.RESIZE_URL+"', mulkiya_front) END"), "mulkiya_front_url"],
				[Sequelize.literal("CASE WHEN mulkiya_back='' THEN '' ELSE CONCAT('"+process.env.RESIZE_URL+"', mulkiya_back) END"), "mulkiya_back_url"],
				[Sequelize.literal("CASE WHEN profile_pic='' THEN '' ELSE CONCAT('"+process.env.RESIZE_URL+"', profile_pic) END"), "profile_pic_url"]
			],
			include: [
				{
					model: Db.users,
					attributes: { exclude: ["blocked", "created_by", "created_at", "updated_at"] }
				}
			]
		});
		var push_data = {
			order_id: (data.order_id).toString(),
			type: "OrderEvent",
			user_id: (data.customer_user_id).toString(),
			user_detail_id: (data.customer_user_detail_id).toString(),
			expiry: moment().utc().add(45, "seconds").format("YYYY-MM-DD HH:mm:ss")
		};
		if(order.release_offer_id == 0)
		{
			var relid = '';
		}else{
			var relid = order.release_offer_id;
		}
		

		var NonCOrders = []; //Add M3 Part 3.2
		
		var socket_data = {
			type: "SerRequest",
			order: {
				release_offer_id: relid,
				quotation_id: '',
				reward_discount_price: order.reward_discount_price,  //Add M3 Part 3.2
				floor_charge: order.floor_charge,  //Add M3 Part 3.2
				order_id: order.order_id,
				driver_user_detail_id: order.driver_user_detail_id,
				order_timings: data.order_timings,
				continouos_startdt: order.continouos_startdt, //Add M3 Part 3.2
				continuous_enddt: order.continuous_enddt, //Add M3 Part 3.2
				request_type: order.request_type, //Add M3 Part 3.2
				NonContinueSOrder: NonCOrders, //Add M3 Part 3.2
				order_request_timeout: 47, //Add M3 Part 3.2
				total_request_driver: 1, //Add M3 Part 3.2
				pickup_address: order.pickup_address,
				pickup_latitude: parseFloat(order.pickup_latitude),
				pickup_longitude: parseFloat(order.pickup_longitude),
				dropoff_address: order.dropoff_address,
				dropoff_latitude: parseFloat(order.dropoff_latitude),
				dropoff_longitude: parseFloat(order.dropoff_longitude),
				payment: payment,
				organisation_coupon_user_id: data.organisation_coupon_user_id,
				future: data.future,
				category_id: data.category_id,
				brand: {
					category_id: data.category_id,
					category_brand_id: data.category_brand_id,
					category_brand_product_id: data.category_brand_product_id,
				},
				user: {
					user_id: data.customer_user_id,
					user_detail_id: data.customer_user_detail_id,
					name: customerdetails.user.name,
					rating_count: 0,
					rating_avg: 0,
					profile_pic: customerdetails.profile_pic,
					profile_pic_url: customerdetails.profile_pic_url,
					phone_number: customerdetails.user.phone_number,
					phone_code: customerdetails.user.phone_code
				},
				coupon_users: null,
				orderImages,
				details: order.details,
				material_details: order.material_details,
				products: products
			}
		};
		Libs.commonFunctions.pushReassignOrder(Driverusers, push_data, socket_data, order, data.category, "OrderEvent");
	
		return response.status(200).json({success: 0, statusCode: 200});
	} 
    catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

exports.getSentOffers = async (request, response) => {
    try 
    {
        var data            = request.body;
        var Details         = data.Details;
		var categoryId      = '4';
		var categoryDetails = await Db.categories.findOne({ where: {category_id: categoryId}});
		var date            = moment().tz(data.Details.timezone).format("YYYY-MM-DD");
		console.log(date);
		var distance        = categoryDetails.maximum_distance;
		var result = await Db.sequelize.query("SELECT ud.*,cbpd.name as productName,cbd.name as brandName,c.transit_offer_percentage,c.transit_buraq_margin FROM release_offers AS ud JOIN category_brand_product_details AS cbpd ON( cbpd.category_brand_product_id = ud.product_id ) JOIN category_brand_details AS cbd ON(cbd.category_brand_id = cbpd.category_brand_id) JOIN categories AS c ON(cbd.category_id = c.category_id) WHERE ud.status='Approved' AND ud.offer_expire = 0 AND DATE(ud.expire_date) >= '"+date+"' AND cbpd.language_id = "+data.app_language_id+" AND cbd.language_id = "+data.app_language_id+" HAVING (ROUND(((3959 * ACOS(COS(RADIANS("+data.latitude+")) * COS(RADIANS(ud.start_latitude)) * COS(RADIANS(ud.start_longitude) -RADIANS("+data.longitude+")) + SIN(RADIANS("+data.latitude+")) * SIN(RADIANS(ud.start_latitude)))) * 1.67),2) <= "+distance+") ORDER BY release_offer_id DESC", {type: Sequelize.QueryTypes.SELECT});
        return response.status(200).json({success: 1, statusCode: 200,result: result});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

exports.acceptSentOffer = async (request, response) => {
    try 
    {
        var data                       = request.body;
        var Details                    = data.Details;
		data.user_detail_id            = Details.user_detail_id;
        data.user_id                   = Details.user_id;
        data.access_token              = Details.access_token;
        data.app_language_id           = request.app_language_id;
        data.user_name                 = Details.user.name;
		data.category_id               = '4';
		data.category_brand_product_id = '0';
		data.created_at                = request.created_at;
		var SentOfferDetails           = await Db.release_offers.findOne({ where: {release_offer_id: data.release_offer_id}});
		var ReleaseId                  = SentOfferDetails.release_offer_id;
		data.ReleaseId                 = ReleaseId;
		data.pickup_address            = SentOfferDetails.start_address;
		data.pickup_latitude           = SentOfferDetails.start_latitude;
		data.pickup_longitude          = SentOfferDetails.start_longitude;
		data.dropoff_address           = SentOfferDetails.end_address;
		data.dropoff_latitude          = SentOfferDetails.end_latitude;
		data.dropoff_longitude         = SentOfferDetails.end_longitude;
		data.productId                 = SentOfferDetails.product_id;
		data.offer_price               = SentOfferDetails.offer_price;
		var DriverId                   = SentOfferDetails.driver_id;
		data.driverId                  = DriverId;
		var DriverDetails              = await Services.driverListingServices.DriverServiceDetails(data);
		data.category_brand_id         = DriverDetails[0].category_brand_id;
		data.products                  = DriverDetails;
        var CateGoryProducts = Array();
        //var JsonProducts     = JSON.parse(data.products);
		
		if (SentOfferDetails.offer_expire == 1)
			return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this offer has been expired")});
		
        var orderQuzntity    = '0';
        for (let j = 0; j < DriverDetails.length; j++) {
			orderQuzntity = parseInt(orderQuzntity) + parseInt(DriverDetails[j].product_quantity);
			CateGoryProducts.push(DriverDetails[j].category_brand_product_id);
        }
        data.CateGoryProducts = CateGoryProducts;
        data.category         = await Services.categoryBrandProductServices.CatDetailsAllLang(data);
        if (!data.category)
			return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, Please check your parameters")});
		
        data.distance = parseFloat(data.category.maximum_distance);
		data.order_distance = 500;
        data.user_type_id = data.driver_user_type_id = 2;
        data.order_type                  = "Service";
        data.product_quantity            = parseInt(orderQuzntity);
        data.seller_user_type_id         = 2;
		data.future                      = "0";
		data.organisation_coupon_user_id = 0;
		data.payment_type                = "Cash";
		var cc                           = Array();
        var final_charge                 = '0.0';
        for (let i = 0; i < DriverDetails.length; i++) {
            final_charge = parseFloat(final_charge) + parseFloat(DriverDetails[i].price_per_item);
            cc.push(DriverDetails[i].category_brand_product_id);
        }
		data.drivers            = await Services.driverListingServices.SingleTruckOrders(DriverId);
		console.log(data.drivers);
		data.timings            = moment().tz("Asia/Muscat").format("YYYY-MM-DD hh:mm:ss");
		data.order_timings      = data.created_at;
		data.order_token        = "OR-" + Libs.commonFunctions.uniqueId();
        ///////////     Customer    /////////////////
        data.customer_user_id         = Details.user_id;
        data.customer_user_detail_id  = Details.user_detail_id;
        data.customer_organisation_id = Details.organisation_id;
        data.customer_user_type_id    = Details.user_type_id;
        data.user_card_id             = 0;
        ///////////     Customer    /////////////////
        data.order_status     = "Searching",
        data.payment_status   = "Pending",
        data.refund_status    = "NoNeed",
        data.created_by       = "User";
        data.order_distance   = data.order_distance ? parseFloat(data.order_distance) : 0;
        data.order_time       = 0;
        data.product_sq_mt    = 0;
        data.product_weight   = data.product_weight ? parseFloat(data.product_weight) : 0;
       // data.buraq_percentage = parseFloat(data.category.buraq_percentage);
	    data.buraq_percentage = parseFloat(data.category.transit_buraq_margin);
        //////////////////////      YesSer Percentage        ////////////////////////
        data.product_actual_value        = data.category.Brand.Product.actual_value;
        data.product_alpha_charge        = data.category.Brand.Product.alpha_price;
        data.product_per_quantity_charge = data.category.Brand.Product.price_per_quantity;
        data.product_per_weight_charge   = data.category.Brand.Product.price_per_weight;
        data.product_per_distance_charge = data.category.Brand.Product.price_per_distance;
        data.product_per_hr_charge       = data.category.Brand.Product.price_per_hr;
        data.product_per_sq_mt_charge    = data.category.Brand.Product.price_per_sq_mt;
        data.bottle_returned_value       = data.bottle_returned_value ? data.bottle_returned_value : 0;
        data.bottle_charge  = 0;
		data.initial_charge = parseFloat(data.product_alpha_charge) + parseFloat(final_charge) + (parseFloat(data.product_per_distance_charge) * parseFloat(data.order_distance)) + (parseFloat(data.product_per_hr_charge) * parseFloat(data.order_time)) + (parseFloat(data.product_per_sq_mt_charge) * parseFloat(data.product_sq_mt)) + (data.bottle_charge);
        data.coupon_charge  = 0;
        data.final_charge   = data.initial_charge;
        data.admin_charge   = parseFloat(((data.buraq_percentage * data.final_charge) / 100).toFixed(3));
        data.final_charge   = data.offer_price;
		var order           = await Services.orderServices.latestCreateNewRow(data);
		data.order_id       = order.order_id;
        var pp = Array();
        for (let i = 0; i < DriverDetails.length; i++)
        {
            var productss = Array();
            productss.order_id                  = data.order_id;
            productss.category_id               = data.category_id;
            productss.category_brand_id         = data.category_brand_id;
            productss.category_brand_product_id = DriverDetails[i].category_brand_product_id;
            productss.product_weight            = DriverDetails[i].product_weight;
            //productss.price_per_item            = DriverDetails[i].price_per_item;
            productss.price_per_item            = data.final_charge; 
            productss.image_url                 = "";
            productss.productName               = DriverDetails[i].productName;
            productss.product_quantity          = DriverDetails[i].product_quantity;
			productss.gift_offer                = DriverDetails[i].gift_offer;
            productss.created_at                = data.created_at;
            productss.updated_at                = data.created_at;
            productss.app_language_id           = data.app_language_id;
            await Services.orderServices.CreateProducts(productss);
        }
		var order_images     = [];
        var order_images_url = [];
        /*if (data.future == "0")
        {
            data.timeOutMessage = response.trans("Sorry, no driver is currently available");
            setTimeout(function () {
                orderRequestedTimeout(data);
            }, 47 * 1000);
        }*/
		 //////////////////      Drivers Push Plus Socket    ////////////////////////////
        if (order.fcm_socket_ids.length > 0) {
            var push_data = {
                order_id: (data.order_id).toString(),
                type: "OrderEvent",
                user_id: (data.customer_user_id).toString(),
                user_detail_id: (data.customer_user_detail_id).toString(),
                expiry: moment().utc().add(45, "seconds").format("YYYY-MM-DD HH:mm:ss")
            };
            var products = await Services.orderServices.AllOrderProducts(data);
            var socket_data = {
                type: "SerRequest",
                order: {
					release_offer_id: ReleaseId,
					quotation_id: "",
					reward_discount_price: order.reward_discount_price,  //Add M3 Part 3.2
					floor_charge: order.floor_charge,  //Add M3 Part 3.2
                    order_id: data.order_id,
                    driver_user_detail_id: order.driver_user_detail_id,
                    order_timings: data.order_timings,
                    pickup_address: order.pickup_address,
                    pickup_latitude: parseFloat(order.pickup_latitude),
                    pickup_longitude: parseFloat(order.pickup_longitude),
                    dropoff_address: order.dropoff_address,
                    dropoff_latitude: parseFloat(order.dropoff_latitude),
                    dropoff_longitude: parseFloat(order.dropoff_longitude),
					continouos_startdt: order.continouos_startdt ? order.continouos_startdt : "",//Add M3 Part 3.2
					continuous_enddt: order.continuous_enddt ? order.continuous_enddt : "",//Add M3 Part 3.2
					request_type: order.request_type ? order.request_type : "",//Add M3 Part 3.2
					NonContinueSOrder: [], //Add M3 Part 3.2
					order_request_timeout: 47, //Add M3 Part 3.2
					total_request_driver: 1, //Add M3 Part 3.2
                    payment: order.payment,
                    organisation_coupon_user_id: data.organisation_coupon_user_id,
                    future: data.future,
                    category_id: data.category_id,
                    brand: {
                        category_id: data.category_id,
                        category_brand_id: data.category_brand_id,
                        category_brand_product_id: data.category_brand_product_id,
                    },
                    user: {
                        user_id: data.customer_user_id,
                        user_detail_id: data.customer_user_detail_id,
                        name: Details.user.name,
                        rating_count: Details.user.rating_count,
                        rating_avg: Details.user.ratings_avg,
                        profile_pic: Details.profile_pic,
                        profile_pic_url: Details.profile_pic_url,
                        phone_number: Details.user.phone_number,
                        phone_code: Details.user.phone_code
                    },
                    coupon_users: null,
                    order_images_url,
                    details: order.details,
                    material_details: order.material_details,
                    products: products
                }
            };
            Libs.commonFunctions.pushPlusEventMultipleUsers(order.fcm_socket_ids, push_data, socket_data, order, data.category, "OrderEvent");
        }
        data.socket_ids = order.socket_ids;
        data.fcm_ids    = order.fcm_ids;
        var order       = await Services.orderServices.latestUserBookedOrderDetails(data);
        order = order[0];
        ///////////////////     Deduct Etokens, Coupon Users    ////////////////////////////////
        if (order.organisation_coupon_user)
        {
            order.organisation_coupon_user.quantity_left = order.organisation_coupon_user.quantity_left - data.product_quantity;
            Services.orgCouponUserServices.updateOrgCouponUser(order.organisation_coupon_user.organisation_coupon_user_id, order.organisation_coupon_user.quantity_left, data.created_at);
        }
		order.quotation_id = "";
		order.release_offer_id = ReleaseId;
		return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Service requested successfully"), result: order, data});
    } 
	catch (e) 
	{
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};
exports.getAllCustomerQuotation = async (request, response) => {
    try 
    {
        var data            = request.body;
        var Details         = data.Details;
		var categories      = await Db.categories.findOne({ where: {category_id: '4'}});
		//console.log(categories.interval_time);
		if(categories)
		{
			var intervaltime    = categories.interval_time;
		}else{
			var intervaltime    = 1;
		}
		var intervaltime    = categories.interval_time;
		data.or_1hr         = moment().subtract(intervaltime, 'h').format("YYYY-MM-DD HH:mm:ss");
		console.log(data.or_1hr);
		console.log(moment().format("YYYY-MM-DD HH:mm:ss"));
		var Quotations      = await Services.orderServices.getAllCustomerQuotation(data);
		//console.log("Quotations",JSON.stringify(Quotations));
		return response.status(200).json({success: 1, statusCode: 200,result: Quotations});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

//User Send Invitation API
//Role M3 Raghav
exports.userSendInvitation = async (request, response) => {
    try 
    {
        var data            = request.body;
        var Details         = data.Details;
		/*var invitation      = await Db.user_invitations.findOne({ where: {sender_user_detail_id: data.sender_user_detail_id,receiver_user_detail_id: Details.user_detail_id}});
		if(invitation)
		{
			return response.status(400).json({success: 1, statusCode: 400,msg: "Sorry, you have already inistalled app from this user."});
		}*/
		//Add New Entry After install
		var user_order_notification = await Db.user_invitations.create({
			sender_user_detail_id: data.sender_user_detail_id,
			receiver_user_detail_id: Details.user_detail_id,
			created_at: data.created_at
		});
		//Get User Existing Reward Point
		var userDetails      = await Db.user_details.findOne({ where: {user_detail_id: data.sender_user_detail_id}});
		var rewardpoint      = userDetails.total_reward_point;
		//Get Reward Point Per Invitation
		var RewardPoints     = await Db.reward_points.findOne({ where: {status: '1'}});
		
		if(RewardPoints)
		{
			console.log("RewardPoints",RewardPoints);
			var perinvite = RewardPoints.add_point;
		}else{
			var perinvite = 0;
		}
		//Add Reward Point
		var totalrewardpoint = parseInt(rewardpoint) + parseInt(perinvite);
		console.log("totalrewardpoint",totalrewardpoint);
		//Update Reward Point
		await Db.user_details.update(
			{
				total_reward_point: totalrewardpoint,
			},
			{
				where: {
					user_detail_id: data.sender_user_detail_id
				}
			}
		 );
		 
		return response.status(200).json({success: 1, statusCode: 200,result: data});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};


exports.paymentsuccess = async (request, response) => {
    try {
        var req              = request.body;
		console.log(req);
		var orderDetail      = await Db.orders.findOne({where: {order_id: req.orderId}});
		var CustomerID       = orderDetail.customer_user_detail_id;
		var DriverID         = orderDetail.driver_user_detail_id;
		console.log(CustomerID);
		console.log(DriverID);
		var driver_details   = await Db.user_details.findOne({where: {user_detail_id: DriverID}});
		var customer_details = await Db.user_details.findOne({where: {user_detail_id: CustomerID}});
		var data = Array();
		data.cust_push_data = {
			type: "PaymentNotification",
			message: "Order payment has been done successfully",
			future: "0",
			title: "YesSer"
		};
		Libs.commonFunctions.SendpaymentNotificationtousers(driver_details, data.cust_push_data);
		Libs.commonFunctions.SendpaymentNotificationtousers(customer_details, data.cust_push_data);
        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Updated successfully")});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

//module.exports = timeordercase;
//////////////////////		User Requests Service 		////////////////////////////////////
exports.getDriveravailableforservices = async (request, response) => {
    try {
        var data = request.body;
		
        var Details = request.uDetails;
        data.user_detail_id = Details.user_detail_id;
        data.user_id = Details.user_id;
        data.access_token = Details.access_token;
        data.app_language_id = request.app_language_id;
        data.user_latitude  = Details.latitude; //Add M3 Part 3.2
        data.user_longitude = Details.longitude; //Add M3 Part 3.2
        data.user_name = Details.user.name;
        data.created_at = request.created_at;
		
		data.request_type = data.request_type ? data.request_type : 'SingleRequest';
		
        request.checkBody("category_id", response.trans("Category id is required")).notEmpty();
        request.checkBody("category_brand_id", response.trans("Category brand id is required")).notEmpty();

            request.checkBody("dropoff_address", response.trans("Dropoff address is required")).notEmpty();
            request.checkBody("dropoff_latitude", response.trans("Dropoff latitude is required")).notEmpty();
            request.checkBody("dropoff_longitude", response.trans("Dropoff longitude is required")).notEmpty();

        if (data.category_id && data.category_id == 4 && data.category_id == 5) {
            request.checkBody("pickup_address", response.trans("Pickup address is required")).notEmpty();
            request.checkBody("pickup_latitude", response.trans("Pickup latitude is required")).notEmpty();
            request.checkBody("pickup_longitude", response.trans("Pickup longitude is required")).notEmpty();
        }
		if(data.request_type == "SingleRequest")
		{
			request.checkBody("order_timings", response.trans("Order timings is required")).notEmpty();
		}

        request.checkBody("future", response.trans("Future parameter is required")).notEmpty();
        request.checkBody("payment_type", response.trans("Payment type is required")).notEmpty();
        request.checkBody("payment_type", response.trans("Payment type can either be 'Cash' or 'Card' or 'eToken'")).isIn(["Card", "Cash", "eToken"]);
        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});
        data.category_brand_product_id = data.category_brand_product_id;
        if (data.category_id != 1 && data.category_id != 3 && data.category_id != 2 && data.category_id != 4 && data.category_id != 5 && data.category_id != 16)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, currently only gas, Drinking water, Water tanker and Truck services are available")});

        var coupon_id = data.coupon_id ? data.coupon_id : '0';
		
        ////////////        Cat Details         ///////////////////////
        var CateGoryProducts = Array();
        var JsonProducts = JSON.parse(data.products);
        var orderQuzntity = '0';
        for (let j = 0; j < JsonProducts.length; j++) {
            orderQuzntity = parseInt(orderQuzntity) + parseInt(JsonProducts[j].product_quantity);
            CateGoryProducts.push(JsonProducts[j].category_brand_product_id);
        }
        data.CateGoryProducts = CateGoryProducts;
		console.log(data.CateGoryProducts);
        data.category = await Services.categoryBrandProductServices.CatDetailsAllLang(data);
        if (!data.category)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, Please check your parameters")});

		//Add M2 Part 2
		if(parseFloat(data.category.Brand.is_default) == 1)
        {
            data.distance = parseFloat(data.category.maximum_distance);
        }
        else
        {
            data.distance = parseFloat(data.category.Brand.maximum_radius);
        }
        ////////////        Cat Details         ///////////////////////
        data.user_type_id = data.driver_user_type_id = 2;
        data.order_type = "Service";
        data.product_quantity = parseInt(orderQuzntity);
        data.seller_user_type_id = 2;
        ///////////////////////         Future Ride Check Add M3 Part 3.2       ///////////////////////////////////////
        if (data.future == "1" && data.request_type == "SingleRequest") {
            data.or_1hr = moment(data.now, "YYYY-MM-DD HH:mm:ss").add(55, "minutes");
            var isBefore = moment(data.order_timings, "YYYY-MM-DD HH:mm:ss").isBefore(data.or_1hr);
            if (isBefore && data.category_id == 2)
                return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Please select timing one hour after from now for scheduling a service")});

            var scheduled_check = await Db.orders.findOne({
                where: {
                    customer_user_id: data.user_id,
                    customer_user_detail_id: data.user_detail_id,
                    order_status: {$in: ["Scheduled", "DPending", "DApproved"]}
                }
            });

            if (scheduled_check && scheduled_check.order_type == "Support")
                return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, you already have scheduled a support"), scheduled_check});
        }
		
        ///////////////////////         Future Ride Check       ///////////////////////////////////////
        data.organisation_coupon_user_id = 0;
        // //////////////////////// Coupon Check    //////////////////////////////////
		////////////////////////    Reward Point Update Part M3.2 ////////////////////////////
		var userDetails      = await Db.user_details.findOne({ where: {user_detail_id: Details.user_detail_id}});
		var rewardpoint      = userDetails.total_reward_point; 
		var redemmedpoint    = data.reward_discount_point ? data.reward_discount_point : 0;

        var cc = Array();
        var final_charge = '0.0';
        for (let i = 0; i < JsonProducts.length; i++) {
            final_charge = parseFloat(final_charge) + parseFloat(JsonProducts[i].price_per_item);
            data.product_actual_value = JsonProducts[i].price_per_item;
            data.product_alpha_charge = data.category.Brand.Product.alpha_price;
            data.product_per_quantity_charge = data.category.Brand.Product.price_per_quantity;
            data.product_per_weight_charge = data.category.Brand.Product.price_per_weight;
            data.product_per_distance_charge = data.category.Brand.Product.price_per_distance;
            data.product_per_hr_charge = data.category.Brand.Product.price_per_hr;
            data.product_per_sq_mt_charge = data.category.Brand.Product.price_per_sq_mt;
            data.bottle_returned_value = data.bottle_returned_value ? data.bottle_returned_value : 0;
            cc.push(JsonProducts[i].category_brand_product_id);
        }
        var CategoryProductsID = cc.toString();
        data.CategoryProductsID = CategoryProductsID;
		var DriverAvail = [];
        if (data.category_id == 2) { ////////////       Drinking water Service  ///////////////
            data.day_string_check = Libs.commonFunctions.DayString(data.order_timings, data.timezone);
            DriverAvail = await Services.driverListingServices.AllDrivers4OrdersDrinkingWater(data);
            data.coupon_user_id = 0;
        } else {////////////       Other Service   ///////////////////////
            if (data.category_id && (data.category_id == 4 || data.category_id == "4")) {
				//Add M2 Part 2
				var driver_user_detail_id = data.driver_user_detail_id ? data.driver_user_detail_id : '0';
				DriverAvail = await Services.driverListingServices.Driver4TruckOrders(data);
				console.log('singledoubledrivers=====',data.drivers);
            } else {
                DriverAvail = await Services.driverListingServices.Driver4OrdersExceptDrinkingWater(data);
            }
        }
		//console.log(DriverAvail);
		//End Non Continue Create Multiple Orders
		if (DriverAvail.length == 0 && process.env.NODE_ENV != "development") {
			return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, no driver is currently available")});
        }
		
		var order_requests = [];
		for(var t =0;t<DriverAvail.length;t++)
		{	
			if(DriverAvail[t].profile_pic != "")
			{
				var profilePicURL = process.env.RESIZE_URL + DriverAvail[t].profile_pic;
			}else{
				var profilePicURL = "";
			}
			var lat = DriverAvail[t].latitude;  
			var lng = DriverAvail[t].longitude; 
			var nam = data.user_latitude;  
			var cou = data.user_longitude;
			var d   = calculateDistance(nam, cou, lat, lng, "K");
			var rating = await Services.userDetailServices.userRatings('2',DriverAvail[t].user_detail_id);
			console.log(rating);
			order_requests.push({
				user_id: DriverAvail[t].user_id,
				user_detail_id: DriverAvail[t].user_detail_id,
				user_type_id: 2,
				category_id: DriverAvail[t].category_id,
				category_brand_id: DriverAvail[t].category_brand_id,
				name: DriverAvail[t].name,
				latitude: DriverAvail[t].latitude,
				longitude: DriverAvail[t].longitude,
				rating_count: rating ? rating[0].rating_count : 0,
				rating_avg: rating ? rating[0].ratings_avg : 0,
				profile_pic: DriverAvail[t].profile_pic,
				profile_pic_url: profilePicURL,
				distance_from_customer: d.toFixed(2),
				created_at: data.created_at,
				updated_at: data.created_at
			});
		}
		
		console.log(order_requests);
		
        ///////////////////     Deduct Etokens, Coupon Users    ////////////////////////////////
        return response.status(200).json({success: 1, statusCode: 200, msg: "Fetch driver list successfully", result: order_requests});

    } catch (e)
    {
        console.log("e ===", e);
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

function calculateDistance(lat1, lon1, lat2, lon2, unit) {
	  var radlat1 = Math.PI * lat1/180
	  var radlat2 = Math.PI * lat2/180
	  var radlon1 = Math.PI * lon1/180
	  var radlon2 = Math.PI * lon2/180
	  var theta = lon1-lon2
	  var radtheta = Math.PI * theta/180
	  var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	  dist = Math.acos(dist)
	  dist = dist * 180/Math.PI
	  dist = dist * 60 * 1.1515
	  if (unit=="K") { dist = dist * 1.609344 }
	  if (unit=="N") { dist = dist * 0.8684 }
	  return dist;
}