
var Libs = require(appRoot+"/libs");//////////////////		Libraries
var Services = require(appRoot+"/services");///////////		Services
var Db = require(appRoot+"/models");//////////////////		Db Models
//var Configs = require(appRoot+"/configs");/////////////		Configs

/**
 * <b>User Etoken Companies Listings</b>
 *
 * @private
 * @method            POST
 */
exports.userCompaniesList = async (request, response) => {
	try{
		var data = request.body;

		request.checkBody("skip", response.trans("Skip is required")).notEmpty();
		request.checkBody("take", response.trans("Take is required")).notEmpty();

		request.checkBody("latitude", response.trans("Latitude is required")).notEmpty();
		request.checkBody("longitude", response.trans("Longitude is required")).notEmpty();

		request.checkBody("category_brand_id", response.trans("category_brand_id is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var companies = await Services.waterServices.newUserGetServingCompaniesList(data.skip, data.take, data.latitude, data.longitude, data.category_brand_id, data.category_brand_product_id);
    
		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Companies listings"), result: companies });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

////////////////////        User Purchased List         /////////////////////////////////////
exports.userPurchasesList = async (request, response) => {
	try{

		var data = request.body;

		request.checkBody("skip", response.trans("Skip is required")).notEmpty();
		request.checkBody("take", response.trans("Take is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var companies = await Services.waterServices.userPurchasedeTokenList(data.user_detail_id, parseInt(data.skip), parseInt(data.take), data.app_language_id);

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Companies listings"), result: companies });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

///////////////////     Single Organisation Etokens List        //////////////////////////////
exports.usereTokenList = async (request, response) => {
	try{

		var data = request.body;

		request.checkBody("organisation_id", response.trans("organisation_id is required")).notEmpty();
		request.checkBody("category_brand_id", response.trans("category_brand_id is required")).notEmpty();

		request.checkBody("skip", response.trans("Skip is required")).notEmpty();
		request.checkBody("take", response.trans("Take is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var org = await Services.waterServices.orgDetails4EToken(data.organisation_id);

		var etokens = await Services.waterServices.singleComanyEtokensList(data.organisation_id, data.category_brand_id, parseInt(data.skip), parseInt(data.take), data.app_language_id);

		var result = {
			//data,
			org,
			etokens 
		};
    
		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Etokens listings"), result });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

//////////////////      User Etoken Purchase        ////////////////////////////////////
exports.usereTokenPurchase = async (request, response) => {
	try{

		var data = request.body;

		request.checkBody("organisation_coupon_id", response.trans("organisation_coupon_id is required")).notEmpty();
		request.checkBody("buraq_percentage", response.trans("buraq_percentage is required")).notEmpty();

		request.checkBody("bottle_returned_value", response.trans("bottle_returned_value is required")).notEmpty();
		request.checkBody("bottle_charge", response.trans("bottle_charge is required")).notEmpty();

		request.checkBody("quantity", response.trans("quantity is required")).notEmpty();
		request.checkBody("payment_type", response.trans("payment_type is required")).notEmpty();//Cash, Card

		request.checkBody("price", response.trans("price is required")).notEmpty();
		request.checkBody("eToken_quantity", response.trans("eToken_quantity is required")).notEmpty();

		request.checkBody("address", response.trans("address is required")).notEmpty();
		request.checkBody("address_latitude", response.trans("address_latitude is required")).notEmpty();
		request.checkBody("address_longitude", response.trans("address_longitude is required")).notEmpty();


		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var eToken = await Services.waterServices.etokenDetails4Purchase(data.organisation_coupon_id);
		if(!eToken)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this eToken is no longer available") });

		/////////// Past Purchased Nearest Record within 3 Kms
		var purchasedNearest = await Services.waterServices.userPurchasedNearestEtoken(data.user_detail_id, eToken.organisation_id, eToken.category_brand_id, eToken.category_brand_product_id, data.address_latitude, data.address_longitude, 3);

		if(!purchasedNearest)
		{//////////////     First Time      ///////////////////////

			purchasedNearest = await Db.organisation_coupon_users.create({
				organisation_coupon_id: data.organisation_coupon_id,
				category_id: eToken.category_id,
				category_brand_id: eToken.category_brand_id,
				category_brand_product_id: eToken.category_brand_product_id,
				seller_user_id: eToken.user_id,
				seller_user_detail_id: eToken.user_detail_id,
				seller_user_type_id: eToken.user_type_id,
				seller_organisation_id: eToken.organisation_id,
				customer_user_id: data.user_id,
				customer_user_detail_id: data.user_detail_id,
				customer_user_type_id: data.user_type_id,
				customer_organisation_id: data.user_organisation_id,
				bottle_quantity: data.quantity,
				quantity: eToken.quantity,
				quantity_left: eToken.quantity,
				address: data.address,
				address_latitude: data.address_latitude,
				address_longitude: data.address_longitude,
				created_at: data.created_at,
				updated_at: data.created_at
			});

		}//////////////     First Time      ///////////////////////
		else
		{//////////////     Not First Time  ///////////////////////

			purchasedNearest = JSON.parse(JSON.stringify(purchasedNearest));

			data.updateHistory = await Db.organisation_coupon_users.update(
				{
					bottle_quantity: data.quantity,
					quantity: Db.sequelize.literal("quantity +"+data.eToken_quantity),
					quantity_left: Db.sequelize.literal("quantity_left +"+data.eToken_quantity),
					updated_at: data.created_at
				},
				{
					where: { organisation_coupon_user_id: purchasedNearest.organisation_coupon_user_id }
				}
			);

		}//////////////     Not First Time  ///////////////////////

		////////////    Price Calculation       ////////////////////////////
		data.initial_charge = parseFloat(data.price);
		data.bottle_charge = (data.bottle_returned_value == 0) ? parseFloat(data.bottle_charge) : 0;

		data.initial_charge = data.initial_charge + data.bottle_charge;

		data.admin_charge = parseFloat(((data.initial_charge/100)*parseFloat(data.buraq_percentage)).toFixed(3));

		data.final_charge = data.initial_charge+data.admin_charge;
		data.bank_charge = 0;
		////////////    Price Calculation       ////////////////////////////

		data.payment = await Db.payments.create({
			customer_user_id: data.user_id,
			customer_user_detail_id: data.user_detail_id,
			customer_user_type_id: data.user_type_id,
			user_card_id: 0,
			customer_organisation_id: data.user_organisation_id,
			order_id: 0,
			seller_user_id: eToken.user_id,
			seller_user_detail_id: eToken.user_detail_id,
			seller_user_type_id: eToken.user_type_id,
			seller_organisation_id: eToken.organisation_id,
			organisation_coupon_user_id: purchasedNearest.organisation_coupon_user_id,
			payment_type: data.payment_type,
			payment_status: "Pending",
			refund_status: "NoNeed",
			transaction_id: null,
			refund_id: null,
			buraq_percentage: data.buraq_percentage,
			product_actual_value: data.eToken_quantity,
			product_quantity: data.quantity,
			initial_charge: data.initial_charge,
			admin_charge: data.admin_charge,
			bank_charge: data.bank_charge,
			final_charge: data.final_charge,
			bottle_charge: data.bottle_charge,
			bottle_returned_value: data.bottle_returned_value,
			created_at: data.created_at,
			updated_at: data.created_at
		});

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Etoken purchased successfully") });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

//////////////////////      Customer COnfirm Order      ////////////////////////////////
exports.userConfirmOrder = async (request, response) => {
	try{
		var data = request.body;

		request.checkBody("order_id", response.trans("order_id is required")).notEmpty();
		request.checkBody("status", response.trans("status is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var order = await Services.orderServices.usereTokenOrderDetails(data.order_id, data.user_detail_id);
		if(!order)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not available"), result: order });
		else if(order.organisation_coupon_user_id == 0)
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this facility is only available for etoken orders"), result: order });
		else if(order.order_status == "SerComplete" || order.SerCustConfirm == "SerCustConfirm")
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already completed"), result: order });
		else if((order.order_status == "CustCancel") || (order.order_status == "SerCancel") || (order.order_status == "SupCancel") || (order.order_status == "SerReject") || (order.order_status == "SerTimeout") || (order.order_status == "DSchCancelled") || (order.order_status == "DSchTimeout") || (order.order_status == "SysSchCancelled") )
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already cancelled"), result: order });
		else if(order.order_status != "SerCustPending")
			return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is no longer pending for your confirmation"), result: order });

		if(data.status == "0")
		{////////////       Reject Order        //////////////////////////////

			data.order_status = "SerCustCancel";

			var msg = response.trans("Order rejected successfully");

			var message = Libs.commonFunctions.generate_lang_message(" has rejected your order", order.DriverDetails.language_id);

		}////////////       Reject Order        //////////////////////////////
		else
		{////////////       Accept Order        //////////////////////////////

			data.order_status = "SerCustConfirm";

			var msg = response.trans("Order confirmed successfully");

			var message = Libs.commonFunctions.generate_lang_message(" has confirmed your order", order.DriverDetails.language_id);

			data.transaction_id = "P-"+Libs.commonFunctions.uniqueId();

			data.update_payment = await Db.payments.update(
				{
					payment_status: "Completed",
					updated_at: data.created_at
				},
				{
					where: { payment_id: order.payment.payment_id }
				}
			);

			data.update_etoken_payments = await Db.payments.update(
				{
					transaction_id: data.transaction_id,
					payment_status: "Completed",
					updated_at: data.created_at
				},
				{
					where: {
						customer_user_detail_id: data.user_detail_id,
						organisation_coupon_user_id: order.organisation_coupon_user_id,
						payment_status: "Pending",
						refund_status: "NoNeed"
					}
				}
			);

			data.updateHistory = await Db.organisation_coupon_users.update(
				{
					quantity_left: Db.sequelize.literal("quantity_left -"+order.payment.product_quantity),
					updated_at: data.created_at
				},
				{
					where: { organisation_coupon_user_id: order.organisation_coupon_user_id }
				}
			);

		}////////////       Accept Order        //////////////////////////////

		///////////////     Update Db       ///////////////////////////////////
		data.update_order = await Db.orders.update(
			{
				order_status: data.order_status,// SerCustCancel or SerCustConfirm
				updated_at: data.created_at
			},
			{
				where: {
					order_id: order.order_id
				}
			}
		);
		///////////////     Update Db       ///////////////////////////////////

		////////////////		Socket Event 	///////////////////////////
		var temp_order = await Services.orderServices.driverAcceptOrderDetails(data, order.DriverDetails.language_id);
                 
		data.socket_data = {
			type: data.order_status,// SerCustCancel or SerCustConfirm
			order: temp_order,
			message: data.Details.user.name+message
		};
		Libs.commonFunctions.emitToDevice("OrderEvent", order.DriverDetails.user_detail_id, data.socket_data);
		////////////////		Socket Event 	///////////////////////////

		////////////////		Push Notifications 	///////////////////////
		if(order.DriverDetails.fcm_id != "" && order.DriverDetails.notifications == "1")
		{
			data.cust_push_data = {
				order_id: (data.order_id).toString(),
				type: data.order_status,// SerCustCancel or SerCustConfirm
				message: data.Details.user.name+message
			};

			var push = Libs.commonFunctions.latestSendPushNotification([order.DriverDetails], data.cust_push_data);
		}
		////////////////		Push Notifications 	///////////////////////

		return response.status(200).json({ success: 1, statusCode: 200, msg, result: order });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};