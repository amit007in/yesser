var moment = require("moment");
var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;

var Libs = require(appRoot + "/libs");//////////////////		Libraries
var Services = require(appRoot + "/services");///////////		Services
var Db = require(appRoot + "/models");//////////////////		Db Models
var Configs = require(appRoot + "/configs");/////////////		Configs

//var commonFunctions = require(appRoot+"/libs/commonFunctions");//////////////////		Libraries
/////////////////////////////		Service Drivers Listings		///////////////////////////////
exports.homeApi = async (request, response) => {
    try {
        var data = request.body;
        request.checkBody("category_id", response.trans("Service type is required")).notEmpty();
        request.checkBody("latitude", response.trans("Latitude is required")).notEmpty();
        request.checkBody("longitude", response.trans("Longitude is required")).notEmpty();
        request.checkBody("distance", response.trans("Distance is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});
        // get category type from database
        var category_type = await Services.categoryServices.getCategoryTypeById(data);

        if (category_type[0]["category_type"] == "Service") {
            data.user_type_id = 2;
            data.category_type = "Service";

            var categories = await Services.categoryServices.singleServiceCatDetails(data);
        } else {
            data.user_type_id = 3;
            data.category_type = "Support";
            var categories = [];
        }

        data.past_5Mins = moment(data.created_at, "YYYY-MM-DD HH:mm:ss").subtract(2, "days").format("YYYY-MM-DD HH:mm:ss");
        var drivers = await Services.driverListingServices.userHomeMapDrivers(data);

        var currentOrders = await Services.orderServices.customerCurrentOrders(data);
        var lastCompletedOrders = await Services.orderServices.customerLastCompletedOrders(data.user_detail_id, data.app_language_id);

        var result = {
            data,
            categories,
            drivers,
            //Details,
            currentOrders,
            lastCompletedOrders,
            Versioning: Configs.appData.Versioning
        };
        //console.log("home api result", result);
        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Service drivers listings"), result});

    } catch (e) {
        console.log("error home api ==", e);
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

////////////////////////		Order Timeout 		/////////////////////////////////////
exports.userTesting = async (request, response) => {
    try {

        var data = request.body;

        return response.status(200).json({success: 1, statusCode: 200, msg: data});

    } catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

//////////////////////		User Requests Service 		////////////////////////////////////
exports.userServiceRequest = async (request, response) => {
    try {
        var data = request.body;
        // console.log("requiest=====>",request.files);
        var Details = request.uDetails;
        data.user_detail_id = Details.user_detail_id;
        data.user_id = Details.user_id;
        data.access_token = Details.access_token;
        data.app_language_id = request.app_language_id;
        data.user_name = Details.user.name;
        data.created_at = request.created_at;
        request.checkBody("category_id", response.trans("Category id is required")).notEmpty();
        request.checkBody("category_brand_id", response.trans("Category brand id is required")).notEmpty();
        //request.checkBody("products.*.category_brand_product_id", response.trans("Category brand product is required")).notEmpty();
        //request.checkBody("products.*.product_quantity", response.trans("Product quantity is required")).notEmpty();
        //if (data.category_id != 5) {
            request.checkBody("dropoff_address", response.trans("Dropoff address is required")).notEmpty();
            request.checkBody("dropoff_latitude", response.trans("Dropoff latitude is required")).notEmpty();
            request.checkBody("dropoff_longitude", response.trans("Dropoff longitude is required")).notEmpty();
        //}
        if (data.category_id && data.category_id == 4 && data.category_id == 5) {
            request.checkBody("pickup_address", response.trans("Pickup address is required")).notEmpty();
            request.checkBody("pickup_latitude", response.trans("Pickup latitude is required")).notEmpty();
            request.checkBody("pickup_longitude", response.trans("Pickup longitude is required")).notEmpty();
        }
        request.checkBody("order_timings", response.trans("Order timings is required")).notEmpty();
        request.checkBody("future", response.trans("Future parameter is required")).notEmpty();
        request.checkBody("payment_type", response.trans("Payment type is required")).notEmpty();
        request.checkBody("payment_type", response.trans("Payment type can either be 'Cash' or 'Card' or 'eToken'")).isIn(["Card", "Cash", "eToken"]);
        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});
        data.category_brand_product_id = data.category_brand_product_id;
        if (data.category_id != 1 && data.category_id != 3 && data.category_id != 2 && data.category_id != 4 && data.category_id != 5 && data.category_id != 16)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, currently only gas, Drinking water, Water tanker and Truck services are available")});


        var coupon_id = data.coupon_id ? data.coupon_id : '0';
        console.log("sasdfsdfs", data);
        if (coupon_id != '0') {
            var max_rides = await Services.userServices.checkPromoUserMaxRides(data);
            if (max_rides != null) {
                var max_ridess = JSON.parse(JSON.stringify(max_rides));
                var OldRides = max_ridess.max_rides;
                if (OldRides == 0) {
                    return response.status(406).json({success: 0, statusCode: 406, msg: response.trans("Coupon maximum limit reached."), result: {}});
                }
            }
        }

        ////////////        Cat Details         ///////////////////////
        var CateGoryProducts = Array();
        var JsonProducts = JSON.parse(data.products);
        var orderQuzntity = '0';
        for (let j = 0; j < JsonProducts.length; j++) {
            orderQuzntity = parseInt(orderQuzntity) + parseInt(JsonProducts[j].product_quantity);
            CateGoryProducts.push(JsonProducts[j].category_brand_product_id);
        }
        data.CateGoryProducts = CateGoryProducts;
        data.category = await Services.categoryBrandProductServices.CatDetailsAllLang(data);
        console.log("rien ==", data.category);
        if (!data.category)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, Please check your parameters")});
        data.distance = parseFloat(data.category.maximum_distance);
        ////////////        Cat Details         ///////////////////////
        data.user_type_id = data.driver_user_type_id = 2;
        data.order_type = "Service";
        data.product_quantity = parseInt(orderQuzntity);
        data.seller_user_type_id = 2;
        ///////////////////////         Future Ride Check       ///////////////////////////////////////
        if (data.future == "1") {
            data.or_1hr = moment(data.now, "YYYY-MM-DD HH:mm:ss").add(55, "minutes");
            var isBefore = moment(data.order_timings, "YYYY-MM-DD HH:mm:ss").isBefore(data.or_1hr);
            if (isBefore && data.category_id == 2)
                return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Please select timing one hour after from now for scheduling a service")});

            var scheduled_check = await Db.orders.findOne({
                where: {
                    customer_user_id: data.user_id,
                    customer_user_detail_id: data.user_detail_id,
                    order_status: {$in: ["Scheduled", "DPending", "DApproved"]}
                }
            });
            //if(scheduled_check && scheduled_check.order_type == "Service")
            //return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, you already have scheduled a service"), scheduled_check });
            //  else if(scheduled_check && scheduled_check.order_type == "Support")
            if (scheduled_check && scheduled_check.order_type == "Support")
                return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, you already have scheduled a support"), scheduled_check});
        }
        ///////////////////////         Future Ride Check       ///////////////////////////////////////
        data.organisation_coupon_user_id = 0;
        // //////////////////////// Coupon Check    //////////////////////////////////
        if (data.coupon_user_id) {
            var coupon_users = await Services.couponUserServices.purchasedCouponDetail4Order(data);
            if (!coupon_users)
                return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this coupon is no longer valid")});
        } else {
            data.coupon_user_id = 0;
        }
        ////////////////////////    Coupon Check    //////////////////////////////////
        //Start 25-April-2019
        var cc = Array();
        var final_charge = '0.0';
        for (let i = 0; i < JsonProducts.length; i++) {
            final_charge = parseFloat(final_charge) + parseFloat(JsonProducts[i].price_per_item);

            data.product_actual_value = JsonProducts[i].price_per_item;
            data.product_alpha_charge = data.category.Brand.Product.alpha_price;
            data.product_per_quantity_charge = data.category.Brand.Product.price_per_quantity;
            //data.product_per_quantity_charge = final_charge;
            data.product_per_weight_charge = data.category.Brand.Product.price_per_weight;
            data.product_per_distance_charge = data.category.Brand.Product.price_per_distance;
            data.product_per_hr_charge = data.category.Brand.Product.price_per_hr;
            data.product_per_sq_mt_charge = data.category.Brand.Product.price_per_sq_mt;
            data.bottle_returned_value = data.bottle_returned_value ? data.bottle_returned_value : 0;

            cc.push(JsonProducts[i].category_brand_product_id);
        }
        console.log('finalcharge=======', final_charge);
        var CategoryProductsID = cc.toString();
        data.CategoryProductsID = CategoryProductsID;
        //End 25-April-2019
        if (data.category_id == 2) { ////////////       Drinking water Service  ///////////////
            data.day_string_check = Libs.commonFunctions.DayString(data.order_timings, data.timezone);
            data.drivers = await Services.driverListingServices.AllDrivers4OrdersDrinkingWater(data);
            data.coupon_user_id = 0;
        } else {////////////       Other Service   ///////////////////////
            if (data.category_id && (data.category_id == 4 || data.category_id == "4")) {
                data.drivers = await Services.driverListingServices.Driver4TruckOrders(data);
            } else {
                data.drivers = await Services.driverListingServices.Driver4OrdersExceptDrinkingWater(data);
            }
        }
        ///////////////////         No Driver Found SMS     ////////////////////////
        data.timings = moment(data.order_timings, "YYYY-MM-DD HH:mm:ss").tz("Asia/Muscat").format("YYYY-MM-DD hh:mm:ss");
        data.sms_message = "Alert:  New " + data.category.category_details[0].name + " Order, Product: " + data.category.Brand.Product.category_brand_product_details[0].name + " x " + data.product_quantity + ", Name: " + Details.user.name + ", Mobile: " + Details.user.phone_number + ", Order date: " + data.timings;
        if (data.drivers.length == 0 && process.env.NODE_ENV != "development") {
            // var sendSMS = Libs.commonFunctions.multipleSendSMS(data.sms_message+", Status: No Driver", Configs.appData.sms_numbers);
            //send data to database
            data.sms = [];
            //send data to database
            data.sms.message = "New " + data.category.category_details[0].name + " Order";
            data.sms.customer_id = Details.user_id;
            data.sms.products = data.products;
            data.sms.customer_id = data.user_id;
            data.sms.customer_name = data.user_name;
            data.sms.order_date = data.timings;
            data.sms.sms_object = data.sms_message;

            await Services.userServices.AddOrderFailure(data.sms);
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, no driver is currently available")});
        }
        ///////////////////         No Driver Found SMS     ////////////////////////
        data.order_token = "OR-" + Libs.commonFunctions.uniqueId();
        ///////////     Customer    /////////////////
        data.customer_user_id = Details.user_id;
        data.customer_user_detail_id = Details.user_detail_id;
        data.customer_organisation_id = Details.organisation_id;
        data.customer_user_type_id = Details.user_type_id;
        data.user_card_id = 0;
        ///////////     Customer    /////////////////
        data.order_status = "Searching",
                data.payment_status = "Pending",
                data.refund_status = "NoNeed",
                data.created_by = "User";
        data.order_distance = data.order_distance ? parseFloat(data.order_distance) : 0;
        data.order_time = 0;
        data.product_sq_mt = 0;
        data.product_weight = data.product_weight ? parseFloat(data.product_weight) : 0;
        data.buraq_percentage = parseFloat(data.category.buraq_percentage);
        //////////////////////      Buraq Percentage        ////////////////////////
        data.product_actual_value = data.category.Brand.Product.actual_value;
        data.product_alpha_charge = data.category.Brand.Product.alpha_price;
        data.product_per_quantity_charge = data.category.Brand.Product.price_per_quantity;
        //data.product_per_quantity_charge = final_charge;
        data.product_per_weight_charge = data.category.Brand.Product.price_per_weight;
        data.product_per_distance_charge = data.category.Brand.Product.price_per_distance;
        data.product_per_hr_charge = data.category.Brand.Product.price_per_hr;
        data.product_per_sq_mt_charge = data.category.Brand.Product.price_per_sq_mt;
        data.bottle_returned_value = data.bottle_returned_value ? data.bottle_returned_value : 0;


        data.bottle_charge = 0;
        //////////////////      Pricing Category Wise       ///////////////////////////////////////
        if (data.category_id == 1)
        {/////////////////////      Gas         ///////////////////////////////////////////////
            data.initial_charge = parseFloat(data.product_alpha_charge) + (parseFloat(data.product_per_quantity_charge) * parseFloat(data.product_quantity));
        }/////////////////////      Gas         ///////////////////////////////////////////////
        else
        {
            data.initial_charge = parseFloat(data.product_alpha_charge) + (parseFloat(data.product_per_quantity_charge) * parseFloat(data.product_quantity)) + (parseFloat(data.product_per_distance_charge) * parseFloat(data.order_distance)) + (parseFloat(data.product_per_hr_charge) * parseFloat(data.order_time)) + (parseFloat(data.product_per_sq_mt_charge) * parseFloat(data.product_sq_mt)) + (data.bottle_charge);
        }
        data.coupon_charge = 0;
        data.final_charge = data.initial_charge;
        data.admin_charge = parseFloat(((data.buraq_percentage * data.final_charge) / 100).toFixed(3));
        data.final_charge = parseFloat((data.final_charge + data.admin_charge).toFixed(3));
        var order = await Services.orderServices.latestCreateNewRow(data);

        data.order_id = order.order_id;
        //Add Products Start 25-April-2019
        var pp = Array();
        for (let i = 0; i < JsonProducts.length; i++)
        {
            var productss = Array();
            productss.order_id = data.order_id;
            productss.category_id = data.category_id;
            productss.category_brand_id = data.category_brand_id;
            productss.category_brand_product_id = JsonProducts[i].category_brand_product_id;
            productss.product_weight = JsonProducts[i].product_weight;
            productss.price_per_item = JsonProducts[i].price_per_item;
            productss.image_url = JsonProducts[i].image_url;
            productss.productName = JsonProducts[i].productName;
            productss.product_quantity = JsonProducts[i].product_quantity;
            productss.created_at = data.created_at;
            productss.updated_at = data.created_at;
            productss.app_language_id = data.app_language_id;
            await Services.orderServices.CreateProducts(productss);
        }

        /*var couponId = data.coupon_id ? data.coupon_id : 0;
         if(couponId > 0)
         {
         console.log("adsdfsdfsdf",couponId);
         await Services.orderServices.DeductCouponRides(data);
         }*/
        //End 25-April-2019
        var order_images = [];
        var order_images_url = [];
        if (request.files && request.files.length > 0) {
            request.files.forEach(async (image) => {
                order_images.push({
                    order_id: data.order_id,
                    image: image.filename,
                    created_at: data.created_at,
                    updated_at: data.created_at
                });
                order_images_url.push({
                    order_id: data.order_id,
                    image: image.filename,
                    image_url: process.env.RESIZE_URL + image.filename
                });
            });
        }

        if (order_images.length > 0)
            await Db.order_images.bulkCreate(order_images);
        //////////////////      Drivers Push Plus Socket    ////////////////////////////
        if (order.fcm_socket_ids.length > 0) {
            var push_data = {
                order_id: (data.order_id).toString(),
                type: "OrderEvent",
                user_id: (data.customer_user_id).toString(),
                user_detail_id: (data.customer_user_detail_id).toString(),
                expiry: moment().utc().add(45, "seconds").format("YYYY-MM-DD HH:mm:ss")
            };
            var products = await Services.orderServices.AllOrderProducts(data);
            //console.log("products ==", products);
            var socket_data = {
                type: "SerRequest",
                order: {
                    order_id: data.order_id,
                    driver_user_detail_id: order.driver_user_detail_id,
                    order_timings: data.order_timings,
                    pickup_address: order.pickup_address,
                    pickup_latitude: parseFloat(order.pickup_latitude),
                    pickup_longitude: parseFloat(order.pickup_longitude),
                    dropoff_address: order.dropoff_address,
                    dropoff_latitude: parseFloat(order.dropoff_latitude),
                    dropoff_longitude: parseFloat(order.dropoff_longitude),
                    payment: order.payment,
                    organisation_coupon_user_id: data.organisation_coupon_user_id,
                    future: data.future,
                    category_id: data.category_id,
                    brand: {
                        category_id: data.category_id,
                        category_brand_id: data.category_brand_id,
                        category_brand_product_id: data.category_brand_product_id,
                    },
                    user: {
                        user_id: data.customer_user_id,
                        user_detail_id: data.customer_user_detail_id,
                        name: Details.user.name,
                        rating_count: Details.user.rating_count,
                        rating_avg: Details.user.ratings_avg,
                        profile_pic: Details.profile_pic,
                        profile_pic_url: Details.profile_pic_url,
                        phone_number: Details.user.phone_number,
                        phone_code: Details.user.phone_code
                    },
                    coupon_users: coupon_users ? coupon_users : null,
                    order_images_url,
                    details: order.details,
                    material_details: order.material_details,
                    products: products
                }
            };
            Libs.commonFunctions.pushPlusEventMultipleUsers(order.fcm_socket_ids, push_data, socket_data, order, data.category, "OrderEvent");
        }
        //////////////////      Drivers Push Plus Socket    ////////////////////////////
        //////////      No Response Timeout After 2 Minutes     ///////////
        if (data.future == "0")
        {
            data.timeOutMessage = response.trans("Sorry, no driver is currently available");
            setTimeout(function () {
                //data.order_request_status = "Searching";
                orderRequestedTimeout(data);// this code will only run when time has ellapsed
            }, 47 * 1000);
        }
        //////////      No Response Timeout After 2 Minutes     ///////////
        data.socket_ids = order.socket_ids;
        data.fcm_ids = order.fcm_ids;
        var order = await Services.orderServices.latestUserBookedOrderDetails(data);
        order = order[0];
        ///////////////////     Deduct Etokens, Coupon Users    ////////////////////////////////
        if (order.organisation_coupon_user)
        {
            order.organisation_coupon_user.quantity_left = order.organisation_coupon_user.quantity_left - data.product_quantity;
            Services.orgCouponUserServices.updateOrgCouponUser(order.organisation_coupon_user.organisation_coupon_user_id, order.organisation_coupon_user.quantity_left, data.created_at);
        }
        //}
        ///////////////////     Deduct Etokens, Coupon Users    ////////////////////////////////
        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Service requested successfully"), result: order, data});

    } catch (e)
    {
        console.log("e ===", e);
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};



exports.userServiceRequests = async (request, response) => {
    try {
        var data = request.body;
        console.log("Request api =>", request.files);
        var Details = request.uDetails;
        data.user_detail_id = Details.user_detail_id;
        data.user_id = Details.user_id;
        data.access_token = Details.access_token;
        data.app_language_id = request.app_language_id;
        data.user_name = Details.user.name;
        data.created_at = request.created_at;
        request.checkBody("category_id", response.trans("Category id is required")).notEmpty();
        request.checkBody("category_brand_id", response.trans("Category brand id is required")).notEmpty();
        //request.checkBody("products.*.category_brand_product_id", response.trans("Category brand product is required")).notEmpty();
        //request.checkBody("products.*.product_quantity", response.trans("Product quantity is required")).notEmpty();
        request.checkBody("dropoff_address", response.trans("Dropoff address is required")).notEmpty();
        request.checkBody("dropoff_latitude", response.trans("Dropoff latitude is required")).notEmpty();
        request.checkBody("dropoff_longitude", response.trans("Dropoff longitude is required")).notEmpty();
        if (data.category_id && data.category_id == 4 && data.category_id == 5) {
            request.checkBody("pickup_address", response.trans("Pickup address is required")).notEmpty();
            request.checkBody("pickup_latitude", response.trans("Pickup latitude is required")).notEmpty();
            request.checkBody("pickup_longitude", response.trans("Pickup longitude is required")).notEmpty();
        }
        request.checkBody("order_timings", response.trans("Order timings is required")).notEmpty();
        request.checkBody("future", response.trans("Future parameter is required")).notEmpty();
        request.checkBody("payment_type", response.trans("Payment type is required")).notEmpty();
        request.checkBody("payment_type", response.trans("Payment type can either be 'Cash' or 'Card' or 'eToken'")).isIn(["Card", "Cash", "eToken"]);
        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});
        data.category_brand_product_id = data.category_brand_product_id;
        if (data.category_id != 1 && data.category_id != 3 && data.category_id != 2 && data.category_id != 4 && data.category_id != 5 && data.category_id != 16)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, currently only gas, Drinking water, Water tanker and Truck services are available")});

        ////////////        Cat Details         ///////////////////////
        var CateGoryProducts = Array();
        var JsonProducts = JSON.parse(data.products);
        var orderQuzntity = '0';
        for (let j = 0; j < JsonProducts.length; j++) {
            orderQuzntity = parseInt(orderQuzntity) + parseInt(JsonProducts[j].product_quantity);
            CateGoryProducts.push(JsonProducts[j].category_brand_product_id);
        }
        data.CateGoryProducts = CateGoryProducts;
        data.category = await Services.categoryBrandProductServices.CatDetailsAllLang(data);
        if (!data.category)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, Please check your parameters")});
        data.distance = parseFloat(data.category.maximum_distance);
        ////////////        Cat Details         ///////////////////////
        data.user_type_id = data.driver_user_type_id = 2;
        data.order_type = "Service";
        data.product_quantity = parseInt(orderQuzntity);
        data.seller_user_type_id = 2;
        ///////////////////////         Future Ride Check       ///////////////////////////////////////
        if (data.future == "1") {
            data.or_1hr = moment(data.now, "YYYY-MM-DD HH:mm:ss").add(55, "minutes");
            var isBefore = moment(data.order_timings, "YYYY-MM-DD HH:mm:ss").isBefore(data.or_1hr);
            if (isBefore && data.category_id == 2)
                return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Please select timing one hour after from now for scheduling a service")});

            var scheduled_check = await Db.orders.findOne({
                where: {
                    customer_user_id: data.user_id,
                    customer_user_detail_id: data.user_detail_id,
                    order_status: {$in: ["Scheduled", "DPending", "DApproved"]}
                }
            });
            //if(scheduled_check && scheduled_check.order_type == "Service")
            //return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, you already have scheduled a service"), scheduled_check });
            //  else if(scheduled_check && scheduled_check.order_type == "Support")
            if (scheduled_check && scheduled_check.order_type == "Support")
                return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, you already have scheduled a support"), scheduled_check});
        }
        ///////////////////////         Future Ride Check       ///////////////////////////////////////
        data.organisation_coupon_user_id = 0;
        // //////////////////////// Coupon Check    //////////////////////////////////
        if (data.coupon_user_id) {
            var coupon_users = await Services.couponUserServices.purchasedCouponDetail4Order(data);
            if (!coupon_users)
                return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this coupon is no longer valid")});
        } else {
            data.coupon_user_id = 0;
        }
        ////////////////////////    Coupon Check    //////////////////////////////////
        //Start 25-April-2019
        var cc = Array();
        var final_charge = '0.0';
        for (let i = 0; i < JsonProducts.length; i++) {
            final_charge = parseFloat(final_charge) + parseFloat(JsonProducts[i].price_per_item);

            data.product_actual_value = JsonProducts[i].price_per_item;
            data.product_alpha_charge = data.category.Brand.Product.alpha_price;
            data.product_per_quantity_charge = data.category.Brand.Product.price_per_quantity;
            //data.product_per_quantity_charge = final_charge;
            data.product_per_weight_charge = data.category.Brand.Product.price_per_weight;
            data.product_per_distance_charge = data.category.Brand.Product.price_per_distance;
            data.product_per_hr_charge = data.category.Brand.Product.price_per_hr;
            data.product_per_sq_mt_charge = data.category.Brand.Product.price_per_sq_mt;
            data.bottle_returned_value = data.bottle_returned_value ? data.bottle_returned_value : 0;

            cc.push(JsonProducts[i].category_brand_product_id);
        }
        console.log('finalcharge=======', final_charge);
        var CategoryProductsID = cc.toString();
        data.CategoryProductsID = CategoryProductsID;
        //End 25-April-2019
        if (data.category_id == 2) { ////////////       Drinking water Service  ///////////////
            data.day_string_check = Libs.commonFunctions.DayString(data.order_timings, data.timezone);
            data.drivers = await Services.driverListingServices.AllDrivers4OrdersDrinkingWater(data);
            data.coupon_user_id = 0;
        } else {////////////       Other Service   ///////////////////////
            if (data.category_id && (data.category_id == 4 || data.category_id == "4")) {
                data.drivers = await Services.driverListingServices.Driver4TruckOrders(data);
            } else {
                data.drivers = await Services.driverListingServices.Driver4OrdersExceptDrinkingWater(data);
            }
        }
        ///////////////////         No Driver Found SMS     ////////////////////////
        data.timings = moment(data.order_timings, "YYYY-MM-DD HH:mm:ss").tz("Asia/Muscat").format("YYYY-MM-DD hh:mm:ss");
        data.sms_message = "Alert:  New " + data.category.category_details[0].name + " Order, Product: " + data.category.Brand.Product.category_brand_product_details[0].name + " x " + data.product_quantity + ", Name: " + Details.user.name + ", Mobile: " + Details.user.phone_number + ", Order date: " + data.timings;
        if (data.drivers.length == 0 && process.env.NODE_ENV != "development") {
            // var sendSMS = Libs.commonFunctions.multipleSendSMS(data.sms_message+", Status: No Driver", Configs.appData.sms_numbers);
            //send data to database
            data.sms = [];
            //send data to database
            data.sms.message = "New " + data.category.category_details[0].name + " Order";
            data.sms.customer_id = Details.user_id;
            data.sms.products = data.products;
            data.sms.customer_id = data.user_id;
            data.sms.customer_name = data.user_name;
            data.sms.order_date = data.timings;
            data.sms.sms_object = data.sms_message;

            await Services.userServices.AddOrderFailure(data.sms);
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, no driver is currently available")});
        }
        ///////////////////         No Driver Found SMS     ////////////////////////
        data.order_token = "OR-" + Libs.commonFunctions.uniqueId();
        ///////////     Customer    /////////////////
        data.customer_user_id = Details.user_id;
        data.customer_user_detail_id = Details.user_detail_id;
        data.customer_organisation_id = Details.organisation_id;
        data.customer_user_type_id = Details.user_type_id;
        data.user_card_id = 0;
        ///////////     Customer    /////////////////
        data.order_status = "Searching",
                data.payment_status = "Pending",
                data.refund_status = "NoNeed",
                data.created_by = "User";
        data.order_distance = data.order_distance ? parseFloat(data.order_distance) : 0;
        data.order_time = 0;
        data.product_sq_mt = 0;
        data.product_weight = data.product_weight ? parseFloat(data.product_weight) : 0;
        data.buraq_percentage = parseFloat(data.category.buraq_percentage);
        //////////////////////      Buraq Percentage        ////////////////////////
        data.product_actual_value = data.category.Brand.Product.actual_value;
        data.product_alpha_charge = data.category.Brand.Product.alpha_price;
        data.product_per_quantity_charge = data.category.Brand.Product.price_per_quantity;
        //data.product_per_quantity_charge = final_charge;
        data.product_per_weight_charge = data.category.Brand.Product.price_per_weight;
        data.product_per_distance_charge = data.category.Brand.Product.price_per_distance;
        data.product_per_hr_charge = data.category.Brand.Product.price_per_hr;
        data.product_per_sq_mt_charge = data.category.Brand.Product.price_per_sq_mt;
        data.bottle_returned_value = data.bottle_returned_value ? data.bottle_returned_value : 0;


        data.bottle_charge = 0;
        //////////////////      Pricing Category Wise       ///////////////////////////////////////
        if (data.category_id == 1)
        {/////////////////////      Gas         ///////////////////////////////////////////////
            data.initial_charge = parseFloat(data.product_alpha_charge) + (parseFloat(data.product_per_quantity_charge) * parseFloat(data.product_quantity));
        }/////////////////////      Gas         ///////////////////////////////////////////////
        else
        {
            data.initial_charge = parseFloat(data.product_alpha_charge) + (parseFloat(data.product_per_quantity_charge) * parseFloat(data.product_quantity)) + (parseFloat(data.product_per_distance_charge) * parseFloat(data.order_distance)) + (parseFloat(data.product_per_hr_charge) * parseFloat(data.order_time)) + (parseFloat(data.product_per_sq_mt_charge) * parseFloat(data.product_sq_mt)) + (data.bottle_charge);
        }
        data.coupon_charge = 0;
        data.final_charge = data.initial_charge;
        data.admin_charge = parseFloat(((data.buraq_percentage * data.final_charge) / 100).toFixed(3));
        data.final_charge = parseFloat((data.final_charge + data.admin_charge).toFixed(3));
        var order = await Services.orderServices.latestCreateNewRow(data);
        data.order_id = order.order_id;
        //Add Products Start 25-April-2019
        var pp = Array();
        for (let i = 0; i < JsonProducts.length; i++)
        {
            var productss = Array();
            productss.order_id = data.order_id;
            productss.category_id = data.category_id;
            productss.category_brand_id = data.category_brand_id;
            productss.category_brand_product_id = JsonProducts[i].category_brand_product_id;
            productss.product_weight = JsonProducts[i].product_weight;
            productss.price_per_item = JsonProducts[i].price_per_item;
            productss.image_url = JsonProducts[i].image_url;
            productss.productName = JsonProducts[i].productName;
            productss.product_quantity = JsonProducts[i].product_quantity;
            productss.created_at = data.created_at;
            productss.updated_at = data.created_at;
            productss.app_language_id = data.app_language_id;
            await Services.orderServices.CreateProducts(productss);
        }
        //End 25-April-2019
        var order_images = [];
        var order_images_url = [];
        if (request.files && request.files.length > 0) {
            request.files.forEach(async (image) => {
                order_images.push({
                    order_id: data.order_id,
                    image: image.filename,
                    created_at: data.created_at,
                    updated_at: data.created_at
                });
                order_images_url.push({
                    order_id: data.order_id,
                    image: image.filename,
                    image_url: process.env.RESIZE_URL + image.filename
                });
            });
        }

        if (order_images.length > 0)
            await Db.order_images.bulkCreate(order_images);
        //////////////////      Drivers Push Plus Socket    ////////////////////////////
        if (order.fcm_socket_ids.length > 0) {
            var push_data = {
                order_id: (data.order_id).toString(),
                type: "OrderEvent",
                user_id: (data.customer_user_id).toString(),
                user_detail_id: (data.customer_user_detail_id).toString(),
                expiry: moment().utc().add(45, "seconds").format("YYYY-MM-DD HH:mm:ss")
            };
            var products = await Services.orderServices.AllOrderProducts(data);
            //console.log("products ==", products);
            var socket_data = {
                type: "SerRequest",
                order: {
                    order_id: data.order_id,
                    driver_user_detail_id: order.driver_user_detail_id,
                    order_timings: data.order_timings,
                    pickup_address: order.pickup_address,
                    pickup_latitude: parseFloat(order.pickup_latitude),
                    pickup_longitude: parseFloat(order.pickup_longitude),
                    dropoff_address: order.dropoff_address,
                    dropoff_latitude: parseFloat(order.dropoff_latitude),
                    dropoff_longitude: parseFloat(order.dropoff_longitude),
                    payment: order.payment,
                    organisation_coupon_user_id: data.organisation_coupon_user_id,
                    future: data.future,
                    category_id: data.category_id,
                    brand: {
                        category_id: data.category_id,
                        category_brand_id: data.category_brand_id,
                        category_brand_product_id: data.category_brand_product_id,
                    },
                    user: {
                        user_id: data.customer_user_id,
                        user_detail_id: data.customer_user_detail_id,
                        name: Details.user.name,
                        rating_count: Details.user.rating_count,
                        rating_avg: Details.user.ratings_avg,
                        profile_pic: Details.profile_pic,
                        profile_pic_url: Details.profile_pic_url,
                        phone_number: Details.user.phone_number,
                        phone_code: Details.user.phone_code
                    },
                    coupon_users: coupon_users ? coupon_users : null,
                    order_images_url,
                    details: order.details,
                    material_details: order.material_details,
                    products: products
                }
            };
            Libs.commonFunctions.pushPlusEventMultipleUsers(order.fcm_socket_ids, push_data, socket_data, order, data.category, "OrderEvent");
        }
        //////////////////      Drivers Push Plus Socket    ////////////////////////////
        //////////      No Response Timeout After 2 Minutes     ///////////
        if (data.future == "0")
        {
            data.timeOutMessage = response.trans("Sorry, no driver is currently available");
            setTimeout(function () {
                //data.order_request_status = "Searching";
                orderRequestedTimeout(data);// this code will only run when time has ellapsed
            }, 47 * 1000);
        }
        //////////      No Response Timeout After 2 Minutes     ///////////
        data.socket_ids = order.socket_ids;
        data.fcm_ids = order.fcm_ids;
        var order = await Services.orderServices.latestUserBookedOrderDetails(data);
        order = order[0];
        ///////////////////     Deduct Etokens, Coupon Users    ////////////////////////////////
        if (order.organisation_coupon_user)
        {
            order.organisation_coupon_user.quantity_left = order.organisation_coupon_user.quantity_left - data.product_quantity;
            Services.orgCouponUserServices.updateOrgCouponUser(order.organisation_coupon_user.organisation_coupon_user_id, order.organisation_coupon_user.quantity_left, data.created_at);
        }
        //}
        ///////////////////     Deduct Etokens, Coupon Users    ////////////////////////////////
        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Service requested successfully"), result: order, data});

    } catch (e)
    {
        console.log("e ===", e);
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

///////////////////////		Order Request Timeout 		/////////////////////////////////
async function orderRequestedTimeout(data)
{
    try {

        var order = await Services.orderServices.currentOrderTimeoutDetails(data.order_id);
        if (!order || order.order_status != "Searching")
            return {success: 1, statusCode: 200, msg: "No need for automatic timeout", order};

        order = JSON.parse(JSON.stringify(order));

        // if(process.env.NODE_ENV != "development")
        // 	var sendSMS = Libs.commonFunctions.multipleSendSMS(data.sms_message+", Status: Timeout", Configs.appData.sms_numbers);

        data.status = "SerTimeout";

        data.update_order = await Db.orders.update(
                {
                    order_status: data.status,
                    payment_status: data.status,
                    cancelled_by: data.status,
                    updated_at: data.created_at
                },
                {
                    where: {
                        order_id: data.order_id
                    }
                }
        );

        if (order.order_request_ids.length != 0)
        {/////////	Order Request Update 		///////////////////

            data.update_order_requests = Db.order_requests.update(
                    {
                        order_request_status: data.status,
                        updated_at: data.created_at
                    },
                    {
                        where: {
                            order_request_id: {$in: order.order_request_ids}
                        }
                    }
            );

        }/////////	Order Request Update 		///////////////////

        ///////////////////////////////////////////////////
        ////////////////////		Customer Push Socket

        ////////////////		Driver Push Notifications 	///////////////////////
        if (order.CRequest && order.CRequest.DriverDetails.fcm_id != "" && order.CRequest.DriverDetails.notifications == "1")
        {

            var message = Libs.commonFunctions.generate_lang_message(" has cancelled the service order", order.CRequest.DriverDetails.language_id);

            data.push_data = {
                order_id: (data.order_id).toString(),
                type: data.status, //SerTimeout
                message: data.timeOutMessage,
                future: order.future
            };

            var push = Libs.commonFunctions.latestSendPushNotification([order.CRequest.DriverDetails], data.push_data);

        }

        ////////////////		Driver Push Notifications 	///////////////////////


        ////////////////		Socket Event 	///////////////////////////
        var temp_order = await Services.orderServices.driverAcceptOrderDetails(data, order.CustomerDetails.language_id);

        data.socket_data = {
            type: data.status,
            order: temp_order//order_id
        };

        var event = Libs.commonFunctions.emitToDevice("OrderEvent", order.CustomerDetails.user_detail_id, data.socket_data);
        ////////////////		Socket Event 	///////////////////////////

        ////////////////////		Customer Push Socket
        ///////////////////////////////////////////////////
        return {success: 1, statusCode: 200, msg: "Service automatically rejected successfully", result: order};

    } catch (e)
    {
        return {success: 0, statusCode: 500, msg: e.message};
    }
}

////////////////////////		Cancel A Service 	/////////////////////////////////////
exports.userServiceCancel = async (request, response) => {
    try {

        var data = request.body;
        var Details = data.Details;
        delete data.Details;

        request.checkBody("order_id", response.trans("Order id is required")).notEmpty();
        request.checkBody("cancel_reason", response.trans("Cancel reason is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        ////////////////	Order Get And status check 	//////////////////////
        var order = await Services.orderServices.customerOrder4Cancellations(data);
        if (order.length == 0)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not available")});
        order = order[0];
        if (order.customer_user_detail_id != Details.user_detail_id)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order do not belong to you"), result: order});
        else if ((order.order_status == "CustCancel") || (order.order_status == "SerCancel") || (order.order_status == "SupCancel") || (order.order_status == "SerReject") || (order.order_status == "SerTimeout") || (order.order_status == "DSchCancelled") || (order.order_status == "DSchTimeout") || (order.order_status == "SysSchCancelled"))
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already cancelled"), result: order});
        else if (order.order_status == "SerComplete" || order.order_status == "SerCustConfirm")
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already completed"), result: order});
        // else if(order.order_status == 'SerComplete' && order.order_type == 'Service')
        // 	return response.status(400).json({ success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already completed"), result: order });
        // ////////////////	Order Get And status check 	//////////////////////

        data.order_request_status = "CustCancel";
        data.cancelled_by = "Cust";

        data.update_order = await Db.orders.update(
                {
                    order_status: data.order_request_status,
                    cancel_reason: data.cancel_reason,
                    cancelled_by: data.cancelled_by,
                    updated_at: data.created_at
                },
                {
                    where: {order_id: data.order_id}
                }
        );
        data.update_payment = await Db.payments.update(
                {
                    payment_status: data.order_request_status,
                    updated_at: data.created_at
                },
                {
                    where: {order_id: data.order_id}
                }
        );


        data.fcm_ids = [];
        data.socket_ids = [];
        data.order_request_ids = [];
        order.drivers.forEach(function (driver) {
            if (driver.fcm_id != "")
                data.fcm_ids.push(driver.fcm_id);
            data.socket_ids.push(driver.driver_user_detail_id);
            data.order_request_ids.push(driver.order_request_id);
        });

        ////////////////	Update DB 		///////////////////////////////

        if (data.order_request_ids.length != 0) {
            data.update_order_request = await Db.order_requests.update({
                order_request_status: data.order_request_status,
                updated_at: data.created_at
            },
                    {
                        where: {
                            order_request_id: {$in: data.order_request_ids}
                        }
                    });
        }
        ////////////////	Update DB 		///////////////////////////////

        ////////////////		Push Notifications 	///////////////////////
        // if(data.fcm_ids.length != 0)
        // 	{
        // 		var message = generate_lang_message(" has cancelled the service order", order.CRequest.DriverDetails.language_id);

        // 		data.push_data = {
        // 			order_id: (data.order_id).toString(),
        // 			type: data.order_request_status,//CustCancel
        // 			user_id: (order.customer_user_id).toString(), 
        // 			user_detail_id: (order.customer_user_detail_id).toString(),
        // 			message: Details.user.name+response.trans(" has cancelled the service order")
        // 		};
        // 		var push = Libs.commonFunctions.newSendPushNotifications(data.fcm_ids, data.push_data);
        // 	}

        if (order.CRequest && order.CRequest.DriverDetails.fcm_id != "" && order.CRequest.DriverDetails.notifications == "1") {

            var message = Libs.commonFunctions.generate_lang_message(" has cancelled the service order", order.CRequest.DriverDetails.language_id);

            data.push_data = {
                order_id: (data.order_id).toString(),
                type: data.order_request_status, //CustCancel
                message: Details.user.name + message
            };

            var push = Libs.commonFunctions.latestSendPushNotification([order.CRequest.DriverDetails], data.push_data);

        }
        ////////////////		Push Notifications 	///////////////////////

        ////////////////		Socket Event 	///////////////////////////
        var products = await Services.orderServices.AllOrderProducts(data);
        data.socket_data = {
            type: "SerCancel",
            order_id: order.order_id,
            order: {
                order_id: order.order_id,
                driver_user_detail_id: order.driver_user_detail_id,
                user: {
                    user_id: order.customer_user_id,
                    user_detail_id: order.customer_user_detail_id
                },
                products: products
            }
        };

        var event = Libs.commonFunctions.emitDynamicEvent("OrderEvent", data.socket_ids, data.socket_data);
        ////////////////		Socket Event 	///////////////////////////

        //////////////////	Coupons or Etokens 	////////////////////////////
        if (order.coupon_user) {
            await Db.coupon_users.update(
                    {
                        rides_left: order.coupon_user.rides_left + 1,
                        updated_at: data.created_at
                    },
                    {
                        where: {coupon_user_id: order.coupon_user_id}
                    }
            );

        }
        // else if(order.organisation_coupon_user)
        // 	{

        // 		await Db.organisation_coupon_users.update(
        // 					{
        // 						quantity_left: order.organisation_coupon_user.quantity_left+order.payment.product_quantity,
        // 						updated_at: data.created_at
        // 					},
        // 					{
        // 						where: {organisation_coupon_user_id: order.organisation_coupon_user_id}
        // 					}
        // 		);

        // 	}
        //////////////////	Coupons or Etokens 	////////////////////////////

        var order = await Services.orderServices.latestUserBookedOrderDetails(data);

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Order Cancelled successfully"), result: order[0]});

    } catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

///////////////////////		Service Ongoings 	/////////////////////////////////////////////
exports.userServiceOngoing = async (request, response) => {
    try {

        var data = request.body;
        // console.log("hjhhjhjhjf",data);
        var currentOrders = await Services.orderServices.customerCurrentOrders(data);

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Ongoing orders"), result: currentOrders});

    } catch (e)
    {
        //console.log("err ==",e);
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

///////////////////////		User Service Rate 		//////////////////////////////////////////
exports.userServiceRate = async (request, response) => {
    try {

        var data = request.body;
        var Details = data.Details;
        delete data.Details;

        request.checkBody("order_id", response.trans("Order id is required")).notEmpty();
        request.checkBody("ratings", response.trans("Ratings is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});
        // console.log('Rate Service =',data);
        ////////////////	Order Get And status check 	//////////////////////
        var order = await Services.orderServices.customerOrder4Push(data);
        if (!order)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order is currently not available")});
        else if (order.customer_user_detail_id != Details.user_detail_id)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order do not belong to you"), result: order});
        else if ((order.order_status == "CustCancel") || (order.order_status == "SerCancel") || (order.order_status == "SupCancel") || (order.order_status == "SerReject") || (order.order_status == "SerTimeout") || (order.order_status == "DSchCancelled") || (order.order_status == "DSchTimeout") || (order.order_status == "SysSchCancelled"))
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order is already cancelled"), result: order});
        // else if(order.order_status != 'SerComplete' && order.order_type == 'Service')
        else if (order.order_type == "Service" && !((order.order_status == "SerCustConfirm") || (order.order_status == "SerComplete")))
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order is not completed yet"), result: order});
        else if (order.order_status != "SupComplete" && order.order_type == "Support")
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this order is not completed yet"), result: order});
        else if (order.CustRatings)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, you have already rated this order"), result: order});
        ////////////////	Order Get And status check 	//////////////////////

        order.CustRatings = await Db.order_ratings.create({
            order_id: data.order_id,
            customer_user_id: order.customer_user_id,
            customer_user_detail_id: order.customer_user_detail_id,
            customer_organisation_id: order.customer_organisation_id,
            driver_user_id: order.driver_user_id,
            driver_user_detail_id: order.driver_user_detail_id,
            driver_organisation_id: order.driver_organisation_id,
            ratings: data.ratings,
            comments: data.comments ? data.comments : "",
            created_by: "Customer",
            deleted: "0",
            blocked: "0",
            created_at: data.created_at,
            updated_at: data.created_at
        });
        data.order_rating_id = order.CustRatings.order_rating_id;

        data.type = "CustRatedService";
        data.message = Details.user.name + " has rated your service";

        data.driver_notification = await Db.user_detail_notifications.create({
            user_id: order.driver_user_id,
            user_detail_id: order.driver_user_detail_id,
            user_type_id: order.driver_user_type_id,
            user_organisation_id: order.driver_organisation_id,
            sender_user_id: order.customer_user_id,
            sender_user_detail_id: order.customer_user_detail_id,
            sender_type_id: order.customer_user_type_id,
            sender_organisation_id: order.customer_organisation_id,
            order_id: order.order_id,
            order_request_id: order.CRequest.order_request_id,
            order_rating_id: order.CustRatings.order_rating_id,
            message: data.message,
            type: data.type,
            is_read: "0",
            deleted: "0",
            created_at: data.created_at,
            updated_at: data.created_at
        });

        ////////////////		Push Notifications 	///////////////////////
        if (order.DriverDetails.fcm_id != "" && order.DriverDetails.notifications == "1")
        {
            var message = Libs.commonFunctions.generate_lang_message(" has rated your service", order.DriverDetails.language_id);

            data.push_data = {
                order_id: (data.order_id).toString(),
                type: data.type, //CustRatedService
                user_id: (order.customer_user_id).toString(),
                user_detail_id: (order.customer_user_detail_id).toString(),
                message: Details.user.name + message,
                future: order.future
            };

            var push = Libs.commonFunctions.latestSendPushNotification([order.DriverDetails], data.push_data);
        }
        ////////////////		Push Notifications 	///////////////////////

        var products = await Services.orderServices.AllOrderProducts(data);
        ////////////////		Socket Event 	///////////////////////////
        data.socket_data = {
            type: data.type,
            message: data.message,
            order: {
                order_id: order.order_id,
                driver_user_detail_id: order.driver_user_detail_id,
                user: {
                    user_id: order.customer_user_id,
                    user_detail_id: order.customer_user_detail_id
                },
                products: products
            }
        };
        var event = await Libs.commonFunctions.emitDynamicEvent("OrderEvent", [order.DriverDetails.driver_user_detail_id], data.socket_data);
        //io.sockets.emit('CustRatedService', data.socket_data );
        ////////////////		Socket Event 	///////////////////////////

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Order rated successfully")});

    } catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

////////////////////////		User Etokens 	//////////////////////////////////////////
exports.userServiceETokens = async (request, response) => {
    try {

        var data = request.body;

        //console.log("dataaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", data);

        request.checkBody("category_id", response.trans("Service type is required")).notEmpty();

        request.checkBody("latitude", response.trans("Latitude is required")).notEmpty();
        request.checkBody("longitude", response.trans("Longitude is required")).notEmpty();
        request.checkBody("distance", response.trans("Distance is required")).notEmpty();
        request.checkBody("order_timings", response.trans("Order timings is required")).notEmpty();

        request.checkBody("take", response.trans("Take is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        data.day_string_check = Libs.commonFunctions.DayString(data.order_timings, data.timezone);

        data.skip = 0;

        var history = await Services.eTokensServices.userETokenHistory(data);

        //var brands = await Services.eTokensServices.OffersListings(data);
        var brands = await Services.eTokensServices.filterBrandEtokens(data);

        var result = {
            //data,
            history,
            brands
        };

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Token details"), result});

    } catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

///////////////////////		Etokens 	Paginate 		////////////////////////////////////
exports.userETokensPaginate = async (request, response) => {
    try {

        var data = request.body;

        request.checkBody("category_id", response.trans("Service type is required")).notEmpty();
        request.checkBody("category_brand_id", response.trans("Category brand id is required")).notEmpty();
        request.checkBody("latitude", response.trans("Latitude is required")).notEmpty();
        request.checkBody("longitude", response.trans("Longitude is required")).notEmpty();
        request.checkBody("distance", response.trans("Distance is required")).notEmpty();
        request.checkBody("skip", response.trans("Skip is required")).notEmpty();
        request.checkBody("take", response.trans("Take is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        var etokens = await Services.eTokensServices.PaginateETokens(data);

        var result = {
            etokens
        };

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Tokens pagination"), result});

    } catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};


///////////////////////////		Buy Tokens 	//////////////////////////////////////////
exports.userETokenBuy = async (request, response) => {
    try {

        var data = request.body;

        //		console.log("dataaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", data);

        request.checkBody("organisation_coupon_id", response.trans("Organisation coupon id is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        data.category_id = 3;

        var eToken = await Services.eTokensServices.eTokenDetails(data);
        if (!eToken || (!eToken.Org && !eToken.user_detail))
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, this eToken is no longer available"), eToken});

        if (eToken.Org)
            data.buraq_percentage = eToken.Org.OrgCat.buraq_percentage;
        else
            data.buraq_percentage = eToken.user_detail.UCat.buraq_percentage;

        var user_card = await Db.user_cards.findOne({where: {user_id: data.user_id}});
        if (!user_card)
            return response.status(400).json({success: 0, statusCode: 400, msg: response.trans("Sorry, no card is added for this user")});

        data.user_card_id = user_card.user_card_id;

        data.order_id = 0;
        data.coupon_id = 0;
        data.seller_user_id = eToken.user_id;
        data.seller_user_detail_id = eToken.user_detail_id;
        data.seller_user_type_id = eToken.user_type_id;
        data.seller_organisation_id = eToken.organisation_id;
        data.payment_status = "Completed";
        data.initial_charge = data.final_charge = data.product_actual_value = eToken.price;
        data.bank_charge = 0;
        data.admin_charge = ((data.initial_charge / 100) * data.buraq_percentage).toFixed(3);
        //( (data.initial_charge) - ((data.initial_charge/100)*data.buraq_percentage) )//.toFixed(2);

        data.metadata = {
            organisation_coupon_id: data.organisation_coupon_id,
            organisation_id: eToken.organisation_id
        };

        data.message = "Charge for EToken " + data.organisation_coupon_id;

        data.payment = await Libs.stripeFunctions.CouponPayment(data);///////	Stripe Payment
        data.transaction_id = data.payment.id;

        data.payment = await Services.paymentServices.CreateNewRow(data);
        data.payment_id = data.payment.payment_id;

        data.quantity = eToken.quantity;
        data.category_brand_product_id = eToken.category_brand_product_id;
        data.category_id = eToken.category_id;
        data.category_brand_id = eToken.category_brand_id;

        data.organisation_coupon_user = await Services.orgCouponUserServices.CreateNewRow(data);
        data.organisation_coupon_user_id = data.organisation_coupon_user.organisation_coupon_user_id;

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("eToken purchased successfully"), result: data.organisation_coupon_user_id});

    } catch (e)
    {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};


exports.addAddress = async (request, response) => {
    try {
        var data = request.body;
        data.user_id = data.user_id;
        data.title = data.title;
        data.flat_no = data.flat_no;
        data.floor_no = data.floor_no;
        data.building_name = data.building_name;
        data.address = data.address;
        data.address_latitude = data.address_latitude;
        data.address_longitude = data.address_longitude;

        request.checkBody("title", response.trans("Title is required")).notEmpty();
        request.checkBody("flat_no", response.trans("Flat number is required")).notEmpty();
        request.checkBody("user_id", response.trans("User is required")).notEmpty();
        request.checkBody("address", response.trans("Address id is required")).notEmpty();
        request.checkBody("address_latitude", response.trans("Address Lat is required")).notEmpty();
        request.checkBody("address_longitude", response.trans("Address Lng is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        var result = await Services.userServices.addNewAddress(data);

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Address added successfully"), result});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

exports.getAddress = async (request, response) => {
    try {
        var data = request.body;
        data.user_id = data.user_id;

        request.checkBody("user_id", response.trans("User is required")).notEmpty();
        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        var addresses = await Services.userServices.GetAllUserAddress(data);

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Address received successfully"), addresses});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};



exports.deleteAddress = async (request, response) => {
    try {
        var data = request.body;
        data.address_id = data.address_id;

        request.checkBody("address_id", response.trans("Address id is required")).notEmpty();
        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        var delete_address = await Services.userServices.deleteUserAddressById(data);

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Address deleted successfully"), delete_address});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};



exports.editAddress = async (request, response) => {
    try {

        var data = request.body;
        data.address_id = data.address_id;
        data.title = data.title;
        data.flat_no = data.flat_no;
        data.floor_no = data.floor_no;
        data.building_name = data.building_name;
        data.address = data.address;
        data.address_latitude = data.address_latitude;
        data.address_longitude = data.address_longitude;

        request.checkBody("address_id", response.trans("Address id is required")).notEmpty();
        request.checkBody("title", response.trans("Title is required")).notEmpty();
        request.checkBody("flat_no", response.trans("Flat number is required")).notEmpty();
        request.checkBody("address", response.trans("Address is required")).notEmpty();
        request.checkBody("address_latitude", response.trans("Address Lat is required")).notEmpty();
        request.checkBody("address_longitude", response.trans("Address Lng is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        var updated_address = await Services.userServices.updateUserAddress(data);

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Address updated successfully"), updated_address});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};

exports.SendMultipleNotificationtousers = async (request, response) => {
    console.log('request', request);
    var req = request.body;
    var data = Array();
    data.cust_push_data = {
        type: "BulkNotification",
        message: req.message,
        future: "0",
        title: req.title,
        couponId: req.couponId,
        serviceId: req.serviceId,
        image: req.image,
        mediaType: "Image"
    };
    var users = await Db.user_details.findAll({where: {blocked: '0', user_type_id: req.type}});
    var push = Libs.commonFunctions.SendMultipleNotificationtousers(users, data.cust_push_data);
    return response.status(200).json({success: 0, statusCode: 500});
};

exports.updatePromotionNotification = async (request, response) => {
    try {

        var data = request.body;
        console.log(data);
        data.status = data.status;

        request.checkBody("status", response.trans("Status is required")).notEmpty();

        var errors = request.validationErrors();
        if (errors)
            return response.status(400).json({success: 0, statusCode: 400, msg: errors[0].msg});

        var updated_status = await Services.userServices.updateUserPromotionNotification(data);

        return response.status(200).json({success: 1, statusCode: 200, msg: response.trans("Updated successfully")});
    } catch (e) {
        return response.status(500).json({success: 0, statusCode: 500, msg: e.message});
    }
};


