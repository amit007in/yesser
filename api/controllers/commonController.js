var moment = require("moment"); //Add M2 Part2
var Libs = require(appRoot+"/libs");//////////////////		Libraries
var Services = require(appRoot+"/services");///////////		Services
var Db = require(appRoot+"/models");//////////////////		Db Models
var Configs = require(appRoot+"/configs");/////////////		Configs


///////////////////////		Org Listings 	//////////////////////////////////////////
exports.orgListings = async (request, response) => {
	try{
		var data = request.body;

		var orgs = await Services.orgServices.appGetOrgs(data);

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Organisation listings"), result: orgs });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};


///////////////////////		Emergency contacts 	//////////////////////////////////////////
exports.eContact = async (request, response) => {
	try{
		var data = request.body;

		var eContacts = await Services.eContactServices.appGetEContacts(data);

		// var result = {
		// 	eContacts: eContacts,
		// 	//data
		// }

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Emergency contacts"), result: eContacts });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

///////////////////////		Update App Data 		///////////////////////////////////////
exports.updateData = async (request, response) => {
	try{

		var data = request.body;
		var Details = data.Details;
		delete data.Details;
		
		data.timezonez = Libs.commonFunctions.changeTimezoneFormat(data.timezone ? data.timezone : Details.timezone);
		//Add M2 Part2
		var user = await Db.user_logins.findOne({
			where: {
				user_id: data.user_id,
				user_type_id: data.user_type_id,
				login_at: moment().format("YYYY-MM-DD")
			}
		});
		if(!user)
		{
			data.currentDate  = moment().format("YYYY-MM-DD");
			console.log("New login today");
			var userDetailLogin = await Services.userDetailServices.createNewUserLogin(data, Details);
		}
		
		var userDetail = await Services.userDetailServices.updateRow(data, Details);

		//var AppDetail = await Services.userDetailServices.UserGetAppDetails(data);
		var services = await Services.categoryServices.servicesWithBrands(data);
		var supports = await Services.categoryServices.supportsListings(data);

		if(Details.user_type_id == 1)
		{
			var AppDetail = await Services.userDetailServices.UserGetAppDetails(data);
				
			var currentOrders = await Services.orderServices.customerCurrentOrders(data);
		}
		else if(Details.user_detail_id == 2)
		{//////////
			var AppDetail = await Services.userDetailServices.ServiceDriverLoginDetails(data);

			var currentOrders = await Services.orderServices.driverCurrentOrders(data);
		}//////////
		else
		{
			var AppDetail = await Services.userDetailServices.UserGetAppDetails(data);
			var currentOrders = await Services.orderServices.driverCurrentOrders(data);
		}

		AppDetail = JSON.parse(JSON.stringify(AppDetail));
		AppDetail.profile_pic_url = (AppDetail.profile_pic  == "") ? "" : process.env.RESIZE_URL+AppDetail.profile_pic;

		//////////////		Ratings 	///////////////////////////
		var ratings = await Services.userDetailServices.userRatings(AppDetail.user_type_id ,AppDetail.user_detail_id);

		AppDetail.rating_count = parseInt(ratings[0].rating_count);
		AppDetail.ratings_avg = parseInt(ratings[0].ratings_avg);
		//////////////		Ratings 	///////////////////////////

		var result = {
			AppDetail,
			currentOrders,
			services,
			supports,
			Versioning: Configs.appData.Versioning
		};

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Details updated successfully"), result, ratings });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

////////////////////		Search Organisations 		//////////////////////////////////
exports.orgSearch = async (request, response) => {
	try{

		var data = request.body;

		request.checkBody("licence_number", response.trans("Licence number is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var org = await Db.organisations.findOne({
			where: {
				licence_number: data.licence_number
			}
		});

		var result = {
			organisation_id: org ? org.organisation_id : 0
		};

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Organisation searched successfully"), result });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};
///////////////////		User Update Online Offline 		//////////////////////////////////
exports.updateOnline = async (request, response) => {
	try{
		var data = request.body;
		var Details = data.Details;
		delete data.Details;
		
		request.checkBody("online_status", response.trans("Online status is required")).notEmpty();
			
		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		//Add M2 Part2
		var user = await Db.user_logins.findOne({
			where: {
				user_id: data.user_id,
				user_type_id: data.user_type_id,
				login_at: moment().format("YYYY-MM-DD")
			}
		});
		if(!user)
		{
			data.currentDate  = moment().format("YYYY-MM-DD");
			console.log("New login today");
			var userDetailLogin = await Services.userDetailServices.createNewUserLogin(data, Details);
		}
		
		var update = await Db.user_details.update(
			{
				online_status: data.online_status,
				updated_at: data.created_at
			},
			{
				where: { user_detail_id: data.user_detail_id }
			}
		);

		// var result = {
		// 	//data
		// };

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Status updated successfully"), online_status: data.online_status });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

///////////////////////			Single Category Brand Products 	/////////////////////////////
exports.brandProducts = async (request, response) => {
	try{
		var data = request.body;
		var Details = data.Details;
		delete data.Details;

		request.checkBody("category_brand_id", response.trans("Service brand is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var products = await Services.categoryBrandProductServices.ServiceBrandProductsWithDetails(data);

		// var result = {
		// 	products
		// };

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Brand all products"), result: products });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

///////////////////////		Service Brands With Products 		///////////////////////////
exports.serviceBrands = async (request, response) => {
	try{

		var data = request.body;
		var Details = data.Details;
		delete data.Details;

		request.checkBody("category_id", response.trans("Category id is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var brands = await Services.categoryServices.singleServiceCatDetails(data);

		// var result = {
		// 	brands
		// };

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Service all brands"), result: brands });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

///////////////////////			Contact Us 		//////////////////////////////////////////////
exports.contactus = async (request, response) => {
	try{

		var data = request.body;
		var Details = data.Details;
		delete data.Details;

		request.checkBody("message", response.trans("Message is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		var contact = await Db.admin_contactus.create({
			user_id: data.user_id,
			user_detail_id: data.user_detail_id,
			user_type_id: data.user_type_id,
			parent_admin_contactus_id: 0,
			message: data.message,
			is_read: "0",			
			//admin_reply: "",
			created_at: data.created_at,
			updated_at: data.created_at
		});

		// var result = {
		// 	contact
		// };

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Thankyou for contact us, admin will reach you soon") });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

//////////////////////////		Logout User 	///////////////////////////////////////////
exports.logout = async (request, response) => {
	try{

		var data = request.body;
		var Details = data.Details;

		var update_user_detail = await Db.user_details.update(
			{
				online_status: "0",
				access_token: "",
				otp: "",
				fcm_id: "",
				socket_id: "",
				updated_at: data.created_at
			},
			{
				where: { user_detail_id: Details.user_detail_id }
			}
		);

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Logged out successfully") });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

//////////////////////////		Add Track Image ///////////////////////////////////////////
exports.addTrackImage = async (request, response) => {
	try{

		var data = request.body;

		request.checkBody("order_id", response.trans("Order id is required")).notEmpty();
		request.checkBody("track_path", response.trans("Track path is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		data.track_image = (request.file) ? request.file.filename : "";

		var update_order = await Db.orders.update(
			{
				track_path: data.track_path,
				track_image: data.track_image,
				updated_at: data.created_at
			},
			{
				where: { order_id: data.order_id }
			}
		);

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Order updated successfully"), data });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

//////////////////////		Service Listings 	//////////////////////////////////////////////
exports.supportsList = async (request, response) => {
	try{

		var data = request.body;
		delete data.Details;

		var supports = await Services.categoryServices.supportsListings(data);

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Support listings"), result: supports });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

///////////////////////////////		Setting Update 	////////////////////////////////////////
exports.settingUpdate = async (request, response) => {
	try{

		var data = request.body;

		var update_data = {
			language_id: data.app_language_id,
			updated_at: data.created_at
		};

		if(data.notifications)
			update_data.notifications = data.notifications;

		var update_user = await Db.user_details.update(
			update_data,
			{
				where: { user_id: data.user_id }
			}
		);

		var AppDetail = await Services.userDetailServices.UserGetAppDetails(data);

		var AppDetail = JSON.parse(JSON.stringify(AppDetail));
		if(AppDetail.profile_pic != "")
			AppDetail.profile_pic_url = process.env.RESIZE_URL+AppDetail.profile_pic;
		else
			AppDetail.profile_pic_url = "";

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("Setting updated successfully"), result: AppDetail });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

//////////////////		Test Push 		///////////////////////////////////
exports.testPush = async (request, response) => {
	try{

		var data = request.body;

		request.checkBody("socket_id", response.trans("socket_id is required")).notEmpty();

		var errors = request.validationErrors();
		if (errors)
			return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});


		//io.sockets.to(data.socket_id).emit('OrderEvent', {data: 'data'});

		//io.sockets.sockets[data.socket_id].disconnect();


		if(io.sockets.sockets[data.socket_id]!=undefined){
		  //console.log('TRUEEEEEEEEEEEEEEEEEEE'io.sockets.sockets[data.socket_id]); 
		  var temp1 = true;

		}else{

			var temp1 = false;
		  //console.log("Socket not connected");
		}

		//data.temp = JSON.stringify(data.temp);


		return response.status(400).json({ success: 0, statusCode: 200, data, temp1});


		// request.checkBody("fcm_id", response.trans("fcm_id is required")).notEmpty();
		// request.checkBody("device_type", response.trans("device_type is required")).notEmpty();
		// request.checkBody("silent", response.trans("silent is required")).notEmpty();
		// request.checkBody("order_id", response.trans("order_id is required")).notEmpty();
		// request.checkBody("type", response.trans("type is required")).notEmpty();

		// var errors = request.validationErrors();
		// 	if (errors)
		// 		return response.status(400).json({ success: 0, statusCode: 400, msg: errors[0].msg});

		// var pushData = {
		// 	message: data.message ? data.message : 'Test',
		// 	type: data.type,
		// 	order_id: (data.order_id).toString()
		// }

		// if(data.silent == '1')
		// 	{
		// 		var options = {
		// 			"content_available": true,
		// 			"collapseKey": pushData.order_id
		// 		}

		// 	}
		// else
		// 	{
		// 		var options = {
		// 			"content_available": false,
		// 			"collapseKey": pushData.order_id
		// 		}
		// 	}

		// if(data.device_type == 'Ios')
		// 		{

		// 			pushData.title = process.env.APP_NAME;
		// 			pushData.body = pushData.message;

		// 			var payload = {
		// 				data: {
		// 					order_id: pushData.order_id,
		// 					type: pushData.type
		// 				}
		// 				//notification: pushData
		// 			};					

		// 		}
		// 	else
		// 		{

		// 			var payload = {
		// 				data: pushData
		// 			};

		// 		}

		// 	var admin = require("firebase-admin");
		
		// 	admin.messaging().sendToDevice(data.fcm_id, payload, options)
		// 		.then(function(response) {
		// 				console.log("Push Messageeeeeeeeeeeeeeeeeeeeeeeeeeeeeee -",data, payload, options);
		// 		})
		// 		.catch(function(error) {
		// 			console.log('Error sending FCM push:', data, payload, error.message, options);
		// 		});


		// 	return response.status(200).json({ success: 1, statusCode: 200, msg: 'Push sent successfully' });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message, data});
	}
};

