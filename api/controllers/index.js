module.exports ={
	indexController: require(appRoot+"/controllers/indexController"),
	commonController: require(appRoot+"/controllers/commonController"),

	///////		Services 	///////////////
	serviceController: require(appRoot+"/controllers/Services/serviceController"),
	serviceOrderController: require(appRoot+"/controllers/Services/serviceOrderController"),
	serviceOtherController: require(appRoot+"/controllers/Services/serviceOtherController"),
	serviceWaterController: require(appRoot+"/controllers/Services/serviceWaterController"),
	///////		Services 	///////////////

	///////		Support 	///////////////
	supportController: require(appRoot+"/controllers/Supports/supportController"),
	///////		Support 	///////////////

	///////		Users 	///////////////////
	userController: require(appRoot+"/controllers/Users/userController"),
	userServiceController: require(appRoot+"/controllers/Users/userServiceController"),
	userOtherController: require(appRoot+"/controllers/Users/userOtherController"),
	userWaterController: require(appRoot+"/controllers/Users/userWaterController"),
	///////		Users 	///////////////////

};