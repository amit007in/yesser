var Languages = require(appRoot+"/models/").languages;

var appOtherData = require(appRoot+"/configs/appData");

///////////////		App Data 	//////////////////////
exports.appdata = async (request, response) => {
	try{

		///////////////		Languages 	//////////////////////
		var languages = await Languages.findAll({
			where: {"blocked": "0"},
			attributes: ["language_id", "language_name", "language_code", "is_default"],
			order: [ ["sort_order", "ASC"] ]
		});
		///////////////		Languages 	//////////////////////

		var result = {
			Versioning: appOtherData.Versioning,
			languages
		};

		return response.status(200).json({ success: 1, statusCode: 200, msg: response.trans("App data"), result });

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};