var randomstring = require("randomstring");
var uniqid = require("uniqid");
var moment = require("moment-timezone");
var req = require("request");
var admin = require("firebase-admin");

admin.initializeApp({
	credential: admin.credential.cert({
	  	projectId: process.env.FCM_PROJECT_ID,
	  	clientEmail: process.env.FCM_CLIENT_EMAIL,
	  	privateKey: process.env.FCM_PRIVATE_KEY
	})
});
var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;
var async = require('async');
var Db = require(appRoot + "/models");//////////////////		Db Models
/////////////////////		Multiple SMS 		////////////////////////////////
exports.multipleSendSMS = async (message, phone_numbers) =>
{

	var options = { 
		method: "POST",
		  	url: "https://ismartsms.net/RestApi/api/SMS/PostSMS",
		  	headers: { "Content-Type": "application/x-www-form-urlencoded" },
		  	form: 
		   		{
		   			UserID: process.env.SMS_USER_ID,
		     		Password: process.env.SMS_PASSWORD,
		     		Message: message,
		     		Language: "0",
		     		MobileNo: phone_numbers,
		     		ScheddateTime:  moment().tz("Asia/Muscat").add(5, "seconds").format("MM/DD/YYYY hh:mm:ss")
		     	} 
		     };

	req(options, function (error, res, body) {
		  if (error) //throw new Error(error);
		  	console.log("Error sending SMS", error, phone_numbers);
		  else
		  	console.log("Success sending SMS", body, phone_numbers);

	});


};


/////////////////////		Send SMS 		////////////////////////////////////
exports.sendSMS = async (message, numb) => {

	//numb = numb.replace('+', '');

	var options = { 
		method: "POST",
		  	url: "https://ismartsms.net/RestApi/api/SMS/PostSMS",
		  	headers: { "Content-Type": "application/x-www-form-urlencoded" },
		  	form: 
		   		{
		   			UserID: process.env.SMS_USER_ID,
		     		Password: process.env.SMS_PASSWORD,
		     		Message: message,
		     		Language: "0",
		     		MobileNo: ["968"+numb],
		     		ScheddateTime:  moment().tz("Asia/Muscat").add(5, "seconds").format("MM/DD/YYYY hh:mm:ss")
		     		// "09/07/2018 12:30:00"//MM/DD/YYYY hh:mm:ss
		     	} 
		     };

	req(options, function (error, res, body) {
		  if (error) //throw new Error(error);
		  	console.log("Error sending SMS", error, "968"+numb);
		  else
		  	console.log("Success sending SMS", body, "968"+numb);

	});

};

/////////////////////		Generate OTP 	/////////////////////////////////////
exports.generateOTP = (length) => {

	return randomstring.generate({
	  	length: length,
	  	charset: "numeric"
	});

};

/////////////////////		Generate OTP 	/////////////////////////////////////
exports.generateAccessToken = (length) => {

	return randomstring.generate({
	  	length: length,
	  	charset: "alphabetic"
	});

};

/////////////////////		Buraq generate Email 		/////////////////////////
exports.generateEmail = () => {
	return uniqid()+process.env.MAIL_ENDING;
};

//////////////////////		Change Timezone String Format 	/////////////////////
exports.changeTimezoneFormat = (timez) => {
	return moment.tz(timez).format("Z");
};

/////////////////////		Current UTC 		/////////////////////////////////
exports.currentUTC = () => {
	return moment.utc().format("YYYY-MM-DD HH:mm:ss");
};

/////////////////////		Generate Unique Id 	///////////////////////////////////
exports.uniqueId = () => {
	return uniqid();
};

/////////////////////		Emit Data To Devices 	///////////////////////////
exports.emitDynamicEvent = async (event_name ,socket_ids, data) => {
//console.log('Socketttzzzzzzzzzzzzzzzzzzzztttttttttttttttsssssssssssssssssssss', event_name, socket_ids);
	socket_ids.forEach(function(user_detail_id) {
		try{
			io.sockets.in("App"+user_detail_id).emit(event_name, data);
			//console.log('Emitted Socket Event - ',event_name, 'App'+user_detail_id);
			//console.log('Emitted Socket Event data- ',data);
		}
		catch(e)
		{
			console.log('Error Emiting Socket Event -', event_name, e.message, data);
		}
	});

};

////////////////////	Emot to Single Device 	////////////////////////////////
exports.emitToDevice = async (event_name, user_detail_id, data) => {
	try{
		//console.log('Single Emitted Socket Event - ',event_name);
		//console.log('Single Emitted Socket Event data - ',data);
		io.sockets.in("App"+user_detail_id).emit(event_name, data);
	}
	catch(e)
	{
		console.log('Error Single Emiting Socket Event -', event_name, e.message, data);
	}
};

exports.generate_lang_message = (message, language_id) => {

	if(language_id == 1)
		var locale = "1En";
	else if(language_id == 2)
		var locale = "2Hi";
	else if(language_id == 3)
		var locale = "3Ur";
	else if(language_id == 4)
		var locale = "4Ch";
	else
		var locale = "5Ar";

	return trans({phrase: message, locale});///////////	Translate

};

//////////////////		Push Plus Socket 	/////////////////////////////////////////////////
exports.pushPlusEventMultipleUsers = async (fcm_socket_ids, push_data, socket_data, order, category, event_name) => {

	push_data.sound = "custom_tone.wav";
	let finalOutput = {};
	try {
		 new Promise(async (resolve, reject)=> {
			await async.each(fcm_socket_ids, async function(fcm_socket_id) {
				if(fcm_socket_id.fcm_id != "" && fcm_socket_id.notifications == "1")
					{
						push_data.message = fcm_socket_id.message;
						let payload = {
							notification: {
								title: process.env.APP_NAME,
								body: fcm_socket_id.message,
								push_type: push_data.type,
								sound: "custom_tone.wav",
								tag: "null"
							},
							data: push_data
						};

						admin.messaging().sendToDevice(fcm_socket_id.fcm_id, payload);
					}

				order.payment.buraq_percentage = fcm_socket_id.buraq_percentage;
				order.payment.bottle_charge = fcm_socket_id.bottle_charge;

				socket_data.order.brand.category_name = category.category_details[fcm_socket_id.language_id -1].name;
				socket_data.message = fcm_socket_id.message;
//console.log("order.category_id",order.category_id);
				if(order.category_id  == 1 || order.category_id == 3)
				{
					
					//console.log('updatedProductCollection2: ', fcm_socket_id.driver_user_detail_id);
					socket_data.order.brand.brand_name = category.category_details[fcm_socket_id.language_id -1].name;
					socket_data.order.brand.name = category.Brand.Product.category_brand_product_details[fcm_socket_id.language_id - 1].name;
				}
				else if(order.category_id < 6 || order.category_id == 16) //Add M3 Part 3.2
				{
					//console.log('updatedProductCollection2: ', fcm_socket_id.driver_user_detail_id);
					socket_data.order.brand.brand_name = category.Brand.category_brand_details[fcm_socket_id.language_id - 1].name;
					socket_data.order.brand.name = category.Brand.Product.category_brand_product_details[fcm_socket_id.language_id - 1].name;
				}
				else
				{
					//console.log('updatedProductCollection3: ', fcm_socket_id.driver_user_detail_id);
					socket_data.order.brand.brand_name = "";
					socket_data.order.brand.name = "";
				}
				let tramsproducts = socket_data.order.products;
				//console.log('updatedProductCollection: ', tramsproducts);
				let updatedProductCollection = await getProductName(tramsproducts, fcm_socket_id.language_id);
				//console.log('updatedProductCollection: ', updatedProductCollection);
				socket_data.order.products = updatedProductCollection;
				finalOutput = socket_data;
				//console.log('finalOutput: ', JSON.parse(JSON.stringify(finalOutput.order.products)));
				io.sockets.in("App"+fcm_socket_id.driver_user_detail_id).emit(event_name, finalOutput);
			}, function(err){
				if(err) {
					reject(err);
				} else {
					resolve(finalOutput);
				}

			});

		});

	} catch(err) {
		console.log(err);
	}

	/*fcm_socket_ids.forEach(function(fcm_socket_id) {
		try{

			if(fcm_socket_id.fcm_id != "" && fcm_socket_id.notifications == "1")
			{
				let payload = {
					notification: {
						title: process.env.APP_NAME,
						body: fcm_socket_id.message,
						push_type: push_data.type,
						sound: "custom_tone.wav",
						tag: "null"
					},
					data: push_data
				};

				admin.messaging().sendToDevice(fcm_socket_id.fcm_id, payload);
			}

			order.payment.buraq_percentage = fcm_socket_id.buraq_percentage;
			order.payment.bottle_charge = fcm_socket_id.bottle_charge;

			socket_data.order.brand.category_name = category.category_details[fcm_socket_id.language_id -1].name;
			socket_data.message = fcm_socket_id.message;

			if(order.category_id  == 1 || order.category_id == 3)
			{
				socket_data.order.brand.brand_name = category.category_details[fcm_socket_id.language_id -1].name;
				socket_data.order.brand.name = category.Brand.Product.category_brand_product_details[fcm_socket_id.language_id - 1].name;
			}
			else if(order.category_id < 6)
			{
				socket_data.order.brand.brand_name = category.Brand.category_brand_details[fcm_socket_id.language_id - 1].name;
				socket_data.order.brand.name = category.Brand.Product.category_brand_product_details[fcm_socket_id.language_id - 1].name;
			}
			else
			{
				socket_data.order.brand.brand_name = "";
				socket_data.order.brand.name = "";
			}
			let tramsproducts = socket_data.order.products;
			//socket_data.order.products = tramsproducts;
			console.log("App"+fcm_socket_id.driver_user_detail_id, "Emit Event Room");
			io.sockets.in("App"+fcm_socket_id.driver_user_detail_id).emit(event_name, socket_data);
			// io.sockets.in(fcm_socket_id.socket_id).emit(event_name, socket_data);
		}
		catch(e)
		{
			console.log("Error Emiting Socket Plus Push 4 Order Booking -", fcm_socket_id, e.message);
		}
	});*/
	
};


//Add M2 Part 2
//////////////////		Push Plus Socket 	/////////////////////////////////////////////////
exports.pushReassignOrder = async (fcm_socket_id, push_data, socket_data, order, category, event_name) => {

	push_data.sound = "custom_tone.wav";
	let finalOutput = {};
	try {
		
		try{
			//console.log("payload==========",fcm_socket_id.fcm_id);
			if(fcm_socket_id.fcm_id != "" && fcm_socket_id.notifications == "1")
			{
				if (fcm_socket_id.language_id == 1)
				var locale = "1En";
				else if (fcm_socket_id.language_id == 2)
					var locale = "2Hi";
				else if (fcm_socket_id.language_id == 3)
					var locale = "3Ur";
				else if (fcm_socket_id.language_id == 4)
					var locale = "4Ch";
				else
					var locale = "5Ar";

				var message = "";
				message = "Admin" + trans({phrase: " has requested a new service", locale});
				push_data.message = message;
				let payload = {
					notification: {
						title: process.env.APP_NAME,
						body: message,
						push_type: push_data.type,
						sound: "custom_tone.wav",
						tag: "null"
					},
					data: push_data
				};
				//console.log("payload==========",payload);
				admin.messaging().sendToDevice(fcm_socket_id.fcm_id, payload);
			}

				//order.payment.buraq_percentage = socket_data.order.payment.buraq_percentage;
				//order.payment.bottle_charge = socket_data.order.payment.bottle_charge;

				socket_data.order.brand.category_name = category.category_details[fcm_socket_id.language_id -1].name;
				socket_data.message = message;

				if(order.category_id  == 1 || order.category_id == 3)
				{
					socket_data.order.brand.brand_name = category.category_details[fcm_socket_id.language_id -1].name;
					socket_data.order.brand.name = category.Brand.Product.category_brand_product_details[fcm_socket_id.language_id - 1].name;
				}
				else if(order.category_id < 6)
				{
					socket_data.order.brand.brand_name = category.Brand.category_brand_details[fcm_socket_id.language_id - 1].name;
					socket_data.order.brand.name = category.Brand.Product.category_brand_product_details[fcm_socket_id.language_id - 1].name;
				}
				else
				{
					socket_data.order.brand.brand_name = "";
					socket_data.order.brand.name = "";
				}
				let tramsproducts = socket_data.order.products;
				//console.log('updatedProductCollection: ', tramsproducts);
				let updatedProductCollection = await getProductName(tramsproducts, fcm_socket_id.language_id);
				//console.log('updatedProductCollection: ', updatedProductCollection);
				socket_data.order.products = updatedProductCollection;
				finalOutput = socket_data;
				//console.log('finalOutput: ', JSON.parse(JSON.stringify(finalOutput)));
				io.sockets.in("App"+fcm_socket_id.user_detail_id).emit(event_name, finalOutput);
		}
		catch(e)
		{
			console.log("Error Emiting Socket Plus Push 4 Order Booking -", fcm_socket_id, e.message);
		}
		

	} catch(err) {
		console.log(err);
	}

};

async function getProductName (tramsproducts, language_id) {

	 for (const tramsproduct of tramsproducts) {
	    const productsCollection = await  
	    Db.sequelize.query("SELECT name FROM category_brand_product_details  WHERE category_brand_product_id='"+tramsproduct.category_brand_product_id+"' AND language_id='"+language_id+"'", {type: Sequelize.QueryTypes.SELECT })
		.then( async (response) => {
			tramsproducts[0].productName = response[0].name;
		});
		//console.log('tramsproducts: ', tramsproducts);
	    return tramsproducts;
	 }
	
}


/////////////////////		Update Order Map Data 	/////////////////////////////////////////
exports.DayString = (timings, timezone) =>
{

	if(timezone == undefined)
		timezone = "Asia/Muscat";

	var day = moment(timings, "YYYY-MM-DD HH:mm:ss").tz(timezone).format("dddd");

	var column;
	switch (day) {
			    case "Sunday":
			        column = "sunday_service";
			        break;
			    case "Monday":
			        column = "monday_service";
			        break;
			    case "Tuesday":
			        column = "tuesday_service";
			        break;
			    case "Wednesday":
			        column = "wednesday_service";
			        break;
			    case "Thursday":
			        column = "thursday_service";
			        break;
			    case "Friday":
			        column = "friday_service";
			        break;
			    case "Saturday":
			        column = "saturday_service";
	}

	return "oa."+column+" = '1'";

};

///////////////////////		Latest Send Push 		////////////////////////////////
exports.latestSendPushNotification = (fcm_socket_ids, push_data) => {

	//push_data.sound = "custom_tone.wav";

	var payload = {
		notification: {
			title: process.env.APP_NAME,
			body: push_data.message,
			push_type: push_data.type,
			sound: "custom_tone.wav",
			tag: "null"
		},
		data: push_data
	};
	 console.log('Payload',payload);
	fcm_socket_ids.forEach(function(fcm_socket_id) {
		try{

			///////////////			New Code 		//////////////////////////////////////////////////
			// if(fcm_socket_id.device_type == 'Ios')
			// 	{
			// 		push_data.title = process.env.APP_NAME;
			// 		push_data.body = push_data.message;

			// 		var payload = {
			// 			notification: push_data
			// 		};
								
			// 	}
			// else
			// 	{
			// 		var payload = {
			// 			data: push_data
			// 		};
			// 	}

			// admin.messaging().sendToDevice(fcm_socket_id.fcm_id, payload)
			// .then(function(response) {
			// 	console.log("Push Messageeeeeeeeeeeeeeeeeeeeeeeeeeeeeee -", fcm_socket_id.device_type ,fcm_socket_id.fcm_id, payload, response);
			// })
			///////////////			New Code 		//////////////////////////////////////////////////

			///////////////			Old Code 		//////////////////////////////////////////////////
			admin.messaging().sendToDevice(fcm_socket_id.fcm_id, payload);
			 
			///////////////			Old Code 		//////////////////////////////////////////////////

		}
		catch(e)
		{
			console.log("Error latestSendPushNotification Simple Push -", fcm_socket_id, e.message);
		}
	});


};
//Add M2 Part 2
exports.pushPlusEventsQuotation = (fcm_socket_ids, push_data, socket_data,category, event_name) => {
	push_data.sound = "custom_tone.wav";
	fcm_socket_ids.forEach(function(fcm_socket_id) {
		try{
			if(socket_data.type != "SerCancelQuoteRequest")
			{
				if(fcm_socket_id.fcm_id != "" && fcm_socket_id.notifications == "1")
				{
					push_data.message = fcm_socket_id.message;
					let payload = {
						notification: {
							title: process.env.APP_NAME,
							body: fcm_socket_id.message,
							push_type: push_data.type,
							sound: "custom_tone.wav",
							tag: "null"
						},
						data: push_data
					};
					admin.messaging().sendToDevice(fcm_socket_id.fcm_id, payload);
				}
			}
			socket_data.order.brand.category_name = category.category_details[fcm_socket_id.language_id -1].name;
			socket_data.message                   = fcm_socket_id.message;
			socket_data.order.brand.brand_name    = category.Brand.category_brand_details[fcm_socket_id.language_id - 1].name;
			socket_data.order.brand.name          = category.Brand.Product.category_brand_product_details[fcm_socket_id.language_id - 1].name;
			console.log("App"+fcm_socket_id.driver_user_detail_id, "Emit Event Room");
			io.sockets.in("App"+fcm_socket_id.driver_user_detail_id).emit(event_name, socket_data);
		}
		catch(e)
		{
			console.log("Error Emiting Socket Plus Push 4 Order Booking -", fcm_socket_id, e.message);
		}
	});
};
//Add M2 Part 2
exports.pushSendCustomerQuotation = (customer, push_data, socket_data, event_name) => {
	push_data.sound = "custom_tone.wav";
	//fcm_socket_ids.forEach(function(fcm_socket_id) {
		try{
			if(customer.fcm_id != "" && customer.notifications == "1")
			{
				let payload = {
					notification: {
						title: process.env.APP_NAME,
						body: push_data.message,
						push_type: push_data.type,
						sound: "custom_tone.wav",
						tag: "null"
					},
					data: push_data
				};
				admin.messaging().sendToDevice(customer.fcm_id, payload);
			}
			//socket_data.order.brand.category_name = category.category_details[fcm_socket_id.language_id -1].name;
			socket_data.message                     = push_data.message;
			//socket_data.order.brand.brand_name    = category.Brand.category_brand_details[fcm_socket_id.language_id - 1].name;
			//socket_data.order.brand.name          = category.Brand.Product.category_brand_product_details[fcm_socket_id.language_id - 1].name;
			console.log("App"+customer.user_detail_id, "Emit Event Room");
			io.sockets.in("App"+customer.user_detail_id).emit(event_name, socket_data);
		}
		catch(e)
		{
			console.log("Error Emiting Socket Plus Push 4 Order Booking -", customer, e.message);
		}
	//});
};


///////////////////////        Latest Send Push         ////////////////////////////////
exports.SendMultipleNotificationtousers = (fcm_socket_ids, push_data) => {
    //console.log('FCM ID',fcm_socket_ids);
    var payload = {
        notification: {
            title: process.env.APP_NAME,
            body: push_data.message,
            sound: "custom_tone.wav",
            tag: "null"
        },
        data: push_data
    };
    console.log('Payload',payload);
    fcm_socket_ids.forEach(function(fcm_socket_id) {
        try{
                var registrationToken = fcm_socket_id.fcm_id;
                //var registrationToken = "cJ6S3uxsLRM:APA91bH6hE6KM23DkDo81sekEXvysm0v9wTvVXfvZxD87CpZeu_dnapYIlh5Rhu6dCfHBFzrWT1-cTNslYZ5lXwmu8FFLotUMNypGfwz0y91XvV83vqW3tLGA4u0cGrus9vDuo2PNPDP";
                
                /*var payload = {
                notification: {
                    title: process.env.APP_NAME,
                    body: push_data.message
                }
                };*/
                var options = {
                    priority: 'high',
                    timeToLive: 60 * 60 * 24
                };
                if(registrationToken != "")
                {
                    console.log('token================',registrationToken);
                    admin.messaging().sendToDevice(registrationToken, payload, options)
                    .then(function(response) {
                        console.log('Successfully sent message:', response);
                    })
                    .catch(function(error) {
                        console.log('Error sending message:', error);
                    });
                }
        }
        catch(e)
        {
            console.log("Error latestSendPushNotification Simple Push -", fcm_socket_ids, e.message);
        }
    });
};

//Add M3 Part 3.2
exports.SendpaymentNotificationtousers = (user, push_data) => {
	push_data.sound = "custom_tone.wav";
	try{
		if(user.fcm_id != "" && user.notifications == "1")
		{
			let payload = {
				notification: {
					title: process.env.APP_NAME,
					body: push_data.message,
					push_type: push_data.type,
					sound: "custom_tone.wav",
					tag: "null"
				},
				data: push_data
			};
			console.log("payload======",payload);
			admin.messaging().sendToDevice(user.fcm_id, payload);
		}
	}
	catch(e)
	{
		console.log("Error Emiting Socket Plus Push 4 Order Booking -", user, e.message);
	}
};

