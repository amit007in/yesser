var moment = require("moment");
var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;

var commonFunctions = require(appRoot+"/libs/commonFunctions");//////////////////		Libraries
var Libs = require(appRoot+"/libs");//////////////////		Libraries
var Services = require(appRoot+"/services");///////////		Services
var Db = require(appRoot+"/models");//////////////////		Db Models
var Configs = require(appRoot+"/configs");/////////////		Configs


////////////////////////	Services about to start 	////////////////////////////////////
exports.oneHrOrdersLeft =  async () => {

	var data = {};

	data.now = moment.utc().format("YYYY-MM-DD HH:mm:ss");//"2018-09-13 08:03:41"; //
	data.now_1hr = moment.utc().add(1, "hour").format("YYYY-MM-DD HH:mm:ss");//"2018-09-13 09:03:41";//

	console.log("60 Minutes Cron - "+ data.now);
        console.log("60 Minutes Cron - "+ data.now_1hr);
	data.adminMessages = {
		"en": "There is schedule delivery after 1 hour from now with customer ",
		"hi": trans({phrase: "There is schedule delivery after 1 hour from now with customer ", locale: "2Hi"}),
		"ur": trans({phrase: "There is schedule delivery after 1 hour from now with customer ", locale: "3Ur"}),
		"ch": trans({phrase: "There is schedule delivery after 1 hour from now with customer ", locale: "4Ch"}),
		"ar": trans({phrase: "There is schedule delivery after 1 hour from now with customer ", locale: "5Ar"}),
	};

	data.custMessages = {
		"en": "You have a scheduled order after 1 hour Service - ",
		"hi": trans({phrase: "You have a scheduled order after 1 hour Service - ", locale: "2Hi"}),
		"ur": trans({phrase: "You have a scheduled order after 1 hour Service - ", locale: "3Ur"}),
		"ch": trans({phrase: "You have a scheduled order after 1 hour Service - ", locale: "4Ch"}),
		"ar": trans({phrase: "You have a scheduled order after 1 hour Service - ", locale: "5Ar"})
	};

	var orders = await Db.orders.findAll({
		attributes: [
			"order_id", "order_timings", "customer_user_id", "customer_user_detail_id", "driver_user_id", "driver_user_detail_id", "order_status", "future", "category_id", "category_brand_product_id", "category_brand_id",
			[Sequelize.literal("( TIMESTAMPDIFF(minute, now(), order_timings) )"), "time_left"],
		],
		where: {
			order_status: "Scheduled",
			//order_type: "Service",
			order_timings: { $between: [data.now, data.now_1hr] }
		},
		include: [
			{
				model: Db.payments
			},
			{
				as: "Customer",
				model: Db.users
			},
			{
				as: "CustomerDetails",
				model: Db.user_details
			},
			{
				as: "Driver",
				model: Db.users,
			},
			{
				as: "DriverDetails",
				model: Db.user_details,
				include: [
					{
						required: false,
						as: "Org",
						model: Db.organisations
					}
				]
			},
			{
				as: "CRequest",
				model: Db.order_requests,
				where: {
					order_request_status: "Accepted"
				}
			},
			{
				model: Db.categories,
				as: "Category",
				include: [{
						model: Db.category_details,
						as: "Details"
					}]
			}

		],
		order: [ ["updated_at","ASC"] ],

		having: {
			time_left: {
				$lte: 60
			}
		},
		group: "order_id"

	})
		.then( (orders) => {
                        console.log("schudled orders====",orders);
			orders =  JSON.parse(JSON.stringify(orders));

			for (let index = 0; index < orders.length; index++) 
			{
				orders[index].temp = scheduleStarted(orders[index]);
			}

		});

};

/////////////////////	New Schedule Start 	////////////////////////////////////////////////
var newScheduleStarted = async (data, order) => {
	try{

		data.created_at = moment.utc().format("YYYY-MM-DD HH:mm:ss");

		var Customer = order.Customer;
		var CustomerDetails = order.CustomerDetails;
		var Driver = order.Driver;
		var DriverDetails = order.DriverDetails;

		if(!data.order_distance)
			data.order_distance = 0;

		data.adminMessages = {
			"en": "There is schedule delivery after 1 hour from now with customer "+Customer.name+", "+Customer.phone_code+" "+Customer.phone_number,
			"hi": trans({phrase: "There is schedule delivery after 1 hour from now with customer ", locale: "2Hi"})+Customer.name+", "+Customer.phone_code+" "+Customer.phone_number,
			"ur": trans({phrase: "There is schedule delivery after 1 hour from now with customer ", locale: "3Ur"})+Customer.name+", "+Customer.phone_code+" "+Customer.phone_number,
			"ch": trans({phrase: "There is schedule delivery after 1 hour from now with customer ", locale: "4Ch"})+Customer.name+", "+Customer.phone_code+" "+Customer.phone_number,
			"ar": trans({phrase: "There is schedule delivery after 1 hour from now with customer ", locale: "5Ar"})+Customer.name+", "+Customer.phone_code+" "+Customer.phone_number,
		};

		//you have schedule order after 1 hour service - cylinder small/big/xyz price-xyz , driver name-xyz"
		data.custMessages = {
			"en": "You have a scheduled order after 1 hour Service - "+order.Category.category_details[0].name+", "+order.category_brand_product.category_brand_product_details[0].name+", OMR "+order.payment.final_charge+", "+order.Driver.name,
	
			"hi": trans({phrase: "You have a scheduled order after 1 hour Service - ", locale: "2Hi"})+order.Category.category_details[1].name+", "+order.category_brand_product.category_brand_product_details[1].name+", OMR "+order.payment.final_charge+", "+order.Driver.name,
	
			"ur": trans({phrase: "You have a scheduled order after 1 hour Service - ", locale: "3Ur"})+order.Category.category_details[2].name+", "+order.category_brand_product.category_brand_product_details[2].name+", OMR "+order.payment.final_charge+", "+order.Driver.name,
	
			"ch": trans({phrase: "You have a scheduled order after 1 hour Service - ", locale: "4Ch"})+order.Category.category_details[3].name+", "+order.category_brand_product.category_brand_product_details[3].name+", OMR "+order.payment.final_charge+", "+order.Driver.name,
	
			"ar": trans({phrase: "You have a scheduled order after 1 hour Service - ", locale: "5Ar"})+order.Category.category_details[4].name+", "+order.category_brand_product.category_brand_product_details[4].name+", OMR "+order.payment.final_charge+", "+order.Driver.name,
		};
		//["1En", "2Hi", "3Ur", "4Ch", "5Ar"],

		////////////////		Admin SMS 		////////////////////////////////////
		data.phone_numbers = ["96898133781", "96890630609", "96894704604"];
		var sendSMS = Libs.commonFunctions.multipleSendSMS(data.adminMessages.en, data.phone_numbers);
		////////////////		Admin SMS 		////////////////////////////////////

		////////////////		Driver SMS 		////////////////////////////////////
		if(data.app_language_id == 2)
			data.driver_sms_message = data.adminMessages.hi;
		else if(data.app_language_id == 3)
			data.driver_sms_message = data.adminMessages.ur;
		else if(data.app_language_id == 4)
			data.driver_sms_message = data.adminMessages.ch;
		else if(data.app_language_id == 5)
			data.driver_sms_message = data.adminMessages.ar;
		else
			data.driver_sms_message = data.adminMessages.en;


		////////////////		Driver SMS 		////////////////////////////////////


	}
	catch(e)
	{
		console.log("Single Order Schedule Start Cron Error -"+order.order_id, e.message);
	}
};

/////////////////////		Schedule Start 	////////////////////////////////////////////////
var scheduleStarted = async (order) => {
	try{
		console.log("Startinggggggggggggggggggggggggggggggggg Order", order.order_id);

		var data = {};
		data.created_at = moment.utc().format("YYYY-MM-DD HH:mm:ss");

		var Customer = order.Customer;
		var CustomerDetails = order.CustomerDetails;
		var Driver = order.Driver;
		var DriverDetails = order.DriverDetails;

		if(!data.order_distance)
			data.order_distance = 0;

		///////////		Order Time 	//////////////////////
		data.order_time = order.payment.order_time; //0;

		///////////////////		Update 	Db 	/////////////////////////////
		data.update_order = await Db.orders.update({
				order_status:  "DPending",
				updated_at: data.created_at
			},
			{
				where: { order_id: order.order_id }
			}
		);
		///////////////////		Update 	Db 	/////////////////////////////

		///////////////////////////////////////////////////
		////////////////////		Customer Push Socket
		var cust_message = commonFunctions.generate_lang_message("Your scheduled service is about to start", CustomerDetails.language_id);

		////////////////		Push Notifications 	///////////////////////
		if(CustomerDetails.fcm_id != "" && CustomerDetails.notifications == "1"){

			data.cust_push_data = {
				order_id: (order.order_id).toString(),
				type: "DPending",
				message: cust_message
			};
					
			var push = commonFunctions.latestSendPushNotification([CustomerDetails], data.cust_push_data);		
		}
		////////////////		Push Notifications 	///////////////////////

		////////////////////		Customer Push Socket
		///////////////////////////////////////////////////

		///////////////////////////////////////////////////
		////////////////////		Driver Push Socket
		var driver_message = commonFunctions.generate_lang_message("Your scheduled service is about to start", DriverDetails.language_id);

		////////////////		Push Notifications 	///////////////////////
		if(DriverDetails.fcm_id != "" && DriverDetails.notifications == "1")
		{

			data.driver_push_data = {
				order_id: (order.order_id).toString(),
				type: "DPending",
				message: driver_message,
				future: order.future
			};
					
			var push = commonFunctions.latestSendPushNotification([DriverDetails], data.driver_push_data);		
		}
		////////////////		Push Notifications 	///////////////////////

		////////////////		Socket Event 	///////////////////////////
		data.socket_data = {
			type: "DPending",
			order: {
				order_id: order.order_id,
				driver_user_detail_id: order.driver_user_detail_id,
				user: {
					user_id: order.customer_user_id,
					user_detail_id: order.customer_user_detail_id
				}
			},
			message: driver_message
		};

		var event = commonFunctions.emitDynamicEvent("OrderEvent", [DriverDetails.user_detail_id], data.socket_data);
		//////////////		Socket Event 	///////////////////////////

		var con = await Db.user_detail_notifications.bulkCreate([
			{
				/////////		Receiver 	/////////////////
				user_id: CustomerDetails.user_id,
				user_detail_id: CustomerDetails.user_detail_id,
				user_type_id: 1,
				user_organisation_id: CustomerDetails.organisation_id,
				///////////		Sender 	/////////////////////
				sender_user_id: DriverDetails.user_id,
				sender_user_detail_id: DriverDetails.user_detail_id,
				sender_type_id: 2,
				sender_organisation_id: DriverDetails.organisation_id,
				///////////		Order 	/////////////////////
				order_id: order.order_id,
				order_request_id: 0,
				order_rating_id: 0,

				message: cust_message,
				type: "DPending",
				is_read: "0",
				deleted: "0",
				created_at: data.created_at,
				updated_at: data.created_at
			},
			{
				/////////		Receiver 	/////////////////
				user_id: DriverDetails.user_id,
				user_detail_id: DriverDetails.user_detail_id,
				user_type_id: 2,
				user_organisation_id: DriverDetails.organisation_id,
				///////////		Sender 	/////////////////////
				sender_user_id: CustomerDetails.user_id,
				sender_user_detail_id: CustomerDetails.user_detail_id,
				sender_type_id: 1,
				sender_organisation_id: CustomerDetails.organisation_id,
				///////////		Order 	/////////////////////
				order_id: order.order_id,
				order_request_id: 0,
				order_rating_id: 0,

				message: driver_message,
				type: "DPending",
				is_read: "0",
				deleted: "0",
				created_at: data.created_at,
				updated_at: data.created_at
			}
		]);

		//////////////////		Driver Push Socket
		/////////////////////////////////////////////////

		return order;

	}
	catch(e)
	{

		console.log("60 Minutes Cron Error -"+order.order_id, e.message);

		return e.message;
	}
};

////////////////////		50 Minutes Past 		//////////////////////////////////////////
exports.fiftyMinutesPast = async () => {

	var data = {};

	data.now = moment.utc().format("YYYY-MM-DD HH:mm:ss");//"2018-09-13 08:03:41"; //
	data.now_1hr = moment.utc().add(1, "hour").format("YYYY-MM-DD HH:mm:ss");//"2018-09-13 09:03:41";//

	console.log("50 Minutes Cron - "+ data.now);

	var orders = await Db.orders.findAll({
		attributes: [
			"order_id", "order_timings", "customer_user_id", "customer_user_detail_id", "driver_user_id", "driver_user_detail_id", "order_status", "future", "category_id", "category_brand_product_id", "category_brand_id",
			//[Sequelize.literal("TIMESTAMPDIFF(minute, order_timings, orders.updated_at)"), 'time_past']
			// [Sequelize.literal("(SELECT TIMESTAMPDIFF(minute, now(), UDN.created_at) FROM user_detail_notifications AS UDN WHERE UDN.order_id=orders.order_id AND type='DPending' LIMIT 0,1)"), 'time_left']
			[Sequelize.literal("( TIMESTAMPDIFF(minute, now(), order_timings) )"), "time_left"],
		],
		where: {
			order_status: "DPending",
			order_timings: { $between: [data.now, data.now_1hr] }
		},
		include: [
			{
				model: Db.payments
			},
			{
				as: "Customer",
				model: Db.users
			},
			{
				as: "CustomerDetails",
				model: Db.user_details
			},
			{
				as: "Driver",
				model: Db.users,
			},
			{
				as: "DriverDetails",
				model: Db.user_details,
				include: [
					{
						required: false,
						as: "Org",
						model: Db.organisations
					}
				]
			},
			{
				as: "CRequest",
				model: Db.order_requests,
				where: {
					order_request_status: "Accepted"
				}
			},

		],

		order: [ ["updated_at","ASC"] ],
		having: {
			time_left: {
				$lte: 10
			}
		}

	})
		.then( (orders) => {

			orders =  JSON.parse(JSON.stringify(orders));

			for (let index = 0; index < orders.length; index++) 
			{
				orders[index].temp = driverNoResponse(orders[index]);
			}

			return orders;

		});

	return orders;

};

/////////////////		Driver No Response 		/////////////////////////////////////////////
var driverNoResponse = async (order) => {
	try{
	///console.log('NO Response Cron Log ------------------zzzzzzzzzzzzzzzzzzzzzzz-------------',order.order_id);

		var data = {};
		data.created_at = moment.utc().format("YYYY-MM-DD HH:mm:ss");

		var Customer = order.Customer;
		var CustomerDetails = order.CustomerDetails;
		var Driver = order.Driver;
		var DriverDetails = order.DriverDetails;

		data.order_request_status = "DSchTimeout";
		data.cancelled_by = "System";
		data.cancel_reason = "No Response";

		////////////////	Update DB 		///////////////////////////////
		data.update_order = await Db.orders.update(
			{
				order_status: data.order_request_status,
				cancel_reason: data.cancel_reason,
				cancelled_by: data.cancelled_by,
				updated_at: data.created_at
			},
			{
				where: { order_id: order.order_id }
			}
		);

		data.update_payment = await Db.payments.update(
			{
				payment_status: data.order_request_status,
				updated_at: data.created_at
			},
			{
				where: { order_id: order.order_id }
			}
		);


		var con = await Db.user_detail_notifications.bulkCreate([
			{
				/////////		Receiver 	/////////////////
				user_id: CustomerDetails.user_id,
				user_detail_id: CustomerDetails.user_detail_id,
				user_type_id: 1,
				user_organisation_id: CustomerDetails.organisation_id,
				///////////		Sender 	/////////////////////
				sender_user_id: DriverDetails.user_id,
				sender_user_detail_id: DriverDetails.user_detail_id,
				sender_type_id: 2,
				sender_organisation_id: DriverDetails.organisation_id,
				///////////		Order 	/////////////////////
				order_id: order.order_id,
				order_request_id: 0,
				order_rating_id: 0,

				message: cust_message,
				type: data.order_request_status,
				is_read: "0",
				deleted: "0",
				created_at: data.created_at,
				updated_at: data.created_at
			},
			{
				/////////		Receiver 	/////////////////
				user_id: DriverDetails.user_id,
				user_detail_id: DriverDetails.user_detail_id,
				user_type_id: 2,
				user_organisation_id: DriverDetails.organisation_id,
				///////////		Sender 	/////////////////////
				sender_user_id: CustomerDetails.user_id,
				sender_user_detail_id: CustomerDetails.user_detail_id,
				sender_type_id: 1,
				sender_organisation_id: CustomerDetails.organisation_id,
				///////////		Order 	/////////////////////
				order_id: order.order_id,
				order_request_id: 0,
				order_rating_id: 0,

				message: driver_message,
				type: data.order_request_status,
				is_read: "0",
				deleted: "0",
				created_at: data.created_at,
				updated_at: data.created_at
			}
		]);


		////////////////	Update DB 		///////////////////////////////

		///////////////////////////////////////////////////
		////////////////////		Customer Push Socket
		var cust_message = commonFunctions.generate_lang_message("Your scheduled service has been cancelled because of non availability of driver", CustomerDetails.language_id);

		////////////////		Push Notifications 	///////////////////////
		if(CustomerDetails.fcm_id != "" && CustomerDetails.notifications == "1")
		{

			data.cust_push_data = {
				order_id: (order.order_id).toString(),
				type: data.order_request_status,
				message: cust_message
			};
					
			var push = commonFunctions.latestSendPushNotification([CustomerDetails], data.cust_push_data);		
		}
		////////////////		Push Notifications 	///////////////////////
		////////////////////		Customer Push Socket
		///////////////////////////////////////////////////

		///////////////////////////////////////////////////
		////////////////////		Driver Push Socket
		var driver_message = commonFunctions.generate_lang_message("Your scheduled service has been cancelled because of no response from you", DriverDetails.language_id);

		////////////////		Push Notifications 	///////////////////////
		if(DriverDetails.fcm_id != "" && DriverDetails.notifications == "1")
		{

			data.driver_push_data = {
				order_id: (order.order_id).toString(),
				type: data.order_request_status,
				message: driver_message,
				future: order.future
			};
					
			var push = commonFunctions.latestSendPushNotification([DriverDetails], data.driver_push_data);		
		}
		////////////////		Push Notifications 	///////////////////////

		////////////////		Socket Event 	///////////////////////////
		data.socket_data = {
			type: data.order_request_status,
			driver_user_detail_id: order.driver_user_detail_id,
			order: {
				order_id: order.order_id,
				user: {
					user_id: order.customer_user_id,
					user_detail_id: order.customer_user_detail_id
				}
			},
			message: driver_message
		};

		var event = commonFunctions.emitDynamicEvent("OrderEvent", [DriverDetails.user_detail_id], data.socket_data);
		//////////////		Socket Event 	///////////////////////////

		//////////////////		Driver Push Socket
		/////////////////////////////////////////////////

		return order;

	}
	catch(e)
	{

		console.log("50 Minutes Cron Error -"+order.order_id, e.message);

		// await Db.orders.update(
		// 	{
		// 		updated_at: data.created_at
		// 	},
		// 	{
		// 		where: {order_id: order.order_id}
		// 	}
		// );

		return e.message;

	}
};

////////////////////		55 Minutes Past Cron 	//////////////////////////////
exports.fiftyFiveMinutesPast = async () => {

	var data = {};

	data.now = moment.utc().format("YYYY-MM-DD HH:mm:ss");//"2018-09-13 08:03:41"; //
	data.now_1hr = moment.utc().add(1, "hour").format("YYYY-MM-DD HH:mm:ss");//"2018-09-13 09:03:41";//

	console.log("55 Minutes Cron - "+ data.now);

	var orders = await Db.orders.findAll({
		attributes: [
			"order_id", "order_timings", "customer_user_id", "customer_user_detail_id", "driver_user_id", "driver_user_detail_id", "order_status", "future", "category_id", "category_brand_product_id", "category_brand_id",
			[Sequelize.literal("(SELECT COUNT(*) FROM orders as O1 WHERE O1.order_status IN ('Ongoing') AND O1.order_id != orders.order_id AND O1.customer_user_detail_id=orders.customer_user_detail_id)"), "o_ongoin_rides"],
			[Sequelize.literal("( TIMESTAMPDIFF(minute, now(), order_timings) )"), "time_left"],
		],
		where: {
			order_status: ["DApproved", "Ongoing"], //"DPending",
			order_timings: { $between: [data.now, data.now_1hr] }
		},

		include: [
			{
				model: Db.payments
			},
			{
				as: "Customer",
				model: Db.users
			},
			{
				as: "CustomerDetails",
				model: Db.user_details
			},
			{
				as: "Driver",
				model: Db.users,
			},
			{
				as: "DriverDetails",
				model: Db.user_details,
				include: [{
						required: false,
						as: "Org",
						model: Db.organisations
					}]
			},
			{
				as: "CRequest",
				model: Db.order_requests,
				where: {
					order_request_status: "Accepted"
				}
			}/*,
                        {
                            as: "Products",
			    model: Db.order_products,
                        }*/

		],

		having: {
			time_left: {
				$lte: 5
			},
			o_ongoin_rides: {
				$gte: 1
			}
		},

		order: [ ["updated_at","ASC"] ],

	})
		.then( (orders) => {

			orders =  JSON.parse(JSON.stringify(orders));

			for (let index = 0; index < orders.length; index++) 
			{
				orders[index].temp = custOngoingRide(orders[index]);
			}

			return orders;

		});

};

//////////////////		Custoemr Ongoing Order 		//////////////////////////////
/////////////////		Driver No Response 		/////////////////////////////////////////////
var custOngoingRide = async (order) => {
	try{

		var data = {};
		data.created_at = moment.utc().format("YYYY-MM-DD HH:mm:ss");

		var Customer = order.Customer;
		var CustomerDetails = order.CustomerDetails;
		var Driver = order.Driver;
		var DriverDetails = order.DriverDetails;

		data.order_request_status = "SysSchCancelled";
		data.cancelled_by = "System";
		data.cancel_reason = "User Ongoing Ride";

		////////////////	Update DB 		///////////////////////////////
		data.update_order = await Db.orders.update(
			{
				order_status: data.order_request_status,
				cancel_reason: data.cancel_reason,
				cancelled_by: data.cancelled_by,
				updated_at: data.created_at
			},
			{
				where: { order_id: order.order_id }
			}
		);

		data.update_payment = await Db.payments.update(
			{
				payment_status: data.order_request_status,
				updated_at: data.created_at
			},
			{
				where: { order_id: order.order_id }
			}
		);
		////////////////	Update DB 		///////////////////////////////

		///////////////////////////////////////////////////
		////////////////////		Customer Push Socket
		var cust_message = commonFunctions.generate_lang_message("Your scheduled service has been cancelled", CustomerDetails.language_id);

		////////////////		Push Notifications 	///////////////////////
		if(CustomerDetails.fcm_id != "" && CustomerDetails.notifications == "1")
		{

			data.cust_push_data = {
				order_id: (order.order_id).toString(),
				type: data.order_request_status,
				message: cust_message
			};
					
			var push = commonFunctions.latestSendPushNotification([CustomerDetails], data.cust_push_data);		
		}
		////////////////		Push Notifications 	///////////////////////
		////////////////////		Customer Push Socket
		///////////////////////////////////////////////////

		///////////////////////////////////////////////////
		////////////////////		Driver Push Socket
		var driver_message = commonFunctions.generate_lang_message("Your scheduled service has been cancelled", DriverDetails.language_id);

		////////////////		Push Notifications 	///////////////////////
		if(DriverDetails.fcm_id != "" && DriverDetails.notifications == "1")
		{

			data.driver_push_data = {
				order_id: (order.order_id).toString(),
				type: data.order_request_status,
				message: driver_message,
				future: order.future
			};
					
			var push = commonFunctions.latestSendPushNotification([DriverDetails], data.driver_push_data);		
		}
		////////////////		Push Notifications 	///////////////////////

		////////////////		Socket Event 	///////////////////////////
		data.socket_data = {
			type: data.order_request_status,
			order: {
				order_id: order.order_id,
				driver_user_detail_id: order.driver_user_detail_id,
				user: {
					user_id: order.customer_user_id,
					user_detail_id: order.customer_user_detail_id
				}
			},
			message: driver_message
		};

		var event = commonFunctions.emitDynamicEvent("OrderEvent", [DriverDetails.user_detail_id], data.socket_data);
		//////////////		Socket Event 	///////////////////////////

		var con = await Db.user_detail_notifications.bulkCreate([
			{
				/////////		Receiver 	/////////////////
				user_id: CustomerDetails.user_id,
				user_detail_id: CustomerDetails.user_detail_id,
				user_type_id: 1,
				user_organisation_id: CustomerDetails.organisation_id,
				///////////		Sender 	/////////////////////
				sender_user_id: DriverDetails.user_id,
				sender_user_detail_id: DriverDetails.user_detail_id,
				sender_type_id: 2,
				sender_organisation_id: DriverDetails.organisation_id,
				///////////		Order 	/////////////////////
				order_id: order.order_id,
				order_request_id: 0,
				order_rating_id: 0,

				message: cust_message,
				type: data.order_request_status,
				is_read: "0",
				deleted: "0",
				created_at: data.created_at,
				updated_at: data.created_at
			},
			{
				/////////		Receiver 	/////////////////
				user_id: DriverDetails.user_id,
				user_detail_id: DriverDetails.user_detail_id,
				user_type_id: 2,
				user_organisation_id: DriverDetails.organisation_id,
				///////////		Sender 	/////////////////////
				sender_user_id: CustomerDetails.user_id,
				sender_user_detail_id: CustomerDetails.user_detail_id,
				sender_type_id: 1,
				sender_organisation_id: CustomerDetails.organisation_id,
				///////////		Order 	/////////////////////
				order_id: order.order_id,
				order_request_id: 0,
				order_rating_id: 0,

				message: driver_message,
				type: data.order_request_status,
				is_read: "0",
				deleted: "0",
				created_at: data.created_at,
				updated_at: data.created_at
			}
		]);


	}
	catch(e)
	{

		console.log("55 Minutes Cron Error -"+order.order_id, e.message);

		// await Db.orders.update(
		// 	{
		// 		updated_at: data.created_at
		// 	},
		// 	{
		// 		where: {order_id: order.order_id}
		// 	}
		// );

		return e.message;

	}
};


///////////////////////////		Update Online Offline Status 		///////////////////////////////////////
exports.changeOnlineOffline = async () => {

	var data = {};

	data.created_at = moment.utc().format("YYYY-MM-DD HH:mm:ss");
	data.past_45min = moment.utc().subtract(2, "days").format("YYYY-MM-DD HH:mm:ss");

	data.updateStatus = await Db.user_details.update(
		{
			online_status: "0",
			socket_id: "",
			updated_at: data.created_at
		},
		{
			where: {
				online_status: "1",
				user_type_id: { $ne: 1 },
				updated_at: { $lte: data.past_45min }
			},
			//logging: console.log
		}
	);

};

//////////////////////////		Searching Order Update Cron 		///////////////////////////////////////
exports.searchingOrdersCronCancel = async () => {

	console.log("Searching Order Cancel Cron");

	return await Db.sequelize.query("SELECT *, TIMESTAMPDIFF(SECOND, O.created_at, now()) as secDiff FROM orders O JOIN user_details UD ON (UD.user_id=O.customer_user_id AND UD.user_detail_id=O.customer_user_detail_id) WHERE O.order_status='Searching' HAVING secDiff > 50", {type: Sequelize.QueryTypes.SELECT })
		.then( (orders) => {

			orders =  JSON.parse(JSON.stringify(orders));

			for (let index = 0; index < orders.length; index++) 
			{
				orders[index].temp = cancelSearchingOrder(orders[index]);
			}

			return orders;

		});

};

///////////////////////		Cancel Searching Order 		//////////////////////////////////////
var cancelSearchingOrder = async (order) => {
	try{
		var data = {};
		data.status = "SerTimeout";
		data.created_at = moment.utc().format("YYYY-MM-DD HH:mm:ss");
		data.order_id = order.order_id;
		data.app_language_id = order.language_id;
		console.log("orderorder_id=======");
		
		var adminRoundRobin = await Db.admins.findOne({ where: { admin_id: '1' } });
		if(adminRoundRobin.round_robin == "false")
		{
			data.update_order = await Db.orders.update(
				{
					order_status: data.status,
					//payment_status: data.status,
					cancelled_by: data.status,
					updated_at: data.created_at
				},
				{
					where: {
						order_id: order.order_id,
						order_status: "Searching"
					}
				}
			);


			var updateRequest = await Db.order_requests.update(
				{
					order_request_status: data.status,
					driver_request_status: '1',
					updated_at: data.created_at
				},
				{
					where: {
						order_id: order.order_id,
						order_request_status: "Searching"						
					}
				}
			);
			
			//Add M3 Part 3.2
			if(order.schulderd_order_token != "")
			{
				var stoken   = order.schulderd_order_token;
				await Db.orders.update(
						{
							order_status: data.status,
							cancelled_by: data.status
						},
						{
							where: {schulderd_order_token: stoken}
						}
				);
			}
			
			////////////////		Socket Event 	///////////////////////////
			var temp_order = await Services.orderServices.driverAcceptOrderDetails(data, data.app_language_id);

			data.socket_data = {
				type: data.status,
				order: temp_order
			};

			var event = commonFunctions.emitDynamicEvent("OrderEvent", [order.user_detail_id], data.socket_data);
			////////////////		Socket Event 	///////////////////////////
		}

	}
	catch(e)
	{
		console.log("1 Minutes Cron Error -"+order.order_id, e.message);

		return e.message;
	}
};

//////////////////			Driver Reaching Location 	///////////////////////////////////////
exports.ReachingLocation = async () => { 

	// var orders = await Db.sequelize.query("SELECT *, ROUND(((3959*acos(cos(radians(O.dropoff_latitude)) * cos(radians(UD.latitude)) * cos(radians(UD.longitude)-radians(O.dropoff_longitude))+sin(radians(O.dropoff_latitude))*sin(radians(UD.latitude))))* 1.67),5) as distance FROM orders AS O JOIN user_details AS UD on (UD.user_detail_id=O.driver_user_detail_id) WHERE O.order_status = 'DApproved' HAVING ( (SELECT COUNT(*) FROM user_detail_notifications AS UDN WHERE UDN.order_id=O.order_id AND type='About2Start') = 0) AND distance < 2 ", {type: Sequelize.QueryTypes.SELECT})

	var orders = await Db.sequelize.query("SELECT *, O.category_id as category_id, ROUND(((3959*acos(cos(radians(O.dropoff_latitude)) * cos(radians(UD.latitude)) * cos(radians(UD.longitude)-radians(O.dropoff_longitude))+sin(radians(O.dropoff_latitude))*sin(radians(UD.latitude))))* 1.67),5) as distance_dropoff, ROUND(((3959*acos(cos(radians(O.pickup_latitude)) * cos(radians(UD.latitude)) * cos(radians(UD.longitude)-radians(O.pickup_longitude))+sin(radians(O.pickup_latitude))*sin(radians(UD.latitude))))* 1.67),5) as distance_pickup FROM orders AS O JOIN user_details AS UD on (UD.user_detail_id=O.driver_user_detail_id) WHERE O.order_status = 'DApproved' HAVING ( (SELECT COUNT(*) FROM user_detail_notifications AS UDN WHERE UDN.order_id=O.order_id AND type='About2Start') = 0) AND ((CASE WHEN O.category_id = 4 THEN distance_pickup ELSE distance_dropoff END) < 2)", {type: Sequelize.QueryTypes.SELECT})
		.then( (orders) => {

			orders =  JSON.parse(JSON.stringify(orders));

			for (let index = 0; index < orders.length; index++) 
			{
				orders[index].temp = driverReached(orders[index]);
			}

			return orders;

		});

};

exports.SendCronNotification = async () => {
    //var users = await Db.notifications.findAll({ where: {status: 'Active',broadcastDate: moment(new Date()).format("YYYY-MM-DD HH:mm:ss")} });
    var users = await Db.notifications.findAll({ where: {status: 'Active',recurring:'On',broadcastDate: '2019-05-02 11:30:20'} });
    users.forEach(function(notification){
        var message    = notification.message;
        var type       = (notification.type == "Customer") ? '1' : '2';
        var title      = notification.title;
        var coupon_id  = notification.coupon_id;
        var service_id = notification.service_id;
        var image      = notification.image;
        var data = Array();
        data.cust_push_data = {
            type: "BulkNotification",
            message: message,
            future: "0",
            title: title,
            couponId: coupon_id,
            serviceId: service_id,
            image: image,
            mediaType: "Image"
        };
        var addresses = GetAllUNotification(data,type);
        console.log(addresses);
        //Libs.commonFunctions.SendMultipleNotificationtousers(userss, data.cust_push_data);
        
    });
    return "hjf";
};

var GetAllUNotification = async (data,type) => {
    
    return await Db.sequelize.query("SELECT * FROM user_details  WHERE user_type_id='"+type+"'", {type: Sequelize.QueryTypes.SELECT })
		.then( (orders) => {

			orders =  JSON.parse(JSON.stringify(orders));

			return orders;

		});
   
}

///////////////////////		Driver Reached Pickup
var driverReached = async (order) => { console.log("driver reaching order ==", order);
	try{

		var data = {};
		data.order_request_status = "About2Start";
		data.created_at = moment.utc().format("YYYY-MM-DD HH:mm:ss");

		var driver_message = commonFunctions.generate_lang_message("You are about to reach the pickup location", order.language_id);

		////////////////		Push Notifications 	///////////////////////
		if(order.fcm_id != "" && order.notifications == "1")
		{

			data.driver_push_data = {
				order_id: (order.order_id).toString(),
				type: data.order_request_status,
				message: driver_message,
				future: order.future
			};
					
			var push = commonFunctions.latestSendPushNotification([order], data.driver_push_data);		
		}
		////////////////		Push Notifications 	///////////////////////

		////////////////		Socket Event 	///////////////////////////
		data.socket_data = {
			type: data.order_request_status,
			order: {
				order_id: order.order_id,
				driver_user_detail_id: order.driver_user_detail_id,
				user: {
					user_id: order.customer_user_id,
					user_detail_id: order.customer_user_detail_id
				}
			},
			message: driver_message
		};

		var event = commonFunctions.emitDynamicEvent("OrderEvent", [order.user_detail_id], data.socket_data);
		//////////////		Socket Event 	///////////////////////////

		var con = await Db.user_detail_notifications.create(
			{
				/////////		Receiver 	/////////////////
				user_id: order.driver_user_id,
				user_detail_id: order.driver_user_detail_id,
				user_type_id: order.driver_user_type_id,
				user_organisation_id: order.driver_organisation_id,
				///////////		Sender 	/////////////////////
				sender_user_id: order.customer_user_id,
				sender_user_detail_id: order.customer_user_detail_id,
				sender_type_id: order.customer_user_type_id,
				sender_organisation_id: order.customer_organisation_id,
				///////////		Order 	/////////////////////
				order_id: order.order_id,
				order_request_id: 0,
				order_rating_id: 0,

				message: driver_message,
				type: data.order_request_status,
				is_read: "0",
				deleted: "0",
				created_at: data.created_at,
				updated_at: data.created_at
			}
		);

		//////////////////		Driver Push Socket
		/////////////////////////////////////////////////

		return order;

	}
	catch(e)
	{
		console.log("1 Minutes Cron Error -"+order.order_id, e.message);

		return e.message;
	}
};

////////////////////