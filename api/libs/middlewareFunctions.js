var Services = require(appRoot+"/services");///////////   Services
var Db = require(appRoot+"/models");//////////////////    Db Models
var Libs = require(appRoot+"/libs");//////////////////    Libraries

var moment = require("moment-timezone");

////////////////////////////////////////////////////////////
///////////////////////////     Rating Logged In  /////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
exports.UserRatingloginInRequired = async (request, response, next) => {
	try{
		//console.log("request.originalUrl", request.originalUrl);

		var data = request.body;

		if(!request.get("access_token"))
			return response.status(401).json({ success: -3, statusCode: 401, msg: response.trans("Your token has expired. Please login first")});

		request.body.access_token = request.get("access_token");

		var Details = await Services.userDetailServices.DetailRatingsMiddlewareGet(request.body);
		if(!Details)
			return response.status(401).json({ success: -3, statusCode: 401, msg: response.trans("Your token has expired. Please login first")});

		Details = Details[0];
		// if(Details.blocked == '1')
		//     return response.status(403).json({success: -2, statusCode: 403, msg: response.trans("Sorry, this service is currently not available to you. Please contact admin") });

		//////////////      Blocked Check Cust, Driver, Org Except for Order Routes   //////////////////
		if((Details.user_type_id == 1) && (request.originalUrl != "/user/service/Ongoing") && (request.originalUrl != "/user/service/Cancel") )
		{

			if( (Details.blocked == "1") || (Details.Org && Details.Org.blocked == "1") )
				return response.status(403).json({success: -2, statusCode: 403, msg: response.trans("Sorry, this service is currently not available to you. Please contact admin") });

		}
		else if((Details.user_type_id == 2) && (request.originalUrl != "/service/order/Ongoin") && (request.originalUrl != "/service/order/End") )
		{

			if( (Details.blocked == "1") || (Details.Org && Details.Org.blocked == "1") )
				return response.status(403).json({success: -2, statusCode: 403, msg: response.trans("Sorry, this service is currently not available to you. Please contact admin") });

		}
		// else
		// {

		// }
		//////////////      Blocked Check Cust, Driver, Org Except for Order Routes   //////////////////

		request.uDetails = request.body.Details = Details;
		request.body.user_id = Details.user_id;
		request.body.user_detail_id = Details.user_detail_id;
		request.body.user_type_id = Details.user_type_id;
		request.body.user_organisation_id = Details.organisation_id;

		// request.user_id = Details.user_id;
		// request.user_detail_id = Details.user_detail_id;
		// request.user_type_id = Details.user_type_id;
		// request.user_organisation_id = Details.organisation_id;

		//////////////////////    Language Set  //////////////////////////
		if(request.body.language_id)
			request.app_language_id = request.body.app_language_id = request.body.language_id;
		else if(request.get("language_id"))
			request.app_language_id = request.body.app_language_id = request.get("language_id");
		else
			request.app_language_id = request.body.app_language_id = Details.language_id;

		if(request.body.app_language_id == 2)
			response.setLocale("2Hi");
		else if(request.body.app_language_id == 3)
			response.setLocale("3Ur");
		else if(request.body.app_language_id == 4)
			response.setLocale("4Ch");
		else if(request.body.app_language_id == 5)
			response.setLocale("5Ar");
		else
			response.setLocale("1En");
		//////////////////////    Language Set  //////////////////////////

		request.created_at = request.body.created_at = moment.utc().format("YYYY-MM-DD HH:mm:ss");

		// new Date().toISOString().
		//          replace(/T/, ' ').      // replace T with a space
		//          replace(/\..+/, '');

		next();

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message, Details});
	}
};

////////////////////////////////////////////////////////////
///////////////////////////     Logged In Required     /////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
exports.loginInRequired = async (request, response, next) => {
	try{

		var data = request.body;

		if(!request.get("access_token"))
			return response.status(401).json({ success: -3, statusCode: 401, msg: response.trans("Your token has expired. Please login first")});

		request.body.access_token = request.get("access_token");

		var Details = await Services.userDetailServices.DetailMiddlewareGet(request.body);
		if(!Details)
			return response.status(401).json({ success: -3, statusCode: 401, msg: response.trans("Your token has expired. Please login first")});

		//////////////      Blocked Check Cust, Driver, Org Except for Order Routes   //////////////////
		if((Details.user_type_id == 1) && (request.originalUrl != "/user/service/Ongoing") && (request.originalUrl != "/user/service/Cancel") )
		{

			if( (Details.blocked == "1") || (Details.Org && Details.Org.blocked == "1") )
				return response.status(403).json({success: -2, statusCode: 403, msg: response.trans("Sorry, this service is currently not available to you. Please contact admin") });

		}
		else if((Details.user_type_id == 2) && (request.originalUrl != "/service/order/Ongoin") && (request.originalUrl != "/service/order/End") )
		{

			if( (Details.blocked == "1") || (Details.Org && Details.Org.blocked == "1") )
				return response.status(403).json({success: -2, statusCode: 403, msg: response.trans("Sorry, this service is currently not available to you. Please contact admin") });

		}
		// else
		// {

		// }
		//////////////      Blocked Check Cust, Driver, Org Except for Order Routes   //////////////////

		request.uDetails = request.body.Details = Details;
		request.body.user_id = Details.user_id;
		request.body.user_detail_id = Details.user_detail_id;
		request.body.user_type_id = Details.user_type_id;
		request.body.user_organisation_id = Details.organisation_id;

		//////////////////////    Language Set  //////////////////////////
		if(request.body.language_id)
			request.app_language_id = request.body.app_language_id = request.body.language_id;
		else if(request.get("language_id"))
			request.app_language_id = request.body.app_language_id = request.get("language_id");
		else
			request.app_language_id = request.body.app_language_id = Details.language_id;

		if(request.body.app_language_id == 2)
			response.setLocale("2Hi");
		else if(request.body.app_language_id == 3)
			response.setLocale("3Ur");
		else if(request.body.app_language_id == 4)
			response.setLocale("4Ch");
		else if(request.body.app_language_id == 5)
			response.setLocale("5Ar");
		else
			response.setLocale("1En");
		//////////////////////    Language Set  //////////////////////////

		request.created_at = request.body.created_at = moment.utc().format("YYYY-MM-DD HH:mm:ss");

		next();

	}
	catch(e)
	{
		return response.status(500).json({ success: 0, statusCode: 500, msg: e.message});
	}
};

//////////////////////      Language Middleware     /////////////////////////////////////////////////////////////
exports.languageMiddleware = async function(request, response, next)
{

	if(!request.get("language_id"))
		request.body.app_language_id = 1;
	else
		request.body.app_language_id = request.get("language_id");

	if(request.body.app_language_id == 2)
		response.setLocale("2Hi");
	else if(request.body.app_language_id == 3)
		response.setLocale("3Ur");
	else if(request.body.app_language_id == 4)
		response.setLocale("4Ch");
	else if(request.body.app_language_id == 5)
		response.setLocale("5Ar");
	else
		response.setLocale("1En");

	request.created_at = request.body.created_at = moment.utc().format("YYYY-MM-DD HH:mm:ss");

	next();

};