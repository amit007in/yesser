var stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

/////////////////////		Create Stripe Customer 		///////////////////////////////
exports.createCustomer = async (data) => {

	return await stripe.customers.create({

		description: `Customer for ${data.email}`,
		email: data.email,
		metadata: {
			"phone_code": data.phone_code, 
			"phone_number": data.phone_number 
		},
		source: "tok_visa"
		// source: {
	 //        object: 'card',
	 //        exp_month: 12,
	 //        exp_year: 2024,
	 //        number: "4242424242424242",
	 //        cvc: 23
		//   	}

	});

};

////////////////		Make Payment 		/////////////////////////////////////////////
exports.namePayment = async (order) => {

	return await stripe.charges.create({
	
		  	amount: order.final_charge*100,
		  	currency: "usd",
		  	customer: order.Customer.stripe_customer_id,
		  	description: "Charge for Order - "+order.order_id,
		  	metadata: {
		  		order_id: order.order_id,
		  		customer_user_id: order.customer_user_id,
		  		customer_user_detail_id: order.customer_user_detail_id,
		  		customer_organisation_id: order.customer_organisation_id,
		  		user_card_id: order.user_card_id,
		  		driver_user_id: order.driver_user_id,
		  		driver_user_detail_id: order.driver_user_detail_id,
		  		driver_organisation_id: order.driver_organisation_id,
		  		driver_user_type_id: order.driver_user_type_id,
		  		category_id: order.category_id,
		  		category_brand_id: order.category_brand_id,
		  		category_brand_product_id: order.category_brand_product_id
		  	}
		
	});

};

/////////////////		Token COupon Payment 	/////////////////////////////////////////
exports.CouponPayment = async (data) => {

	return await stripe.charges.create({	
		  	amount: data.final_charge*100,
		  	currency: "usd",
		  	customer: data.Details.user.stripe_customer_id,
		  	description: data.message,
		  	metadata: data.metadata
		
	});

	// return await stripe.charges.create({
	// 		  	amount: data.final_charge*100,
	// 		  	currency: "usd",
	// 		  	customer: data.Details.user.stripe_customer_id,
	// 		  	description: data.message,
	// 		  	metadata: data.metadata
	// 		}, function(err, charge) {

	// 			if(err)
	// 				console.log(err.message);
	// 			else
	// 				return charge;

	// 		  // asynchronously called
	// });


};

///////////////////////		Make Payment 		//////////////////////////////////////////
exports.makePayment = async (charge, description, metadata, customer) => {

	return await stripe.charges.create({	
		  	amount: charge*100,
		  	currency: "usd",
		  	customer,
		  	description,
		  	metadata		
	});

};

/////////////////////////