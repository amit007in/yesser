"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var OrganisationAreaDrivers = sequelize.define("organisation_area_drivers", 
		{

			organisation_area_driver_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			organisation_area_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	OrganisationAreaDrivers.associate = function(models) {

		OrganisationAreaDrivers.belongsTo(models.organisation_areas, { as: "org_area", foreignKey: "organisation_area_id", sourceKey: "organisation_area_id", onDelete: "cascade" });

		OrganisationAreaDrivers.belongsTo(models.user_details, { as: "driver", foreignKey: "user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });

		OrganisationAreaDrivers.belongsTo(models.organisations, { as: "org", foreignKey: "organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });

	};

	return OrganisationAreaDrivers;
};
