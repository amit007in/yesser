"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    var PromoUser = sequelize.define("promo_users",
            {
                id: {type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true},
                coupon_id: {type: Sequelize.INTEGER, allowNull: true},
                user_id: {type: Sequelize.INTEGER, allowNull: true},
                category_id: {type: Sequelize.INTEGER, allowNull: true},
                brand_id: {type: Sequelize.INTEGER, allowNull: true},
                created_at: {type: DataTypes.STRING(255), allowNull: false},
                updated_at: {type: DataTypes.STRING(255), allowNull: false}
            },
            {
                underscored: true,
                timestamps: false
            }
    );

    PromoUser.associate = function (models) {
       PromoUser.belongsTo(models.coupons, {foreignKey: "coupon_id", sourceKey: "coupon_id", onDelete: "cascade"});
    };

    return PromoUser;
};
