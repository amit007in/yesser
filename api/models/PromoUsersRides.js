"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var Promo_users_rides = sequelize.define("promo_users_rides", 
		{
			id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },
			
                        coupon_id: { type: Sequelize.INTEGER },
                        user_id: { type: Sequelize.INTEGER },
                        max_rides: { type: Sequelize.INTEGER }
		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	

	return Promo_users_rides;
};
