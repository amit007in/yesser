"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    var PromocodeProducts = sequelize.define("promocode_products",
            {
                coupon_id: {type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true},
                product_id: {type: Sequelize.INTEGER},
                category_id: {type: Sequelize.INTEGER},
                brand_id: {type: Sequelize.INTEGER},
                created_at: {type: DataTypes.STRING(30), allowNull: false},
                updated_at: {type: DataTypes.STRING(30), allowNull: false}
            },
            {
                underscored: true,
                timestamps: false
            }
    );

    PromocodeProducts.associate = function (models) {
       PromocodeProducts.belongsTo(models.coupons, {foreignKey: "coupon_id", sourceKey: "coupon_id", onDelete: "cascade"});
       PromocodeProducts.belongsTo(models.category_brand_product_details, {foreignKey: "product_id", sourceKey: "category_brand_product_id", onDelete: "cascade"});
    };

    return PromocodeProducts;
};
