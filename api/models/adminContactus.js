var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var AdminContactus = sequelize.define("admin_contactus", 
		{
			admin_contactus_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			user_type_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			parent_admin_contactus_id: {
				type: Sequelize.BIGINT,
				allowNull: false
			},

			message: { type: DataTypes.TEXT},

			is_read: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },
			
			//admin_reply: { type: DataTypes.TEXT},

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false },

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	AdminContactus.associate = function(models) {

		AdminContactus.belongsTo(models.users, { as: "User", foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });

		AdminContactus.belongsTo(models.user_details, { as: "UserDetail", foreignKey: "user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });

		AdminContactus.belongsTo(models.user_types, { as: "UserType", foreignKey: "user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });

	};

	return AdminContactus;
};
