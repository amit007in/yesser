"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var adminUserDetailLogins = sequelize.define("admin_user_detail_logins", 
		{
			admin_user_detail_login_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			admin_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			user_type_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			access_token: { type: DataTypes.STRING(100), allowNull: false },

			timezone: { type: DataTypes.STRING(100), allowNull: false, defaultValue: "Asia/Muscat" },
			timezonez: { type: DataTypes.STRING(10), allowNull: false, defaultValue: "+04:00" },

			ip_address: { type: DataTypes.STRING(50), allowNull: false },
			otp: { type: DataTypes.STRING(4), allowNull: false },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false },

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	adminUserDetailLogins.associate = function(models) {

	};

	return adminUserDetailLogins;
};
