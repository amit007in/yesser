"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var Admin = sequelize.define("admins", 
		{
			admin_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			name: { type: DataTypes.STRING(255), allowNull: false },
			job: { type: DataTypes.STRING(255), allowNull: false },

			email: { type: DataTypes.STRING(255), allowNull: false },
			password: { type: DataTypes.TEXT },

			forgot_token: { type: DataTypes.STRING(100), allowNull: false },
			forgot_token_validity: { type: DataTypes.DATE, allowNull: false },

			profile_pic: { type: DataTypes.STRING(100), allowNull: false },

			//timezone: { type: DataTypes.STRING(100), allowNull: false, defaultValue: "Asia/Kolkata" },
			//timezonez: { type: DataTypes.STRING(10), allowNull: false, defaultValue: "+05:30" },
 
			service_maximum_dist: { type: Sequelize.BIGINT },
			support_maximum_dist: { type: Sequelize.BIGINT },
			buraq_percentage: { type: DataTypes.FLOAT(5,2), allowNull: false, validate: { min: 0, max: 100 }, defaultValue: 0.0 },
			round_robin: {type: DataTypes.STRING(255), allowNull: false,	defaultValue: ""},
			round_robin_time: {type: DataTypes.INTEGER(11), allowNull: false, defaultValue: 30},
			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false },

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	Admin.associate = function(models) {

	};

	return Admin;
};
