"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var Category = sequelize.define("categories", 
		{
			category_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			category_type: { type: DataTypes.STRING(10), allowNull: false, defaultValue: "Support" },
			
			buraq_percentage: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 10.00 },
			sort_order: { type: Sequelize.BIGINT },

			image: { type: DataTypes.STRING(100), allowNull: false, defaultValue: "" },
			maximum_distance: { type: DataTypes.STRING(5), allowNull: false, defaultValue: "5" },

			blocked: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },
			default_brands: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			maximum_rides: { type: Sequelize.BIGINT, defaultValue: 10 },
			interval_time: { type: DataTypes.STRING(255), allowNull: false, defaultValue: "" },
			transit_offer_percentage: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
			transit_buraq_margin: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
			driver_quotation_timeout: { type: DataTypes.STRING(255), allowNull: false, defaultValue: "" },
			geofencing_status: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0}, //Add M3
			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false },

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	Category.associate = function(models) {

		Category.hasOne(models.category_details, { as: "Details", foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });
		Category.hasMany(models.category_details, { foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });

		Category.hasOne(models.category_brands, { as: "Brand", foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });
		Category.hasMany(models.category_brands, { as: "Brands", foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });

		Category.hasOne(models.category_brand_details, { as: "BrandDetail", foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });
		Category.hasMany(models.category_brand_details, { as: "BrandDetails", foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });

		Category.hasMany(models.category_brand_products, { foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });

		Category.hasMany(models.category_brand_product_details, { foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });
		Category.hasMany(models.user_driver_detail_services, { foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });

		Category.hasMany(models.orders, { as: "Orders", foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });

		Category.hasMany(models.organisation_categories, { as: "OrgCategory", foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });

		Category.hasMany(models.organisation_coupons, { as: "OrgCoupon", foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });

	};

	return Category;
};
