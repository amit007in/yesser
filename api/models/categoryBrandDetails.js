"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var categoryBrandDetails = sequelize.define("category_brand_details", 
		{
			category_brand_detail_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			category_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},

			language_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			category_brand_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			name: { type: DataTypes.STRING(255), allowNull: false },
			image: { type: DataTypes.STRING(100), allowNull: false },
			description: { type: DataTypes.TEXT },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false },

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	categoryBrandDetails.associate = function(models) {

		categoryBrandDetails.belongsTo(models.categories, { foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });
		
		categoryBrandDetails.belongsTo(models.languages, { foreignKey: "language_id", sourceKey: "language_id", onDelete: "cascade" });

		categoryBrandDetails.belongsTo(models.category_brands, { foreignKey: "category_brand_id", sourceKey: "category_brand_id", onDelete: "cascade" });


	};

	return categoryBrandDetails;
};
