"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var categoryBrandProductDetails = sequelize.define("category_brand_product_details", 
		{
			category_brand_product_detail_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			///////////		Category 	/////////////////
			category_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},
			category_brand_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			category_brand_product_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			///////////		Category 	/////////////////

			language_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			name: { type: DataTypes.STRING(255), allowNull: false },
			description: { type: DataTypes.TEXT },
			gift_offer: { type: DataTypes.STRING(255), allowNull: false },
			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false },

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	categoryBrandProductDetails.associate = function(models) {

		///////////		Category 	/////////////////
		categoryBrandProductDetails.belongsTo(models.categories, { foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });
		categoryBrandProductDetails.belongsTo(models.category_brands, { foreignKey: "category_brand_id", sourceKey: "category_brand_id", onDelete: "cascade" });
		categoryBrandProductDetails.belongsTo(models.category_brand_products, { foreignKey: "category_brand_product_id", sourceKey: "category_brand_product_id", onDelete: "cascade" });
		///////////		Category 	/////////////////

		categoryBrandProductDetails.belongsTo(models.languages, { foreignKey: "language_id", sourceKey: "language_id", onDelete: "cascade" });
		
		categoryBrandProductDetails.hasMany(models.organisation_coupon_users, { foreignKey: "category_brand_product_id", sourceKey: "category_brand_product_id", onDelete: "cascade" });


	};

	return categoryBrandProductDetails;
};
