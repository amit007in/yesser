"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var CategoryBPGeofencing = sequelize.define("category_brand_product_geofencing_prices", 
		{
			id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
            category_id: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
            category_brand_id: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
            category_brand_product_id 	: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
            latitude: { type: DataTypes.FLOAT(9,6), allowNull: false, defaultValue: 0.0 },
			longitude: { type: DataTypes.FLOAT(9,6), allowNull: false, defaultValue: 0.0 },
            radius: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
            area_name: {type: DataTypes.STRING(255), allowNull: false ,defaultValue: ""},
            price: { type: DataTypes.FLOAT(9,6), allowNull: false, defaultValue: 0.0 },
			created_at: {type: 'TIMESTAMP',defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),allowNull: false},
			updated_at: {type: 'TIMESTAMP',defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),allowNull: false}
		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	return CategoryBPGeofencing;
};