"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var categoryBrandProducts = sequelize.define("category_brand_products", 
		{
			category_brand_product_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			///////////		Category 	/////////////////
			category_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},
			category_brand_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			///////////		Category 	/////////////////

			category_sub_type: { type: DataTypes.STRING(10), allowNull: false, defaultValue: "Quantity" },//'Quantity or Brand or Company'
			si_unit: { type: DataTypes.STRING(10), allowNull: false },

			actual_value: { type: DataTypes.DOUBLE(10,2), allowNull: false },
			alpha_price: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },
			price_per_quantity: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },
			price_per_distance: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },
			price_per_weight: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },
			price_per_hr: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },
			price_per_sq_mt: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },

			sort_order: { type: Sequelize.BIGINT },

			multiple: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },
			blocked: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			image: { type: DataTypes.STRING(100), allowNull: false },

			min_quantity: { type: Sequelize.BIGINT, defaultValue: 1 },
			max_quantity: { type: Sequelize.BIGINT, defaultValue: 100 },
			
			gift_quantity : { type: Sequelize.BIGINT, allowNull: false, defaultValue: 0 },
			gift_offer: { type: DataTypes.STRING(255), allowNull: false },
			product_margin: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 }, //Add M3
			geofencing_status: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0}, //Add M3
			should_use_buraq_margin: {type: DataTypes.INTEGER(11), allowNull: false, defaultValue: 1}, //Add M3
			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false },

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	categoryBrandProducts.associate = function(models) {

		categoryBrandProducts.belongsTo(models.categories, { foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });
		categoryBrandProducts.belongsTo(models.category_brands, { foreignKey: "category_brand_id", sourceKey: "category_brand_id", onDelete: "cascade" });

		categoryBrandProducts.hasOne(models.category_brand_product_details, { as: "Details", foreignKey: "category_brand_product_id", sourceKey: "category_brand_product_id", onDelete: "cascade" });
		categoryBrandProducts.hasMany(models.category_brand_product_details, { foreignKey: "category_brand_product_id", sourceKey: "category_brand_product_id", onDelete: "cascade" });

		categoryBrandProducts.hasMany(models.user_driver_detail_products, { foreignKey: "category_brand_product_id", sourceKey: "category_brand_product_id", onDelete: "cascade" });

		categoryBrandProducts.hasMany(models.orders, { as: "Orders", foreignKey: "category_brand_product_id", sourceKey: "category_brand_product_id", onDelete: "cascade" });

		categoryBrandProducts.hasMany(models.organisation_coupons, { as: "OrgCoupon", foreignKey: "category_brand_product_id", sourceKey: "category_brand_product_id", onDelete: "cascade" });

	};

	return categoryBrandProducts;
};
