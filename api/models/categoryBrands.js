"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var CategoryBrands = sequelize.define("category_brands", 
		{
			category_brand_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			category_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},

			sort_order: { type: Sequelize.BIGINT },
			buraq_percentage: { type: DataTypes.DOUBLE(5,2), allowNull: false, defaultValue: 10.00 },

			category_brand_type: { type: DataTypes.STRING(10), allowNull: false },//'Either its Brand or Model or Make Etc depending on category type'

			blocked: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },
			maximum_radius: { type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0}, //Add M2 Part 2
			is_default: { type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 1}, //Add M2 Part 2
			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false },

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	CategoryBrands.associate = function(models) {

		CategoryBrands.belongsTo(models.categories, { foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });

		CategoryBrands.hasOne(models.category_brand_details, { as: "Details", foreignKey: "category_brand_id", sourceKey: "category_brand_id", onDelete: "cascade" });
		CategoryBrands.hasMany(models.category_brand_details, { foreignKey: "category_brand_id", sourceKey: "category_brand_id", onDelete: "cascade" });

		CategoryBrands.hasMany(models.category_brand_products, { as: "Products", foreignKey: "category_brand_id", sourceKey: "category_brand_id", onDelete: "cascade" });
		CategoryBrands.hasOne(models.category_brand_products, { as: "Product", foreignKey: "category_brand_id", sourceKey: "category_brand_id", onDelete: "cascade" });

		CategoryBrands.hasMany(models.category_brand_product_details, { foreignKey: "category_brand_id", sourceKey: "category_brand_id", onDelete: "cascade" });

		CategoryBrands.hasMany(models.orders, { as: "Orders", foreignKey: "category_brand_id", sourceKey: "category_brand_id", onDelete: "cascade" });

		CategoryBrands.hasMany(models.organisation_coupons, { as: "OrgCoupon", foreignKey: "category_brand_id", sourceKey: "category_brand_id", onDelete: "cascade" });

	};

	return CategoryBrands;
};
