"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var categoryDetail = sequelize.define("category_details", 
		{
			category_detail_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			category_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},

			language_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			name: { type: DataTypes.STRING(255), allowNull: false },
			description: { type: DataTypes.TEXT },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false },

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	categoryDetail.associate = function(models) {

		categoryDetail.belongsTo(models.categories, { foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });
		
		categoryDetail.belongsTo(models.languages, { foreignKey: "language_id", sourceKey: "language_id", onDelete: "cascade" });

		categoryDetail.hasMany(models.category_brands, { as: "Brand", foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });
		categoryDetail.hasMany(models.category_brand_details, { as: "brands", foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });

	};

	return categoryDetail;
};
