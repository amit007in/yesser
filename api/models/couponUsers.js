"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var CouponUsers = sequelize.define("coupon_users", 
		{
			coupon_user_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			coupon_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			///////////		Customer 	/////////////////
			user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			user_type_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			///////////		Customer 	/////////////////

			payment_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			rides_value: { type: DataTypes.BIGINT, allowNull: false },
			coupon_value: { type: DataTypes.STRING(20), allowNull: false },

			rides_left: { type: DataTypes.BIGINT, allowNull: false },

			price: { type: DataTypes.FLOAT(10,2), allowNull: false },
			coupon_type: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "Percentage" },

			deleted: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },
			blocked: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false },
			expires_at: { type: DataTypes.STRING(30), allowNull: false }
		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	CouponUsers.associate = function(models) {

		//		CouponUsers.belongsTo(models.coupons, { foreignKey: "coupon_id", sourceKey: "coupon_id", onDelete: "cascade" });

		///////////		Customer 	/////////////////
		CouponUsers.belongsTo(models.users, { foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });
		CouponUsers.belongsTo(models.user_details, { foreignKey: "user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		CouponUsers.belongsTo(models.user_types, { foreignKey: "user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });
		///////////		Customer 	/////////////////

		//		CouponUsers.belongsTo(models.payments, { foreignKey: "payment_id", sourceKey: "payment_id", onDelete: "cascade" });
		// CouponUsers.hasMany(models.orders, { foreignKey: "coupon_user_id", sourceKey: "coupon_user_id", onDelete: "cascade" });

	};

	return CouponUsers;
};
