"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var Coupons = sequelize.define("coupons", 
		{
			coupon_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },
			code: { type: DataTypes.STRING(30), allowNull: false },
			amount_value: { type: DataTypes.STRING(20), allowNull: true },
			rides_value: { type: DataTypes.BIGINT, allowNull: false },
			coupon_type: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "Percentage" },
			expiry_days: { type: Sequelize.INTEGER },
			price: { type: DataTypes.FLOAT(10,2), allowNull: false },
			blocked: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },
			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false },
			expires_at: { type: DataTypes.STRING(30), allowNull: false },
                        title : { type: DataTypes.STRING(225), allowNull: true },
                        category_id: { type: Sequelize.INTEGER },
                        description: { type: DataTypes.STRING(255), allowNull: true },
                        brand_id: { type: Sequelize.INTEGER },
                        product_id: { type: Sequelize.INTEGER },
                        users: { type: DataTypes.STRING(500), allowNull: true},
                        start_at: { type: DataTypes.STRING(255), allowNull: true},
                        image: { type: DataTypes.STRING(255), allowNull: true},
						all_users_status: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0}
		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	Coupons.associate = function(models) {

		Coupons.hasMany(models.coupon_users, { foreignKey: "coupon_id", sourceKey: "coupon_id", onDelete: "cascade" });
                Coupons.hasMany(models.category_details, { foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });
                Coupons.hasMany(models.category_brand_details, { foreignKey: "category_brand_id", sourceKey: "brand_id", onDelete: "cascade" });
                Coupons.hasMany(models.category_brand_product_details, {as : "products", foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });
                Coupons.hasMany(models.promocode_products, {as: "promocode_products", foreignKey: "coupon_id", sourceKey: "coupon_id", onDelete: "cascade"});
                Coupons.hasMany(models.promo_users, {as: "promo_users", foreignKey: "coupon_id", sourceKey: "coupon_id", onDelete: "cascade"});
                Coupons.hasMany(models.promo_users_rides, { foreignKey: "coupon_id", sourceKey: "coupon_id", onDelete: "cascade" });
               
// Coupons.belongsTo(models.promo_users_rides, {as : "promo_users_rides", foreignKey: "coupon_id", sourceKey: "coupon_id", onDelete: "cascade" });
		//Coupons.hasMany(models.payments, { foreignKey: "coupon_id", sourceKey: "coupon_id", onDelete: "cascade" });

		// Coupons.belongsTo(models.users, { foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });
		// Coupons.belongsTo(models.user_details, { foreignKey: "user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		// Coupons.belongsTo(models.user_types, { foreignKey: "user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });

		// //Coupons.hasMany(models.coupons, { foreignKey: "coupon_id", sourceKey: "coupon_id", onDelete: "cascade" });
		// Coupons.hasMany(models.organisation_coupons, { foreignKey: "organisation_coupon_id", sourceKey: "organisation_coupon_id", onDelete: "cascade" });
		

	};

	return Coupons;
};
