"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var EmergencyContactDetails = sequelize.define("emergency_contact_details", 
		{

			emergency_contact_detail_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			emergency_contact_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			language_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			name: { type: DataTypes.STRING(255), allowNull: false },
			image: { type: DataTypes.STRING(100), allowNull: false },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	EmergencyContactDetails.associate = function(models) {

		EmergencyContactDetails.belongsTo(models.languages, { foreignKey: "language_id", sourceKey: "language_id", onDelete: "cascade" });

		EmergencyContactDetails.belongsTo(models.emergency_contacts, { as: "emergency_contacts", foreignKey: "emergency_contact_id", sourceKey: "emergency_contact_id", onDelete: "cascade" });
		

	};

	return EmergencyContactDetails;
};
