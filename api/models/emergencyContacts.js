"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var EmergencyContacts = sequelize.define("emergency_contacts", 
		{

			emergency_contact_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			sort_order: { type: Sequelize.BIGINT,  allowNull: false	},

			phone_number: { type: DataTypes.BIGINT, allowNull: false },

			blocked: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	EmergencyContacts.associate = function(models) {
		
		EmergencyContacts.hasMany(models.emergency_contact_details, { as: "emergency_contact_details", foreignKey: "emergency_contact_id", sourceKey: "emergency_contact_id", onDelete: "cascade" });

	};

	return EmergencyContacts;
};
