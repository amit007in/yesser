"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var FloorMargin = sequelize.define("floor_margins", 
		{
			floormargin_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
			price: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },
			status: { type: DataTypes.INTEGER(11), allowNull: false, defaultValue: 0 },
			floor_name: { type: DataTypes.INTEGER(11), allowNull: false, defaultValue: 0 },
			all_floor_price_status: { type: DataTypes.INTEGER(11), allowNull: false, defaultValue: 1 },
			created_at: {type: 'TIMESTAMP',defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),allowNull: false},
			updated_at: {type: 'TIMESTAMP',defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),allowNull: false}
		},
		{
			underscored: true,
			timestamps: false
		}
	);
		return FloorMargin;
};

	