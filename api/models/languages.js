"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var Language = sequelize.define("languages", 
		{
			language_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			language_name: { type: DataTypes.STRING(100), allowNull: false },
			language_code: { type: DataTypes.STRING(2), allowNull: false },

			display_for: { type: DataTypes.STRING(3), allowNull: false },

			sort_order: { type: DataTypes.BIGINT, allowNull: false },

			blocked: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },
			is_default: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			created_at: { type: DataTypes.DATE, allowNull: false, defaultValue: sequelize.literal("CURRENT_TIMESTAMP") },
			updated_at: { type: DataTypes.DATE, allowNull: false, defaultValue: sequelize.literal("CURRENT_TIMESTAMP") }

		},
		{
		    underscored: true,
		    //timestamps: false
		}
	);

	Language.associate = function(models) {

		Language.hasMany(models.category_details, { foreignKey: "language_id", sourceKey: "language_id", onDelete: "cascade" });
		Language.hasMany(models.category_brand_details, { foreignKey: "language_id", sourceKey: "language_id", onDelete: "cascade" });
		Language.hasMany(models.category_brand_product_details, { foreignKey: "language_id", sourceKey: "language_id", onDelete: "cascade" });

		Language.hasMany(models.emergency_contact_details, { foreignKey: "language_id", sourceKey: "language_id", onDelete: "cascade" });

		Language.hasMany(models.organisations, { foreignKey: "language_id", sourceKey: "language_id", onDelete: "cascade" });

		Language.hasMany(models.user_details, { foreignKey: "language_id", sourceKey: "language_id", onDelete: "cascade" });

	};

	return Language;
};
