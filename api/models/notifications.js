"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var notification = sequelize.define("notifications", 
		{
			notification_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
                        title: {type: DataTypes.STRING(255), allowNull: true, defaultValue: ""},
                        message: {type: DataTypes.STRING(255), allowNull: true, defaultValue: ""},
                        type: {type: DataTypes.STRING(255), allowNull: true, defaultValue: ""},
                        broadcastDate: {type: DataTypes.DATE(), allowNull: true, defaultValue: ""},
                        status: {type: DataTypes.STRING(255), allowNull: true, defaultValue: ""},
                        recurring: {type: DataTypes.STRING(255), allowNull: true, defaultValue: ""},
                        media_type: {type: DataTypes.STRING(255), allowNull: true, defaultValue: "image"},
                        image: {type: DataTypes.STRING(255), allowNull: true, defaultValue: ""},
                        videothumb: {type: DataTypes.STRING(255), allowNull: true, defaultValue: ""},
                        video: {type: DataTypes.STRING(255), allowNull: true, defaultValue: ""},
                        coupon_id: {type: Sequelize.INTEGER},
                        service_id: {type: Sequelize.INTEGER,allowNull: true, defaultValue: "0"},
                        created_at: {type: DataTypes.DATE(), allowNull: true, defaultValue: ""},
                        updated_at: {type: DataTypes.DATE(), allowNull: true, defaultValue: ""}
		},
		{
		    underscored: true,
		    timestamps: false
		}
	);
        notification.associate = function(models) {

		notification.belongsTo(models.coupons, { as: "promocode", foreignKey: "coupon_id", sourceKey: "coupon_id", onDelete: "cascade" });

	};

	return notification;
};
