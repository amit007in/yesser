"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var OrderFailures = sequelize.define("order_failures", 
		{
			failure_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
                        message: {type: DataTypes.STRING(255), allowNull: true, defaultValue: ""},
                        products: {type: DataTypes.STRING(500), allowNull: true, defaultValue: ""},
                        customer_id: {type: DataTypes.INTEGER(11), allowNull: false ,defaultValue: ""},
                        customer_name: {type: DataTypes.STRING(255), allowNull: true,defaultValue: ""},
			order_date: { type: DataTypes.STRING(500), allowNull: true ,defaultValue: ""},
                        sms_object: { type: DataTypes.STRING(500), allowNull: true ,defaultValue: ""}
		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	OrderFailures.associate = function(models) {
                OrderFailures.belongsTo(models.users, { as: "User", foreignKey: "user_id", sourceKey: "customer_id", onDelete: "cascade" });
	};

	return OrderFailures;
};
