"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var orderImages = sequelize.define("order_images", 
		{
			order_image_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			order_id: {	type: Sequelize.BIGINT, allowNull: false },

			image: { type: DataTypes.STRING(100), allowNull: false },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	orderImages.associate = function(models) {

		orderImages.belongsTo(models.orders, { foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });

	};

	return orderImages;
};
