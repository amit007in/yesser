"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var OrderNoncontinuesDates = sequelize.define("order_noncontinues_dates", 
		{
			id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
            order_id: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
            schudled_date: { type: DataTypes.STRING(255), allowNull: false, defaultValue: "" },
            scheduled_token: { type: DataTypes.STRING(255), allowNull: false, defaultValue: "" },
			created_at: {type: 'TIMESTAMP',defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),allowNull: false}
		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	return OrderNoncontinuesDates;
};