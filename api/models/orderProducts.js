"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var orderProducts = sequelize.define("order_products", 
		{
			order_product_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },
			order_id: { type: Sequelize.BIGINT, allowNull: false },
                        category_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},
			category_brand_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			category_brand_product_id: {
				type: Sequelize.BIGINT, 
				allowNull: true
			},
			product_weight: {
				type: DataTypes.STRING(255), 
				allowNull: false
			},
			price_per_item: {
				type: DataTypes.STRING(255), 
				allowNull: true
			},
			image_url: {
				type: DataTypes.STRING(255), 
				allowNull: false
			},
			productName: {
				type: DataTypes.STRING(255), 
				allowNull: false
			},
			product_quantity: {
				type: DataTypes.STRING(255), 
				allowNull: false
			},
			gift_offer: {
				type: DataTypes.STRING(255), 
				allowNull: false,
				defaultValue: ""
			},
			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	orderProducts.associate = function(models) {
		orderProducts.belongsTo(models.orders, { foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });
	};
	return orderProducts;
};
