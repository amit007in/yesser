"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var OrderRatings = sequelize.define("order_ratings", 
		{
			order_rating_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			order_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			//////////		Customer 	///////////////////
			customer_user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			customer_user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			customer_organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			//////////		Customer 	///////////////////

			//////////		Driver 	///////////////////////
			driver_user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			driver_user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			driver_organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			//////////		Driver 	///////////////////////

			ratings: { type: DataTypes.SMALLINT, allowNull: false, defaultValue: 5 },
			comments: { type: DataTypes.TEXT },

			created_by: { type: DataTypes.STRING(10), allowNull: false, defaultValue: "Customer" },

			deleted: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },
			blocked: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }
		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	OrderRatings.associate = function(models) {

		OrderRatings.belongsTo(models.orders, { as: "Order", foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });

		OrderRatings.belongsTo(models.users, {as: "CustomerUser", foreignKey: "customer_user_id", sourceKey: "user_id", onDelete: "cascade" });
		OrderRatings.belongsTo(models.user_details, {as: "CustomerUserDetails", foreignKey: "customer_user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		OrderRatings.belongsTo(models.organisations, {as: "CustomerOrg", foreignKey: "customer_organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });

		OrderRatings.belongsTo(models.users, {as: "Driver", foreignKey: "driver_user_id", sourceKey: "user_id", onDelete: "cascade" });
		OrderRatings.belongsTo(models.user_details, {as: "DriverDetails", foreignKey: "driver_user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		OrderRatings.belongsTo(models.organisations, {as: "DriverOrg", foreignKey: "driver_organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });

	};

	return OrderRatings;
};
