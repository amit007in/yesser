"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var OrderRequests = sequelize.define("order_requests", 
		{
			order_request_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			order_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			///////////		Driver 	/////////////////////
			driver_user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			driver_user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			driver_organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			///////////		Driver 	/////////////////////

			order_request_status: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "Seaching" },

			driver_request_latitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0 },
			driver_request_longitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0 },

			driver_current_latitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0 },
			driver_current_longitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0 },

			accepted_at: { type: DataTypes.STRING(30), allowNull: true, defaultValue: null },

			rotation: { type: DataTypes.STRING(10), allowNull: false, defaultValue: "0" },

			driver_confirmed_latitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0 },
			driver_confirmed_longitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0 },
			confirmed_at: { type: DataTypes.STRING(30), allowNull: true, defaultValue: null },
			started_at: { type: DataTypes.STRING(30), allowNull: true, defaultValue: null },

			driver_started_latitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0 },
			driver_started_longitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0 },

			full_track: { type: DataTypes.TEXT },
			distance_customer: { type: DataTypes.STRING(255), allowNull: false, defaultValue: "" },//Add M3 Part 3.2
			driver_request_status: {type: DataTypes.INTEGER(11), allowNull: false, defaultValue: 0},//Add M3 Part 3.2
			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	OrderRequests.associate = function(models) {

		OrderRequests.belongsTo(models.orders, { as: "Order", foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });

		///////////		Driver 	/////////////////////
		OrderRequests.belongsTo(models.users, {as: "Driver", foreignKey: "driver_user_id", sourceKey: "user_id", onDelete: "cascade" });
		OrderRequests.belongsTo(models.user_details, {as: "DriverDetails", foreignKey: "driver_user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		OrderRequests.belongsTo(models.organisations, {as: "DriverOrg", foreignKey: "driver_organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });
		///////////		Driver 	/////////////////////


	};

	return OrderRequests;
};
