"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var Orders = sequelize.define("orders", 
		{
			order_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			order_token: { type: DataTypes.STRING(50), allowNull: false, unique: true },

			/////////		Customer 	/////////////////
			customer_user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			customer_user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			customer_organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			customer_user_type_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			/////////		Customer 	/////////////////

			///////////		Driver 	/////////////////////
			driver_user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			driver_user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			driver_organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			driver_user_type_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			///////////		Driver 	/////////////////////

			///////////		Category 	/////////////////
			category_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			category_brand_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			category_brand_product_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			///////////		Category 	/////////////////

			continuous_order_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},

			order_status: { type: DataTypes.STRING(20), allowNull: false },
			//payment_status: { type: DataTypes.STRING(20), allowNull: false },
			//refund_status: { type: DataTypes.STRING(20), allowNull: false },

			pickup_address: { type: DataTypes.STRING(500), allowNull: false, defaultValue: "" },
			pickup_latitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0 },
			pickup_longitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0 },

			dropoff_address: { type: DataTypes.STRING(500), allowNull: false },
			dropoff_latitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0 },
			dropoff_longitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0 },

			order_type: { type: DataTypes.STRING(10), allowNull: true },
			created_by: { type: DataTypes.STRING(10), allowNull: false, defaultValue: "Customer" },

			order_timings: { type: DataTypes.STRING(30), allowNull: false },
			//{ type: DataTypes.DATE, allowNull: false },
			future: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			continouos_startdt: { type: DataTypes.STRING(30), allowNull: true, defaultValue: null },
			continuous_enddt: { type: DataTypes.STRING(30), allowNull: true, defaultValue: null },
			continuous_time: { type: DataTypes.TIME, allowNull: true, defaultValue: null },

			continuous: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			cancel_reason: { type: DataTypes.TEXT },
			cancelled_by: { type: DataTypes.STRING(10), allowNull: false },

			my_turn: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			organisation_coupon_user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},
			coupon_user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},
 
			track_path: { type: Sequelize.TEXT, allowNull: true, defaultValue: null	},
			track_image: { type: DataTypes.STRING(100), allowNull: true, defaultValue: null },
			details: { type: Sequelize.TEXT, allowNull: true, defaultValue: null },
			material_details: { type: Sequelize.TEXT, allowNull: true, defaultValue: null },
			quotation_price: { type: DataTypes.STRING(255), allowNull: false, defaultValue: "" }, 
			quotation_text: { type: DataTypes.STRING(255), allowNull: false, defaultValue: "" }, 
			quotation_file: { type: DataTypes.STRING(255), allowNull: false, defaultValue: "" },
			release_offer_id: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0}, 
			quotation_id: { type: DataTypes.STRING(255), allowNull: false, defaultValue: "" }, 	
			reward_discount_price: {type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0.0},
			floor_charge: {type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0.0},
			reward_discount_point: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
			schulderd_order_token: { type: DataTypes.STRING(255), allowNull: false, defaultValue: "" },
			request_type: { type: DataTypes.STRING(255), allowNull: false, defaultValue: "" },
			order_request_timeout: {type: DataTypes.INTEGER(11), allowNull: false, defaultValue: 47},
			total_request_driver: {type: DataTypes.INTEGER(11), allowNull: false, defaultValue: 0},
			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false },
			//{ type: DataTypes.DATE, allowNull: false, defaultValue: sequelize.literal("CURRENT_TIMESTAMP") }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	Orders.associate = function(models) {

		///////////////			Customer 		////////////////
		Orders.belongsTo(models.users, {as: "Customer", foreignKey: "customer_user_id", sourceKey: "user_id", onDelete: "cascade" });
		Orders.belongsTo(models.user_details, {as: "CustomerDetails", foreignKey: "customer_user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		Orders.belongsTo(models.organisations, {as: "CustomerOrg", foreignKey: "customer_organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });
		Orders.belongsTo(models.user_types, {as: "CustomerType", foreignKey: "customer_user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });
		///////////////			Customer 		////////////////

		///////////////			Driver 		////////////////////
		Orders.belongsTo(models.users, {as: "Driver", foreignKey: "driver_user_id", sourceKey: "user_id", onDelete: "cascade" });
		Orders.belongsTo(models.user_details, {as: "DriverDetails", foreignKey: "driver_user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		Orders.belongsTo(models.organisations, {as: "DriverOrg", foreignKey: "driver_organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });
		Orders.belongsTo(models.user_types, {as: "DriverType", foreignKey: "driver_user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });
		///////////////			Driver 		////////////////////

		///////////////			Categories 	////////////////////
		Orders.belongsTo(models.categories, { as: "Category", foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });
		Orders.belongsTo(models.category_brands, { as: "CategoryBrand", foreignKey: "category_brand_id", sourceKey: "category_brand_id", onDelete: "cascade" });
		Orders.belongsTo(models.category_brand_products, { foreignKey: "category_brand_product_id", sourceKey: "category_brand_product_id", onDelete: "cascade" });

		Orders.belongsTo(models.category_brand_product_details, { as: "CategoryBrandProduct", foreignKey: "category_brand_product_id", sourceKey: "category_brand_product_id", onDelete: "cascade" });
		///////////////			Categories 	////////////////////

		Orders.belongsTo(models.organisation_coupon_users, { foreignKey: "organisation_coupon_user_id", sourceKey: "organisation_coupon_user_id", onDelete: "cascade" });
		Orders.belongsTo(models.coupon_users, { foreignKey: "coupon_user_id", sourceKey: "coupon_user_id", onDelete: "cascade" });

		///////////////		Driver Requests 	//////////////////
		Orders.hasOne(models.order_requests, { as: "CRequest", foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });
		Orders.hasOne(models.order_requests, { as: "cRequest", foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });
		Orders.hasMany(models.order_requests, { as: "ORequests", foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });
		///////////////		Driver Requests 	//////////////////

		Orders.hasOne(models.payments, { /*as: 'Payment',*/ foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });

		///////////////			Ratings 	////////////////////
		Orders.hasOne(models.order_ratings, { as: "CustRatings", foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });
		Orders.hasOne(models.order_ratings, { as: "DriverRatings", foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });

		Orders.hasOne(models.order_ratings, { as: "ratingByUser", foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });
		Orders.hasOne(models.order_ratings, { as: "ratingByDriver", foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });
		///////////////			Ratings 	////////////////////

		Orders.hasMany(models.order_images, { foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });
		Orders.hasMany(models.order_images, { as: "order_images_url", foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });
                
                Orders.hasMany(models.order_products, {as: "OrderProducts", foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });
	};

	return Orders;
};
