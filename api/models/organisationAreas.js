"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var OrganisationAreas = sequelize.define("organisation_areas", 
		{
			organisation_area_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			category_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			label: { type: DataTypes.STRING(500), allowNull: false },

			address: { type: DataTypes.STRING(500), allowNull: false },
			address_latitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0 },
			address_longitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0 },

			distance: {	type: Sequelize.BIGINT, allowNull: false },

			sunday_service: { type: DataTypes.TINYINT, allowNull: false, defaultValue: 1 },
			monday_service: { type: DataTypes.TINYINT, allowNull: false, defaultValue: 1 },
			tuesday_service: { type: DataTypes.TINYINT, allowNull: false, defaultValue: 1 },
			wednesday_service: { type: DataTypes.TINYINT, allowNull: false, defaultValue: 1 },
			thursday_service: { type: DataTypes.TINYINT, allowNull: false, defaultValue: 1 },
			friday_service: { type: DataTypes.TINYINT, allowNull: false, defaultValue: 1 },
			saturday_service: { type: DataTypes.TINYINT, allowNull: false, defaultValue: 1 },

			deleted: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: 0 },
			blocked: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: 0 },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	OrganisationAreas.associate = function(models) {

		OrganisationAreas.belongsTo(models.organisations, { as: "Org", foreignKey: "organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });

		OrganisationAreas.hasMany(models.organisation_area_drivers, { as: "drivers", foreignKey: "organisation_area_id", sourceKey: "organisation_area_id", onDelete: "cascade" });
		OrganisationAreas.hasOne(models.organisation_area_drivers, { as: "driver", foreignKey: "organisation_area_id", sourceKey: "organisation_area_id", onDelete: "cascade" });

		

	};

	return OrganisationAreas;
};
