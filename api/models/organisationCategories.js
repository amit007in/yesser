"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var organisationCategories = sequelize.define("organisation_categories", 
		{
			organisation_category_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			category_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			category_type: { type: DataTypes.STRING(10), allowNull: false, defaultValue: "Service" },

			buraq_percentage: { type: DataTypes.DOUBLE(5,2), allowNull: false, defaultValue: 10.00 },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	organisationCategories.associate = function(models) {

		organisationCategories.belongsTo(models.organisations, { as: "Org", foreignKey: "organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });

		organisationCategories.belongsTo(models.categories, { as: "Category", foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });

	};

	return organisationCategories;
};
