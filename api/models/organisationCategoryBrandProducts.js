"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var orgCategoryBrandProducts = sequelize.define("organisation_category_brand_products", 
		{
			organisation_category_brand_product_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			///////////		Category 	/////////////////
			category_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},
			category_brand_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			category_brand_product_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			///////////		Category 	/////////////////

			alpha_price: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },

			price_per_quantity: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },
			price_per_distance: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },
			price_per_weight: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },
			price_per_hr: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },
			price_per_sq_mt: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	orgCategoryBrandProducts.associate = function(models) {

		orgCategoryBrandProducts.belongsTo(models.organisations, { foreignKey: "organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });

		///////////		Category 	/////////////////
		orgCategoryBrandProducts.belongsTo(models.categories, { foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });
		orgCategoryBrandProducts.belongsTo(models.category_brands, { foreignKey: "category_brand_id", sourceKey: "category_brand_id", onDelete: "cascade" });
		orgCategoryBrandProducts.belongsTo(models.category_brand_products, { foreignKey: "category_brand_product_id", sourceKey: "category_brand_product_id", onDelete: "cascade" });
		///////////		Category 	/////////////////



	};

	return orgCategoryBrandProducts;
};
