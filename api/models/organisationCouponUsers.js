"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var OrganisationCouponUsers = sequelize.define("organisation_coupon_users", 
		{
			organisation_coupon_user_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			///////////		Organisation 	/////////////
			organisation_coupon_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			///////////		Organisation 	/////////////

			///////////		Category 	/////////////////
			category_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			category_brand_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			category_brand_product_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			///////////		Category 	/////////////////

			///////////		Seller 	/////////////////
			seller_user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			seller_user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			seller_user_type_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			seller_organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},
			///////////		Seller 	/////////////////

			//////////////		User 	///////////////////
			customer_user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			customer_user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			customer_user_type_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			customer_organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},
			//////////////		User 	///////////////////

			bottle_quantity: { type: DataTypes.INTEGER, allowNull: false },
			quantity: { type: DataTypes.INTEGER, allowNull: false },
			quantity_left: { type: DataTypes.INTEGER, allowNull: false },

			address: { type: DataTypes.STRING(500), allowNull: false, defaultValue: "" },
			address_latitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0 },
			address_longitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0 },


			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false },

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	OrganisationCouponUsers.associate = function(models) {

		///////////		Organisation 	/////////////
		//OrganisationCouponUsers.belongsTo(models.organisations, { as: "Org", foreignKey: "organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });
		OrganisationCouponUsers.belongsTo(models.organisation_coupons, { as: "OrgCoupon", foreignKey: "organisation_coupon_id", sourceKey: "organisation_coupon_id", onDelete: "cascade" });
		///////////		Organisation 	/////////////

		///////////		Category 	/////////////////
		OrganisationCouponUsers.belongsTo(models.categories, { as: "Category", foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });
		OrganisationCouponUsers.belongsTo(models.category_brands, { as: "CategoryBrand", foreignKey: "category_brand_id", sourceKey: "category_brand_id", onDelete: "cascade" });
		OrganisationCouponUsers.belongsTo(models.category_brands, { as: "CategoryBrandProduct", foreignKey: "category_brand_product_id", sourceKey: "category_brand_product_id", onDelete: "cascade" });

		OrganisationCouponUsers.belongsTo(models.category_brand_details, { as: "categoryBrand", foreignKey: "category_brand_id", sourceKey: "category_brand_id", onDelete: "cascade" });
		OrganisationCouponUsers.belongsTo(models.category_brand_product_details, { as: "categoryBrandProduct", foreignKey: "category_brand_product_id", sourceKey: "category_brand_product_id", onDelete: "cascade" });

		///////////		Category 	/////////////////

		///////////		Seller 	/////////////////////
		OrganisationCouponUsers.belongsTo(models.users, {as: "Seller", foreignKey: "seller_user_id", sourceKey: "user_id", onDelete: "cascade" });
		OrganisationCouponUsers.belongsTo(models.user_details, {as: "SellerDetails", foreignKey: "seller_user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		OrganisationCouponUsers.belongsTo(models.user_types, {as: "SellerType", foreignKey: "seller_user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });
		OrganisationCouponUsers.belongsTo(models.organisations, {as: "sellerOrg", foreignKey: "seller_organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });

		///////////		Seller 	/////////////////////

		///////////////			Customer 		/////
		OrganisationCouponUsers.belongsTo(models.users, {as: "Customer", foreignKey: "customer_user_id", sourceKey: "user_id", onDelete: "cascade" });
		OrganisationCouponUsers.belongsTo(models.user_details, {as: "CustomerDetails", foreignKey: "customer_user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		OrganisationCouponUsers.belongsTo(models.organisations, {as: "CustomerOrg", foreignKey: "customer_organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });
		OrganisationCouponUsers.belongsTo(models.user_types, {as: "CustomerType", foreignKey: "customer_user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });
		///////////////			Customer 		//////

		OrganisationCouponUsers.hasMany(models.payments, { foreignKey: "organisation_coupon_user_id", sourceKey: "organisation_coupon_user_id", onDelete: "cascade" });
		OrganisationCouponUsers.hasMany(models.orders, { foreignKey: "organisation_coupon_user_id", sourceKey: "organisation_coupon_user_id", onDelete: "cascade" });

	};

	return OrganisationCouponUsers;
};
