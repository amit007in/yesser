"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var OrganisationCoupons = sequelize.define("organisation_coupons", 
		{
			organisation_coupon_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			///////////	User 	/////////////////////////
			user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			user_type_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			///////////	User 	/////////////////////////

			///////////		Category 	/////////////////
			category_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			category_brand_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			category_brand_product_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			///////////		Category 	/////////////////

			price: { type: DataTypes.FLOAT(10,2), allowNull: false },
			quantity: { type: DataTypes.BIGINT, allowNull: false },
			description: { type: DataTypes.TEXT, allowNull: false },

			deleted: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },
			blocked: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	OrganisationCoupons.associate = function(models) {

		OrganisationCoupons.belongsTo(models.organisations, { as: "Org", foreignKey: "organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });

		///////////	User 	/////////////////////////
		OrganisationCoupons.belongsTo(models.users, { foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });
		OrganisationCoupons.belongsTo(models.user_details, { foreignKey: "user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		OrganisationCoupons.belongsTo(models.user_types, { foreignKey: "user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });
		///////////	User 	/////////////////////////

		///////////		Category 	/////////////////
		OrganisationCoupons.belongsTo(models.categories, { as: "Category", foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });
		OrganisationCoupons.belongsTo(models.category_brand_details, { as: "categoryBrand", foreignKey: "category_brand_id", sourceKey: "category_brand_id", onDelete: "cascade" });
		OrganisationCoupons.belongsTo(models.category_brand_product_details, { as: "categoryBrandProduct", foreignKey: "category_brand_product_id", sourceKey: "category_brand_product_id", onDelete: "cascade" });
		///////////		Category 	/////////////////

		//OrganisationCoupons.hasMany(models.payments, { foreignKey: "organisation_coupon_id", sourceKey: "organisation_coupon_id", onDelete: "cascade" });
		OrganisationCoupons.hasMany(models.organisation_coupon_users, { foreignKey: "organisation_coupon_id", sourceKey: "organisation_coupon_id", onDelete: "cascade" });
		OrganisationCoupons.hasOne(models.organisation_coupon_users, { as: "single_history", foreignKey: "organisation_coupon_id", sourceKey: "organisation_coupon_id", onDelete: "cascade" });

	};

	return OrganisationCoupons;
};
