"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var Org = sequelize.define("organisations", 
		{
			organisation_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			language_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			name: { type: DataTypes.STRING(255), allowNull: false },

			licence_number: { type: DataTypes.STRING(50), allowNull: false, unique: true },
			username: { type: DataTypes.STRING(255), allowNull: false, unique: true },
			email: { type: DataTypes.STRING(255), allowNull: false, unique: true },

			password: { type: DataTypes.TEXT },
			image: { type: DataTypes.STRING(100), allowNull: false },

			phone_code: { type: DataTypes.STRING(5), allowNull: false, defaultValue: "+968" },
			phone_number: { type: DataTypes.BIGINT, allowNull: false, unique: true },

			stripe_connect_id: { type: DataTypes.STRING(100), allowNull: true },
			forgot_token: { type: DataTypes.STRING(100), allowNull: true },
			forgot_token_validity: { type: DataTypes.DATE, allowNull: true },

			timezone: { type: DataTypes.STRING(100), allowNull: false, defaultValue: "Asia/Muscat" },
			timezonez: { type: DataTypes.STRING(10), allowNull: false, defaultValue: "+04:00" },

			credit_amount_limit: { type: DataTypes.DOUBLE(20, 4), defaultValue: -1 },
			credit_day_limit: { type: DataTypes.BIGINT, allowNull: false, defaultValue: -1 },
			credit_start_dt: { type: DataTypes.STRING(30), allowNull: true },

			blocked: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },
			created_by: { type: DataTypes.STRING(10), allowNull: false, defaultValue: "App" },

			sort_order: { type: Sequelize.BIGINT, defaultValue: 0 },

			address: { type: DataTypes.STRING(500), allowNull: false },
			address_latitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0 },
			address_longitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0 },

			buraq_percentage: { type: DataTypes.DOUBLE(5,2), allowNull: false, defaultValue: 10.00 },
			bottle_charge: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "0" },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	Org.associate = function(models) {

		Org.belongsTo(models.languages, { foreignKey: "language_id", sourceKey: "language_id", onDelete: "cascade" });
		// User.hasMany(models.user_details, { foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });

		///////////////			Orders 	/////////////////////////
		Org.hasMany(models.orders, { as: "ORequested", foreignKey: "customer_organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });
		Org.hasMany(models.orders, { as: "OProvided", foreignKey: "driver_organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });
		///////////////			Orders 	/////////////////////////

		///////////////			Ratings 	/////////////////////
		Org.hasMany(models.order_ratings, { as: "CustomerOrgReview", foreignKey: "customer_organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });
		Org.hasMany(models.order_ratings, { as: "OrgDriverCustReview", foreignKey: "driver_organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });
		///////////////			Ratings 	/////////////////////

		///////////////			Notifications 	/////////////////
		Org.hasMany(models.user_detail_notifications, { as: "ReceivedNotifications", foreignKey: "organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });
		Org.hasMany(models.user_detail_notifications, { as: "SentNotifications", foreignKey: "sender_organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });
		///////////////			Notifications 	/////////////////


		Org.hasMany(models.organisation_areas, { as: "OrgArea", foreignKey: "organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });

		Org.hasOne(models.organisation_categories, { as: "OrgCat", foreignKey: "organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });
		Org.hasMany(models.organisation_categories, { as: "OrgCategory", foreignKey: "organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });

		Org.hasMany(models.organisation_coupons, { as: "Etokens", foreignKey: "organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });


	};

	return Org;
};
