"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var Payments = sequelize.define("payments", 
		{
			payment_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			//////////////		User 	///////////////////
			customer_user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			customer_user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			customer_user_type_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			user_card_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
			},
			customer_organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},
			//////////////		User 	///////////////////

			order_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},

			///////////////		Seller 	/////////////////////
			seller_user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			seller_user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			seller_user_type_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			seller_organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},
			///////////////		Seller 	/////////////////////

			payment_type: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "Cash" },
			payment_status: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "Pending" },
			refund_status: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "NoNeed" },

			transaction_id: { type: DataTypes.STRING(100), allowNull: true, defaultValue: null },
			refund_id: { type: DataTypes.STRING(100), allowNull: true, defaultValue: null },

			buraq_percentage: { type: DataTypes.DOUBLE(5,2), allowNull: false, defaultValue: 0 },

			product_actual_value: { type: DataTypes.STRING(20), allowNull: false },
			product_quantity: { type: DataTypes.INTEGER, allowNull: false },
			product_weight: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "0" },
			product_sq_mt: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "0" },
			order_distance: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "0" },
			order_time: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "0" },

			product_alpha_charge: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "0" },
			product_per_quantity_charge: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "0" },
			product_per_weight_charge: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "0" },
			product_per_distance_charge: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "0" },
			product_per_hr_charge: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "0" },
			product_per_sq_mt_charge: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "0" },

			initial_charge: { type: DataTypes.STRING(20), allowNull: false },
			admin_charge: { type: DataTypes.STRING(20), allowNull: false },
			bank_charge: { type: DataTypes.STRING(20), allowNull: false },
			final_charge: { type: DataTypes.STRING(20), allowNull: false },

			bottle_charge: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "0" },
			bottle_returned_value: { type: DataTypes.BIGINT, allowNull: false, defaultValue: 0 },
                        
                        coupon_id: { type: DataTypes.INTEGER, allowNull: true },
                        //discounted_value: { type: DataTypes.STRING(30), allowNull: true },
			discounted_value: {type: DataTypes.DOUBLE(10,2), allowNull: true, defaultValue: 0.0},
			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	Payments.associate = function(models) {

		///////////////			Customer 		////////////////
		Payments.belongsTo(models.users, {as: "Customer", foreignKey: "customer_user_id", sourceKey: "user_id", onDelete: "cascade" });
		Payments.belongsTo(models.user_details, {as: "CustomerDetails", foreignKey: "customer_user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		Payments.belongsTo(models.organisations, {as: "CustomerOrg", foreignKey: "customer_organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });
		Payments.belongsTo(models.user_types, {as: "CustomerType", foreignKey: "customer_user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });
		Payments.belongsTo(models.user_cards, { as: "Card", foreignKey: "user_card_id", sourceKey: "user_card_id", onDelete: "cascade" });
		///////////////			Customer 		////////////////

		///////////////			Seller 		////////////////
		Payments.belongsTo(models.users, {as: "Seller", foreignKey: "seller_user_id", sourceKey: "user_id", onDelete: "cascade" });
		Payments.belongsTo(models.user_details, {as: "SellerDetails", foreignKey: "seller_user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		Payments.belongsTo(models.user_types, {as: "SellerType", foreignKey: "seller_user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });
		Payments.belongsTo(models.organisations, {as: "SellerOrg", foreignKey: "seller_organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });
		///////////////			Seller 		////////////////

		Payments.belongsTo(models.orders, { as: "Order", foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });

		// Payments.belongsTo(models.organisation_coupon_users, { foreignKey: "organisation_user_coupon_id", sourceKey: "organisation_user_coupon_id", onDelete: "cascade" });
		
		// Payments.hasOne(models.coupon_users, { foreignKey: "payment_id", sourceKey: "payment_id", onDelete: "cascade" });
		// Payments.hasOne(models.organisation_coupon_users, { foreignKey: "payment_id", sourceKey: "payment_id", onDelete: "cascade" });

	};

	return Payments;
};
