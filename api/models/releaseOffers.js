"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var ReleaseOffers = sequelize.define("release_offers", 
		{
			release_offer_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
            driver_id: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
            start_address: {type: DataTypes.STRING(255), allowNull: true, defaultValue: ""},
            end_address: {type: DataTypes.STRING(255), allowNull: false ,defaultValue: ""},
            start_latitude: {type: DataTypes.STRING(255), allowNull: false ,defaultValue: ""},
            start_longitude: {type: DataTypes.STRING(255), allowNull: false ,defaultValue: ""},
            end_latitude: {type: DataTypes.STRING(255), allowNull: false ,defaultValue: ""},
            end_longitude: {type: DataTypes.STRING(255), allowNull: false ,defaultValue: ""},
            margin: {type: DataTypes.STRING(255), allowNull: true,defaultValue: ""},
			offer_price: { type: DataTypes.STRING(255), allowNull: true ,defaultValue: ""},
         	status: { type: DataTypes.STRING(255), allowNull: true ,defaultValue: "Pending"},
			product_id: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
			expire_date: { type: DataTypes.STRING(255), allowNull: true ,defaultValue: ""},
			offer_expire: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
			created_at: {type: 'TIMESTAMP',defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),allowNull: false}
		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	return ReleaseOffers;
};
