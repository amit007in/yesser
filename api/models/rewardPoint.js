"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var RewardPoint = sequelize.define("reward_points", 
		{
			reward_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
			send_invite: { type: DataTypes.INTEGER(11), allowNull: false, defaultValue: 0 },
			add_point: { type: DataTypes.INTEGER(11), allowNull: false, defaultValue: 0 },
			reedemption_reward: { type: DataTypes.INTEGER(11), allowNull: false, defaultValue: 0 },
			reedemption_price: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },
			status: { type: DataTypes.INTEGER(11), allowNull: false, defaultValue: 0 },
			created_at: {type: 'TIMESTAMP',defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),allowNull: false},
			updated_at: {type: 'TIMESTAMP',defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),allowNull: false}
		},
		{
			underscored: true,
			timestamps: false
		}
	);
	return RewardPoint;
};

	