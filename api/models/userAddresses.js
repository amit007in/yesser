"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var UserAddresses = sequelize.define("user_addresses", 
		{
			address_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },
                        title: {type: DataTypes.STRING(255), allowNull: false},
                        flat_no: {type: DataTypes.STRING(255), allowNull: false},
                        floor_no: {type: DataTypes.STRING(255), allowNull: false},
                        building_name: {type: DataTypes.STRING(255), allowNull: false},
                        user_id: { type: Sequelize.BIGINT, allowNull: false, defaultValue: 0 },
			address: { type: DataTypes.STRING(500), allowNull: true },
			address_latitude: { type: DataTypes.FLOAT(7,4), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0 },
			address_longitude: { type: DataTypes.FLOAT(7,4), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0 }
		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	UserAddresses.associate = function(models) {
                UserAddresses.belongsTo(models.users, { as: "User", foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });
	};

	return UserAddresses;
};
