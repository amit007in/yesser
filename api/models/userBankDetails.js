"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var userBankDetails = sequelize.define("user_bank_details", 
		{
			user_bank_detail_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			// CREATE TABLE `user_bank_details` (
			//   `user_bank_detail_id` bigint(20) NOT NULL,
			//   `user_id` bigint(20) UNSIGNED NOT NULL,
			//   `bank_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
			//   `bank_account_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			//   `deleted` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
			//   `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
			//   `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			//   `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
			// ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


			bank_name: { type: DataTypes.STRING(255), allowNull: false },
			bank_account_number: { type: DataTypes.STRING(100), allowNull: false },

			active: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },
			deleted: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	userBankDetails.associate = function(models) {

		userBankDetails.belongsTo(models.users, { foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });

	};

	return userBankDetails;
};
