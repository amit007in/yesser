"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var UserCards = sequelize.define("user_cards", 
		{
			user_card_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			card_id: { type: DataTypes.STRING(100), allowNull: false },
			fingerprint: { type: DataTypes.STRING(100), allowNull: false },

			last4: { type: DataTypes.STRING(4), allowNull: false },
			brand: { type: DataTypes.STRING(20), allowNull: false },

			is_default: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },
			deleted: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	UserCards.associate = function(models) {

		UserCards.belongsTo(models.users, { foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });

		UserCards.hasMany(models.payments, { as: "Payments", foreignKey: "user_card_id", sourceKey: "user_card_id", onDelete: "cascade" });

	};

	return UserCards;
};
