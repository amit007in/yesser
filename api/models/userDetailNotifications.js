"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var userDetailNotifications = sequelize.define("user_detail_notifications", 
		{
			user_detail_notification_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			/////////		Receiver 	/////////////////
			user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			user_type_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			user_organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			/////////		Receiver 	/////////////////

			///////////		Sender 	/////////////////////
			sender_user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			sender_user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			sender_type_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			sender_organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			///////////		Sender 	/////////////////////

			///////////		Order 	/////////////////////
			order_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			order_request_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			order_rating_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			///////////		Order 	/////////////////////

			message: { type: DataTypes.STRING(500), allowNull: false },
			type: { type: DataTypes.STRING(20), allowNull: false },

			is_read: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },
			deleted: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	userDetailNotifications.associate = function(models) {

		///////////////			Receiver 		////////////////
		userDetailNotifications.belongsTo(models.users, {as: "Receiver", foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });
		userDetailNotifications.belongsTo(models.user_details, {as: "ReceiverDetails", foreignKey: "user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		userDetailNotifications.belongsTo(models.organisations, {as: "ReceiverOrg", foreignKey: "organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });
		userDetailNotifications.belongsTo(models.user_types, {as: "ReceiverUserType", foreignKey: "user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });
		///////////////			Receiver 		////////////////

		///////////////			Sender 		////////////////
		userDetailNotifications.belongsTo(models.users, {as: "Sender", foreignKey: "user_id", sourceKey: "sender_user_id", onDelete: "cascade" });
		userDetailNotifications.belongsTo(models.user_details, {as: "SenderDetails", foreignKey: "user_detail_id", sourceKey: "sender_user_detail_id", onDelete: "cascade" });
		userDetailNotifications.belongsTo(models.organisations, {as: "SenderOrg", foreignKey: "organisation_id", sourceKey: "sender_organisation_id", onDelete: "cascade" });
		userDetailNotifications.belongsTo(models.user_types, {as: "SenderUserType", foreignKey: "user_type_id", sourceKey: "sender_user_type_id", onDelete: "cascade" });
		///////////////			Receiver 		////////////////

		///////////////			Order 		////////////////////
		userDetailNotifications.belongsTo(models.orders, {as: "Order", foreignKey: "order_id", sourceKey: "order_id", onDelete: "cascade" });
		userDetailNotifications.belongsTo(models.order_requests, {as: "OrderRequest", foreignKey: "order_request_id", sourceKey: "order_request_id", onDelete: "cascade" });
		userDetailNotifications.belongsTo(models.order_ratings, {as: "OrderRating", foreignKey: "order_rating_id", sourceKey: "order_rating_id", onDelete: "cascade" });
		///////////////			Order 		////////////////////



	};

	return userDetailNotifications;
};
