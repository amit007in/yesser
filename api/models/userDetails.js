"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var UserDetails = sequelize.define("user_details", 
		{
			user_detail_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},
			user_type_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			language_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			category_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},
			category_brand_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},

			organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},

			online_status: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			otp: { type: DataTypes.STRING(4), allowNull: false },

			password: { type: DataTypes.TEXT },

			profile_pic: { type: DataTypes.STRING(100), allowNull: true },
			access_token: { type: DataTypes.STRING(150), allowNull: true },

			latitude: { type: DataTypes.FLOAT(7,4), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0 },
			longitude: { type: DataTypes.FLOAT(7,4), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0 },

			timezone: { type: DataTypes.STRING(100), allowNull: false, defaultValue: "Asia/Muscat" },
			timezonez: { type: DataTypes.STRING(10), allowNull: false, defaultValue: "+04:00" },

			socket_id: { type: DataTypes.STRING(255), allowNull: true },
			device_type: { type: DataTypes.ENUM("Ios", "Android"), allowNull: false, defaultValue: "Android" },
			fcm_id: { type: DataTypes.STRING(500), allowNull: false },

			mulkiya_number: { type: DataTypes.STRING(50), allowNull: true },
			mulkiya_front: { type: DataTypes.STRING(100), allowNull: true },
			mulkiya_back: { type: DataTypes.STRING(100), allowNull: true },
			mulkiya_validity: { type: DataTypes.DATE, allowNull: true },

			created_by: { type: DataTypes.STRING(10), allowNull: false, defaultValue: "App" },

			blocked: { type: DataTypes.ENUM("0","1", "2"), allowNull: false, defaultValue: "0" },
			notifications: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "1" },
			bearing: { type: DataTypes.FLOAT, allowNull: false, defaultValue: 0.0 },

			forgot_token: { type: DataTypes.STRING(100), allowNull: true },
			forgot_token_validity: { type: DataTypes.DATE, allowNull: true },

			maximum_rides: { type: Sequelize.BIGINT, defaultValue: 10 },
			total_reward_point: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	UserDetails.associate = function(models) {

		///////////////			User 	/////////////////////////
		UserDetails.belongsTo(models.users, { foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });
		UserDetails.belongsTo(models.user_types, { foreignKey: "user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });
		///////////////			User 	/////////////////////////

		UserDetails.belongsTo(models.languages, { foreignKey: "language_id", sourceKey: "language_id", onDelete: "cascade" });

		///////////////			Categories Etc 	/////////////////
		UserDetails.belongsTo(models.categories, { as: "Category", foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });
		UserDetails.belongsTo(models.category_brands, { as: "CategoryBrand", foreignKey: "category_brand_id", sourceKey: "category_brand_id", onDelete: "cascade" });

		UserDetails.belongsTo(models.organisations, { as: "Org", foreignKey: "organisation_id", sourceKey: "organisation_id", onDelete: "cascade" });
		///////////////			Categories Etc 	/////////////////

		UserDetails.hasMany(models.organisation_coupons, { as: "Etokens", foreignKey: "user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });

		UserDetails.hasMany(models.admin_contactus, { foreignKey: "user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });


		UserDetails.hasMany(models.user_driver_detail_products, { foreignKey: "user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });

		UserDetails.hasOne(models.user_driver_detail_services, { as: "UCat", foreignKey: "user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		UserDetails.hasMany(models.user_driver_detail_services, { foreignKey: "user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });

		///////////////			Orders 	/////////////////////////
		UserDetails.hasMany(models.orders, { as: "ORequested", foreignKey: "customer_user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		UserDetails.hasMany(models.orders, { as: "OProvided", foreignKey: "driver_user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		///////////////			Orders 	/////////////////////////

		///////////////			Ratings 	/////////////////////
		//		UserDetails.hasOne(models.order_ratings, { as: "CReview", foreignKey: "customer_user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		UserDetails.hasMany(models.order_ratings, { as: "CustomerReview", foreignKey: "customer_user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });

		//		UserDetails.hasOne(models.order_ratings, { as: "DReview", foreignKey: "driver_user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		UserDetails.hasMany(models.order_ratings, { as: "DriverReview", foreignKey: "driver_user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		///////////////			Ratings 	/////////////////////

		///////////////			Notifications 	/////////////////
		UserDetails.hasMany(models.user_detail_notifications, { as: "ReceivedNotifications", foreignKey: "user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		UserDetails.hasMany(models.user_detail_notifications, { as: "SentNotifications", foreignKey: "sender_user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });
		///////////////			Notifications 	/////////////////


	};

	return UserDetails;
};
