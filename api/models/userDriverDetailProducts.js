"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var userDriverDetailProducts = sequelize.define("user_driver_detail_products", 
		{
			user_driver_detail_product_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			category_brand_product_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			alpha_price: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },

			price_per_quantity: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },
			price_per_distance: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },
			price_per_weight: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },
			price_per_hr: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },
			price_per_sq_mt: { type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0 },

			deleted: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	userDriverDetailProducts.associate = function(models) {

		userDriverDetailProducts.belongsTo(models.users, { foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });

		userDriverDetailProducts.belongsTo(models.user_details, { foreignKey: "user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });

		userDriverDetailProducts.belongsTo(models.category_brand_products, { foreignKey: "category_brand_product_id", sourceKey: "category_brand_product_id", onDelete: "cascade" });

	};

	return userDriverDetailProducts;
};
