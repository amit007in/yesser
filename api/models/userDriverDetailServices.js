"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var userDriverDetailServices = sequelize.define("user_driver_detail_services", 
		{
			user_driver_detail_service_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			user_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			user_detail_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			category_id: {
				type: Sequelize.BIGINT, 
				allowNull: false
			},

			deleted: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			buraq_percentage: { type: DataTypes.DOUBLE(5,2), allowNull: false, defaultValue: 10.00 },

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	userDriverDetailServices.associate = function(models) {

		userDriverDetailServices.belongsTo(models.users, { foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });

		userDriverDetailServices.belongsTo(models.user_details, { foreignKey: "user_detail_id", sourceKey: "user_detail_id", onDelete: "cascade" });

		userDriverDetailServices.belongsTo(models.categories, { foreignKey: "category_id", sourceKey: "category_id", onDelete: "cascade" });

	};

	return userDriverDetailServices;
};
