"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var userInvitatons = sequelize.define("user_invitations", 
		{
			id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
            sender_user_detail_id: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
            receiver_user_detail_id: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
			created_at: {type: 'TIMESTAMP',defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),allowNull: false}
		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	return userInvitatons;
};