"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var userLogins = sequelize.define("user_logins", 
		{
			id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
            user_id: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
            user_type_id: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
            login_at: {type: DataTypes.STRING(255), allowNull: false ,defaultValue: ""},
			created_at: {type: 'TIMESTAMP',defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),allowNull: false}
		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	return userLogins;
};