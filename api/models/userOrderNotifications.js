"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var userOrderNotification = sequelize.define("user_order_notifications", 
		{
			user_order_notification_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
            order_id: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
            sender_user_detail_id: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
            receiver_user_detail_id: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
            is_read: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0},
            message: {type: DataTypes.STRING(255), allowNull: false ,defaultValue: ""},
            type: {type: DataTypes.STRING(255), allowNull: false ,defaultValue: ""},
			created_at: {type: 'TIMESTAMP',defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),allowNull: false}
		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	return userOrderNotification;
};