"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var QuotationOrders = sequelize.define("user_quotations", 
	{
			id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
			quotation_unique_id: {
				type:DataTypes.STRING(255), allowNull: false, defaultValue: ""
			},
			customer_user_id: {
				type: Sequelize.INTEGER, 
				allowNull: false, defaultValue: 0
			},
			customer_user_detail_id: {
				type: Sequelize.INTEGER, 
				allowNull: false, defaultValue: 0
			},
			customer_organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false, defaultValue: 0
			},
			customer_user_type_id: {
				type: Sequelize.BIGINT, 
				allowNull: false, defaultValue: 0
			},
			driver_user_id: {
				type: Sequelize.INTEGER, 
				allowNull: false, defaultValue: 0
			},
			driver_user_detail_id: {
				type: Sequelize.INTEGER, 
				allowNull: false, defaultValue: 0
			},
			driver_organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false, defaultValue: 0
			},
			driver_user_type_id: {
				type: Sequelize.BIGINT, 
				allowNull: false, defaultValue: 0
			},
			category_id: {
				type: Sequelize.BIGINT, 
				allowNull: false, defaultValue: 0
			},
			category_brand_id: {
				type: Sequelize.BIGINT, 
				allowNull: false, defaultValue: 0
			},
			category_products: { type: Sequelize.TEXT, allowNull: true, defaultValue: null },
			quotation_status: { type: DataTypes.STRING(255), allowNull: false },
			pickup_address: { type: DataTypes.STRING(255), allowNull: false, defaultValue: "" },
			pickup_latitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0 },
			pickup_longitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0 },
			dropoff_address: { type: DataTypes.STRING(255), allowNull: false },
			dropoff_latitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0 },
			dropoff_longitude: { type: DataTypes.FLOAT(9,6), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0 },
			details: { type: Sequelize.TEXT, allowNull: true, defaultValue: null },
			material_details: { type: Sequelize.TEXT, allowNull: true, defaultValue: null },
			payment_type: { type: DataTypes.STRING(255), allowNull: false },
			order_distance: { type: DataTypes.STRING(255), allowNull: false },
			order_images: { type: Sequelize.TEXT, allowNull: true, defaultValue: null },
			quotation_file: { type: Sequelize.TEXT, allowNull: true, defaultValue: "" },
			quotation_text: { type: DataTypes.STRING(255), allowNull: false, defaultValue: "" },
			quotation_price: { type: DataTypes.STRING(255), allowNull: false, defaultValue: "" },
			timeDurationLeft: { type: Sequelize.TEXT, allowNull: true, defaultValue: "" },
			floor_charge: {type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0.0}, //Add M3 Part 3.2
			reward_discount_price: {type: DataTypes.DOUBLE(10,2), allowNull: false, defaultValue: 0.0}, //Add M3 Part 3.2
			reward_discount_point: {type: DataTypes.INTEGER(11), allowNull: true, defaultValue: 0}, //Add M3 Part 3.2
			created_at: {type: 'TIMESTAMP',defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),allowNull: false}
	},
	{
		underscored: true,
		timestamps: false
	}
	);
		return QuotationOrders;
};

	