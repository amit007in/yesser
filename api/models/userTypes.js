"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var UserTypes = sequelize.define("user_types", 
		{
			user_type_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			type: { type: DataTypes.STRING(10), allowNull: false },

			blocked: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },

			created_at: { type: DataTypes.DATE, allowNull: false, defaultValue: sequelize.literal("CURRENT_TIMESTAMP") },
			updated_at: { type: DataTypes.DATE, allowNull: false, defaultValue: sequelize.literal("CURRENT_TIMESTAMP") }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	UserTypes.associate = function(models) {

		UserTypes.hasMany(models.admin_contactus, { foreignKey: "user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });

		UserTypes.hasMany(models.user_details, { foreignKey: "user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });

		UserTypes.hasMany(models.orders, { as: "ORequested", foreignKey: "customer_user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });
		UserTypes.hasMany(models.orders, { as: "OProvided", foreignKey: "driver_user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });

		UserTypes.hasMany(models.organisation_coupons, { as: "Etokens", foreignKey: "user_type_id", sourceKey: "user_type_id", onDelete: "cascade" });

	};

	return UserTypes;
};
