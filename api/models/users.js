"use strict";

var Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
	var Users = sequelize.define("users", 
		{
			user_id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },

			organisation_id: {
				type: Sequelize.BIGINT, 
				allowNull: false,
				defaultValue: 0
			},
			stripe_customer_id: { type: DataTypes.STRING(100), allowNull: true },
			stripe_connect_id: { type: DataTypes.STRING(100), allowNull: true },

			name: { type: DataTypes.STRING(255), allowNull: true },
			email: { type: DataTypes.STRING(255), allowNull: false },

			phone_code: { type: DataTypes.STRING(5), allowNull: false },
			phone_number: { type: DataTypes.BIGINT, allowNull: false },

			stripe_connect_token: { type: DataTypes.STRING(100), allowNull: false },

			address: { type: DataTypes.STRING(500), allowNull: true },
			address_latitude: { type: DataTypes.FLOAT(7,4), allowNull: false, validate: { min: -90, max: 90 }, defaultValue: 0.0 },
			address_longitude: { type: DataTypes.FLOAT(7,4), allowNull: false, validate: { min: -180, max: 180 }, defaultValue: 0.0 },

			blocked: { type: DataTypes.ENUM("0","1"), allowNull: false, defaultValue: "0" },
			created_by: { type: DataTypes.STRING(10), allowNull: false, defaultValue: "App" },

			bottle_charge: { type: DataTypes.STRING(20), allowNull: false, defaultValue: "0" },
			buraq_percentage: { type: DataTypes.FLOAT(5,2), allowNull: false, defaultValue: "10" },
                        
                        promotion_notification: {type: Sequelize.TINYINT, allowNull: false,defaultValue: 1},

			created_at: { type: DataTypes.STRING(30), allowNull: false },
			updated_at: { type: DataTypes.STRING(30), allowNull: false }

		},
		{
		    underscored: true,
		    timestamps: false
		}
	);

	Users.associate = function(models) {

		Users.hasMany(models.admin_contactus, { foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });

		Users.hasMany(models.user_bank_details, { foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });
		Users.hasOne(models.user_bank_details, { as: "active_bank", foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });
		
		Users.hasMany(models.user_cards, { foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });
		Users.hasOne(models.user_cards, { as: "Default", foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });
		
		Users.hasOne(models.user_details, { as: "user_detail", foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });
		Users.hasMany(models.user_details, { foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });


		Users.hasMany(models.user_driver_detail_products, { foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });
		Users.hasMany(models.user_driver_detail_services, { foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });

		///////////////			Orders 	/////////////////////////
		Users.hasMany(models.orders, { as: "ORequested", foreignKey: "customer_user_id", sourceKey: "user_id", onDelete: "cascade" });
		Users.hasMany(models.orders, { as: "OProvided", foreignKey: "driver_user_id", sourceKey: "user_id", onDelete: "cascade" });
		///////////////			Orders 	/////////////////////////

		///////////////			Ratings 	/////////////////////
		Users.hasMany(models.order_ratings, { as: "CustomerReview", foreignKey: "customer_user_id", sourceKey: "user_id", onDelete: "cascade" });
		Users.hasMany(models.order_ratings, { as: "DriverReview", foreignKey: "driver_user_id", sourceKey: "user_id", onDelete: "cascade" });
		///////////////			Ratings 	/////////////////////

		Users.hasMany(models.organisation_coupons, { as: "Etokens", foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });

		///////////////			Notifications 	/////////////////
		Users.hasMany(models.user_detail_notifications, { as: "ReceivedNotifications", foreignKey: "user_id", sourceKey: "user_id", onDelete: "cascade" });
		Users.hasMany(models.user_detail_notifications, { as: "SentNotifications", foreignKey: "sender_user_id", sourceKey: "user_id", onDelete: "cascade" });
		///////////////			Notifications 	/////////////////



	};

	return Users;
};
