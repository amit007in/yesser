var express = require("express");
var router = express.Router();

var Controllers = require(appRoot+"/controllers");///////////////////////		Controllers
var Libs = require(appRoot+"/libs");//////////////////		Libraries

/**
 * @swagger
 * /service/order/accept:
 *   post:
 *     description: For Service Driver Accepting Request
 *     tags: [Driver Service]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: order_id
 *         description: Order id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: latitude
 *         description: Latitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/accept", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOrderController.acceptRequest);
/**
 * @swagger
 * /service/order/reject:
 *   post:
 *     description: For Service Driver Rejecting Request
 *     tags: [Driver Service]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: order_id
 *         description: Order id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: latitude
 *         description: Latitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/reject", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOrderController.rejectRequest);
/**
 * @swagger
 * /service/order/ongoing:
 *   post:
 *     description: For Ongoing orders
 *     tags: [Driver Service]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/ongoing", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOrderController.servicesOngoing);
/**
 * @swagger
 * /service/order/start:
 *   post:
 *     description: For Starting Order
 *     tags: [Driver Service]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: order_id
 *         description: Order id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: latitude
 *         description: Latitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/start", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOrderController.startRequest);
/**
 * @swagger
 * /service/order/change/turn:
 *   post:
 *     description: For Service Driver Changing Turn
 *     tags: [Driver Service]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: order_id
 *         description: Order id
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/change/turn", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOrderController.driverOrderChangeTurn);
/**
 * @swagger
 * /service/order/end:
 *   post:
 *     description: For Service Driver Ending Service
 *     tags: [Driver Service]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: order_id
 *         description: Order id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: latitude
 *         description: Latitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: payment_type
 *         description: Payment Type Can be Either 'Cash' or 'Card'
 *         in: formData
 *         required: true
 *         type: string
 *       - name: order_distance
 *         description: Distance in KMs
 *         in: formData
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/end", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOrderController.endRequest);
/**
 * @swagger
 * /service/order/cancel:
 *   post:
 *     description: For Cancelling Service Request
 *     tags: [Driver Service]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: order_id
 *         description: Order id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: cancel_reason
 *         description: Cancel reason
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/cancel", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOrderController.CancelRequest);
/**
 * @swagger
 * /service/order/rate:
 *   post:
 *     description: For Rating a completed service
 *     tags: [Driver Service]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: order_id
 *         description: Order id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: ratings
 *         description: Ratings 1 to 5
 *         in: formData
 *         required: true
 *         type: number
 *       - name: comments
 *         description: Comments
 *         in: formData
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/rate", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOrderController.driverServiceRate);
/**
 * @swagger
 * /service/order/confirm:
 *   post:
 *     description: For Confirming or Unconfirming a order
 *     tags: [Driver Service]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: order_id
 *         description: Order id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: latitude
 *         description: Latitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: approved
 *         description: Approves - 1, Unapproves - 0
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/confirm", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOrderController.driverScheduledServiceConfirm);
//Add M2 Part 2
router.post("/driverSendQuotations", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOrderController.driverSendQuotation);

router.post("/driverSendQuotation", [Libs.middlewareFunctions.loginInRequired ,upload.array("quotation_file") ], Controllers.serviceOrderController.driverSendQuotations);

router.post("/getAllDriversQuotation", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOrderController.getAllDriversQuotation);

router.post("/driverRejectQuotation", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOrderController.driverRejectQuotation);

router.post("/ReshuffleOrder", Controllers.serviceOrderController.acceptShuffleRequest);

module.exports = router;
