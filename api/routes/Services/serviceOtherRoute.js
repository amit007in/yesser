var express = require("express");
var router = express.Router();

var Controllers = require(appRoot+"/controllers");///////////////////////		Controllers
var Libs = require(appRoot+"/libs");//////////////////		Libraries
/**
 * @swagger
 * /service/other/order/history:
 *   post:
 *     description: For Serive Order history
 *     tags: [Service Other Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: skip
 *         description: Page Number
 *         in: formData
 *         required: true
 *         type: number
 *       - name: take
 *         description: Number of Tokens at a time
 *         in: formData
 *         required: true
 *         type: number
 *       - name: type
 *         description: Order Type - 1 for past, 2 for ongoing
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/order/history", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOtherController.driverHistory);
/**
 * @swagger
 * /service/other/order/details:
 *   post:
 *     description: For Service Order details
 *     tags: [Service Other Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: order_id
 *         description: Order id
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/order/details", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOtherController.driverOrderDetails);
/**
 * @swagger
 * /service/other/earnings:
 *   post:
 *     description: For Service Driver Earnings
 *     tags: [Service Other Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: start_dt
 *         description: Start DateTime Y-m-d H:m:s
 *         in: formData
 *         required: true
 *         type: string
 *       - name: end_dt
 *         description: End DateTime Y-m-d H:m:s
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/earnings", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOtherController.driverEarnings);

//Add M2 Part 2
router.post("/addReleaseOffer", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOtherController.addReleaseOffer);

router.post("/getDriverOffers", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOtherController.getDriverOffers);

router.post("/getDriverProductList", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOtherController.getDriverProductList);

router.post("/deleteDriverOffer", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceOtherController.deleteDriverOffer);

router.post("/SendOffertoCustomer", Controllers.serviceOtherController.SendOffertoCustomer);

module.exports = router;