var express = require("express");
var router = express.Router();

var Controllers = require(appRoot+"/controllers");///////////////////////		Controllers
var Libs = require(appRoot+"/libs");//////////////////		Libraries

/**
 * @swagger
 * /service/sendOtp:
 *   post:
 *     description: For Service Sending Otp
 *     tags: [Service Register Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: true
 *         type: string
 *       - name: phone_code
 *         description: Phone Code.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: phone_number
 *         description: Phone Number.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: timezone
 *         description: Timezone Eg - Asia/Calcutta.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: latitude
 *         description: Latitude Default -0
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude Default -0
 *         in: formData
 *         required: true
 *         type: number
 *       - name: socket_id
 *         description: Socket Id
 *         in: formData
 *         required: false
 *         type: string
 *       - name: fcm_id
 *         description: FCM Id for push notifications
 *         in: formData
 *         required: false
 *         type: string
 *       - name: device_type
 *         description: Device Type
 *         in: formData
 *         required: true
 *         enum: ['Ios', 'Android']
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/sendOtp", [Libs.middlewareFunctions.languageMiddleware], Controllers.serviceController.serviceSendOtp);
/**
 * @swagger
 * /service/verifyOTP:
 *   post:
 *     description: For Service Driver Verifying OTP
 *     tags: [Service Register Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: otp
 *         description: OTP
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/verifyOTP", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceController.verifyOTP);
/**
 * @swagger
 * /service/addPersonalDetail:
 *   post:
 *     description: For Service Driver Adding name, organisation
 *     tags: [Service Register Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: name
 *         description: Driver Name
 *         in: formData
 *         required: true
 *         type: string
 *       - name: organisation_id
 *         description: Organisation Id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: profile_pic
 *         description: Profile Pic Image
 *         in: formData
 *         required: false
 *         type: file
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/addPersonalDetail", [Libs.middlewareFunctions.loginInRequired, upload.single("profile_pic") ], Controllers.serviceController.addPersonalDetail);
/**
 * @swagger
 * /service/addBankDetail:
 *   post:
 *     description: For Service Driver Adding Bank Details and Category, Category Brand
 *     tags: [Service Register Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: category_id
 *         description: Service ID
 *         in: formData
 *         required: true
 *         type: string
 *       - name: category_brand_id
 *         description: Category Brand Id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: category_brand_product_ids
 *         description: Category Brand Product Ids Eg - [1,2]
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/addBankDetail", [Libs.middlewareFunctions.loginInRequired, upload.array("mulkiya")], Controllers.serviceController.addBankDetail);
/**
 * @swagger
 * /service/addMulkiyaDetail:
 *   post:
 *     description: For Service Driver Adding Mulkiya Details
 *     tags: [Service Register Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: mulkiya_number
 *         description: Mulkiya Number
 *         in: formData
 *         required: true
 *         type: string
 *       - name: mulkiya_validity
 *         description: Mulkiya Validity Date in UTC timezone format - 'Y-m-D H:i:s'
 *         in: formData
 *         required: true
 *         type: string
 *       - name: bank_name
 *         in: formData
 *         description: Bank Name
 *         required: false
 *         type: string
 *       - name: bank_account_number
 *         in: formData
 *         description: Bank Account Number
 *         required: false
 *         type: string
 *       - name: mulkiya[0]
 *         in: formData
 *         description: Mulkiya Front image
 *         required: image
 *         type: file
 *       - name: mulkiya[1]
 *         in: formData
 *         description: Mulkiya Back image
 *         required: image
 *         type: file
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/addMulkiyaDetail", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceController.addMulkiyaDetail);
/**
 * @swagger
 * /service/profileUpdate:
 *   post:
 *     description: For Service Driver Profile Update
 *     tags: [Service Register Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: name
 *         description: Name
 *         in: formData
 *         required: true
 *         type: string
 *       - name: mulkiya_number
 *         description: Mulkiya Number
 *         in: formData
 *         required: true
 *         type: string
 *       - name: mulkiya_validity
 *         description: Mulkiya Validity Date in UTC timezone format - 'Y-m-D H:i:s'
 *         in: formData
 *         required: true
 *         type: string
 *       - name: mulkiya[0]
 *         in: formData
 *         description: Mulkiya Front image
 *         required: image
 *         type: file
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/profileUpdate", [Libs.middlewareFunctions.loginInRequired, upload.array("mulkiya")], Controllers.serviceController.profileUpdate);
/**
 * @swagger
 * /service/picUpdate:
 *   post:
 *     description: For Updating profile pic
 *     tags: [Service Register Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: profile_pic
 *         in: formData
 *         description: Profile Pic
 *         required: image
 *         type: file
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/picUpdate", [Libs.middlewareFunctions.loginInRequired, upload.single("profile_pic")], Controllers.serviceController.picUpdate);

module.exports = router;

// type: 
// items:
//   type: string
//   format: binary