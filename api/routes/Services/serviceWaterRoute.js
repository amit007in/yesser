var express = require("express");
var router = express.Router();

var Controllers = require(appRoot+"/controllers");///////////////////////		Controllers
var Libs = require(appRoot+"/libs");//////////////////		Libraries
/**
 * @swagger
 * /service/water/areas/list:
 *   post:
 *     description: For Driver Water Section Response JSON "https://pastebin.com/RAUykhnH"
 *     tags: [Driver Water]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: day
 *         description: day, 0 for Sunday, 1 for monday so on
 *         in: formData
 *         required: true
 *         type: number
 *       - name: latitude
 *         description: Latitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: skip
 *         description: Page Number
 *         in: formData
 *         required: true
 *         type: number
 *       - name: take
 *         description: Number of records at a time
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/areas/list", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceWaterController.driverAreasList);
/**
 * @swagger
 * /service/water/area/customers/list:
 *   post:
 *     description: For Driver Area Customers Listings Response JSON "https://pastebin.com/QT4XT98n"
 *     tags: [Driver Water]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: organisation_area_id
 *         description: Area Id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: latitude
 *         description: Latitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: skip
 *         description: Page Number
 *         in: formData
 *         required: true
 *         type: number
 *       - name: take
 *         description: Number of records at a time
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/area/customers/list", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceWaterController.driverAreaCustomersList);
/**
 * @swagger
 * /service/water/initiate/order:
 *   post:
 *     description: For Driver Initiating Order for drinking water Response JSON "https://pastebin.com/7K67mpN5"
 *     tags: [Driver Water]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: organisation_coupon_user_id
 *         description: Org Coupon Purchase Id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: latitude
 *         description: Latitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/initiate/order", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceWaterController.driverInitiateOrder);
/**
 * @swagger
 * /service/water/end/order:
 *   post:
 *     description: For Driver ending etoken order
 *     tags: [Driver Water]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: order_id
 *         description: Order Id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: latitude
 *         description: Latitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: product_quantity
 *         description: Product Quantity
 *         in: formData
 *         required: true
 *         type: number
 *       - name: order_distance
 *         description: Distance in KMs
 *         in: formData
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/end/order", [Libs.middlewareFunctions.loginInRequired], Controllers.serviceWaterController.driverEndOrder);


module.exports = router;