var express = require("express");
var router = express.Router();

var Controllers = require(appRoot+"/controllers");///////////////////////		Controllers
var Libs = require(appRoot+"/libs");//////////////////		Libraries


/**
 * @swagger
 * /support/sendOtp:
 *   post:
 *     description: For Support Sending Otp
 *     tags: [Support Register Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: true
 *         type: string
 *       - name: phone_code
 *         description: Phone Code.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: phone_number
 *         description: Phone Number.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: timezone
 *         description: Timezone Eg - Asia/Calcutta.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: latitude
 *         description: Latitude Default -0
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude Default -0
 *         in: formData
 *         required: true
 *         type: number
 *       - name: fcm_id
 *         description: FCM Id for push notifications
 *         in: formData
 *         required: false
 *         type: string
 *       - name: device_type
 *         description: Device Type
 *         in: formData
 *         required: true
 *         enum: ['Ios', 'Android']
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/sendOtp", [Libs.middlewareFunctions.languageMiddleware], Controllers.supportController.serviceSendOtp);
/**
 * @swagger
 * /support/verifyOTP:
 *   post:
 *     description: For Support Driver Verifying OTP
 *     tags: [Support Register Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: otp
 *         description: OTP
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/verifyOTP", [Libs.middlewareFunctions.loginInRequired], Controllers.supportController.verifyOTP);


router.post("/addPersonalDetail", [Libs.middlewareFunctions.loginInRequired, upload.single("profile_pic") ], Controllers.supportController.addPersonalDetail);

module.exports = router;
