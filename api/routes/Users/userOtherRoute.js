var express = require("express");
var router = express.Router();

var Controllers = require(appRoot+"/controllers");///////////////////////		Controllers
var Libs = require(appRoot+"/libs");//////////////////		Libraries
/**
 * @swagger
 * /user/other/coupons:
 *   post:
 *     description: For Coupons listings
 *     tags: [User Other Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/coupons", [Libs.middlewareFunctions.loginInRequired], Controllers.userOtherController.userCoupons);
/**
 * @swagger
 * /user/other/coupon/buy:
 *   post:
 *     description: For Buying Coupon
 *     tags: [User Other Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: coupon_id
 *         description: Coupon Id
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/coupon/buy", [Libs.middlewareFunctions.loginInRequired], Controllers.userOtherController.userCouponBuy);
/**
 * @swagger
 * /user/other/payment/details:
 *   post:
 *     description: For Etokens Sum with brand
 *     tags: [User Other Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: category_brand_id
 *         description: Category Brand Id
 *         in: formData
 *         required: false
 *         type: number
 *       - name: category_brand_product_id
 *         description: Product id
 *         in: formData
 *         required: false
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/payment/details", [Libs.middlewareFunctions.loginInRequired], Controllers.userOtherController.userPaymentDetails);
/**
 * @swagger
 * /user/other/order/history:
 *   post:
 *     description: For User Order history
 *     tags: [User Other Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: skip
 *         description: Page Number
 *         in: formData
 *         required: true
 *         type: number
 *       - name: take
 *         description: Number of Tokens at a time
 *         in: formData
 *         required: true
 *         type: number
 *       - name: type
 *         description: Order Type - 1 for past, 2 for ongoing
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/order/history", [Libs.middlewareFunctions.loginInRequired], Controllers.userOtherController.userHistory);
/**
 * @swagger
 * /user/other/order/details:
 *   post:
 *     description: For User Order details
 *     tags: [User Other Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: order_id
 *         description: Order id
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/order/details", [Libs.middlewareFunctions.loginInRequired], Controllers.userOtherController.userOrderDetails);

module.exports = router;