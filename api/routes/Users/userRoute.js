var express = require("express");
var router = express.Router();

var Controllers = require(appRoot+"/controllers");///////////////////////		Controllers
var Libs = require(appRoot+"/libs");//////////////////		Libraries
/**
 * @swagger
 * /user/sendOtp:
 *   post:
 *     description: For User Sending Otp
 *     tags: [User Register Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: true
 *         type: string
 *       - name: phone_code
 *         description: Phone Code.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: phone_number
 *         description: Phone Number.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: timezone
 *         description: Timezone Eg - Asia/Calcutta.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: latitude
 *         description: Latitude Default -0
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude Default -0
 *         in: formData
 *         required: true
 *         type: number
 *       - name: socket_id
 *         description: Socket Id
 *         in: formData
 *         required: false
 *         type: string
 *       - name: fcm_id
 *         description: FCM Id for push notifications
 *         in: formData
 *         required: false
 *         type: string
 *       - name: device_type
 *         description: Device Type
 *         in: formData
 *         required: true
 *         enum: ['Ios', 'Android']
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/sendOtp", [Libs.middlewareFunctions.languageMiddleware], Controllers.userController.userSendOtp);
/**
 * @swagger
 * /user/verifyOTP:
 *   post:
 *     description: For User Verifying OTP
 *     tags: [User Register Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: otp
 *         description: OTP
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/verifyOTP", [Libs.middlewareFunctions.loginInRequired], Controllers.userController.verifyOTP);
/**
 * @swagger
 * /user/addName:
 *   post:
 *     description: For User Adding name
 *     tags: [User Register Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *         
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *         
 *       - name: name
 *         description: User Name
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/addName", [Libs.middlewareFunctions.loginInRequired], Controllers.userController.addName);
/**
 * @swagger
 * /user/profileUpdate:
 *   post:
 *     description: For User updating name, profile pic
 *     tags: [User Register Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *         
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *         
 *       - name: name
 *         description: Driver Name
 *         in: formData
 *         required: true
 *         type: string
 *         
 *       - name: profile_pic
 *         description: Profile Pic Image
 *         in: formData
 *         required: false
 *         type: file
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/profileUpdate", [Libs.middlewareFunctions.loginInRequired, upload.single("profile_pic")], Controllers.userController.profileUpdate);

/**
 * @swagger
 * /user/notifications/history:
 *   post:
 *     description: get all notification history
 *     tags: [User Notification Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *         
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *         
 *       - name: take
 *         description: Page no for list
 *         in: json
 *         required: true
 *         type: integer
 *         
 *       - name: skip
 *         description: total result on a page
 *         in: json
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/notifications/history", [Libs.middlewareFunctions.loginInRequired], Controllers.userController.notificationHistory);


/**
 * @swagger
 * /user/notification/detail:
 *   post:
 *     description: get notification detail
 *     tags: [User Notification Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *         
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *         
 *       - name: notification_id
 *         description: notification_id
 *         in: json
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/notification/detail", [Libs.middlewareFunctions.loginInRequired], Controllers.userController.getNotificationById);

/**
 * @swagger
 * /user/promocode/list:
 *   post:
 *     description: get promocode list
 *     tags: [User promocode Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: name
 *         description: Promocode list
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/promocode/list", [Libs.middlewareFunctions.loginInRequired], Controllers.userController.getPromocodeList);
router.post("/promocode/lists", [Libs.middlewareFunctions.loginInRequired], Controllers.userController.getPromocodeLists);


/**
 * @swagger
 * /user/promocode/detail:
 *   post:
 *     description: get promocode detail
 *     tags: [User Promocode Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: name
 *         description: promocode details
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/promocode/detail", [Libs.middlewareFunctions.loginInRequired], Controllers.userController.getPromocodeById);



/**
 * @swagger
 * /user/apply/promocode:
 *   post:
 *     description: apply promocode
 *     tags: [User promocode Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: name
 *         description: apply Promocode
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/apply/promocode", [Libs.middlewareFunctions.loginInRequired], Controllers.userController.applyPromocode);

module.exports = router;