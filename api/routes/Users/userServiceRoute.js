var express = require("express");
var router = express.Router();

var Controllers = require(appRoot+"/controllers");///////////////////////		Controllers
var Libs = require(appRoot+"/libs");//////////////////		Libraries
/**
 * @swagger
 * /user/service/homeApi:
 *   post:
 *     description: For User Home Drivers Listings
 *     tags: [User Drivers]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: category_id
 *         description: Category or Service Id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: latitude
 *         description: Latitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: distance
 *         description: Distance for which want drivers listings in KMs
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/homeApi", [Libs.middlewareFunctions.loginInRequired], Controllers.userServiceController.homeApi);
//router.post('/gasBooking', [Libs.middlewareFunctions.loginInRequired], Controllers.userServiceController.userServicegasBooking);
/**
 * @swagger
 * /user/service/Request:
 *   post:
 *     description: For Requesting New Service
 *     tags: [User Service]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: category_id
 *         description: Category or Service Id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: category_brand_id
 *         description: Brand Id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: category_brand_product_id
 *         description: Product Id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: product_quantity
 *         description: Product Quantity
 *         in: formData
 *         required: true
 *         type: number
 *       - name: pickup_address
 *         description: Pickup Address
 *         in: formData
 *         required: false
 *         type: string 
 *       - name: pickup_latitude
 *         description: Pickup Latitude
 *         in: formData
 *         required: false
 *         type: number
 *       - name: pickup_longitude
 *         description: Pickup Longitude
 *         in: formData
 *         required: false
 *         type: number
 *       - name: dropoff_address
 *         description: DropOff Address
 *         in: formData
 *         required: false
 *         type: string 
 *       - name: dropoff_latitude
 *         description: DropOff Latitude
 *         in: formData
 *         required: false
 *         type: number
 *       - name: dropoff_longitude
 *         description: DropOff Longitude
 *         in: formData
 *         required: false
 *         type: number
 *       - name: order_timings
 *         description: Order Timing - Current or Future Timing in UTC in format 'Y-m-d H:i:s'
 *         in: formData
 *         required: true
 *         type: string
 *       - name: future
 *         description: Future parameter can either be '0' or '1'
 *         in: formData
 *         required: true
 *         type: string
 *       - name: payment_type
 *         description: Payment Type Can be Either 'Cash' or 'Card' or 'eToken'
 *         in: formData
 *         required: true
 *         type: string
 *       - name: distance
 *         description: Distance for which want drivers listings in KMs
 *         in: formData
 *         required: true
 *         type: number
 *       - name: organisation_coupon_user_id
 *         description: eToken ID for drinking water 5lts quantity
 *         in: formData
 *         required: false
 *         type: number
 *       - name: coupon_user_id
 *         description: Coupon User Id
 *         in: formData
 *         required: false
 *         type: number
 *       - name: order_images[0]
 *         in: formData
 *         description: Array of Images
 *         required: false
 *         type: file
 *       - name: product_weight
 *         description: Product Weight
 *         in: formData
 *         required: false
 *         type: number
 *       - name: product_sq_mt
 *         description: Product Sq meter value
 *         in: formData
 *         required: false
 *         type: number
 *       - name: details
 *         in: formData
 *         description: Details
 *         required: false
 *         type: string
 *       - name: material_details
 *         in: formData
 *         description: Material Details
 *         required: false
 *         type: string
 *       - name: order_distance
 *         description: Distance in KMs
 *         in: formData
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/request", [ Libs.middlewareFunctions.UserRatingloginInRequired, upload.array("order_images") ], Controllers.userServiceController.userServiceRequest);
router.post("/requests", [ Libs.middlewareFunctions.UserRatingloginInRequired, upload.array("order_images") ], Controllers.userServiceController.userServiceRequests);
/**
 * @swagger
 * /user/service/Cancel:
 *   post:
 *     description: For Cancelling Service Request
 *     tags: [User Service]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: order_id
 *         description: Order id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: cancel_reason
 *         description: Cancel reason
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/cancel", [Libs.middlewareFunctions.loginInRequired], Controllers.userServiceController.userServiceCancel);
/**
 * @swagger
 * /user/service/Ongoing:
 *   post:
 *     description: For Ongoing orders
 *     tags: [User Service]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/ongoing", [Libs.middlewareFunctions.loginInRequired], Controllers.userServiceController.userServiceOngoing);
/**
 * @swagger
 * /user/service/Rate:
 *   post:
 *     description: For Rating a completed service
 *     tags: [User Service]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: order_id
 *         description: Order id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: ratings
 *         description: Ratings 1 to 5
 *         in: formData
 *         required: true
 *         type: number
 *       - name: comments
 *         description: Comments
 *         in: formData
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/rate", [Libs.middlewareFunctions.loginInRequired], Controllers.userServiceController.userServiceRate);
/**
 * @swagger
 * /user/service/ETokens:
 *   post:
 *     description: For Etokens History and Listings
 *     tags: [User Service]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: category_id
 *         description: Category or Service Id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: category_brand_id
 *         description: Category Brand Id
 *         in: formData
 *         required: false
 *         type: number
 *       - name: latitude
 *         description: Latitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: distance
 *         description: Distance for which want drivers listings in KMs
 *         in: formData
 *         required: true
 *         type: number
 *       - name: take
 *         description: Number of Tokens at a time
 *         in: formData
 *         required: true
 *         type: number
 *       - name: order_timings
 *         description: Order Timing - Current or Future Timing in UTC in format 'Y-m-d H:i:s'
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/eTokens", [Libs.middlewareFunctions.loginInRequired], Controllers.userServiceController.userServiceETokens);
/**
 * @swagger
 * /user/service/ETokens/Paginate:
 *   post:
 *     description: For Etokens Pagination
 *     tags: [User Service]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: category_id
 *         description: Category or Service Id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: category_brand_id
 *         description: Brand Id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: latitude
 *         description: Latitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: distance
 *         description: Distance for which want drivers listings in KMs
 *         in: formData
 *         required: true
 *         type: number
 *       - name: skip
 *         description: Page Number
 *         in: formData
 *         required: true
 *         type: number
 *       - name: take
 *         description: Number of Tokens at a time
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/eToken/paginate", [Libs.middlewareFunctions.loginInRequired], Controllers.userServiceController.userETokensPaginate);
/**
 * @swagger
 * /user/service/eToken/buy:
 *   post:
 *     description: For Buying Etokens
 *     tags: [User Service]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: organisation_coupon_id
 *         description: Etoken Id
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/eToken/buy", [Libs.middlewareFunctions.loginInRequired], Controllers.userServiceController.userETokenBuy);

router.post("/test", [Libs.middlewareFunctions.loginInRequired], Controllers.userServiceController.userTesting);

router.post("/addaddress", [Libs.middlewareFunctions.loginInRequired], Controllers.userServiceController.addAddress);

router.post("/getaddress", [Libs.middlewareFunctions.loginInRequired], Controllers.userServiceController.getAddress);

router.post("/deleteaddress", [Libs.middlewareFunctions.loginInRequired], Controllers.userServiceController.deleteAddress);

router.post("/editaddress", [Libs.middlewareFunctions.loginInRequired], Controllers.userServiceController.editAddress);

router.post("/SendMultipleNotificationtousers", Controllers.userServiceController.SendMultipleNotificationtousers);

router.post("/ReassignOrder", Controllers.userServiceController.ReassignOrder); //Add M2 Part 2


router.post("/updatePromotionNotification", [Libs.middlewareFunctions.loginInRequired],Controllers.userServiceController.updatePromotionNotification);
//Add M2
router.post("/getQuotation", [ Libs.middlewareFunctions.UserRatingloginInRequired, upload.array("order_images") ], Controllers.userServiceController.userSendQuotation);  //Add M2 Part 2
router.post("/cancelQuotation", [ Libs.middlewareFunctions.UserRatingloginInRequired, upload.array("order_images") ], Controllers.userServiceController.userCancelQuotation);  //Add M2 Part 2
router.post("/getSentOffers", [ Libs.middlewareFunctions.UserRatingloginInRequired], Controllers.userServiceController.getSentOffers);  
router.post("/acceptSentOffer", [ Libs.middlewareFunctions.UserRatingloginInRequired], Controllers.userServiceController.acceptSentOffer);  
router.post("/createQuoattionOrder", [ Libs.middlewareFunctions.UserRatingloginInRequired], Controllers.userServiceController.userQuotationServiceRequest);  

router.post("/getAllCustomerQuotation", [ Libs.middlewareFunctions.UserRatingloginInRequired], Controllers.userServiceController.getAllCustomerQuotation);

//M3
router.post("/userSendInvitation", [ Libs.middlewareFunctions.UserRatingloginInRequired], Controllers.userServiceController.userSendInvitation);
 
router.post("/paymentsuccess", Controllers.userServiceController.paymentsuccess); //Add M3 Part 3.2

router.post("/availableDrivers", [ Libs.middlewareFunctions.UserRatingloginInRequired, upload.array("order_images") ], Controllers.userServiceController.getDriveravailableforservices);

module.exports = router;