var express = require("express");
var router = express.Router();

var Controllers = require(appRoot+"/controllers");///////////////////////		Controllers
var Libs = require(appRoot+"/libs");//////////////////		Libraries
/**
 * @swagger
 * /user/water/companies/list:
 *   post:
 *     description: For User Organisation Listings
 *     tags: [User Water]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: latitude
 *         description: Latitude is required in case of New
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude is required in case of New
 *         in: formData
 *         required: true
 *         type: number
 *       - name: category_brand_id
 *         description: Brand Id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: category_brand_product_id
 *         description: Product Id
 *         in: formData
 *         required: false
 *         type: number
 *       - name: skip
 *         description: Page Number
 *         in: formData
 *         required: true
 *         type: number
 *       - name: take
 *         description: Number of records at a time
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/companies/list", [Libs.middlewareFunctions.loginInRequired], Controllers.userWaterController.userCompaniesList);
/**
 * @swagger
 * /user/water/purchases/list:
 *   post:
 *     description: For User Purchases Listings
 *     tags: [User Water]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: skip
 *         description: Page Number
 *         in: formData
 *         required: true
 *         type: number
 *       - name: take
 *         description: Number of records at a time
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/purchases/list", [Libs.middlewareFunctions.loginInRequired], Controllers.userWaterController.userPurchasesList);

/**
 * @swagger
 * /user/water/etokens/list:
 *   post:
 *     description: For User Organisation Etokens Listings
 *     tags: [User Water]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: organisation_id
 *         description: organisation_id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: category_brand_id
 *         description: category_brand_id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: skip
 *         description: Page Number
 *         in: formData
 *         required: true
 *         type: number
 *       - name: take
 *         description: Number of records at a time
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/etokens/list", [Libs.middlewareFunctions.loginInRequired], Controllers.userWaterController.usereTokenList);
/**
 * @swagger
 * /user/water/etoken/purchase:
 *   post:
 *     description: For User Purchasing Etoken
 *     tags: [User Water]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: organisation_coupon_id
 *         description: Coupon Id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: buraq_percentage
 *         description: Percentage Value
 *         in: formData
 *         required: true
 *         type: number
 *       - name: bottle_returned_value
 *         description: Have bottles or not
 *         in: formData
 *         required: true
 *         enum: [0,1]
 *         type: number
 *       - name: bottle_charge
 *         description: Bottle Charges in case first time
 *         in: formData
 *         required: true
 *         type: number
 *       - name: quantity
 *         description: Quantity of bottles
 *         in: formData
 *         required: true
 *         type: number
 *       - name: payment_type
 *         description: Payment Type
 *         in: formData
 *         required: true
 *         type: string
 *         enum: ["Cash", "Card"]
 *       - name: price
 *         description: Price of Etoken
 *         in: formData
 *         required: true
 *         type: number
 *       - name: eToken_quantity
 *         description: Quantity of etokens purchased
 *         in: formData
 *         required: true
 *         type: number
 *       - name: address
 *         description: Address
 *         in: formData
 *         required: true
 *         type: string 
 *       - name: address_latitude
 *         description: Address Latitude
 *         in: formData
 *         required: true
 *         type: number
 *       - name: address_longitude
 *         description: Address Longitude
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/etoken/purchase", [Libs.middlewareFunctions.loginInRequired], Controllers.userWaterController.usereTokenPurchase);
/**
 * @swagger
 * /user/water/confirm/order:
 *   post:
 *     description: For User Confirming A drinking water order
 *     tags: [User Water]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: order_id
 *         description: order_id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: status
 *         description: Accepting or Rejecting Order
 *         in: formData
 *         required: true
 *         type: string
 *         enum: ["0", "1"]
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/confirm/order", [Libs.middlewareFunctions.loginInRequired], Controllers.userWaterController.userConfirmOrder);

module.exports = router;