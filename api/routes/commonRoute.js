var express = require("express");
var router = express.Router();

var Controllers = require(appRoot+"/controllers");///////////////////////		Controllers
var Libs = require(appRoot+"/libs");//////////////////		Libraries

/**
 * @swagger
 * /common/Orgs:
 *   post:
 *     description: For Org Listings
 *     tags: [Common Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/orgs", [Libs.middlewareFunctions.loginInRequired], Controllers.commonController.orgListings);
/**
 * @swagger
 * /common/eContacts:
 *   post:
 *     description: For Getting emergency contact details
 *     tags: [Common Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/eContacts", [Libs.middlewareFunctions.loginInRequired], Controllers.commonController.eContact);
/**
 * @swagger
 * /common/updateData:
 *   post:
 *     description: For Updating Details after logging
 *     tags: [Common Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: language_id
 *         in: header
 *         description: Old Language Id in Header
 *         required: false
 *         type: string
 *       - name: timezone
 *         description: Timezone Eg - Asia/Calcutta.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: latitude
 *         description: Latitude Default -0
 *         in: formData
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: Longitude Default -0
 *         in: formData
 *         required: true
 *         type: number
 *       - name: fcm_id
 *         description: FCM Id for push notifications
 *         in: formData
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/updateData", [Libs.middlewareFunctions.loginInRequired], Controllers.commonController.updateData);
/**
 * @swagger
 * /common/orgSearch:
 *   post:
 *     description: For Searching Organisation
 *     tags: [Common Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: licence_number
 *         description: Licence Number
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/orgSearch", [Libs.middlewareFunctions.loginInRequired], Controllers.commonController.orgSearch);
/**
 * @swagger
 * /common/updateOnline:
 *   post:
 *     description: For Updating Online Status '0' or '1'
 *     tags: [Common Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: online_status
 *         description: Online status updated successfully
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/updateOnline", [Libs.middlewareFunctions.loginInRequired], Controllers.commonController.updateOnline);
/**
 * @swagger
 * /common/serviceBrands:
 *   post:
 *     description: For Single Service Brands with their products
 *     tags: [Common Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: category_id
 *         description: Service Category Id
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/serviceBrands", [Libs.middlewareFunctions.loginInRequired], Controllers.commonController.serviceBrands);
/**
 * @swagger
 * /common/brandProducts:
 *   post:
 *     description: For Brand all products
 *     tags: [Common Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: category_brand_id
 *         description: Category Brand Id
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/brandProducts", [Libs.middlewareFunctions.loginInRequired], Controllers.commonController.brandProducts);
/**
 * @swagger
 * /common/contactus:
 *   post:
 *     description: For Contacting Admin
 *     tags: [Common Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: message
 *         description: Message
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/contactus", [Libs.middlewareFunctions.loginInRequired], Controllers.commonController.contactus);
/**
 * @swagger
 * /common/logout:
 *   post:
 *     description: For Logout
 *     tags: [Common Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/logout", [Libs.middlewareFunctions.loginInRequired], Controllers.commonController.logout);
/**
 * @swagger
 * /common/updateMap:
 *   post:
 *     description: For Updating track image or static map
 *     tags: [Common Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: order_id
 *         description: Order Id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: track_path
 *         description: Track Path
 *         in: formData
 *         required: true
 *         type: string
 *       - name: track
 *         description: Track Image with markers or path
 *         in: formData
 *         required: false
 *         type: file
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/updateMap", [Libs.middlewareFunctions.loginInRequired, upload.single("track")], Controllers.commonController.addTrackImage);
/**
 * @swagger
 * /common/support/list:
 *   post:
 *     description: For Support list
 *     tags: [Common Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */

router.post("/support/list", [Libs.middlewareFunctions.loginInRequired], Controllers.commonController.supportsList);
/**
 * @swagger
 * /common/settingUpdate:
 *   post:
 *     description: For Setting Update
 *     tags: [Common Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: access_token
 *         in: header
 *         description: Access Token in Header
 *         required: true
 *         type: string
 *       - name: language_id
 *         in: header
 *         description: Language Id in Header
 *         required: false
 *         type: string
 *       - name: notifications
 *         description: 1 for On, 0 for Off
 *         in: formData
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/settingUpdate", [Libs.middlewareFunctions.loginInRequired], Controllers.commonController.settingUpdate);
/**
 * @swagger
 * /common/testPush:
 *   post:
 *     description: For Testing Push
 *     tags: [Common Api]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: fcm_id
 *         in: formData
 *         description: FCM ID
 *         required: true
 *         type: string
 *       - name: device_type
 *         description: Device Type
 *         in: formData
 *         required: true
 *         enum: ['Ios', 'Android']
 *         type: string
 *       - name: silent
 *         description: 1 for On, 0 for Off
 *         in: formData
 *         required: false
 *         enum: ["0", "1"]
 *         type: string
 *       - name: order_id
 *         description: Order id
 *         in: formData
 *         required: true
 *         type: number
 *       - name: type
 *         description: Type of push
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Api Error
 */
router.post("/testPush", Controllers.commonController.testPush);


module.exports = router;