var express = require("express");
var router = express.Router();

router.get("/", function(req, response) {
	response.render("index", { title: "Express" });
});

router.get("/PPolicy", function(req, response) {
	return response.render("Pages/1En/privacy", { title: response.trans("Privacy Terms and Conditions") });
});

/* GET home page. */
router.get("/pages/:userType/:lang?", function(req, response) {
//	http://192.168.100.45:9006/pages/Customer/1

	var data = req.params;

	if(data.userType == "Customer")
	{/////////		Customers 	/////////////////////////////

		if(!data.lang || data.lang == "1")
		{
			return response.render("Pages/1En/userTC", { title: response.trans("Customer Terms and Conditions") });
		}
		else if(data.lang == "2")
		{
			return response.render("Pages/2Hi/userTC", { title: response.trans("Customer Terms and Conditions") });
		}
		else if(data.lang == "3")
		{
			return response.render("Pages/3Ur/userTC", { title: response.trans("Customer Terms and Conditions") });
		}
		else if(data.lang == "4")
		{
			return response.render("Pages/4Ch/userTC", { title: response.trans("Customer Terms and Conditions") });
		}
		else if(data.lang == "5")
		{
			return response.render("Pages/5Ar/userTC", { title: response.trans("Customer Terms and Conditions") });
		}

	}/////////		Customers 	/////////////////////////////
	else if(req.param("userType") == "Service")
	{/////////		Services 	/////////////////////////////

		if(!data.lang || data.lang == "1")
		{
			return response.render("Pages/1En/serviceTC", { title: response.trans("Service Terms and Conditions") });
		}
		else if(data.lang == "2")
		{
			return response.render("Pages/2Hi/serviceTC", { title: response.trans("Service Terms and Conditions") });
		}
		else if(data.lang == "3")
		{
			return response.render("Pages/3Ur/serviceTC", { title: response.trans("Service Terms and Conditions") });
		}
		else if(data.lang == "4")
		{
			return response.render("Pages/4Ch/serviceTC", { title: response.trans("Service Terms and Conditions") });
		}
		else if(data.lang == "5")
		{
			return response.render("Pages/5Ar/serviceTC", { title: response.trans("Service Terms and Conditions") });
		}

	}/////////		Services 	/////////////////////////////
	else if(req.param("userType") == "Support")
	{/////////		Supports 	/////////////////////////////

		if(!data.lang || data.lang == "1")
		{
			return response.render("Pages/1En/supportTC", { title: response.trans("Support Terms and Conditions") });
		}
		else if(data.lang == "2")
		{
			return response.render("Pages/2Hi/supportTC", { title: response.trans("Support Terms and Conditions") });
		}
		else if(data.lang == "3")
		{
			return response.render("Pages/3Ur/supportTC", { title: response.trans("Support Terms and Conditions") });
		}
		else if(data.lang == "4")
		{
			return response.render("Pages/4Ch/supportTC", { title: response.trans("Support Terms and Conditions") });
		}
		else if(data.lang == "5")
		{
			return response.render("Pages/5Ar/supportTC", { title: response.trans("Support Terms and Conditions") });
		}

	}/////////		Supports 	/////////////////////////////

	//console.log("data.langdata.langdata.langdata.langdata.lang", data.usertype, data.lang);

	response.render("index", { title: "express" });
});

module.exports = router;