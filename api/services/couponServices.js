var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;

var Db = require(appRoot+"/models");//////////////////		Db Models

//////////////////////		Latest Create Row 	//////////////////////////////////////

exports.CouponsListings = async (data) => {

	return await Db.sequelize.query("SELECT coupon_id, amount_value, rides_value, coupon_type, price, expires_at, (SELECT COUNT(*) FROM coupon_users as cu WHERE cu.coupon_id=c.coupon_id) as purchase_counts FROM coupons as c WHERE ( c.blocked='0' AND c.expires_at >= now() )  ORDER BY purchase_counts DESC",{ type: Sequelize.QueryTypes.SELECT});

};

///////////////////////		Single Coupon Details 	//////////////////////////////////
exports.couponDetails = async (data) => {

	return await Db.coupons.findOne({

		where: {
			coupon_id: data.coupon_id,
			blocked: "0",
			expires_at: {$gte: data.created_at}
		}

	});

};

///////////////////////