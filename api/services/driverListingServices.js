var moment = require("moment");
var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;

var Db = require(appRoot+"/models");//////////////////		Db Models

/////////////////////		User home Map Drivers 		////////////////////////////////////
exports.userHomeMapDrivers = async (data) => {

	return await Db.sequelize.query("SELECT ud.user_detail_id, ud.bearing, ud.user_type_id, ud.category_id, ud.category_brand_id, ud.organisation_id, ud.latitude as latitude, ud.longitude as longitude, ROUND(((3959*acos(cos(radians("+data.latitude+")) * cos(radians(ud.latitude)) * cos(radians(ud.longitude)-radians("+data.longitude+"))+sin(radians("+data.latitude+"))*sin(radians(ud.latitude))))* 1.67),2) as distance, COALESCE(o.blocked, '0') as o_blocked, (CASE WHEN o.organisation_id IS NULL THEN 1 ELSE (SELECT COUNT(*) FROM organisation_categories as oc WHERE oc.organisation_id=o.organisation_id AND oc.category_id=ud.category_id AND oc.category_type='"+data.category_type+"') END) as o_serving, ud.updated_at as updated_at FROM user_details as ud LEFT JOIN organisations as o ON o.organisation_id=ud.organisation_id WHERE ud.user_type_id="+data.user_type_id+" AND ud.online_status='1' AND ud.otp='' AND ud.access_token != '' AND ud.blocked != '1' AND ud.category_id="+data.category_id+" AND ud.user_id!="+data.user_id+" HAVING distance <= "+data.distance+" AND o_blocked='0' AND o_serving > 0 AND updated_at >= '"+data.past_5Mins+"' ORDER BY distance ASC LIMIT 0, 8 ",{ type: Sequelize.QueryTypes.SELECT});

};

////////////////	Trucks Drivers Only 		////////////////////////////////////////////
exports.Driver4TruckOrders = (data) => {

	return Db.sequelize.query("SELECT *, ud.user_id, ud.notifications, ud.device_type, ud.language_id, ud.user_detail_id, ud.socket_id,u.name, ud.latitude, ud.longitude, ud.organisation_id, COALESCE(oc.buraq_percentage, udds.buraq_percentage) as buraq_percentage, COALESCE(o.bottle_charge, u.bottle_charge) as bottle_charge, COALESCE(o.blocked, '0') as org_blocked, (CASE WHEN ud.organisation_id = 0 THEN '1' ELSE (CASE WHEN oc.organisation_category_id IS NOT NULL THEN '1' ELSE '0' END) END) as org_cat FROM user_details as ud JOIN users as u ON (u.user_id=ud.user_id AND u.blocked='0') JOIN user_driver_detail_products as uddp ON (uddp.user_detail_id=ud.user_detail_id AND uddp.user_id=ud.user_id AND uddp.category_brand_product_id IN ('"+data.CategoryProductsID+"') AND uddp.deleted='0') JOIN user_driver_detail_services as udds ON (udds.user_detail_id=ud.user_detail_id AND udds.deleted='0' AND udds.category_id="+data.category_id+") LEFT JOIN organisations as o ON (o.organisation_id=ud.organisation_id) LEFT JOIN organisation_categories as oc ON (oc.organisation_id=ud.organisation_id AND oc.category_id="+data.category_id+") WHERE (ud.user_type_id="+data.user_type_id+" AND ud.category_id="+data.category_id+" AND ud.category_brand_id="+data.category_brand_id+" AND ud.online_status='1' AND ud.otp='' AND ud.access_token!='' AND ud.mulkiya_number!='' AND ud.blocked!='1' AND ud.user_id!="+data.user_id+") HAVING ( (org_blocked='0') AND (org_cat='1') AND (ROUND(((3959*acos(cos(radians("+data.pickup_latitude+")) * cos(radians(ud.latitude)) * cos(radians(ud.longitude)-radians("+data.pickup_longitude+"))+sin(radians("+data.pickup_latitude+"))*sin(radians(ud.latitude))))* 1.67),2) <= "+data.distance+") AND ((SELECT COUNT(*) FROM orders WHERE driver_user_detail_id=ud.user_detail_id AND order_status IN ('Ongoing','Accepted', 'Confirmed', 'DPending', 'DApproved') ) = 0)  )  ",{ type: Sequelize.QueryTypes.SELECT});

};



////////////////	Single Driver 4 Water 	////////////////////////////////////////////////
exports.SingleDriver4OrdersDrinkingWaterOnly = async (data) => {
// online_status

	return await Db.sequelize.query("SELECT ud.user_id, ud.notifications, ud.device_type, ud.organisation_id, ud.user_type_id, ud.language_id, ud.category_id, ud.category_brand_id, ud.profile_pic, ud.user_detail_id, ud.socket_id, ud.latitude, ud.longitude, ud.fcm_id, ud.mulkiya_number, u.name, u.email, u.phone_code, u.phone_number, ud.organisation_id, udds.buraq_percentage as buraq_percentage, u.bottle_charge FROM user_details as ud JOIN users as u ON (u.user_id=ud.user_id AND u.blocked='0') JOIN user_driver_detail_services as udds ON (udds.user_detail_id=ud.user_detail_id AND udds.deleted='0' AND udds.category_id="+data.category_id+") WHERE (ud.user_detail_id="+data.driver_user_detail_id+" AND ud.user_type_id="+data.user_type_id+" AND ud.category_id="+data.category_id+" AND ud.category_brand_id="+data.category_brand_id+" AND ud.online_status='1' AND ud.otp='' AND ud.access_token!='' AND ud.mulkiya_number!='' AND ud.blocked!='1' AND ud.user_id!="+data.user_id+") HAVING ( (ROUND(((3959*acos(cos(radians("+data.dropoff_latitude+")) * cos(radians(ud.latitude)) * cos(radians(ud.longitude)-radians("+data.dropoff_longitude+"))+sin(radians("+data.dropoff_latitude+"))*sin(radians(ud.latitude))))* 1.67),2) <= "+data.distance+")  ", {type: Sequelize.QueryTypes.SELECT});

};

var area_check_string_func = function(latitude, longitude, category_id, day_string_check)
{

	return "(SELECT COUNT(*) FROM organisation_areas as oa where oa.organisation_id=o.organisation_id AND oa.category_id="+category_id+" AND oa.deleted='0' AND oa.blocked='0' AND "+day_string_check+" AND ( ROUND(((3959*acos(cos(radians("+latitude+")) * cos(radians(oa.address_latitude)) * cos(radians(oa.address_longitude)-radians("+longitude+"))+sin(radians("+latitude+"))*sin(radians(oa.address_latitude))))* 1.67),2)  <= oa.distance )  ) ";

};

//////////////		Single Org Drivers 4 Water 	////////////////////////////////////////////
exports.singleOrgDrivers4OrdersDrinkingWater = async (data) => {

	var area_check_string = area_check_string_func(data.dropoff_latitude, data.dropoff_longitude, data.category_id, data.day_string_check);

	return await Db.sequelize.query("SELECT ud.user_id, ud.organisation_id, ud.user_type_id, ud.language_id, ud.category_id, ud.category_brand_id, ud.profile_pic, ud.user_detail_id, ud.socket_id, ud.latitude, ud.longitude, ud.fcm_id, ud.mulkiya_number, u.name, ud.notifications, ud.device_type, u.email, u.phone_code, u.phone_number, oc.buraq_percentage, o.bottle_charge, "+area_check_string+" as area_check  FROM user_details as ud JOIN users as u ON (u.user_id=ud.user_id AND u.blocked='0') JOIN user_driver_detail_services as udds ON (udds.user_detail_id=ud.user_detail_id AND udds.deleted='0' AND udds.category_id="+data.category_id+") JOIN organisations as o ON (o.organisation_id=ud.organisation_id AND o.blocked='0') JOIN organisation_categories as oc ON (oc.organisation_id=ud.organisation_id AND oc.category_id="+data.category_id+") WHERE (ud.organisation_id="+data.driver_organisation_id+" AND ud.user_type_id="+data.user_type_id+" AND ud.category_id="+data.category_id+" AND ud.category_brand_id="+data.category_brand_id+" AND ud.online_status='1' AND ud.otp='' AND ud.access_token!='' AND ud.mulkiya_number!='' AND ud.blocked!='1' AND ud.user_id!="+data.user_id+") HAVING ( (ROUND(((3959*acos(cos(radians("+data.dropoff_latitude+")) * cos(radians(ud.latitude)) * cos(radians(ud.longitude)-radians("+data.dropoff_longitude+"))+sin(radians("+data.dropoff_latitude+"))*sin(radians(ud.latitude))))* 1.67),2) <= "+data.distance+")  ", {type: Sequelize.QueryTypes.SELECT});

};

//////////////	All Drivers 4 Orders Drinking Water 	////////////////
exports.AllDrivers4OrdersDrinkingWater = async (data) => {

	var area_check_string = area_check_string_func(data.dropoff_latitude, data.dropoff_longitude, data.category_id, data.day_string_check);

	return await Db.sequelize.query("SELECT ud.user_id, ud.organisation_id, ud.user_type_id, ud.language_id, ud.category_id, ud.category_brand_id, ud.profile_pic, ud.user_detail_id, ud.socket_id, ud.latitude, ud.longitude, ud.fcm_id, ud.mulkiya_number, u.name, ud.notifications, ud.device_type, u.email, u.phone_code, u.phone_number, COALESCE(oc.buraq_percentage, udds.buraq_percentage) as buraq_percentage, COALESCE(o.bottle_charge, u.bottle_charge) as bottle_charge, COALESCE(o.blocked, '0') as org_blocked, (CASE WHEN ud.organisation_id = 0 THEN '1' ELSE (CASE WHEN oc.organisation_category_id IS NOT NULL THEN '1' ELSE '0' END) END) as org_cat, (CASE WHEN o.organisation_id IS NULL THEN 1 ELSE "+area_check_string+"  END) as area_check  FROM user_details as ud JOIN users as u ON (u.user_id=ud.user_id AND u.blocked='0') JOIN user_driver_detail_products as uddp ON (uddp.user_detail_id=ud.user_detail_id AND uddp.user_id=ud.user_id AND uddp.category_brand_product_id IN ('"+data.CategoryProductsID+"') AND uddp.deleted='0') JOIN user_driver_detail_services as udds ON (udds.user_detail_id=ud.user_detail_id AND udds.deleted='0' AND udds.category_id="+data.category_id+") LEFT JOIN organisations as o ON (o.organisation_id=ud.organisation_id) LEFT JOIN organisation_categories as oc ON (oc.organisation_id=ud.organisation_id AND oc.category_id="+data.category_id+") WHERE (ud.user_type_id="+data.user_type_id+" AND ud.category_id="+data.category_id+" AND ud.category_brand_id="+data.category_brand_id+" AND ud.online_status='1' AND ud.otp='' AND ud.access_token!='' AND ud.mulkiya_number!='' AND ud.blocked!='1' AND ud.user_id!="+data.user_id+") HAVING ( (org_blocked='0') AND (org_cat='1') AND (ROUND(((3959*acos(cos(radians("+data.dropoff_latitude+")) * cos(radians(ud.latitude)) * cos(radians(ud.longitude)-radians("+data.dropoff_longitude+"))+sin(radians("+data.dropoff_latitude+"))*sin(radians(ud.latitude))))* 1.67),2) <= "+data.distance+")  )  ",{ type: Sequelize.QueryTypes.SELECT});

};

/////////////////////		Drivers 4 Ride Except Drinking water 	////////////////////////
exports.Driver4OrdersExceptDrinkingWater = async (data) => {

	return await Db.sequelize.query("SELECT *, ud.user_id, ud.notifications,u.name, ud.category_id, ud.category_brand_id, ud.device_type, ud.language_id, ud.user_detail_id, ud.socket_id, ud.latitude, ud.longitude, ud.organisation_id, COALESCE(oc.buraq_percentage, udds.buraq_percentage) as buraq_percentage, COALESCE(o.bottle_charge, u.bottle_charge) as bottle_charge, COALESCE(o.blocked, '0') as org_blocked, (CASE WHEN ud.organisation_id = 0 THEN '1' ELSE (CASE WHEN oc.organisation_category_id IS NOT NULL THEN '1' ELSE '0' END) END) as org_cat FROM user_details as ud JOIN users as u ON (u.user_id=ud.user_id AND u.blocked='0') JOIN user_driver_detail_products as uddp ON (uddp.user_detail_id=ud.user_detail_id AND uddp.user_id=ud.user_id AND uddp.category_brand_product_id IN ('"+data.CategoryProductsID+"') AND uddp.deleted='0') JOIN user_driver_detail_services as udds ON (udds.user_detail_id=ud.user_detail_id AND udds.deleted='0' AND udds.category_id="+data.category_id+") LEFT JOIN organisations as o ON (o.organisation_id=ud.organisation_id) LEFT JOIN organisation_categories as oc ON (oc.organisation_id=ud.organisation_id AND oc.category_id="+data.category_id+") WHERE (ud.user_type_id="+data.user_type_id+" AND ud.category_id="+data.category_id+" AND ud.category_brand_id="+data.category_brand_id+" AND ud.online_status='1' AND ud.otp='' AND ud.access_token!='' AND ud.mulkiya_number!='' AND ud.blocked!='1' AND ud.user_id!="+data.user_id+") HAVING ( (org_blocked='0') AND (org_cat='1') AND (ROUND(((3959*acos(cos(radians("+data.dropoff_latitude+")) * cos(radians(ud.latitude)) * cos(radians(ud.longitude)-radians("+data.dropoff_longitude+"))+sin(radians("+data.dropoff_latitude+"))*sin(radians(ud.latitude))))* 1.67),2) <= "+data.distance+") )  ",{ type: Sequelize.QueryTypes.SELECT});

};

/////////////////////		Get Drivers for Booking 		////////////////////////////////
exports.userServiceBookingDriverListings = async (data) => {

	return await Db.sequelize.query("SELECT *, ud.user_id as user_id, ud.notifications, ud.device_type, ud.language_id, ud.user_detail_id as user_detail_id, ud.socket_id as socket_id, ud.latitude as latitude, ud.longitude as longitude, ud.organisation_id as organisation_id, ROUND(((3959*acos(cos(radians("+data.dropoff_latitude+")) * cos(radians(ud.latitude)) * cos(radians(ud.longitude)-radians("+data.dropoff_longitude+"))+sin(radians("+data.dropoff_latitude+"))*sin(radians(ud.latitude))))* 1.67),2) as distance, COALESCE(o.blocked, '0') as o_blocked, (CASE WHEN o.organisation_id IS NULL THEN 1 ELSE (SELECT COUNT(*) FROM organisation_categories as oc WHERE oc.organisation_id=o.organisation_id AND oc.category_id=ud.category_id AND oc.category_type='"+data.category_type+"') END) as o_serving FROM user_details as ud JOIN user_driver_detail_products as uddp ON uddp.user_detail_id=ud.user_detail_id LEFT JOIN organisations as o ON o.organisation_id=ud.organisation_id WHERE (uddp.category_brand_product_id="+data.category_brand_product_id+" AND uddp.deleted='0' AND uddp.user_id=ud.user_id) AND (ud.user_type_id="+data.user_type_id+" AND ud.category_id="+data.category_id+" AND ud.category_brand_id="+data.category_brand_id+" AND ud.online_status='1' AND ud.otp='' AND ud.access_token!='' AND ud.mulkiya_number!='' AND ud.blocked!='1' AND ud.user_id!="+data.user_id+") HAVING ( (distance <= "+data.distance+") AND (o_blocked='0') AND (o_serving > 0) )  ",{ type: Sequelize.QueryTypes.SELECT});

};

/////////////////		Admin Track Drivers 	////////////////////////////////////
exports.adminDriverMaps = async (data) => {

	return await await Db.user_details.findAll({

		where: {
			category_id: data.category_ids,
			latitude: {$ne: 0.0},
			longitude: {$ne: 0.0},
			online_status: data.online
		},
		attributes: [
			"user_detail_id", "category_id", "latitude", "longitude", "user_detail_id", "user_id", "user_type_id"
		],
		include: [
			{
				model: Db.users,
				attributes: [
					"name", "phone_number"
				]
			}
		]

	});

};

//////////////////		Organisation Drivers 	/////////////////////////////////////
exports.orgDriverMaps = async (data) => {
	return await await Db.user_details.findAll({

		where: {
			organisation_id: data.organisation_id,
			category_id: data.category_ids,
			latitude: {$ne: 0.0},
			longitude: {$ne: 0.0},
			online_status: data.online
		},
		attributes: [
			"user_detail_id", "category_id", "latitude", "longitude", "user_detail_id", "user_id", "user_type_id"
		],
		include: [
			{
				model: Db.users,
				attributes: [
					"name", "phone_number"
				]
			}
		]

	});

};


/////////////////		Admin Track Recovery Breakdown Truck 	////////////////////////////////////
exports.breakdownRecoveryTracking = async (data) => {
	return await await Db.user_details.findAll({
		where: {
			category_id: data.category_ids,
                        category_brand_id: 9,
			latitude: {$ne: 0.0},
			longitude: {$ne: 0.0},
			online_status: data.online
		},
		attributes: ["user_detail_id", "category_id", "latitude", "longitude", "user_id", "user_type_id"],
		include: [{
				model: Db.users,
				attributes: [
					"name", "phone_number"
				]
			}]
	});
};
/////////////////////
//Add M2 Part 2
//Add Sent Offer
//var date = moment(data.expire_date, "YYYY-MM-DD").tz(data.timezone).format("YYYY-MM-DD");
exports.addReleaseOffer = async (data) => {
    return Db.release_offers.create({
        driver_id: data.user_id,
        start_address: data.start_address,
        end_address: data.end_address,
        start_latitude: data.start_latitude,
        start_longitude: data.start_longitude,
        end_latitude: data.end_latitude,
        end_longitude: data.end_longitude,
        margin: "",
		offer_price: data.offer_price,
		product_id: data.category_brand_product_id,
		expire_date: moment(data.expire_date, "YYYY-MM-DD").tz(data.Details.timezone).format("YYYY-MM-DD"),
        status: "Pending"
    })
};

//////////////////		Organisation Drivers 	/////////////////////////////////////
exports.DriverServiceDetails = async (data) => {
	return await Db.sequelize.query("SELECT cbp.price_per_weight AS product_weight,cbp.gift_offer,cbp.category_brand_id, cbp.price_per_quantity AS price_per_item,cbp.image AS image_url, cbp.category_brand_product_id AS category_brand_product_id,cbpd.name AS productName, cbp.min_quantity AS product_quantity FROM category_brand_products AS cbp JOIN category_brand_product_details AS cbpd ON cbpd.category_brand_product_id = cbp.category_brand_product_id WHERE(cbpd.category_brand_product_id = "+data.productId+" AND cbpd.language_id = "+data.app_language_id+")",{ type: Sequelize.QueryTypes.SELECT});
};
//////////////////		Organisation Drivers 	/////////////////////////////////////
exports.SingleTruckOrders = async (driverId) => {
	return Db.sequelize.query("SELECT *, ud.user_id,ud.notifications,ud.device_type,ud.language_id,ud.user_detail_id,ud.socket_id, ud.latitude,ud.longitude,ud.organisation_id FROM user_details AS ud JOIN users AS u ON( u.user_id = ud.user_id AND u.blocked = '0') WHERE ( ud.user_id = "+driverId+" AND ud.user_type_id = '2')",{ type: Sequelize.QueryTypes.SELECT});
};

exports.getDriverProductList = async (driverId,app_language_id) => {
	return Db.sequelize.query("SELECT uddp.*,cbpd.name,c.transit_offer_percentage,c.transit_buraq_margin FROM user_driver_detail_products AS uddp JOIN category_brand_product_details AS cbpd ON(cbpd.category_brand_product_id = uddp.category_brand_product_id) JOIN categories AS c ON(c.category_id = cbpd.category_id) WHERE (uddp.user_id = "+driverId+" AND cbpd.language_id = "+app_language_id+") ORDER BY cbpd.name DESC",{ type: Sequelize.QueryTypes.SELECT});
};
////////////////	Trucks Drivers Only 		////////////////////////////////////////////
exports.SingleDriver4TruckOrders = (id) => {
	return Db.sequelize.query("SELECT *, ud.user_id,ud.notifications,ud.device_type,ud.language_id,ud.user_detail_id,ud.socket_id, ud.latitude,ud.longitude,ud.organisation_id FROM user_details AS ud JOIN users AS u ON( u.user_id = ud.user_id AND u.blocked = '0') WHERE ( ud.user_detail_id = "+id+")",{ type: Sequelize.QueryTypes.SELECT});

};
//////// Get Driver Offers //////////
exports.getDriverOffers = (driverId,app_language_id) => {
	var startdate = moment().subtract(15, "days").format("YYYY-MM-DD");
	console.log(startdate);
	return Db.sequelize.query("SELECT ud.*,cbpd.name AS productName,cbd.name AS brandName,c.transit_offer_percentage,c.transit_buraq_margin FROM release_offers AS ud JOIN category_brand_product_details AS cbpd ON(cbpd.category_brand_product_id = ud.product_id)JOIN category_brand_details AS cbd ON(cbd.category_brand_id = cbpd.category_brand_id  )JOIN categories AS c ON(cbd.category_id = c.category_id)WHERE driver_id = "+driverId+" AND cbpd.language_id = "+app_language_id+" AND cbd.language_id = "+app_language_id+" AND DATE(ud.created_at) >= '"+startdate+"' ORDER BY release_offer_id DESC",{ type: Sequelize.QueryTypes.SELECT});
};
//////// Get Customers Near By For Send Notification for Sent Offer /////////
exports.getNearByCustomer = (data) => {
	return Db.sequelize.query("SELECT *,ud.user_id,u.name,ud.notifications,ud.device_type,ud.language_id,ud.user_detail_id,ud.socket_id,ud.latitude,ud.longitude,ud.organisation_id FROM user_details AS ud JOIN users AS u ON(u.user_id = ud.user_id AND u.blocked = '0')WHERE(ud.user_type_id = '1' AND ud.access_token != '' AND ud.blocked != '1') HAVING (ROUND(((3959 * ACOS(COS(RADIANS("+data.start_latitude+")) * COS(RADIANS(ud.latitude)) * COS(RADIANS(ud.longitude) - RADIANS("+data.start_longitude+")) + SIN(RADIANS("+data.start_latitude+")) * SIN(RADIANS(ud.latitude)))) * 1.67),2) <= '100')",{ type: Sequelize.QueryTypes.SELECT});
};

//////// Get Customers Near By For Send Notification for Sent Offer /////////
exports.CheckDriverOngoingOrder = (id) => {
	return Db.sequelize.query("SELECT COUNT(*) as totalorder FROM orders WHERE driver_user_detail_id = "+id+" AND order_status IN( 'Ongoing','Accepted','Confirmed', 'DPending','DApproved')",{ type: Sequelize.QueryTypes.SELECT});
};