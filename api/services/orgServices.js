var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;

var Db = require(appRoot+"/models");//////////////////		Db Models

///////////////		Org Listings 	//////////////////////
exports.appGetOrgs = async (data) => {

	return await Db.sequelize.query("SELECT o.organisation_id, o.name, o.licence_number, o.image, CONCAT('"+process.env.RESIZE_URL+"',o.image) as image_url FROM organisations as o WHERE o.blocked='0' ORDER BY o.sort_order ", { type: Sequelize.QueryTypes.SELECT});

};
/////////////////////