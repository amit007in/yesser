var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;

var Db = require(appRoot+"/models");//////////////////		Db Models

//////////////////////		Latest Create Row 	//////////////////////////////////////
exports.CreateNewRow = async (data) => {

	return await Db.payments.create({
		//////////////		User 	///////////////////
		customer_user_id: data.user_id,
		customer_user_detail_id: data.user_detail_id,
		customer_user_type_id: data.user_type_id,
		user_card_id: data.user_card_id,
		customer_organisation_id: data.user_organisation_id,
		//////////////		User 	///////////////////
		order_id: data.order_id,
		coupon_id: data.coupon_id,
		organisation_coupon_id: data.organisation_coupon_id,

		seller_user_id: data.seller_user_id,
		seller_user_detail_id: data.seller_user_detail_id,
		seller_user_type_id: data.seller_user_type_id,
		seller_organisation_id: data.seller_organisation_id,

		payment_type: "Card",
		payment_status: data.payment_status,
		refund_status: "NoNeed",

		transaction_id: data.transaction_id,
		refund_id: "",

		buraq_percentage: data.buraq_percentage,

		product_actual_value: data.product_actual_value,
		product_quantity: 1,
		product_weight: "0",
		order_distance: "0",

		product_alpha_charge: "0",
		product_per_quantity_charge: "0",
		product_per_weight_charge: "0",
		product_per_distance_charge: "0",

		initial_charge: data.initial_charge,
		admin_charge: data.admin_charge,
		bank_charge: data.bank_charge,
		final_charge: data.final_charge,

		bottle_charge: "0",
		bottle_returned_value: "0",

		created_at: data.created_at,
		updated_at: data.created_at
	});

};

//////////////////////	New Create Payments 		////////////////////////////////////////
//exports.createeTokenPayment =

/////////////////////		Driver Payments 	////////////////////////////////////////////
exports.driverEarningHistory = async (driver_detail_id, start_dt, end_dt, app_language_id) => {

	return Db.payments.findAll({

		include: [
			{
				required: true,
				model: Db.orders,
				as: "Order"
			}
		],

		where: {
			seller_user_detail_id: driver_detail_id,
			updated_at: {
        	$between: [start_dt, end_dt]
    	}
		},

		order: [ ["updated_at", "ASC"] ]

	})

		.then( (payments) => {

			var promises = [];

			payments = JSON.parse(JSON.stringify(payments));

			payments.forEach(function(payment) {
				// 	order
				promises.push(
					Promise.all([

						orderBrandDetails(payment.Order.category_id, payment.Order.category_brand_id, payment.Order.category_brand_product_id, app_language_id),
					])
						.spread(function(brand) {

							payment.brand = brand.length > 0 ? brand[0] : {};

							return payment;

          		})
				);

			});

			return Promise.all(promises);

		});




	//orderBrandDetails = async (category_id, category_brand_id, category_brand_product_id, app_language_id) => {

};

///////////////////////////		Order Brand Details 	////////////////////////////

orderBrandDetails = async (category_id, category_brand_id, category_brand_product_id, app_language_id) => {

	return Db.sequelize.query("SELECT cbpd.category_brand_product_id, cbpd.name as product_name, cbd.category_brand_id, cbpd.name as name, cbd.name as brand_name, cd.name as category_name, cbp.image, (CASE WHEN cbp.image='' THEN '' ELSE CONCAT('"+process.env.RESIZE_URL+"',cbp.image) END) as image_url FROM category_brand_product_details as cbpd JOIN category_brand_products AS cbp ON (cbp.category_brand_product_id=cbpd.category_brand_product_id) JOIN category_brand_details as cbd ON (cbd.category_brand_id="+category_brand_id+" AND cbd.language_id="+app_language_id+") JOIN category_details as cd ON (cd.category_id=cbpd.category_id AND cd.language_id="+app_language_id+") WHERE cbpd.category_brand_product_id="+category_brand_product_id+" AND cbpd.language_id="+app_language_id+" LIMIT 0, 1 ",
		    { type: Sequelize.QueryTypes.SELECT}
		        );

};


////////////////////