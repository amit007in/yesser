var bcrypt = require("bcrypt-nodejs");

var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;

var Db = require(appRoot+"/models");//////////////////		Db Models

///////////////		User Detail Insert 	////////////////////////////////
exports.createNewRow = async (data) => {

	return await Db.user_details.create({

		user_id: data.user_id,

		user_type_id: data.user_type_id,
		language_id: data.app_language_id, //data.language_id ? data.language_id : data.app_language_id,
		category_id: data.category_id ? data.category_id : 0,
		category_brand_id: data.category_brand_id ? data.category_brand_id : 0,

		online_status: "0",
		otp: data.otp ? data.otp : "",

		password: data.password ? bcrypt.hashSync(data.password) : "",

		profile_pic: data.profile_pic ? data.profile_pic : "",
		access_token: data.access_token,

		latitude: data.latitude ? data.latitude : 0.0,
		longitude: data.longitude ? data.longitude : 0.0,

		timezone: data.timezone ? data.timezone : "Asia/Kolkata",
		timezonez: data.timezonez ? data.timezonez : "+05:30",

		socket_id: data.socket_id ? data.socket_id : "",
		fcm_id: data.fcm_id ? data.fcm_id : "",
		device_type: data.device_type ? data.device_type : "Android",

		mulkiya_number: data.mulkiya_number ? data.mulkiya_number : "",
		mulkiya_front: data.mulkiya_front_name ? data.mulkiya_front_name : "",
		mulkiya_back: data.mulkiya_back_name ? data.mulkiya_back_name : "",
		mulkiya_validity: data.mulkiya_validity ? data.mulkiya_validity : null,

		maximum_rides: data.maximum_rides ? data.maximum_rides : 10,

		created_at: data.created_at,
		updated_at: data.created_at

	});

};

/////////////////////////		Update User Detail Record 		//////////////////
exports.updateRow = async (data, oldDetails) => {

	return await Db.user_details.update(
		{

			language_id: data.app_language_id, //data.language_id ? data.language_id : oldDetails.language_id,
			category_id: data.category_id ? data.category_id : oldDetails.category_id,
			category_brand_id: data.category_brand_id ? data.category_brand_id : oldDetails.category_brand_id,

			organisation_id: data.organisation_id ? data.organisation_id : oldDetails.organisation_id,

			online_status: data.online_status ? data.online_status : oldDetails.online_status,
			otp: data.otp ? data.otp : oldDetails.otp,

			password: data.password ? bcrypt.hashSync(data.password) : oldDetails.password,

			profile_pic: data.profile_pic_name ? data.profile_pic_name : oldDetails.profile_pic,
			access_token: data.access_token ? data.access_token : oldDetails.access_token,

			latitude: data.latitude ? data.latitude : oldDetails.latitude,
			longitude: data.longitude ? data.longitude : oldDetails.longitude,

			timezone: data.timezone ? data.timezone : oldDetails.timezone,
			timezonez: data.timezonez ? data.timezonez : oldDetails.timezonez,

			socket_id: data.socket_id ? data.socket_id : oldDetails.socket_id,
			fcm_id: data.fcm_id ? data.fcm_id : oldDetails.fcm_id,
			//device_type: data.device_type ? data.device_type : oldDetails.device_type,

			mulkiya_number: data.mulkiya_number ? data.mulkiya_number : oldDetails.mulkiya_number,
			mulkiya_front: data.mulkiya_front_name ? data.mulkiya_front_name : oldDetails.mulkiya_front,
			mulkiya_back: data.mulkiya_back_name ? data.mulkiya_back_name : oldDetails.mulkiya_back,
			mulkiya_validity: data.mulkiya_validity ? data.mulkiya_validity : oldDetails.mulkiya_validity,

			forgot_token: data.forgot_token ? data.forgot_token : oldDetails.forgot_token,
			forgot_token_validity: data.forgot_token_validity ? data.forgot_token_validity : oldDetails.forgot_token_validity,

			maximum_rides: data.maximum_rides ? data.maximum_rides : oldDetails.maximum_rides,

			updated_at: data.created_at

		},
		{
			where: { user_detail_id: data.user_detail_id},
			//logging: console.log
		}
	);

};

////////////////////////		Middleware Get User 	//////////////////////////////
exports.DetailMiddlewareGet = async (data) => {

	return await Db.user_details.findOne({ 
		where: {"access_token": data.access_token},
		include: [
			{
				//as:	"User",
				model: Db.users,
			},
			{
				required: false,
				as: "Org",
				model: Db.organisations
			}
		]
	});

};

////////////////////////		Middleware Get User 	//////////////////////////////
exports.DetailRatingsMiddlewareGet = async (data) => {

	return await Db.sequelize.query("SELECT *, (CASE WHEN ud.profile_pic='' THEN '' ELSE CONCAT('"+process.env.RESIZE_URL+"', ud.profile_pic) END) as profile_pic_url FROM user_details as ud WHERE ud.access_token='"+data.access_token+"' LIMIT 0,1 ",
		{ type: Sequelize.QueryTypes.SELECT}
	)
		.then( (userDetails) => {

			if(userDetails.length == 0)
				return null;

			var promises = [];

			userDetails = JSON.parse(JSON.stringify(userDetails));
			var userDetail = userDetails[0];

			promises.push(
				Promise.all([
					Db.sequelize.query("SELECT *, (SELECT COUNT(*) FROM order_ratings WHERE order_ratings.customer_user_detail_id="+userDetail.user_detail_id+" AND order_ratings.deleted='0' AND order_ratings.blocked='0' AND order_ratings.created_by='Driver') as rating_count, (SELECT COALESCE( ROUND(AVG(ratings),0) ,0) FROM order_ratings WHERE order_ratings.customer_user_detail_id="+userDetail.user_detail_id+" AND order_ratings.deleted='0' AND order_ratings.blocked='0' AND order_ratings.created_by='Driver') as ratings_avg FROM users as u WHERE u.user_id="+userDetail.user_id+" LIMIT 0,1",	{ type: Sequelize.QueryTypes.SELECT} ),
					Db.sequelize.query("SELECT * FROM organisations as o WHERE o.organisation_id="+userDetail.organisation_id+" LIMIT 0,1",	{ type: Sequelize.QueryTypes.SELECT} ), 
				])
					.spread(function(user, org) {   // Maps results from the 2nd promise array

            	userDetail.user = user[0];
            	userDetail.Org = org.length != 0 ? org[0] : null;

						return userDetail;

           	})

			);

			return Promise.all(promises);

		});

};

///////////////////////		Get App Data 		//////////////////////////////////////
exports.UserGetAppDetails = async (data) => {

	return await Db.user_details.findOne({ 
		where: {"access_token": data.access_token},
		attributes: { exclude: ["otp", "password", "blocked", "created_by", "created_at", "updated_at", "forgot_token", "forgot_token_validity"] },
		include: [{
				//as:	"User",
				model: Db.users,
				attributes: { exclude: ["blocked", "created_by", "created_at", "updated_at"] }
			}]
	});

};

//////////////////		User Details Reatngs 	///////////////////////////////////////
exports.userRatings = async (user_type_id, user_detail_id) => {

	if(user_type_id == 1)
	    return await Db.sequelize.query("SELECT (SELECT COUNT(*) FROM order_ratings WHERE order_ratings.customer_user_detail_id="+user_detail_id+" AND order_ratings.deleted='0' AND order_ratings.blocked='0' AND order_ratings.created_by='Driver') as rating_count, (SELECT COALESCE( ROUND(AVG(ratings),0) ,0) FROM order_ratings WHERE order_ratings.customer_user_detail_id="+user_detail_id+" AND order_ratings.deleted='0' AND order_ratings.blocked='0' AND order_ratings.created_by='Driver') as ratings_avg FROM admins LIMIT 0,1",	{ type: Sequelize.QueryTypes.SELECT} );
	else
		return await Db.sequelize.query("SELECT (SELECT COUNT(*) FROM order_ratings WHERE order_ratings.driver_user_detail_id="+user_detail_id+" AND order_ratings.deleted='0' AND order_ratings.blocked='0' AND order_ratings.created_by='Customer') as rating_count, (SELECT COALESCE( ROUND(AVG(ratings),0) ,0) FROM order_ratings WHERE order_ratings.driver_user_detail_id="+user_detail_id+" AND order_ratings.deleted='0' AND order_ratings.blocked='0' AND order_ratings.created_by='Customer') as ratings_avg FROM admins LIMIT 0,1",	{ type: Sequelize.QueryTypes.SELECT} );		

};

////////////////////////	Service Driver Details 	//////////////////////////////////
exports.ServiceDriverLoginDetails = async (data) => {

	return await Db.user_details.findOne({ 
		where: {"access_token": data.access_token},
		attributes: [
			"user_detail_id", "user_id", "user_type_id", "language_id", "category_id", "category_brand_id", "profile_pic", "access_token", "latitude", "longitude", "mulkiya_number", "mulkiya_front", "mulkiya_back", "mulkiya_validity","approved",

			[Sequelize.literal("CASE WHEN mulkiya_front='' THEN '' ELSE CONCAT('"+process.env.RESIZE_URL+"', mulkiya_front) END"), "mulkiya_front_url"],
			[Sequelize.literal("CASE WHEN mulkiya_back='' THEN '' ELSE CONCAT('"+process.env.RESIZE_URL+"', mulkiya_back) END"), "mulkiya_back_url"],
			[Sequelize.literal("CASE WHEN profile_pic='' THEN '' ELSE CONCAT('"+process.env.RESIZE_URL+"', profile_pic) END"), "profile_pic_url"]
			//[Sequelize.literal('2222'), 'shop_count']
		 ],
		include: [
			// {
			// 	model: Db.user_driver_detail_products
			// },
			{
				model: Db.users,
				attributes: { exclude: ["blocked", "created_by", "created_at", "updated_at"] },
				include: [
					{
						required : false,
						as: "active_bank",
						model: Db.user_bank_details,
						where: { active: "1", deleted: "0" },
						attributes: { exclude: ["active", "deleted", "created_at", "updated_at"] }
					}
				]
			},
			{
				model: Db.user_driver_detail_services,
				attributes: ["category_id", "buraq_percentage"]
			}
		]
	});

	// "user_driver_detail_service_id": 3,
	// "user_id": 27,
	// "user_detail_id": 27,
	// "category_id": 1,
	// "deleted": "0",
	// "buraq_percentage": 10,
	// "created_at": "2018-08-29 11:08:32",
	// "updated_at": "2018-08-29 11:08:32"


};

///////////////////////		Support Driver Details 		//////////////////////////////////
exports.SupportDriverLoginDetails = async (data) => {

	return await Db.user_details.findOne({
		where: {"access_token": data.access_token},
		attributes: [
			"user_detail_id", "user_id", "user_type_id", "language_id", "category_id", "category_brand_id", "profile_pic", "access_token", "latitude", "longitude", "mulkiya_number", "mulkiya_front", "mulkiya_back", "mulkiya_validity",
			[Sequelize.literal("CASE WHEN mulkiya_front='' THEN '' ELSE CONCAT('"+process.env.RESIZE_URL+"', mulkiya_front) END"), "mulkiya_front_url"],
			[Sequelize.literal("CASE WHEN mulkiya_back='' THEN '' ELSE CONCAT('"+process.env.RESIZE_URL+"', mulkiya_back) END"), "mulkiya_back_url"],
			[Sequelize.literal("CASE WHEN profile_pic='' THEN '' ELSE CONCAT('"+process.env.RESIZE_URL+"', profile_pic) END"), "profile_pic_url"]
			//[Sequelize.literal('2222'), 'shop_count']
		 ],

		include: [
			{
				model: Db.users,
				attributes: { exclude: ["blocked", "created_by", "created_at", "updated_at"] }
			}
		]


	})
		.then( (userDetail) => {

			var promises = [];

			userDetail = JSON.parse(JSON.stringify(userDetail));

			data.user_detail_id = userDetail.user_detail_id;
			data.user_id = userDetail.user_id;

			promises.push(
				Promise.all([
					userSupportList(data)
				])
					.spread(function(services) {   // Maps results from the 2nd promise array

            	userDetail.services = services;

						return userDetail;

           	})
			);

			return Promise.all(promises);

		});

};

////////////////		User Supports Listings 		/////////////////////////////////
userSupportList = async (data) => {

	return await Db.sequelize.query("SELECT c.category_id, c.category_type, c.image, (CONCAT('"+process.env.RESIZE_URL+"',c.image)) as image_url, c.default_brands, cd.name, cd.description, c.sort_order, CASE WHEN udds.user_driver_detail_service_id IS NULL THEN '0' ELSE '1' END as selected FROM categories as c JOIN category_details as cd ON cd.category_id=c.category_id LEFT JOIN user_driver_detail_services as udds ON (udds.user_detail_id="+data.user_detail_id+" AND udds.category_id=c.category_id AND udds.deleted='0') WHERE c.blocked='0' AND c.category_type='Support' AND cd.language_id="+data.app_language_id+" ORDER BY c.sort_order ASC ",
		{ type: Sequelize.QueryTypes.SELECT}
	);


	// return await Db.sequelize.query("SELECT c.category_id, c.category_type, c.image, (CONCAT('"+process.env.RESIZE_URL+"',c.image)) as image_url, c.default_brands, cd.name, cd.description, c.sort_order FROM categories as c JOIN category_details as cd ON cd.category_id=c.category_id WHERE c.blocked='0' AND c.category_type='Support' AND cd.language_id="+data.app_language_id+" ORDER BY c.sort_order ASC ",
	//            { type: Sequelize.QueryTypes.SELECT}
	//        )

};

////////////////////////	Driver Products Ids 		////////////////////////
exports.driverProductsIds = (user_detail_id) => {

	return Db.user_driver_detail_products.findAll({
		where: { user_detail_id, deleted: "0" }
	})
		.then( (products) => {

			var productIds = [0];

			products.forEach((product) => {

				productIds.push(product.category_brand_product_id);

			});

			return productIds;

		});

};

//Add M2 part 2
///////////////		User Login Insert 	////////////////////////////////
exports.createNewUserLogin = async (data) => {
	return await Db.user_logins.create({
		user_id: data.user_id,
		user_type_id: data.user_type_id,
		login_at: data.currentDate
	});
};
