var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;
var moment = require("moment");

var Db = require(appRoot + "/models");//////////////////		Db Models
var Configs = require(appRoot + "/configs");/////////////		Configs


///////////////		User OTP Profile 	////////////////////////////////
exports.userOtpProfile = async (data) => {
    return await Db.users.findOne({
        where: {phone_number: data.phone_number},
        include: [
            {
                required: false,
                as: "user_detail",
                model: Db.user_details,
                where: {user_type_id: data.user_type_id}
            }
        ]
    });
};

////////////////////	Create User 		////////////////////////////
exports.createNewRow = async (data) => {

    return await Db.users.create({

        organisation_id: data.organisation_id ? data.organisation_id : 0,
        stripe_customer_id: data.stripe_customer_id ? data.stripe_customer_id : "",
        stripe_connect_id: data.stripe_connect_id ? data.stripe_connect_id : "",

        name: data.name ? data.name : "",
        email: data.email,

        phone_code: data.phone_code,
        phone_number: data.phone_number,

        address: data.address ? data.address : "",
        address_latitude: data.address_latitude ? data.address_latitude : 0.0,
        address_longitude: data.address_longitude ? data.address_longitude : 0.0,

        stripe_connect_token: data.stripe_connect_token ? data.stripe_connect_token : "",

        blocked: "0",
        created_by: data.created_by,

        created_at: data.created_at,
        updated_at: data.created_at

    });

};

///////////////////		Update User Row 		////////////////////////
exports.updateRow = async (data, oldDetails) => {

    return await Db.users.update(
            {

                organisation_id: data.organisation_id ? data.organisation_id : oldDetails.organisation_id,
                stripe_customer_id: data.stripe_customer_id ? data.stripe_customer_id : oldDetails.stripe_customer_id,
                stripe_connect_id: data.stripe_connect_id ? data.stripe_connect_id : oldDetails.stripe_connect_id,

                name: data.name ? data.name : oldDetails.name,
                email: data.email ? data.email : oldDetails.email,

                phone_code: data.phone_code ? data.phone_code : oldDetails.phone_code,
                phone_number: data.phone_number ? data.phone_number : oldDetails.phone_number,

                address: data.address ? data.address : oldDetails.address,
                address_latitude: data.address_latitude ? data.address_latitude : oldDetails.address_latitude,
                address_longitude: data.address_longitude ? data.address_longitude : oldDetails.address_longitude,

                stripe_connect_token: data.stripe_connect_token ? data.stripe_connect_token : oldDetails.stripe_connect_token,

                updated_at: data.created_at

            },
            {
                where: {user_id: data.user_id}
            }
    );

};
////////////////	Add New User 		////////////////////////////////
exports.userSignInProfile = async (data) => {

    return await Users.findOne({
        where: {"user_id": data.user_id},
        // include: [
        // 		{
        // 			as:	"userDevice",
        // 			model: userDevices,
        // 			where: {deviceId: data.deviceId}
        // 		},
        // 		{
        // 			as: "userInterests",
        // 			model: userInterests
        // 		}
        // ]
    });

};
////////////////	Add New User 		////////////////////////////////


////////////////	Add New Address	////////////////////////////////
exports.addNewAddress = async (data) => {
    return await Db.user_addresses.create({
        user_id: data.user_id,
        title: data.title,
        flat_no: data.flat_no,
        floor_no: data.floor_no,
        building_name: data.building_name,
        address: data.address,
        address_latitude: data.address_latitude,
        address_longitude: data.address_longitude
    });
};

/* get all addresses of user by id */
exports.GetAllUserAddress = async(data) => {
    return await Db.user_addresses.findAll({
        where: {user_id: data.user_id},
        order: [
            ['address_id', 'DESC']
        ]
    });
};

/* delete address by id */
exports.deleteUserAddressById = async(data) => {
    return await Db.user_addresses.destroy({where: {address_id: data.address_id}});
};

/* update address by id */
exports.updateUserAddress = async(data) => {
    return await Db.user_addresses.update({
        title: data.title,
        flat_no: data.flat_no,
        floor_no: data.floor_no,
        building_name: data.building_name,
        address: data.address,
        address_latitude: data.address_latitude,
        address_longitude: data.address_longitude
    }, {
        where: {address_id: data.address_id}
    }
    ).then((result) => {
        console.log("result", result);
        return result;
    });
};

exports.updateUserPromotionNotification = async(data) => {
    console.log(data);
    return await Db.users.update({
        promotion_notification: data.status
    }, {
        where: {user_id: data.user_id},
        logging: console.log
    })
};

/* update address by id */
exports.GetAllUNotification = async(data, type) => {
    return await Db.user_details.findAll({
        where: {user_type_id: type, blocked: '0'}
    });
};


/* save data when no driver is found */
exports.AddOrderFailure = async(data) => {
    return await Db.order_failures.create({
        message: data.message,
        products: data.products,
        customer_id: data.customer_id,
        customer_name: data.customer_name,
        order_date: data.order_date,
        sms_object: data.sms_object
    });
};


/* get all notification list */
exports.GetAllNotification = async(data) => {
    var format = "YYYY-MM-DD HH:mm:ss";
    var UserDateTime = new Date().toLocaleString("en-US", {timeZone: data.Details.timezone});
    UserDateTime = new Date(UserDateTime);
    var date_string = UserDateTime.toLocaleString();
    var date = moment(date_string).format(format);
    
    return await Db.notifications.findAll({
        order: [["updated_at", "DESC"]],
        limit: parseInt(data.take),
        offset: parseInt(data.skip),
        include: [
            {
                required: false,
                as: "promocode",
                model: Db.coupons
            }
        ],
        where: {created_at: {$lte: date}}
    });
};


/* delete address by id */
exports.GetNotificationById = async(data) => {
    return await Db.notifications.find({
        include: [
            {
                required: false,
                as: "promocode",
                model: Db.coupons
            }
        ],
        where: {notification_id: data.notification_id}
    });
};

exports.getAllCouponIds = async(data) => {
    var data = await Db.promo_users.findAll({
        where: data,
        raw: true
    });
    var ids = [];
    data.forEach(function (element) {
        ids.push(element.coupon_id);
    });
    return ids;
};

exports.getAllPromoUserProducts = async(id) => {
    var data = await Db.promocode_products.findAll({
        where: {
            coupon_id: id
        },
        raw: true
    });

    return data;
};


/* get all notification list */
exports.getAllPromocodes = async(data, coupon_ids) => {
    var format = "YYYY-MM-DD HH:mm:ss";
    var UserDateTime = new Date().toLocaleString("en-US", {timeZone: data.Details.timezone});
    UserDateTime = new Date(UserDateTime);
    var date_string = UserDateTime.toLocaleString();
    var date = moment(date_string).format(format);

    var whereStatement = {
        expires_at: {$gte: date},
        start_at: {$lte: date},
        blocked: '0',
        coupon_id: coupon_ids
    };

    if (data.category_id)
        whereStatement.category_id = data.category_id;
    if (data.product_id)
        whereStatement.product_id = data.product_id;
    if (data.brand_id)
        whereStatement.brand_id = data.brand_id;


    return await Db.coupons.findAll({
        where: whereStatement,
        order: [["created_at", "DESC"]],
        limit: parseInt(data.take),
        offset: parseInt(data.skip),
        include: [
            {
                required: false,
                as: "category_details",
                model: Db.category_details,
                where: {
                    language_id: data.app_language_id
                }
            },
            {
                required: false,
                as: "category_brand_details",
                model: Db.category_brand_details,
                where: {
                    language_id: data.app_language_id
                }
            },
            {
                required: false,
                as: "promo_users_rides",
                model: Db.promo_users_rides,
                where: {
                    coupon_id: {$col: 'coupons.coupon_id'},
                    user_id: data.user_id
                }
            },
            {
                required: false,
                model: Db.promo_users,
                as: "promo_users",
                where: {
                    user_id: data.user_id,
                    coupon_id: {$col: 'coupons.coupon_id'}
                }
            }
        ],
        logging: console.log
    }).then((coupons) => {
        var promises = [];
        coupons = JSON.parse(JSON.stringify(coupons));

        coupons.forEach(function (order) {
            var coupon_id = order.coupon_id;
            promises.push(
                    Promise.all([
                        GetPromoProducts(coupon_id)
                    ])
                    .spread(function (promocode_products) {
                        order.promocode_products = promocode_products.length > 0 ? promocode_products : [];
                        return order;
                    })
                    );
        });
        return Promise.all(promises);
    });
};

/*
 exports.getAllPromocodess = async(data) => {
 var whereStatement = {
 user_id: data.user_id
 //expires_at: {$gte: new Date()}
 };
 
 if (data.category_id)
 whereStatement.category_id = data.category_id;
 if (data.brand_id)
 whereStatement.brand_id = data.brand_id;
 
 
 return await Db.promo_users.findAll({
 where: whereStatement,
 //order: [["coupons.expires_at", "ASC"]],
 limit: parseInt(data.take),
 offset: parseInt(data.skip),
 include: [
 {
 required: false,
 model: Db.coupons,
 where: {
 expires_at: {$gte: new Date()}
 }
 },
 {
 required: false,
 as: "category_details",
 model: Db.category_details,
 where: {
 language_id: data.app_language_id,
 category_id: data.category_id
 }
 },
 ],
 logging: console.log
 });
 }; */


/* get all notification list */
exports.getAllProducts = async(data) => {
    return await Db.category_brand_product_details.findAll({
        where: {
            language_id: data.app_language_id,
            category_id: data.category_id,
            category_brand_id: data.brand_id
        }
    });
};


/* delete address by id */
exports.GetPromocodeById = async(data) => {
    return await Db.coupons.find({
        include: [
            {
                required: false,
                as: "category_details",
                model: Db.category_details
            },
            {
                required: false,
                as: "category_brand_details",
                model: Db.category_brand_details
            }
        ],
        where: {coupon_id: data.promocode_id}
    });
};

//User Request API
exports.checkPromoUserMaxRides = async(data) => {
    return await Db.promo_users_rides.find({
        where: {
            coupon_id: data.coupon_id,
            user_id: data.user_id
        },
        logging: console.log
    });
};

exports.MaxRidesUpdate = async(data, rides) => {
    return await Db.promo_users_rides.update({
        max_rides: rides
    }, {
        where: {coupon_id: data.coupon_id,
            user_id: data.user_id
        }
    }
    ).then((result) => {
        console.log("result", result);
        return result;
    });
};

//Driver Accept API Use
exports.checkPromoCodeMaxRides = async(id) => {
    return await Db.promo_users_rides.find({
        where: {
            coupon_id: id
        },
        logging: console.log
    });
};

exports.PromoCodeMaxRidesUpdate = async(couponId,CustomerId, rides) => {
    return await Db.promo_users_rides.update({
        max_rides: rides
    }, {
        where: {coupon_id: couponId,
            user_id: CustomerId
        }
    }
    ).then((result) => {
        console.log("result", result);
        return result;
    });
};

//Apply Promocode APi USE
exports.checkPromoUserRides = async(data) => {
    console.log("data", data);
    return await Db.coupons.find({
        include: [
            {
                required: false,

                as: "promo_users_rides",
                model: Db.promo_users_rides,
                where: {
                    coupon_id: {$col: 'coupons.coupon_id'},
                    user_id: data.user_id,
                    max_rides: '0'
                }
            }
        ],
        where: {
            code: data.coupon_code,
            category_id: data.category_id,
            brand_id: data.brand_id
        },
        logging: console.log
    });
};

/* check promocode */
exports.checkPromocode = async(data) => {
    return await Db.coupons.find({
        include: [
            {
                required: false,
                raw: true,
                as: "category_details",
                model: Db.category_details,
                where: {
                    language_id: data.app_language_id
                }
            },
            {
                required: false,
                raw: true,
                as: "category_brand_details",
                model: Db.category_brand_details,
                where: {
                    language_id: data.app_language_id
                }
            },
            {
                required: false,
                raw: true,
                model: Db.promo_users,
                as: "promo_users",
                where: {
                    user_id: data.user_id,
                    category_id: data.category_id,
                    brand_id: data.brand_id
                }
            }
        ],
        where: {
            code: data.coupon_code,
            category_id: data.category_id,
            brand_id: data.brand_id
        },
        logging: console.log
    }).then((coupons) => {
        var promises = [];
        coupons = JSON.parse(JSON.stringify(coupons));
        console.log("orders======customer", coupons);
        //coupons.forEach(function (order) {
        var coupon_id = coupons.coupon_id;
        // console.log("data ===", data);
        promises.push(
                Promise.all([
                    GetPromoProducts(coupon_id)
                ])
                .spread(function (promocode_products) {
                    coupons.promocode_products = promocode_products.length > 0 ? promocode_products : [];
                    return coupons;
                })
                );
        //});
        return Promise.all(promises);
    });

};
function GetPromoProducts(id)
{
    //console.log('orderproduct',data);
    return Db.sequelize.query("SELECT * FROM promocode_products AS promocode_products  WHERE promocode_products.coupon_id = " + id + "",
            {type: Sequelize.QueryTypes.SELECT}
    );
}