var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;

var Db = require(appRoot+"/models");//////////////////		Db Models

/////////////////       User Get Serving Companies in Area List        /////////////////
exports.newUserGetServingCompaniesList = (skip, take, latitude, longitude, category_brand_id, category_brand_product_id) => {

	var org_area_dist = "ROUND(((3959*acos(cos(radians("+latitude+")) * cos(radians(OA.address_latitude)) * cos(radians(OA.address_longitude)-radians("+longitude+"))+sin(radians("+latitude+"))*sin(radians(OA.address_latitude))))* 1.67),2)";

	var join_org_area = "(SELECT COUNT(*) as total, OA.organisation_id, OA.deleted, OA.blocked, SUM(sunday_service) AS sunday_service, SUM(monday_service) as monday_service, SUM(tuesday_service) as tuesday_service, SUM(wednesday_service) as wednesday_service, SUM(thursday_service) as thursday_service, SUM(friday_service) as friday_service, SUM(saturday_service) as saturday_service FROM organisation_areas OA WHERE ("+org_area_dist+" <= distance AND OA.deleted='0' AND OA.blocked='0') GROUP BY organisation_id) as OA ON (OA.organisation_id=O.organisation_id)";

	if(category_brand_product_id == undefined || category_brand_product_id == 0)
	{
		var etokenCheck = "((SELECT COUNT(*) FROM organisation_coupons OC WHERE OC.organisation_id=O.organisation_id AND OC.deleted='0' AND OC.blocked='0' AND OC.category_brand_id="+category_brand_id+") > 0)";
	}
	else
	{
		var etokenCheck = "((SELECT COUNT(*) FROM organisation_coupons OC WHERE OC.organisation_id=O.organisation_id AND OC.deleted='0' AND OC.blocked='0' AND OC.category_brand_id="+category_brand_id+" AND OC.category_brand_product_id="+category_brand_product_id+") > 0)";
	}

	return Db.sequelize.query("SELECT O.organisation_id, O.name, O.image, CONCAT('"+process.env.RESIZE_URL+"',O.image) as image_url, O.phone_code, O.phone_number, O.sort_order, (CASE WHEN OA.sunday_service>0 THEN '1' ELSE '0' END) AS sunday_service, (CASE WHEN OA.monday_service>0 THEN '1' ELSE '0' END) AS monday_service, (CASE WHEN OA.tuesday_service>0 THEN '1' ELSE '0' END) AS tuesday_service, (CASE WHEN OA.wednesday_service>0 THEN '1' ELSE '0' END) AS wednesday_service, (CASE WHEN OA.thursday_service>0 THEN '1' ELSE '0' END) AS thursday_service, (CASE WHEN OA.friday_service>0 THEN '1' ELSE '0' END) AS friday_service, (CASE WHEN OA.saturday_service>0 THEN '1' ELSE '0' END) AS saturday_service FROM organisations O JOIN organisation_categories OC ON (OC.organisation_id=O.organisation_id AND OC.category_id=2 AND OC.category_type='Service' AND OC.deleted='0') JOIN "+join_org_area+" WHERE O.blocked='0' AND (sunday_service!='0' OR monday_service!='0' OR tuesday_service!='0' OR wednesday_service!='0' OR thursday_service!='0' OR friday_service!='0' OR saturday_service!='0') AND "+etokenCheck+" ORDER BY sort_order ASC LIMIT "+skip+", "+take+" ", { type: Sequelize.QueryTypes.SELECT });

};

//////////////////		User Get Purchased Etoken Companies  List 	//////////////////
exports.userPurchasedeTokenList = (customer_user_detail_id, skip, take, language_id) => {

	return Db.organisation_coupon_users.findAll({
		where: { customer_user_detail_id, quantity_left: { $gte: 0 } },

		attributes: [
			"organisation_coupon_user_id", "organisation_coupon_id", "category_id", "category_brand_id", "category_brand_product_id", "seller_user_id", "seller_user_detail_id", "seller_user_type_id", "seller_organisation_id", "customer_user_id", "customer_user_detail_id", "bottle_quantity", "quantity", "quantity_left", "address", "address_latitude", "address_longitude", "created_at", "updated_at",
			[Sequelize.literal("(SELECT COALESCE(SUM(final_charge),0) FROM payments P1 WHERE P1.organisation_coupon_user_id=organisation_coupon_users.organisation_coupon_user_id AND P1.payment_status=\"Pending\" AND P1.refund_status=\"NoNeed\")"), "pendingPaymentAmount"],
			[Sequelize.literal("(SELECT COALESCE(SUM(final_charge),0) FROM payments P2 WHERE P2.organisation_coupon_user_id=organisation_coupon_users.organisation_coupon_user_id AND P2.payment_status=\"Success\" AND P2.refund_status=\"NoNeed\")"), "paidPaymentAmount"],
		],

		include: [
			{
				model: Db.organisations,
				as: "sellerOrg",
				attributes: [
					"organisation_id", "name", "username", "phone_code", "phone_number", "image",
					[Sequelize.literal("(CONCAT('"+process.env.RESIZE_URL+"',sellerOrg.image))") , "image_url"]
				]
			},
			{
				model: Db.category_brand_product_details,
				as: "categoryBrandProduct",
				on: { "$organisation_coupon_users.category_brand_product_id$" : {$col: "categoryBrandProduct.category_brand_product_id"}, "$categoryBrandProduct.language_id$": language_id },
				attributes: ["category_brand_product_id", "name", "description"]
			},
			{
				model: Db.category_brand_details,
				as: "categoryBrand",
				on: { "$organisation_coupon_users.category_brand_id$" : {$col: "categoryBrand.category_brand_id"}, "$categoryBrand.language_id$": language_id },
				attributes: ["category_brand_id", "name", "description", "image", 
					[Sequelize.literal("(CONCAT('"+process.env.RESIZE_URL+"',categoryBrand.image))") , "image_url"]
				]
				//attributes: ['category_brand_id', 'name', 'description']
			}
			
		],

		order: [ ["updated_at", "DESC"] ],
		limit: take,
		offset: skip
	});


};


/////////////////       User Get Serving Companies in Area List        /////////////////
exports.userGetServingCompaniesList = (skip, take, latitude, longitude, day) => {

	if(day == 0)
		var day_check = " AND sunday_service = \"1\" ";
	else if(day == 1)
		var day_check = " AND monday_service = \"1\" ";
	else if(day == 2)
		var day_check = " AND tuesday_service = \"1\" ";
	else if(day == 3)
		var day_check = " AND wednesday_service = \"1\" ";
	else if(day == 4)
		var day_check = " AND thursday_service = \"1\" ";
	else if(day == 5)
		var day_check = " AND friday_service = \"1\" ";
	else
		var day_check = " AND saturday_service = \"1\" ";

	var org_area_dist = "ROUND(((3959*acos(cos(radians("+latitude+")) * cos(radians(OA.address_latitude)) * cos(radians(OA.address_longitude)-radians("+longitude+"))+sin(radians("+latitude+"))*sin(radians(OA.address_latitude))))* 1.67),2) as cDistance";

	return Db.sequelize.query("SELECT org_area.* FROM (SELECT O.organisation_id, OA.organisation_area_id, O.name, O.image, CONCAT('"+process.env.RESIZE_URL+"',O.image) as image_url, O.phone_code, O.phone_number, O.sort_order, "+org_area_dist+", OA.distance, OA.sunday_service, OA.monday_service, OA.tuesday_service, OA.wednesday_service, OA.thursday_service, OA.friday_service, OA.saturday_service FROM organisation_areas OA JOIN organisations O ON (O.organisation_id=OA.organisation_id AND O.blocked='0') JOIN organisation_categories OC ON (OC.organisation_id=O.organisation_id AND OC.category_id=2 AND OC.category_type='Service' AND OC.deleted='0') HAVING cDistance <= OA.distance "+day_check+" ORDER BY O.sort_order, cDistance ASC) as org_area GROUP BY organisation_id LIMIT "+skip+", "+take+" ", { type: Sequelize.QueryTypes.SELECT, logging: console.log });

};

///////////////////////////		Etokens Listings 		///////////////////////////////////
exports.singleComanyEtokensList = (organisation_id, category_brand_id, skip, take, language_id) => {

	return Db.organisation_coupons.findAll({

		where: { organisation_id, category_brand_id, deleted: "0", blocked: "0" },
		attributes: ["organisation_coupon_id", "organisation_id", "category_id", "category_brand_id", "category_brand_product_id", "price", "quantity", "description"],
		include: [
			{
				model: Db.category_brand_details,
				as: "categoryBrand",
				on: { "$organisation_coupons.category_brand_id$" : {$col: "categoryBrand.category_brand_id"}, "$categoryBrand.language_id$": language_id },
				attributes: ["category_brand_detail_id", "category_brand_id", "name", "image",
					[Sequelize.literal("(CONCAT('"+process.env.RESIZE_URL+"',image))") , "image_url"],
					"description"]
			},
			{
				model: Db.category_brand_product_details,
				as: "categoryBrandProduct",
				on: { "$organisation_coupons.category_brand_product_id$" : {$col: "categoryBrandProduct.category_brand_product_id"}, "$categoryBrandProduct.language_id$": language_id },
				attributes: ["category_brand_product_id", "name", "description"]
			}

		],
		order: [["organisation_coupon_id", "DESC"]],
		limit: take,
		offset: skip,

	});

};

////////////////////		Organisation Details 4 Etokens 		////////////////////////////
exports.orgDetails4EToken = (organisation_id) => {

	return Db.organisations.findOne({

		where: {  organisation_id},
		attributes: ["organisation_id", "name", "image", 
			[Sequelize.literal("(CONCAT('"+process.env.RESIZE_URL+"',image))") , "image_url"],
			[Sequelize.literal("(SELECT buraq_percentage FROM categories WHERE category_id=2)") , "buraq_percentage"],
			"bottle_charge"]
	});

};

///////////////////		Etoken Details 4 Purchase 		/////////////////////////////////////
exports.etokenDetails4Purchase = (organisation_coupon_id) => {

	// var area_dist = "ROUND(((3959*acos(cos(radians("+latitude+")) * cos(radians(single_history.address_latitude)) * cos(radians(single_history.address_longitude)-radians("+longitude+"))+sin(radians("+latitude+"))*sin(radians(single_history.address_latitude))))* 1.67),2)"

	return Db.organisation_coupons.findOne({

		where: {  organisation_coupon_id, deleted: "0", blocked: "0" },
		// include: [
		// 	{
		// 		required: false,
		// 		model: Db.organisation_coupon_users,
		// 		as: 'single_history',
		// 		where: { customer_user_id, customer_user_detail_id, category_brand_id: Sequelize.col('organisation_coupons.category_brand_id'), category_brand_product_id: Sequelize.col('organisation_coupons.category_brand_product_id') },
		// 		attributes: [
		// 			'organisation_coupon_user_id', 'organisation_coupon_id', 'category_id', 'category_brand_id', 'category_brand_product_id', 'seller_user_id', 'seller_user_detail_id', 'seller_user_type_id', 'seller_organisation_id', 'customer_user_id', 'customer_user_detail_id', 'customer_user_type_id', 'customer_organisation_id', 'bottle_quantity', 'quantity', 'quantity_left', 'address', 'address_latitude', 'address_longitude',
		// 			[Sequelize.literal(area_dist), 'distance']
		// 		],
		// 		//[{ distance: { $lte: 3 } }]
		// 	}
		// ]
	});

};

////////////////		User Nearest Purchased Etoken History 	//////////////////
exports.userPurchasedNearestEtoken = (customer_user_detail_id, seller_organisation_id, category_brand_id, category_brand_product_id, latitude, longitude, distance) => {
	
	var area_dist = "ROUND(((3959*acos(cos(radians("+latitude+")) * cos(radians(address_latitude)) * cos(radians(address_longitude)-radians("+longitude+"))+sin(radians("+latitude+"))*sin(radians(address_latitude))))* 1.67),2)";

	return Db.organisation_coupon_users.findOne({

		where: Sequelize.literal(`customer_user_detail_id = ${customer_user_detail_id} AND seller_organisation_id = ${seller_organisation_id} AND category_brand_id = ${category_brand_id} AND category_brand_product_id = ${category_brand_product_id} AND ${area_dist} <= ${distance} `),

		attributes: [
			"organisation_coupon_user_id", "organisation_coupon_id", "category_id", "category_brand_id", "category_brand_product_id", "seller_user_id", "seller_user_detail_id", "seller_user_type_id", "seller_organisation_id", "customer_user_id", "customer_user_detail_id", "customer_user_type_id", "customer_organisation_id", "bottle_quantity", "quantity", "quantity_left", "address", "address_latitude", "address_longitude",
			[Sequelize.literal(area_dist), "distance"]
		],
		order: [ [Sequelize.col("distance"),"ASC"] ]
	});

};

////////////////////////////////////////////////////////
///////////////////////			Driver Section		///////////////////////////////
////////////////////////////////////////////////////////

////////////////		Driver Areas Listings 		////////////////////////////////////
exports.driverAreasListings = (organisation_id, driver_user_detail_id, skip, take, day, latitude, longitude) => {

	var org_area_dist = "(ROUND(((3959*acos(cos(radians("+latitude+")) * cos(radians(address_latitude)) * cos(radians(address_longitude)-radians("+longitude+"))+sin(radians("+latitude+"))*sin(radians(address_latitude))))* 1.67),2))";

	var org_where = {
		organisation_id, deleted: "0", blocked: "0"
	};

	if(day == 0)
		org_where.sunday_service="1";
	else if(day == 1)
		org_where.monday_service="1";
	else if(day == 2)
		org_where.tuesday_service="1";
	else if(day == 3)
		org_where.wednesday_service="1";
	else if(day == 4)
		org_where.thursday_service="1";
	else if(day == 5)
		org_where.friday_service="1";
	else
		org_where.saturday_service="1";

	return Db.organisation_areas.findAll({

		where: org_where,
		attributes: ["organisation_area_id", "organisation_id", "label", "address", "address_latitude", "address_longitude",
			[Sequelize.literal(org_area_dist), "distance"]
		],
		include: [
			{
				required: true,
				model: Db.organisation_area_drivers,
				as: "driver",
				where: { user_detail_id: driver_user_detail_id },
				attributes: ["organisation_area_driver_id", "organisation_area_id", "user_detail_id"]
			}
		],

		order: [ ["distance", "ASC"] ],
		limit: take,
		offset: skip,

	});

};

///////////////		Driver Area Wise Customers Listings 	/////////////////////////
exports.driverAreaWiseCustomersListings = (driver_user_id, driver_user_detail_id, driver_organisation_id, organisation_area_id, driver_category_brand_id, driverPIdsJoin, latitude, longitude, skip, take, app_language_id, created_at) => {

	///////////		Customer Nearest Area Id Get 	///////////////////////////////////
	var org_area_dist = "ROUND(((3959*acos(cos(radians(OCU.address_latitude)) * cos(radians(OA1.address_latitude)) * cos(radians(OA1.address_longitude)-radians(OCU.address_longitude))+sin(radians(OCU.address_latitude))*sin(radians(OA1.address_latitude))))* 1.67),4)";

	var nearest_area_id_get = `(SELECT organisation_area_id FROM organisation_areas OA1 WHERE OA1.organisation_id=${driver_organisation_id} AND OA1.deleted='0' AND OA1.blocked='0' AND ${org_area_dist} <= OA1.distance ORDER BY ${org_area_dist} ASC LIMIT 0,1) as nearest_area_id`;
	///////////		Customer Nearest Area Id Get 	///////////////////////////////////

	var customer_dist_from_driver = "ROUND(((3959*acos(cos(radians("+latitude+")) * cos(radians(OCU.address_latitude)) * cos(radians(OCU.address_longitude)-radians("+longitude+"))+sin(radians("+latitude+"))*sin(radians(OCU.address_latitude))))* 1.67),4) as distance";

	///////////////		JOINs String 		//////////////////////////
	var productDetailsJoin = `JOIN category_brand_product_details AS CBPD ON (CBPD.category_brand_product_id=OCU.category_brand_product_id AND CBPD.language_id=${app_language_id})`;
	var brandDetailsJoin = `JOIN category_brand_details AS CBD ON (CBD.category_brand_id=OCU.category_brand_id AND CBD.language_id=${app_language_id})`;
	var leftJoinRatings = "LEFT JOIN (SELECT COUNT(*) as rating_count, ROUND(AVG(ODR.ratings),0) as ratings_avg, ODR.customer_user_detail_id FROM order_ratings ODR WHERE ODR.deleted='0' AND ODR.blocked='0' AND ODR.created_by='Driver' GROUP BY customer_user_detail_id) AS ODR ON (ODR.customer_user_detail_id=OCU.customer_user_detail_id)";
	///////////////		JOINs String 		//////////////////////////

	///////////////	Today Orders Check & Count 	//////////////////////
	var todayCompletedOrder = `COALESCE((SELECT O1.order_status FROM orders O1 WHERE O1.organisation_coupon_user_id=OCU.organisation_coupon_user_id AND DATE(O1.order_timings)=DATE('${created_at}') ORDER BY O1.updated_at DESC LIMIT 0,1), 'NoOrder') as todayOrderStatus`;

	var todayMyOrder = `COALESCE((SELECT order_id FROM orders O2 WHERE O2.organisation_coupon_user_id=OCU.organisation_coupon_user_id AND DATE(O2.order_timings)=DATE('${created_at}') AND O2.driver_user_detail_id=${driver_user_detail_id} AND O2.driver_user_id=${driver_user_id} ORDER BY O2.updated_at DESC LIMIT 0,1),0) as todayMyOrderId`;
	///////////////	Today Orders Check & Count 	//////////////////////

	return Db.sequelize.query(`SELECT OCU.organisation_coupon_user_id, OCU.organisation_coupon_id, OCU.category_id, OCU.category_brand_id, OCU.category_brand_product_id, OCU.bottle_quantity, OCU.quantity, OCU.quantity_left, OCU.address, OCU.address_latitude, OCU.address_longitude, U.user_id, UD.user_detail_id, U.name, U.phone_code, U.phone_number, UD.profile_pic, (CASE WHEN UD.profile_pic='' THEN '' ELSE CONCAT('${process.env.RESIZE_URL}', UD.profile_pic) END) as profile_pic_url, ${nearest_area_id_get}, ${customer_dist_from_driver}, COALESCE(ODR.rating_count,0) as rating_count, COALESCE(ODR.ratings_avg,0) as ratings_avg, CBPD.name as product_name, CBD.name as brand_name, ${todayCompletedOrder}, ${todayMyOrder} FROM organisation_coupon_users OCU JOIN users U ON (U.user_id=OCU.customer_user_id) JOIN user_details UD ON (UD.user_detail_id=OCU.customer_user_detail_id AND UD.blocked='0') ${brandDetailsJoin} ${productDetailsJoin} ${leftJoinRatings} WHERE OCU.seller_organisation_id=${driver_organisation_id} AND OCU.quantity_left > 0 AND OCU.category_brand_id=${driver_category_brand_id} AND OCU.category_brand_product_id IN (${driverPIdsJoin}) HAVING nearest_area_id=${organisation_area_id} ORDER BY distance ASC LIMIT ${skip}, ${take} `, { type: Sequelize.QueryTypes.SELECT});

};

//////////////////	Customer Etoken Details 4 Startinf Order 	/////////////////////////
exports.userEtoken4StartingOrder = (organisation_coupon_user_id, created_at, driver_user_id, driver_user_detail_id, driver_organisation_id, driver_category_brand_id) => {

	///////////////	Today Orders Check & Count 	//////////////////////
	var todayMyOrder = `COALESCE((SELECT order_id FROM orders O2 WHERE O2.organisation_coupon_user_id=organisation_coupon_users.organisation_coupon_user_id AND DATE(O2.order_timings)=DATE('${created_at}') AND O2.order_status NOT IN ('SerComplete', 'CTimeout') ORDER BY O2.updated_at DESC LIMIT 0,1),0)`;//todayMyOrderId
	///////////////	Today Orders Check & Count 	//////////////////////

	return Db.organisation_coupon_users.findOne({

		where: { organisation_coupon_user_id, seller_organisation_id: driver_organisation_id, category_brand_id: driver_category_brand_id },

		attributes: [
			"organisation_coupon_user_id", "organisation_coupon_id", "category_id", "category_brand_id", "category_brand_product_id", "seller_user_id", "seller_user_detail_id", "seller_user_type_id", "seller_organisation_id", "customer_user_id", "customer_user_detail_id", "customer_user_type_id", "customer_organisation_id", "bottle_quantity", "quantity", "quantity_left", "address", "address_latitude", "address_longitude",
			[Sequelize.literal(todayMyOrder), "todayOrderId"]
		],
		include: [
			{
				model: Db.users,
				as: "Customer"
			},
			{
				model: Db.user_details,
				as: "CustomerDetails"
			}
		]

	});

};