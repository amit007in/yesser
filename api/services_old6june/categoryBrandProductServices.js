var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;

var Db = require(appRoot+"/models");//////////////////		Db Models

/////////////////////		Single Brand Products 		///////////////////////////////
exports.ServiceBrandProductsWithDetails = async (data) => {

	return await Db.sequelize.query("SELECT cbp.category_brand_product_id, cbpd.name, cbpd.description, cbp.sort_order, cbp.actual_value, cbp.alpha_price, cbp.price_per_quantity, cbp.price_per_distance, cbp.price_per_weight, cbp.price_per_hr, cbp.price_per_sq_mt, (CASE WHEN cbp.image='' THEN '' ELSE CONCAT('"+process.env.PRODUCT_URL+"',cbp.image) END) as image_url, cbp.min_quantity, cbp.max_quantity FROM category_brand_products cbp JOIN category_brand_product_details as cbpd ON cbpd.category_brand_product_id=cbp.category_brand_product_id WHERE cbp.blocked='0' AND cbpd.language_id="+data.app_language_id+" AND cbp.category_brand_id="+data.category_brand_id+" ORDER BY cbp.sort_order ASC ",
		{ type: Sequelize.QueryTypes.SELECT}
	);

	// return await Db.sequelize.query("SELECT c.category_id, c.category_type, c.default_brands, cd.name, cd.description FROM categories as c JOIN category_details as cd ON cd.category_id=c.category_id WHERE c.blocked='0' AND c.category_type='Service' AND cd.language_id="+data.app_language_id+" ORDER BY c.sort_order ASC ",
	//            { type: Sequelize.QueryTypes.SELECT}

};

////////////////		Single Product Details 		///////////////////////////////////
exports.singleProductDetail4Sockets =  async (data) => {

	return await Db.sequelize.query("SELECT cbp.*, cbpd.name as name, cbpd.description as description, (SELECT name FROM category_brand_details as cbd WHERE cbd.category_brand_id=cbp.category_brand_id AND cbd.language_id="+data.app_language_id+" ) as brand_name, (SELECT buraq_percentage FROM categories as c WHERE c.category_id=cbp.category_id) as buraq_percentage, (CASE WHEN cbp.image='' THEN '' ELSE CONCAT('"+process.env.RESIZE_URL+"',cbp.image) END) as image_url FROM category_brand_products as cbp JOIN category_brand_product_details as cbpd ON cbpd.category_brand_product_id=cbp.category_brand_product_id WHERE cbpd.language_id="+data.app_language_id+" AND cbp.category_brand_product_id="+data.category_brand_product_id+" LIMIT 0,1 ",
		{ type: Sequelize.QueryTypes.SELECT}
	)
		.then( (product) => {

			return product[0];

		});


};

// "category_brand_detail_id": 1,
//                    "category_id": 1,
//                    "language_id": 1,
//                    "category_brand_id": 1,
//                    "name": "Default",
//                    "image": "0436501f040b667cae9d03003156e97b766b19f6ivpiin0DrGJZr4rfLD3soGWPT.png",
//                    "description": "",
//                    "created_at": "2018-07-30 11:33:51",
//                    "updated_at": "2018-07-31 05:51:16"

////////////////		Single Product Details 		///////////////////////////////////
exports.ProductDetailsAllLang =  async (data) => {

	return await Db.category_brands.findOne({
		where: {
			category_brand_id: data.category_brand_id
		},
		include: [
			{
				model: Db.category_brand_details,
				attributes: ["name", "image", "language_id"]
			},
			{
				model: Db.category_brand_products,
				as: "Product",
				where: { category_brand_product_id: data.category_brand_product_id },
				include: [
					{
						model: Db.category_brand_product_details,
						attributes: ["name", "language_id", "description"]
					}
				]
			}
		]
	});

};

////////////////////		Single Cat Single Brand All Details 	////////////////////////////////////
exports.CatDetailsAllLang = async (data) => { 

	return await Db.categories.findOne({

		where: { category_id: data.category_id },
		include: [
			{
				model: Db.category_details,
				attributes: ["name", "description"]
			},
			{
				model: Db.category_brands,
				as: "Brand",
				where: { category_brand_id: data.category_brand_id },
				attributes: ["category_brand_id", "category_id", "category_brand_type"],
				include: [
					{
						model: Db.category_brand_details,
						attributes: ["name", "image", "language_id"]
					},
					{
						model: Db.category_brand_products,
						as: "Product",
						where: { category_brand_product_id:  {in: data.CateGoryProducts.map(item => {return item;})} },
						include: [{
								model: Db.category_brand_product_details,
								attributes: ["name", "language_id", "description"]
							}]
					}
				]
			}
		],
                logging:console.log
	});

};


///////////////////////
