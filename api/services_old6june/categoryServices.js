var _ = require("lodash");
const got = require("got");
//var request = require('request');

var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;

var {ServiceBrandProductsWithDetails} = require(appRoot+"/services/categoryBrandProductServices");
//var Services = require(appRoot+"/services");///////////		Services
var Db = require(appRoot+"/models");//////////////////		Db Models

////////////////////		All Services With Brands & Products 		/////////////
exports.newServicesWithBrands = async (data) => {

	var categories = await Db.category_details.findAll({

		where: {language_id: data.app_language_id},
		attributes: {
			include: ["category_id", "name", "description", [Sequelize.col("category.default_brands"), "default_brands", "buraq_percentage"] ],
			exclude: ["category_detail_id", "created_at", "updated_at", "language_id"]
		},
		include: [

			{
				model: Db.categories,
				required: true,
				where: { category_type: "Service", blocked: "0" }
			},
			{
				as: "brands",
				where: { language_id: data.app_language_id },
				model: Db.category_brand_details,
				required: true,
				attributes: {

					include: ["category_brand_id", "name", "description", "image", 
						[Sequelize.fn("CONCAT", process.env.RESIZE_URL, Sequelize.col("brands.image")), "image_url"]
					  ],
					exclude: ["category_brand_detail_id", "created_at", "updated_at", "language_id"]

				},
				include: [
					{
						model: Db.category_brands,
						where: { blocked: "0" }
					}
				],
				order: [["sort_order","DESC"]]
			}

		]

	});

	/*
	var categories =  await Db.categories.findAll({

		where: { category_type: 'Service', blocked: '0' },
		attributes: ["category_id", "default_brands"],
		include: [

			{
				as: "Details",
				model: Db.category_details,
				where: {language_id: data.app_language_id},
				attributes: {
					include: [
						"name", "description", "category_id", [Sequelize.col('categories.default_brands'), 'default_brands']
					]
					
				},
				required: true
			},

			{
				as: "BrandDetails",
				model: Db.category_brand_details,
				where: {language_id: data.app_language_id},
				attributes: ["category_id", "name", "image",
					[Sequelize.fn('CONCAT', process.env.RESIZE_URL, Sequelize.col('BrandDetails.image')), 'image_url'],
				],
				required: true,
				order: ["sort_order", "ASC"]
			},

			// {
			// 	as: "Brand",
			// 	model: Db.category_brands,
			// 	where: {blocked: '0'},
			// 	attributes: ["category_brand_id"],
			// 	required: true,
			// 	include: [

			// 		{
			// 			as: "Details",
			// 			model: Db.category_brand_details,
			// 			where: {language_id: data.app_language_id},
			// 			//attributes: ["name", "description"],
			// 			required: true
			// 		},
			// 		{
			// 			as: "Details",
			// 			model: Db.category_brand_product_details,
			// 			where: {language_id: data.app_language_id},
			// 			//attributes: ["name", "description"],
			// 			required: true
			// 		},



			// 	]
			// }

		]

	})*/

	return categories;

};

/////////////////		Includes 		/////////////////////////////////////////////

// var catSingleDetail =	{
// 			as: "Details",
// 			model: Db.category_brand_details,
// 			where: {language_id: data.app_language_id},
// 			attributes: ["name", "image", "description"]
// 	}

////////////////	Driver Service Filter 	////////////////////////////////
exports.servicesWithBrandsOrgFilter = (data) => {

	if(data.organisation_id == 0)
	{

		var query = `SELECT c.category_id, c.buraq_percentage, c.category_type, c.default_brands, cd.name, cd.description FROM categories as c JOIN category_details as cd ON cd.category_id=c.category_id WHERE c.blocked='0' AND c.category_type='Service' AND cd.language_id=${data.app_language_id} ORDER BY c.sort_order ASC `;

	}
	else
	{
		var query = `SELECT c.category_id, c.buraq_percentage, c.category_type, c.default_brands, cd.name, cd.description FROM categories as c JOIN organisation_categories OC ON (OC.category_id=c.category_id AND OC.organisation_id=${data.organisation_id} AND OC.deleted="0") JOIN category_details as cd ON cd.category_id=c.category_id WHERE c.blocked='0' AND c.category_type='Service' AND cd.language_id=${data.app_language_id} ORDER BY c.sort_order ASC `;
	}

	return Db.sequelize.query(query,
		{ type: Sequelize.QueryTypes.SELECT}
	)
		.then( (services) => {

			var promises = [];

			services = JSON.parse(JSON.stringify(services));

			services.forEach(function(service) {

        	data.category_id = service.category_id;

				promises.push(
					Promise.all([
                      	serviceBrandsWithDetails(data)
					])
						.spread(function(brands) {   // Maps results from the 2nd promise array

							service.brands = brands;

							return service;

            	})
				);

			});

			return Promise.all(promises);

		});

};

////////////////////		App Get Services with Brands 	///////////////
exports.servicesWithBrands = async (data) => {

	return await Db.sequelize.query("SELECT c.category_id, c.buraq_percentage, c.category_type, c.default_brands, cd.name, cd.description FROM categories as c JOIN category_details as cd ON cd.category_id=c.category_id WHERE c.blocked='0' AND c.category_type='Service' AND cd.language_id="+data.app_language_id+" ORDER BY c.sort_order ASC ",
		{ type: Sequelize.QueryTypes.SELECT}
	)
		.then( (services) => {

			var promises = [];

			services = JSON.parse(JSON.stringify(services));

			services.forEach(function(service) {

        	data.category_id = service.category_id;

				promises.push(
					Promise.all([
                      	serviceBrandsWithDetails(data)
					])
						.spread(function(brands) {   // Maps results from the 2nd promise array

							service.brands = brands;

							return service;

            	})
				);

			});

			return Promise.all(promises);

		});

};

////////////////		App Supports Listings 		/////////////////////////////////
exports.appSupportList = async (data) => {
	return await Db.sequelize.query("SELECT c.category_id, c.buraq_percentage, c.category_type, c.image, (CONCAT('"+process.env.RESIZE_URL+"',c.image)) as image_url, c.default_brands, cd.name, cd.description, c.sort_order FROM categories as c JOIN category_details as cd ON cd.category_id=c.category_id WHERE c.blocked='0' AND c.category_type='Support' AND cd.language_id="+data.app_language_id+" ORDER BY c.sort_order ASC ",
		{ type: Sequelize.QueryTypes.SELECT}
	);
};

/////////////////////	Service Brands with Details 	/////////////////////////
exports.singleServiceCatDetails = async (data) => {
	return await serviceBrandsWithDetails(data)
		.then( (brands) => { 
			var promises = [];
			brands = JSON.parse(JSON.stringify(brands));
		        brands.forEach(function(brand) {
		        	data.category_brand_id = brand.category_brand_id;
		                promises.push(
		                    Promise.all([
		                    ServiceBrandProductsWithDetails(data)
		                ])
		                .spread(function(products) {   // Maps results from the 2nd promise array
		                    brand.products = products;
		                    return brand;
		            	})
		            );
		        });
		            return Promise.all(promises);
		});
};

/////////////////////	App Get Supports Listings 		/////////////////////////////
exports.supportsListings = async (data) => {
	return await Db.sequelize.query("SELECT c.category_id, c.buraq_percentage, c.category_type, cd.name, cd.description, c.image, (CONCAT('"+process.env.PRODUCT_URL+"',c.image)) as image_url FROM categories as c JOIN category_details as cd ON cd.category_id=c.category_id WHERE c.blocked='0' AND c.category_type='Support' AND cd.language_id="+data.app_language_id+" ORDER BY c.sort_order ASC ",
		{ type: Sequelize.QueryTypes.SELECT}
	);
};

////////////////////		Category Brands with Details 	/////////////////////////
function serviceBrandsWithDetails(data){
	return Db.sequelize.query("SELECT cb.category_brand_id, cb.sort_order, cb.category_id, cbd.name, cbd.image, (CONCAT('"+process.env.PRODUCT_URL+"',cbd.image)) as image_url FROM category_brands as cb JOIN category_brand_details AS cbd ON cbd.category_brand_id=cb.category_brand_id WHERE cb.blocked='0' AND cb.category_id="+data.category_id+" AND cbd.language_id="+data.app_language_id+" ORDER BY cb.sort_order ASC ",{ type: Sequelize.QueryTypes.SELECT});
}

///////////////////		Brand Details 		/////////////////////////////////////////
exports.brandLangDetails = async (data) => {
	return Db.sequelize.query("SELECT cb.category_brand_id, cb.sort_order, cb.category_id, cbd.name, cbd.image, (CONCAT('"+process.env.RESIZE_URL+"',cbd.image)) as image_url FROM category_brands as cb JOIN category_brand_details AS cbd ON cbd.category_brand_id=cb.category_brand_id WHERE cb.blocked='0' AND cb.category_id="+data.category_id+" AND cbd.language_id="+data.app_language_id+" ORDER BY cb.sort_order ASC ",{ type: Sequelize.QueryTypes.SELECT});
};

////////////////////	App Get Categories 		/////////////////////////////////////
exports.appServiceCategories = async (data) => {
	var categories = await got( process.env.LARAVEL_URL+"app_cat_lists",
		{ 
			json: true,
			body: {
				app_language_id: data.app_language_id
			}
		}
	);
	
	return categories.body;


	// 	return await Db.categories.findAll({

	// 		where: {
	// 			blocked: "0",
	// 			category_type: "Service",
	// 		},
	// 		attributes: {
	// 			exclude: ["blocked", "created_at", "updated_at", 'image']
	// 		},
	// 		include: [
	// 			{
	// 				as: "Details",
	// 				model: Db.category_details,
	// 				where: {language_id: data.app_language_id},
	// 				attributes: {
	// 					exclude: ["created_at", "updated_at"]
	// 				}
	// 			},
	// 			{//////////////		Brands 		//////////////////////////

	// 				as: "Brands",
	// 				model: Db.category_brands,
	// 				where: { blocked: "0" },
	// 				attributes: {
	// 					exclude: ["created_at", "updated_at", "blocked"]
	// 				},
	// 				include: [
	// 					{
	// 						as: "Details",
	// 						model: Db.category_brand_details,
	// 						where: {language_id: data.app_language_id},
	// 						attributes: ["name", "image", "description"]
	// 					},
	// 					{////////////		Products 	///////////////
	// 						as: "Products",
	// 						model: Db.category_brand_products,
	// 						where: { blocked: "0" },
	// 						attributes: {
	// 							exclude: ["blocked", "created_at", "updated_at"]
	// 						},
	// 						include: [
	// 							{
	// 								as: "Details",
	// 								model: Db.category_brand_product_details,
	// 								where: { language_id: data.app_language_id },
	// 								attributes: ["name", "description"]
	// 								// {
	// 								// 	include: ["created_at", "updated_at"]
	// 								// },
	// 							}
	// 						],
	// 						order: [
	// 							["sort_order", "ASC"]
	// 						]
	// 					}////////////		Products 	///////////////

	// 				],
	// 				order: [
	// 					["sort_order", "ASC"]
	// 				]

	// 			},//////////////		Brands 		//////////////////////////

	// 		],
	// 		order: [
	// 			["sort_order", "ASC"]
	// 		]

	// 	})

	//         .then( (services) => {

	//         	var rservices = JSON.parse(JSON.stringify(services));

	//             var promises = []
	//             var event;
			
	// 			//////////////		Category
	//             rservices.forEach(function(service) {


	//             	//service.Brands = _.orderBy(service.Brands, ['sort_order'],['asc']);

	//             	//service.Brands = _.sortBy(service.Brands, o => o.sort_order);

	//             	//_.sortBy(_.pluck(data, "sortData"), ["a", "b"]);
	//             	// = ordered;

	// //////////////		Each Brands 	////////////////
	// 		       	service.Brands.forEach(function(brand) {

	// 		        	console.log('Service - ', service.category_id, brand.category_brand_id);

	// 		            promises.push(

	// 			            Promise.all([])
	// 					    .spread(function() {   // Maps results from the 2nd promise array

	// 					    	if(brand.Details.image != '')
	// 					    		brand.Details.image_url = process.env.RESIZE_URL+brand.Details.image;
	// 					    	else
	// 					    		brand.Details.image_url = '';

	// 					        return service;

	// 					    })
	// 					);


	// 			    });
	// //////////////		Each Brands 	////////////////


	//             })//////////////		Category

	//             return Promise.all(promises);

	//         });


	// return await Db.categories.findAll({

	// 	where: {
	// 		blocked: "0",
	// 		category_type: "Service",
	// 	},
	// 	attributes: {
	// 		exclude: ["blocked", "created_at", "updated_at", 'image']
	// 	},
	// 	include: [
	// 		{
	// 			as: "Details",
	// 			model: Db.category_details,
	// 			where: {language_id: data.app_language_id},
	// 			attributes: {
	// 				exclude: ["created_at", "updated_at"]
	// 			}
	// 		},
	// 		{//////////////		Brands 		//////////////////////////

	// 			as: "Brands",
	// 			model: Db.category_brands,
	// 			where: { blocked: "0" },
	// 			attributes: {
	// 				exclude: ["created_at", "updated_at", "blocked"]
	// 			},
	// 			include: [
	// 				{
	// 					as: "Details",
	// 					model: Db.category_brand_details,
	// 					where: {language_id: data.app_language_id},
	// 					attributes: ["name", "image", "description"]
	// 				},
	// 				{////////////		Products 	///////////////
	// 					as: "Products",
	// 					model: Db.category_brand_products,
	// 					where: { blocked: "0" },
	// 					attributes: {
	// 						exclude: ["blocked", "created_at", "updated_at"]
	// 					},
	// 					include: [
	// 						{
	// 							as: "Details",
	// 							model: Db.category_brand_product_details,
	// 							where: { language_id: data.app_language_id },
	// 							attributes: ["name", "description"]
	// 							// {
	// 							// 	include: ["created_at", "updated_at"]
	// 							// },
	// 						}
	// 					],
	// 					order: [
	// 						["sort_order", "ASC"]
	// 					]
	// 				}////////////		Products 	///////////////

	// 			],
	// 			order: [
	// 				["sort_order", "ASC"]
	// 			]

	// 		},//////////////		Brands 		//////////////////////////

	// 	],
	// 	order: [
	// 		["sort_order", "ASC"]
	// 	]

	// });


};


exports.getCategoryTypeById = async (data) => {
    return Db.sequelize.query("SELECT category_type FROM categories WHERE category_id="+ data.category_id +" LIMIT 0,1",{ type: Sequelize.QueryTypes.SELECT });
};
/////////////////////		