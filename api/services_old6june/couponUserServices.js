var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;

var Db = require(appRoot+"/models");//////////////////		Db Models

/////////////////////// 	Create New Row 		//////////////////////////////////////
exports.CreateNewRow = async (data) => {

	return await Db.coupon_users.create({

		coupon_id: data.coupon_id,
		///////////		Customer 	/////////////////
		user_id: data.user_id,
		user_detail_id: data.user_detail_id,
		user_type_id: data.user_type_id,
		///////////		Customer 	/////////////////

		payment_id: data.payment_id,

		rides_value: data.rides_value,
		coupon_value: data.coupon_value,

		rides_left: data.rides_left,

		price: data.price,
		coupon_type: data.coupon_type,

		deleted: "0",
		blocked: "0",

		created_at: data.created_at,
		updated_at: data.created_at,
		expires_at: data.expires_at

	});

};

//////////////////////		Latest Create Row 	//////////////////////////////////////
exports.userCouponsHistory = async (data) => {

	return await Db.coupon_users.findAll({

		where: { 
			rides_left: { $gt: 0 },
			user_detail_id: data.user_detail_id,
			expires_at: { $gte: data.created_at },
			deleted: "0",
			blocked: "0"
		},
		attributes: { exclude: ["deleted", "blocked", "created_at", "updated_at"] },
		orderBy: [ ["updated_at","DESC"] ]

	});

};

//////////////////////		Purchased Coupon 4 Order 	//////////////////////////////
exports.purchasedCouponDetail4Order = async (data) => {

	return await Db.coupon_users.findOne({

		where: { 
			coupon_user_id: data.coupon_user_id,
			user_detail_id: data.user_detail_id,
			rides_left: { $gt: 0 },
			expires_at: { $gte: data.created_at },
			deleted: "0",
			blocked: "0"
		},
		attributes: ["coupon_user_id", "coupon_value", "coupon_type"]

	});

};


////////////////////