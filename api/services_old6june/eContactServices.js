var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;


var Db = require(appRoot+"/models");//////////////////		Db Models

/////////////////////////		App Get Emergency Contacts 		//////////////////////////////////
exports.appGetEContacts = async (data) => {

	return await Db.sequelize.query("SELECT ec.emergency_contact_id, ec.phone_number, ecd.name, ec.sort_order from emergency_contacts as ec JOIN emergency_contact_details as ecd ON ecd.emergency_contact_id=ec.emergency_contact_id WHERE ecd.language_id = '"+data.app_language_id+"' AND ec.blocked='0' ORDER BY ec.sort_order ASC ",{ type: Sequelize.QueryTypes.SELECT});

};