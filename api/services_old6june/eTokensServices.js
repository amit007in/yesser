var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;

var Db = require(appRoot+"/models");//////////////////		Db Models

/////////////////		USer Etoken History 	///////////////////////////
exports.userETokenHistory = async (data) => {

	return await Db.organisation_coupon_users.findAll({

		where: { 
			quantity_left: { $gt: 0 },
			customer_user_detail_id: data.user_detail_id,
			category_id: data.category_id
		},
		orderBy: [ ["updated_at","DESC"] ]
		// include: [
		// 	{
		// 		model: Db.organisation_coupons,
		// 		as: "OrgCoupon"
		// 	}
		// ]

	})
		.then( (etokens) => {

			var promises = [];

			etokens = JSON.parse(JSON.stringify(etokens));

			etokens.forEach(function(etoken) {

				promises.push(
					Promise.all([
                      	eTokenBrandDetails(etoken.category_brand_id, data.app_language_id)
					])
						.spread(function(brands) {   // Maps results from the 2nd promise array

							etoken.brand = brands[0];
 
							return etoken;

            	})
				);

			});

			return Promise.all(promises);

		});	

};

//////////////////		Brand Details 		////////////////////////////
function eTokenBrandDetails(category_brand_id, app_language_id)
{
	return Db.sequelize.query("SELECT cbd.category_brand_id, cbd.category_id, cbd.name as category_brand_name, cbd.image, CONCAT('"+process.env.RESIZE_URL+"',cbd.image) as image_url, cbd.description from category_brands as cb JOIN category_brand_details as cbd ON ( cbd.category_brand_id=cb.category_brand_id AND cbd.language_id="+app_language_id+") WHERE (cb.category_brand_id="+category_brand_id+")   ",
		            { type: Sequelize.QueryTypes.SELECT}
		        );
}

//////////////////		Offers Listings 	//////////////////////////////////////////
exports.OffersListings = async (data) => {

	return await Db.sequelize.query("SELECT cb.category_brand_id, cb.category_id, cbd.name as category_brand_name, cbd.image, (CONCAT('"+process.env.RESIZE_URL+"',cbd.image)) as image_url, cbd.description FROM category_brands as cb JOIN category_brand_details AS cbd ON cbd.category_brand_id=cb.category_brand_id WHERE cbd.language_id="+data.app_language_id+" AND cb.blocked='0' AND cb.category_id="+data.category_id+" ORDER BY sort_order ASC ", { type: Sequelize.QueryTypes.SELECT })
		.then( (brands) => {

			var promises = [];
			brands = JSON.parse(JSON.stringify(brands));

			brands.forEach(function(brand) {

		    data.category_brand_id = brand.category_brand_id;

		    promises.push(
		        Promise.all([
		        	CategoryBrandTokenCount(data),
		            CategoryBrandTokenLists(data)
		        ])
		        .spread(function(etokens_count, etokens) {   // Maps results from the 2nd promise array

		        	brand.etokens_count = etokens_count;
		            brand.etokens = etokens;

		            return brand;

		        })
		    );

			});

			return Promise.all(promises);

		});

};

//////////////////////		Brand Tokens 		////////////////////////////////////////
function CategoryBrandTokenLists(data) {

	return Db.sequelize.query("SELECT oc.organisation_coupon_id, oc.organisation_id, oc.category_brand_product_id, oc.price, oc.description, oc.quantity, cbpd.name as category_brand_product_name, (SELECT COUNT(*) FROM organisation_areas as oa WHERE oa.deleted='0' AND oa.blocked='0' AND oa.organisation_id=o.organisation_id AND  ( (ROUND(((3959*acos(cos(radians("+data.latitude+")) * cos(radians(oa.address_latitude)) * cos(radians(oa.address_longitude)-radians("+data.longitude+"))+sin(radians("+data.latitude+"))*sin(radians(oa.address_latitude))))* 1.67),2)) <= oa.distance ) ) as dist_count FROM organisation_coupons as oc JOIN category_brand_products as cbp ON cbp.category_brand_product_id=oc.category_brand_product_id JOIN category_brand_product_details as cbpd ON cbpd.category_brand_product_id=oc.category_brand_product_id JOIN organisations as o ON o.organisation_id=oc.organisation_id WHERE o.blocked='0' AND cbpd.language_id="+data.app_language_id+" AND cbp.blocked='0' AND oc.category_id="+data.category_id+" AND oc.category_brand_id="+data.category_brand_id+" AND oc.deleted='0' AND oc.blocked='0' HAVING dist_count > 0 AND ( (SELECT COUNT(*) FROM organisation_coupon_users as ocu WHERE ocu.organisation_coupon_id=oc.organisation_coupon_id AND ocu.customer_user_detail_id="+data.user_detail_id+" AND ocu.quantity_left > 0) = 0 ) ORDER BY o.sort_order ASC, oc.updated_at DESC LIMIT "+data.skip+", "+data.take+" ",
	            { type: Sequelize.QueryTypes.SELECT}
	        );
}

//////////////////////		Brand Tokens 		////////////////////////////////////////
function CategoryBrandTokenCount(data) {

	return Db.sequelize.query("SELECT COUNT(*) as etokens_count FROM organisation_coupons as oc JOIN category_brand_products as cbp ON cbp.category_brand_product_id=oc.category_brand_product_id JOIN category_brand_product_details as cbpd ON cbpd.category_brand_product_id=oc.category_brand_product_id JOIN organisations as o ON o.organisation_id=oc.organisation_id WHERE o.blocked='0' AND cbpd.language_id="+data.app_language_id+" AND cbp.blocked='0' AND oc.category_id="+data.category_id+" AND oc.category_brand_id="+data.category_brand_id+" AND oc.deleted='0' AND oc.blocked='0' AND (SELECT COUNT(*) FROM organisation_areas as oa WHERE oa.deleted='0' AND oa.blocked='0' AND oa.organisation_id=oc.organisation_id AND ( ( (ROUND(((3959*acos(cos(radians("+data.latitude+")) * cos(radians(oa.address_latitude)) * cos(radians(oa.address_longitude)-radians("+data.longitude+"))+sin(radians("+data.latitude+"))*sin(radians(oa.address_latitude))))* 1.67),2)) <= oa.distance ) ) > 0 ) AND ((SELECT COUNT(*) FROM organisation_coupon_users as ocu WHERE ocu.organisation_coupon_id=oc.organisation_coupon_id AND ocu.customer_user_detail_id="+data.user_detail_id+" AND ocu.quantity_left > 0) = 0) ",
	            { type: Sequelize.QueryTypes.SELECT}
	        )
		.then((result) => {

			if(result.length == 0)
				return 0;
			else
				return result[0].etokens_count;

		});
}

/////////////////////		Etokens Paginate 	////////////////////////////
exports.PaginateETokens = async (data) => {
	//return await CategoryBrandTokenLists(data);
	return await filterBrandEtokens(data);
};

/////////////////////		Etoken Details 		////////////////////////////
exports.eTokenDetails = async (data) => {

	return await Db.organisation_coupons.findOne({

		where: {
			organisation_coupon_id: data.organisation_coupon_id,
			deleted: "0",
			blocked: "0"
		},
		include: [
			{
				required: false,
				model: Db.organisations,
				as: "Org",
				include: [
					{
						required: true,
						model: Db.organisation_categories,
						as: "OrgCat",
						where: { category_id: data.category_id }
					}
				]
			},
			{
				required: false,
				model: Db.user_details,
				include: [
					{
						required: true,
						model: Db.user_driver_detail_services,
						as: "UCat",
						where: { category_id: data.category_id }
					}
				]
			}
		]

	});

};

//////////////////////		Filter Brand Etokens 		//////////////////////////////////////////
exports.filterBrandEtokens = async (data) => {

	data.category_brand_product_id = data.category_brand_product_id ? data.category_brand_product_id : 0;

	var brands_string = brandsListings(data.app_language_id, data.category_id, data.category_brand_id ? data.category_brand_id : 0 );///		Brands Finding SQL String one Brand or multiple

	return await Db.sequelize.query(brands_string, { type: Sequelize.QueryTypes.SELECT })
		.then( (brands) => {

			var promises = [];
			brands = JSON.parse(JSON.stringify(brands));

			brands.forEach(function(brand) {


			    promises.push(
			        Promise.all([
			        	//CategoryBrandTokenCount(data),
						//eTokensListingFileringCount(data.user_detail_id, data.app_language_id, data.category_id, brand.category_brand_id, data.category_brand_product_id, data.day_string_check, data.latitude, data.longitude, data.distance, data.skip, data.take),
						eTokensListingFilering(data.user_detail_id, data.app_language_id, data.category_id, brand.category_brand_id, data.category_brand_product_id, data.day_string_check, data.latitude, data.longitude, data.distance, data.skip, data.take)
			        ])
			        .spread(function(etokens) {   // Maps results from the 2nd promise array

			        	brand.etokens_count = etokens.length;
			            brand.etokens = etokens;

			            return brand;

			        })
			    );

			});

			return Promise.all(promises);

		});


};

///////////////////		Etoken Filtering Function 		//////////////
function eTokensListingFilering(user_detail_id, app_language_id, category_id, category_brand_id, category_brand_product_id, day_string_check, latitude, longitude, distance, skip, take)
{

	if(category_brand_product_id == 0)
		var brand_product_check_string = "oc.category_brand_product_id != 0";
	else
		var brand_product_check_string = "oc.category_brand_product_id = "+category_brand_product_id;

	var area_check_string = area_check_string_func(latitude, longitude, category_id, day_string_check);////	Org Area Check String
	var purchase_check_string = purchase_check_function(user_detail_id);// Currently unexpired etoken or not

	return Db.sequelize.query("SELECT oc.organisation_coupon_id, o.organisation_id, oc.category_brand_product_id, oc.price, oc.description, oc.quantity, cbpd.name as category_brand_product_name FROM organisation_coupons as oc JOIN organisations as o ON (o.organisation_id=oc.organisation_id AND o.blocked='0') JOIN category_brand_products as cbp ON (cbp.category_brand_product_id=oc.category_brand_product_id AND cbp.blocked='0') JOIN category_brand_product_details as cbpd ON (cbpd.category_brand_product_id=oc.category_brand_product_id AND cbpd.language_id="+app_language_id+" ) JOIN organisation_categories AS OC1 ON (OC1.organisation_id=o.organisation_id AND OC1.deleted='0' AND OC1.category_id=oc.category_id) WHERE oc.deleted='0' AND oc.blocked='0' AND oc.category_brand_id="+category_brand_id+" AND oc.category_id="+category_id+" AND "+brand_product_check_string+" HAVING "+area_check_string+" > 0 AND "+purchase_check_string+" ORDER BY o.sort_order ASC, oc.updated_at DESC ",
	            { type: Sequelize.QueryTypes.SELECT}
	        );

}

////////////////////		Etokens Filtering count 	////////////////
function eTokensListingFileringCount(user_detail_id, app_language_id, category_id, category_brand_id, category_brand_product_id, day_string_check, latitude, longitude, distance, skip, take)
{


	if(category_brand_product_id == 0)
		var brand_product_check_string = "oc.category_brand_product_id != 0";
	else
		var brand_product_check_string = "oc.category_brand_product_id = "+category_brand_product_id;

	var area_check_string = area_check_string_func(latitude, longitude, category_id, day_string_check);////	Org Area Check String
	var purchase_check_string = purchase_check_function(user_detail_id);// Currently unexpired etoken or not

	return Db.sequelize.query("SELECT COUNT(*) as etokens_count, oc.organisation_coupon_id, o.organisation_id FROM organisation_coupons as oc JOIN organisations as o ON (o.organisation_id=oc.organisation_id AND o.blocked='0') JOIN category_brand_products as cbp ON (cbp.category_brand_product_id=oc.category_brand_product_id AND cbp.blocked='0') JOIN category_brand_product_details as cbpd ON (cbpd.category_brand_product_id=oc.category_brand_product_id AND cbpd.language_id="+app_language_id+") WHERE oc.deleted='0' AND oc.blocked='0' AND oc.category_brand_id="+category_brand_id+" AND oc.category_id="+category_id+" AND "+brand_product_check_string+" HAVING "+area_check_string+" > 0 AND "+purchase_check_string+" LIMIT 0, 1 ",
	            { type: Sequelize.QueryTypes.SELECT}
	        )
		.then((result) => {

			if(result.length == 0)
				return 0;
			else
				return result[0].etokens_count;

		});


	/*	return Db.sequelize.query("SELECT COUNT(*) as etokens_count, oc.organisation_coupon_id, o.organisation_id FROM organisation_coupons as oc JOIN organisations as o ON (o.organisation_id=oc.organisation_id AND o.blocked='0') JOIN category_brand_products as cbp ON (cbp.category_brand_product_id=oc.category_brand_product_id AND cbp.blocked='0') JOIN category_brand_product_details as cbpd ON (cbpd.category_brand_product_id=oc.category_brand_product_id AND cbpd.language_id="+app_language_id+" ) WHERE oc.deleted='0' AND oc.blocked='0' AND oc.category_brand_id="+category_brand_id+" AND oc.category_id="+category_id+" AND "+brand_product_check_string+" HAVING "+area_check_string+" > 0 AND "+purchase_check_string+" LIMIT 0, 1 ",
	            { type: Sequelize.QueryTypes.SELECT}
	        )
	.then((result) => {

		if(result.length == 0)
			return 0;
		else
			return result[0].etokens_count;

	})*/

}

//////////////////	Already Purchased Check String 	//////////////////
function purchase_check_function(user_detail_id)
{
	return "( (SELECT COUNT(*) FROM organisation_coupon_users as ocu WHERE ocu.organisation_coupon_id=oc.organisation_coupon_id AND ocu.customer_user_detail_id="+user_detail_id+" AND ocu.quantity_left > 0) = 0 )";
}

//////////////////	Brands Listings Function 	//////////////////////
function brandsListings(app_language_id, category_id, category_brand_id) {

	if(category_brand_id == 0)
		var brand_check_string = "cb.category_brand_id != 0";
	else
		var brand_check_string = "cb.category_brand_id = "+category_brand_id;


	return "SELECT cb.category_brand_id, cb.category_id, cbd.name as category_brand_name, cbd.image, (CONCAT('"+process.env.RESIZE_URL+"',cbd.image)) as image_url, cbd.description FROM category_brands as cb JOIN category_brand_details AS cbd ON cbd.category_brand_id=cb.category_brand_id WHERE cbd.language_id="+app_language_id+" AND cb.blocked='0' AND cb.category_id="+category_id+" AND "+brand_check_string+" ORDER BY sort_order ASC ";

}

//////////////////////////

var area_check_string_func = function(latitude, longitude, category_id, day_string_check)
{
// ud
	return "(SELECT COUNT(*) FROM organisation_areas as oa where oa.organisation_id=o.organisation_id AND oa.category_id="+category_id+" AND oa.deleted='0' AND oa.blocked='0' AND "+day_string_check+" AND ( ROUND(((3959*acos(cos(radians("+latitude+")) * cos(radians(oa.address_latitude)) * cos(radians(oa.address_longitude)-radians("+longitude+"))+sin(radians("+latitude+"))*sin(radians(oa.address_latitude))))* 1.67),2)  <= oa.distance )  ) ";


};
