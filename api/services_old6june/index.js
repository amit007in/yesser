module.exports = {

	categoryBrandProductServices: require(appRoot+"/services/categoryBrandProductServices"),
	categoryServices: require(appRoot+"/services/categoryServices"),
	couponServices: require(appRoot+"/services/couponServices"),

	driverListingServices: require(appRoot+"/services/driverListingServices"),

	eContactServices: require(appRoot+"/services/eContactServices"),
	eTokensServices: require(appRoot+"/services/eTokensServices"),

	orgServices: require(appRoot+"/services/orgServices"),
	orgCouponUserServices: require(appRoot+"/services/orgCouponUserServices"),

	paymentServices: require(appRoot+"/services/paymentServices"),
	orderServices: require(appRoot+"/services/orderServices"),
	orgCouponUserServices: require(appRoot+"/services/orgCouponUserServices"),

	userBankDetailServices: require(appRoot+"/services/userBankDetailServices"),
	couponUserServices: require(appRoot+"/services/couponUserServices"),
	userServices: require(appRoot+"/services/userServices"),
	userDetailServices: require(appRoot+"/services/userDetailServices"),

	waterServices: require(appRoot+"/services/waterServices"),

};