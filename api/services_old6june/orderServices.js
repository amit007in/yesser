var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;

var Db = require(appRoot + "/models");//////////////////		Db Models
var Configs = require(appRoot + "/configs");/////////////		Configs

/////////////////////////////////////////////////
////////////////////		Attributes 			//////////////////////////////////
/////////////////////////////////////////////////
var orderDetailsSeqAtts = [
    "order_id", "order_token", "customer_user_id", "customer_user_detail_id", "customer_organisation_id", "customer_user_type_id", "driver_user_id", "driver_user_detail_id", "driver_organisation_id", "driver_user_type_id", "category_id", "category_brand_id", "category_brand_id", "category_brand_product_id", "continuous_order_id", "order_status", "pickup_address", "pickup_latitude", "pickup_longitude", "dropoff_address", "dropoff_latitude", "dropoff_longitude", "order_type", "created_by", "order_timings", "future", "continouos_startdt", "continuous_enddt", "continuous_time", "continuous", "cancel_reason", "cancelled_by", "organisation_coupon_user_id", "coupon_user_id", "track_path", "track_image", "details", "material_details", "my_turn", "created_at", "updated_at",
    [Sequelize.literal("(CASE WHEN track_image=\"\" THEN \"\" ELSE CONCAT(\"" + process.env.RESIZE_URL + "\", track_image) END)"), "track_image_url"]
];

var oRatingsSeqAtts = [
    "order_rating_id", "order_id", "ratings", "comments", "created_by", "created_at"
];

var oImagesSeqAtts = [
    "order_image_id", "image", "order_id",
    [Sequelize.literal("(CONCAT(\"" + process.env.RESIZE_URL + "\", image))"), "image_url"]
];

var oReqSeqAtts = ["order_request_id", "order_id", "accepted_at", "confirmed_at", "started_at", "updated_at", "full_track"];

/////////////////////////////////////////////////
////////////////////		Attributes 			//////////////////////////////////
/////////////////////////////////////////////////


/////////////////////////////////////////////////
////////////////////		Attributes 			//////////////////////////////////
/////////////////////////////////////////////////
var oCouponsHistoryInclude = {
    model: Db.organisation_coupon_users,
    required: false,
    attributes: [
        "organisation_coupon_user_id", "organisation_coupon_id", "bottle_quantity", "quantity", "quantity_left", "address", "address_latitude", "address_longitude",
        [Sequelize.literal("(SELECT SUM(final_charge) FROM payments P WHERE P.organisation_coupon_user_id=orders.organisation_coupon_user_id AND P.payment_status=\"Pending\" AND P.refund_status=\"NoNeed\" )"), "pendingPaymentAmount"]
    ]
};

/////////////////////////////////////////////////
////////////////////		Attributes 			//////////////////////////////////
/////////////////////////////////////////////////

exports.CreateProducts = async (productss) => {
    return Db.order_products.create({
        order_id: productss.order_id,
        category_id: productss.category_id,
        category_brand_id: productss.category_brand_id,
        category_brand_product_id: productss.category_brand_product_id,
		product_weight: productss.product_weight,
        price_per_item: productss.price_per_item,
        image_url: productss.image_url,
        productName: productss.productName,
        product_quantity: productss.product_quantity,
        created_at: productss.created_at,
        updated_at: productss.created_at
    })
};


exports.DeductCouponRides = async (productss) => {
    
    var coupon = await Db.coupons.findOne({where: {coupon_id: productss.coupon_id}});
    console.log("couponapplied===========",coupon);
    /*return Db.order_products.create({
        order_id: productss.order_id,
        category_id: productss.category_id,
        category_brand_id: productss.category_brand_id,
        category_brand_product_id: productss.category_brand_product_id,
		product_weight: productss.product_weight,
        price_per_item: productss.price_per_item,
        image_url: productss.image_url,
        productName: productss.productName,
        product_quantity: productss.product_quantity,
        created_at: productss.created_at,
        updated_at: productss.created_at
    })*/
};

//////////////////////		Latest Create Row 	//////////////////////////////////////
exports.latestCreateNewRow = async (data) => {

    return await Db.orders.create({

        order_token: data.order_token,
        /////////		Customer 	/////////////////
        customer_user_id: data.customer_user_id,
        customer_user_detail_id: data.customer_user_detail_id,
        customer_organisation_id: data.customer_organisation_id,
        customer_user_type_id: data.customer_user_type_id,
        /////////		Customer 	/////////////////

        ///////////		Driver 	/////////////////////
        driver_user_id: 0,
        driver_user_detail_id: 0,
        driver_organisation_id: 0,
        driver_user_type_id: data.driver_user_type_id,
        ///////////		Driver 	/////////////////////

        ///////////		Category 	/////////////////
        category_id: data.category_id,
        category_brand_id: data.category_brand_id,
        category_brand_product_id: data.category_brand_product_id,
        ///////////		Category 	/////////////////

        order_status: "Searching",

        pickup_address: data.pickup_address ? data.pickup_address : "",
        pickup_latitude: data.pickup_latitude ? data.pickup_latitude : 0.0,
        pickup_longitude: data.pickup_longitude ? data.pickup_longitude : 0.0,

        dropoff_address: data.dropoff_address ? data.dropoff_address : "",
        dropoff_latitude: data.dropoff_latitude ? data.dropoff_latitude : 0.0,
        dropoff_longitude: data.dropoff_longitude ? data.dropoff_longitude : 0.0,

        order_type: data.order_type,
        created_by: data.created_by,

        order_timings: data.order_timings,
        future: data.future,

        continuous_order_id: data.continuous_order_id ? data.continuous_order_id : 0,
        continouos_startdt: data.continouos_startdt ? data.continouos_startdt : null,
        continuous_enddt: data.continuous_enddt ? data.continuous_enddt : null,
        continuous_time: data.continuous_time ? data.continuous_time : null,
        continuous: data.continuous ? data.continuous : "0",

        cancel_reason: "",
        cancelled_by: "",

        organisation_coupon_user_id: data.organisation_coupon_user_id,
        coupon_user_id: data.coupon_user_id,

        track_path: "",
        track_image: "",
        details: data.details ? data.details : "",
        material_details: data.material_details ? data.material_details : "",

        created_at: data.created_at,
        updated_at: data.created_at
    })/*.then((order) => { ///////////////		Create Payment 	//////////////////
			order                 = JSON.parse(JSON.stringify(order));
			var products          = [];
			order.app_language_id = data.app_language_id;
			var orsss             = orderProduct(order);
            order.products        = JSON.parse(JSON.stringify(orsss));
            return order;
    })*/.then((order) => { ///////////////		Create Payment 	//////////////////

                order = JSON.parse(JSON.stringify(order));

                return Db.payments.create({
                    
                    customer_user_id: data.user_id,
                    customer_user_detail_id: data.user_detail_id,
                    customer_user_type_id: data.user_type_id,
                    user_card_id: 0,
                    customer_organisation_id: data.user_organisation_id,

                    order_id: order.order_id,
                    organisation_coupon_id: 0,

                    seller_user_id: 0,
                    seller_user_detail_id: 0,
                    seller_user_type_id: data.seller_user_type_id,
                    seller_organisation_id: 0,

                    payment_type: data.payment_type,
                    payment_status: "Pending",
                    refund_status: "NoNeed",

                    transaction_id: "",
                    refund_id: "",

                    buraq_percentage: data.buraq_percentage,

                    product_actual_value: data.product_actual_value,
                    product_quantity: data.product_quantity,
                    product_weight: data.product_weight,
                    product_sq_mt: data.product_sq_mt0,
                    order_distance: data.order_distance,
                    order_time: 0,

                    product_alpha_charge: data.product_alpha_charge,
                    product_per_quantity_charge: data.product_per_quantity_charge,
                    product_per_weight_charge: data.product_per_weight_charge,
                    product_per_distance_charge: data.product_per_distance_charge,
                    product_per_hr_charge: data.product_per_hr_charge,
                    product_per_sq_mt_charge: data.product_per_sq_mt_charge,

                    initial_charge: data.initial_charge,
                    admin_charge: data.admin_charge,
                    bank_charge: 0,
                    final_charge: data.final_charge,

                    bottle_charge: data.bottle_charge,
                    bottle_returned_value: data.bottle_returned_value ? data.bottle_returned_value : 0,
                    
                    coupon_id: data.coupon_id ? data.coupon_id : 0,
                    discounted_value: data.discounted_value,
                    
                    created_at: data.created_at,
                    updated_at: data.created_at

                }).then(function (payment) {

                    order.payment = JSON.parse(JSON.stringify(payment));

                    order.payment.bottle_charge = parseFloat(order.payment.bottle_charge);

                    //console.log(JSON.stringify(order, null, 2));

                    return order;

                });

            })///////////////		Create Payment 	//////////////////
            .then(async (order) => {

                var promises = [];
                var order_requests = [];
                var fcm_ids = [];
                var socket_ids = [];
                var fcm_socket_ids = [];

                //order = JSON.parse(JSON.stringify(order));

                data.drivers.forEach(function (driver) {

                    order_requests.push({
                        order_id: order.order_id,
                        driver_user_id: driver.user_id,
                        driver_user_detail_id: driver.user_detail_id,
                        driver_organisation_id: driver.organisation_id,
                        order_request_status: "Searching",
                        driver_request_latitude: driver.latitude,
                        driver_request_longitude: driver.longitude,
                        driver_current_latitude: driver.latitude,
                        driver_current_longitude: driver.longitude,
                        accepted_at: null,
                        driver_confirmed_latitude: 0.0,
                        driver_confirmed_longitude: 0.0,
                        driver_started_latitude: 0.0,
                        driver_started_longitude: 0.0,
                        full_track: "[]",
                        created_at: data.created_at,
                        updated_at: data.created_at
                    });

                    /////////////////////////		Language 	/////////////////////
                    if (driver.language_id == 1)
                        var locale = "1En";
                    else if (driver.language_id == 2)
                        var locale = "2Hi";
                    else if (driver.language_id == 3)
                        var locale = "3Ur";
                    else if (driver.language_id == 4)
                        var locale = "4Ch";
                    else
                        var locale = "5Ar";

                    var message = "";
                    message = data.user_name + trans({phrase: " has requested a new service", locale});
                    /////////////////////////		Language 	/////////////////////
                    fcm_socket_ids.push({
                        driver_user_id: driver.user_id,
                        driver_user_detail_id: driver.user_detail_id,
                        fcm_id: driver.fcm_id,
                        socket_id: driver.socket_id,
                        language_id: driver.language_id,
                        buraq_percentage: driver.buraq_percentage,
                        bottle_charge: parseFloat(driver.bottle_charge),
                        notifications: driver.notifications,
                        message
                    });

                });

                if (order_requests.length != 0)
                    order.order_requests = await Db.order_requests.bulkCreate(order_requests);

                order.fcm_ids = fcm_ids;
                order.socket_ids = socket_ids;
                order.fcm_socket_ids = fcm_socket_ids;
				
                //console.log("order_request ====", order_requests, null, 2);

                return order;
            });

};


//////////////////////		Create Row 		//////////////////////////////////////////
exports.createNewRow = async (data) => {

    return await Db.orders.create({

        order_token: data.order_token,

        /////////		Customer 	/////////////////
        customer_user_id: data.customer_user_id,
        customer_user_detail_id: data.customer_user_detail_id,
        customer_organisation_id: data.customer_organisation_id,
        customer_user_type_id: data.customer_user_type_id,
        user_card_id: data.user_card_id,
        /////////		Customer 	/////////////////

        ///////////		Driver 	/////////////////////
        driver_user_id: 0,
        driver_user_detail_id: 0,
        driver_organisation_id: 0,
        driver_user_type_id: data.driver_user_type_id,
        ///////////		Driver 	/////////////////////

        ///////////		Category 	/////////////////
        category_id: data.category_id,
        category_brand_id: data.category_brand_id,
        category_brand_product_id: data.category_brand_product_id,
        ///////////		Category 	/////////////////

        continuous_order_id: data.continuous_order_id ? data.continuous_order_id : 0,

        order_status: "Searching",
        payment_status: "Pending",
        refund_status: "NoNeed",

        pickup_address: data.pickup_address,
        pickup_latitude: data.pickup_latitude,
        pickup_longitude: data.pickup_longitude,

        dropoff_address: data.dropoff_address ? data.dropoff_address : "",
        dropoff_latitude: data.dropoff_latitude ? data.dropoff_latitude : 0.0,
        dropoff_longitude: data.dropoff_longitude ? data.dropoff_longitude : 0.0,

        category_type: data.category_type ? data.category_type : "Service",
        order_type: data.order_type,
        created_by: data.created_by,

        order_timings: data.order_timings,
        future: data.future,

        continouos_startdt: data.continouos_startdt ? data.continouos_startdt : null,
        continuous_enddt: data.continuous_enddt ? data.continuous_enddt : null,
        continuous_time: data.continuous_time ? data.continuous_time : null,
        continuous: data.continuous ? data.continuous : "0",

        transaction_id: "",
        payment_type: data.payment_type,
        cancel_reason: "",
        refund_id: "",

        cancelled_by: "",
        order_weight: data.order_weight ? data.order_weight : 0,
        order_distance: "0",

        initial_charge: data.initial_charge,
        admin_charge: data.admin_charge,
        final_charge: data.final_charge,
        bottle_charge: data.bottle_charge ? data.bottle_charge : 0,

        bottle_returned: data.bottle_returned ? data.bottle_returned : "2",
        bottle_returned_value: data.bottle_returned_value,
        bottle_first_time_value: data.bottle_first_time_value,

        product_quantity: data.product_quantity,

        product_actual_value: data.product_actual_value,
        product_alpha_price: data.product_alpha_price,
        product_price_per_quantity: data.product_price_per_quantity,
        product_price_per_distance: data.product_price_per_distance ? data.product_price_per_distance : "0",
        product_price_per_weight: data.product_price_per_weight ? data.product_price_per_weight : "0",

        organisation_coupon_user_id: data.organisation_coupon_user_id ? data.organisation_coupon_user_id : 0,

        created_at: data.created_at,
        updated_at: data.updated_at


    })
            .then(async (order) => {

                var promises = [];
                var order_requests = [];
                var fcm_ids = [];
                var socket_ids = [];

                order = JSON.parse(JSON.stringify(order));

                data.drivers.forEach(function (driver) {

                    order_requests.push({
                        order_id: order.order_id,
                        driver_user_id: driver.user_id,
                        driver_user_detail_id: driver.user_detail_id,
                        driver_organisation_id: driver.organisation_id,
                        order_request_status: "Searching",
                        driver_request_latitude: driver.latitude,
                        driver_request_longitude: driver.longitude,
                        driver_current_latitude: driver.latitude,
                        driver_current_longitude: driver.longitude,
                        accepted_at: null,
                        driver_confirmed_latitude: 0.0,
                        driver_confirmed_longitude: 0.0,
                        driver_started_latitude: 0.0,
                        driver_started_longitude: 0.0,
                        full_track: "[]",
                        created_at: data.created_at,
                        updated_at: data.created_at
                    });

                    if (driver.fcm_id != "")
                        fcm_ids.push(driver.fcm_id);
                    if (driver.socket_id != "")
                        socket_ids.push(driver.socket_id);

                });

                if (order_requests.length != 0)
                    order.order_requests = await Db.order_requests.bulkCreate(order_requests);

                order.fcm_ids = fcm_ids;
                order.socket_ids = socket_ids;

                return order;
            });


};

///////////////////////////		Order Details 4 Drivers Socket Emit 	////////////////////////////////
exports.orderDetails4SocketEmit = async (data) => {

    var order = await Db.orders.findOne({where: {order_id: data.order_id}});
    // .then( (order) => {

    // 	var promises = [];

    // 	order = JSON.parse(JSON.stringify(order));

    //        services.forEach(function(service) {

    //        	data.category_id = service.category_id;

    //                promises.push(
    //                    Promise.all([
    //                      	serviceBrandsWithDetails(data)
    //                ])
    //                .spread(function(brands) {   // Maps results from the 2nd promise array

    //                    service.brands = brands;

    //                    return service;

    //            	})
    //            );

    //        });

    //            return Promise.all(promises);

    // });			

};

////////////////////////////		User Current Orders 		///////////////////////////////////
exports.customerCurrentOrders = async (data) => {  console.log("customerCurrentOrders ==", data);

    return Db.orders.findAll({
        where: {
            customer_user_detail_id: data.user_detail_id,
            order_status: {$in: ["Searching", "Ongoing", "Confirmed"]},
            organisation_coupon_user_id: 0
        },
        include: [{
                model: Db.payments,
            },
            oCouponsHistoryInclude
        ],
        order: [["updated_at", "DESC"]]
    })
            .then((orders) => {
                var promises = [];
                orders = JSON.parse(JSON.stringify(orders));
                console.log("orders======customer",orders);
                orders.forEach(function (order) {
                 // console.log('order 1==',order);
                    data.order_id = order.order_id;
                    data.category_brand_product_id = order.category_brand_product_id;
                    data.category_brand_id = order.category_brand_id;
                    data.driver_user_detail_id = order.driver_user_detail_id;
                   // console.log("data ===", data);
                    promises.push(
                            Promise.all([
                                orderProductWithBrandDetails(data),
                                orderDriverDetails(data),
				orderProduct(data),
                                orderPromocodes(order.payment.coupon_id)
                            ])
                            .spread(function (brands, drivers, products,promos) {   // Maps results from the 2nd promise array
                                order.brand = brands[0];
                                order.driver = (drivers.length != 0) ? drivers[0] : null;
				order.products = products.length > 0 ? products : [];
                                order.promocodes  = promos.length > 0 ? promos : [];
                                return order;
                            })
                            );

                });
                //console.log("orders", orders);
                //console.log("promises", promises);
                return Promise.all(promises);
            });

};

////////////////////////	Customer Single Order Details 	////////////////////////////////
exports.customerSingleOrderDetails = async (data) => {

    return Db.orders.findAll({
        where: {
            order_token: data.order_token
        },
        include: [{
                model: Db.payments
            }]
    })
    .then((orders) => {
        var promises = [];
        orders = JSON.parse(JSON.stringify(orders));
        orders.forEach(function (order) { // console.log('order 2==',order);
            data.category_brand_product_id = order.category_brand_product_id;
            data.category_brand_id = order.category_brand_id;
            data.driver_user_detail_id = order.driver_user_detail_id;

            promises.push(
                    Promise.all([
                        orderProductWithBrandDetails(data),
                        orderDriverDetails(data)
                    ])
                    .spread(function (brands, drivers) {   // Maps results from the 2nd promise array

                        order.brand = brands[0];
                        order.driver = (drivers.length != 0) ? drivers[0] : null;

                        return order;
                    })
            );
        });

        return Promise.all(promises);
    });
};


////////////////////////    Customer Multiple Order Details     ////////////////////////////////
exports.customerMultipleOrderDetails = async (data) => {
    return Db.orders.findAll({
        where: {
            order_token: {$in: data.order_token}
        },
         order_status: {$in: ["Searching", "Ongoing", "Confirmed"]},
         include: [{ model: Db.payments }]
    }) 
    .then((orders) => { 
        var promises = [];
        orders = JSON.parse(JSON.stringify(orders));
        orders.forEach(function (order) {  //.log('order 3==',order);
            data.category_brand_product_id = order.category_brand_product_id;
            data.category_brand_id = order.category_brand_id;
            data.driver_user_detail_id = order.driver_user_detail_id;
            data.order_id = order.order_id;
            promises.push(
                    Promise.all([
                        orderProductWithBrandDetails(data),
                        orderDriverDetails(data),
                        orderProduct(data)
                    ])
                    .spread(function (brands, drivers, products) {   // Maps results from the 2nd promise array
                        order.brand = brands[0];
                        order.driver = (drivers.length != 0) ? drivers[0] : null;
                        order.products = products.length > 0 ? products : [];
                        return order;
                    })
            );
        });
        return Promise.all(promises);
    });
};

///////////////////////////	Driver Ongoing Orders 		////////////////////////////////////
exports.driverCurrentOrders = async (data) => {

    return Db.orders.findAll({
        where: {
            driver_user_detail_id: data.user_detail_id,
            order_status: {$in: ["DApproved", "DPending", "Ongoing", "Confirmed"]}
        },
        include: [{
                model: Db.payments
            }],
        order: [["updated_at", "DESC"]]
    })
            .then((orders) => {
                var promises = [];
                orders = JSON.parse(JSON.stringify(orders));
                orders.forEach(function (order) {
		     console.log('order 4==',order);
                    data.order_id = order.order_id;
                    data.category_brand_product_id = order.category_brand_product_id;
                    data.category_brand_id = order.category_brand_id;
                    data.customer_user_detail_id = order.customer_user_detail_id;
                    promises.push(
                            Promise.all([
                                orderProductWithBrandDetails(data),
                                orderCustomerDetails(order.customer_user_detail_id),
				orderProduct(data),
                            ])
                            .spread(function (brands, users, products) {   // Maps results from the 2nd promise array
                                order.brand = brands[0];
                                order.user = (users.length != 0) ? users[0] : null;
				order.products = products.length > 0 ? products : [];
                                return order;
                            })
                            );
                });

                return Promise.all(promises);
            });

};

///////////////////		Driver Pending Orders 		////////////////////////////////////////
exports.driverPendingOrders = async (data) => {

    return Db.orders.findAll({
        where: {
            driver_user_detail_id: 0,
            order_status: {$in: ["Searching"]}
        },
        include: [{ model: Db.payments }],
        having: Sequelize.literal("( (SELECT COUNT(*) FROM order_requests WHERE order_id=orders.order_id AND driver_user_detail_id=" + data.user_detail_id + " AND order_request_status=\"Searching\") != 0 )"),
        order: [["updated_at", "DESC"]]
    })
    .then((orders) => {
        var promises = [];
        orders = JSON.parse(JSON.stringify(orders));

        orders.forEach(function (order) {  //console.log('order 5==',order);
            data.order_id = order.order_id;
            data.category_brand_product_id = order.category_brand_product_id;
            data.category_brand_id = order.category_brand_id;
            data.customer_user_detail_id = order.customer_user_detail_id;

            promises.push(
                    Promise.all([
                        orderProductWithBrandDetails(data),
                        orderCustomerDetails(order.customer_user_detail_id),
                        orderProduct(data),
                    ])
                    .spread(function (brands, users,products) {   // Maps results from the 2nd promise array
                        order.brand = brands[0];
                        order.user = (users.length != 0) ? users[0] : null;
                        order.products = products.length > 0 ? products : [];
                        return order;
                    })
            );
        });
        return Promise.all(promises);
    });

};

//////////////////		Order Product With Brand Details 		////////////////////////////
function orderProductWithBrandDetails(data){
    //return Db.sequelize.query("SELECT cbp.*, cbpd.name as name, cbpd.description as description, (SELECT name FROM category_brand_details as cbd WHERE cbd.category_brand_id=cbp.category_brand_id AND cbd.language_id=" + data.app_language_id + " ) as brand_name, cd.name as category_name, (CASE WHEN cbp.image='' THEN '' ELSE CONCAT('" + process.env.RESIZE_URL + "',cbp.image) END) as image_url FROM category_brand_products as cbp JOIN category_brand_product_details as cbpd ON cbpd.category_brand_product_id=cbp.category_brand_product_id JOIN category_details as cd ON cd.category_id=cbp.category_id WHERE cbpd.language_id=" + data.app_language_id + " AND cd.language_id=" + data.app_language_id + " AND cbp.category_brand_product_id=" + data.category_brand_product_id + " LIMIT 0,1 ",
	//return Db.sequelize.query("SELECT cbp.*, cbpd.name as name, cbpd.description as description, (SELECT name FROM category_brand_details as cbd WHERE cbd.category_brand_id=cbp.category_brand_id AND cbd.language_id=" + data.app_language_id + " ) as brand_name, cd.name as category_name, (CASE WHEN cbp.image='' THEN '' ELSE CONCAT('" + process.env.PRODUCT_URL + "',cbp.image) END) as image_url FROM category_brand_products as cbp JOIN category_brand_product_details as cbpd ON cbpd.category_brand_product_id=cbp.category_brand_product_id JOIN category_details as cd ON cd.category_id=cbp.category_id WHERE cbpd.language_id=" + data.app_language_id + " AND cd.language_id=" + data.app_language_id + " LIMIT 0,1 ",
        return Db.sequelize.query("SELECT cbp.*, cbpd.name as name, cbpd.description as description, (SELECT name FROM category_brand_details as cbd WHERE cbd.category_brand_id=cbp.category_brand_id AND cbd.language_id=" + data.app_language_id + " ) as brand_name, cd.name as category_name, (CASE WHEN cbp.image='' THEN '' ELSE CONCAT('" + process.env.RESIZE_URL + "',cbp.image) END) as image_url FROM category_brand_products as cbp JOIN category_brand_product_details as cbpd ON cbpd.category_brand_product_id=cbp.category_brand_product_id JOIN category_details as cd ON cd.category_id=cbp.category_id WHERE cbpd.language_id=" + data.app_language_id + " AND cd.language_id=" + data.app_language_id + " AND cbp.category_brand_id=" + data.category_brand_id + " LIMIT 0,1 ",
	{
		type: Sequelize.QueryTypes.SELECT
	}
    );
}

/**
*@role Get Mulitple Products
*@date May 01 2019
*/
function orderProduct(data) { 
	/*return Db.sequelize.query("SELECT product_weight,category_brand_product_id,image_url,productName,product_quantity,price_per_item FROM order_products AS products  WHERE products.order_id = "+data.order_id+"",
		{ type: Sequelize.QueryTypes.SELECT}
	);*/
    var NewProduct = new Array();
     Db.sequelize.query("SELECT cbp.price_per_weight AS product_weight, cbp.price_per_quantity AS price_per_item, cbp.image AS image_url, cbp.category_brand_product_id AS category_brand_product_id, cbpd.name AS productName, cbp.max_quantity AS product_quantity FROM orders AS orders JOIN category_brand_product_details AS cbpd ON cbpd.category_brand_product_id = orders.category_brand_product_id JOIN category_brand_products AS cbp ON cbp.category_brand_product_id = orders.category_brand_product_id WHERE cbpd.language_id = " + data.app_language_id + "  AND orders.order_id = "+data.order_id+"",
        { type: Sequelize.QueryTypes.SELECT}
    ).then(userss => {
        for (var i = 0; i <userss.length; i++) {
            NewProduct.push(userss[i]);
        }
        console.log("NewProduct1 ===",NewProduct);
    // We don't need spread here, since only the results will be returned for select queries
    });
    
    return Db.sequelize.query("SELECT product_weight,category_brand_product_id,image_url,productName,product_quantity,price_per_item FROM order_products AS products  WHERE products.order_id = "+data.order_id+"",
        { type: Sequelize.QueryTypes.SELECT}
    ).then(users => {
        for (var j = 0; j < users.length; j++) {
                NewProduct.push(users[j]);
            }    
            console.log("NewProduct ===",NewProduct);
            return NewProduct;
   });
    //console.log("NewProduct=",NewProduct);
    
    
}


///////////////////		Order Accepted Driver 		////////////////////////////////////////
function orderCustorOrDriver(data)
{

    return Db.sequelize.query("SELECT *, ud.latitude as latitude, ud.longitude as longitude, (CASE WHEN ud.profile_pic = '' THEN '' ELSE CONCAT('" + process.env.RESIZE_URL + "',ud.profile_pic) END) as profile_pic_url FROM user_details as ud JOIN users as u ON u.user_id=ud.user_id WHERE ud.user_detail_id=" + data.user_detail_id + " LIMIT 0,1 ",
            {type: Sequelize.QueryTypes.SELECT}
    );

}

///////////////////		Order Accepted Driver 		////////////////////////////////////////
function orderCustomerDetails(customer_user_detail_id)
{

    return Db.sequelize.query("SELECT u.phone_number, u.phone_code, u.user_id, u.name, ud.notifications, ud.user_detail_id, ud.user_type_id, ud.profile_pic, ud.latitude as latitude, ud.longitude as longitude, (CASE WHEN ud.profile_pic = '' THEN '' ELSE CONCAT('" + process.env.RESIZE_URL + "',ud.profile_pic) END) as profile_pic_url, (SELECT COUNT(*) FROM order_ratings as ors WHERE ors.customer_user_id=ud.user_id AND ors.customer_user_detail_id=ud.user_detail_id AND ors.created_by='Driver' AND ors.deleted='0' AND ors.blocked='0') as rating_count, (SELECT ROUND(COALESCE(AVG(ratings),0),0) FROM order_ratings as ors WHERE ors.customer_user_id=ud.user_id AND ors.customer_user_detail_id=ud.user_detail_id AND ors.created_by='Driver' AND ors.deleted='0' AND ors.blocked='0') as rating_avg FROM user_details as ud JOIN users as u ON u.user_id=ud.user_id WHERE ud.user_detail_id=" + customer_user_detail_id + " LIMIT 0,1 ",
            {type: Sequelize.QueryTypes.SELECT}
    );

}


///////////////////		Order Accepted Driver 		////////////////////////////////////////
function orderDriverDetails(data)
{

    return Db.sequelize.query("SELECT u.phone_number, u.user_id, u.name, ud.notifications, ud.user_detail_id, ud.user_type_id, ud.category_id, ud.category_brand_id, ud.profile_pic, ud.latitude as latitude, ud.longitude as longitude, (CASE WHEN ud.profile_pic = '' THEN '' ELSE CONCAT('" + process.env.RESIZE_URL + "',ud.profile_pic) END) as profile_pic_url, (SELECT COUNT(*) FROM order_ratings as ors WHERE ors.driver_user_detail_id=ud.user_detail_id AND ors.created_by='Customer' AND ors.deleted='0' AND ors.blocked='0') as rating_count, (SELECT ROUND(COALESCE(AVG(ors1.ratings),0),0) FROM order_ratings as ors1 WHERE ors1.driver_user_detail_id=ud.user_detail_id AND ors1.created_by='Customer' AND ors1.deleted='0' AND ors1.blocked='0') as rating_avg FROM user_details as ud JOIN users as u ON u.user_id=ud.user_id WHERE ud.user_detail_id=" + data.driver_user_detail_id + " LIMIT 0,1 ",
            {type: Sequelize.QueryTypes.SELECT}
    );

}



////////////////////		Order Requested Drivers 	/////////////////////////////////////
function orderRequestedDrivers(data)
{

    // return Db.order_requests.findAll({
    // 	where: {
    // 		order_id: data.order_id,
    // 		order_request_status: ["Searching", "Accepted"]
    // 	},
    // 	attributes: Configs.appData.order_request_atts///	Ats
    // });

    return Db.sequelize.query("SELECT order_request_id, order_id, driver_user_id, driver_user_detail_id, driver_organisation_id, order_request_status, ud.latitude, ud.longitude , ud.language_id, ud.socket_id, ud.fcm_id, ud.maximum_rides FROM order_requests JOIN user_details as ud ON ud.user_detail_id=order_requests.driver_user_detail_id WHERE order_requests.order_id=" + data.order_id + " AND order_requests.order_request_status IN ('Searching','Accepted') ",
            {type: Sequelize.QueryTypes.SELECT}
    );

}

///////////////////////		Customer Order Details Create Response ///////////////////////////////////
exports.latestUserBookedOrderDetails = async (data) => {

    return await Db.orders.findOne({
        attributes: Configs.appData.order_atts, ///		Atts
        where: {order_id: data.order_id},
        include: [
            {
                model: Db.payments,
                required: true,
                attributes: Configs.appData.payment_atts///		Atts
            },
            {
                model: Db.coupon_users,
                required: false,
                attributes: Configs.appData.coupon_user_atts///		Atts
            },
            {
                model: Db.organisation_coupon_users,
                required: false,
                attributes: Configs.appData.organisation_coupon_users_atts///		Atts
            },
            {
                model: Db.order_images,
                required: false,
                attributes: ["order_image_id", "image", "order_id",
                    [Sequelize.fn("concat", process.env.RESIZE_URL, Sequelize.col("image")), "image_url"]
                ]
            }
        ]

    })
            .then((order) => { // console.log('order 6==',order);

                var promises = [];

                order = JSON.parse(JSON.stringify(order));

                data.category_brand_product_id = order.category_brand_product_id;
                data.category_brand_id = order.category_brand_id;

                promises.push(
				  Promise.all([
						orderProductWithBrandDetails(data),
						orderProduct(data)
					])
					.spread(function (brands,ssorder) {   // Maps results from the 2nd promise array
						order.brand = brands[0];
						order.products        =  ssorder.length > 0 ? ssorder : [];
						return order;
					})
				);

                return Promise.all(promises);
            });

};

////////////////////////////		Customer Order For Cancellation 	//////////////////////////////////
exports.customerOrder4Cancellations = async (data) => {

    return await Db.orders.findOne({
        where: {order_id: data.order_id},
        attributes: Configs.appData.order_atts, ///	Const Atts
        include: [
            {
                required: false,
                model: Db.coupon_users,
                attributes: Configs.appData.coupon_user_atts///	Const Atts
            },
            {
                required: false,
                model: Db.organisation_coupon_users,
                attributes: Configs.appData.organisation_coupon_users_atts///	Const Atts
            },
            {
                model: Db.payments,
                attributes: Configs.appData.payment_atts
            },
            {
                required: false,
                as: "CRequest",
                model: Db.order_requests,
                where: {
                    order_request_status: "Accepted"
                },
                include: [
                    {
                        required: true,
                        model: Db.user_details,
                        as: "DriverDetails"
                    }
                ]

            }
        ]
    })
            .then((order) => {

                if (!order)
                    return [];

                var promises = [];

                order = JSON.parse(JSON.stringify(order));

                promises.push(
                        Promise.all([
                            orderRequestedDrivers(data)
                        ])
                        .spread(function (drivers) {   // Maps results from the 2nd promise array

                            order.drivers = drivers;

                            return order;

                        })
                        );

                return Promise.all(promises);

            });

};

///////////////////		Driver Order 4 Cancellation 	/////////////////////////////////////
exports.driverOrder4Cancellations = async (data) => {

    var order = await Db.orders.findOne({

        where: {order_id: data.order_id},
        attributes: Configs.appData.order_atts, ///		Order Atts
        include: [
            {
                model: Db.payments,
                attributes: Configs.appData.payment_atts///		Payment Atts
            },
            {
                as: "Customer",
                model: Db.users,
                attributes: Configs.appData.driver_cust_atts///		Atts
                        //required: false
            },
            {
                as: "CustomerDetails",
                model: Db.user_details,
                attributes: Configs.appData.driver_cust_detail_atts///		Atts
                        //required: false
            },
            {
                as: "CRequest",
                model: Db.order_requests,
                where: {
                    driver_user_detail_id: data.user_detail_id
                },
                //required: false
            },
            {
                model: Db.coupon_users,
                attributes: Configs.appData.coupon_user_atts///	Const Atts
            },
            {
                model: Db.organisation_coupon_users,
                attributes: Configs.appData.organisation_coupon_users_atts///	Const Atts
            },
        ]

    })
            .then((order) => {  //console.log('order 7==',order);

                if (!order)
                    return null;

                var promises = [];
                order = JSON.parse(JSON.stringify(order));

                data.category_brand_product_id = order.category_brand_product_id;
                data.category_brand_id = order.category_brand_id;

                promises.push(
                        Promise.all([
                            orderProductWithBrandDetails(data)
                        ])
                        .spread(function (brand) {   // Maps results from the 2nd promise array

                            order.brand = brand[0];

                            return order;

                        })
                        );

                return Promise.all(promises);

            });

    if (!order)
        return null;
    else
        return order[0];

};

///////////////////		Driver Order Detail 4 Accepting, Rejecting 	/////////////////////////
exports.driverOrderBasicDetails = async (data) => {

    var order = await Db.orders.findOne({

        where: {order_id: data.order_id},
        include: [
            {
                model: Db.payments,
                attributes: Configs.appData.payment_atts///		Payment Atts
            },
            {
                as: "Customer",
                model: Db.users,
                attributes: Configs.appData.driver_cust_atts, ///		Atts
                include: [
                    {
                        required: false,
                        model: Db.user_cards,
                        as: "Default",
                        where: {is_default: "1", deleted: "0"}
                    }
                ]
            },
            {
                as: "CustomerDetails",
                model: Db.user_details,
                attributes: Configs.appData.driver_cust_detail_atts///		Atts
                        //required: false
            },
            {
                as: "CRequest",
                model: Db.order_requests,
                where: {
                    driver_user_detail_id: data.user_detail_id
                },
                //required: false
            },
            {
                model: Db.coupon_users,
                attributes: Configs.appData.coupon_user_atts, ///	Const Atts
                required: false
            },
            {
                model: Db.organisation_coupon_users,
                attributes: Configs.appData.organisation_coupon_users_atts, ///	Const Atts
                required: false
            },
        ]

    })
            .then((order) => {  //console.log('order 8==',order);

                if (!order)
                    return null;

                var promises = [];
                order = JSON.parse(JSON.stringify(order));

                data.category_brand_product_id = order.category_brand_product_id;
                data.category_brand_id = order.category_brand_id;

                promises.push(
                        Promise.all([
                            orderORequestedDrivers(data),
                            orderProductWithBrandDetails(data),
                            orderCustomerDetails(order.customer_user_detail_id),
			    orderProduct(data)
                        ])
                        .spread(function (ORequests, brand, user,products) {   // Maps results from the 2nd promise array

                            order.ORequests = ORequests;
                            order.brand = brand[0];
                            order.user = user.length > 0 ? user[0] : {};
			    order.products  = products.length > 0 ? products : [];
                            return order;

                        })
                        );

                return Promise.all(promises);

            });

    if (!order)
        return null;
    else
        return order[0];

};

///////////////////		Custoemr Receiving Order Socket Data 	///////////////////////////

exports.driverAcceptOrderDetails = async (ddata, language_id) => {

    return await Db.orders.findOne({
        where: {order_id: ddata.order_id},
        include: [{ model: Db.payments }]
    })
    .then((order) => {
        if (!order)
            return null;

        var promises = [];
        order = JSON.parse(JSON.stringify(order));

        ddata.category_brand_product_id = order.category_brand_product_id;
        ddata.driver_user_detail_id = order.driver_user_detail_id;
        ddata.customer_user_detail_id = order.customer_user_detail_id;
        ddata.app_language_id = language_id;
        ddata.order_id = order.order_id;
        promises.push(
                Promise.all([
                    orderProductWithBrandDetails(ddata),
                    orderDriverDetails(ddata),
                    orderProduct(ddata),
                    orderPromocodes(order.payment.coupon_id)
                ])
                .spread(function (brands, drivers, products,promos) {   // Maps results from the 2nd promise array
                    order.brand = brands[0];
                    order.driver = (drivers.length != 0) ? drivers[0] : null;
                    order.products  = products.length > 0 ? products : [];
                    order.promocodes  = promos.length > 0 ? promos : [];
                    return order;

                })
                );

        return Promise.all(promises);

    });

};


////////////////	Drivers Other Requested Same Order 	////////////////////////////////
function orderORequestedDrivers(data) {

    return Db.sequelize.query("SELECT * FROM order_requests JOIN user_details as ud ON ud.user_detail_id=order_requests.driver_user_detail_id WHERE order_requests.order_id=" + data.order_id + " AND order_requests.order_request_status IN ('Searching') AND order_requests.driver_user_detail_id != " + data.user_detail_id + " ",
            {type: Sequelize.QueryTypes.SELECT}
    );

}

/////////////////////	Driver Order Request Accept Response 	////////////////////////
exports.driverOrderDetails = async (data) => {

    return await Db.orders.findOne({

        where: {order_id: data.order_id},
        attributes: Configs.appData.order_atts,
        include: [
            {
                model: Db.payments,
                attributes: Configs.appData.payment_atts
            },
            {
                as: "Customer",
                model: Db.users,
                attributes: Configs.appData.driver_cust_atts///		Atts
            },
            {
                as: "CustomerDetails",
                model: Db.user_details,
                attributes: Configs.appData.driver_cust_detail_atts///		Atts
            },
            {
                as: "CRequest",
                model: Db.order_requests,
                where: {
                    driver_user_detail_id: data.user_detail_id
                }
            },
            {
                as: "cRequest",
                required: false,
                model: Db.order_requests,
                where: {
                    driver_user_detail_id: data.user_detail_id, order_request_status: "Accepted"
                }
            },
            {
                required: false,
                model: Db.coupon_users,
                attributes: Configs.appData.driver_coupon_user_atts //["coupon_user_id", "coupon_value", "coupon_type"]
            }
        ]

    });

};

////////////////////////		Customer Details For Ratings Etc 		/////////////////
exports.customerOrder4Push = async (data) => {

    return await Db.orders.findOne({

        where: {order_id: data.order_id},
        include: [
            {
                required: true,
                as: "Driver",
                model: Db.users,
            },
            {
                required: true,
                as: "DriverDetails",
                model: Db.user_details
            },
            {
                required: false,
                as: "CustRatings",
                model: Db.order_ratings,
                where: {created_by: "Customer"}
            },
            {
                required: true,
                as: "CRequest",
                model: Db.order_requests,
                where: {
                    order_request_status: "Accepted"
                }
            }
        ]

    });

};

////////////////////////////		Driver Order Details 4 Push 	///////////////////
exports.driverOrder4Push = async (data) => {

    return await Db.orders.findOne({

        where: {order_id: data.order_id},
        include: [
            {
                required: true,
                as: "Customer",
                model: Db.users,
            },
            {
                required: true,
                as: "CustomerDetails",
                model: Db.user_details
            },
            {
                required: false,
                as: "DriverRatings",
                model: Db.order_ratings,
                where: {created_by: "Driver"}
            },
            {
                required: true,
                as: "CRequest",
                model: Db.order_requests,
                where: {
                    order_request_status: "Accepted"
                }
            }
        ]

    });

};

/////////////////////		Order Details for timeout 	////////////////////////////////
exports.currentOrderTimeoutDetails = async (order_id) => {

    var order = await Db.orders.findOne({

        where: {order_id: order_id},
        include: [
            {
                required: true,
                as: "Customer",
                model: Db.users,
            },
            {
                required: true,
                as: "CustomerDetails",
                model: Db.user_details
            },
            {
                required: false,
                as: "CRequest",
                model: Db.order_requests,
                where: {
                    order_request_status: "Accepted"
                },
                include: [
                    {
                        required: true,
                        model: Db.user_details,
                        as: "DriverDetails"
                    }
                ]

            }
        ]

    })
            .then(async (order) => {

                if (!order)
                    return [null];

                var promises = [];

                order = JSON.parse(JSON.stringify(order));

                order.drivers = [];
                order.fcm_ids = [];
                order.socket_ids = [];
                order.order_request_ids = [];

                promises.push(
                        Promise.all([
                            orderRequestPendingDrivers(order_id)
                        ])
                        .spread(function (drivers) {   // Maps results from the 2nd promise array

                            drivers.forEach(function (driver) {

                                if (driver.fcm_id != "")
                                    order.fcm_ids.push(driver.fcm_id);
                                if (driver.socket_id != "")
                                    order.socket_ids.push(driver.socket_id);

                                order.order_request_ids.push(driver.order_request_id);

                            });

                            order.drivers = drivers;

                            return order;

                        })

                        );

                return Promise.all(promises);

            });

    return order[0];

};

////////////////////		Order Requested Drivers 	/////////////////////////////////////
function orderRequestPendingDrivers(order_id)
{

    return Db.sequelize.query("SELECT * FROM order_requests JOIN user_details as ud ON ud.user_detail_id=order_requests.driver_user_detail_id WHERE order_requests.order_id=" + order_id + " AND order_requests.order_request_status IN ('Searching') ",
            {type: Sequelize.QueryTypes.SELECT}
    );

}

/////////////////////	Future Ride Timeout 			/////////////////////////////////////
exports.scheduledOrderTimeoutDetails = async (data) => {

    return await Db.orders.findOne({

        where: {order_id: order_id},
        include: [
            {
                required: true,
                as: "Customer",
                model: Db.users,
            },
            {
                required: true,
                as: "CustomerDetails",
                model: Db.user_details
            },
            {
                required: true,
                as: "Driver",
                model: Db.users
            },
            {
                required: true,
                as: "DriverDetails",
                model: Db.user_details
            }
        ]

    });

    return order[0];

};

/////////////////////		OrdersStarting in 1 hrs 	////////////////////////////////////////
exports.startingInOneHrOrders = async (data) => {

    return await Db.orders.findAll({

        where: {
            // order_timings: {
            // 	$lte: data.moment_1hr,
            // 	$gte: data.moment
            // },
            order_status: "Scheduled",
            future: "1"
        },

        include: [
            {
                as: "Customer",
                model: Db.users,
            },
            {
                as: "CustomerDetails",
                model: Db.user_details
            },
            {
                as: "CustomerOrg",
                model: Db.organisations
            },
            {
                as: "Driver",
                model: Db.users
            },
            {
                as: "DriverDetails",
                model: Db.user_details
            },
            {
                as: "DriverOrg",
                model: Db.organisations
            },
            {
                as: "CRequest",
                model: Db.order_requests,
                where: {order_request_status: "Accepted"}
            }
        ]

    });
    // .then( sync (orders) => {

    // 	var promises = [];

    // 	orders = JSON.parse(JSON.stringify(orders));



    // });

};

//////////////////////		Panel Track Order 		//////////////////////////////////////////////////
exports.PanelTrackOrder = async (data) => {
    return await Db.orders.findOne({
        where: {order_id: data.order_id},
        include: [{
                as: "CRequest",
                model: Db.order_requests,
                where: {order_request_status: "Accepted"},
                required: false
            }]
    })
            .then((order) => {
                if (order.CRequest)
                    order.CRequest.full_track = JSON.parse(order.CRequest.full_track);
                return order;
            });
};

//////////////////////////		User Ride Details 	/////////////////////////////////
exports.RideDetails = async (data) => {

    return Db.sequelize.query("SELECT *, (CASE WHEN track_image='' THEN '' ELSE CONCAT('" + process.env.RESIZE_URL + "',track_image) END) as track_image_url FROM orders as o WHERE o.order_id=" + data.order_id + " LIMIT 0,1 ",
            {type: Sequelize.QueryTypes.SELECT}
    )
            .then((orders) => {

                var promises = [];

                orders = JSON.parse(JSON.stringify(orders));

                orders.forEach(function (order) {

                    promises.push(
                            Promise.all([
                                userDynamicDetails(order.driver_user_detail_id),
                                userDynamicDetails(order.customer_user_detail_id),
                                orderBrandDetails(order.category_id, order.category_brand_id, order.category_brand_product_id, data.app_language_id),
                                orderPaymentDetails(order.order_id),
                                orderCouponDetails(order.coupon_user_id),
                                //orderEtokenDetails(order.organisation_coupon_user_id)
                                orderRatings(order.order_id, "Customer"),
                                orderRatings(order.order_id, "Driver")

                            ])
                            .spread(function (driver, customer, brand, payments, coupon, /*etoken*/ ratingByUser, ratingByDriver) {   // Maps results from the 2nd promise array

                                //if(driver.length)

                                order.driver = driver.length > 0 ? driver[0] : {};
                                order.customer = customer.length > 0 ? customer[0] : {};
                                order.brand = brand.length > 0 ? brand[0] : {};
                                order.payment = payments[0];
                                order.coupon = coupon.length > 0 ? coupon[0] : {};
                                //order.etoken = etoken.length > 0 ? etoken[0] : {};
                                order.ratingByUser = ratingByUser.length > 0 ? ratingByUser[0] : {};
                                order.ratingByDriver = ratingByDriver.length > 0 ? ratingByDriver[0] : {};

                                return order;

                            })
                            );

                });

                return Promise.all(promises);

            });

};

//////////////////////////		Custoemt Ride Details 	//////////////////////////////
exports.customerRideDetails = async (data) => {

    return Db.orders.findAll({
        attributes: orderDetailsSeqAtts,
        where: {order_id: data.order_id},
        include: [
            {
                model: Db.payments
            },
            {
                required: false,
                model: Db.order_ratings,
                as: "ratingByUser",
                where: {created_by: "Customer"},
                attributes: oRatingsSeqAtts
            },
            {
                required: false,
                model: Db.order_ratings,
                as: "ratingByDriver",
                where: {created_by: "Driver"},
                attributes: oRatingsSeqAtts
            },
            {
                required: false,
                model: Db.order_images,
                as: "order_images_url",
                attributes: oImagesSeqAtts
            },
            {
                required: false,
                model: Db.order_requests,
                as: "cRequest",
                where: {order_request_status: "Accepted"},
                attributes: oReqSeqAtts
            }
        ]
    })
            .then((orders) => {

                var promises = [];

                orders = JSON.parse(JSON.stringify(orders));

                orders.forEach(function (order) {
                    data.order_id = order.order_id;
                    promises.push(
                            Promise.all([
                                userDynamicDetails(order.driver_user_detail_id),
                                orderBrandDetails(order.category_id, order.category_brand_id, order.category_brand_product_id, data.app_language_id),
                                orderCouponDetails(order.coupon_user_id),
                                orderProduct(data),
                                orderPromocodes(order.payment.coupon_id)
                                        //orderEtokenDetails(order.organisation_coupon_user_id)
                                        //orderCRequest(order.order_id)
                            ])
                            .spread((driver, brand, coupon,products,promos) => {
                                console.log("prodcuts==",products);
                                order.driver = driver.length > 0 ? driver[0] : {};
                                order.brand = brand.length > 0 ? brand[0] : {};
                                order.coupon = coupon.length > 0 ? coupon[0] : {};
                                order.products = products.length > 0 ? products : [];
                                order.promocode = promos.length > 0 ? promos : [];
                                order.ratingByUser = order.ratingByUser != null ? order.ratingByUser : {};
                                order.ratingByDriver = order.ratingByDriver != null ? order.ratingByDriver : {};
                                order.cRequest = order.cRequest != null ? order.cRequest : {};
                                //order.cRequest = cRequest.length > 0 ? cRequest[0] : {};

                                return order;

                            })
                            );

                });

                return Promise.all(promises);

            });

};

//////////////////////		Order C Request 		/////////////////////////////////////
function orderCRequest(order_id)
{
    return Db.sequelize.query("SELECT accepted_at, confirmed_at, started_at, updated_at FROM order_requests WHERE order_id=" + order_id + " AND order_request_status='Accepted' ",
            {type: Sequelize.QueryTypes.SELECT}
    );
}

//////////////////////////	User Last Completed Order 	////////////////////////////////
exports.customerLastCompletedOrders = async (user_detail_id, app_language_id) => {

    return Db.sequelize.query("SELECT *, (CASE WHEN track_image='' THEN '' ELSE CONCAT('" + process.env.RESIZE_URL + "',track_image) END) as track_image_url FROM orders as o JOIN order_requests ON (order_requests.order_id=o.order_id AND order_requests.order_request_status='Accepted') WHERE o.customer_user_detail_id=" + user_detail_id + " AND o.order_status IN ('SerComplete', 'SupComplete') HAVING ((SELECT COUNT(*) FROM order_ratings WHERE order_id=o.order_id AND created_by='Customer') = 0) ORDER BY o.updated_at DESC LIMIT 0,1 ",
            {type: Sequelize.QueryTypes.SELECT}
    )
            .then((orders) => {

                var promises = [];

                orders = JSON.parse(JSON.stringify(orders));
                console.log("customerLastCompletedOrders ==", orders);
                orders.forEach(function (order) { console.log("customerLastCompletedOrders order_id", order.order_id);

                    var data = {
                        category_brand_product_id: order.category_brand_product_id,
                        app_language_id,
                        order_id: order.order_id,
                        category_brand_id: order.category_brand_id,
                    };

                    promises.push(
                            Promise.all([
                                userDynamicDetails(order.driver_user_detail_id),
                                orderProductWithBrandDetails(data),
                                orderPaymentDetails(order.order_id),
                                orderProduct(data)
                            ])
                            .spread(function (driver, brand, payments, products) {   // Maps results from the 2nd promise array
                                order.driver = driver.length > 0 ? driver[0] : {};
                                order.brand = brand.length > 0 ? brand[0] : {};
                                order.payment = payments[0];
                                order.products = products.length > 0 ? products : [];
                                return order;

                            })
                    );

                });

                return Promise.all(promises);

            });


};

//////////////////////////		Driver Ride Details 	//////////////////////////////
exports.driverRideDetails = async (data) => {

    return Db.orders.findAll({
        attributes: orderDetailsSeqAtts,
        where: {order_id: data.order_id},
        include: [
            {
                model: Db.payments
            },
            {
                required: false,
                model: Db.order_ratings,
                as: "ratingByUser",
                where: {created_by: "Customer"},
                attributes: oRatingsSeqAtts
            },
            {
                required: false,
                model: Db.order_ratings,
                as: "ratingByDriver",
                where: {created_by: "Driver"},
                attributes: oRatingsSeqAtts
            },
            {
                required: false,
                model: Db.order_images,
                as: "order_images_url",
                attributes: oImagesSeqAtts
            }
        ]
    })
            .then((orders) => {

                var promises = [];

                orders = JSON.parse(JSON.stringify(orders));

                orders.forEach(function (order) {

                    promises.push(
                            Promise.all([
                                userDynamicDetails(order.customer_user_detail_id),
                                orderBrandDetails(order.category_id, order.category_brand_id, order.category_brand_product_id, data.app_language_id),
                                orderCouponDetails(order.coupon_user_id),
                                        //orderEtokenDetails(order.organisation_coupon_user_id)
                            ])
                            .spread(function (driver, brand, coupon) {   // Maps results from the 2nd promise array

                                order.driver = driver.length > 0 ? driver[0] : {};
                                order.brand = brand.length > 0 ? brand[0] : {};
                                order.coupon = coupon.length > 0 ? coupon[0] : {};
                                //order.etoken = etoken.length > 0 ? etoken[0] : {};
                                order.ratingByUser = order.ratingByUser != null ? order.ratingByUser : {};
                                order.ratingByDriver = order.ratingByDriver != null ? order.ratingByDriver : {};

                                return order;

                            })
                            );

                });

                return Promise.all(promises);

            });

};

///////////////////////	Driver Ride COmplete Details 	/////////////////////////////
exports.driverCompleteRideDetails = async (order_id, app_language_id) => {

    return Db.orders.findAll({
        attributes: orderDetailsSeqAtts,
        where: {order_id: order_id},
        include: [
            {
                model: Db.payments
            },
            {
                required: false,
                model: Db.order_ratings,
                as: "ratingByUser",
                where: {created_by: "Customer"},
                attributes: oRatingsSeqAtts
            },
            {
                required: false,
                model: Db.order_ratings,
                as: "ratingByDriver",
                where: {created_by: "Driver"},
                attributes: oRatingsSeqAtts
            },
            {
                required: false,
                model: Db.order_images,
                as: "order_images_url",
                attributes: oImagesSeqAtts
            }
        ]
    })
    .then((orders) => {
        var promises = [];
        orders = JSON.parse(JSON.stringify(orders));
                        var data = Array();
                        data.order_id = order_id;
                        data.app_language_id = app_language_id;
        orders.forEach(function (order) {

            promises.push(
                    Promise.all([
                        orderCustomerDetails(order.customer_user_detail_id),
                        orderBrandDetails(order.category_id, order.category_brand_id, order.category_brand_product_id, app_language_id),
                        orderCouponDetails(order.coupon_user_id),
                        orderProduct(data),
                        //orderEtokenDetails(order.organisation_coupon_user_id)
                    ])
                    .spread(function (user, brand, coupon,products /*etoken*/) {   // Maps results from the 2nd promise array
                       console.log(products);
                        order.user = user.length > 0 ? user[0] : {};
                        order.brand = brand.length > 0 ? brand[0] : {};
                        order.coupon = coupon.length > 0 ? coupon[0] : {};
                        order.products = products.length > 0 ? products : [];
                        //order.etoken = etoken.length > 0 ? etoken[0] : {};
                        order.ratingByUser = order.ratingByUser != null ? order.ratingByUser : {};
                        order.ratingByDriver = order.ratingByDriver != null ? order.ratingByDriver : {};

                        return order;
                    })
            );
        });
        return Promise.all(promises);
    });
};

//////////////////////////		Customer Details 	/////////////////////////////////

userDynamicDetails = async (user_detail_id) => {

    return Db.sequelize.query("SELECT u.user_id, ud.user_detail_id, u.name, ud.notifications, u.phone_number, u.phone_code, ud.profile_pic, ud.latitude, ud.longitude, (CASE WHEN ud.profile_pic='' THEN '' ELSE CONCAT('" + process.env.RESIZE_URL + "' , profile_pic) END) as profile_pic_url FROM user_details as ud JOIN users as u ON u.user_id=ud.user_id WHERE ud.user_detail_id=" + user_detail_id + " LIMIT 0,1 ",
            {type: Sequelize.QueryTypes.SELECT}
    );

};

///////////////////////////		Order Brand Details 	////////////////////////////

orderBrandDetails = async (category_id, category_brand_id, category_brand_product_id, app_language_id) => {

    return Db.sequelize.query("SELECT cd.category_id, cbpd.category_brand_product_id, cbpd.name as product_name, cbd.category_brand_id, cbpd.name as name, cbd.name as brand_name, cd.name as category_name, cbp.image, (CASE WHEN cbp.image='' THEN '' ELSE CONCAT('" + process.env.RESIZE_URL + "',cbp.image) END) as image_url FROM category_brand_product_details as cbpd JOIN category_brand_products AS cbp ON (cbp.category_brand_product_id=cbpd.category_brand_product_id) JOIN category_brand_details as cbd ON (cbd.category_brand_id=" + category_brand_id + " AND cbd.language_id=" + app_language_id + ") JOIN category_details as cd ON (cd.category_id=cbpd.category_id AND cd.language_id=" + app_language_id + ") WHERE cbpd.language_id=" + app_language_id + " LIMIT 0, 1 ",
            {type: Sequelize.QueryTypes.SELECT}
    );

};

//////////////////////		Payment Details 		/////////////////////////////////

orderPaymentDetails = async (order_id) => {

    return Db.sequelize.query("SELECT p.payment_id, p.payment_type, p.payment_status, p.refund_status, p.transaction_id, p.refund_id, p.buraq_percentage, p.product_quantity, p.product_weight, p.product_sq_mt, p.order_distance, p.product_alpha_charge, p.product_per_quantity_charge, p.product_per_weight_charge, p.product_per_distance_charge, p.product_per_hr_charge, p.product_per_sq_mt_charge, p.initial_charge, p.admin_charge, p.bank_charge, p.final_charge, p.bottle_charge, p.bottle_returned_value, p.updated_at  FROM payments as p WHERE p.order_id=" + order_id + " LIMIT 0, 1 ",
            {type: Sequelize.QueryTypes.SELECT}
    );

};

//////////////////////		Order Coupon Details 		////////////////////////////

orderCouponDetails = async (coupon_user_id) => {

    if (coupon_user_id == 0)
        return [];

    return Db.sequelize.query("SELECT cu.coupon_user_id, cu.coupon_id, cu.price, cu.coupon_type FROM coupon_users as cu WHERE cu.coupon_user_id=" + coupon_user_id + " ",
            {type: Sequelize.QueryTypes.SELECT}
    );

};


orderPromocodes = async (coupon_id) => {

    if (coupon_id == 0)
        return [];

    return Db.sequelize.query("SELECT cu.* FROM coupons as cu WHERE cu.coupon_id=" + coupon_id + " ",
            {type: Sequelize.QueryTypes.SELECT}
    );

};

/////////////////////		Order Coupon Etoken Details 	/////////////////////////

orderEtokenDetails = async (organisation_coupon_user_id) => {

    if (organisation_coupon_user_id == 0)
        return {};


    return Db.sequelize.query("SELECT ocu.organisation_coupon_user_id, ocu.price FROM organisation_coupon_users as ocu WHERE ocu.organisation_coupon_user_id=" + organisation_coupon_user_id + " ",
            {type: Sequelize.QueryTypes.SELECT}
    );

};

//////////////////////		Order Ratings 		/////////////////////////////////////

orderRatings = async (order_id, created_by) => {

    return Db.sequelize.query("SELECT order_rating_id, ratings, comments, created_by, created_at FROM order_ratings WHERE order_ratings.order_id=" + order_id + " AND created_by = '" + created_by + "' ",
            {type: Sequelize.QueryTypes.SELECT}
    );

};

//////////////////////		Order Images 		//////////////////////////////////////
orderImages = async (order_id) => {

    return Db.sequelize.query("SELECT order_image_id, image, CONCAT('" + process.env.RESIZE_URL + "',image) as image_url FROM order_images as oi WHERE oi.order_id=" + order_id + " ",
            {type: Sequelize.QueryTypes.SELECT}
    );

};

//////////////////////		Orders About to start 	//////////////////////////////////
exports.orderAbout2StartDetails = async (order_id) => {

    return await Db.orders.findOne({
        where: {
            order_id
        },
        include: [
            {
                as: "Customer",
                model: Db.users
            },
            {
                as: "CustomerDetails",
                model: Db.user_details
            },
            {
                as: "Driver",
                model: Db.users,
            },
            {
                as: "DriverDetails",
                model: Db.user_details
            },

            {
                model: Db.coupon_users,
                required: false,
                attributes: Configs.appData.coupon_user_atts///		Atts
            },
            {
                required: false,
                model: Db.order_images,
                attributes: oImagesSeqAtts
            }

        ],

    });

};

///////////////////////// 		User Rides 		/////////////////////////////////////
exports.appRidesListings = async (data) => {

    return Db.sequelize.query("SELECT *, (CASE WHEN track_image='' THEN '' ELSE CONCAT('" + process.env.RESIZE_URL + "',track_image) END) as track_image_url FROM orders as o WHERE " + data.string_check_id + "=" + data.user_detail_id + " AND o.order_status IN " + data.order_status_array + " ORDER BY o.updated_at DESC LIMIT " + data.skip + ", " + data.take + " ",
            {type: Sequelize.QueryTypes.SELECT}
    )
            .then((orders) => {

                var promises = [];

                orders = JSON.parse(JSON.stringify(orders));

                orders.forEach(function (order) {

                    promises.push(
                            Promise.all([
                                orderPaymentDetails(order.order_id),
                            ])
                            .spread(function (payments) {   // Maps results from the 2nd promise array

                                order.payment = payments[0];

                                return order;

                            })
                            );

                });

                return Promise.all(promises);

            });

};

//////////////////		User Past Upcoming Rides 		///////////////////////////
exports.userPastUpcomingOListings = (customer_user_detail_id, skip, take, order_status_array, order_type) => {
//console.log(order_status_array);
    return Db.orders.findAll({
        attributes: orderDetailsSeqAtts,
        where: {
            order_type,
            customer_user_detail_id,
            order_status: {$in: order_status_array}
        },
        include: [
            {
                model: Db.payments
            },
            {
                required: false,
                model: Db.order_requests,
                as: "cRequest",
                where: {order_request_status: "Accepted"},
                attributes: oReqSeqAtts
            }
        ],
        order: [["updated_at", "DESC"]],
        limit: take,
        offset: skip,
    });

};

//////////////////		User Past Upcoming Rides 		///////////////////////////
exports.driverPastUpcomingOListings = (driver_user_detail_id, skip, take, order_status_array, order_type) => {

    return Db.orders.findAll({
        attributes: orderDetailsSeqAtts,
        where: {
            order_type,
            driver_user_detail_id,
            order_status: {$in: order_status_array}
        },
        include: [
            {
                model: Db.payments
            }
        ],
        order: [["updated_at", "DESC"]],
        limit: take,
        offset: skip,
    });

};

////////////////////	Driver Water Order Details		////////////////////////////
exports.driveretokenOrderDetails = (order_id, driver_user_detail_id) => {

    return Db.orders.findOne({

        where: {order_id, driver_user_detail_id},

        include: [
            {
                model: Db.payments
            },
            {
                model: Db.users,
                as: "Customer"
            },
            {
                model: Db.user_details,
                as: "CustomerDetails",
            },
            {
                model: Db.order_requests,
                as: "CRequest",
                where: {driver_user_detail_id, order_request_status: "Accepted"},
                attributes: oReqSeqAtts
            },
            oCouponsHistoryInclude
        ]

    });

};

//////////////////////		Custoemr Water Order Details 		///////////////////////
exports.usereTokenOrderDetails = (order_id, customer_user_detail_id) => {

    return Db.orders.findOne({

        where: {order_id, customer_user_detail_id},
        include: [
            {
                model: Db.payments
            },
            {
                model: Db.users,
                as: "Driver"
            },
            {
                model: Db.user_details,
                as: "DriverDetails"
            }
        ]

    });

};

/////////////////////	Order Details 4 Auto Cancellation 		//////////////////////
exports.etokenOrderDetails4AutoCancellation = (order_id) => {

    return Db.orders.findOne({
        where: {order_id},
        include: [
            {
                model: Db.payments
            },
            {
                model: Db.users,
                as: "Driver"
            },
            {
                model: Db.user_details,
                as: "DriverDetails"
            },
            {
                model: Db.users,
                as: "Customer"
            },
            {
                model: Db.user_details,
                as: "CustomerDetails"
            }
        ]
    });

};


exports.AllOrderProducts = async (data) => { console.log("AllOrderProducts query fn ==", data);
    return Db.sequelize.query("SELECT product_weight,category_brand_product_id,image_url,productName,product_quantity,price_per_item FROM order_products AS products  WHERE products.order_id = "+data.order_id+"",
		{ type: Sequelize.QueryTypes.SELECT}
	);
};