var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;

var Db = require(appRoot+"/models");//////////////////		Db Models

//////////////////////		Latest Create Row 	//////////////////////////////////////
exports.CreateNewRow = async (data) => {

	return await Db.organisation_coupon_users.create({

		organisation_coupon_id: data.organisation_coupon_id,

		///////////		Category 	/////////////////
		category_id: data.category_id,
		category_brand_id: data.category_brand_id,
		category_brand_product_id: data.category_brand_product_id,
		///////////		Category 	/////////////////

		///////////		Seller 	/////////////////
		seller_user_id: data.seller_user_id,
		seller_user_detail_id: data.seller_user_detail_id,
		seller_user_type_id: data.seller_user_type_id,
		seller_organisation_id: data.seller_organisation_id,
		///////////		Seller 	/////////////////

		//////////////		User 	///////////////////
		customer_user_id: data.user_id,
		customer_user_detail_id: data.user_detail_id,
		customer_user_type_id: data.user_type_id,
		customer_organisation_id: data.user_organisation_id,
		//////////////		User 	///////////////////

		payment_id: data.payment_id,

		price: data.final_charge,
		quantity: data.quantity,
		quantity_left: data.quantity,

		created_at: data.created_at,
		updated_at: data.created_at
	});

};

///////////////////		Etoken Details after purchase 		////////////////////////////////////
exports.purchasedEtokenDetails = async (data) => {

	return await Db.organisation_coupon_users.findOne({

		where: { organisation_coupon_user_id: data.organisation_coupon_user_id },
		include: [
			{
				model: Db.organisation_coupons,
				as: "OrgCoupon"
			}
		]
	});

	// 			.then( (etoken) => {

	// 				if(!etoken) return null;

	// 				var promises = [];

	// 				etoken = JSON.parse(JSON.stringify(etoken));

	// 				data.payment_id = etoken.payment_id;

	// 		            promises.push(
	// 		                Promise.all([
	// 		                    paymentHistory(data)
	// 		                ])
	// 		                .spread(function(payment) {   // Maps results from the 2nd promise array

	// 		                    etoken.payment = payment;

	// 		            	})
	// 		            );

	// 		           return Promise.all(promises);

	// 			});

	// if(eToken == null)
	// 	return null
	// else
	// 	return eToken[0];

};

//////////////////////		Payment History 	/////////////////////////////////////
function paymentHistory(data) {

	return Db.payments.findOne({
		where: {
			payment_id: data.payment_id
		}
	});

}

//////////////////		Customer Etoken Purchased History Before applying		/////////////////////////
exports.purchasedETokenDetail4Order = async (data) => {

	return await Db.organisation_coupon_users.findOne({

		where: { 
			organisation_coupon_user_id: data.organisation_coupon_user_id,
			customer_user_detail_id: data.user_detail_id
		},
		include: [
			{
				model: Db.user_details,
				as: "SellerDetails"
			},
			{
				model: Db.organisations,
				as: "SellerOrg"
			}
		]
	});

};

///////////////////////		Update Single Etoken 	/////////////////////////////
exports.updateOrgCouponUser = async (organisation_coupon_user_id, quantity_left, created_at) => {

	return await Db.organisation_coupon_users.update(
		{
			quantity_left: quantity_left,
			updated_at: created_at
		},
		{
			where: { organisation_coupon_user_id: organisation_coupon_user_id }
		}
	);

};

////////////////////