var Db = require(appRoot+"/models");

////////////////////////////	Add New Details 	/////////////////////////////////////
exports.createNewRow = async (data) => {

	return await Db.user_bank_details.create({

		user_id: data.user_id,

		bank_name: data.bank_name ? data.bank_name : "",
		bank_account_number: data.bank_account_number ? data.bank_account_number : "" ,

		active: data.active ? data.active : "0",
		deleted: "0",

		created_at: data.created_at,
		updated_at: data.created_at

	});

};