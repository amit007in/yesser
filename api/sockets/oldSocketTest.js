var Sequelize = require("sequelize");
var Promise = Sequelize.Promise;
var moment = require("moment-timezone");

//var Controllers = require(appRoot+"/controllers");/////		Controllers
var Libs = require(appRoot+"/libs");//////////////////		Libraries
var Services = require(appRoot+"/services");///////////		Services
var Db = require(appRoot+"/models");//////////////////		Db Models
var Configs = require(appRoot+"/configs");/////////////		Configs

// io.configure(function () {

//   io.set('authorization', async (request, callback) => {

//       var cookie = parseCookie(request.headers.cookie);

//       if( !cookie && !cookie['connect.sid'] ) 
//         {
//           return callback(null, false);
//         }
//       else
//         {

//           sessionStore.get(utils.parseSignedCookie(cookie['connect.sid'], 'secret'), function (err, session) {
//             if ( err ) {
//               callback(err.message, false);
//             } else {
//               if ( session && session.username ) {
//                 request.user = session.username;
//                 callback(null, true);
//               } else {
//                 callback(null, false);
//               }
//             }
//           });


//         }
//   });

// });

// io.use( async (socket, response, next) => {

//   if (socket.handshake.query && socket.handshake.query.access_token)
//     {

//       // jwt.verify(socket.handshake.query.token, 'SECRET_KEY', function(err, decoded) {
//       //   if(err) return next(new Error('Authentication error'));
//       //   socket.decoded = decoded;
//       //   next();
//       // });

//       //console.log('11111111111111111111111111111111111111111');
//       next();

//     }
//   else 
//     {
//       //console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
//       return response(new Error('Authentication error'));
//       //next(new Error('Authentication error'));
//     }

// })

io.on("connection", async (socket) => {
	try{

		socket.on("disconnect", async () => {

			console.log("DISCONNECTED!!!!!!!!!!!!!!!!!!!!!!! ", socket.id);

			// var update_ud = await Db.user_details.update(
			//     {
			//       socket_id: "",
			//       updated_at: moment.utc().format('YYYY-MM-DD HH:mm:ss')
			//     },
			//     {
			//       where: { socket_id: socket.id }
			//     }
			// );

		});

		//console.log('Socket ID trying Log --------------------------------------', socket.handshake.query, socket.id);
		///////////////////////   Authentication  ///////////////////////////////////
		if (socket.handshake.query && socket.handshake.query)
		{

			if(socket.handshake.query.access_token)
			{///////////////////    App Token     /////////////////////////////

				var access_token = socket.handshake.query.access_token;

				var AppDetail = await Db.user_details.findOne({ where: {access_token: access_token} });
				if(!AppDetail)
				{
					return io.sockets.to(socket.id).emit("Error", { type: "AuthError", access_token });
				}

				var updated_at = await Libs.commonFunctions.currentUTC();

				var UserUpdate = await Db.user_details.update(
					{
						socket_id: socket.id,
						updated_at: updated_at
					},
					{
						where: {access_token: access_token}
					}
				);

				console.log("Connected!!!!!!!!!!!!!!!!!!!!!!!", socket.id, AppDetail.user_detail_id, updated_at);

				//console.log('Socket Id Updated and Authenticated', access_token, socket.id);

			}///////////////////    App Token     /////////////////////////////
			else
			{///////////////////  Panels  /////////////////////////////////////

				var admin_access_token = socket.handshake.query.admin_access_token;

				var PanelDetail = await Db.admin_user_detail_logins.findOne({ where: {access_token: admin_access_token} });
				if(!PanelDetail)
				{
					return io.sockets.to(socket.id).emit("Error", { type: "AuthError", access_token });
				}

				//console.log('Panel Socket Authenticated', access_token, socket.id );

			}///////////////////  Panels  /////////////////////////////////////

		}
		else
		{
			//console.log('Socket Id Not Updated and Not Authenticated', access_token);
			return io.sockets.to(socket.id).emit("Error", { type: "AuthError", AppDetail });
		}
		///////////////////////   Authentication  ///////////////////////////////////

		socket.on("CommonEvent", async (data, fn) => {
			try{
				//console.log('CommonEvent', data, socket.id);
				// return {awdd: "dadwd"});

				if(!data.type)
					return fn({ success: 0, statusCode: 400, msg: "Socket event type is required"});

				if(!data.access_token)
					return fn({ success: 0, statusCode: 400, msg: "Access Token is required"});

				var AppDetail = await Db.user_details.findOne({ where: {access_token: data.access_token} });
				if(!AppDetail)
					return fn({ success: 0, statusCode: 401, msg: "No device with this token found. Please login to continue" });

				data.user_id=AppDetail.user_id;
      
				// if(userDetail.blocked == '1')
				//   return {success: -2, statusCode: 403, msg: "Sorry, this service is currently not available to you. Please contact admin" });

				data.created_at = moment.utc().format("YYYY-MM-DD HH:mm:ss");

				var result = {success: 1, statusCode: 200, msg: "Common Event", data};

				if(data.type == "UpdateData")
				{
					result = await UpdateData(data, AppDetail);
				}
				else if(data.type == "CustHomeMap")
				{

					//console.log("CustHomeMapppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppp", data );

					result = await CustHomeMap(data, AppDetail);
				}
				else if(data.type == "DCurrentOrders")
				{
					if(AppDetail.user_type_id == 1)
						result = { success: 0, statusCode: 400, msg: "This facility is only available to Drivers" };
					else
						result = await DCurrentOrders(data, AppDetail);
				}
				else if(data.type == "CCurrentOrders")
				{
					result = await CCurrentOrders(data, AppDetail);
				}
				else if(data.type == "CustSingleOrder")
				{
					if(!data.order_token)
						result = { success: 0, statusCode: 400, msg: "Sorry, order token is required" };
					else
						result = await CustSingleOrder(data, AppDetail);      
				}

				return fn(result);

			}
			catch(e)
			{
				console.log("CommonEvent Error - "+e.message, data);
				return fn({ success: 0, statusCode: 500, msg: e.message});
			}
		});

		///////////////////   Panel Event     //////////////////////////////////
		socket.on("PanelCommonEvent", async (data, fn) => {

			var result;

			//console.log("-----------------------------------------------------------------------------", data);

			if(data.type == "TrackDriver")
			{///////////    Track Order   //////////////////////
				if(!data.order_id)
					result = { success: 0, statusCode: 400, msg: "Sorry, order id is required" };
				else
					result = await PanelTrackOrder(data);
				//return await AdminTracOrder(data));
			}///////////    Track Order   //////////////////////
			else if(data.type == "AdminDriverMap")
			{///////////    Admin Drivers Map     //////////////

				if(!data.services)
					data.services = [];

				if(!data.supports)
					data.supports = [];

				if(!data.online)
					data.online = 1;

				data.category_ids = data.services.concat(data.supports);

				if(data.category_ids.length == 0)
					drivers = [];
				else
					drivers = await Services.driverListingServices.adminDriverMaps(data);

				result = {  success: 1, statusCode: 200, msg: "All Drivers", drivers, data };

			}///////////    Admin Drivers Map     //////////////
			else if(data.type == "OrgDriverMap")
			{///////////    Admin Drivers Map     //////////////

				if(!data.services)
					data.services = [];

				if(!data.supports)
					data.supports = [];

				if(!data.online)
					data.online = 1;

				data.category_ids = data.services.concat(data.supports);

				if(data.category_ids.length == 0)
					drivers = [];
				else
					drivers = await Services.driverListingServices.orgDriverMaps(data);

				result = {  success: 1, statusCode: 200, msg: "All Drivers", drivers, data };

			}///////////    Admin Drivers Map     //////////////
			else if(data.type == "SingleDTrack")
			{///////////    Single Driver Track   //////////////

				if(!data.user_detail_id)
					result = { success: 0, statusCode: 400, msg: "Sorry, driver id is required" };
				else
					result = await PanelDTrackOrder(data.user_detail_id);          

			}///////////    Single Driver Track   //////////////

			return fn(result);

		});
		///////////////////   Panel Event     //////////////////////////////////

	}
	catch(e)
	{
		//console.log("PanelCommonEvent Error - "+e.message, data);
		return ({ success: 0, statusCode: 500, msg: e.message});
	}
});

///////////////   Single Driver Track   ////////////////////////////////
var PanelDTrackOrder = async (user_detail_id) => {

	var driver = await Db.user_details.findOne({ where: { user_detail_id }, attributes: ["latitude", "longitude", "bearing"] });
	if(!driver)
		return { success: 0, statusCode: 400, msg: "Sorry, this driver is currently not available" };

	return { success: 1, statusCode: 200, msg: "Single Details", driver };

};

/////////////   Panel Track Order     //////////////////////////////////
var PanelTrackOrder = async (data) => {
	try{

		var order = await Services.orderServices.PanelTrackOrder(data);

		if(!order)
			return { success: 0, statusCode: 200, msg: "Sorry, this order is currently not available" };

		if(order.SRequest)
			order.SRequest = JSON.parse(order.SRequest);

		return { success: 1, statusCode: 200, msg: "Orders", order };

	}
	catch(e)
	{
		return { success: 0, statusCode: 500, msg: e.message};
	}
};

/////////////   Update UserDevice Data    ////////////////////////
var UpdateData = async (data, AppDetail) => {
	try{

		//console.log('Socket Data -/////////////////////////////////////////////////////////////////// ', data);
		//    {socket_id: socket.id, type: "UpdateData", "access_token": "u39", "latitude":30.76, "longitude": 76.78, timezone: "Asia/Calcutta", fcm_id: "" }

		var updated_data = {
			//socket_id: data.socket_id,
			updated_at: await Libs.commonFunctions.currentUTC()
		};

		if(data.latitude)
			updated_data.latitude = data.latitude;
		if(data.longitude)
			updated_data.longitude = data.longitude;
		if(data.timezone)
		{
			updated_data.timezone = data.timezone;
			updated_data.timezonez = await Libs.commonFunctions.changeTimezoneFormat(data.timezone);
		}
		if(data.fcm_id)
			updated_data.fcm_id = data.fcm_id;

		// if(data.language_id)
		//   updated_data.language_id = data.language_id;

		if(data.bearing)
			updated_data.bearing = data.bearing;
		//data.rotation = data.bearing ? data.bearing : order.CRequest.rotation;

		var UserUpdate = await Db.user_details.update(
			updated_data,
			{
				where: {user_detail_id: AppDetail.user_detail_id},
			}
		);

		if(AppDetail.user_type_id != 1)
		{
			var AppDetail = await Services.userDetailServices.UserGetAppDetails(data);
		}
		else
		{
			var AppDetail = await Services.userDetailServices.ServiceDriverLoginDetails(data);
		}

		AppDetail = JSON.parse(JSON.stringify(AppDetail));
		AppDetail.profile_pic_url = (AppDetail.profile_pic  == "") ? "" : process.env.RESIZE_URL+AppDetail.profile_pic;

		//////////////    Ratings   ///////////////////////////
		var ratings = await Services.userDetailServices.userRatings(AppDetail.user_type_id ,AppDetail.user_detail_id);

		AppDetail.rating_count = parseInt(ratings[0].rating_count);
		AppDetail.ratings_avg = parseInt(ratings[0].ratings_avg);
		//////////////    Ratings   ///////////////////////////

		return { success: 1, statusCode: 200, msg: "Data updated successfully", AppDetail, Versioning: Configs.appData.Versioning };

	}
	catch(e)
	{
		return { success: 0, statusCode: 500, msg: e.message};
	}
};

///////////   Customer Home Map Drivers //////////////////////////
var CustHomeMap = async (data, AppDetail) => {
	try{
		//  {type: "custHomeMap", "access_token": "u39", "latitude":30.76, "longitude": 76.78, "category_id": 1, distance: 50 }
		if(!data.category_id)
			return { success: 0, statusCode: 400, msg: "Category Id is required"};
		else if(!data.distance)
			return { success: 0, statusCode: 400, msg: "Distance is required"};
		if(!data.latitude)
			return { success: 0, statusCode: 400, msg: "Latitude is required"};
		if(!data.longitude)
			return { success: 0, statusCode: 400, msg: "Longitude is required"};

		if(data.category_id < 6)
		{
			data.user_type_id = 2;
			data.category_type = "Service";
		}
		else
		{
			data.user_type_id = 3;
			data.category_type = "Support";
		}

		// if(!data.distance)
		//   data.distance = 50;

		data.past_5Mins = moment(data.created_at,"YYYY-MM-DD HH:mm:ss").subtract(2, "days").format("YYYY-MM-DD HH:mm:ss");

		var drivers = await Services.driverListingServices.userHomeMapDrivers(data);

		var result = {
			drivers
		};

		return {success: 1, statusCode: 200, msg: "Home map listings", result };

	}
	catch(e)
	{
		return { success: 0, statusCode: 500, msg: e.message};
	}
};

/////////////////////   Custoemr Single Order Details   //////////////////////////////
var CustSingleOrder = async (data, AppDetail) => {
	try{

		//console.log('Customer Single Order', data);

		data.user_detail_id = AppDetail.user_detail_id;
		data.app_language_id = AppDetail.language_id;

		var order = await Services.orderServices.customerSingleOrderDetails(data);
		if(order.length == 0 || order[0].customer_user_detail_id != data.user_detail_id)
			return { success: 0, statusCode: 400, msg: "Sorry, this order is currently not available", order };

		return { success: 1, statusCode: 200, msg: "Customer tracks current orders", result: order[0] };

	}
	catch(e)
	{
		return { success: 0, statusCode: 500, msg: e.message};
	}
};

///////////////////// Driver Current Rides Update     ////////////////////////////////
var DCurrentOrders = async (data, AppDetail) => {
	try{

		var neworders = [];

		//console.log('Current Orders', data);

		//    {type: "DCurrentOrders", "access_token": "u39", "latitude":30.76, "longitude": 76.78 }
		var orders = await Db.orders.findAll({
			where: {
				driver_user_detail_id: AppDetail.user_detail_id,
				order_status: { $in : ["Ongoing", "Accepted"]}// { /*$In: ["Ongoing", "Accepted"]*/ }
			},
			include: [
				{
					as: "CRequest",
					model: Db.order_requests,
					where: {
						driver_user_detail_id: AppDetail.user_detail_id
					}
				},
				{
					model: Db.user_details,
					as: "CustomerDetails"
				}
			]

		})
			.then( (orders) => {

				var promises = [];

				orders = JSON.parse(JSON.stringify(orders));

				orders.forEach(function(order) {

					var full_track = JSON.parse(order.CRequest.full_track);
					full_track.push({"Dt": data.created_at, "latitude": data.latitude, "longitude": data.longitude});
					full_track = JSON.stringify(full_track);

					data.rotation = data.bearing ? data.bearing : order.CRequest.rotation;
					data.bearing = data.rotation;

					promises.push(
						Promise.all([

							Db.sequelize.query("UPDATE user_details SET latitude="+data.latitude+", longitude="+data.longitude+", bearing='"+data.bearing+"', updated_at='"+data.created_at+"' WHERE user_detail_id="+AppDetail.user_detail_id+" "),
							Db.sequelize.query("UPDATE order_requests SET driver_current_latitude="+data.latitude+", driver_current_longitude="+data.longitude+", full_track='"+full_track+"', rotation='"+data.rotation+"', updated_at= '"+data.created_at+"' WHERE  order_request_id="+order.CRequest.order_request_id+" ")

						])
							.spread(function() {   // Maps results from the 2nd promise array
                  

								if(order.CustomerDetails.socket_id != "")
								{/////////////    Emit To customer  ////////////////////

									var socket_data = {
										type: "CurrentOrders",
										order_id: order.order_id,
										driver_id: order.driver_id,
										latitude: data.latitude,
										longitude: data.longitude,
										bearing: data.bearing ? data.bearing : 0.0,
										polyline: data.polyline ? data.polyline : {},
										order_status: order.order_status,
										my_turn: order.my_turn
									};

									Libs.commonFunctions.emitToDevice("OrderEvent", order.CustomerDetails.socket_id, socket_data);
                      
								}/////////////    Emit To customer  ////////////////////

								delete order.CRequest;
								delete order.CustomerDetails; 

								neworders.push(order);

								return order;

							})
					);

				});

				return Promise.all(promises);

			});

		var result = {
			orders: neworders
		};

		////console.log("Driver Current Orders");

		return { success: 1, statusCode: 200, msg: "Driver update current orders", result };

	}
	catch(e)
	{
		//console.log(e.message, 'DCurrentOrders Error');
		return { success: 0, statusCode: 500, msg: e.message};
	}
};
//////////////////////    Event Emit to customer   ///////////////////////////////////////////

/////////////////////     Customer Current Orders   /////////////////////////////////////////
var CCurrentOrders = async (data, AppDetail) =>  {
	try{
		data.user_detail_id = AppDetail.user_detail_id;
		data.app_language_id = AppDetail.language_id;

		var orders = await Services.orderServices.customerCurrentOrders(data);

		var result = {
			orders
		};

		return { success: 1, statusCode: 200, msg: "Customer tracks current orders", result };

	}
	catch(e)
	{
		//console.log(e.message, 'CCurrentOrders Error');
		return { success: 0, statusCode: 500, msg: e.message};
	}
};
/////////////////////